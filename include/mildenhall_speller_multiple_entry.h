/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_multiple_entry.h
 *
 *
 *  mildenhall_speller_multiple_entry.h  */

#ifndef __MILDENHALL_SPELLER_MULTIPLE_ENTRY_H__
#define __MILDENHALL_SPELLER_MULTIPLE_ENTRY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY mildenhall_speller_multiple_entry_get_type()

#define MILDENHALL_SPELLER_MULTIPLE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, MildenhallSpellerMultipleEntry))

#define MILDENHALL_SPELLER_MULTIPLE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, MildenhallSpellerMultipleEntryClass))

#define MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY))

#define MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY))

#define MILDENHALL_SPELLER_MULTIPLE_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, MildenhallSpellerMultipleEntryClass))

typedef struct _MildenhallSpellerMultipleEntry MildenhallSpellerMultipleEntry;
typedef struct _MildenhallSpellerMultipleEntryClass MildenhallSpellerMultipleEntryClass;
typedef struct _MildenhallSpellerMultipleEntryPrivate MildenhallSpellerMultipleEntryPrivate;

/**
 * MildenhallSpellerMultipleEntry:
 *
 * The #MildenhallSpellerMultipleEntry struct contains only private data.
 *
 */
struct _MildenhallSpellerMultipleEntry
{
  ClutterActor parent;
  MildenhallSpellerMultipleEntryPrivate *priv;
};

/**
 * MildenhallSpellerMultipleEntryClass:
 *
 * The #MildenhallSpellerMultipleEntryClass struct contains only private data.
 *
 */
struct _MildenhallSpellerMultipleEntryClass
{
  ClutterActorClass parent_class;
  void (* multiple_state_change) (GObject *pButton, gchar *pEntryId);
};

GType mildenhall_speller_multiple_entry_get_type (void) G_GNUC_CONST;

/*function to create new multi-entry */
ClutterActor *mildenhall_speller_multiple_entry_new (void);
void v_multiple_speller_set_text(MildenhallSpellerMultipleEntry *pSelf, GVariant *pArgList);
void v_multiple_speller_set_clear_text(MildenhallSpellerMultipleEntry *pSelf, gboolean clearText);
void v_multiple_speller_set_focus_entry(MildenhallSpellerMultipleEntry *pSelf, gboolean focus_entry);
void v_multiple_speller_set_model(MildenhallSpellerMultipleEntry *pSelf, ThornburyModel *pModel);
ThornburyModel *multiple_create_text_box_model (void);
ThornburyModel *multiple_create_toggle_model (void);
void v_multiple_speller_set_cursor_focus (MildenhallSpellerMultipleEntry *pSelf, gint cursor_focus_entry);
void v_multiple_speller_column_changed_cb (gint col, gpointer pSelf);


G_END_DECLS

#endif /* __MILDENHALL_SPELLER_MULTIPLE_ENTRY_H__ */

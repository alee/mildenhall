/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**************************************************************************************************
 * Filename          : test_overlay.c
 * Package           : Mildenhall
 * Widget		     : MildenhallOverlay
 * Description       : 
 **************************************************************************************************/

/**********************************************************************
                     	Header Files
***********************************************************************/

#include "mildenhall_overlay.h"

#include <stdio.h>
#include <thornbury/thornbury.h>

/*********************************************************************************************
 * Function:    main
 * Description: Main function
 * Parameters:  argc and argv[]
 * Return:      int
 ********************************************************************************************/
int main(int argc, char *argv[])
{
	ClutterActor *pStage;
	ClutterColor pStageColor = {0x00, 0x00, 0x00, 0xff};

	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;
	/* No property json file for this widget */
	ThornburyItemFactory *pItemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_OVERLAY , NULL);
	GObject *pObject = NULL;
	g_object_get(pItemFactory, "object", &pObject, NULL);
	MildenhallOverlay *pOverlay=MILDENHALL_OVERLAY(pObject);
	pStage = clutter_stage_new();
	clutter_actor_set_background_color(CLUTTER_ACTOR(pStage), &pStageColor);
	clutter_actor_set_size(pStage, 800, 600);
	clutter_actor_add_child(CLUTTER_ACTOR(pStage), CLUTTER_ACTOR(pOverlay));
	clutter_actor_set_position(CLUTTER_ACTOR(pOverlay), 0, 0);
	clutter_actor_show(pStage);
	clutter_main();
	return 0;
}

/*********************************************************************
                        End of file
**********************************************************************/


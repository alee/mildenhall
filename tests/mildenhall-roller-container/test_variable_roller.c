/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/***********************************************************************************
*       @Filename       :       roller-container.c
*       @Module         :       RollerContainer
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       18/07/2012
*----------------------------------------------------------------------------------
*       @Description    :       The basic idea of RollerContainer To customize the
*                               mx-roller as per Apertis requirements.They are,
*                               1)Background color for roller
*                               2)Arrow at the focus item
*                               3)Blurr Effects
*                               4)Any addtional overlay upon the roller if any.
*
************************************************************************************/

#include "mildenhall-internal.h"
#include "mildenhall_roller_container.h"

#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"

#include "sample-item.h"
#include "sample-variable-item.h"
#include "test-utils.h"

static void
roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data)
{
        g_print("*************test app: row = %d\n",row);
}
gboolean append_clb(gpointer userData)
{
	g_autofree gchar *email_icon_file = NULL;
	g_autofree gchar *launcher_video_file = NULL;
	CoglHandle icon;

        static guint new_item_count = 0;
        ThornburyModel *model = (ThornburyModel *)userData;

	email_icon_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "icon_email.png",
	    NULL);

        icon = cogl_texture_new_from_file (email_icon_file,
                        COGL_TEXTURE_NONE,
                        COGL_PIXEL_FORMAT_ANY,
                        NULL);

        g_print("new_item_count = %d\n",new_item_count);

	launcher_video_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "01_appl_launcher_2009.03.09.avi",
	    NULL);

        thornbury_model_append (model, 0, g_strdup_printf ("new item number %i",
                                new_item_count),
                        1, icon,
                        2, g_strdup_printf ("new item number %i",
                                new_item_count),
                        3, FALSE,
                        4, launcher_video_file,
                        -1);
        new_item_count++;
        //v_ROLLERCONT_set_focused_row(MILDENHALL_ROLLER_CONTAINER (userData),0,TRUE);

        return TRUE;

}
gboolean set_focus(gpointer userData)
{
        g_object_set(G_OBJECT(userData),"focused-row", 0,NULL);
        return FALSE;
}

int main(int argc,char ** argv)
{
	g_autofree gchar *prop_file = NULL;
        ThornburyItemFactory *itemFactory;
        ClutterInitError err = clutter_init(&argc, &argv);
        (void)err;
        ClutterColor black = {0x00, 0x00, 0x00, 0xff};
        /* creating the stage */
        ClutterActor *stage = clutter_stage_new();
        clutter_actor_set_background_color(stage, &black);
        clutter_actor_set_size (stage,800,480);

 ThornburyModel *model = lightwood_create_model (2);
        prop_file = _mildenhall_get_resource_path ("mh_variable_roller_prop.json");
        itemFactory = thornbury_item_factory_generate_widget_with_props (
            MILDENHALL_TYPE_ROLLER_CONTAINER, prop_file);
        GObject *pObject = NULL;
        //pObject = g_object_new(MILDENHALL_TYPE_ROLLER_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 300.0, "height", 380.0, "roll-over", TRUE, "reactive", TRUE, "arrow", TRUE, "arrowPosition", 190.0, NULL);
        g_object_get(itemFactory, "object", &pObject, NULL);
        ClutterActor *cont = CLUTTER_ACTOR(pObject);
        g_object_set(pObject,  "item-type", TYPE_SAMPLE_VARIABLE_ITEM,/* "model", model, */ NULL);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "name", COLUMN_NAME);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "label", COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "extra-height", 5);
        g_object_set(pObject,  "model", model, NULL);
	g_signal_connect (G_OBJECT (cont), "roller-item-activated", G_CALLBACK (roller_item_activated_cb), model);
	g_timeout_add_seconds(2, set_focus, pObject);
        clutter_actor_add_child(stage,cont);
        clutter_actor_show(stage);
        clutter_main();
	
	return EXIT_SUCCESS;
}


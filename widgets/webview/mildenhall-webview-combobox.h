/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_WEBVIEW_COMBO_BOX_H__
#define __MILDENHALL_WEBVIEW_COMBO_BOX_H__

#include <glib-object.h>
#include <shm-combobox.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_WEBVIEW_COMBO_BOX mildenhall_webview_combo_box_get_type()

#define MILDENHALL_WEBVIEW_COMBO_BOX(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_WEBVIEW_COMBO_BOX, MildenhallWebviewComboBox))

#define MILDENHALL_WEBVIEW_COMBO_BOX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_WEBVIEW_COMBO_BOX, MildenhallWebviewComboBoxClass))

#define MILDENHALL_IS_WEBVIEW_COMBO_BOX(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_WEBVIEW_COMBO_BOX))

#define MILDENHALL_IS_WEBVIEW_COMBO_BOX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_WEBVIEW_COMBO_BOX))

#define MILDENHALL_WEBVIEW_COMBO_BOX_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_WEBVIEW_COMBO_BOX, MildenhallWebviewComboBoxClass))

typedef struct _MildenhallWebviewComboBox MildenhallWebviewComboBox;
typedef struct _MildenhallWebviewComboBoxClass MildenhallWebviewComboBoxClass;
typedef struct _MildenhallWebviewComboBoxPrivate MildenhallWebviewComboBoxPrivate;

struct _MildenhallWebviewComboBox
{
  ShmComboBox parent;

  MildenhallWebviewComboBoxPrivate *priv;
};

struct _MildenhallWebviewComboBoxClass
{
  ShmComboBoxClass parent_class;
};

G_END_DECLS

#endif /* __MILDENHALL_WEBVIEW_COMBO_BOX_H__ */

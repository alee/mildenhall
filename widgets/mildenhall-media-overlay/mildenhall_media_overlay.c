/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_media_overlay
 * @title: MildenhallMediaOverlay
 * @short_description: An overlay that can be placed on top of video. It includes a play/pause button and text.
 * @see_also: #MildenhallOverlay
 * @stability: stable
 *
 * #MildenhallMediaOverlay uses a  transparent icon, which changes play and
 * pause with a property change and used in Media based applications.
 *
  * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * MildenhallMediaOverlay *overlay = NULL;
 *
 * overlay = mildenhall_media_overlay_new (150, 50, FALSE);
 * g_object_set (overlay, "play-state", TRUE,
 * 		"overlay-text", "currently playing ...", NULL);
 * clutter_actor_add_child (stage, overlay);
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include"mildenhall_media_overlay.h"

/* media overlay text font size */
#define MILDENHALL_MEDIA_OVERLAY_FONT_SIZE_18		"DejaVuSansCondensed 16px"

/* media overlay shadow margin */
#define  MILDENHALL_MEDIA_OVERLAY_SHADOW_MARGIN	24

/* The private structure for the media overlay */
struct _MildenhallMediaOverlayPrivate
{
	gfloat width;
	gfloat height;
	gboolean playState;
	gboolean HasTitleRect;
	gchar *mediaOverlaytext;
	ClutterActor *overlayGroup;
	ClutterActor *overlayPlay;
	ClutterActor *overlayPause;
	ClutterActor *overlayText;
	ClutterActor *coverOverlay;
	gchar *text;
};

/* Define the media overlay as a gobject that actually derives the
 * properties of a mmd actor as it's parent type is mmd
 * actor */

G_DEFINE_TYPE_WITH_PRIVATE   (MildenhallMediaOverlay, mildenhall_media_overlay, CLUTTER_TYPE_ACTOR);

#define MILDENHALL_MEDIA_OVERLAY_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE((o), MILDENHALL_TYPE_MEDIA_OVERLAY, MildenhallMediaOverlayPrivate))

/* media overlay property enum. These refer to the properties of
 * the media overlay created */
enum MildenhallMediaOverlayProperty
{
	PROP_MILDENHALL_MEDIA_OVERLAY_FIRST,
	PROP_MILDENHALL_MEDIA_OVERLAY_PLAY_STATE,
	PROP_MILDENHALL_MEDIA_OVERLAY_TEXT,
	PROP_SCALE,
	PROP_MILDENHALL_MEDIA_OVERLAY_LAST
};


/* static APIs for media overlay property get nethods */
static void get_media_overlay_play_state(GObject *object, GValue *value);
static void get_media_overlay_text(GObject *object, GValue *value);

/* static APIs for media overlay property get nethods */
static void set_media_overlay_play_state(GObject *object, const GValue *value);
static void set_media_overlay_text(GObject *object, const GValue *value);


/* Initialisation and destruction functions of the elements that make up
 * the media overlay */
static void destroy_media_overlay(MildenhallMediaOverlay *object);
static void mildenhall_media_overlay_dispose (GObject *object);
static void mildenhall_media_overlay_finalize (GObject *object);
static void mildenhall_media_overlay_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec);
static void mildenhall_media_overlay_set_property (GObject *object, guint propertyId, const GValue *value, GParamSpec *pspec);
static void create_media_overlay(MildenhallMediaOverlay *mediaOverlay, gfloat, gfloat, gboolean);

static void create_text_actor(MildenhallMediaOverlay *mediaOverlay);
static void update_overlay_play_state(MildenhallMediaOverlay *mediaOverlay);
static void update_overlay_text(MildenhallMediaOverlay *mediaOverlay);

/*********************************************************************************************
 * Function:    destroy_media_overlay
 * Description: Free up all memory taken up by the media overlay
 * Parameters:  The object reference
 * Return:      void
 *********************************************************************************************/
static void destroy_media_overlay(MildenhallMediaOverlay *object)
{
	MildenhallMediaOverlayPrivate *priv;

	g_return_if_fail (MILDENHALL_IS_MEDIA_OVERLAY(object));

	priv = mildenhall_media_overlay_get_instance_private(object);

	if(NULL != priv->text)
	{
		g_free(priv->text);
		priv->text = NULL;
	}
}

/*********************************************************************************************
 * Function:    mildenhall_media_overlay_dispose
 * Description: Dispose the media overlay object
 * Parameters:  The object reference
 * Return:      void
 *********************************************************************************************/
static void mildenhall_media_overlay_dispose (GObject *object)
{
	/* Free all memory held */
	destroy_media_overlay(MILDENHALL_MEDIA_OVERLAY(object));

	G_OBJECT_CLASS (mildenhall_media_overlay_parent_class)->dispose (object);
}

/*********************************************************************************************
 * Function:    mildenhall_media_overlay_finalize
 * Description: Finalize the media element object
 * Parameters:  The object reference
 * Return:      void
 *********************************************************************************************/
static void mildenhall_media_overlay_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_media_overlay_parent_class)->finalize (object);
}


/*********************************************************************************************
 * Function:    mildenhall_media_overlay_get_property
 * Description: Get a property value
 * Parameters:  The object reference, property Id, return location
 *              for where the property value is to be returned and
 *              the param spec of the object
 * Return:      void
 *********************************************************************************************/
static void mildenhall_media_overlay_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	/* Based on the property ID take the appropriate action (Call
	 * the respective method to get the property value) */
	switch(propertyId)
	{
		case PROP_MILDENHALL_MEDIA_OVERLAY_PLAY_STATE:
			get_media_overlay_play_state(object, value);
			break;

		case PROP_MILDENHALL_MEDIA_OVERLAY_TEXT:
			get_media_overlay_text(object, value);
			break;
		case PROP_SCALE:
			g_value_set_float(value, clutter_actor_get_width(CLUTTER_ACTOR(object)) );
			break;
		/* In case, the property is not installed for this object,
		 * throw an appropriate warning */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
	}
}

/*********************************************************************************************
 * Function:    mildenhall_media_overlay_set_property
 * Description: Set a property value
 * Parameters:  The object reference, property Id, return location
 *              for where the property value is to be returned and
 *              the param spec of the object
 * Return:      void
 *********************************************************************************************/
static void mildenhall_media_overlay_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
	/* Based on the property ID take the appropriate action (Call
	 * the respective method to set the property value) */
	gfloat scale;
	//MildenhallMediaOverlayPrivate *priv = MILDENHALL_MEDIA_OVERLAY(object)->priv;
	switch(propertyId)
	{
		case PROP_MILDENHALL_MEDIA_OVERLAY_PLAY_STATE:
			set_media_overlay_play_state(object, value);
			break;

		case PROP_MILDENHALL_MEDIA_OVERLAY_TEXT:
			set_media_overlay_text(object, value);
			break;
		case PROP_SCALE:
			scale = g_value_get_float(value);
			clutter_actor_set_scale(CLUTTER_ACTOR(object),
					scale/clutter_actor_get_width(CLUTTER_ACTOR(object)),
					scale/clutter_actor_get_height(CLUTTER_ACTOR(object)));
			break;
		/* In case, the property is not installed for this object,
		 * throw an appropriate warning */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
	}
}

/*********************************************************************************************
 * Function:    get_media_overlay_play_state
 * Description: Get the media overlay play state property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/
static void get_media_overlay_play_state (GObject *object, GValue *value)
{
	MildenhallMediaOverlayPrivate *priv;

	g_return_if_fail (MILDENHALL_IS_MEDIA_OVERLAY(object));

	priv = mildenhall_media_overlay_get_instance_private(MILDENHALL_MEDIA_OVERLAY(object));

	/* get the play state for the media overlay */
	g_value_set_boolean(value, priv->playState);
}

/*********************************************************************************************
 * Function:    get_media_overlay_play_state
 * Description: Get the media overlay text property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/
static void get_media_overlay_text (GObject *object, GValue *value)
{
	MildenhallMediaOverlayPrivate *priv;

	g_return_if_fail (MILDENHALL_IS_MEDIA_OVERLAY(object));

	priv = mildenhall_media_overlay_get_instance_private(MILDENHALL_MEDIA_OVERLAY(object));

	if(FALSE == priv->HasTitleRect)
		return;


	/* get the text for the media overlay */
	g_value_set_string(value, priv->mediaOverlaytext);
}

/*********************************************************************************************
 * Function:    set_media_overlay_play_state
 * Description: Set the media overlay play state property
 * Parameters:  The object reference, and the property
 *				value for the property
 * Return:      void
 ********************************************************************************************/
static void set_media_overlay_play_state (GObject *object, const GValue *value)
{
	MildenhallMediaOverlayPrivate *priv;

	g_return_if_fail (MILDENHALL_IS_MEDIA_OVERLAY(object));

	priv = mildenhall_media_overlay_get_instance_private(MILDENHALL_MEDIA_OVERLAY(object));

	/* set the play state */
	priv->playState = g_value_get_boolean(value);
	/* update the overlay play state */
	update_overlay_play_state(MILDENHALL_MEDIA_OVERLAY(object));
}

/*********************************************************************************************
 * Function:    set_media_overlay_play_state
 * Description: Set the media overlay text property
 * Parameters:  The object reference, and the property
 *              value for the property
 * Return:      void
 ********************************************************************************************/
static void set_media_overlay_text (GObject *object, const GValue *value)
{
	MildenhallMediaOverlayPrivate *priv;

	g_return_if_fail (MILDENHALL_IS_MEDIA_OVERLAY(object));

	priv = mildenhall_media_overlay_get_instance_private(MILDENHALL_MEDIA_OVERLAY(object));


	/* If the media overlay text variable is holding a value, free it and include
	 * the new value into it */
	if(NULL != priv->mediaOverlaytext)
	{
		g_free(priv->mediaOverlaytext);
		priv->mediaOverlaytext = NULL;
	}
	/* set the media overlay text */
	priv->text = g_value_dup_string(value);
	//g_print("%s \n", priv->text);
	/* update the media text */
	update_overlay_text(MILDENHALL_MEDIA_OVERLAY(object));
}

/*********************************************************************************************
 * Function:    mildenhall_media_overlay_class_initsBar,
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to
					g_object_new
*  Parameters:  The object's class reference
*  Return:      void
*********************************************************************************************/
static void mildenhall_media_overlay_class_init(MildenhallMediaOverlayClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	GParamSpec *pspec = NULL;

	/* Assign pointers to our functions */
	o_class->finalize = mildenhall_media_overlay_finalize;
	o_class->dispose = mildenhall_media_overlay_dispose;
	o_class->set_property = mildenhall_media_overlay_set_property;
	o_class->get_property = mildenhall_media_overlay_get_property;

	/* Install the play state property */
	 /**
         * MildenhallMediaOverlay:play-state:
         *
         * To set the play state as TRUE/FLASE
         */
	pspec = g_param_spec_boolean("play-state",
				"PLAY-STATE",
				"State to indicate if media is playing or paused",
				FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(o_class, PROP_MILDENHALL_MEDIA_OVERLAY_PLAY_STATE, pspec);

	/* Install the media overlay text property */
         /**
         * MildenhallMediaOverlay:overlay-text:
         *
         * To set the text at bottom
         */
	pspec = g_param_spec_string("overlay-text",
				"Media-Overlay-Text",
				"Text to display foe media overlay",
				NULL,
				G_PARAM_READWRITE);
	g_object_class_install_property(o_class, PROP_MILDENHALL_MEDIA_OVERLAY_TEXT, pspec);

	/* Install the media overlay scale property */
	/**
         * MildenhallMediaOverlay:scale:
         *
         * To set scale value for the object
         */
	pspec = g_param_spec_float ("scale",
                              "Scale",
                              "Overlay scale value",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
        g_object_class_install_property (o_class, PROP_SCALE, pspec);

}

/*********************************************************************************************
 * Function:    mildenhall_media_overlay_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters:  The object's reference
 * Return:      void
 *********************************************************************************************/
static void mildenhall_media_overlay_init(MildenhallMediaOverlay *actor)
{
	MildenhallMediaOverlayPrivate *priv = mildenhall_media_overlay_get_instance_private(actor);
	priv->coverOverlay = NULL;
	priv->overlayPause = NULL;

	create_media_overlay(actor, 162.0, 162.0, TRUE);
}

/*********************************************************************************************/
/* EXPOSED API */
/*********************************************************************************************/
/**
 * mildenhall_media_overlay_new:
 * @width: width for the overlay
 * @height: height for the overlay
 * @needTitleRect: if TRUE, will add the title to the overlay widget. FALSE otherwise
 *
 * Creates a new media overlay
 *
 * Returns: (nullable) (transfer full): The new #MildenhallMediaOverlay object
 **/
MildenhallMediaOverlay* mildenhall_media_overlay_new(gfloat width, gfloat height, gboolean needTitleRect)
{
	ClutterActor *mediaOverlay = g_object_new (MILDENHALL_TYPE_MEDIA_OVERLAY, NULL);

	if(mediaOverlay)
	{
			//MildenhallMediaOverlayPrivate *priv = MILDENHALL_MEDIA_OVERLAY(mediaOverlay)->priv;

			create_media_overlay(MILDENHALL_MEDIA_OVERLAY(mediaOverlay), width, height, needTitleRect);

			return MILDENHALL_MEDIA_OVERLAY(mediaOverlay);
	}
	else
		return NULL;
}

/*********************************************************************************************
 * Function:    create_media_overlay
 * Description: Creates the media overlay
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void create_media_overlay(MildenhallMediaOverlay *mediaOverlay, gfloat width, gfloat height, gboolean needTitleRect)
{
	ClutterColor textRectColor = {0x00, 0x00, 0x00, 0xFF};

	MildenhallMediaOverlayPrivate *priv = mildenhall_media_overlay_get_instance_private(mediaOverlay);

	/* create media overlay group */
	clutter_actor_set_size(CLUTTER_ACTOR(mediaOverlay), width, height);
	//priv->height = height;
	//priv->width = width;
# if 0
	/* overlay pause texture */
        priv->coverOverlay = thornbury_ui_texture_create_new(PKGTHEMEDIR "/cover_overlay_shadow.png", width, height, TRUE, FALSE);
        clutter_actor_add_child(CLUTTER_ACTOR(mediaOverlay), priv->coverOverlay);
# endif
	/* overlay play texture */
	priv->overlayPlay = thornbury_ui_texture_create_new(PKGTHEMEDIR "/cover_overlay_pause.png", width/2, height/2, TRUE, FALSE);
	clutter_actor_set_position(priv->overlayPlay, width/4, height/4);
	clutter_actor_add_child(CLUTTER_ACTOR(mediaOverlay), priv->overlayPlay);

	/* overlay pause texture */
	priv->overlayPause = thornbury_ui_texture_create_new(PKGTHEMEDIR "/cover_overlay_play.png", width/2, height/2, TRUE, FALSE);
	clutter_actor_set_position(priv->overlayPause, width/4, height/4);
	clutter_actor_add_child(CLUTTER_ACTOR(mediaOverlay), priv->overlayPause);
	/* hide the pause actor */
	clutter_actor_hide(priv->overlayPause);

	/* Create the title rectangle only if required */
	priv->HasTitleRect = needTitleRect;
	if(TRUE == needTitleRect)
	{
		/* create the rectangle for media text */
		ClutterActor *overlayTextRect = clutter_actor_new();
		clutter_actor_set_background_color(overlayTextRect, &textRectColor);
		clutter_actor_set_position(overlayTextRect, 0 , height*.85);
		clutter_actor_set_size(overlayTextRect, width, .15*height);
		clutter_actor_add_child(CLUTTER_ACTOR(mediaOverlay), overlayTextRect);

		create_text_actor(mediaOverlay);

		g_object_set(mediaOverlay, "overlay-text", "Default-Text", NULL);
	}
}

/*********************************************************************************************
 * Function:    update_overlay_play_state
 * Description: updates the media overlay play state
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void update_overlay_play_state(MildenhallMediaOverlay *mediaOverlay)
{
	//g_print("%s\n", __FUNCTION__);
	MildenhallMediaOverlayPrivate *priv = mildenhall_media_overlay_get_instance_private(mediaOverlay);
	//g_print("%d\n", priv->playState);
	if(priv->playState)
	{
		//g_print("%s %d\n", __FUNCTION__, __LINE__);
		clutter_actor_hide(priv->overlayPlay);
		clutter_actor_show(priv->overlayPause);
	}
	else
	{
		//g_print("%s %d\n", __FUNCTION__, __LINE__);
		clutter_actor_hide(priv->overlayPause);
		clutter_actor_show(priv->overlayPlay);
	}
}

/*********************************************************************************************
 * Function:    update_overlay_text
 * Description: update the media overlay text
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void update_overlay_text(MildenhallMediaOverlay *mediaOverlay)
{
	//g_print("%s\n", __FUNCTION__);
	MildenhallMediaOverlayPrivate *priv = mildenhall_media_overlay_get_instance_private(mediaOverlay);

	create_text_actor(mediaOverlay);
	/* set the new overlay text */
	clutter_text_set_text(CLUTTER_TEXT(priv->overlayText), priv->text);

	//g_print("%f %f\n", clutter_actor_get_width(priv->overlayText), clutter_actor_get_height(priv->overlayText));
	//g_print("%f %f\n", clutter_actor_get_width(CLUTTER_ACTOR(mediaOverlay)), clutter_actor_get_height(CLUTTER_ACTOR(mediaOverlay)));

	if(clutter_actor_get_width(priv->overlayText) >=.5* clutter_actor_get_width(CLUTTER_ACTOR(mediaOverlay)))
		clutter_actor_set_width(priv->overlayText, .5*clutter_actor_get_width(CLUTTER_ACTOR(mediaOverlay)));

	clutter_actor_set_position(priv->overlayText, (clutter_actor_get_width(CLUTTER_ACTOR(mediaOverlay)) - clutter_actor_get_width(priv->overlayText))/2, .85*clutter_actor_get_height(CLUTTER_ACTOR(mediaOverlay)));

	//g_print("%s\n", clutter_text_get_text(CLUTTER_TEXT(priv->overlayText)));
	//g_print("%f %f\n", clutter_actor_get_x(priv->overlayText), clutter_actor_get_y(priv->overlayText));
	//g_print("%f %f\n", priv->width, priv->height);
}

/*********************************************************************************************
 * Function:    create_text_actor
 * Description: create the media overlay text
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void create_text_actor(MildenhallMediaOverlay *mediaOverlay)
{
	ClutterColor textColor = {0xFF, 0xFF, 0xFF, 0xFF};

	MildenhallMediaOverlayPrivate *priv = mildenhall_media_overlay_get_instance_private(mediaOverlay);

	/* destroy the previous text actor */
	if(priv->overlayText)
		clutter_actor_destroy(priv->overlayText);
	/* create new media text */
	priv->overlayText = clutter_text_new();
	clutter_text_set_color(CLUTTER_TEXT(priv->overlayText), &textColor);
	clutter_text_set_font_name (CLUTTER_TEXT(priv->overlayText), MILDENHALL_MEDIA_OVERLAY_FONT_SIZE_18);
	clutter_text_set_ellipsize(CLUTTER_TEXT(priv->overlayText), 3);
	clutter_actor_add_child(CLUTTER_ACTOR(mediaOverlay), priv->overlayText);
}

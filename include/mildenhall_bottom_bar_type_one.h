/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* bottom-bar.h */

#ifndef __MILDENHALL_BOTTOM_BAR_H__
#define __MILDENHALL_BOTTOM_BAR_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_BOTTOM_BAR mildenhall_bottom_bar_get_type ()
G_DECLARE_DERIVABLE_TYPE (MildenhallBottomBar, mildenhall_bottom_bar, MILDENHALL, BOTTOM_BAR, ClutterActor)

struct _MildenhallBottomBarClass
{
  ClutterActorClass parent_class;

  void (*action_press)          (MildenhallBottomBar *self, const gchar *name);
  void (*action_release)        (MildenhallBottomBar *self, const gchar *name);
};

MildenhallBottomBar *mildenhall_bottom_bar_new (void);

G_END_DECLS

#endif /* __BOTTOM_BAR_H__ */

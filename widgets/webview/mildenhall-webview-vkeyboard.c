/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-webview-vkeyboard.h"

#include <thornbury/thornbury.h>

SHM_VKEYBOARD_PLUGIN_DECLARE (MildenhallWebviewVKeyboard,
                              mildenhall_webview_vkeyboard)

#define MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_WEBVIEW_VKEYBOARD, MildenhallWebviewVKeyboardPrivate))

struct _MildenhallWebviewVKeyboardPrivate
{
  MildenhallSpeller *speller; /* owned : Mildenhall speller Object */
};

typedef struct
{
  ShmVKeyboardConfirmCallback callback_handler; /* callback function called on confirm/cancel the text */
  gpointer user_data; /* return data from confirm callback  */
} CloseVKeyboardData;

/* FIXME : Added below delay to render mildenhall speller gracefully
  * see : https://phabricator.apertis.org/T3744 */
static const guint vkeyboard_delay_show = 200;

static gboolean
mildenhall_webview_vkeyboard_hidden_cb (MildenhallSpeller *speller)
{
  g_assert (MILDENHALL_IS_SPELLER (speller));
  clutter_actor_hide (CLUTTER_ACTOR (speller));
  return FALSE;
}

static void
mildenhall_webview_vkeyboard_go_pressed_cb (MildenhallSpeller *speller,
                                            GVariant *variant,
                                            gpointer user_data)
{
  CloseVKeyboardData *method_data;

  g_assert (speller != NULL);
  g_assert (user_data != NULL);

  g_signal_handlers_disconnect_by_func (speller,
      G_CALLBACK (mildenhall_webview_vkeyboard_go_pressed_cb), user_data);
  method_data = (CloseVKeyboardData *) user_data;
  if (method_data->callback_handler != NULL)
    method_data->callback_handler (method_data->user_data);
  g_slice_free (CloseVKeyboardData, method_data);
}

static void
mildenhall_webview_vkeyboard_init (MildenhallWebviewVKeyboard *self)
{
  g_autoptr (ThornburyItemFactory) itemFactory = NULL;

  g_assert (MILDENHALL_IS_WEBVIEW_VKEYBOARD (self));

  self->priv = MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE (self);
  g_assert (self->priv != NULL);
  itemFactory = thornbury_item_factory_generate_widget_with_props (
       MILDENHALL_TYPE_SPELLER,
       PKGDATADIR"/mildenhall_webview_vkeyboard_prop.json");
  g_object_get (itemFactory, "object", &self->priv->speller, NULL);
  g_signal_connect_after (self->priv->speller, "speller-hidden",
                          G_CALLBACK (mildenhall_webview_vkeyboard_hidden_cb),
                          self);
}

static gboolean
mildenhall_webview_vkeyboard_render_delay_cb (ShmVKeyboard *shm_keyboard)
{
  MildenhallWebviewVKeyboardPrivate *priv;

  g_assert (MILDENHALL_IS_WEBVIEW_VKEYBOARD (shm_keyboard));

  priv = MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE (shm_keyboard);
  clutter_actor_show (CLUTTER_ACTOR (priv->speller));
  mildenhall_speller_show (CLUTTER_ACTOR (priv->speller), TRUE);
  return G_SOURCE_REMOVE;
}

static void
mildenhall_webview_vkeyboard_show (ShmVKeyboard *shm_keyboard,
                                   ClutterActor *actor,
                                   const gchar *default_text,
                                   ShmVKeyboardConfirmCallback callback_handler,
                                   gpointer user_data)
{
  MildenhallWebviewVKeyboardPrivate *priv;
  CloseVKeyboardData *method_data;
  guint speller_state;
  ClutterActor *stage;
  gfloat y_offset;
  /* FIXME : stage contains the status bar and default speller layout 
   * contains the "go" button, adding this delta value to adjust the speller
   * layout should display completely and go button overlap with 
   * the menu drawer.
   * see : https://phabricator.apertis.org/T3743 */
  const gfloat y_offset_delta = 20;

  g_assert (MILDENHALL_IS_WEBVIEW_VKEYBOARD (shm_keyboard));
  g_assert (CLUTTER_IS_ACTOR (actor));

  priv = MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE (shm_keyboard);
  g_object_get (priv->speller, "speller-state", &speller_state, NULL);
  if (speller_state == MILDENHALL_SPELLER_SHOWN)
     return;

  method_data = g_slice_new0 (CloseVKeyboardData);
  method_data->callback_handler = callback_handler;
  method_data->user_data = user_data;

  stage = clutter_actor_get_stage (actor);
  y_offset = clutter_actor_get_height (stage) - y_offset_delta;
  clutter_actor_add_child (stage, CLUTTER_ACTOR (priv->speller));
  clutter_actor_set_y (CLUTTER_ACTOR (priv->speller), y_offset);
  g_signal_connect_after (
      priv->speller, "go-pressed",
      G_CALLBACK (mildenhall_webview_vkeyboard_go_pressed_cb), method_data);
  /* FIXME : this delay added to render speller gracefully
   * see : https://phabricator.apertis.org/T3744 */
  g_timeout_add (vkeyboard_delay_show,
                 (GSourceFunc) mildenhall_webview_vkeyboard_render_delay_cb,
                 shm_keyboard);
}

static void
mildenhall_webview_vkeyboard_hide (ShmVKeyboard *shm_keyboard)
{
  MildenhallWebviewVKeyboardPrivate *priv;

  g_assert (MILDENHALL_IS_WEBVIEW_VKEYBOARD (shm_keyboard));

  priv = MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE (shm_keyboard);
  mildenhall_speller_hide (CLUTTER_ACTOR (priv->speller), TRUE);
}

static void
mildenhall_webview_vkeyboard_dispose (GObject *object)
{
  MildenhallWebviewVKeyboardPrivate *priv = MILDENHALL_WEBVIEW_VKEYBOARD_PRIVATE (object);

  G_OBJECT_CLASS (mildenhall_webview_vkeyboard_parent_class)->dispose (object);

  g_signal_handlers_disconnect_by_func (priv->speller,
      G_CALLBACK (mildenhall_webview_vkeyboard_hidden_cb), object);
  g_clear_object (&priv->speller);
}


static void
mildenhall_webview_vkeyboard_class_init (MildenhallWebviewVKeyboardClass *kclass)
{
  ShmVKeyboardClass *vkeyboard_class;
  GObjectClass *object_class;

  g_assert (kclass != NULL);

  object_class = G_OBJECT_CLASS (kclass);
  vkeyboard_class = SHM_VKEYBOARD_CLASS (kclass);
  g_type_class_add_private (kclass, sizeof (MildenhallWebviewVKeyboardPrivate));

  object_class->dispose = mildenhall_webview_vkeyboard_dispose;
  vkeyboard_class->show = mildenhall_webview_vkeyboard_show;
  vkeyboard_class->hide = mildenhall_webview_vkeyboard_hide;
}

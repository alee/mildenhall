/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-loading-bar.h */

#ifndef __MILDENHALL_LOADING_BAR_H__
#define __MILDENHALL_LOADING_BAR_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/* *********************************************************************
                      Include files
 *********************************************************************** */

#include <glib-object.h>
#include <clutter/clutter.h>
#include <string.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define MILDENHALL_TYPE_LOADING_BAR mildenhall_loading_bar_get_type()

#define MILDENHALL_LOADING_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_LOADING_BAR, MildenhallLoadingBar))

#define MILDENHALL_LOADING_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_LOADING_BAR, MildenhallLoadingBarClass))

#define MILDENHALL_IS_LOADING_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_LOADING_BAR))

#define MILDENHALL_IS_LOADING_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_LOADING_BAR))

#define MILDENHALL_LOADING_BAR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_LOADING_BAR, MildenhallLoadingBarClass))

typedef struct _MildenhallLoadingBar MildenhallLoadingBar;
typedef struct _MildenhallLoadingBarClass MildenhallLoadingBarClass;
typedef struct _MildenhallLoadingBarPrivate MildenhallLoadingBarPrivate;

struct _MildenhallLoadingBar
{
  ClutterActor parent;

  MildenhallLoadingBarPrivate *priv;
};

struct _MildenhallLoadingBarClass
{
  ClutterActorClass parent_class;
};

GType mildenhall_loading_bar_get_type (void) G_GNUC_CONST;

MildenhallLoadingBar *mildenhall_loading_bar_new (void);
void v_create_loading_bar(MildenhallLoadingBar *pLoadingBar);
G_END_DECLS

#endif /* __MILDENHALL_LOADING_BAR_H__ */
/* *********************************************************************
                        End of file
 *********************************************************************** */



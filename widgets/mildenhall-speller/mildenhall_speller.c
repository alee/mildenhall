/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_speller
 * @title: MildenhallSpeller
 * @short_description: It is a widget which allows  a user to enter the different set of characters.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #ThornburyModel
 *
 * A virtual keyboard. It works by using its own text entry widgets rather than
 * by sending key events immediately to the application.
 * There are signals to be notified when the user has pressed a key and when
 * text is committed.
 * It provides 'history-support' using which the speller can show auto complete text
 * when the user has entered any of the characters.
 * The signal "key-pad-pressed" shall be emitted to the applications whenever the user
 * has pressed any of the characters from the Keypad.
 * The Speller provided by Mildenhall supports several types of entries where text
 * can be typed. The entry type can be specified as a property of the Speller object.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 *  ThornburyItemFactory *item_factory = NULL;
 *  ThornburyModel *speller_model = NULL;
 *  GObject *object = NULL;
 *  MildenhallSpeller *self = NULL;
 *  ClutterActor *actor = NULL;
 *  GVariantBuilder *row_values = NULL, *row_values1 = NULL;
 *  GVariant *p = NULL, *q = NULL;
 *
 *  item_factory = thornbury_item_factory_generate_widget_with_props (
 *              MILDENHALL_TYPE_SPELLER,
 *              "/usr/share/mildenhall/mildenhall_speller_prop.json");
 *  g_object_get (item_factory, "object", &object, NULL);
 *  self = MILDENHALL_SPELLER (object);
 *  actor = CLUTTER_ACTOR (self);
 *  clutter_actor_add_child (stage, actor);
 *
 *  speller_model = thornbury_list_model_new (COLUMN_LAST, G_TYPE_BOOLEAN,
 *                                           0, G_TYPE_FLOAT, 0,
 *                                           G_TYPE_STRING, NULL,
 *                                           G_TYPE_POINTER, NULL,
 *                                           G_TYPE_BOOLEAN, 0,
 *                                           G_TYPE_FLOAT, 0,
 *                                           G_TYPE_STRING, NULL,
 *                                           G_TYPE_POINTER, NULL,
 *                                           G_TYPE_STRING, NULL,
 *                                           G_TYPE_STRING, NULL, -1);
 *
 *  row_values = g_variant_builder_new (G_VARIANT_TYPE_ARRAY);
 *  g_variant_builder_add (row_values, "{ss}", "SEARCH", "SEARCH");
 *  g_variant_builder_add (row_values, "{ss}", "URL", "URL");
 *
 *  row_values1 = g_variant_builder_new (G_VARIANT_TYPE_ARRAY);
 *  g_variant_builder_add (row_values1, "{ss}", "album",
 * 			   "/test-drawer-base/icon_music_artists_AC.png");
 *  g_variant_builder_add (row_values1, "{ss}", "artist",
 * 			   "/test-drawer-base/icon_internet_AC.png");
 *
 *  p = g_variant_builder_end (row_values);
 *  q = g_variant_builder_end (row_values1);
 *
 *  //append model info for default entry
 *  thornbury_model_append (speller_model, COLUMN_LEFT_IS_TEXT, TRUE,
 *                         COLUMN_LEFT_WIDTH, 158.0, COLUMN_LEFT_ID,
 *                         "BUTTON1", COLUMN_LEFT_INFO, (gpointer) p,
 *                         COLUMN_RIGHT_IS_TEXT, FALSE, COLUMN_RIGHT_WIDTH, 58.0,
 *                         COLUMN_RIGHT_ID, "BUTTON2",
 *                         COLUMN_RIGHT_INFO, (gpointer) q,
 *                         COLUMN_TEXT_BOX_DEFAULT_TEXT, "", COLUMN_ENTRY_ID,
 *                         "entry", -1);
 *
 *  g_object_set (actor, "model", speller_model, NULL);
 *  g_variant_unref (row_values);
 *  g_variant_unref (row_values1);
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include "mildenhall_speller.h"
#include "mildenhall_speller_key_pad.h"
#include "mildenhall_speller_urlhistory.h"

/* Speller Properties */
enum _MildenhallSpellerProperties
{
    MILDENHALL_SPELLER_PROP_ENUM_FIRST,
    MILDENHALL_SPELLER_PROP_ENUM_LAYOUT,
    MILDENHALL_SPELLER_PROP_ENUM_SHOWN,
    MILDENHALL_SPELLER_PROP_ENUM_HIDDEN,
    MILDENHALL_SPELLER_PROP_ENUM_ENTRY,
    MILDENHALL_SPELLER_PROP_ENUM_OFFSET,
    MILDENHALL_SPELLER_PROP_ENUM_MODEL,
    MILDENHALL_SPELLER_PROP_ENUM_SPELLER_STATE,
    MILDENHALL_SPELLER_PROP_ENUM_LAYOUT_STATE,
    MILDENHALL_SPELLER_PROP_ENUM_HISTORY_SUPPORT,
    MILDENHALL_SPELLER_PROP_ENUM_CURSOR_FOCUS,
    MILDENHALL_SPELLER_PROP_ENUM_SELECT_TYPE,
    MILDENHALL_SPELLER_PROP_ENUM_LAST
};

G_DEFINE_TYPE (MildenhallSpeller, mildenhall_speller, CLUTTER_TYPE_ACTOR)

#define SPELLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER, MildenhallSpellerPrivate))

#define MILDENHALL_SPELLER_ADJ 2

static GType mildenhall_speller_get_entry_type (void) G_GNUC_CONST;
#define MILDENHALL_SPELLER_GET_ENTRY_TYPE (mildenhall_speller_get_entry_type())

static void mildenhall_speller_attach_drag (MildenhallSpeller *pSelf , ClutterActor *pActor);
static void mildenhall_speller_attach_drag_to_openhead (MildenhallSpeller *pSelf , ClutterActor *pActor);

static gboolean b_keypad_repeat_key(gpointer pUserData);
/* speller signals */
enum MildenhallSpellerSignals
{
    SIG_MILDENHALL_SPELLER_FIRST_SIGNAL,
	SIG_MILDENHALL_SPELLER_GO_PRESSED, //used for go press
	SIG_MILDENHALL_SPELLER_KEY_EVENT,  //used for key press
	SIG_MILDENHALL_SPELLER_SHOWN,      //used for speller shown
	SIG_MILDENHALL_SPELLER_HIDDEN,     //used for speller hide
    SIG_MILDENHALL_SPELLER_ENTRY_STATUS,//used for entry action
	SIG_MILDENHALL_SPELLER_LAST_SIGNAL
};

/* private structure mem of speller */
struct _MildenhallSpellerPrivate
{
    gchar *layout; //for speller layout
    gboolean shown;      //var to show speller
    gboolean hidden;     //var to hide speller
    gboolean clearEntry; //var to clear speller entry
    gboolean longPress;
    gboolean historySupport;
    gboolean bSelectText;

    gfloat offset;       //to position the speller
    guint spellerState;

    gchar *headOpenPath;  //speller open header icon
    gchar *headClosePath; //speller closed header icon
    gchar *capsLockPath;  //speller capslock icon
    gchar *defaultLayout;  //layout
    gchar *layoutState;  //layout state
    gchar *repeatData;  //layout state
    gchar *pPressedKey;

    gint focus_entry;

    ThornburyModel *model;
    ClutterActor *capsLock;
    ClutterActor *headOpen;
    ClutterActor *headClose;

    ClutterActor *defaultEntry;
    ClutterActor *defaultPad; // speller keypad
    ClutterActor *spellerGroup;

    MildenhallSpellerState state;
    MildenhallSpellerEntryType entryType;
    //gint clicktime;
    gfloat gesture_start_pos;
    gfloat open_head_gesture_start_pos;
    //gboolean gestureEnabled;
    guint key_repeat_source;
    ClutterActor *topBar;
    gchar *topBarPath;
};

static guint32 uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_LAST_SIGNAL] = {0,};
static void v_mildenhall_speller_transition_callback (ClutterAnimation *pAnim, gpointer pUserData);

/********************************************************
 * Function : v_mildenhall_speller_shown_cb
 * Description: callback on hide
 * Parameters: ClutterAnimation*, ClutterActor*
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_shown_cb (ClutterAnimation *pAnim, gpointer pUserData)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER (pUserData);
    pSelf->priv->state = MILDENHALL_SPELLER_SHOWN;
    g_signal_emit(pSelf, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_SHOWN], 0, NULL);
    //pSelf->priv->gestureEnabled = FALSE;
    g_object_notify(G_OBJECT(pSelf),"speller-state");
}

/**
 * mildenhall_speller_show:
 * Returns: void
 *
 * api to show the speller
 *
 */
void mildenhall_speller_show (ClutterActor *pSpeller ,gboolean pAnimate)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER (pSpeller);
    gfloat fltSpellerHeight;
    g_return_if_fail(CLUTTER_IS_ACTOR(pSpeller));

    if (pSelf->priv->state != MILDENHALL_SPELLER_HIDDEN && (pSelf->priv->state != MILDENHALL_SPELLER_HIDE_REQUEST))
        return;

    clutter_actor_hide(pSelf->priv->headClose);
    clutter_actor_show(pSelf->priv->headOpen);

    if (pSelf->priv->entryType == MILDENHALL_SPELLER_MULTIPLE_ENTRY)
    {
	fltSpellerHeight = 89;
    }
    else if (pSelf->priv->entryType == MILDENHALL_SPELLER_MULTILINE_ENTRY)
    {
        fltSpellerHeight = (clutter_actor_get_y (pSpeller) - clutter_actor_get_height (pSpeller) + pSelf->priv->offset);
    }
    else
    {
	fltSpellerHeight = (clutter_actor_get_y (pSpeller) - clutter_actor_get_height (pSpeller) + pSelf->priv->offset);
	g_object_set (pSelf->priv->defaultEntry, "entry-editable", TRUE, NULL);
    }
    if(pAnimate)
    {
        pSelf->priv->state = MILDENHALL_SPELLER_SHOW_REQUEST;
        clutter_actor_save_easing_state(pSpeller);
        clutter_actor_set_easing_mode (pSpeller, CLUTTER_EASE_OUT_BOUNCE);
        clutter_actor_set_easing_duration (pSpeller,1000);
        clutter_actor_set_y (pSpeller, fltSpellerHeight );
        clutter_actor_restore_easing_state (pSpeller);
    }
    else
    {
        clutter_actor_set_y(pSpeller,fltSpellerHeight);
        pSelf->priv->state = MILDENHALL_SPELLER_SHOWN;
        g_signal_emit(pSelf, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_SHOWN], 0, NULL);
    }
    g_object_notify(G_OBJECT(pSelf),"speller-state");

   // g_object_get(clutter_settings_get_default(),"double-click-time",&pSelf->priv->clicktime,NULL);
    //g_object_set(clutter_settings_get_default(),"double-click-time",50,NULL);
}

/********************************************************
 * Function : v_mildenhall_speller_hide_cb
 * Description: callback on hide
 * Parameters: ClutterAnimation*, ClutterActor*
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_hide_cb (ClutterAnimation *pAnim, gpointer pUserData)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER (pUserData);
    pSelf->priv->state = MILDENHALL_SPELLER_HIDDEN;
    g_object_notify(G_OBJECT(pSelf),"speller-state");
    g_signal_emit(pSelf, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_HIDDEN], 0, NULL);

    if(pSelf->priv->key_repeat_source)
	{
	    g_source_remove(pSelf->priv->key_repeat_source);
	    pSelf->priv->key_repeat_source = 0;
	}

    if( 0.0 == pSelf->priv->offset)
    {
        clutter_actor_hide(pSelf->priv->headOpen);
        clutter_actor_show(pSelf->priv->headClose);
    }
}

/**
 * mildenhall_speller_hide:
 *
 * api to hide the speller
 *
 */
void mildenhall_speller_hide (ClutterActor *pSpeller , gboolean bAnimate)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER (pSpeller);
    g_return_if_fail(CLUTTER_IS_ACTOR(pSpeller));

    if (pSelf->priv->state != MILDENHALL_SPELLER_SHOWN && (pSelf->priv->state != MILDENHALL_SPELLER_SHOW_REQUEST))
        return;

    if(bAnimate)
    {
        pSelf->priv->state = MILDENHALL_SPELLER_HIDE_REQUEST;
        clutter_actor_save_easing_state(pSpeller);
        clutter_actor_set_easing_mode (pSpeller, CLUTTER_EASE_OUT_BOUNCE);
        clutter_actor_set_easing_duration (pSpeller, 1000);
        clutter_actor_set_y (pSpeller, clutter_actor_get_y(pSpeller) + clutter_actor_get_height(pSpeller) - pSelf->priv->offset );
        clutter_actor_restore_easing_state (pSpeller);
    }
    else
    {
        clutter_actor_set_y(pSpeller,clutter_actor_get_y(pSpeller) + clutter_actor_get_height(pSpeller) - pSelf->priv->offset);
        pSelf->priv->state = MILDENHALL_SPELLER_HIDDEN;
        g_signal_emit(pSelf, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_HIDDEN], 0, NULL);
    }
    if(NULL != pSelf->priv->defaultEntry)
    {
        g_object_set( pSelf->priv->defaultEntry ,"clear-text",TRUE, NULL);
        g_object_notify(G_OBJECT(pSelf),"speller-state");
        //g_object_set(clutter_settings_get_default(),"double-click-time",pSelf->priv->clicktime,NULL);
    }
    if(MILDENHALL_SPELLER_DEFAULT_ENTRY == pSelf->priv->entryType)
    {
    	g_object_set( pSelf->priv->defaultEntry ,"entry-editable",FALSE, NULL);
    }
    if(MILDENHALL_SPELLER_MULTIPLE_ENTRY == pSelf->priv->entryType)
    {
    	g_object_set( pSelf->priv->defaultEntry ,"cursor-focus",0, NULL);
    }
}

/********************************************************
 * Function : v_mildenhall_speller_transition_callback
 * Description: callback on hide
 * Parameters: ClutterAnimation*, ClutterActor*
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_transition_callback (ClutterAnimation *pAnim, gpointer pUserData)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER (pUserData);
    if(pSelf->priv->state == MILDENHALL_SPELLER_HIDDEN || pSelf->priv->state == MILDENHALL_SPELLER_HIDE_REQUEST)
    {
        v_mildenhall_speller_hide_cb(pAnim,pUserData);
    }
    else
    {
        v_mildenhall_speller_shown_cb(pAnim,pUserData);
    }
}


/********************************************************
 * Function : v_speller_row_added_cb
 * Description: callback on model row added
 * Parameters: ThornburyModel*, ThornburyModelIter*, MildenhallSpeller*
 * Return value: void
 ********************************************************/
static void v_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpeller *pSelf)
{
}

/********************************************************
 * Function : v_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpeller *
 * Return value: void
 ********************************************************/
static void v_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallSpeller *pSelf)
{
}

/********************************************************
 * Function : v_speller_row_removed_cb
 * Description: callback on model row remove
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpeller *
 * Return value: void
 ********************************************************/
static void v_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpeller *pSelf)
{

	MildenhallSpellerPrivate *priv = SPELLER_PRIVATE(pSelf);
	/* this callback gives the actual row count before removal, and not after removal of row */
	gint inRemovedRow = thornbury_model_iter_get_row (pIter);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && thornbury_model_get_n_rows(priv->model) == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
}

/********************************************************
 * Function : b_go_press_cb
 * Description: callback on go press
 * Parameters: ClutterActor,userdata
 * Return value: gboolean
 ********************************************************/
static gboolean b_go_press_cb(ClutterActor *actor, gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pUserData );
    GVariant* pVariant = NULL;
    if(NULL != priv->defaultEntry)
    {
        g_object_get(priv->defaultEntry, "text", &pVariant, NULL);
    }
    g_signal_emit(pUserData, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_GO_PRESSED], 0, pVariant);
    return TRUE;
}

/********************************************************
 * Function : v_speller_entry_action_cb
 * Description: callback on entry press
 * Parameters: GObject,buttonName,buttonId,userdata
 * Return value: void
 ********************************************************/
static void v_speller_entry_action_cb(GObject *pButton, gchar *pButtonName ,gchar *pButtonId,gpointer pUserData)
{
    g_signal_emit(pUserData, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_ENTRY_STATUS], 0, g_strdup(pButtonName),g_strdup(pButtonId));
}

/********************************************************
 * Function : v_multiple_speller_entry_action_cb
 * Description: callback on entry press
 * Parameters: GObject,buttonName,buttonId,userdata
 * Return value: void
 ********************************************************/
static void v_multiple_speller_entry_action_cb(GObject *pEntry,gchar *pEntryId,gpointer pUserData)
{
    g_signal_emit(pUserData, uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_ENTRY_STATUS], 0, g_strdup(pEntryId),g_strdup(pEntryId));
}

static void v_speller_slide_down_cb(GObject *pEntry,gpointer pUserData)
{
	mildenhall_speller_hide(CLUTTER_ACTOR(pUserData) , TRUE);
}

/********************************************************
 * Function : v_speller_update_default_entry_ui
 * Description: function to update the speller entry
 * Parameters: speller object reference
 * Return value: void
 ********************************************************/
static void v_speller_update_default_entry_ui(MildenhallSpeller *pSelf)
{
	MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pSelf );

    priv->defaultEntry = g_object_new(MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, NULL);
    g_object_set( priv->defaultEntry,"model", priv->model,NULL);
	g_object_set(priv->defaultEntry ,"enable-history",priv->historySupport, NULL);

    if(priv->defaultEntry)
    {
        clutter_actor_set_position(priv->defaultEntry, 2,clutter_actor_get_height(CLUTTER_ACTOR(priv->headOpen)));
        clutter_actor_add_child(priv->spellerGroup, priv->defaultEntry);
        clutter_actor_set_position(priv->topBar,2,clutter_actor_get_height(priv->spellerGroup));
        clutter_actor_add_child(priv->spellerGroup,priv->topBar);
    }
    if(NULL != priv->defaultPad)
    {
        if(!g_strcmp0(priv->defaultLayout,"ac_speller_style.json"))
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(priv->spellerGroup)));
            if(NULL != priv->defaultLayout)
            {
                g_free(priv->defaultLayout);
                priv->defaultLayout = NULL;
            }
        }
        else
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(priv->spellerGroup))-MILDENHALL_SPELLER_ADJ);
        }
        clutter_actor_add_child(priv->spellerGroup, priv->defaultPad);
    }
    g_object_set( priv->defaultEntry,"reactive", TRUE,NULL);
    g_signal_connect (priv->defaultEntry, "entry-action", G_CALLBACK (v_speller_entry_action_cb), pSelf );
    g_signal_connect (priv->defaultEntry, "slide-down", G_CALLBACK (v_speller_slide_down_cb), pSelf );

   if(TRUE == priv->shown)
    {
        g_object_set(priv->defaultEntry ,"entry-editable",TRUE, NULL);
    }
    else
    {
        g_object_set(priv->defaultEntry ,"entry-editable",FALSE, NULL);
    }
    g_signal_connect (priv->defaultEntry, "go-action", G_CALLBACK (b_go_press_cb), pSelf );
}

/********************************************************
 * Function : v_speller_update_four_entry_ui
 * Description: function to update the four toggle speller entry
 * Parameters: speller object reference
 * Return value: void
 ********************************************************/
static void v_speller_update_four_entry_ui(MildenhallSpeller *pSelf)
{
	MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pSelf );
    priv->defaultEntry = g_object_new(MILDENHALL_TYPE_SPELLER_FOUR_TOGGLE_ENTRY, NULL);
    g_object_set( priv->defaultEntry,"model", priv->model,NULL);

    if(priv->defaultEntry)
    {
        MILDENHALL_SPELLER_PRINT("speller group height = %f \n",clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup)));
        clutter_actor_set_position(priv->defaultEntry, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->headOpen)));
        clutter_actor_add_child(priv->spellerGroup, priv->defaultEntry);
        clutter_actor_set_position(pSelf->priv->topBar,2,clutter_actor_get_height(pSelf->priv->spellerGroup));
        clutter_actor_add_child(pSelf->priv->spellerGroup,pSelf->priv->topBar);
    }
    if(NULL != priv->defaultPad)
    {
        if(!g_strcmp0(priv->defaultLayout,"ac_speller_style.json"))
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup)));
            if(NULL != priv->defaultLayout)
            {
                g_free(priv->defaultLayout);
                priv->defaultLayout = NULL;
            }
        }
        else
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup))-MILDENHALL_SPELLER_ADJ);
        }
        clutter_actor_add_child(priv->spellerGroup, priv->defaultPad);
    }
    g_object_set( priv->defaultEntry,"reactive", TRUE,NULL);
    if(TRUE == pSelf->priv->shown)
    {
        g_object_set(pSelf->priv->defaultEntry ,"entry-editable",TRUE, NULL);
    }
    else
    {
        g_object_set(pSelf->priv->defaultEntry ,"entry-editable",FALSE, NULL);
    }
    g_signal_connect (priv->defaultEntry, "go-action", G_CALLBACK (b_go_press_cb), pSelf );
}

/********************************************************
 * Function : v_speller_update_four_entry_ui
 * Description: function to update the multiline speller entry
 * Parameters: speller object reference
 * Return value: void
 ********************************************************/
static void v_speller_update_multiline_entry_ui(MildenhallSpeller *pSelf)
{
	MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pSelf );
    priv->defaultEntry = g_object_new(MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, NULL);
    g_object_set( priv->defaultEntry,"model", priv->model,NULL);

    if(priv->defaultEntry)
    {
        clutter_actor_set_position(priv->defaultEntry, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->headOpen)));
        clutter_actor_add_child(priv->spellerGroup, priv->defaultEntry);
    }
    if(NULL != priv->defaultPad)
    {
        if(!g_strcmp0(priv->defaultLayout,"ac_speller_style.json"))
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup))-44);
            if(NULL != priv->defaultLayout)
            {
                g_free(priv->defaultLayout);
                priv->defaultLayout = NULL;
            }
        }
        else
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup))-MILDENHALL_SPELLER_ADJ);
        }
        clutter_actor_add_child(priv->spellerGroup, priv->defaultPad);
    }
}

/********************************************************
 * Function : v_speller_update_four_entry_ui
 * Description: function to update the multiple speller entry
 * Parameters: speller object reference
 * Return value: void
 ********************************************************/
static void v_speller_update_multiple_entry_ui(MildenhallSpeller *pSelf)
{
	MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pSelf );

    priv->defaultEntry = g_object_new(MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, NULL);
    g_object_set( priv->defaultEntry,"model", priv->model,NULL);

    if(priv->defaultEntry)
    {
        clutter_actor_set_position(priv->defaultEntry, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->headOpen)));
        clutter_actor_add_child(priv->spellerGroup, priv->defaultEntry);
        clutter_actor_set_position(pSelf->priv->topBar,2,122);
        clutter_actor_add_child(pSelf->priv->spellerGroup,pSelf->priv->topBar);
    }
    if(NULL != priv->defaultPad)
    {
        if(!g_strcmp0(priv->defaultLayout,"ac_speller_style.json"))
        {
            clutter_actor_set_position(priv->defaultPad, 2,132);
        }
        else
        {
            clutter_actor_set_position(priv->defaultPad, 2,clutter_actor_get_height(CLUTTER_ACTOR(pSelf->priv->spellerGroup))-MILDENHALL_SPELLER_ADJ);
        }
        clutter_actor_add_child(priv->spellerGroup, priv->defaultPad);
        clutter_actor_set_child_above_sibling(priv->spellerGroup,priv->defaultPad, NULL);
    }
    g_signal_connect (priv->defaultEntry, "multiple-entry-action", G_CALLBACK (v_multiple_speller_entry_action_cb), pSelf );
}

/****************************************************
 * Function : v_speller_update_view
 * Description: update the speller entry
 * Parameters: MildenhallSpeller*
 * Return value: None
 *******************************************************/
static void v_speller_update_view( MildenhallSpeller *pSelf )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE( pSelf );
    switch(priv->entryType)
    {
        case MILDENHALL_SPELLER_NO_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT( "\n MILDENHALL_SPELLER_NO_ENTRY \n" );
            break;
        }
        case MILDENHALL_SPELLER_DEFAULT_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT( "\n entered into MILDENHALL_SPELLER_DEFAULT_ENTRY \n" );
        	v_speller_update_default_entry_ui(pSelf);
            break;
        }
        case MILDENHALL_SPELLER_THREE_TOGGLE_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT(" \n MILDENHALL_SPELLER_THREE_TOGGLE_ENTRY \n");
            break;
        }
        case MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT( "\n entered into MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY \n" );
        	v_speller_update_four_entry_ui(pSelf);
             break;
         }
        case MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT(" \n MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY \n");
            break;
        }
        case MILDENHALL_SPELLER_MULTILINE_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT( "\n entered into MILDENHALL_SPELLER_MULTILINE_ENTRY \n" );
            v_speller_update_multiline_entry_ui(pSelf);
            break;
        }
        case MILDENHALL_SPELLER_MULTIPLE_ENTRY:
        {
            MILDENHALL_SPELLER_PRINT(" \n MILDENHALL_SPELLER_MULTIPLE_ENTRY \n");
            v_speller_update_multiple_entry_ui(pSelf);
            break;
        }
	case MILDENHALL_SPELLER_TYPE_NONE:
	{
	    break;
	}
        default:
        {
            MILDENHALL_SPELLER_PRINT(" \n MILDENHALL_SPELLER_DEFAULT \n");
            break;
        }
    }
    mildenhall_speller_attach_drag(pSelf,priv->spellerGroup);

    if(pSelf->priv->offset > 0)
    {
    	mildenhall_speller_attach_drag_to_openhead(pSelf,pSelf->priv->headOpen);
    }
    clutter_actor_set_child_above_sibling(priv->spellerGroup, priv->headClose, NULL);
}

/****************************************************
 * Function : b_on_gesture_begin
 * Description: function on gesture begin
 * Parameters: ClutterGestureAction*,ClutterActor,userdata
 * Return value: gboolean
 *******************************************************/
static gboolean
b_on_gesture_begin (ClutterGestureAction *action, ClutterActor *actor, gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = MILDENHALL_SPELLER(pUserData)->priv;
    gfloat x,y;
    clutter_gesture_action_get_press_coords(action,0,&x,&y);
    priv->gesture_start_pos = y;
    return TRUE;
}

/****************************************************
 * Function : b_on_gesture_progress
 * Description: function on gesture progress
 * Parameters: ClutterGestureAction*,ClutterActor,userdata
 * Return value: gboolean
 *******************************************************/
static gboolean
b_on_gesture_progress (ClutterGestureAction *action, ClutterActor *actor, gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = MILDENHALL_SPELLER(pUserData)->priv;

    gfloat x,y;
    clutter_gesture_action_get_motion_coords(action,0,&x,&y);
    MILDENHALL_SPELLER_PRINT("Action Motion coords = %f \n",(y-priv->gesture_start_pos));
    if(((y - priv->gesture_start_pos) > 70.0) && (priv->state == MILDENHALL_SPELLER_SHOWN))
    {
        mildenhall_speller_hide(CLUTTER_ACTOR(pUserData) , TRUE);
    }
    else if(((priv->gesture_start_pos - y) > 70.0) && (priv->state == MILDENHALL_SPELLER_HIDDEN))
    {
        mildenhall_speller_show(CLUTTER_ACTOR(pUserData) , TRUE);
    }
    else
    {
        return TRUE;
    }
    //priv->gestureEnabled = TRUE;
    return FALSE;
}


static void
b_on_gesture_cancel (ClutterGestureAction *action, ClutterActor *actor, gpointer pUserData)
{
	MildenhallSpellerPrivate *priv = MILDENHALL_SPELLER(pUserData)->priv;

	gfloat x,y;
	clutter_gesture_action_get_motion_coords(action,0,&x,&y);
	MILDENHALL_SPELLER_PRINT("Action Motion coords = %f \n",(y-priv->gesture_start_pos));
    if(((y - priv->gesture_start_pos) > 70.0) && (priv->state == MILDENHALL_SPELLER_SHOWN))
    {
    	MILDENHALL_SPELLER_PRINT("speller hide");
    }
    else if(((priv->gesture_start_pos - y) > 70.0) && (priv->state == MILDENHALL_SPELLER_HIDDEN))
    {
    	MILDENHALL_SPELLER_PRINT("speller show");
    }
    else
    {
    	MILDENHALL_SPELLER_PRINT("nothg happened");
    }
    //priv->gestureEnabled = TRUE;
}


/****************************************************
 * Function : mildenhall_speller_attach_drag
 * Description: function to attach drag functionality
 * Parameters: speller object ref , ClutterActor
 * Return value: gboolean
 *******************************************************/
static void mildenhall_speller_attach_drag (MildenhallSpeller *pSelf , ClutterActor *pActor)
{
    ClutterAction *gesture_action = clutter_gesture_action_new ();
    clutter_actor_add_action (pActor, gesture_action);
    g_signal_connect (gesture_action, "gesture-begin", G_CALLBACK (b_on_gesture_begin), pSelf);
    g_signal_connect (gesture_action, "gesture-progress", G_CALLBACK (b_on_gesture_progress), pSelf);
    g_signal_connect (gesture_action, "gesture-cancel", G_CALLBACK (b_on_gesture_cancel), pSelf);
    clutter_actor_set_reactive (pActor, TRUE);
}

/****************************************************
 * Function : b_on_gesture_begin
 * Description: function on gesture begin
 * Parameters: ClutterGestureAction*,ClutterActor,userdata
 * Return value: gboolean
 *******************************************************/
static gboolean
b_on_openhead_gesture_begin (ClutterGestureAction *action, ClutterActor *actor, gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = MILDENHALL_SPELLER(pUserData)->priv;
    gfloat x,y;
    clutter_gesture_action_get_press_coords(action,0,&x,&y);
    priv->open_head_gesture_start_pos = y;
    return TRUE;
}

/****************************************************
 * Function : b_on_openhead_gesture_progress
 * Description: function on gesture progress
 * Parameters: ClutterGestureAction*,ClutterActor,userdata
 * Return value: gboolean
 *******************************************************/
static gboolean
b_on_openhead_gesture_progress (ClutterGestureAction *action, ClutterActor *actor, gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = MILDENHALL_SPELLER(pUserData)->priv;

    gfloat x,y;
    clutter_gesture_action_get_motion_coords(action,0,&x,&y);

    if(((priv->open_head_gesture_start_pos - y) > 20.0) && (priv->state == MILDENHALL_SPELLER_HIDDEN))
    {
        mildenhall_speller_show(CLUTTER_ACTOR(pUserData) , TRUE);
    }
    else
    {
        return TRUE;
    }
    return FALSE;
}

/****************************************************
 * Function : mildenhall_speller_attach_drag_to_openhead
 * Description: function to attach drag
 * Parameters: speller object ref,ClutterActor
 * Return value: gboolean
 *******************************************************/
static void mildenhall_speller_attach_drag_to_openhead (MildenhallSpeller *pSelf , ClutterActor *pActor)
{
    ClutterAction *gesture_action = clutter_gesture_action_new ();
    clutter_actor_add_action (pActor, gesture_action);
    g_signal_connect (gesture_action, "gesture-begin", G_CALLBACK (b_on_openhead_gesture_begin), pSelf);
    g_signal_connect (gesture_action, "gesture-progress", G_CALLBACK (b_on_openhead_gesture_progress), pSelf);
    clutter_actor_set_reactive (pActor, TRUE);
}

/****************************************************
 * Function : v_speller_set_model
 * Description: function to set model to speller
 * Parameters: speller object ref,ThornburyModel
 * Return value: void
 *******************************************************/
static void v_speller_set_model(MildenhallSpeller *pSelf, ThornburyModel *pModel)
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (pSelf);
    if(!MILDENHALL_IS_SPELLER(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

    if(NULL != priv->model)
    {
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_speller_row_added_cb),   pSelf);
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_speller_row_changed_cb), pSelf);
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_speller_row_removed_cb), pSelf);
        g_object_unref (priv->model);
        priv->model = NULL;
    }
    /* update the new model with signals */
	if ( NULL != pModel)
    {
        g_return_if_fail (G_IS_OBJECT (pModel));
        priv->model = g_object_ref(pModel);
        g_signal_connect (priv->model, "row-added",   G_CALLBACK (v_speller_row_added_cb),   pSelf );
        g_signal_connect (priv->model, "row-changed", G_CALLBACK (v_speller_row_changed_cb), pSelf );
        g_signal_connect (priv->model, "row-removed", G_CALLBACK (v_speller_row_removed_cb), pSelf );
    }
    if(NULL != priv->model)
    {
        v_speller_update_view(pSelf);
    }
	g_object_notify (G_OBJECT(pSelf), "model");
}

/********************************************************
 * Function : v_key_press_cb
 * Description: callabck on button pressed
 * Parameters: GObject*, pKey , pSelf
 * Return value: void
 ********************************************************/
static gboolean b_key_press_cb ( GObject *pObject, gchar *pKey , gpointer pSelf )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE(pSelf);
	if(priv->key_repeat_source)
	{
	    g_source_remove(priv->key_repeat_source);
	    priv->key_repeat_source = 0;
	}

	return FALSE;
}

/****************************************************
 * Function : p_mildenhall_speller_create_keyevent
 * Description: function to create clutterEvent from key press
 * Parameters: key_name,ClutterEventType,speller object ref
 * Return value: ClutterEvent
 *******************************************************/
static ClutterEvent* p_mildenhall_speller_create_keyevent(const gchar* key_name , ClutterEventType type,MildenhallSpeller *pSelf)
{
    ClutterEvent *keyevent;
    if( type == CLUTTER_BUTTON_PRESS)
    {
        keyevent = clutter_event_new (CLUTTER_KEY_PRESS);
        keyevent->type      = CLUTTER_KEY_PRESS;
    }
    else
    {
        keyevent = clutter_event_new (CLUTTER_KEY_RELEASE);
        keyevent->type      = CLUTTER_KEY_RELEASE;
    }

    keyevent->key.time  = CLUTTER_CURRENT_TIME;
    keyevent->key.flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
    keyevent->key.stage = CLUTTER_STAGE (clutter_actor_get_stage (CLUTTER_ACTOR(pSelf)));
    keyevent->key.modifier_state = 0;
    keyevent->key.keyval =  (gint)g_utf8_get_char (key_name);
    keyevent->key.unicode_value = (gint)g_utf8_get_char (key_name);

    if(!g_strcmp0(key_name,"SYMBOLS"))
    {
        keyevent->key.keyval = CLUTTER_KEY_leftradical;
        keyevent->key.unicode_value =  0;
    }
    else if(!g_strcmp0(key_name,"SHIFT") )
    {
        keyevent->key.keyval = CLUTTER_KEY_Shift_L;
        keyevent->key.unicode_value =  0;
    }
    else if(!g_strcmp0(key_name,"RETURN") || !g_strcmp0(key_name,"go"))
    {
        keyevent->key.keyval = CLUTTER_KEY_Return;
        keyevent->key.unicode_value = CLUTTER_KEY_Return;
        /* hardware keycode of RETURN key */
        keyevent->key.hardware_keycode = 36;
    }
    else if(!g_strcmp0(key_name,"BKSP") || !g_strcmp0(key_name,"←") )
    {
        keyevent->key.keyval = CLUTTER_KEY_BackSpace;
        keyevent->key.unicode_value = CLUTTER_KEY_BackSpace;
        /* hardware keycode of BKSP key */
        keyevent->key.hardware_keycode = 22;
    }
    else if(!g_strcmp0(key_name,"SPACE") )
    {
        keyevent->key.keyval = CLUTTER_KEY_space;
        keyevent->key.unicode_value = CLUTTER_KEY_space;
    }
    else if(!g_strcmp0(key_name,"AUTOCOMPLETE") )
    {
        keyevent->key.keyval = CLUTTER_KEY_End;
        keyevent->key.unicode_value = CLUTTER_KEY_space;
    }
    return keyevent;
}

/********************************************************
 * Function : v_key_release_cb
 * Description: callabck on button release
 * Parameters: GObject*, pKey , pSelf
 * Return value: void
 ********************************************************/
static gboolean b_key_release_cb( GObject *pObject, gchar *pKey , gpointer pSelf )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE(pSelf);

	if(priv->key_repeat_source)
	{
	    g_source_remove(priv->key_repeat_source);
	    priv->key_repeat_source = 0;
	}

    /*if(TRUE == priv->gestureEnabled)
    {
        priv->gestureEnabled = FALSE;
        return FALSE;
    }*/

    if (priv->entryType != MILDENHALL_SPELLER_NO_ENTRY)
    {
	GVariantBuilder *gvb;
	GVariant *arg_list;
	if ((g_strcmp0 (pKey, "BKSP") == 0) || (g_strcmp0 (pKey, "←") == 0))
	{
	    clutter_event_put (p_mildenhall_speller_create_keyevent (g_strdup (pKey), CLUTTER_BUTTON_PRESS, pSelf));
	}
	gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
	g_variant_builder_add (gvb, "{ss}", "KEY", g_strdup (pKey));
	arg_list = g_variant_builder_end (gvb);
	g_object_set (priv->defaultEntry, "text", arg_list, NULL);
	g_variant_builder_unref (gvb);
    }
    if(!g_strcmp0(pKey,"SHIFT"))
    {
        gboolean isEnabled;
        g_object_get(priv->defaultPad,"long-press-status",&isEnabled,NULL);

        if(FALSE == isEnabled)
            clutter_actor_hide(priv->capsLock);
    }
    if(!g_strcmp0(pKey,"SYMBOLS"))
    {
        clutter_actor_hide(priv->capsLock);
    }
    if(NULL == priv->defaultEntry)
    {
        clutter_event_put(p_mildenhall_speller_create_keyevent(g_strdup(pKey),CLUTTER_BUTTON_PRESS,pSelf));
    }
    return FALSE;
}

/****************************************************
 * Function : b_keypad_repeat_key
 * Description: function to repeate the key
 * Parameters: userdata
 * Return value: gboolean
 *******************************************************/
static gboolean b_keypad_repeat_key(gpointer pUserData)
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (pUserData);
    GVariantBuilder *gvb;
    GVariant *arg_list;
    if(!MILDENHALL_IS_SPELLER(pUserData))
	{
		g_warning("invalid instance\n");
		return FALSE;
	}

    if(NULL == priv->defaultEntry)
    {
        clutter_event_put(p_mildenhall_speller_create_keyevent(g_strdup(priv->repeatData),CLUTTER_BUTTON_PRESS,pUserData));
        return TRUE;
    }
    gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
    g_variant_builder_add (gvb, "{ss}", "KEY", g_strdup(priv->repeatData) );
    arg_list = g_variant_builder_end (gvb);
    g_object_set(priv->defaultEntry,"text",arg_list,NULL);
    g_variant_builder_unref(gvb);
    return TRUE;
}

/********************************************************
 * Function : v_key_clicked_cb
 * Description: callback on button clicked
 * Parameters: GObject*, pKey , pSelf
 * Return value: void
 ********************************************************/
static void v_key_clicked_cb( GObject *pObject, gchar *pKey , gpointer pSelf )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE(pSelf);

    if(!g_strcmp0(pKey,"SHIFT"))
    {
        clutter_actor_show(priv->capsLock);
    }
    else if(!g_strcmp0(pKey,"BKSP") || !g_strcmp0(pKey,"←") )
    {
    	//TBD remove the char from entry
    }
    else
    {
      priv->repeatData = g_strdup(pKey);
      priv->key_repeat_source = g_timeout_add(60,b_keypad_repeat_key,pSelf);
    }
}
/********************************************************
 * Function : v_key_leave_cb
 * Description: callback on key leave
 * Parameters: GObject*, pKey , pSelf
 * Return value: void
 ********************************************************/
static void v_key_leave_cb( GObject *pObject, gchar *pKey , gpointer pSelf )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE(pSelf);
    if(priv->key_repeat_source)
	{
	    g_source_remove(priv->key_repeat_source);
	    priv->key_repeat_source = 0;
	}
}

/********************************************************
 * Function : v_swipe_cb
 * Parameters: ClutterSwipeAction ,object reference,
               swipe direction ,userdata
 * Return value: void
 ********************************************************/
static void v_swipe_cb(ClutterSwipeAction *pAction, ClutterActor *pActor, ClutterSwipeDirection direction, gpointer pUserData)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pUserData);
    if(pActor == pSelf->priv->headOpen )
    {
        if ((direction == CLUTTER_SWIPE_DIRECTION_DOWN) || (direction == 6) || (direction == 10))
        {
            MILDENHALL_SPELLER_PRINT(" CLUTTER_SWIPE_DIRECTION_DOWN \n");
            mildenhall_speller_hide(CLUTTER_ACTOR(pSelf), TRUE);

        }
    }
    else if(pActor == pSelf->priv->headClose)
    {
        if ((direction == CLUTTER_SWIPE_DIRECTION_UP) || (direction == 5) || (direction == 9))
        {
            MILDENHALL_SPELLER_PRINT(" CLUTTER_SWIPE_DIRECTION_UP \n");
            mildenhall_speller_show(CLUTTER_ACTOR(pSelf), TRUE);
        }
    }
    else
    {
        ;
    }
}

/********************************************************
 * Function : mildenhall_speller_get_entry_type
 * Description: define entry type enum as GType
 * Parameters: void
 * Return value: void
 ********************************************************/
static GType mildenhall_speller_get_entry_type (void)
{
    /* argument must point to a static 0-initialized variable,
     * that will be set to a value other than 0 at the end of
     * the initialization section.
     */
    static volatile gsize gEnumTypeVolatile = 0;

    /* swipe direction as enum initialization section */
    if (g_once_init_enter (&gEnumTypeVolatile))
    {
        /* A structure which contains a single flags value, its name, and its nickname. */
        static const GEnumValue arValues[] =
        {
            { MILDENHALL_SPELLER_NO_ENTRY,           "NO_ENTRY", "no_entry" },
            { MILDENHALL_SPELLER_DEFAULT_ENTRY,      "DEFAULT_ENTRY", "default" },
            { MILDENHALL_SPELLER_THREE_TOGGLE_ENTRY, "THREE_TOGGLE_ENTRY", "three_toggle" },
            { MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY,  "FOUR_TOGGLE_ENTRY", "four_toggle" },
            { MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY,  "FIVE_TOGGLE_ENTRY", "five_toggle" },
            { MILDENHALL_SPELLER_MULTILINE_ENTRY,    "MULTILINE_ENTRY", "multi_line" },
            { MILDENHALL_SPELLER_MULTIPLE_ENTRY,     "MULTIPLE_ENTRY", "multiple" },
            { 0, NULL, NULL }
        };
        GType gEnumType;
        /* Registers a new static flags type with the name name. */
        gEnumType = g_enum_register_static (g_intern_static_string ("MildenhallSpellerEntryType"), arValues);
        /* In combination with g_once_init_leave() and the unique address value_location,
         * it can be ensured that an initialization section will be executed only once
         * during a program's life time, and that concurrent threads are blocked
         * until initialization completed.
         */
        g_once_init_leave (&gEnumTypeVolatile, gEnumType);
    }
    return gEnumTypeVolatile;
}

/********************************************************
 * Function : v_speller_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_speller_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
    MildenhallSpeller *pSpeller = pUserData;
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (pSpeller);

    if(! MILDENHALL_IS_SPELLER(pUserData) )
        return;

    if(NULL != pKey)
    {
        gchar *pStyleKey = g_strdup(pKey);
        gchar *pPath = (gchar*)g_value_get_string(pValue);
        if(NULL != pPath)
        {
            if( 0 == g_strcmp0(pStyleKey, "head-open") )
            {
                priv->headOpenPath = g_strdup_printf( PKGTHEMEDIR"/%s",pPath);
            }
            else if( 0 == g_strcmp0(pStyleKey, "head-close") )
            {
                priv->headClosePath = g_strdup_printf( PKGTHEMEDIR"/%s",pPath);
            }
            else if( 0 == g_strcmp0(pStyleKey, "caps-lock") )
            {
                priv->capsLockPath = g_strdup_printf( PKGTHEMEDIR"/%s",pPath);
            }
            else if( 0 == g_strcmp0(pStyleKey, "top-bar") )
            {
                priv->topBarPath = g_strdup_printf( PKGTHEMEDIR"/%s",pPath);
            }
             else
            {
                ;
            }
            if(NULL != pStyleKey)
            {
                g_free(pStyleKey);
                pStyleKey = NULL;
            }
        }
    }
}

/**
 * mildenhall_speller_set_layout:
 * @self: a #MildenhallSpeller
 * @layout: the new value
 *
 * Update #MildenhallSpeller:layout property
 *
 */
void
mildenhall_speller_set_layout (MildenhallSpeller *self, const gchar *layout)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));
  g_return_if_fail (layout != NULL);

  if (!g_strcmp0 (priv->defaultLayout, layout))
    return;

  g_free (priv->layout);
  priv->layout = g_strconcat (PKGDATADIR, "/", layout, NULL);

  g_clear_object (&priv->defaultPad);
  priv->defaultPad = g_object_new (MILDENHALL_TYPE_SPELLER_KEY_PAD,
                                   "layout-state", priv->layoutState,
                                   "long-press-status", FALSE,
                                   "layout", priv->layout,
                                   "reactive", TRUE,
                                   NULL);

  if (!g_strcmp0 ("ac_speller_style.json", layout))
    {
      g_clear_object (&priv->capsLock);
      priv->capsLock = thornbury_ui_texture_create_new (priv->capsLockPath, 0, 0, FALSE, FALSE);
      clutter_actor_set_position (priv->capsLock, 5, 123);
      clutter_actor_add_child (priv->defaultPad, priv->capsLock);
      clutter_actor_hide (priv->capsLock);
    }
  /* FIXME: What if a different layout is given? */ 

  mildenhall_speller_attach_drag (self, priv->defaultPad);
  g_signal_connect (priv->defaultPad, "key-pressed", 
                    G_CALLBACK (b_key_press_cb),
                    self);
  g_signal_connect (priv->defaultPad, "key-released",
                    G_CALLBACK (b_key_release_cb),
                    self);
  g_signal_connect (priv->defaultPad, "key-clicked",
                    G_CALLBACK (v_key_clicked_cb),
                    self);
  g_signal_connect (priv->defaultPad, "key-leave",
                    G_CALLBACK (v_key_leave_cb),
                    self);
  g_signal_connect (priv->defaultPad, "go-press",
                    G_CALLBACK (b_go_press_cb),
                    self);

  g_free (priv->defaultLayout);
  priv->defaultLayout = g_strdup (layout);

  g_object_notify (G_OBJECT (self), "layout");
}

/**
 * mildenhall_speller_set_layout_state:
 * @self: a #MildenhallSpeller
 * @layout_state: the new value
 *
 * Update #MildenhallSpeller:layout-state property
 *
 */
void
mildenhall_speller_set_layout_state (MildenhallSpeller *self, const gchar *layout_state)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));
  g_return_if_fail (layout_state != NULL);

  if (!g_strcmp0 (priv->layoutState, layout_state))
    return;

  if (priv->defaultPad != NULL)
    g_object_set (priv->defaultPad, "layout-state", layout_state, NULL);
  
	g_free (priv->layoutState);
  priv->layoutState = g_strdup (layout_state);

  g_object_notify (G_OBJECT (self), "layout-state");
}

/**
 * mildenhall_speller_set_entry:
 * @self: a #MildenhallSpeller
 * @entry_type: the new value
 *
 * Update #MildenhallSpeller:entry property
 *
 */
void
mildenhall_speller_set_entry (MildenhallSpeller *self, MildenhallSpellerEntryType entry_type)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if (priv->entryType == entry_type)
    return;

  if (entry_type == MILDENHALL_SPELLER_NO_ENTRY)
    {
      if  (priv->defaultPad != NULL)
        {
          if (!g_strcmp0(priv->defaultLayout,"ac_speller_style.json"))
            {
              clutter_actor_set_position (priv->defaultPad, 2,
                clutter_actor_get_height (CLUTTER_ACTOR (priv->headOpen)) + MILDENHALL_SPELLER_ADJ);

              g_clear_pointer (&priv->defaultLayout, g_free);
            }
          else
            {
              clutter_actor_set_position (priv->defaultPad, 2,
                clutter_actor_get_height (CLUTTER_ACTOR (priv->headOpen)) - MILDENHALL_SPELLER_ADJ);
            }

          clutter_actor_set_position (priv->topBar, 2, clutter_actor_get_height (priv->spellerGroup));
          clutter_actor_add_child (priv->spellerGroup, priv->topBar);
          clutter_actor_add_child (priv->spellerGroup, priv->defaultPad);
        }
      clutter_actor_set_child_above_sibling (priv->spellerGroup, priv->headClose, NULL);
    }
  /* FIXME: What should we do for the other ENTRYs? */

  priv->entryType = entry_type;
  g_object_notify (G_OBJECT (self), "entry");
}

/**
 * mildenhall_speller_clear_text:
 * @self: a #MildenhallSpeller
 *
 * Clear text from entry
 *
 */
void
mildenhall_speller_clear_text (MildenhallSpeller *self)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  if (priv->defaultEntry != NULL)
    g_object_set (priv->defaultEntry, "clear-text", TRUE, NULL);
}

/**
 * mildenhall_speller_set_history_support:
 * @self: a #MidenhallSpeller
 * @support: the new value
 *
 * Update #MildenhallSpeller:history-support property
 */
void
mildenhall_speller_set_history_support (MildenhallSpeller *self, gboolean support)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if (priv->historySupport == support)
    return;

  if (priv->defaultEntry != NULL
   && priv->entryType == MILDENHALL_SPELLER_DEFAULT_ENTRY)
    {
      g_object_set (priv->defaultEntry, "enable-history", support, NULL);
    }

  priv->historySupport = support;
  g_object_notify (G_OBJECT (self), "history-support");
}

/**
 * mildenhall_speller_set_shown:
 * @self: a #MildenhallSpeller
 * @show: the new value
 *
 * Update #MildenhallSpeller:show property
 *
 */
void
mildenhall_speller_set_shown (MildenhallSpeller *self, gboolean show)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if (priv->shown == show)
    return;

  if (show)
    mildenhall_speller_show (CLUTTER_ACTOR (self), show);
  /* FIXME: Don't we need to call 'hide' when show is FALSE? */

  priv->shown = show;
  g_object_notify (G_OBJECT (self), "shown");
}

/**
 * mildenhall_speller_set_hidden:
 * @self: a #MildenhallSpeller
 * @hide: the new value
 *
 * Update #MildenhallSpeller:hidden property
 *
 */
void
mildenhall_speller_set_hidden (MildenhallSpeller *self, gboolean hide)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if (priv->hidden == hide)
    return;

  if (hide)
    {
      mildenhall_speller_hide (CLUTTER_ACTOR (self), FALSE);
      if (priv->offset == 0.0)
        {
          clutter_actor_hide (priv->headOpen);
          clutter_actor_show (priv->headClose);
        }
    }
  /* FIXME: Don't we need to call 'show' when hide is FALSE? */

  priv->hidden = hide;
  g_object_notify (G_OBJECT (self), "hidden");
}

/**
 * mildenhall_speller_set_yoffset:
 * @self: a #MildenhallSpeller
 * @yoffset: the new value
 *
 * Update #MildenhallSpeller:y-offset property
 */
void
mildenhall_speller_set_yoffset (MildenhallSpeller *self, gfloat yoffset)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if (priv->offset == yoffset)
    return;

  if (priv->offset > 0)
    clutter_actor_hide (priv->headClose);

  priv->offset = yoffset;
  g_object_notify (G_OBJECT (self), "y-offset");
}

/**
 * mildenhall_speller_set_state:
 * @self: a #MildenhallSpeller
 * @state: set speller state
 *
 * Update #MildenhallSpeller:speller-state property
 */
void
mildenhall_speller_set_state (MildenhallSpeller *self, gint state)
{
  MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_SPELLER (self));

  if ((gint) priv->spellerState == state)
    return;

  priv->spellerState = state;
  g_object_notify (G_OBJECT (self), "speller-state");
}

/**
 * v_speller_set_cursor_to_focus:
 * @inSpellerFocusEntry : set speller entry to focus
 *
 * set speller entry to focus
 *
 */
void v_speller_set_cursor_to_focus( MildenhallSpeller *pSelf,gint inSpellerFocusEntry )
{
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (pSelf);
    if(!MILDENHALL_IS_SPELLER(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->focus_entry = inSpellerFocusEntry;
	if(MILDENHALL_SPELLER_MULTIPLE_ENTRY == pSelf->priv->entryType)
	{
		g_object_set( pSelf->priv->defaultEntry ,"cursor-focus",inSpellerFocusEntry, NULL);
	}
}

static void v_speller_set_select_text( MildenhallSpeller *pSelf,gboolean bSelectText )
{
    if(!MILDENHALL_IS_SPELLER(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	g_object_set( pSelf->priv->defaultEntry ,"mark-text",bSelectText, NULL);
}

/********************************************************
 * Function : v_mildenhall_speller_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_set_property (GObject *pObject,guint uinPropertyId, const GValue *pValue, GParamSpec *pPspec)
{
   MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pObject);
   switch ( uinPropertyId )
   {
        case MILDENHALL_SPELLER_PROP_ENUM_LAYOUT :
        {
            mildenhall_speller_set_layout (pSelf, g_value_get_string (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_LAYOUT_STATE :
        {
            mildenhall_speller_set_layout_state (pSelf, g_value_get_string (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_SHOWN:
        {
            mildenhall_speller_set_shown (pSelf, g_value_get_boolean (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_HIDDEN:
        {
            mildenhall_speller_set_hidden (pSelf, g_value_get_boolean (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_HISTORY_SUPPORT:
        {
              mildenhall_speller_set_history_support (pSelf, g_value_get_boolean (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_OFFSET:
        {
            mildenhall_speller_set_yoffset (pSelf, g_value_get_float (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_ENTRY:
        {
            mildenhall_speller_set_entry (pSelf, g_value_get_enum (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_MODEL:
        {
            v_speller_set_model (pSelf, g_value_get_object ( pValue ));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_CURSOR_FOCUS:
        {
            v_speller_set_cursor_to_focus (pSelf, g_value_get_int ( pValue ));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_SPELLER_STATE:
        {
            mildenhall_speller_set_state (pSelf, g_value_get_int (pValue));
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_SELECT_TYPE:
        {
        	v_speller_set_select_text(pSelf, g_value_get_boolean ( pValue ));
        	break;
        }
        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
            break;
        }
   }
}

/********************************************************
 * Function : v_mildenhall_speller_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_get_property ( GObject *pObject, guint uinPropertyId, GValue *pValue, GParamSpec *pPspec )
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pObject);
    switch ( uinPropertyId )
    {
        case MILDENHALL_SPELLER_PROP_ENUM_LAYOUT :
        {
            g_value_take_string (pValue, pSelf->priv->layout);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_LAYOUT_STATE :
        {
            g_value_take_string (pValue, pSelf->priv->layoutState);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_SHOWN:
        {
            g_value_set_boolean(pValue, pSelf->priv->shown);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_HIDDEN:
        {
            g_value_set_boolean(pValue, pSelf->priv->hidden);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_HISTORY_SUPPORT:
        {
            g_value_set_boolean(pValue, pSelf->priv->historySupport);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_OFFSET:
        {
            g_value_set_float(pValue, pSelf->priv->offset);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_ENTRY:
        {
            g_value_set_enum (pValue, pSelf->priv->entryType);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_MODEL:
        {
            g_value_set_object (pValue, pSelf->priv->model);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_CURSOR_FOCUS:
        {
            g_value_set_int (pValue, pSelf->priv->focus_entry);
            break;
        }
        case MILDENHALL_SPELLER_PROP_ENUM_SPELLER_STATE:
        {
            g_value_set_uint (pValue, pSelf->priv->state);
            break;
        }
        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
            break;
        }
    }
}

/********************************************************
 * Function : v_mildenhall_speller_constructed
 * Description: the constructor function is called by g_object_new()
                to complete the object initialization after all the
                construction properties are set.
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_constructed (GObject *pObject)
{
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pObject);

    ClutterAction *swipeActionDown = NULL;  // for speller swipe down
    ClutterAction *swipeActionUp = NULL; // for speller swipe up

    //create a group for speller
    pSelf->priv->spellerGroup = clutter_actor_new();

    //create a headopen & headclose texture and add to group
    if(NULL != pSelf->priv->headOpenPath)
    {
        MILDENHALL_SPELLER_PRINT("creation of header close image for the speller \n");
        pSelf->priv->headOpen = thornbury_ui_texture_create_new(pSelf->priv->headOpenPath,0, 0, FALSE, FALSE);
        clutter_actor_set_reactive(pSelf->priv->headOpen,TRUE);

        swipeActionUp = clutter_swipe_action_new();
        g_signal_connect (swipeActionUp,  "swept", G_CALLBACK (v_swipe_cb), pSelf);
    }

    if(NULL != pSelf->priv->headClosePath)
    {
        MILDENHALL_SPELLER_PRINT("creation of header close image for the speller \n");
        pSelf->priv->headClose = thornbury_ui_texture_create_new(pSelf->priv->headClosePath, 0, 0, FALSE, FALSE);
        clutter_actor_set_reactive(pSelf->priv->headClose,TRUE);

        swipeActionDown = clutter_swipe_action_new();
        g_signal_connect (swipeActionDown,"swept", G_CALLBACK (v_swipe_cb), pSelf);
    }
    if ((NULL != pSelf->priv->headOpen) && (NULL != pSelf->priv->headClose))
    {
        clutter_actor_add_action (CLUTTER_ACTOR(pSelf->priv->headOpen), swipeActionDown);
        clutter_actor_add_action (CLUTTER_ACTOR(pSelf->priv->headClose),swipeActionUp);
    }

    clutter_actor_add_child(CLUTTER_ACTOR(pSelf),pSelf->priv->spellerGroup );
    clutter_actor_set_position( pSelf->priv->spellerGroup, 2,0);

    clutter_actor_add_child(pSelf->priv->spellerGroup, pSelf->priv->headOpen);
    clutter_actor_set_position(pSelf->priv->headOpen, 2,0);

    clutter_actor_add_child(pSelf->priv->spellerGroup, pSelf->priv->headClose);
    clutter_actor_set_position(pSelf->priv->headClose, 2 ,0);

    g_signal_connect(CLUTTER_ACTOR(pSelf),"transitions-completed",G_CALLBACK(v_mildenhall_speller_transition_callback),pSelf);
    pSelf->priv->topBar = thornbury_ui_texture_create_new(pSelf->priv->topBarPath,0, 0,FALSE, FALSE);
}

/********************************************************
 * Function : v_mildenhall_speller_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_dispose (GObject *pObject)
{
    MildenhallSpeller *pSpeller = MILDENHALL_SPELLER (pObject);
    MildenhallSpellerPrivate *priv = SPELLER_PRIVATE (pSpeller);

    G_OBJECT_CLASS (mildenhall_speller_parent_class)->dispose (pObject);

    g_clear_pointer (&priv->layout, g_free);
    g_clear_pointer (&priv->layoutState, g_free);
    g_clear_pointer (&priv->defaultLayout, g_free);

    if(NULL != priv->headOpenPath)
    {
        g_free(priv->headOpenPath);
        priv->headOpenPath = NULL;
    }
    if(NULL != priv->headClosePath)
    {
        g_free(priv->headClosePath);
        priv->headClosePath = NULL;
    }
    if(NULL != priv->capsLockPath)
    {
        g_free(priv->capsLockPath);
        priv->capsLockPath = NULL;
    }
}

/********************************************************
 * Function : v_mildenhall_speller_finalize
 * Description: Finalize the speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_finalize (GObject *pObject)
{
    G_OBJECT_CLASS (mildenhall_speller_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_speller_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_init (MildenhallSpeller *pSelf)
{
    /* get the hash table for style properties */
    GHashTable *pStyleHash = thornbury_style_set (PKGDATADIR"/mildenhall_speller_style.json");

    pSelf->priv = SPELLER_PRIVATE (pSelf);
    pSelf->priv->layout = NULL;
    pSelf->priv->state  = MILDENHALL_SPELLER_HIDDEN;
    pSelf->priv->headOpenPath  = NULL;
    pSelf->priv->headClosePath = NULL;
    pSelf->priv->capsLockPath = NULL;
    pSelf->priv->defaultPad = NULL;
    pSelf->priv->model = NULL;
    pSelf->priv->shown = FALSE;
    pSelf->priv->hidden = TRUE;
    pSelf->priv->clearEntry = FALSE;
    pSelf->priv->defaultLayout = NULL;
    pSelf->priv->longPress = FALSE;
    pSelf->priv->historySupport = FALSE;
    pSelf->priv->bSelectText = FALSE;
    pSelf->priv->entryType = MILDENHALL_SPELLER_TYPE_NONE;

    //pSelf->priv->gestureEnabled = FALSE;
    pSelf->priv->defaultEntry = NULL;
    pSelf->priv->topBar       = NULL;
    pSelf->priv->topBarPath   = NULL;
    pSelf->priv->offset   = 0.0;
    pSelf->priv->focus_entry =0;

    /* pares the hash for styles */
    if(NULL != pStyleHash)
    {
        GHashTableIter iter;
        gpointer pKey, pValue;
        g_hash_table_iter_init(&iter, pStyleHash);
        /* iter per layer */
        while (g_hash_table_iter_next (&iter, &pKey, &pValue))
        {
            GHashTable *pHash = pValue;
            if(NULL != pHash)
            {
                g_hash_table_foreach(pHash, v_speller_parse_style, pSelf);
            }
        }
        /* free the style hash */
        thornbury_style_free(pStyleHash);
    }
}

/********************************************************
 * Function : mildenhall_speller_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_class_init ( MildenhallSpellerClass *pKlass )
{
    GObjectClass *pObject_class = G_OBJECT_CLASS (pKlass);
    GParamSpec *pPspec = NULL;

    g_type_class_add_private (pKlass, sizeof (MildenhallSpellerPrivate));

    pObject_class->get_property = v_mildenhall_speller_get_property;
    pObject_class->set_property = v_mildenhall_speller_set_property;
    pObject_class->dispose      = v_mildenhall_speller_dispose;
    pObject_class->finalize     = v_mildenhall_speller_finalize;
    pObject_class->constructed  = v_mildenhall_speller_constructed;

   /**
    * MildenhallSpeller:text-selection:
    *
    * set TRUE to select the text
    *
    */
   pPspec = g_param_spec_boolean ( "text-selection",
                                   "text-selection",
                                   "text-selection",
                                   FALSE,
                                   G_PARAM_READWRITE);
   g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_SELECT_TYPE, pPspec );

   /**
     * MildenhallSpeller:layout:
     *
     * set layout
     *
     */
    pPspec = g_param_spec_string( "layout",
                                  "layout",
                                  "layout",
                                  NULL,
                                  (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS ));
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_LAYOUT, pPspec );

    /**
     * MildenhallSpeller:layout-state:
     *
     * set layout-state
     *
     */
    pPspec = g_param_spec_string( "layout-state",
                                  "layout-state",
                                  "layout-state",
                                  NULL,
                                  (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS ));
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_LAYOUT_STATE, pPspec );

    /**
     * MildenhallSpeller:shown:
     *
     * set shown to TRUE/FALSE
     *
     */
    pPspec = g_param_spec_boolean ( "shown",
                                    "shown",
                                    "shown",
                                    FALSE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_SHOWN, pPspec );

    /**
     * MildenhallSpeller:hidden:
     *
     * set hidden to TRUE/FALSE
     *
     */
    pPspec = g_param_spec_boolean ( "hidden",
                                    "hidden",
                                    "hidden",
                                    TRUE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_HIDDEN, pPspec );

    /**
     * MildenhallSpeller:history-support:
     *
     * set history-support to TRUE/FALSE
     *
     */
    pPspec = g_param_spec_boolean ( "history-support",
                                    "history-support",
                                    "history-support",
                                    FALSE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_HISTORY_SUPPORT, pPspec );

    /**
     * MildenhallSpeller:y-offset:
     *
     * set y-offset to 0 for default
     *
     */
    pPspec = g_param_spec_float ( "y-offset",
                                  "y-offset",
                                  "y-offset",
                                  0.0, G_MAXFLOAT,
                                  0.0,
                                  G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_OFFSET, pPspec );

    /**
     * MildenhallSpeller:entry:
     *
     * set entry type according to requriment
     *
     */
    pPspec = g_param_spec_enum ( "entry",
                                 "entry",
                                 "entry",
                                 MILDENHALL_SPELLER_GET_ENTRY_TYPE,
                                 MILDENHALL_SPELLER_DEFAULT_ENTRY,
                                 G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_SPELLER_PROP_ENUM_ENTRY, pPspec );

    /**
     * MildenhallSpeller:speller-state:
     *
     * set speller-state to LOWER/UPPER/SYMBOL
     *
     */
    /* FIXME: This has wrong min/max/default values. T1457 */
    pPspec = g_param_spec_uint( "speller-state",
                                "speller state",
                                "state of speller",
                                0,MILDENHALL_SPELLER_HIDDEN,
                                MILDENHALL_SPELLER_HIDDEN,
                                G_PARAM_READABLE);
    g_object_class_install_property (pObject_class, MILDENHALL_SPELLER_PROP_ENUM_SPELLER_STATE, pPspec);

    /**
     * MildenhallSpeller:model:
     * 
     * Sets data to the speller.To give the data to speller refer #MildenhallSpellerModelColumns.
     *
     */
    pPspec = g_param_spec_object ( "model",
                                   "Model",
                                   "model",
                                   G_TYPE_OBJECT,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObject_class, MILDENHALL_SPELLER_PROP_ENUM_MODEL, pPspec);

	/**
	 * MildenhallSpeller:cursor-entry:
	 *
	 * this to set focus entry
	 */
	pPspec = g_param_spec_int ( "cursor-entry",
			"cursor-entry",
			"cursor-entry",
			0,
			40,
			0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObject_class, MILDENHALL_SPELLER_PROP_ENUM_CURSOR_FOCUS, pPspec);

    /**
     * MildenhallSpeller::go-pressed
     * @self: a #MildenhallSpeller
     *
     * Signal emitted when go button is pressed
     */
    uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_GO_PRESSED] = g_signal_new ("go-pressed",
            G_TYPE_FROM_CLASS (pObject_class),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallSpellerClass, go_pressed),
            NULL, NULL,
            g_cclosure_marshal_generic,
            G_TYPE_NONE, 1,G_TYPE_VARIANT);

    /**
     * MildenhallSpeller::key-pad-pressed
     * @self: a #MildenhallSpeller
     * @keypad: pressed keypad
     * @keyname: pressed keypad name
     *
     * Signal emitted when key-pad is pressed
     *
     */
    /* FIXME: never used for API users, but for the consistency with 'go-pressed',
       this is not removed.  See T1465 */
    uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_KEY_EVENT] = g_signal_new ("key-pad-pressed",
            G_TYPE_FROM_CLASS (pObject_class),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallSpellerClass, key_pad_pressed),
            NULL, NULL,
            g_cclosure_marshal_VOID__STRING,
            G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_STRING);

    /**
     * MildenhallSpeller::speller-shown
     * @self: a #MildenhallSpeller
     *
     * Signal emitted speller is shown
     *
     */
    uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_SHOWN] = g_signal_new ("speller-shown",
            G_TYPE_FROM_CLASS (pObject_class),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallSpellerClass, shown),
            NULL, NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE, 0);

    /**
     * MildenhallSpeller::speller-hidden
     * @self: a #MildenhallSpeller
     *
     * Signal emitted speller is hidden
     *
     */
    uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_HIDDEN] = g_signal_new ("speller-hidden",
            G_TYPE_FROM_CLASS (pObject_class),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallSpellerClass, hidden),
            NULL, NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE, 0);

   /**
    * MildenhallSpeller::entry-status:
    * @self: a #MildenhallSpeller reference
    * @button_name: the name of button
    * @button_id: the id of button
    *
    * Signal emitted entry is pressed
    */
    uin_mildenhall_speller_signals[SIG_MILDENHALL_SPELLER_ENTRY_STATUS] = g_signal_new ("entry-status",
            G_TYPE_FROM_CLASS (pObject_class),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallSpellerClass, entry_status),
            NULL, NULL,
            g_cclosure_marshal_generic,
            G_TYPE_NONE, 2,G_TYPE_STRING, G_TYPE_STRING);
}

/**
 * mildenhall_speller_new:
 *
 * Creates a speller object
 *
 * Returns: (transfer full): a new #MildenhallSpeller
 */
MildenhallSpeller *
mildenhall_speller_new (void)
{
  return g_object_new (MILDENHALL_TYPE_SPELLER, NULL);
}

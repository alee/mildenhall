/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* meta-info-header.h */

#ifndef _META_INFO_HEADER_H
#define _META_INFO_HEADER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_META_INFO_HEADER mildenhall_meta_info_header_get_type ()
G_DECLARE_DERIVABLE_TYPE (MildenhallMetaInfoHeader, mildenhall_meta_info_header, MILDENHALL, META_INFO_HEADER, ClutterActor)

struct _MildenhallMetaInfoHeaderClass
{
  ClutterActorClass parent_class;
  void (*icon_toggled) (MildenhallMetaInfoHeader *self, const gchar *new_state);
};

MildenhallMetaInfoHeader *mildenhall_meta_info_header_new (void);

G_END_DECLS

#endif /* _META_INFO_HEADER_H */

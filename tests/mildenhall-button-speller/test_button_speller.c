/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_button_speller.h"
#include "mildenhall-internal.h"

#include <thornbury/thornbury.h>

/* header model columns */
enum
{
        COLUMN_TEXT,
        COLUMN_NONE
};

/* creation of model */
ThornburyModel *create_test_speller_model()
{
        ThornburyModel *model = NULL;
        model = (ThornburyModel *)thornbury_list_model_new (COLUMN_NONE,
                                        G_TYPE_STRING, NULL,
                                        -1);
        thornbury_model_append (model,
                                         COLUMN_TEXT,"TEST",
                                        -1);

        return model;

}

int main (int argc, char **argv)
{
    g_autofree gchar *pImage = NULL;
    g_autofree gchar *overlay_img_file = NULL;
    g_autofree gchar *prop_file = NULL;
    ThornburyItemFactory *itemFactory;
    int clInErr = clutter_init(&argc, &argv);
    if (clInErr != CLUTTER_INIT_SUCCESS)
        return -1;

    ClutterActor *stage;
    stage = clutter_stage_new ();

    pImage = g_build_filename (_mildenhall_get_theme_path (),
        "test-drawer-base",
        "background.png",
        NULL);

    ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
    clutter_actor_add_child(stage, actor);

    clutter_actor_set_size (stage, 728, 480);
    g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

    overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
        "content_overlay.png",
        NULL);

    ClutterActor *overlay = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
    clutter_actor_add_child(stage, overlay);

    prop_file = _mildenhall_get_resource_path ("mildenhall_button_speller_text_prop.json");
    itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_BUTTON_SPELLER,
        prop_file);
    GObject *pObject = NULL;
    g_object_get(itemFactory, "object", &pObject, NULL);
    MildenhallButtonSpeller *pButtonSpeller = MILDENHALL_BUTTON_SPELLER(pObject);
    ClutterActor *pButton = CLUTTER_ACTOR(pButtonSpeller);
    clutter_actor_add_child(stage, pButton);
    g_object_set(pButton,"font-type","DejaVuSansCondensed 32px","reactive",TRUE,NULL);

    	/* set header model */
	ThornburyModel *pTestSpeller = create_test_speller_model();
	g_object_set(pButton, "model", pTestSpeller, NULL);

    clutter_actor_show (stage);
    clutter_main();
    return 0;
}


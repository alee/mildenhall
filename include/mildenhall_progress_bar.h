/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_progress_bar.h */

#ifndef __MILDENHALL_PROGRESS_BAR_H__
#define __MILDENHALL_PROGRESS_BAR_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include "liblightwood-progressbase.h"
#include <math.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_PROGRESS_BAR mildenhall_progress_bar_get_type()

#define MILDENHALL_PROGRESS_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_PROGRESS_BAR, MildenhallProgressBar))

#define MILDENHALL_PROGRESS_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_PROGRESS_BAR, MildenhallProgressBarClass))

#define MILDENHALL_IS_PROGRESS_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_PROGRESS_BAR))

#define MILDENHALL_IS_PROGRESS_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_PROGRESS_BAR))

#define MILDENHALL_PROGRESS_BAR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_PROGRESS_BAR, MildenhallProgressBarClass))

typedef struct _MildenhallProgressBar MildenhallProgressBar;
typedef struct _MildenhallProgressBarClass MildenhallProgressBarClass;
typedef struct _MildenhallProgressBarPrivate MildenhallProgressBarPrivate;


struct _MildenhallProgressBar
{
  LightwoodProgressBase parent;

  MildenhallProgressBarPrivate *priv;
};

struct _MildenhallProgressBarClass
{
  LightwoodProgressBaseClass parent_class;
};

GType mildenhall_progress_bar_get_type (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __MILDENHALL_PROGRESS_BAR_H__ */

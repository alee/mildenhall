/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* meta-info-footer.h */

#ifndef __MILDENHALL_META_INFO_FOOTER_H
#define __MILDENHALL_META_INFO_FOOTER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_META_INFO_FOOTER mildenhall_meta_info_footer_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallMetaInfoFooter, mildenhall_meta_info_footer, MILDENHALL, META_INFO_FOOTER, ClutterActor)

MildenhallMetaInfoFooter *mildenhall_meta_info_footer_new (void);

gboolean mildenhall_meta_info_footer_get_show_top       (MildenhallMetaInfoFooter *self);
void mildenhall_meta_info_footer_set_show_top           (MildenhallMetaInfoFooter *self,
                                                         gboolean enable);

gboolean mildenhall_meta_info_footer_get_show_left_icon (MildenhallMetaInfoFooter *self);
void mildenhall_meta_info_footer_set_show_left_icon     (MildenhallMetaInfoFooter *self,
                                                         gboolean enable);

gboolean mildenhall_meta_info_footer_get_show_ratings   (MildenhallMetaInfoFooter *self);
void mildenhall_meta_info_footer_set_show_ratings       (MildenhallMetaInfoFooter *self,
                                                         gboolean enable);

ThornburyModel * mildenhall_meta_info_footer_get_model  (MildenhallMetaInfoFooter *self);
void mildenhall_meta_info_footer_set_model              (MildenhallMetaInfoFooter *self,
                                                         ThornburyModel *model);

G_END_DECLS

#endif /* __MILDENHALL_META_INFO_FOOTER_H */

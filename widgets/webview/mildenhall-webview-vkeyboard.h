/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-webview-speller.h */

#ifndef __MILDENHALL_WEBVIEW_VKEYBOARD_H__
#define __MILDENHALL_WEBVIEW_VKEYBOARD_H__

#include <glib-object.h>
#include "mildenhall_speller.h"
#include <shm-vkeyboard.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_WEBVIEW_VKEYBOARD mildenhall_webview_vkeyboard_get_type()

#define MILDENHALL_WEBVIEW_VKEYBOARD(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_WEBVIEW_VKEYBOARD, MildenhallWebviewVKeyboard))

#define MILDENHALL_WEBVIEW_VKEYBOARD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_WEBVIEW_VKEYBOARD, MildenhallWebviewVKeyboardClass))

#define MILDENHALL_IS_WEBVIEW_VKEYBOARD(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_WEBVIEW_VKEYBOARD))

#define MILDENHALL_IS_WEBVIEW_VKEYBOARD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_WEBVIEW_VKEYBOARD))

#define MILDENHALL_WEBVIEW_VKEYBOARD_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_WEBVIEW_VKEYBOARD, MildenhallWebviewVKeyboardClass))

typedef struct _MildenhallWebviewVKeyboard MildenhallWebviewVKeyboard;
typedef struct _MildenhallWebviewVKeyboardClass MildenhallWebviewVKeyboardClass;
typedef struct _MildenhallWebviewVKeyboardPrivate MildenhallWebviewVKeyboardPrivate;

struct _MildenhallWebviewVKeyboard
{
  ShmVKeyboard parent;

  MildenhallWebviewVKeyboardPrivate *priv;
};

struct _MildenhallWebviewVKeyboardClass
{
  ShmVKeyboardClass parent_class;
};

G_END_DECLS

#endif /* __MILDENHALL_WEBVIEW_VKEYBOARD_H__ */

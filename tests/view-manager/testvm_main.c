/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "testvm.h"

#include <canterbury/gdbus/canterbury.h>
#include <thornbury/thornbury.h>

enum
{
        COLUMN_NAME,
        COLUMN_ICON,
        COLUMN_TOOLTIP_TEXT,
        COLUMN_REACTIVE,
        COLUMN_LAST
};

G_DEFINE_TYPE (testvm, testvm, CLUTTER_TYPE_ACTOR)

#define testvm_PRIVATE(o) \
(G_TYPE_INSTANCE_GET_PRIVATE ((o), testvm_TYPE, testvmPrivate))

ThornburyViewManager *view_manager;

static void initialize_client_handlers(ClutterActor* actor);

int main( gint argc, gchar **argv )
{
  initialize_ui_toolkit(argc , argv);
  ClutterActor *stage = create_mildenhall_window(testvm_APP_NAME);

  ClutterActor *testvm = NULL;
	testvm = testvm_create();
	gchar *path = "/home/mildenhall/workspace/testvm/resources/views/";

	ThornburyViewManagerAppData *appdata = g_new0(ThornburyViewManagerAppData,1);
	appdata->app= CLUTTER_ACTOR(testvm);
	appdata->default_view="View2";
	appdata->viewspath = path;
	appdata->app_name = testvm_APP_NAME;



	view_manager = thornbury_view_manager_new(appdata );
	thornbury_build_all_views(view_manager);
	g_signal_connect(view_manager,"view_switch_end",G_CALLBACK(view_switched),NULL);

	GHashTable *models = create_models("context");
	thornbury_set_widgets_models(view_manager,models);

	GHashTable *signals = create_signals("context");
	thornbury_set_widgets_controllers(view_manager,signals);

	clutter_actor_add_child (CLUTTER_ACTOR (stage),testvm);
    initialize_client_handlers(testvm);

  clutter_main ();
	return TRUE;
}
static void initialize_client_handlers(ClutterActor* actor)
{
  initialize_app_manager_client_handler(actor);
}
static void testvm_get_property (GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
  switch (property_id)
  {
	 default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}
static void testvm_set_property (GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
  switch (property_id)
  {
     default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}
static void testvm_dispose (GObject *object)
{
  G_OBJECT_CLASS (testvm_parent_class)->dispose (object);
}
static void testvm_finalize (GObject *object)
{
  G_OBJECT_CLASS (testvm_parent_class)->finalize (object);
}
ClutterActor *testvm_create()
{
	testvm *testvm_o = g_object_new(testvm_TYPE, NULL);
/*	testvmPrivate *priv = testvm(testvm_o)->priv;

	GError *err = NULL;
	ClutterColor color = { 0xFF, 0xFF, 0xFF, 0xFF };

  priv->testvm_text =  clutter_text_new_full(MILDENHALL_DEFAULT_FONT(45),"testvm",&color);
	clutter_actor_set_size (priv->testvm_text,300,100);
	clutter_actor_set_position(priv->testvm_text,250,190);
	clutter_actor_add_child(CLUTTER_ACTOR(testvm_o),priv->testvm_text);
	clutter_actor_show(priv->testvm_text);
*/

	return CLUTTER_ACTOR(testvm_o);

}

static void testvm_class_init (testvmClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	g_type_class_add_private (klass, sizeof (testvmPrivate));
	object_class->get_property = testvm_get_property;
	object_class->set_property = testvm_set_property;
	object_class->dispose = testvm_dispose;
	object_class->finalize = testvm_finalize;
}


static void testvm_init (testvm *self)
{
	self->priv = testvm_PRIVATE (self);

}

GHashTable * create_models(gchar *widgetname)
{
	GHashTable *models = g_hash_table_new(g_str_hash,g_str_equal);

	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
									G_TYPE_STRING, NULL,
									G_TYPE_STRING, NULL,
									G_TYPE_STRING, NULL,
									G_TYPE_BOOLEAN, NULL,
									-1);
	thornbury_model_append (model, COLUMN_NAME, "MUSIC",
									COLUMN_ICON, "testvm/icon_music_AC.png",
									COLUMN_TOOLTIP_TEXT, "MUSIC",
									COLUMN_REACTIVE, TRUE,
									-1);
	thornbury_model_append (model, COLUMN_NAME, "ARTISTS",
									COLUMN_ICON, "testvm/icon_music_artists_AC.png",
									COLUMN_TOOLTIP_TEXT, "ARTIST",
									COLUMN_REACTIVE, TRUE,
									-1);
	thornbury_model_append (model,  COLUMN_NAME, "INTERNET",
									COLUMN_ICON, "testvm/icon_internet_AC.png",
									COLUMN_TOOLTIP_TEXT, "INTERNET",
									COLUMN_REACTIVE, TRUE,
									-1);
	thornbury_model_append (model, COLUMN_NAME, "ALBUMS",
									COLUMN_ICON, "testvm/icon_music_AC.png",
									COLUMN_TOOLTIP_TEXT, "ALBUMS",
									COLUMN_REACTIVE, TRUE,
									-1);
	g_hash_table_insert(models,widgetname,model);
	return models;
}

GHashTable * create_signals(gchar *widgetname)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;

	ThornburyViewManagerSignalData *sig_data = g_new0(ThornburyViewManagerSignalData,1);
	sig_data->signalname = "drawer-open";
	sig_data->func = G_CALLBACK(drawer_opened);
	sig_data->data = NULL;

	ThornburyViewManagerSignalData *sig_data_2 = g_new0(ThornburyViewManagerSignalData,1);
	sig_data_2->signalname = "drawer-close";
	sig_data_2->func = G_CALLBACK(drawer_closed);
	sig_data_2->data = NULL;

	ThornburyViewManagerSignalData *sig_data_3 = g_new0(ThornburyViewManagerSignalData,1);
	sig_data_3->signalname = "drawer-button-released";
	sig_data_3->func = G_CALLBACK(drawer_button_released);
	sig_data_3->data = NULL;

	lstsignals = g_list_append(lstsignals,sig_data);
	lstsignals = g_list_append(lstsignals,sig_data_2);
	lstsignals = g_list_append(lstsignals,sig_data_3);

	g_hash_table_insert(signals,widgetname,lstsignals);



	return signals;
}

void drawer_opened(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void drawer_closed(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void drawer_button_released(GObject *pActor, gchar *pName, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);
        g_print("DRAWER_WIDGET: button released = %s\n", pName);
        if(!g_strcmp0("INTERNET",pName))
        {
        	GHashTable *models = create_models("context_1");
			thornbury_set_widgets_models(view_manager,models);

			GHashTable *signals = create_signals("context_1");
			thornbury_set_widgets_controllers(view_manager,signals);
			thornbury_switch_view(view_manager,"view1",TRUE,-1);
        }
        else if(!g_strcmp0("ARTISTS",pName))
        {
        	GHashTable *models = create_models("context_2");
			thornbury_set_widgets_models(view_manager,models);

			GHashTable *signals = create_signals("context_2");
			thornbury_set_widgets_controllers(view_manager,signals);
			thornbury_switch_view(view_manager,"view3",TRUE,-1);

        }
        else if(!g_strcmp0("MUSIC",pName))
        {
        	//load_previous_view(view_manager);
        }

}
void view_switched(ThornburyViewManager *vmgr,gpointer *data)
{
	gchar *view = (gchar *)data;
	g_print("switched to view : %s\n",view);
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**************************************************************************************************
* Filename          : testvm.h
* Package           : MILDENHALL
* Application       : testvm
* Copyright         : 
* Description       : 
**************************************************************************************************/
/* inclusion guard */
#ifndef __testvm_H__
#define __testvm_H__

#include <thornbury/thornbury.h>
#include "mildenhall_ui_utility.h"

G_BEGIN_DECLS

#define testvm_TYPE testvm_get_type()

#define testvm(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  testvm_TYPE, testvm))

#define testvm_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  testvm_TYPE, testvmClass))

#define testvm_IS(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  testvm_TYPE))

#define testvm_IS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  testvm_TYPE))

#define testvm_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  testvm_TYPE, HelloWorldClass))

typedef struct _testvm        testvm;
typedef struct _testvmClass    testvmClass;
typedef struct _testvmPrivate  testvmPrivate;

#define testvm_APP_NAME "testvm"

struct _testvm
{
  ClutterActor parent;

  testvmPrivate *priv;
};

struct _testvmClass
{
  ClutterActorClass parent_class;
};
struct _testvmPrivate
{
   ClutterActor      *testvm_text;
};

GType testvm_get_type (void) G_GNUC_CONST;
ClutterActor *testvm_create();
GHashTable * create_models(gchar *widgetname);
GHashTable * create_signals(gchar *widgetname);
void drawer_opened(GObject *pActor, gpointer pUserData);
void drawer_closed(GObject *pActor, gpointer pUserData);
void drawer_button_released(GObject *pActor, gchar *pName, gpointer pUserData);
void view_switched(ThornburyViewManager *vmgr,gpointer *data);

void initialize_app_manager_client_handler(ClutterActor* actor);

G_END_DECLS

#endif /* __testvm_H__ */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_SELECTION_POPUP_ITEM_H
#define _MILDENHALL_SELECTION_POPUP_ITEM_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <clutter/clutter.h>
#include <glib-object.h>
#include <thornbury/thornbury.h>

/******************************************************************************
 *	@Filename : mildenhall-selection-popup-item.h
 *	@Created on : Jan 7, 2013
 *-----------------------------------------------------------------------------
 * 	@Created on : Jan 7, 2013
 *------------------------------------------------------------------------------
 *  @Description : Header file for mildenhall_selection_popup_item type
 *******************************************************************************/




/********************************************************************************
 * @Defines
 *******************************************************************************/

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SELECTION_POPUP_ITEM (mildenhall_selection_popup_item_get_type())

#define MILDENHALL_SELECTION_POPUP_ITEM(obj) \
		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				MILDENHALL_TYPE_SELECTION_POPUP_ITEM, MildenhallSelectionPopupItem))

#define MILDENHALL_SELECTION_POPUP_ITEM_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_CAST ((klass), \
				MILDENHALL_TYPE_SELECTION_POPUP_ITEM, MildenhallSelectionPopupItemClass))

#define MILDENHALL_IS_SELECTION_POPUP_ITEM(obj) \
		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
				MILDENHALL_TYPE_SELECTION_POPUP_ITEM))

#define MILDENHALL_IS_SELECTION_POPUP_ITEM_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_TYPE ((klass), \
				MILDENHALL_TYPE_SELECTION_POPUP_ITEM))

#define MILDENHALL_SELECTION_POPUP_ITEM_GET_CLASS(obj) \
		(G_TYPE_INSTANCE_GET_CLASS ((obj), \
				MILDENHALL_TYPE_SELECTION_POPUP_ITEM, MildenhallSelectionPopupItemClass))

#define SELECTION_POPUP_GET_ITEM_TYPE (v_selection_popup_get_item_type())

typedef struct _MildenhallSelectionPopupItem MildenhallSelectionPopupItem;
typedef struct _MildenhallSelectionPopupItemClass MildenhallSelectionPopupItemClass;


/**
 * _MildenhallSelectionPopupItem:
 * structure to hold the  variables of MILDENHALL selection popup Roller Item
 */

/**
 * enMildenhallSelectionPopupItemType:
 * @TYPE_ICON_LABEL_LABEL: |ICON |LABEL |LABEL | type|  will be created
 * @TYPE_ICON_LABEL_RADIO: |ICON |LABEL |RADIO | type|  will be created
 * @TYPE_ICON_LABEL_CONFIRM: |ICON |LABEL|	will be created and Go Button will be provided
 *
 * Item type that can be created using mildenhall_selection_popup_itam
 */
typedef enum
{
	/*< public >*/
	TYPE_FIRST,
	TYPE_ICON_LABEL_LABEL,
	TYPE_ICON_LABEL_RADIO,
	TYPE_ICON_LABEL_CONFIRM,
	TYPE_LAST
} enMildenhallSelectionPopupItemType;

struct _MildenhallSelectionPopupItem
{
	/*< private >*/
	ClutterGroup group;;
	GHashTable *pStyleHash;
	gchar *pNormalImagePath;
	gchar *pPressedImagePath;
	gchar *pMainTextFont;
	gchar *pSecondTextFont;
	ClutterColor MainTextColor;
	ClutterColor SecondTextColor;
	ClutterColor VerticalLineSeperatorColor;
	gchar *pLeftIcon;
	gchar *pRightFieldString;
	//gint inItemType;
	ThornburyModel *model;
	ClutterActor *pLeftField;
	ClutterActor *pMiddleField;
	ClutterActor *pRightField;
	gchar *pMiddleText;
	ClutterActor *pItemTexture;
	ClutterActor* pVerticalLine;
	gboolean bSignalConnected;
	gboolean bPressedState;
	enMildenhallSelectionPopupItemType inItemType;
};

/**
 * _MildenhallSelectionPopupItemClass:
 * structure to hold the  variables of MILDENHALL selection popup Roller Item Class
 */

struct _MildenhallSelectionPopupItemClass
{
	/*< private >*/
	ClutterGroupClass parent_class;
};


typedef struct _MildenhallSelPopupItemIconDetail MildenhallSelPopupItemIconDetail;
struct _MildenhallSelPopupItemIconDetail
{
	ClutterContent *pIconContent;
	gchar *pLocalFilePath;
	gchar *pHttpFilePath;
	gchar *pRowId;
	gboolean bHttpIconUpdated;
};




GType v_selection_popup_get_item_type(void);

GType mildenhall_selection_popup_item_get_type (void) G_GNUC_CONST;

ClutterActor* mildenhall_selection_popup_item_new (void);

G_END_DECLS

#endif /* _MILDENHALL_SELECTION_POPUP_ITEM_H */

/*** END OF FILE *************************************************************/

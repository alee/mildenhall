/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_button_speller
 * @title: MildenhallButtonSpeller
 * @short_description: creates a button for speller widget
 * @see_also: #MildenhallButtonDrawer, #LightwoodButton
 *
 * #MildenhallButtonSpeller widget can be used to display a button for
 * the speller widget. Typically it displays a text describing its action.
 * It is also possible to include an image inside the speller button.
 * It also provides model using which the application can set the various
 * properties using v_button_speller_set_model ().
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 * MildenhallButtonSpeller *button = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 * 	MILDENHALL_TYPE_TOGGLE_BUTTON,
 * 	"/usr/Application/MyApp/share/mildenhall_button_speller_text_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * button = MILDENHALL_BUTTON_SPELLER (object);
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (button));
 *
 * ]|
 *
 * Refer #ThornburyItemFactory for Thornbury Item Factory details.
 *
 * Since: 0.3.0
 */

#include "mildenhall_button_speller.h"

#define MILDENHALL_BUTTON_SPELLER_PRINT(...)  //g_print(__VA_ARGS__)

G_DEFINE_TYPE (MildenhallButtonSpeller, mildenhall_button_speller, LIGHTWOOD_TYPE_BUTTON)

#define BUTTON_SPELLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_BUTTON_SPELLER, MildenhallButtonSpellerPrivate))

typedef struct _MildenhallButtonSpellerPrivate MildenhallButtonSpellerPrivate;
typedef enum _MildenhallButtonSpellerProperty MildenhallButtonSpellerProperty;

/* property enums */
enum _MildenhallButtonSpellerProperty
{
    BUTTON_PROP_FIRST = 0,
        BUTTON_PROP_ENUM_TYPE,
        BUTTON_PROP_ENUM_NAME,
        BUTTON_PROP_ENUM_WIDTH,
        BUTTON_PROP_ENUM_HEIGHT,
        BUTTON_PROP_ENUM_X,
        BUTTON_PROP_ENUM_Y,
        BUTTON_PROP_ENUM_NORMAL,
        BUTTON_PROP_ENUM_PRESSED,
        BUTTON_PROP_ENUM_FONT_TYPE,
        BUTTON_PROP_ENUM_MODEL,
    BUTTON_PROP_LAST
};

struct _MildenhallButtonSpellerPrivate
{
	ClutterActor *pButtonGroup;
	ClutterActor *pBgNormal;
	ClutterActor *pBgPressed;
	ClutterActor *pSpellerIcon;
	gboolean bLeaveState;
	ThornburyModel *pModel;

	gfloat x;
	gfloat y;
	gfloat width;
	gfloat height;
    gchar *normalImage;
	gchar *pressedImage;
	gchar *type;
	gchar *name;
	gchar *fontType;
	ClutterActor *pLabel;
};


static void v_button_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallButtonSpeller *pButtonSpeller);
static void v_button_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallButtonSpeller *pButtonSpeller);
static void v_button_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallButtonSpeller *pButtonSpeller);
static void mildenhall_button_speller_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec);
static void mildenhall_button_speller_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec);
static void v_button_speller_update_view( MildenhallButtonSpeller *pButtonSpeller );

/********************************************************
 * Function : v_button_speller_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonSpeller *
 * Return value: void
 ********************************************************/
static void v_button_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallButtonSpeller *pButtonSpeller)
{
	MILDENHALL_BUTTON_SPELLER_PRINT("MILDENHALL_BUTTON_SPELLER_PRINT: %s\n", __FUNCTION__);
	/* update view */
	v_button_speller_update_view(pButtonSpeller);
	clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonSpeller));
}

/********************************************************
 * Function : v_button_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonSpeller *
 * Return value: void
 ********************************************************/
static void v_button_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallButtonSpeller *pButtonSpeller)
{
	MILDENHALL_BUTTON_SPELLER_PRINT("MILDENHALL_BUTTON_SPELLER_PRINT: %s\n", __FUNCTION__);
	/* update view */
	v_button_speller_update_view(pButtonSpeller);
	clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonSpeller));
}

/********************************************************
 * Function : v_button_speller_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonSpeller *
 * Return value: void
 ********************************************************/
static void v_button_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallButtonSpeller *pButtonSpeller)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
        /* this callback gives the actual row count before removal, and not after removal of row */
	gint inRemovedRow = thornbury_model_iter_get_row (pIter);
	MILDENHALL_BUTTON_SPELLER_PRINT("MILDENHALL_BUTTON_SPELLER_PRINT: %s\n", __FUNCTION__);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && thornbury_model_get_n_rows(priv->pModel) == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
}

/********************************************************
 * Function : v_button_speller_press_cb
 * Description: callabck on button speller press
 * Parameters: MildenhallButtonSpeller*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_speller_press_cb( ClutterActor *pButtonSpeller, gpointer pUserData )
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}

	if(NULL != priv->pBgPressed)
	{
        clutter_actor_show(priv->pBgPressed);
	}
	if(NULL != priv->pBgNormal)
	{
        clutter_actor_hide(priv->pBgNormal);
	}
}

/********************************************************
 * Function : v_button_speller_longpress_cb
 * Description: callabck on button speller long press
 * Parameters: MildenhallButtonSpeller*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_speller_longpress_cb( ClutterActor *pButtonSpeller, gpointer pUserData )
{
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
}

/********************************************************
 * Function : v_button_speller_release_cb
 * Description: callabck on button speller release
 * Parameters: MildenhallButtonSpeller*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_speller_release_cb( ClutterActor *pButtonSpeller, gpointer pUserData )
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}

    if(NULL != priv->pBgPressed)
    {
        clutter_actor_hide(priv->pBgPressed);
    }
    if(NULL != priv->pBgNormal)
    {
        clutter_actor_show(priv->pBgNormal);
    }
}

/********************************************************
 * Function : b_button_speller_leave_cb
 * Description: callabck on button speller leave
 * Parameters: MildenhallButtonSpeller*, pUserData
 * Return value: void
 ********************************************************/
static gboolean b_button_speller_leave_cb( ClutterActor *pButtonSpeller, ClutterEvent *pEvent, gpointer pUserData )
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return FALSE;
	}
	v_button_speller_release_cb(pButtonSpeller, NULL);

	priv->bLeaveState = TRUE;
	return FALSE;
}

 /**
 * mildenhall_button_speller_new:
 *
 * Creates a new #MildenhallButtonSpeller.
 *
 * Returns: (transfer full): speller button object
 */
ClutterActor *mildenhall_button_speller_new ( void )
{
    MILDENHALL_BUTTON_SPELLER_PRINT(" %s\n", __FUNCTION__);
    return g_object_new ( MILDENHALL_TYPE_BUTTON_SPELLER, NULL );
}

/********************************************************
 * Function : mildenhall_button_speller_dispose
 * Description: Dispose the mildenhall button speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_dispose ( GObject *pObject )
{
    MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pObject);
    MILDENHALL_BUTTON_SPELLER_PRINT ( " %s\n", __FUNCTION__ );

    g_clear_object (&priv->pModel);

    G_OBJECT_CLASS ( mildenhall_button_speller_parent_class )->dispose ( pObject );
}

/********************************************************
 * Function : mildenhall_button_speller_finalize
 * Description: Finalize the mildenhall button speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_finalize ( GObject *pObject )
{
    MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE(pObject);

    MILDENHALL_BUTTON_SPELLER_PRINT(" %s\n", __FUNCTION__ );

    g_free (priv->name);
    g_free (priv->type);
    g_free (priv->fontType);
    g_free (priv->normalImage);
    g_free (priv->pressedImage);

    G_OBJECT_CLASS ( mildenhall_button_speller_parent_class )->finalize ( pObject );
}


/********************************************************
 * Function : v_button_speller_update_view
 * Description: update the button view
 * Parameters: MildenhallButtonSpeller *
 * Return value: void
 ********************************************************/
static void v_button_speller_update_view( MildenhallButtonSpeller *pButtonSpeller )
{
    MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller );
    MILDENHALL_BUTTON_SPELLER_PRINT("MILDENHALL_BUTTON_SPELLER_PRINT: %s\n", __FUNCTION__);
    if (G_IS_OBJECT(priv->pModel) && thornbury_model_get_n_rows ( priv->pModel) > 0 )
    {
        ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->pModel, 0 );
        if(NULL != pIter)
        {
            GValue value = { 0, };
            thornbury_model_iter_get_value(pIter, 0, &value);
            if(!g_strcmp0(priv->type,"texture"))
            {
		gchar* pNormalImage = g_strdup (g_value_get_string (&value));
		gfloat flX, flY;
            	if(NULL != priv->pSpellerIcon)
            	{
            		clutter_actor_destroy(priv->pSpellerIcon);
            		priv->pSpellerIcon = NULL;
            	}
                priv->pSpellerIcon = thornbury_ui_texture_create_new(pNormalImage, 0, 0, FALSE, FALSE);
                g_free (pNormalImage);

               if(clutter_actor_get_width(priv->pSpellerIcon ) > priv->width)
                    g_object_set(priv->pSpellerIcon , "width", priv->width , NULL);
	        flX = (priv->width-clutter_actor_get_width (priv->pSpellerIcon )) / 2;
                /* set the position of icon to mid of the button */
                flY = (priv->height-clutter_actor_get_height (priv->pSpellerIcon)) / 2;
                g_object_set(priv->pSpellerIcon , "x", flX, "y", flY, NULL);
                clutter_actor_add_child(CLUTTER_ACTOR(pButtonSpeller), priv->pSpellerIcon );
            }
            else
            {
                 v_button_speller_set_text(CLUTTER_ACTOR(pButtonSpeller), g_value_get_string(&value));
            }
            g_value_unset (&value);
        }
    }
}

 /**
 * v_button_speller_set_model:
 * @pButtonSpeller : object reference
 * @pModel :set the model for button
 *
 * set the model for button
 *
 */
void v_button_speller_set_model(MildenhallButtonSpeller *pButtonSpeller, ThornburyModel *pModel)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}

    if(NULL != priv->pModel)
    {
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_button_speller_row_added_cb),   pButtonSpeller);
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_button_speller_row_changed_cb), pButtonSpeller);
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_button_speller_row_removed_cb), pButtonSpeller);
        g_object_unref (priv->pModel);
        priv->pModel = NULL;
    }
    /* update the new model with signals */
	if ( NULL != pModel)
    {
        g_return_if_fail (G_IS_OBJECT (pModel));
        priv->pModel = g_object_ref(pModel);

        g_signal_connect (priv->pModel, "row-added",   G_CALLBACK (v_button_speller_row_added_cb),   pButtonSpeller );
        g_signal_connect (priv->pModel, "row-changed", G_CALLBACK (v_button_speller_row_changed_cb), pButtonSpeller );
        g_signal_connect (priv->pModel, "row-removed", G_CALLBACK (v_button_speller_row_removed_cb), pButtonSpeller );
    }
    if ( NULL != pModel)
    {
        v_button_speller_update_view(pButtonSpeller);
    }
	clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonSpeller));
	g_object_notify (G_OBJECT(pButtonSpeller), "model");
}

 /**
 * v_button_speller_set_width:
 * @pButtonSpeller : object reference
 * @flWidth :set the width for button
 *
 * set the width for button
 *
 */
void v_button_speller_set_width(MildenhallButtonSpeller *pButtonSpeller, gfloat flWidth)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    MILDENHALL_BUTTON_SPELLER_PRINT(" \n flWidth %f \n",flWidth);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(priv->width != flWidth)
    {
            clutter_actor_set_width(CLUTTER_ACTOR(pButtonSpeller), flWidth);
            priv->width = flWidth;
    }
}

 /**
 * v_button_speller_set_height:
 * @pButtonSpeller : object reference
 * @flHeight :set the height for button
 *
 * set the height for button
 *
 */
void v_button_speller_set_height(MildenhallButtonSpeller *pButtonSpeller, gfloat flHeight)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
						MILDENHALL_BUTTON_SPELLER_PRINT(" \n flHeight %f \n",flHeight);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(priv->height != flHeight)
    {
        clutter_actor_set_height(CLUTTER_ACTOR(pButtonSpeller), flHeight);
            priv->height = flHeight;
    }
}

 /**
 * v_button_speller_set_y:
 * @pButtonSpeller : object reference
 * @flX :set the y position
 *
 * set the y position
 *
 */
void v_button_speller_set_y(MildenhallButtonSpeller *pButtonSpeller, gfloat flY)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    MILDENHALL_BUTTON_SPELLER_PRINT(" \n flY %f \n",flY);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(priv->y != flY)
    {
            clutter_actor_set_y(CLUTTER_ACTOR(pButtonSpeller), flY);
            priv->y = flY;
    }
}

 /**
 * v_button_speller_set_x:
 * @pButtonSpeller : object reference
 * @flX :set the x position
 *
 * set the x position
 *
 */
void v_button_speller_set_x(MildenhallButtonSpeller *pButtonSpeller, gfloat flX)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
				MILDENHALL_BUTTON_SPELLER_PRINT(" \n flX %f \n",flX);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(priv->x != flX)
    {
            clutter_actor_set_x(CLUTTER_ACTOR(pButtonSpeller), flX);
            priv->x = flX;
    }
}

 /**
 * v_button_speller_set_name:
 * @pButtonSpeller : object reference
 * @pName :set the name for button
 *
 * set the name for button
 *
 */
void v_button_speller_set_name(MildenhallButtonSpeller *pButtonSpeller, gchar *pName)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
			MILDENHALL_BUTTON_SPELLER_PRINT(" \n pName %s \n",pName);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if (g_strcmp0 (priv->name, pName) != 0)
    {
            clutter_actor_set_name(CLUTTER_ACTOR(pButtonSpeller), pName);
            g_free (priv->name);
            priv->name = g_strdup (pName);
    }
}


 /**
 * v_button_speller_set_type:
 * @pButtonSpeller : object reference
 * @pType :set the type as text/texture
 *
 * set the value according to type
 *
 */
void v_button_speller_set_type(MildenhallButtonSpeller *pButtonSpeller, gchar *pType)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
		MILDENHALL_BUTTON_SPELLER_PRINT(" \n pType %s \n",pType);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if (g_strcmp0 (priv->type, pType) != 0)
    {
            g_free (priv->type);
            priv->type = g_strdup(pType);
    }
}

 /**
 * v_button_speller_set_normal:
 * @pButtonSpeller : object reference
 * @pNormal : icon to set at default
 *
 * set the icon at default
 *
 */
void v_button_speller_set_normal(MildenhallButtonSpeller *pButtonSpeller, gchar *pNormal)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n pNormal %s \n",pNormal);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(NULL != pNormal)
    {
        g_free (priv->normalImage);
        if(NULL == priv->pBgNormal)
        {
            priv->normalImage = g_build_filename (_mildenhall_get_theme_path (),
						  pNormal,
						  NULL);
            priv->pBgNormal = thornbury_ui_texture_create_new(priv->normalImage, priv->width,priv->height, FALSE, FALSE);
            clutter_actor_add_child(priv->pButtonGroup, priv->pBgNormal);
        }
    }
}

 /**
 * v_button_speller_set_pressed:
 * @pButtonSpeller : object reference
 * @pPressed : icon to set when pressed
 *
 * set the icon when button is pressed
 *
 */
void v_button_speller_set_pressed(MildenhallButtonSpeller *pButtonSpeller, gchar *pPressed)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);

    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if(NULL != pPressed)
    {
        g_free (priv->pressedImage);
        if(NULL == priv->pBgPressed)
        {
            priv->pressedImage = g_build_filename (_mildenhall_get_theme_path (),
						   pPressed,
						   NULL);
            priv->pBgPressed = thornbury_ui_texture_create_new(priv->pressedImage, priv->width,priv->height, FALSE, FALSE);
            clutter_actor_add_child(priv->pButtonGroup, priv->pBgPressed);
            clutter_actor_hide(priv->pBgPressed);
        }
    }
}


/**
 * v_button_speller_set_font:
 * @pButtonSpeller : object reference
 * @pPressed : icon to set when pressed
 *
 * set the icon when button is pressed
 *
 */
void v_button_speller_set_font(MildenhallButtonSpeller *pButtonSpeller, gchar *pFontType)
{
	MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);

    if(!MILDENHALL_IS_BUTTON_SPELLER(pButtonSpeller))
	{
		g_warning("invalid button\n");
		return;
	}
    if (pFontType != NULL)
    {
        g_free (priv->fontType);
        priv->fontType = g_strdup(pFontType);
        if (priv->pLabel != NULL)
            clutter_text_set_font_name (CLUTTER_TEXT(priv->pLabel),priv->fontType);
    }
}


/********************************************************
 * Function : mildenhall_button_speller_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	MildenhallButtonSpeller *pButtonSpeller =  MILDENHALL_BUTTON_SPELLER (pObject);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s %d \n", __FUNCTION__,uinPropertyID);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pObject))
	{
		g_warning("invalid button\n");
		return;
	}

    switch ( uinPropertyID )
    {
        case BUTTON_PROP_ENUM_TYPE:
				v_button_speller_set_type(pButtonSpeller, (gchar *)g_value_get_string ( pValue ));
               break;

        case BUTTON_PROP_ENUM_NAME:
                v_button_speller_set_name(pButtonSpeller, (gchar *)g_value_get_string ( pValue ));
                break;

        case BUTTON_PROP_ENUM_WIDTH:
                v_button_speller_set_width(pButtonSpeller, g_value_get_float ( pValue ));
                break;

        case BUTTON_PROP_ENUM_HEIGHT:
				v_button_speller_set_height(pButtonSpeller, g_value_get_float ( pValue ));
                break;

        case BUTTON_PROP_ENUM_X:
                v_button_speller_set_x(pButtonSpeller, g_value_get_float ( pValue ));
                break;

        case BUTTON_PROP_ENUM_Y:
				v_button_speller_set_y(pButtonSpeller, g_value_get_float ( pValue ));
                break;

        case BUTTON_PROP_ENUM_NORMAL:
                v_button_speller_set_normal(pButtonSpeller, (gchar *)g_value_get_string ( pValue ));
                break;

        case BUTTON_PROP_ENUM_PRESSED:
				v_button_speller_set_pressed(pButtonSpeller, (gchar *)g_value_get_string ( pValue ));
                break;

        case BUTTON_PROP_ENUM_FONT_TYPE:
				v_button_speller_set_font(pButtonSpeller, (gchar *)g_value_get_string ( pValue ));
                break;

        case BUTTON_PROP_ENUM_MODEL:
				v_button_speller_set_model(pButtonSpeller, g_value_get_object ( pValue ));
                break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID ( pObject, uinPropertyID, pspec );
            break;
    }
}


/********************************************************
 * Function : mildenhall_button_speller_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_class_init ( MildenhallButtonSpellerClass *pKlass )
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
	GParamSpec *pspec = NULL;

        MILDENHALL_BUTTON_SPELLER_PRINT (" %s\n", __FUNCTION__);

	g_type_class_add_private (pKlass, sizeof (MildenhallButtonSpellerPrivate));
    pObjectClass->get_property = mildenhall_button_speller_get_property;
    pObjectClass->set_property = mildenhall_button_speller_set_property;
    pObjectClass->dispose = mildenhall_button_speller_dispose;
    pObjectClass->finalize = mildenhall_button_speller_finalize;

    /**
     * MildenhallButtonSpeller:type:
     *
     * Set this to known the type of button to create
     */
    pspec = g_param_spec_string ("type",
                                   "type",
                                   "type of the button",
                                   NULL,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_TYPE, pspec);

    /**
     * MildenhallButtonSpeller:name:
     *
     * Set this to known the name of button
     */
    pspec = g_param_spec_string ("name",
                                   "name",
                                   "name of the button",
                                   NULL,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_NAME, pspec);

    /**
     * MildenhallButtonSpeller:normal-image:
     *
     * Set this image to button at button release
     */
    pspec = g_param_spec_string ("normal-image",
                                   "normal-image",
                                   "normal-image",
                                   NULL,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_NORMAL, pspec);

    /**
     * MildenhallButtonSpeller:pressed-image:
     *
     * Set this image to button at button press
     */
    pspec = g_param_spec_string ("pressed-image",
                                   "pressed-image",
                                   "pressed-image",
                                   NULL,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_PRESSED, pspec);

    /**
     * MildenhallButtonSpeller:font-type:
     *
     * Set the font to button
     */
    pspec = g_param_spec_string ("font-type",
                                   "font-type",
                                   "font-type",
                                   NULL,
                                   G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_FONT_TYPE, pspec);

    /**
     * MildenhallButtonSpeller:width:
     *
     * this to set width of the button
     */
    pspec = g_param_spec_float ("width",
                                    "Width",
                                    "Width of the button",
                                    0.0, G_MAXFLOAT,
                                    0.0,
                                    G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_WIDTH, pspec);

    /**
     * MildenhallButtonSpeller:height:
     *
     * this to set height of the button
     */
	pspec = g_param_spec_float ("height",
                                    "Height",
                                    "Height of the button",
                                    0.0, G_MAXFLOAT,
                                    0.0,
                                    G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, BUTTON_PROP_ENUM_HEIGHT, pspec);

    /**
     * MildenhallButtonSpeller:x:
     *
     * this to set x position of the button
     */
	pspec = g_param_spec_float ("x",
                                    "x",
                                    "x of the button",
                                    0.0, G_MAXFLOAT,
                                    0.0,
                                    G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, BUTTON_PROP_ENUM_X, pspec);

    /**
     * MildenhallButtonSpeller:y:
     *
     * this to set y position of the button
     */
	pspec = g_param_spec_float ("y",
                                    "y",
                                    "y of the button",
                                    0.0, G_MAXFLOAT,
                                    0.0,
                                    G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, BUTTON_PROP_ENUM_Y, pspec);

    /**
     * MildenhallButtonSpeller:model:
     *
     * this to set model to button
     */
    pspec = g_param_spec_object ("model",
                                    "Model",
                                    "model having button states data",
                                    G_TYPE_OBJECT,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObjectClass, BUTTON_PROP_ENUM_MODEL, pspec );
}

/********************************************************
 * Function : mildenhall_button_speller_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_init ( MildenhallButtonSpeller *self )
{
    MILDENHALL_BUTTON_SPELLER_PRINT( " %s\n", __FUNCTION__ );
	self->priv = BUTTON_SPELLER_PRIVATE ( self );

	self->priv->pButtonGroup = NULL;
	self->priv->pBgNormal = NULL;
	self->priv->pBgPressed = NULL;
    self->priv->bLeaveState = FALSE;

	self->priv->x = 0.0;
	self->priv->y = 0.0;
	self->priv->width = 0.0;
	self->priv->height = 0.0;
	self->priv->name = NULL;
	self->priv->type = NULL;
	self->priv->normalImage = NULL;
	self->priv->pressedImage = NULL;
	self->priv->fontType = NULL;
	self->priv->pSpellerIcon = NULL;

	/* initialise the button group */
	self->priv->pButtonGroup = clutter_actor_new();
    clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pButtonGroup);
    clutter_actor_show (self->priv->pButtonGroup);

	/* connect the signals */
	g_signal_connect( self, "button-press",   G_CALLBACK(v_button_speller_press_cb), NULL );
	g_signal_connect( self, "button-release", G_CALLBACK(v_button_speller_release_cb), NULL );
	g_signal_connect( self, "leave-event",    G_CALLBACK(b_button_speller_leave_cb), NULL );
	g_signal_connect( self, "button-long-press",G_CALLBACK(v_button_speller_longpress_cb), NULL );
    g_signal_connect( self, "button-reset",   G_CALLBACK(v_button_speller_release_cb), NULL);
}



 /**
 * v_button_speller_set_text:
 * @pActor: object reference
 * @pText: text to set
 *
 * sets the text for button
 *
 */
void v_button_speller_set_text(ClutterActor *pActor, const gchar *pText)
{
        MildenhallButtonSpeller *pButtonSpeller = MILDENHALL_BUTTON_SPELLER (pActor);
        MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
        ClutterColor white = {0xFF, 0xFF, 0xFF, 0xFF};
        ClutterColor transp =  {0x98,0xa9,0xb3,0xff};
        gfloat flX, flY;

	MILDENHALL_BUTTON_SPELLER_PRINT("MILDENHALL_BUTTON_SPELLER_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_BUTTON_SPELLER(pActor))
	{
		g_warning("invalid button\n");
		return ;
	}
	if (NULL == pText)
		return;

    if(NULL != priv->pLabel)
    {
        clutter_actor_destroy(CLUTTER_ACTOR(priv->pLabel));
        priv->pLabel = NULL;
    }

    if(!g_strcmp0("CANCEL",pText) || !g_strcmp0("AUTOCOMPLETE",pText) || !g_strcmp0("SYMBOLS",pText) ||
       !g_strcmp0("SPACE",pText) || !g_strcmp0("←",pText))
    {
        priv->pLabel= clutter_text_new_full (priv->fontType, pText, &transp);
    }
    else if(!g_strcmp0("RETURN",pText))
    {
        priv->pLabel= clutter_text_new_full (priv->fontType, " ", &transp);
    }
    else
    {
        if( NULL == priv->fontType)
        {
            priv->pLabel = clutter_text_new_full ("DejaVuSansCondensed 32px", pText, &white);
        }
        else
        {
            priv->pLabel = clutter_text_new_full (priv->fontType, pText, &white);
        }
    }
    if(clutter_actor_get_width(priv->pLabel) > priv->width)
        g_object_set(priv->pLabel, "width", priv->width , NULL);
    flX = (priv->width-clutter_actor_get_width (priv->pLabel)) / 2;

    /* set the position of icon to mid of the button */
    flY = (priv->height-clutter_actor_get_height(priv->pLabel)) / 2;
    g_object_set(priv->pLabel, "x", flX, "y", flY, NULL);
    clutter_actor_add_child (priv->pButtonGroup, priv->pLabel);
}

/********************************************************
 * Function : mildenhall_button_speller_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_button_speller_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
        MildenhallButtonSpeller *pButtonSpeller = MILDENHALL_BUTTON_SPELLER (pObject);
        MildenhallButtonSpellerPrivate *priv = BUTTON_SPELLER_PRIVATE (pButtonSpeller);
	MILDENHALL_BUTTON_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
    if(!MILDENHALL_IS_BUTTON_SPELLER(pObject))
	{
		g_warning("invalid button\n");
		return;
	}

	switch ( uinPropertyID )
	{
        case BUTTON_PROP_ENUM_MODEL:
				g_value_set_object ( pValue, priv->pModel);
                break;

        case BUTTON_PROP_ENUM_WIDTH:
                g_value_set_float ( pValue, priv->width);
                break;

        case BUTTON_PROP_ENUM_HEIGHT:
				g_value_set_float ( pValue, priv->height);
                break;

        case BUTTON_PROP_ENUM_X:
                g_value_set_float ( pValue, priv->x);
                break;

        case BUTTON_PROP_ENUM_Y:
				g_value_set_float ( pValue, priv->y);
                break;

        case BUTTON_PROP_ENUM_NAME:
                g_value_set_string ( pValue, priv->name);
                break;

        case BUTTON_PROP_ENUM_TYPE:
				g_value_set_string ( pValue, priv->type);
                break;

        case BUTTON_PROP_ENUM_NORMAL:
                g_value_set_string ( pValue, priv->normalImage);
                break;

        case BUTTON_PROP_ENUM_PRESSED:
				g_value_set_string ( pValue, priv->pressedImage);
                break;

        case BUTTON_PROP_ENUM_FONT_TYPE:
				g_value_set_string ( pValue, priv->fontType);
                break;

		default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID ( pObject, uinPropertyID, pspec );
                break;
	}
}


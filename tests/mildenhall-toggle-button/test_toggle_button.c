/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_toggle_button.h"

#include <thornbury/thornbury.h>

#define MILDENHALL_TEST_TOGGLE_BUTTON_PRINT( a ...)			g_print(a);
gint count;

/* model columns */
enum
{
	COLUMN_TEXT,
	COLUMN_ACTIVE_ICON_PATH,
	COLUMN_INACTIVE_ICON_PATH,
	COLUMN_STATE_ID,
	COLUMN_LAST
};


/* button state changed callback, if multi state */
static void button_toggled(GObject *button, gpointer newState)
{

	g_print("MILDENHALL_TEST_TOGGLE_BUTTON_PRINT: new state = %s\n", (gchar*)newState);
	count++;
	if(count == 15)
	{
		g_object_set (button,
		              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
		              NULL);
		count=0;
	}
	if(count == 5)
		g_object_set (button,
		              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
		              NULL);
	if(count == 10)
		g_object_set (button,
		              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
		              NULL);
}


/* creation of model */
ThornburyModel *create_model()
{
	ThornburyModel *model = NULL;
        g_autofree gchar *music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-button-drawer",
            "icon_music_AC.png",
            NULL);

	model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			-1);
	thornbury_model_append (model, COLUMN_TEXT, "MILDENHALL-III", COLUMN_ACTIVE_ICON_PATH, music_icon_file, COLUMN_INACTIVE_ICON_PATH, music_icon_file,
			COLUMN_STATE_ID, "MILDENHALL-III",
			-1);

	return model;

}

int main (int argc, char **argv)
{
	g_autofree gchar *prop_file = NULL;
	ThornburyItemFactory *itemFactory;
        g_autofree gchar *internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-button-drawer",
            "icon_internet_AC.png",
            NULL);
        g_autofree gchar *icon_pause_file = g_build_filename (_mildenhall_get_theme_path (),
            "icon_pause.png",
            NULL);
        g_autofree gchar *music_artists_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-button-drawer",
            "icon_music_artists_AC.png",
            NULL);
	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;

	ClutterActor *stage;
	ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

	stage = clutter_stage_new ();
	clutter_actor_set_background_color (stage, &black);
	clutter_actor_set_size (stage, 728, 480);
	g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	prop_file = _mildenhall_get_resource_path ("mildenhall_toggle_button_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
	    MILDENHALL_TYPE_TOGGLE_BUTTON, prop_file);
	GObject *pObject = NULL;

	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallToggleButton *toggle = MILDENHALL_TOGGLE_BUTTON(pObject);
	ClutterActor *button = CLUTTER_ACTOR(toggle);

	clutter_actor_add_child(stage, button);
	ThornburyModel *model = create_model();
	g_object_set(button, "state-model", model,NULL);

	thornbury_model_append (model, COLUMN_TEXT, "TEST",
			COLUMN_ACTIVE_ICON_PATH, internet_icon_file, COLUMN_INACTIVE_ICON_PATH, icon_pause_file,
			COLUMN_STATE_ID, "TEST", -1);

	thornbury_model_append (model, COLUMN_TEXT, "abc",
			COLUMN_ACTIVE_ICON_PATH, music_artists_icon_file, COLUMN_INACTIVE_ICON_PATH, NULL,
			COLUMN_STATE_ID, "abc", -1);

	int display_option;
	g_print("\n ENTER THE Button TYPE \n < 1 > for Active < 2 > for Normal \n");
	scanf("%d",&display_option);
	if(display_option == 1)
	{
		g_object_set(button, "highlight", TRUE,NULL);
		g_object_set (button, "alignment-type", MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE, NULL);
	}
	else
	{
		g_object_set(button, "highlight", FALSE,NULL);
		g_object_set (button, "alignment-type", MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT, NULL);
	}


	g_print("\n ENTER THE Alignment TYPE \n < 1 > for LEFT < 2 > for MIDDLE < 3 > for RIGHT \n");
	scanf("%d",&display_option);
	if(display_option == 1)
		g_object_set (button, "alignment-type", MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT, NULL);
	else if(display_option == 2)
		g_object_set (button, "alignment-type", MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE, NULL);
	else
		g_object_set (button, "alignment-type", MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT, NULL);

	g_signal_connect(button, "button-toggled",  G_CALLBACK(button_toggled), NULL);

	clutter_actor_show (stage);

	clutter_main();

	return 0;

}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Sample item for MildenhallRoller
 *
 *
 */

#include "sample-item.h"
#include "mildenhall-internal.h"

#include <thornbury/thornbury.h>

#include <liblightwood-roller.h>
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include "liblightwood-glowshader.h"

#include "mildenhall_radio_button.h"

#define ROLLER_FONT(size) "DejaVuSansCondensed " #size "px"
#define ROLLER_ARROW_MARKUP "&#x25B8;"

static ClutterColor const NormalColor = {0x98, 0xA9, 0xB2, 0xFF};

#define WITH_RADIO_BUTTON 1
//static void sample_item_expandable_iface_init (LightwoodExpandableIface *iface);

struct _MildenhallSampleItem
{
  ClutterActor parent;

  ClutterActor *icon;
  ClutterActor *label;
  ClutterActor *arrow_up;
  ClutterActor *arrow_down;
  ClutterActor *arrow_right;
  ClutterActor *extra_separator;
  ClutterActor *vertical_line2;
  ClutterActor *vertical_line1;
  ClutterActor *horizontal;
  ClutterActor *extra_horizontal;

  ClutterActor *pAppTypes[3] ;
  ClutterActor *pRadioButtons[3] ;
  ClutterActor *pSelectIcon ;
  guint uinSelect;
  gboolean bSignalConnected;

  ThornburyModel *model;
  gchar *text;
  gboolean row_called;
  gboolean expanded;
  ClutterEffect *glow_effect_1;
  ClutterEffect *glow_effect_2;
  ClutterEffect *glow_effect_3;

  ClutterEffect *glow_effect_text[3];
  ClutterEffect *glow_effect_radio[3];
};

G_DEFINE_TYPE (MildenhallSampleItem, mildenhall_sample_item, CLUTTER_TYPE_ACTOR);


enum
{
  PROP_0,

  PROP_ICON,
  PROP_LABEL,
  PROP_ARROW_UP,
  PROP_ARROW_DOWN,
  PROP_MODEL,
  PROP_ROW,
  PROP_FOCUSED,
  PROP_APP_TYPE,
  PROP_APP_TYPE_SELECTED ,
};

static void
sample_item_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  MildenhallSampleItem *item = MILDENHALL_SAMPLE_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
      break;
    case PROP_LABEL:
      g_value_set_string (value,
          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    case PROP_APP_TYPE_SELECTED:
    {
		guint uinSelectAppType = -1 ;
		guint inCount = 0 ;
#if WITH_RADIO_BUTTON
		for( ; inCount < 3 ; inCount++)
		{
			if(NULL != item->pRadioButtons[inCount])
			{
				if( TRUE == mildenhall_radio_button_get_current_state(MILDENHALL_RADIO_BUTTON(item->pRadioButtons[inCount])))
				{
					uinSelectAppType = inCount ;
					break ;
				}
			}
		}
#endif
    	g_value_set_uint(value , uinSelectAppType);
    }
    break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void on_notify_language ( GObject    *object,
                                 GParamSpec *pspec,
                                 gpointer    user_data)
{
	MildenhallSampleItem *item = MILDENHALL_SAMPLE_ITEM (user_data);
  clutter_text_set_text(CLUTTER_TEXT (item->label),gettext(item->text));
}



static ClutterActor *
p_sample_item_create_text (MildenhallSampleItem *pItem, const gchar* pFont)
{
	ClutterActor  *pInfoLabel  = clutter_text_new();
	clutter_text_set_single_line_mode (CLUTTER_TEXT (pInfoLabel), TRUE);
	clutter_text_set_color(CLUTTER_TEXT(pInfoLabel),&NormalColor);
	clutter_text_set_font_name(CLUTTER_TEXT(pInfoLabel),pFont);
	clutter_actor_add_child(CLUTTER_ACTOR(pItem),	pInfoLabel);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pInfoLabel),PANGO_ELLIPSIZE_END);
	return pInfoLabel ;

}



static ClutterActor *
p_sample_item_create_radio_buttons (MildenhallSampleItem *pItem, gpointer pButtonId)
{


	ClutterActor *pButton = mildenhall_radio_button_new();
	clutter_actor_insert_child_above (CLUTTER_ACTOR(pItem) ,pButton ,NULL);
	/* Group ID should not be null */
	g_object_set(pButton,"width",30.0,"height",30.0,"reactive", FALSE, "group-id", g_strdup_printf("%p", pItem),
			"current-state",FALSE, "button-id" ,pButtonId ,  NULL);
	return pButton ;

}



static void
sample_item_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
	MildenhallSampleItem *item = MILDENHALL_SAMPLE_ITEM (object);
	//clutter_actor_hide(item->arrow_right);

	switch (property_id)
	{
		case PROP_ICON:
			{
                                thornbury_ui_texture_set_from_file (item->icon, g_value_get_string(value), 0,0,FALSE,TRUE);
                                clutter_actor_show(item->icon);
                        }
			break;
		case PROP_LABEL:
			clutter_text_set_text (CLUTTER_TEXT (item->label),
					gettext((gchar *)g_value_get_string (value)));
			item->text=g_strdup(g_value_get_string (value));

			if(NULL != item->pAppTypes[0])
			{
				gfloat fltX = clutter_actor_get_x(item->pAppTypes[0]);
				gfloat fltIconX = clutter_actor_get_x(item->label);
				clutter_actor_set_width(CLUTTER_ACTOR(item->label) ,(fltX - fltIconX ));
			}


			break;
		case PROP_ARROW_UP:
			if (g_value_get_int (value) == ARROW_HIDDEN)
				clutter_actor_hide (item->arrow_up);
			else
			{
				if (g_value_get_int (value) == ARROW_WHITE)
					clutter_actor_set_opacity (item->arrow_up, 255);
				else
					clutter_actor_set_opacity (item->arrow_up, 180);

				clutter_actor_show (item->arrow_up);
			}
			break;
		case PROP_ARROW_DOWN:
			if (g_value_get_int (value) == ARROW_HIDDEN)
				clutter_actor_hide (item->arrow_down);
			else
			{
				if (g_value_get_int (value) == ARROW_WHITE)
					clutter_actor_set_opacity (item->arrow_down, 255);
				else
					clutter_actor_set_opacity (item->arrow_down, 180);

				clutter_actor_show (item->arrow_down);
			}
			break;
		case PROP_MODEL:
			item->model = g_value_get_object (value);
			break;
		case PROP_ROW:
			{
				ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));
				if(FALSE == item->row_called)
								{
									 g_signal_connect ((parent),
									                    "notify::language",
									                    G_CALLBACK (on_notify_language),
									                    item);
								}

									item->row_called=TRUE;
				if(parent != NULL)
				{
					ThornburyModel * model = lightwood_roller_get_model(LIGHTWOOD_ROLLER(parent));
					gfloat roller_width;
					//g_print("&&&&&&\n");
						clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent),-1,NULL,&roller_width);
						clutter_actor_set_position (item->vertical_line1, roller_width - 7, 0);
						//clutter_actor_show (item->vertical_line1);
						//clutter_actor_set_position (item->vertical_line2, roller_width-2, 0);
						//clutter_actor_show (item->vertical_line2);

					clutter_actor_set_size(item->horizontal,(guint)roller_width - 73,1);
					clutter_actor_show(item->horizontal);

					if (parent != NULL &&
							model != NULL &&
							g_value_get_uint (value) == thornbury_model_get_n_rows (model) - 1)
					{
						clutter_actor_set_size(item->extra_horizontal,(guint)roller_width - 73,1);
						clutter_actor_show (item->extra_separator);
					}
					else
						clutter_actor_hide (item->extra_separator);
				}
				break;
			}
		case PROP_FOCUSED:
			{
				//ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));
				//gfloat roller_width;
				//clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent),-1,NULL,&roller_width);
				//clutter_actor_set_position (item->arrow_right, roller_width - 25,10 );
#if 1
				if (g_value_get_boolean (value))
				{
					//MildenhallSampleItemClass *klass = MILDENHALL_SAMPLE_ITEM_GET_CLASS (object);
					//clutter_actor_show(item->arrow_right);
#if 1
                                        guint inCount = 0;
					if (clutter_actor_get_effect (item->icon, "glow") != NULL)
						return;
#endif

					clutter_actor_add_effect_with_name (item->icon, "glow", item->glow_effect_1);
					clutter_actor_add_effect_with_name (item->label, "glow", item->glow_effect_2);


					for( ; inCount < 3 ; inCount++)
					{
						if(  NULL != item->pAppTypes[inCount])
						{
							if (clutter_actor_get_effect (item->pAppTypes[inCount] , "glow") == NULL)
							{
								clutter_actor_add_effect_with_name (item->pAppTypes[inCount], "glow", item->glow_effect_text[inCount]);
							}
#if WITH_RADIO_BUTTON
							if(NULL != item->pRadioButtons[inCount])
							{
								if (clutter_actor_get_effect (item->pRadioButtons[inCount] , "glow") == NULL)
								{
									clutter_actor_add_effect_with_name (item->pRadioButtons[inCount], "glow", item->glow_effect_radio[inCount]);
								}
							}
							g_object_set(G_OBJECT(item->pRadioButtons[inCount]), "reactive", FALSE, NULL);
#else
							if(NULL != item->pSelectIcon)
							{
								if (clutter_actor_get_effect (item->pSelectIcon , "glow") == NULL)
								{
									clutter_actor_add_effect_with_name (item->pSelectIcon, "glow", item->glow_effect_radio[0]);
								}
							}
#endif
						}
					}

				}
				else
				{
					//clutter_actor_hide(item->arrow_right);
                                        guint inCount = 0;
					if (clutter_actor_get_effect (item->icon, "glow") == NULL)
						return;

					clutter_actor_remove_effect_by_name (item->icon, "glow");
					clutter_actor_remove_effect_by_name (item->label, "glow");


					for( ; inCount < 3 ; inCount++)
					{
						if(  NULL != item->pAppTypes[inCount])
						{
							if (clutter_actor_get_effect (item->pAppTypes[inCount] , "glow") != NULL)
							{
								clutter_actor_remove_effect_by_name (item->pAppTypes[inCount], "glow");
							}
#if WITH_RADIO_BUTTON
							if(NULL != item->pRadioButtons[inCount])
							{
								if (clutter_actor_get_effect (item->pRadioButtons[inCount] , "glow") != NULL)
								{
									clutter_actor_remove_effect_by_name (item->pRadioButtons[inCount], "glow");
								}
							}
							g_object_set(G_OBJECT(item->pRadioButtons[inCount]), "reactive", FALSE, NULL);
#else
							if(NULL != item->pSelectIcon)
							{
								if (clutter_actor_get_effect (item->pSelectIcon , "glow") != NULL)
								{
									clutter_actor_remove_effect_by_name (item->pSelectIcon, "glow");
								}
							}
#endif
						}
					}

				}
#endif
			}
			break;

		case PROP_APP_TYPE :
		{
			gchar **pAppTypes = g_value_get_pointer(value);

			if(NULL != pAppTypes )
			{
				guint inCount =  0 ;
				for( ;NULL != pAppTypes[inCount]  && inCount < 3 ;inCount++ )
				{
					if(NULL == item->pAppTypes[inCount])
					{
						item->pAppTypes[inCount] =p_sample_item_create_text(item , ROLLER_FONT(18));
						clutter_actor_set_size(item->pAppTypes[inCount] ,64,40);
						clutter_actor_set_position(item->pAppTypes[inCount],(445 + (inCount*50)) ,15 );
						item->glow_effect_text[inCount] = g_object_ref_sink (lightwood_glow_shader_new ());
#if WITH_RADIO_BUTTON
						item->pRadioButtons[inCount]  = p_sample_item_create_radio_buttons(item , GUINT_TO_POINTER(inCount));
						clutter_actor_set_position(item->pRadioButtons[inCount] , (445 + (inCount*50)) ,30);
						item->glow_effect_radio[inCount] = g_object_ref_sink (lightwood_glow_shader_new ());
//						g_signal_connect(item->pRadioButtons[inCount] , "state-change",  G_CALLBACK(sample_item_radio_button_toggled), item);
#endif
					}
					clutter_text_set_text(	CLUTTER_TEXT(item->pAppTypes[inCount]),	gettext(pAppTypes[inCount] ));
				}
#if WITH_RADIO_BUTTON
				mildenhall_radio_button_set_current_state(MILDENHALL_RADIO_BUTTON(item->pRadioButtons[item->uinSelect]),TRUE);
#else
				if(NULL ==item->pSelectIcon)
					clutter_actor_set_position (item->pSelectIcon , (445 + (item->uinSelect *50)), ( 35));
#endif

				if(NULL != item->pAppTypes[0] && NULL == item->label )
				{
					gfloat fltX = clutter_actor_get_x(item->pAppTypes[0]);
					gfloat fltIconX = clutter_actor_get_x(item->label);
					clutter_actor_set_width(CLUTTER_ACTOR(item->label) ,(fltX - fltIconX ));
				}

			}
		}
		break;
		case PROP_APP_TYPE_SELECTED :
		{
			guint uinSelectAppType = g_value_get_uint (value) ;
			g_autofree gchar *select_icon_path = g_build_filename (
			    _mildenhall_get_theme_path (),
			    "icon_selected.png",
			    NULL);

			if(uinSelectAppType < 3)
			{
				item->uinSelect = uinSelectAppType;
#if WITH_RADIO_BUTTON

				if(NULL != item->pRadioButtons[uinSelectAppType])
				{
					mildenhall_radio_button_set_current_state(MILDENHALL_RADIO_BUTTON(item->pRadioButtons[uinSelectAppType]),TRUE);
				}

#else
				if(NULL ==item->pSelectIcon)
				{
					item->pSelectIcon = thornbury_ui_texture_create_new (
					    select_icon_path, 20, 20, FALSE, FALSE);

					clutter_actor_add_child(CLUTTER_ACTOR(item), item->pSelectIcon );
					item->glow_effect_radio[0] = g_object_ref_sink (lightwood_glow_shader_new ());
				}
				clutter_actor_set_position (item->pSelectIcon , (445 + (uinSelectAppType *50)), ( 35));
#endif
			}

		}
		break;


		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
sample_item_dispose (GObject *obj)
{
  MildenhallSampleItem *item = MILDENHALL_SAMPLE_ITEM (obj);
  gsize i;

  g_clear_object (&item->glow_effect_1);
  g_clear_object (&item->glow_effect_2);

  for (i = 0; i < 3; i++)
    {
      g_clear_object (&item->glow_effect_text[i]);
      g_clear_object (&item->glow_effect_radio[i]);
    }

  G_OBJECT_CLASS (mildenhall_sample_item_parent_class)->dispose (obj);
}

/* LightwoodExpandable implementation */
#if 0

static void
sample_item_set_expanded (LightwoodExpandable *expandable,
                          gboolean       expanded,
                          gboolean       animated)
{
  if (expanded == MILDENHALL_SAMPLE_ITEM (expandable)->expanded)
    return;

  if (expanded)
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 100.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), 100);
    }
  else
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 64.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), -1);
    }

  MILDENHALL_SAMPLE_ITEM (expandable)->expanded = expanded;
}


static gboolean
button_release_event (ClutterActor       *actor,
                      ClutterButtonEvent *event)
{
  ClutterActor *parent = clutter_actor_get_parent (actor);

  g_return_val_if_fail (LIGHTWOOD_IS_FIXED_ROLLER (parent), FALSE);

  g_debug ("actor '%s' has been activated, (un)expanding",
           clutter_actor_get_name (actor));

  if (MILDENHALL_SAMPLE_ITEM (actor)->expanded)
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), NULL);
  else
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), actor);

  return FALSE;
}
static void
actor_allocate (ClutterActor           *actor,
                const ClutterActorBox  *box,
                ClutterAllocationFlags  flags)
{
	CLUTTER_ACTOR_CLASS (sample_item_parent_class)->allocate (actor, box, flags);
}
static void
actor_paint (ClutterActor *actor)
{
	//MildenhallSampleItem *sample_item = MILDENHALL_SAMPLE_ITEM(actor);
	//if(sample_item->vertical_line1)
	//	clutter_actor_paint(sample_item->vertical_line1);
	//if(sample_item->vertical_line2)
	//	clutter_actor_paint(sample_item->vertical_line2);
	CLUTTER_ACTOR_CLASS (sample_item_parent_class)->paint (actor);
}
static void
sample_item_expandable_iface_init (LightwoodExpandableIface *iface)
{
  //iface->set_expanded = sample_item_set_expanded;
}
#endif


static void
mildenhall_sample_item_class_init (MildenhallSampleItemClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	//ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
	GParamSpec *pspec;

	object_class->get_property = sample_item_get_property;
	object_class->set_property = sample_item_set_property;
	object_class->dispose = sample_item_dispose;

	//actor_class->paint = actor_paint;
	//actor_class->allocate = actor_allocate;

	//actor_class->button_release_event = button_release_event;

	pspec = g_param_spec_string ("icon",
			"icon",
			"COGL texture handle for the icon",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ICON, pspec);

	pspec = g_param_spec_string ("label",
			"label",
			"Text for the label",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_LABEL, pspec);

	pspec = g_param_spec_int ("arrow-up",
			"arrow up",
			"Controls the display of the upwards arrow",
			ARROW_WHITE,
			ARROW_LAST,
			ARROW_HIDDEN,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_ARROW_UP, pspec);

	pspec = g_param_spec_int ("arrow-down",
			"arrow down",
			"Controls the display of the downwards arrow",
			ARROW_WHITE,
			ARROW_LAST,
			ARROW_HIDDEN,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_ARROW_DOWN, pspec);

	pspec = g_param_spec_object ("model",
			"ThornburyModel",
			"ThornburyModel",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MODEL, pspec);

	pspec = g_param_spec_uint ("row",
			"row number",
			"row number",
			0, G_MAXUINT,
			0,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_ROW, pspec);

	pspec = g_param_spec_boolean ("focused",
			"focused",
			"Whether this actor should be rendered as focused",
			FALSE,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_FOCUSED, pspec);


	/* appstore settings  specific */
	/* call this property to have radio buttons */
		pspec = g_param_spec_pointer ("app-type",
				"array of info regarding app type",
				"array of gchar * having info regarding app type[ release devel both]",
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_APP_TYPE, pspec);


		pspec = g_param_spec_uint ("select-app-type",
				"select-app-type",
				"select-app-type can be used to show the selected app type",
				0, G_MAXUINT,
				0,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_APP_TYPE_SELECTED, pspec);



	//klass->glow_effect_3 = g_object_ref_sink (lightwood_glow_shader_new ());
}
static ClutterActor *
DrawVerticalSeperator(ClutterColor *lineColor,ClutterColor *depthLineColor,gfloat width,gfloat height)
{
	/*Create a separator line group */
        ClutterActor *separatorLineGroup = clutter_actor_new ();

        /*The main separator line */
        ClutterActor *separatorLine = clutter_actor_new();//clutter_rectangle_new_with_color (lineColor);
	clutter_actor_set_background_color(separatorLine,lineColor);
        clutter_actor_set_position (separatorLine, 0, 0);
        clutter_actor_set_size (separatorLine, width, height);
		clutter_actor_add_child(CLUTTER_ACTOR(separatorLineGroup),separatorLine);
        //clutter_container_add_actor (CLUTTER_CONTAINER (separatorLineGroup),separatorLine);

        /*Second line for the depth effect */
        if(NULL != depthLineColor)
	{
		ClutterActor *separatorLine2 = clutter_actor_new();
		clutter_actor_set_background_color(separatorLine,depthLineColor);//clutter_rectangle_new_with_color (depthLineColor);
		clutter_actor_set_position (separatorLine2, 1, 0);
		clutter_actor_set_size (separatorLine2, width, height);
		//clutter_container_add_actor (CLUTTER_CONTAINER (separatorLineGroup),separatorLine2);
		clutter_actor_add_child(CLUTTER_ACTOR(separatorLineGroup),separatorLine2);
	}


        /*Return the separator line group that gets addded to the roller background */
        return separatorLineGroup;
}

static void
mildenhall_sample_item_init (MildenhallSampleItem *sample_item)
{
	g_autofree gchar *arrow_up_file = NULL;
	g_autofree gchar *arrow_down_file = NULL;

	ClutterActor *line;
	ClutterColor text_color = {0x98,0XA9,0XB2,0XFF};// #98A9B2FF
	ClutterColor line_horizon_color = {0xFF, 0xFF, 0xFF, 0x33};
	ClutterColor line_color = {0x00, 0x00, 0x00, 0x66};
	ClutterColor depthLineColor = {0xFF, 0xFF, 0xFF, 0x33};
        ClutterActor *sepLine = NULL;

	gint inLoop = 0;
	while(inLoop < 3)
	{
	sample_item->pAppTypes[inLoop] = NULL ;
	sample_item->pRadioButtons[inLoop] = NULL ;
	inLoop++;
	}
	sample_item->pSelectIcon = NULL ;
	sample_item->uinSelect = 2 ;
	sample_item->bSignalConnected = FALSE ;

	sample_item->row_called=FALSE;

	arrow_up_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "arrow-up.png",
	    NULL);

	sample_item->icon = thornbury_ui_texture_create_new (arrow_up_file, 0, 0, FALSE, FALSE);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->icon);
	clutter_actor_set_position (sample_item->icon, 18, 14);
	clutter_actor_hide (sample_item->icon);

	sample_item->arrow_up = thornbury_ui_texture_create_new (arrow_up_file, 0, 0, FALSE, FALSE);
	clutter_actor_set_position (sample_item->arrow_up, 34, 10);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->arrow_up);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->arrow_up);
	clutter_actor_hide (sample_item->arrow_up);

	arrow_down_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "arrow-down.png",
	    NULL);

	sample_item->arrow_down = thornbury_ui_texture_create_new (arrow_down_file, 0, 0, FALSE, FALSE);
	clutter_actor_set_position (sample_item->arrow_down, 34, 53);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->arrow_down);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->arrow_down);
	clutter_actor_hide (sample_item->arrow_down);

	sample_item->label = clutter_text_new ();
	clutter_text_set_color (CLUTTER_TEXT (sample_item->label), &text_color);
	clutter_text_set_font_name (CLUTTER_TEXT (sample_item->label), ROLLER_FONT(28));
	clutter_actor_set_position (sample_item->label, 79, 15);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->label);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->label);
	clutter_text_set_ellipsize(CLUTTER_TEXT(sample_item->label),PANGO_ELLIPSIZE_END);
	clutter_actor_show (sample_item->label);

	/* Vertical separators */
	sepLine = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
	clutter_actor_set_position (sepLine,6,0);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sepLine);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sepLine);
	clutter_actor_show(sepLine);

	sepLine = DrawVerticalSeperator(&line_color,&depthLineColor,1,64);
	clutter_actor_set_position (sepLine,64,0);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sepLine);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sepLine);
	clutter_actor_show(sepLine);

	sample_item->vertical_line1 = DrawVerticalSeperator(&line_color,&depthLineColor,1,64);
        //clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->vertical_line1);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->vertical_line1);

	//sample_item->vertical_line2 = DrawVerticalSeperator(&line_color,&depthLineColor,1,64);
  //      clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->vertical_line2);

	/* Horizontal separators */
	line = clutter_actor_new();//clutter_rectangle_new_with_color (lineColor);
        clutter_actor_set_background_color(line,&line_horizon_color);//clutter_rectangle_new_with_color (&line_horizon_color);
	clutter_actor_set_size (line, 58, 1);
	clutter_actor_set_position (line, 7, 1);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), line);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), line);
	clutter_actor_show (line);

	sample_item->horizontal = clutter_actor_new();//clutter_rectangle_new_with_color (lineColor);
        clutter_actor_set_background_color(sample_item->horizontal,&line_horizon_color);//clutter_rectangle_new_with_color (&line_horizon_color);
	//clutter_actor_set_size (sample_item->horizontal, 709, 1);
	clutter_actor_set_position (sample_item->horizontal, 66, 1);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->horizontal);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->horizontal);
	clutter_actor_show (sample_item->horizontal);

	/* Extra horizontal separators */
	sample_item->extra_separator = clutter_actor_new ();
	clutter_actor_set_position (sample_item->extra_separator, 0, 63);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item),
	//		sample_item->extra_separator);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item), sample_item->extra_separator);

	line = clutter_actor_new();//clutter_rectangle_new_with_color (lineColor);
        clutter_actor_set_background_color(line,&line_horizon_color);//clutter_rectangle_new_with_color (&line_horizon_color);
	clutter_actor_set_size (line, 58, 1);
	clutter_actor_set_position (line, 7, 0);
	//clutter_container_add_actor (CLUTTER_CONTAINER (sample_item->extra_separator),
	//		line);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item->extra_separator), line);
	clutter_actor_show (line);

	sample_item->extra_horizontal = clutter_actor_new();//clutter_rectangle_new_with_color (lineColor);
        clutter_actor_set_background_color(sample_item->extra_horizontal,&line_horizon_color);//clutter_rectangle_new_with_color (&line_horizon_color);
        //clutter_actor_set_size (horizontal, 709, 1);
        clutter_actor_set_position (sample_item->extra_horizontal, 66, 0);
        //clutter_container_add_actor (CLUTTER_CONTAINER (sample_item->extra_separator), sample_item->extra_horizontal);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_item->extra_separator), sample_item->extra_horizontal);
        clutter_actor_show (sample_item->extra_horizontal);

	clutter_actor_set_reactive (CLUTTER_ACTOR (sample_item), TRUE);
	sample_item->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
	sample_item->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());
}

ClutterActor*
mildenhall_sample_item_new (void)
{
    return g_object_new (MILDENHALL_TYPE_SAMPLE_ITEM, NULL);
}

#ifndef MILDENHALL_DISABLE_DEPRECATED

ClutterActor*
sample_item_new (void)
{
    return mildenhall_sample_item_new ();
}

GType
sample_item_get_type (void)
{
    return mildenhall_sample_item_get_type ();
}

#endif /* MILDENHALL_DISABLE_DEPRECATED */

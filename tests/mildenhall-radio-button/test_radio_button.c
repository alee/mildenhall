/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* test_radio_button.c
 *
 * Created on: Dec 17, 2012
 *
 * test_radio_button.c */

#include "mildenhall_radio_button.h"

#include <thornbury/thornbury.h>

#define MILDENHALL_TEST_RADIO_BUTTON_PRINT( a ...)			g_print(a);

/* button state changed callback, if multi state */
static void button_toggled (GObject *button, gboolean bNewState, gpointer *pButtonId, gpointer *user_data)
{

        ClutterActor *text = CLUTTER_ACTOR (user_data);

	if(bNewState == TRUE)
		g_print("MILDENHALL_TEST_RADIO_BUTTON_PRINT: new state = TRUE , \t Button-Id %s\n",
				(gchar*)pButtonId );
	else
		g_print("MILDENHALL_TEST_RADIO_BUTTON_PRINT: new state = FALSE , \t Button-Id %s\n",
				(gchar*)pButtonId );

		g_object_set(button, "highlight", TRUE ,NULL);

	if (g_strcmp0 (pButtonId, "button-1") == 0)
		clutter_text_set_text (CLUTTER_TEXT (text), "You selected option ONE");
	else if (g_strcmp0 (pButtonId, "button-2") == 0)
		clutter_text_set_text (CLUTTER_TEXT (text), "You selected option TWO");
	else if (g_strcmp0 (pButtonId, "button-3") == 0)
		clutter_text_set_text (CLUTTER_TEXT (text), "You selected option THREE");
}

ClutterActor
*radio_text (gchar *text, gfloat x, gfloat y, ClutterActor *stage)
{
	ClutterActor *radio_text = clutter_text_new ();
	clutter_text_set_text (CLUTTER_TEXT (radio_text), text);
	clutter_actor_set_position (radio_text, x, y);
	clutter_actor_add_child (stage, radio_text);
	clutter_actor_show (radio_text);
	clutter_text_set_color (CLUTTER_TEXT (radio_text), CLUTTER_COLOR_White);
	clutter_text_set_font_name (CLUTTER_TEXT (radio_text), "Serif 14px");
}

int main (int argc, char **argv)
{
        ClutterActor *text;
        MildenhallRadioButton *radio3;
        ClutterActor *button3;
	MILDENHALL_TEST_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);
	MILDENHALL_TEST_RADIO_BUTTON_PRINT("second time %s \n",__FUNCTION__);
	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;

	ClutterActor *stage;
	ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

	stage = clutter_stage_new ();
	clutter_actor_set_background_color (stage, &black);
	clutter_actor_set_size (stage, 728, 480);
	g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	text = clutter_text_new ();
	clutter_text_set_text (CLUTTER_TEXT (text), "select your option:");
	clutter_text_set_font_name (CLUTTER_TEXT (text), "Serif 16px");
	clutter_text_set_color (CLUTTER_TEXT (text), CLUTTER_COLOR_Red);
	clutter_actor_set_position (text, 10, 10);
	clutter_actor_add_child (stage, text);
	clutter_actor_show (text);

	/************************************* Group 1 Button 1  ******/

	ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props
			(MILDENHALL_TYPE_RADIO_BUTTON, PKGDATADIR"/mildenhall_radio_button_prop.json");
	GObject *pObject = NULL;

	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallRadioButton *radio = MILDENHALL_RADIO_BUTTON(pObject);
	ClutterActor *button = CLUTTER_ACTOR(radio);
	clutter_actor_add_child(stage, button);
	/* test Resizing of the button */
	g_object_set(button, "width", 50.0,NULL);
	g_object_set(button, "height", 50.0,NULL);
        g_object_set (button, "x", 0.0, "y", 50.0, NULL);

	/* assign group id */
	g_object_set(button, "group-id", "test",NULL);
	g_object_set(button, "button-id", "button-1",NULL);
	//mildenhall_radio_button_set_current_state(MILDENHALL_RADIO_BUTTON(button),TRUE);

	radio_text ("one", 50.0, 70.0, stage);
	g_signal_connect (button, "state-change",  G_CALLBACK (button_toggled), text);

	/************************************* Group 1 Button 2  ********/

	ThornburyItemFactory *itemFactory2 = thornbury_item_factory_generate_widget_with_props
			(MILDENHALL_TYPE_RADIO_BUTTON, PKGDATADIR"/test-radio-button/mildenhall_radio_button_2_prop.json");
	GObject *pObject2 = NULL;

	g_object_get(itemFactory2, "object", &pObject2, NULL);
	MildenhallRadioButton *radio2 = MILDENHALL_RADIO_BUTTON(pObject2);
	ClutterActor *button2 = CLUTTER_ACTOR(radio2);
	clutter_actor_add_child(stage, button2);
	g_object_set(button2, "width", 50.0,NULL);
	g_object_set(button2, "height", 50.0,NULL);
	g_object_set(button2, "group-id", "test",NULL);
	g_object_set(button2, "button-id", "button-2",NULL);
	//mildenhall_radio_button_set_current_state(MILDENHALL_RADIO_BUTTON(button2),TRUE);
	radio_text ("two", 50.0, 120.0, stage);
	g_signal_connect (button2, "state-change", G_CALLBACK (button_toggled), text);



	/************************************* Group 1 Button 3  ******/

	ThornburyItemFactory *itemFactory3 = thornbury_item_factory_generate_widget_with_props
			(MILDENHALL_TYPE_RADIO_BUTTON, PKGDATADIR"/test-radio-button/mildenhall_radio_button_3_prop.json");
	GObject *pObject3 = NULL;

	g_object_get (itemFactory3, "object", &pObject3, NULL);
	radio3 = MILDENHALL_RADIO_BUTTON (pObject3);
	button3 = CLUTTER_ACTOR (radio3);
	clutter_actor_add_child (stage, button3);
        g_object_set (button3, "width", 50.0, NULL);
        g_object_set (button3, "height", 50.0, NULL);
        g_object_set (button3, "x", 0.0, "y", 150.0, NULL);
	g_object_set (button3, "group-id", "test", NULL);
	g_object_set (button3, "button-id", "button-3", NULL);

	radio_text ("three", 50.0, 170.0, stage);
	g_signal_connect (button3, "state-change", G_CALLBACK (button_toggled), text);












	clutter_actor_show (stage);

	clutter_main();
	return 0;

}

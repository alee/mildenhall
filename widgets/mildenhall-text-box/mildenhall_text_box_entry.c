/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_text_box_entry
 * @title: MildenhallTextBoxEntry
 * @short_description: A text entry widget implementing LightwoodTextBox.
 * @see_also: #ClutterActor, #ThornburyItemFactory
 *
 * #MildenhallTextBoxEntry supports basic formatting of displayed text, password entry,
 * with entered characters converted to an asterisk.
 * Allow editing several lines of text if its multi-line property is turned on.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * MildenhallTextBoxEntry *text_box = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 *      MILDENHALL_TYPE_TEXT_BOX_ENTRY,
 *      "/usr/share/mildenhall/mildenhall_speller_text_box_prop.json");
 * g_object_get (item_factory, "object", &object, NULL);
 *
 * text_box = MILDENHALL_TEXT_BOX_ENTRY (object);
 * clutter_add_child (stage, CLUTTER_ACTOR (text_box));
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include "mildenhall_text_box_entry.h"

#include "mildenhall_scroller.h"

#define MILDENHALL_TEXTBOX_ENTRY_PRINT(...) //g_print(__VA_ARGS__)

G_DEFINE_TYPE(MildenhallTextBoxEntry, mildenhall_text_box_entry, LIGHTWOOD_TYPE_TEXT_BOX)

#define TEXT_BOX_ENTRY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_TEXT_BOX_ENTRY, MildenhallTextBoxEntryPrivate))

/* private structure members for the mildenhall textbox object */
struct _MildenhallTextBoxEntryPrivate
{
	ClutterActor *scroller;
	ThornburyModel *pModel;
	gboolean isMultiline;
	gint inMaxLines;
	gfloat flWidth;
};

enum Properties
{
  PROP_ENUM_MODEL = 1,
};

enum _enTextBoxSignals
{
	SIG_TEXTBOX_FIRST_SIGNAL,
	SIG_TEXTBOX_SCROLLER_STATUS,
	SIG_TEXTBOX_LAST_SIGNAL
};

/* Storage for the signals */
static guint32 textbox_signals[SIG_TEXTBOX_LAST_SIGNAL] = {0,};


static void v_text_box_scroller_create( MildenhallTextBoxEntry *pTextBoxEntry );

#define MAX_LINE_COUNT 15

/******************************************************
 * Function : v_text_box_update_view
 * Description: update the text_box view
 * Parameters: MildenhallTextBoxEntry *
 * Return value: void
 ********************************************************/
static void v_text_box_update_view( MildenhallTextBoxEntry *pTextBoxEntry , ThornburyModelIter *pIter)
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" %s\n", __FUNCTION__);
	//MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE(pTextBoxEntry);
    if (NULL != pIter )
    {
	GValue value = { 0, };
	thornbury_model_iter_get_value (pIter, 0, &value);
	g_object_set (pTextBoxEntry, "text", g_value_get_string (&value), NULL);
	g_value_unset (&value);
    }
}

/********************************************************
 * Function : v_text_box_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallTextBoxEntry *
 * Return value: void
 ********************************************************/
static void v_text_box_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallTextBoxEntry *pTextBoxEntry)
{
	MILDENHALL_TEXTBOX_ENTRY_PRINT(" %s\n", __FUNCTION__);
}

/********************************************************
 * Function : v_text_box_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallTextBoxEntry *
 * Return value: void
 ********************************************************/
static void v_text_box_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallTextBoxEntry *pTextBoxEntry)
{
	MILDENHALL_TEXTBOX_ENTRY_PRINT("%s\n", __FUNCTION__);
	/* update view */
	v_text_box_update_view(pTextBoxEntry,pIter);

}

/********************************************************
 * Function : v_text_box_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallTextBoxEntry *
 * Return value: void
 ********************************************************/
static void v_text_box_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallTextBoxEntry *pTextBoxEntry)
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
        /* this callback gives the actual row count before removal, and not after removal of row */
        gint inRemovedRow = thornbury_model_iter_get_row (pIter);

	MILDENHALL_TEXTBOX_ENTRY_PRINT(" %s\n", __FUNCTION__);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && thornbury_model_get_n_rows(priv->pModel) == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
}

/********************************************************
 * Function : p_mildenhall_text_box_get_model
 * Description: function returns the model
 * Parameters:  MildenhallTextBoxEntry *
 * Return value: ThornburyModel
 ********************************************************/
ThornburyModel *p_mildenhall_text_box_get_model( MildenhallTextBoxEntry *pTextBoxEntry )
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( " \n %s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return NULL;
	}

	return priv->pModel;
}

/********************************************************
 * Function : v_text_box_set_model
 * Description: set a model property
 * Parameters: MildenhallTextBoxEntry,ThornburyModel
 * Return value: void
 ********************************************************/
static void v_text_box_set_model( MildenhallTextBoxEntry *pTextBoxEntry, ThornburyModel *pModel )
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( " \n %s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return;
	}

    if(NULL != priv->pModel)
    {
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_text_box_row_added_cb),   pTextBoxEntry);
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_text_box_row_changed_cb), pTextBoxEntry);
        g_signal_handlers_disconnect_by_func (priv->pModel, G_CALLBACK (v_text_box_row_removed_cb), pTextBoxEntry);
        g_object_unref (priv->pModel);
        priv->pModel = NULL;
    }
    /* update the new model with signals */
	if ( NULL != pModel)
    {
        g_return_if_fail (G_IS_OBJECT (pModel));
        priv->pModel = g_object_ref(pModel);

        g_signal_connect (priv->pModel, "row-added",   G_CALLBACK (v_text_box_row_added_cb),   pTextBoxEntry );
        g_signal_connect (priv->pModel, "row-changed", G_CALLBACK (v_text_box_row_changed_cb), pTextBoxEntry );
        g_signal_connect (priv->pModel, "row-removed", G_CALLBACK (v_text_box_row_removed_cb), pTextBoxEntry );
    }
    if (G_IS_OBJECT(priv->pModel) && thornbury_model_get_n_rows ( priv->pModel) > 0 )
    {
        ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->pModel, 0 );
        v_text_box_update_view(pTextBoxEntry,pIter);
    }
    g_object_notify (G_OBJECT(pTextBoxEntry), "model");
}


/********************************************************
 * Function : mildenhall_text_box_entry_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_get_property( GObject *pObject, guint inProperty_id, GValue *pValue, GParamSpec *pPspec )
{
        MildenhallTextBoxEntry *pTextBoxEntry = MILDENHALL_TEXT_BOX_ENTRY (pObject);
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( " \n %s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pObject ) )
	{
		g_warning( "invalid instance\n" );
		return;
	}

	switch( inProperty_id )
	{
        case PROP_ENUM_MODEL:
        {
            g_value_set_object( pValue, p_mildenhall_text_box_get_model( pTextBoxEntry ));
            break;
        }
		default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID( pObject, inProperty_id, pPspec );
                break;
	}
}

/********************************************************
 * Function : mildenhall_text_box_entry_set_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_set_property( GObject *pObject, guint inProperty_id, const GValue *pValue, GParamSpec *pPspec )
{
        MildenhallTextBoxEntry *pTextBoxEntry = MILDENHALL_TEXT_BOX_ENTRY (pObject);
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( " \n %s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pObject ) )
	{
		g_warning( "invalid instance\n" );
		return;
	}

	switch( inProperty_id )
	{
        case PROP_ENUM_MODEL:
        {
            v_text_box_set_model( pTextBoxEntry, g_value_get_object( pValue ) );
                break;
        }
		default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID( pObject, inProperty_id, pPspec );
                break;
	}
}

/********************************************************
 * Function : mildenhall_text_box_entry_dispose
 * Description: Dispose the mildenhall text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_dispose( GObject *pObject )
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
  	G_OBJECT_CLASS(mildenhall_text_box_entry_parent_class)->dispose( pObject );
}

/********************************************************
 * Function : mildenhall_text_box_entry_finalize
 * Description: Finalize the mildenhall text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_finalize( GObject *pObject )
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
    G_OBJECT_CLASS(mildenhall_text_box_entry_parent_class)->finalize( pObject );
}

/*
static gboolean mildenhall_text_box_entry_key_release_event(ClutterActor *actor,ClutterKeyEvent *event)
{
	g_print("mildenhall_text_box_entry_button_release_event \n");
	CLUTTER_ACTOR_CLASS(mildenhall_text_box_entry_parent_class)->key_release_event( actor,event );
	return FALSE;
}
*/

/********************************************************
 * Function : mildenhall_text_box_entry_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_class_init( MildenhallTextBoxEntryClass *pKlass )
{
    GObjectClass *pObjectClass = G_OBJECT_CLASS( pKlass );
    GParamSpec *pPspec = NULL;
    MILDENHALL_TEXTBOX_ENTRY_PRINT (" \n %s \n", __FUNCTION__);

    g_type_class_add_private( pKlass, sizeof( MildenhallTextBoxEntryPrivate ) );

    pObjectClass->get_property = mildenhall_text_box_entry_get_property;
    pObjectClass->set_property = mildenhall_text_box_entry_set_property;
    pObjectClass->dispose      = mildenhall_text_box_entry_dispose;
    pObjectClass->finalize     = mildenhall_text_box_entry_finalize;

	//ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (pKlass);
	//actor_class->key_release_event = mildenhall_text_box_entry_key_release_event;

    /**
     * MildenhallTextBoxEntry:model:
     *
     * to set model to mildenhall textbox
     *
     */
    pPspec = g_param_spec_object( "model",
                                   "model",
                                   "model having button states data",
                                   G_TYPE_OBJECT,
                                   G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, PROP_ENUM_MODEL, pPspec );


    /**
     * MildenhallTextBoxEntry::scroller-status:
     *
     * ::scroller-status is emitted when scroller status changes
     */
    textbox_signals[SIG_TEXTBOX_SCROLLER_STATUS] = g_signal_new ("scroller-status",
                    G_TYPE_FROM_CLASS (pObjectClass),
                    G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (MildenhallTextBoxEntryClass, scroller_status),
                    NULL, NULL,
                    g_cclosure_marshal_generic,
                    G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

}

/********************************************************
 * Function : mildenhall_text_box_get_multi_line
 * Description: whether multi-line is set or not
 * Parameters: text box object reference
 * Return value: TRUE/FALSE
 ********************************************************/
static gboolean mildenhall_text_box_get_multi_line( MildenhallTextBoxEntry *pTextBoxEntry )
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
        gboolean isMultiline;
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( "%s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return FALSE;
	}
    if(NULL != priv)
    {
		g_object_get(pTextBoxEntry, "multi-line", &isMultiline, NULL);
        priv->isMultiline = isMultiline;
    }
    //v_text_box_scroller_create(pTextBoxEntry);
    return isMultiline;
}

/********************************************************
 * Function : mildenhall_text_box_get_displayed_lines
 * Description: to get visible line count
 * Parameters: text box object reference
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_get_entry_width( MildenhallTextBoxEntry *pTextBoxEntry )
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
        gfloat flWidth;
	MILDENHALL_TEXTBOX_ENTRY_PRINT ("%s \n", __FUNCTION__);
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return ;
	}
    if(NULL != priv)
    {
		g_object_get(pTextBoxEntry, "width", &flWidth, NULL);
		priv->flWidth = flWidth;
		if(priv->scroller != NULL)
		{
			clutter_actor_set_position(priv->scroller, priv->flWidth,0);
		}
    }
}

void mildenhall_text_box_get_displayed_lines (MildenhallTextBoxEntry *pTextBoxEntry)
{
        MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( "%s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return ;
	}

	if( ( TRUE == priv->isMultiline) && (-1!= priv->inMaxLines) && (MAX_LINE_COUNT != priv->inMaxLines))
	{
        gint line_count;
	MILDENHALL_TEXTBOX_ENTRY_PRINT ("multiline is set to true and max lines are more than -1 \n");
        g_object_get(pTextBoxEntry, "displayed-lines", &line_count, NULL);

        MILDENHALL_TEXTBOX_ENTRY_PRINT("  priv->inMaxLines = %d , line_count = %d \n",priv->inMaxLines,line_count);

        if( (priv->inMaxLines+1) > line_count)
        {
            clutter_actor_hide(priv->scroller);
            g_object_set(priv->scroller, "displayed-items", line_count, NULL);
			g_signal_emit(pTextBoxEntry, textbox_signals[SIG_TEXTBOX_SCROLLER_STATUS], 0,0, pTextBoxEntry);

        }
        else
        {
            clutter_actor_show(priv->scroller);
            g_object_set(priv->scroller, "displayed-items", priv->inMaxLines, NULL);
			g_signal_emit(pTextBoxEntry, textbox_signals[SIG_TEXTBOX_SCROLLER_STATUS], 0,1, pTextBoxEntry);
        }
        g_object_set(priv->scroller, "total-items", line_count, NULL);
	}
}


/********************************************************
 * Function : mildenhall_text_box_get_max_lines
 * Description: to get max lines count
 * Parameters: text box object reference
 * Return value: max line count
 ********************************************************/
static gboolean mildenhall_text_box_get_max_lines( MildenhallTextBoxEntry *pTextBoxEntry )
{
	MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
	gint inMaxLines;
   	MILDENHALL_TEXTBOX_ENTRY_PRINT( "%s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return FALSE;
	}
    if(NULL != priv)
    {
        g_object_get(pTextBoxEntry, "max-lines", &inMaxLines, NULL);
        priv->inMaxLines = inMaxLines;
    }
    v_text_box_scroller_create(pTextBoxEntry);
    return TRUE;
}

/********************************************************
 * Function : mildenhall_text_box_scrollup_cb
 * Description: callback invoke at scroller up
 * Parameters: text box object reference
 * Return value: gboolean
 ********************************************************/
static gboolean mildenhall_text_box_scrollup_cb(ClutterActor *actor, gpointer user_data)
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
    g_object_set(user_data, "shift-up", TRUE, NULL );
    return TRUE;
}

/********************************************************
 * Function : mildenhall_text_box_scrolldown_cb
 * Description: callback invoke at scroller down
 * Parameters: text box object reference
 * Return value: gboolean
 ********************************************************/
static gboolean mildenhall_text_box_scrolldown_cb(ClutterActor *actor, gpointer user_data)
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
    g_object_set(user_data,"shift-down",TRUE,NULL);
    return TRUE;
}

/********************************************************
 * Function : v_text_box_scroller_create
 * Description: function to create the mildenhall textbox
 * Parameters: text box object reference
 * Return value: void
 ********************************************************/
static void v_text_box_scroller_create(MildenhallTextBoxEntry *pTextBoxEntry)
{
    MildenhallTextBoxEntryPrivate *priv = TEXT_BOX_ENTRY_PRIVATE (pTextBoxEntry);
    MILDENHALL_TEXTBOX_ENTRY_PRINT( " \n %s \n", __FUNCTION__ );
    if( !MILDENHALL_IS_TEXT_BOX_ENTRY( pTextBoxEntry ) )
	{
		g_warning( "invalid instance\n" );
		return;
	}
    if((TRUE == priv->isMultiline) && (-1!= priv->inMaxLines) && (MAX_LINE_COUNT != priv->inMaxLines))
    {
        MILDENHALL_TEXTBOX_ENTRY_PRINT("multiline is set to true and max lines are more than -1 \n");
        if(priv->inMaxLines > 2 )
            priv->scroller = mildenhall_scroller_new(MILDENHALL_SCROLLER_3L);
        else
            priv->scroller = mildenhall_scroller_new(MILDENHALL_SCROLLER_2L);

        clutter_actor_add_child(CLUTTER_ACTOR(pTextBoxEntry), priv->scroller);
        clutter_actor_set_position(priv->scroller,priv->flWidth,0);
        clutter_actor_hide(priv->scroller);
        g_signal_connect(priv->scroller, "scrolled-up"  , G_CALLBACK( mildenhall_text_box_scrollup_cb ), pTextBoxEntry);
        g_signal_connect(priv->scroller, "scrolled-down", G_CALLBACK( mildenhall_text_box_scrolldown_cb ), pTextBoxEntry);
    }
}

/********************************************************
 * Function : mildenhall_text_box_entry_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_text_box_entry_init( MildenhallTextBoxEntry *pSelf )
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
    pSelf->priv = TEXT_BOX_ENTRY_PRIVATE( pSelf );

    pSelf->priv->isMultiline = FALSE;
    pSelf->priv->inMaxLines = -1;
    pSelf->priv->flWidth = 0.0;
    pSelf->priv->scroller = NULL;

    g_signal_connect (pSelf,"notify::multi-line",G_CALLBACK (mildenhall_text_box_get_multi_line),pSelf);
    g_signal_connect (pSelf,"notify::max-lines",G_CALLBACK (mildenhall_text_box_get_max_lines),pSelf);
    g_signal_connect (pSelf,"notify::displayed-lines",G_CALLBACK (mildenhall_text_box_get_displayed_lines),pSelf);
    g_signal_connect (pSelf,"notify::width",G_CALLBACK (mildenhall_text_box_get_entry_width),pSelf);
}

 /**
 * mildenhall_text_box_entry_new:
 *
 * function to create a new mildenhall textbox
 *
 * Returns: (transfer full): textbox object
 *
 */
ClutterActor *mildenhall_text_box_entry_new( void )
{
    MILDENHALL_TEXTBOX_ENTRY_PRINT(" \n %s \n", __FUNCTION__);
    return g_object_new(MILDENHALL_TYPE_TEXT_BOX_ENTRY, NULL);
}

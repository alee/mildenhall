/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora, Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_INTERNAL_H__
#define __MILDENHALL_INTERNAL_H__

#include <glib.h>

G_BEGIN_DECLS

const gchar *_mildenhall_get_theme_path (void);

gchar *_mildenhall_get_resource_path (const gchar *path);

G_END_DECLS

#endif /* __MILDENHALL_INTERNAL_H__ */

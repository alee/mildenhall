/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_toggle_button.h
 *
 * Created on: Dec 08, 2012
 *
 * mildenhall_toggle_button.h */

#ifndef _MILDENHALL_TOGGLE_BUTTON_H
#define _MILDENHALL_TOGGLE_BUTTON_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "liblightwood-button.h"
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

/**
 * MILDENHALL_TYPE_TOGGLE_BUTTON:
 *
 * Get the #GType of the #MildenhallToggleButton
 */
#define MILDENHALL_TYPE_TOGGLE_BUTTON mildenhall_toggle_button_get_type()

/**
 * MILDENHALL_TOGGLE_BUTTON:
 * @obj:	Valid #GObject
 *
 * Get the #MildenhallToggleButton object.
 */

#define MILDENHALL_TOGGLE_BUTTON(obj) \
		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				MILDENHALL_TYPE_TOGGLE_BUTTON, MildenhallToggleButton))
/**
 * MILDENHALL_TOGGLE_BUTTON_CLASS:
 * @klass:	Valid #GObjectClass
 *
 * Get the #MildenhallToggleButtonClass
 */
#define MILDENHALL_TOGGLE_BUTTON_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_CAST ((klass), \
				MILDENHALL_TYPE_TOGGLE_BUTTON, MildenhallToggleButtonClass))

/**
 * MILDENHALL_IS_TOGGLE_BUTTON:
 * @obj:	Valid #GObject
 *
 * Check if the #GObject is of type #MildenhallToggleButton .
 */
#define MILDENHALL_IS_TOGGLE_BUTTON(obj) \
		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
				MILDENHALL_TYPE_TOGGLE_BUTTON))

/**
 * MILDENHALL_IS_TOGGLE_BUTTON_CLASS:
 * @klass:	Valid #GObjectClass
 *
 * Check if the #GObjectClass is of type #MildenhallToggleButtonClass .
 */
#define MILDENHALL_IS_TOGGLE_BUTTON_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_TYPE ((klass), \
				MILDENHALL_TYPE_TOGGLE_BUTTON))

/**
 * MILDENHALL_TOGGLE_BUTTON_GET_CLASS:
 * @obj:	Valid #GObject
 *
 * Get the #MildenhallToggleButtonClass from the #GObject.
 */
#define MILDENHALL_TOGGLE_BUTTON_GET_CLASS(obj) \
		(G_TYPE_INSTANCE_GET_CLASS ((obj), \
				MILDENHALL_TYPE_TOGGLE_BUTTON, MildenhallToggleButtonClass))

typedef struct _MildenhallToggleButton MildenhallToggleButton;
typedef struct _MildenhallToggleButtonClass MildenhallToggleButtonClass;
typedef struct _MildenhallToggleButtonPrivate MildenhallToggleButtonPrivate;

/**
* MildenhallToggleButton:
* @parent: #LightwoodButton: Parent of #MildenhallToggleButton
*
* Mildenhall Toggle button Object .
*/
struct _MildenhallToggleButton
{
	LightwoodButton parent;

	/*</private>*/
	MildenhallToggleButtonPrivate *priv;
};

/**
* MildenhallToggleButtonClass:
* @parent_class: #LightwoodButtonClass: Parent class of #MildenhallToggleButton
*
* Mildenhall Toggle button Class .
*/

struct _MildenhallToggleButtonClass
{
	LightwoodButtonClass parent_class;

	void (* button_toggled) (GObject *pButton, gpointer* pStateId );
};

GType mildenhall_toggle_button_get_type (void) G_GNUC_CONST;

/**
 * MildenhallToggleButtonDisplayType:
 * @MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE: Never display
 * @MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY: Allow only Image to be displayed in #MildenhallToggleButton
 * @MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY: Allow only Text to be displayed in #MildenhallToggleButton
 * @MILDENHALL_TOGGLE_BUTTON_DISPLAY_IAMGE_TEXT: Allow both text and image to be displayed in #MildenhallToggleButton 
 *
 * Support values of display type
 */
typedef enum /*< prefix=MILDENHALL_TOGGLE_BUTTON_DISPLAY >*/
{
  MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE,
  MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
  MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
  MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
} MildenhallToggleButtonDisplayType;

#define MILDENHALL_TOGGLE_BUTTON_DISPLAY_TYPE mildenhall_toggle_button_display_type_get_type ()
GType mildenhall_toggle_button_display_type_get_type (void) G_GNUC_CONST;

/**
 * MildenhallToggleButtonAlignmentType:
 * @MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE: Invalid alignment type
 * @MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT: Align the text to left in #MildenhallToggleButton
 * @MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE: Align the text to middle in #MildenhallToggleButton
 * @MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT: Align the text to right in #MildenhallToggleButton
 *
 * Support values of alignment type
 */
typedef enum /*< prefix=MILDENHALL_TOGGLE_BUTTON_ALIGNMENT >*/
{
  MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE,
  MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT,
  MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE,
  MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT,
} MildenhallToggleButtonAlignmentType;

#define MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_TYPE mildenhall_toggle_button_alignment_type_get_type ()
GType mildenhall_toggle_button_alignment_type_get_type (void) G_GNUC_CONST;

/* funtion to create the mildenhall toggle button object */
ClutterActor *mildenhall_toggle_button_new (void);


/* functions to set properties */

MildenhallToggleButtonDisplayType mildenhall_toggle_button_get_display_type
                                                        (MildenhallToggleButton *self);
void mildenhall_toggle_button_set_display_type          (MildenhallToggleButton *self,
                                                         MildenhallToggleButtonDisplayType display_type);

MildenhallToggleButtonAlignmentType mildenhall_toggle_button_get_text_alignment
                                                        (MildenhallToggleButton *self);
void mildenhall_toggle_button_set_text_alignment        (MildenhallToggleButton *self,
                                                         MildenhallToggleButtonAlignmentType alignment_type);

void mildenhall_toggle_button_set_current_state(MildenhallToggleButton *pToggleButton, gint inState);
void mildenhall_toggle_button_set_model(MildenhallToggleButton *pToggleButton, ThornburyModel *pModel);
void mildenhall_toggle_button_set_active(MildenhallToggleButton *pToggleButton, gboolean bState);
void mildenhall_toggle_button_set_highlight(MildenhallToggleButton *pToggleButton, gboolean bHighLightState);
/* functions to get properties */

gfloat mildenhall_toggle_button_get_width(MildenhallToggleButton *pToggleButton);
gfloat mildenhall_toggle_button_get_height(MildenhallToggleButton *pToggleButton);
gint mildenhall_toggle_button_get_current_state(MildenhallToggleButton *pToggleButton);
ThornburyModel *mildenhall_toggle_button_get_model(MildenhallToggleButton *pToggleButton);
gboolean mildenhall_toggle_button_get_highlight(MildenhallToggleButton *pToggleButton);



G_END_DECLS

#endif /* __MILDENHALL_TOGGLE_BUTTON_H__ */

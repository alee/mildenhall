/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_widget_container
 * @title: MildenhallWidgetContainer
 * @short_description: A container that can be used to lay out other widgets.
 * It supports animating widgets when moving them inside its view port.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #ThornburyModel
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * A private enum to set column of #ThornburyModel
 * |[<!-- language="C" -->
 * enum
 * {
 *   COLUMN_WIDGET_ACTOR,
 *   COLUMN_WIDGET_X,
 *   COLUMN_WIDGET_Y,
 *   COLUMN_WIDGET_LAST
 * };
 *
 * ]|
 *
 * How to create #MildenhallWidgetContainer
 * |[<!-- language="C" -->
 *
 * ThornburyItemFactory *factory_container;
 * ThornburyItemFactory *item_factory;
 * ThornburyItemFactory *item_factory1;
 * GObject *object = NULL, *object1 = NULL, *object_container = NULL;
 * ClutterActor *widget_container = NULL;
 *
 * ThornburyModel *model = NULL, *model1 = NULL;
 * ClutterActor *cont1 = NULL, *cont = NULL;
 * ThornburyModel *container_model = NULL;
 *
 * g_autofree gchar *launcher_video_file = NULL;
 * g_autofree gchar *content_video_file = NULL;
 *
 * launcher_video_file = g_build_filename ("/usr/share/mildenhall",
 *                                        "mildenhallroller",
 *                                        "01_appl_launcher_2009.03.09.avi",
 *                                        NULL);
 * content_video_file = g_build_filename ("/usr/share/mildenhall",
 *                                       "mildenhallroller",
 *                                       "03_content_roll_2009.03.09.avi",
 *                                       NULL);
 *
 * factory_container = thornbury_item_factory_generate_widget_with_props (
 *       	MILDENHALL_TYPE_WIDGET_CONTAINER, "/usr/share/mildenhall/mildenhall_widget_container_prop.json");
 *
 * g_object_get (factory_container, "object", &object_container, NULL);
 * widget_container = CLUTTER_ACTOR (object_container);
 *
 * container_model = (ThornburyModel*) thornbury_list_model_new (
 * 	COLUMN_WIDGET_LAST, G_TYPE_OBJECT, NULL, G_TYPE_FLOAT, NULL, G_TYPE_FLOAT,
 *      NULL, -1);
 *
 * model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_BOOLEAN, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_FLOAT, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    G_TYPE_STRING, NULL,
 *                                                    -1);
 * for (i = 0; i < 3; i++)
 * {
 *   g_autofree gchar *number = g_strdup_printf ("item number %i", i);
 *   g_autofree gchar *text = g_strdup_printf ("text for item %i", i);
 *
 *   thornbury_model_append (model,
 *                          COLUMN_NAME, number,
 *                          COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
 *                          COLUMN_LABEL, number,
 *                          COLUMN_TOGGLE, i % 2,
 *                          COLUMN_VIDEO, i % 2 == 0 ? launcher_video_file : content_video_file,
 *                          COLUMN_EXTRA_HEIGHT, (float) (i % 100),
 *                          COLUMN_COVER, covers[i % 3],
 *                          COLUMN_THUMB, NULL,
 *                          COLUMN_LONG_TEXT, text,
 *                          -1);
 * }
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 *       MILDENHALL_TYPE_ROLLER_CONTAINER,
 *       "/usr/share/mildenhall/mildenhall_roller_container_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * cont = CLUTTER_ACTOR (object);
 *
 * g_object_set (object, "item-type", MILDENHALL_TYPE_SAMPLE_ITEM, "model",
 *              model, NULL);
 * mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER(cont),
 *                                           "name", COLUMN_NAME);
 * mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER(cont),
 *                                           "label", COLUMN_LABEL);
 * g_signal_connect (G_OBJECT (cont), "roller-item-activated",
 *                  G_CALLBACK (roller_item_activated_cb), widget_container);
 *
 * model1 = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_BOOLEAN, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_FLOAT, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     -1);
 * for (i = 0; i < 3; i++)
 * {
 *   g_autofree gchar *number = g_strdup_printf ("item number %i", i);
 *   g_autofree gchar *text = g_strdup_printf ("text for item %i", i);
 *
 *   thornbury_model_append (model1,
 *                          COLUMN_NAME, number,
 *                          COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
 *                          COLUMN_LABEL, number,
 *                          COLUMN_TOGGLE, i % 2,
 *                          COLUMN_VIDEO, i % 2 == 0 ? launcher_video_file : content_video_file,
 *                          COLUMN_EXTRA_HEIGHT, (float) (i % 100),
 *                          COLUMN_COVER, covers[i % 3],
 *                          COLUMN_THUMB, NULL,
 *                          COLUMN_LONG_TEXT, text,
 *                          -1);
 * }
 * item_factory1 = thornbury_item_factory_generate_widget_with_props (
 * 	MILDENHALL_TYPE_ROLLER_CONTAINER, roller_container_prop_file);
 *
 * g_object_get (item_factory1, "object", &object1, NULL);
 * g_object_set (object1, "background", TRUE, "item-type",
 *              MILDENHALL_TYPE_SAMPLE_ITEM, "model", model1, NULL);
 *
 * cont1 = CLUTTER_ACTOR (object1);
 * mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER(cont1),
 *                                           "name", COLUMN_NAME);
 * mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER(cont1),
 *                                           "label", COLUMN_LABEL);
 * g_signal_connect (G_OBJECT (cont1), "roller-item-activated",
 *                  G_CALLBACK (roller2_item_activated_cb), widget_container);
 *
 * thornbury_model_append (container_model, COLUMN_WIDGET_ACTOR, cont,
 *                        COLUMN_WIDGET_X, 5.0, COLUMN_WIDGET_Y, 0.0, -1);
 * thornbury_model_append (container_model, COLUMN_WIDGET_ACTOR, cont1,
 *                        COLUMN_WIDGET_X, 345.0, COLUMN_WIDGET_Y, 0.0, -1);
 *
 * g_signal_connect (G_OBJECT (widget_container), "move-left-completed",
 *                  G_CALLBACK (left_animation_completed_cb), NULL);
 * g_signal_connect (G_OBJECT (widget_container), "move-right-completed",
 *                  G_CALLBACK (right_animation_completed_cb), NULL);
 * g_signal_connect (G_OBJECT (widget_container), "minimize-completed",
 *                  G_CALLBACK (minimize_completed_cb), NULL);
 * g_signal_connect (G_OBJECT (widget_container), "maximize-completed",
 *                  G_CALLBACK (maximize_completed_cb), NULL);
 *
 * g_object_set (widget_container, "model", container_model, "minimize", 120.0,
 *              NULL);
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (widget_container));
 *
 * ]|
 *
 * Example callback functions for MildenhallRollerContainer::roller-item-activated, MildenhallRollerContainer::move-left-completed,
 * MildenhallRollerContainer::move-right-completed and MildenhallRollerContainer::minimize-completed, maximize-completed.
 * |[<!-- language="C" -->
 * static void
 * roller_item_activated_cb (MildenhallRollerContainer *roller, guint row,
 *                          gpointer data)
 * {
 *   g_debug ("%s:row = %d", __FILE__, row);
 *   g_object_set (G_OBJECT (data), "move-left", 30.0, "maximize", 700.0, NULL);
 * }
 * static void
 * roller2_item_activated_cb (MildenhallRollerContainer *roller, guint row,
 *                           gpointer data)
 * {
 *   g_debug ("%s:row = %d", __FILE__, row);
 *   g_object_set (G_OBJECT (data), "move-right", 400.0,
 * 		  NULL);
 * }
 * static void
 * left_animation_completed_cb (MildenhallWidgetContainer *widget_container,
 *                             gpointer userData)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 * static void
 * right_animation_completed_cb (MildenhallWidgetContainer *widget_container,
 *                              gpointer userData)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 * static void
 * maximize_completed_cb (MildenhallWidgetContainer *widget_container,
 *                       gpointer userData)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 * static void
 * minimize_completed_cb (MildenhallWidgetContainer *widget_container,
 *                       gpointer userData)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include "mildenhall_widget_container.h"

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
//#define WIDGET_CONTAINER_DEBUG(...)   //g_print( __VA_ARGS__)

/* property enums */
enum _enWidgetContainerProperty
{
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_MODEL,
	PROP_ANIMATION_MODE,
	PROP_ANIMATION_TIMELINE,
	PROP_MAXIMIZE,
	PROP_MINIMIZE,
	PROP_MOVE_LEFT,
	PROP_MOVE_RIGHT
};

/* The signals emitted by the mildenhall meta info header */
enum _enWidgetContainerSignals
{
	SIG_MOVE_RIGHT,
	SIG_MOVE_LEFT,
	SIG_MAXIMIZE,
	SIG_MINIMIZE,
	SIG_LAST
};

/* model columns */
enum
{
        COLUMN_WIDGET_ACTOR,
        COLUMN_WIDGET_X,
        COLUMN_WIDGET_Y,
	COLUMN_LAST
};

static guint32 widget_container_signals[SIG_LAST] = {0,};

/* Set the environment variable in terminal to enable traces: export MILDENHALL_WIDGET_CONTIANER_DEBUG=mildenhall-widget-container */
enum _enWidgetContainerDebugFlag
{
        MILDENHALL_WIDGET_CONTAINER_DEBUG = 1 << 0,

};

/* debug flag setting */
guint  mildenhall_widget_container_debug_flags = 0;

static const GDebugKey mildenhall_widget_container_debug_keys[] =
{
        { "mildenhall-widget-container",   MILDENHALL_WIDGET_CONTAINER_DEBUG }
};

#define MILDENHALL_WIDGET_CONTAINER_HAS_DEBUG               ((mildenhall_widget_container_debug_flags ) & 1)
#define WIDGET_CONTAINER_DEBUG( a ...) \
        if (G_LIKELY (MILDENHALL_WIDGET_CONTAINER_HAS_DEBUG )) \
        {                               \
                g_print(a);           \
        }


G_DEFINE_TYPE (MildenhallWidgetContainer, mildenhall_widget_container, CLUTTER_TYPE_ACTOR)

#define WIDGET_CONTAINER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_WIDGET_CONTAINER, MildenhallWidgetContainerPrivate))


struct _MildenhallWidgetContainerPrivate
{
	ThornburyModel *pModel;

	gboolean bMoveLeftRight;
	gboolean bMaxMin;

	guint uinAnimationMode;
	guint uinTimeline;

	gfloat flMoveRight;
	gfloat flMoveLeft;
	gfloat flMaximize;
	gfloat flMinimize;

	ClutterTransition *pMoveLeftRightTrans;
	ClutterTransition *pMaxMinTrans;

	ClutterActor *pBackground;

	ClutterColor marginColor;

	gchar *pBackgroundPath;
};

static void mildenhall_widget_container_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);


/********************************************************
 * Function : mildenhall_widget_container_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_get_property (GObject    *pObject,
                                   guint       uinPropertyID,
                                   GValue     *pValue,
                                   GParamSpec *pspec)
{
	switch (uinPropertyID)
	{
		case PROP_WIDTH:
                        g_value_set_float (pValue,  mildenhall_widget_container_get_width(MILDENHALL_WIDGET_CONTAINER (pObject)));
                        break;
                case PROP_HEIGHT:
                        g_value_set_float (pValue,  mildenhall_widget_container_get_height(MILDENHALL_WIDGET_CONTAINER (pObject)));
                        break;
		case PROP_MODEL:
                        g_value_set_object (pValue, mildenhall_widget_container_get_model(MILDENHALL_WIDGET_CONTAINER (pObject)) );
                        break;
# if 0
		case PROP_MOVE_LEFT:
			g_value_set_float (pValue, mildenhall_widget_container_get_move_leftt(MILDENHALL_WIDGET_CONTAINER (pObject)) );
			break;
		case PROP_MOVE_RIGHT:
			g_value_set_float (pValue, mildenhall_widget_container_get_move_right(MILDENHALL_WIDGET_CONTAINER (pObject)) );
                        break;
		case PROP_MINIMIZE:
			g_value_set_float(pValue, mildenhall_widget_container_get_minimize(MILDENHALL_WIDGET_CONTAINER (pObject)) );
                        break;
		case PROP_MAXIMIZE:
			g_value_set_float (pValue, mildenhall_widget_container_get_maximize(MILDENHALL_WIDGET_CONTAINER (pObject)) );
                        break;
		case PROP_ANIMATION_MODE:
			g_value_set_uint (pValue, mildenhall_widget_container_get_animation_mode(MILDENHALL_WIDGET_CONTAINER (pObject)) );
			break;
		case PROP_ANIMATION_TIMELINE:
			g_value_set_uint (pValue, mildenhall_widget_container_get_animation_timeline(MILDENHALL_WIDGET_CONTAINER (pObject)) );
			break;
# endif
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_widget_container_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_set_property (GObject      *pObject,
                                   guint         uinPropertyID,
                                   const GValue *pValue,
                                   GParamSpec   *pspec)
{
	switch (uinPropertyID)
	{
		case PROP_WIDTH:
			break;
		case PROP_HEIGHT:
			break;
		case PROP_MODEL:
			mildenhall_widget_container_set_model(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_object (pValue));
			break;
		case PROP_MOVE_LEFT:
			mildenhall_widget_container_set_move_left(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_float(pValue));
                        break;
                case PROP_MOVE_RIGHT:
			mildenhall_widget_container_set_move_right(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_float(pValue));
                        break;
                case PROP_MINIMIZE:
			mildenhall_widget_container_set_minimize(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_float(pValue));
                        break;
                case PROP_MAXIMIZE:
			mildenhall_widget_container_set_maximize(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_float(pValue));
                        break;
		case PROP_ANIMATION_MODE:
			mildenhall_widget_container_set_animation_mode(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_uint(pValue));
                        break;
                case PROP_ANIMATION_TIMELINE:
			mildenhall_widget_container_set_animation_timeline(MILDENHALL_WIDGET_CONTAINER (pObject), g_value_get_uint(pValue));
                        break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_widget_container_dispose
 * Description: Dispose the widget container object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_dispose (GObject *pObject)
{
	MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE(MILDENHALL_WIDGET_CONTAINER(pObject));

	if(priv->pBackgroundPath)
        {
                g_free(priv->pBackgroundPath);
                priv->pBackgroundPath = NULL;
        }

	G_OBJECT_CLASS (mildenhall_widget_container_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : mildenhall_widget_container_finalize
 * Description: Finalize the widget container object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_widget_container_parent_class)->finalize (pObject);
}

/********************************************************
 * Function :   mildenhall_widget_container_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_class_init (MildenhallWidgetContainerClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallWidgetContainerPrivate));

	pObjectClass->get_property = mildenhall_widget_container_get_property;
	pObjectClass->set_property = mildenhall_widget_container_set_property;
	pObjectClass->dispose = mildenhall_widget_container_dispose;
	pObjectClass->finalize = mildenhall_widget_container_finalize;

	/**
         * MildenhallWidgetContainer:model:
         *
         * model for the mildenhall widget container
         *
         * Default: NULL
         */
        pspec = g_param_spec_object ("model",
                        "Model",
                        "Model to use to construct the items",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

	/**
         * MildenhallWidgetContainer:animation-mode:
         *
         * mode of animation to animate  widget container
         *
         * Default: CLUTTER_LINEAR
         */
        pspec = g_param_spec_uint ("animation-mode",
                        "Animation-Mode",
                        "Mode of animation",
                        0,
			G_MAXUINT,
			250,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ANIMATION_MODE, pspec);

	/**
         * MildenhallWidgetContainer:animation-timeline:
         *
         * timeline of animation to animate  widget container
         *
         * Default: 2000
         */
        pspec = g_param_spec_uint ("animation-timeline",
                        "Animation-Timeline",
                        "timeline of animation",
                        0,
                        G_MAXUINT,
                        2000,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ANIMATION_TIMELINE, pspec);

	/**
         * MildenhallWidgetContainer:move-left:
         *
         * Move left the  widget container
         *
         * Default: -1
         */
        pspec = g_param_spec_float ("move-left",
                        "Move-Left",
                        "Left animation",
                        -1.0,
                        G_MAXFLOAT,
                        -1.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MOVE_LEFT, pspec);

	/**
         * MildenhallWidgetContainer:move-right:
         *
         * Move right the  widget container
         *
         * Default: -1
         */
        pspec = g_param_spec_float ("move-right",
                        "Move-right",
                        "Rightt animation",
                        -1.0,
                        G_MAXFLOAT,
                        -1.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MOVE_RIGHT, pspec);

	/**
         * MildenhallWidgetContainer:minimize:
         *
         * Minimize the  widget container
         *
         * Default: -1
         */
        pspec = g_param_spec_float ("minimize",
                        "Minimize",
                        "Minimize the widget container ",
                        -1.0,
                        G_MAXFLOAT,
                        -1.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MINIMIZE, pspec);

	/**
         * MildenhallWidgetContainer:maximize:
         *
         * Maximize the  widget container
         *
         * Default: -1
         */
        pspec = g_param_spec_float ("maximize",
                        "Maximize",
                        "Maximize the widget container",
                        -1.0,
                        G_MAXFLOAT,
                        -1.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MAXIMIZE, pspec);

	/**
         * MildenhallWidgetContainer::move-left-completed:
         * @mildenhallWidgetContainer: The object which received the signal
         *
         * ::move-left-completed is emitted when animation for left movement of
 	 *   the widget container finished
         */
        widget_container_signals[SIG_MOVE_LEFT] = g_signal_new ("move-left-completed",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallWidgetContainerClass, move_left_completed),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	/**
         * MildenhallWidgetContainer::move-right-completed:
         * @mildenhallWidgetContainer: The object which received the signal
         *
         * ::move-right-completed is emitted when animation for right movement of
         *  the widget container finished
         */
        widget_container_signals[SIG_MOVE_RIGHT] = g_signal_new ("move-right-completed",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallWidgetContainerClass, move_right_completed),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	/**
         * MildenhallWidgetContainer::maximize-completed:
         * @mildenhallWidgetContainer: The object which received the signal
         *
         * ::maximize-completed is emitted when animation for maximize
	 *   the widget containerfinished
         */
        widget_container_signals[SIG_MAXIMIZE] = g_signal_new ("maximize-completed",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallWidgetContainerClass, maximize_completed),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	/**
         * MildenhallWidgetContainer::minimize-completed:
         * @mildenhallWidgetContainer: The object which received the signal
         *
         * ::minimize-completed is emitted when animation for minimize
	 *   the widget container finished
         */
        widget_container_signals[SIG_MINIMIZE] = g_signal_new ("minimize-completed",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallWidgetContainerClass, minimize_completed),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);


}

/********************************************************
 * Function : mildenhall_widget_container_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_init (MildenhallWidgetContainer *self)
{
        const char *pEnvString;
        /* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set (PKGDATADIR"/mildenhall_widget_container_style.json");

	self->priv = WIDGET_CONTAINER_PRIVATE (self);

	self->priv->bMoveLeftRight = FALSE;
	self->priv->bMaxMin = FALSE;
	self->priv->uinAnimationMode = 250;
	self->priv->uinTimeline = 2000;
	self->priv->flMoveRight = 0.0;
	self->priv->flMoveLeft = 0.0;
	self->priv->flMaximize = 0.0;
	self->priv->flMinimize = 0.0;

	self->priv->pMoveLeftRightTrans = NULL;
	self->priv->pBackgroundPath = NULL;
	self->priv->pBackground = NULL;


	//self->priv->marginColor = {0x00, 0x00, 0x00, 0x00};

	/* check for env to enable traces */
        pEnvString = g_getenv ("MILDENHALL_WIDGET_CONTAINER_DEBUG");
        if (pEnvString != NULL)
        {
                mildenhall_widget_container_debug_flags = g_parse_debug_string (pEnvString, mildenhall_widget_container_debug_keys, G_N_ELEMENTS (mildenhall_widget_container_debug_keys));
                WIDGET_CONTAINER_DEBUG ("MILDENHALL_WIDGET_CONTIANER_PRINT: env_string %s %ld %d \n", pEnvString, G_LIKELY (MILDENHALL_WIDGET_CONTAINER_HAS_DEBUG), mildenhall_widget_container_debug_flags);

        }

        /* pares the hash for styles */
        if(NULL != styleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, styleHash);
                /* iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if(NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, mildenhall_widget_container_parse_style, self);
                        }
                }
                /* free the style hash */
                thornbury_style_free(styleHash);
        }
        g_object_set(self, "height", 0.0, NULL);
}

/********************************************************
 * Function : mildenhall_widget_container_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallWidgetContainer *pWidgetContainer = pUserData;
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        gchar *pStyleKey = NULL;

        WIDGET_CONTAINER_DEBUG ("%s\n", __FUNCTION__);
        g_return_if_fail (MILDENHALL_IS_WIDGET_CONTAINER (pUserData));

        pStyleKey = g_strdup(pKey);
	WIDGET_CONTAINER_DEBUG("%s\n", pStyleKey);
	if(g_strcmp0(pStyleKey, "background") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pBackgroundPath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        WIDGET_CONTAINER_DEBUG("widget container background = %s\n", priv->pBackgroundPath);
                }
        }
	if(g_strcmp0(pStyleKey, "margin-rect-color") == 0)
	{
		clutter_color_from_string(&priv->marginColor,  g_value_get_string(pValue));
	}
	/* free the key */
	if(NULL != pStyleKey)
        {
                g_free(pStyleKey);
                pStyleKey = NULL;
        }

}

/********************************************************
 * Function : widget_container_add_background
 * Description: add background image
 * Parameters: pWidgetContainer*
 * Return value: void
 ********************************************************/
static void widget_container_add_background(MildenhallWidgetContainer *pWidgetContainer)
{
	GError *pError = NULL;

        MildenhallWidgetContainerPrivate  *priv = WIDGET_CONTAINER_PRIVATE(pWidgetContainer);

	gfloat flWidth = 0.0;
	gfloat flHeight = 0.0;
        WIDGET_CONTAINER_DEBUG ("%s\n", __FUNCTION__);

	g_object_get(pWidgetContainer, "width", &flWidth, "height", &flHeight, NULL );

	WIDGET_CONTAINER_DEBUG("total width = %f , total height = %f\n", flWidth, flHeight);

	if(NULL != priv->pBackgroundPath)
	{
		priv->pBackground = thornbury_texture_create_sync(priv->pBackgroundPath, flWidth, flHeight, &pError);
		if(priv->pBackground)
		{
			clutter_actor_insert_child_at_index(CLUTTER_ACTOR(pWidgetContainer), priv->pBackground, 0);
			//clutter_actor_set_size(priv->pBackground, flWidth, flHeight);
		}
	}

}

/********************************************************
 * Function : mildenhall_widget_container_add_actor
 * Description: add the widget to the container
 * Parameters: pWidgetContainer*, ThornburyModelIter *
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_add_actor(MildenhallWidgetContainer *pWidgetContainer, ThornburyModelIter *pIter)
{
	ClutterActor *pActor = NULL;
	gfloat flX = 0.0;
	gfloat flY = 0.0;
	gfloat flWidth = 0.0;
	gfloat flHeight = 0.0;
        WIDGET_CONTAINER_DEBUG ("%s\n", __FUNCTION__);

	thornbury_model_iter_get(pIter, COLUMN_WIDGET_ACTOR, &pActor, -1);
	thornbury_model_iter_get(pIter, COLUMN_WIDGET_X, &flX, -1);
	thornbury_model_iter_get(pIter, COLUMN_WIDGET_Y, &flY, -1);

	WIDGET_CONTAINER_DEBUG("actor = %p, x= %f, y = %f\n ", &pActor, flX, flY);

	if(NULL != pActor)
	{
		/* add actor with position */
		clutter_actor_add_child(CLUTTER_ACTOR(pWidgetContainer), pActor);
		clutter_actor_set_position(pActor, flX, flY);

		g_object_get(G_OBJECT(pActor), "height", &flHeight, NULL);
		g_object_get(G_OBJECT(pWidgetContainer), "width", &flWidth, NULL);
	//	WIDGET_CONTAINER_DEBUG("%d actor added at %f %f position\n", inIndex, flX, flY);
		WIDGET_CONTAINER_DEBUG("container width = %f,actor height = %f\n", flWidth, flHeight);
		/* set container height */
		if( clutter_actor_get_height(CLUTTER_ACTOR(pWidgetContainer)) < flHeight)
		{
			g_object_set(CLUTTER_ACTOR(pWidgetContainer), "height", flHeight, NULL);
		}

		g_object_get(CLUTTER_ACTOR(pWidgetContainer), "width", &flWidth, "height", &flHeight, NULL);
		WIDGET_CONTAINER_DEBUG("container width = %f,container height = %f\n", flWidth, flHeight);
	}
}

/********************************************************
 * Function : mildenhall_widget_container_update_view
 * Description: update the widget on set model
 * Parameters: pWidgetContainer*, pModel*
 * Return value: void
 ********************************************************/
static void mildenhall_widget_container_update_view(MildenhallWidgetContainer *pWidgetContainer, ThornburyModel *pModel)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

	if (G_IS_OBJECT(priv->pModel) && thornbury_model_get_n_rows (priv->pModel) > 0)
    {
		gint inIndex = 0;
		gint inTotalRows = thornbury_model_get_n_rows (priv->pModel);

		for(; inIndex <inTotalRows; inIndex++)
                {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pModel, inIndex);
			if(NULL != pIter)
			{
				mildenhall_widget_container_add_actor(pWidgetContainer, pIter);
			}
		}
		/* add background */
		widget_container_add_background(pWidgetContainer);
	}
}

/********************************************************
 * Function : move_right_animation_completed
 * Description: on move left animation completed, reset the animation flag and emit the signal
 * Parameters: ClutterAnimation*, ClutterActor *
 * Return value: void
 ********************************************************/
static void move_right_animation_completed(ClutterAnimation* animation, ClutterActor *pUserData)
{
        MildenhallWidgetContainer *pWidgetContainer = MILDENHALL_WIDGET_CONTAINER (pUserData);
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);

	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);
	if(! MILDENHALL_IS_WIDGET_CONTAINER(pUserData) )
	{
		g_warning("invalid widget container object\n");
		return;
	}

	g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MOVE_RIGHT], 0, NULL);
	priv->bMoveLeftRight = FALSE;
}

/********************************************************
 * Function :move_left_animation_completed
 * Description: on move left animation completed, reset the animation flag and emit the signal
 * Parameters: ClutterAnimation*, ClutterActor *
 * Return value: void
 ********************************************************/
static void move_left_animation_completed(ClutterAnimation* animation, ClutterActor *pUserData)
{
        MildenhallWidgetContainer *pWidgetContainer = MILDENHALL_WIDGET_CONTAINER (pUserData);
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);

	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);
	if(! MILDENHALL_IS_WIDGET_CONTAINER(pUserData) )
	{
		g_warning("invalid widget container object\n");
		return;
	}

	g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MOVE_LEFT], 0, NULL);
	priv->bMoveLeftRight = FALSE;
}

# if 0
/********************************************************
 * Function :maximize_animation_completed
 * Description: on maximize animation completed, reset the animation flag and emit the signal
 * Parameters: ClutterAnimation*, ClutterActor *
 * Return value: void
 ********************************************************/
static void maximize_animation_completed(ClutterAnimation* animation, ClutterActor *pUserData)
{
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);
        if(! MILDENHALL_IS_WIDGET_CONTAINER(pUserData) )
        {
                g_warning("invalid widget container object\n");
                return;
        }

        MildenhallWidgetContainer *pWidgetContainer = MILDENHALL_WIDGET_CONTAINER(pUserData);
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE(pWidgetContainer);

        g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MAXIMIZE], 0, NULL);
        priv->bMaxMin = FALSE;
}

/********************************************************
 * Function :minimize_animation_completed
 * Description: on minimize animation completed, reset the animation flag and emit the signal
 * Parameters: ClutterAnimation*, ClutterActor *
 * Return value: void
 ********************************************************/
static void minimize_animation_completed(ClutterAnimation* animation, ClutterActor *pUserData)
{
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);
        if(! MILDENHALL_IS_WIDGET_CONTAINER(pUserData) )
        {
                g_warning("invalid widget container object\n");
                return;
        }

        MildenhallWidgetContainer *pWidgetContainer = MILDENHALL_WIDGET_CONTAINER(pUserData);
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE(pWidgetContainer);

        g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MINIMIZE], 0, NULL);
        priv->bMaxMin = FALSE;
}
# endif

/*******************************************************************************************************
 *
 * Exposed APIs
 *
 *******************************************************************************************************/

/**
 * mildenhall_widget_container_set_model:
 * @pWidgetContainer: Object reference
 * @pModel: model for widget container
 *
 * Set the model used by the widget container
 * model format:
 * 	"widget-pointer" - ClutterActor*
 *	"widget-x"	 - x position for widget
 *	"widget-y"	 - y position for widget
 */
void mildenhall_widget_container_set_model(MildenhallWidgetContainer *pWidgetContainer, ThornburyModel *pModel)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

	g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	if(NULL != priv->pModel)
        {
                g_object_unref (priv->pModel);

                priv->pModel = NULL;
        }
        /* update the new model */
        if (pModel != NULL)
        {
                g_return_if_fail (G_IS_OBJECT (pModel));

                priv->pModel = g_object_ref (pModel);
        }

	mildenhall_widget_container_update_view(pWidgetContainer, pModel);

	g_object_notify (G_OBJECT(pWidgetContainer), "model");
}

/*
 * mildenhall_widget_container_set_move_right:
 * @pWidgetContainer: Object reference
 * @flMoveRight: value to move right direction
 *
 * Moves the widget to right direction at given value
 */
void mildenhall_widget_container_set_move_right(MildenhallWidgetContainer *pWidgetContainer, gfloat flMoveRight)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("flMoveRight = %f\n", flMoveRight);

	/* if move-left/move-right already running, don't start it again until completed */
	if(priv->bMoveLeftRight)
		return;

	priv->bMoveLeftRight = TRUE;

	clutter_actor_save_easing_state(CLUTTER_ACTOR(pWidgetContainer) );
        clutter_actor_set_easing_mode (CLUTTER_ACTOR(pWidgetContainer), priv->uinAnimationMode );
        clutter_actor_set_easing_duration (CLUTTER_ACTOR(pWidgetContainer), priv->uinTimeline);
        clutter_actor_set_x (CLUTTER_ACTOR(pWidgetContainer), flMoveRight);

        clutter_actor_restore_easing_state (CLUTTER_ACTOR(pWidgetContainer));

	priv->pMoveLeftRightTrans = clutter_actor_get_transition(CLUTTER_ACTOR(pWidgetContainer), "x") ;
	clutter_transition_set_remove_on_complete(priv->pMoveLeftRightTrans, TRUE);
	g_signal_connect(CLUTTER_TIMELINE (priv->pMoveLeftRightTrans), "completed", G_CALLBACK(move_right_animation_completed), pWidgetContainer);
}

/*
 * mildenhall_widget_container_set_move_left:
 * @pWidgetContainer: Object reference
 * @flMoveLeft: value to move left direction
 *
 * Moves the widget to left direction at given value
 */
void mildenhall_widget_container_set_move_left(MildenhallWidgetContainer *pWidgetContainer, gfloat flMoveLeft)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
	WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("flMoveLeft = %f\n", flMoveLeft);

	/* if move-left/move-right already running, don't start it again until completed */
	if(priv->bMoveLeftRight)
          	return;

	priv->bMoveLeftRight = TRUE;

	clutter_actor_save_easing_state(CLUTTER_ACTOR(pWidgetContainer) );
	clutter_actor_set_easing_mode (CLUTTER_ACTOR(pWidgetContainer), priv->uinAnimationMode );
	clutter_actor_set_easing_duration (CLUTTER_ACTOR(pWidgetContainer), priv->uinTimeline);
	clutter_actor_set_x (CLUTTER_ACTOR(pWidgetContainer), flMoveLeft);

	clutter_actor_restore_easing_state (CLUTTER_ACTOR(pWidgetContainer));

	priv->pMoveLeftRightTrans = clutter_actor_get_transition(CLUTTER_ACTOR(pWidgetContainer), "x") ;
        clutter_transition_set_remove_on_complete(priv->pMoveLeftRightTrans, TRUE);
        g_signal_connect(CLUTTER_TIMELINE (priv->pMoveLeftRightTrans), "completed", G_CALLBACK(move_left_animation_completed), pWidgetContainer);

}

/*
 * mildenhall_widget_container_set_maximize:
 * @pWidgetContainer: Object reference
 * @flMaximize: value to maximize the widget
 *
 * Maximize the widget to given value
 */
void mildenhall_widget_container_set_maximize(MildenhallWidgetContainer *pWidgetContainer, gfloat flMaximize)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        ClutterRect *rect = NULL;
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("flMaximize = %f\n", flMaximize);

	/* if maximize/minimize already running, don't start it again until completed */
	if(priv->bMaxMin)
		return;

	priv->bMaxMin = TRUE;

        rect = clutter_rect_init(clutter_rect_alloc(),
					0,//clutter_actor_get_x(CLUTTER_ACTOR(pWidgetContainer) ),
                                        0,//clutter_actor_get_y(CLUTTER_ACTOR(pWidgetContainer) ),
                                        flMaximize,
                                        clutter_actor_get_height(CLUTTER_ACTOR(pWidgetContainer)) );

        clutter_actor_save_easing_state(CLUTTER_ACTOR(pWidgetContainer) );
        clutter_actor_set_easing_mode (CLUTTER_ACTOR(pWidgetContainer), CLUTTER_EASE_IN_QUART);//priv->uinAnimationMode );
        clutter_actor_set_easing_duration (CLUTTER_ACTOR(pWidgetContainer), priv->uinTimeline);
        g_object_set(CLUTTER_ACTOR(pWidgetContainer), "clip-rect", rect, NULL);

        clutter_actor_restore_easing_state (CLUTTER_ACTOR(pWidgetContainer));

	g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MAXIMIZE], 0, NULL);
        priv->bMaxMin = FALSE;
# if 0
	priv->pMaxMinTrans = clutter_actor_get_transition(CLUTTER_ACTOR(pWidgetContainer), "clip-rect") ;
        clutter_transition_set_remove_on_complete(priv->pMaxMinTrans, TRUE);
        g_signal_connect(CLUTTER_TIMELINE (priv->pMaxMinTrans), "completed", G_CALLBACK(maximize_animation_completed), pWidgetContainer);
#endif
}

/*
 * mildenhall_widget_container_set_minimize:
 * @pWidgetContainer: Object reference
 * @flMinimize: value to minimize the widget
 *
 * Minimize the widget to given value
 */
void mildenhall_widget_container_set_minimize(MildenhallWidgetContainer *pWidgetContainer, gfloat flMinimize)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        ClutterRect *rect = NULL;
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("flMinimize = %f\n", flMinimize);

	/* if maximize/minimize already running, don't start it again until completed */
	if(priv->bMaxMin)
		return;

	priv->bMaxMin = TRUE;

	rect = clutter_rect_init(clutter_rect_alloc(),
					0,//clutter_actor_get_x(CLUTTER_ACTOR(pWidgetContainer)),
					0,//clutter_actor_get_y(CLUTTER_ACTOR(pWidgetContainer)),
					flMinimize,
					clutter_actor_get_height(CLUTTER_ACTOR(pWidgetContainer) ));

	clutter_actor_save_easing_state(CLUTTER_ACTOR(pWidgetContainer) );
        clutter_actor_set_easing_mode (CLUTTER_ACTOR(pWidgetContainer), CLUTTER_EASE_IN_QUART);//priv->uinAnimationMode );
        clutter_actor_set_easing_duration (CLUTTER_ACTOR(pWidgetContainer), priv->uinTimeline);
        g_object_set(CLUTTER_ACTOR(pWidgetContainer), "clip-rect", rect, NULL);

        clutter_actor_restore_easing_state (CLUTTER_ACTOR(pWidgetContainer));

	g_signal_emit(pWidgetContainer, widget_container_signals[SIG_MINIMIZE], 0, NULL);
        priv->bMaxMin = FALSE;
# if 0
	priv->pMaxMinTrans = clutter_actor_get_transition(CLUTTER_ACTOR(pWidgetContainer), "clip-rect") ;
        clutter_transition_set_remove_on_complete(priv->pMaxMinTrans, TRUE);
        g_signal_connect(CLUTTER_TIMELINE (priv->pMaxMinTrans), "completed", G_CALLBACK(minimize_animation_completed), pWidgetContainer);
# endif
}

/*
 * mildenhall_widget_container_set_animation_timeline:
 * @pWidgetContainer: Object reference
 * @uinTimeline: animation duration for the widget container
 *
 * Timeline for the widget to animate
 */
void mildenhall_widget_container_set_animation_timeline(MildenhallWidgetContainer *pWidgetContainer, gint uinTimeline)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("uinTimeline = %d\n", uinTimeline);

	if(priv->bMoveLeftRight || priv->bMaxMin)
	{
		g_warning("Animation is running, can not update the timeline\n");
                return;
	}
	priv->uinTimeline = uinTimeline;
}

/*
 * mildenhall_widget_container_set_animation_mode:
 * @pWidgetContainer: Object reference
 * @uinAnimationMode: mode of animation
 *
 * Animation mode for the widget container
 */
void mildenhall_widget_container_set_animation_mode(MildenhallWidgetContainer *pWidgetContainer, gint uinAnimationMode)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_WIDGET_CONTAINER(pWidgetContainer));

	WIDGET_CONTAINER_DEBUG("uinAnimationMode = %d\n", uinAnimationMode);

	/* if animation is going on, don't update the timeline in between */
	if(priv->bMoveLeftRight || priv->bMaxMin)
	{
		g_warning("Animation is running, can not update the animation mode\n");
                return;
	}
	priv->uinAnimationMode = uinAnimationMode;
}


/**
 * mildenhall_widget_container_get_model:
 * @pWidgetContainer: Object reference
 *
 * gets the container model
 *
 * Returns: model pointer
 */
ThornburyModel *mildenhall_widget_container_get_model(MildenhallWidgetContainer *pWidgetContainer)
{
        MildenhallWidgetContainerPrivate *priv = WIDGET_CONTAINER_PRIVATE (pWidgetContainer);
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_WIDGET_CONTAINER (pWidgetContainer ) )
        {
                g_warning("invalid widget container object\n");
		return NULL;
	}

	return priv->pModel;
}

/**
 * mildenhall_widget_container_get_height:
 * @pWidgetContainer : Object reference
 *
 * gets the height of widget container.
 *
 * Returns: height of the widget container
 */
gfloat mildenhall_widget_container_get_height(MildenhallWidgetContainer *pWidgetContainer)
{
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_WIDGET_CONTAINER (pWidgetContainer ) )
        {
                g_warning("invalid widget container object\n");
                return 0.0;
        }

        //MildenhallWidgetContainerPrivate  *priv = WIDGET_CONTAINER_PRIVATE(pWidgetContainer);

        return clutter_actor_get_height(CLUTTER_ACTOR(pWidgetContainer));
}

/**
 * mildenhall_widget_container_get_width:
 * @pWidgetContainer: Object reference
 *
 * gets the width of widget container.
 *
 * Returns: width of the widget container
 */
gfloat mildenhall_widget_container_get_width(MildenhallWidgetContainer *pWidgetContainer)
{
        WIDGET_CONTAINER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_WIDGET_CONTAINER (pWidgetContainer ) )
        {
                g_warning("invalid widget container object\n");
                return 0.0;
        }

        //MildenhallWidgetContainerPrivate  *priv = WIDGET_CONTAINER_PRIVATE(pWidgetContainer);

        return clutter_actor_get_width(CLUTTER_ACTOR(pWidgetContainer) );
}

/**
 * mildenhall_widget_container_new:
 *
 * Creates a mildenhall widget container object
 *
 * Returns: (transfer full): widget container object
 */
MildenhallWidgetContainer *mildenhall_widget_container_new (void)
{
	return g_object_new (MILDENHALL_TYPE_WIDGET_CONTAINER, NULL);
}

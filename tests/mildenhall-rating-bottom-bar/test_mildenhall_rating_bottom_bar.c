/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_rating_bottom_bar.h"

#include <thornbury/thornbury.h>

#define MILDENHALL_TEST_RATING_BOTTOM_BAR_PRINT( a ...)			g_print(a);
gint count;

/* model columns */
enum _enRatingBottomBarModelInfo
{
	RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS,
	RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT,
	RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT,
	RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT,
	RATING_BOTTOM_BAR_MODEL_COLUMN_LAST
};


/* creation of model */
ThornburyModel *create_model()
{
	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (RATING_BOTTOM_BAR_MODEL_COLUMN_LAST,
			G_TYPE_INT, 0,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			-1);


	/* ratings : -1 to hide */
	thornbury_model_append (model,
			RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS, 9 ,
			RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT, "3 ratings added to this widget",
			RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT,"the cost of the widget is approx 55 $",
			RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT,"no of stars",
			-1);

	return model;

}

gboolean bIsRating = FALSE;

static gboolean b_mildenhall_rating_bottom_bar_change_prop(gpointer pUserData)
{
	MILDENHALL_TEST_RATING_BOTTOM_BAR_PRINT("%s \n",__FUNCTION__);
	g_object_set(pUserData, "is-rating", (bIsRating == TRUE ? FALSE : TRUE ),
			"right-text-font" , (bIsRating == TRUE ? "DejaVuSansCondensed 18px" : "DejaVuSansCondensed 28px" ),
			"right-text-color" ,  (bIsRating == TRUE ? "#98A9B3FF" : "#FFFFFFFF" ),
			"left-bottom-text-font" ,(bIsRating == FALSE ? "DejaVuSansCondensed 18px" : "DejaVuSansCondensed 28px" ),
			"left-bottom-text-color" , (bIsRating == FALSE ? "#98A9B3FF" : "#FFFFFFFF" ),
			"background-color" , (bIsRating == FALSE ? "#00000033" : "#FFFFFF20" ),
			NULL);

	bIsRating = !bIsRating;

	return TRUE;

}

int main (int argc, char **argv)
{
	g_autofree gchar *prop_file = NULL;
	ThornburyItemFactory *itemFactory;
	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;

	ClutterActor *stage;
	ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

	stage = clutter_stage_new ();
	clutter_actor_set_background_color (stage, &black);
	clutter_actor_set_size (stage, 728, 480);
	g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	prop_file = _mildenhall_get_resource_path ("mildenhall_rating_bottom_bar_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
	    MILDENHALL_TYPE_RATING_BOTTOM_BAR, prop_file);
	GObject *pObject = NULL;

	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallRatingBottomBar *toggle = MILDENHALL_RATING_BOTTOM_BAR(pObject);
	ClutterActor *button = CLUTTER_ACTOR(toggle);

	clutter_actor_add_child(stage, button);
	ThornburyModel *model = create_model();
	g_object_set(button, "model", model,NULL);

	g_timeout_add(2000 , b_mildenhall_rating_bottom_bar_change_prop,button);

		clutter_actor_show (stage);

	clutter_main();

	return 0;

}

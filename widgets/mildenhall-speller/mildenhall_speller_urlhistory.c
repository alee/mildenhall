/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-speller-url-history.c */

#include "mildenhall_speller_urlhistory.h"
#include <string.h>


G_DEFINE_TYPE (MildenhallSpellerUrlHistory, mildenhall_speller_url_history, G_TYPE_OBJECT)

#define SPELLER_URL_HISTORY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_URL_HISTORY, MildenhallSpellerUrlHistoryPrivate))

struct _MildenhallSpellerUrlHistoryPrivate
{
	GSList* m_history[HISTORY_LAST];
};


static gint _mildenhall_speller_url_history_compare(gconstpointer search_text,gconstpointer history_item);


/********************************************************
 * Function : mildenhall_speller_url_history_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_url_history_dispose (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_speller_url_history_parent_class)->dispose (object);
}

/********************************************************
 * Function : mildenhall_speller_url_history_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_url_history_class_init (MildenhallSpellerUrlHistoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  g_type_class_add_private (klass, sizeof (MildenhallSpellerUrlHistoryPrivate));
  object_class->dispose = mildenhall_speller_url_history_dispose;
}

/********************************************************
 * Function : mildenhall_speller_url_history_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_url_history_init (MildenhallSpellerUrlHistory *self)
{
  MildenhallSpellerUrlHistoryPrivate* priv = self->priv = SPELLER_URL_HISTORY_PRIVATE (self);
  guint i=0;
  for(i=0;i < HISTORY_LAST;i++)
  {
	  priv->m_history[i] = NULL;
  }
}

/**
 * mildenhall_speller_url_history_new:
 * Returns: MildenhallSpellerUrlHistory
 *
 * Creates a speller history object
 *
 */
MildenhallSpellerUrlHistory *mildenhall_speller_url_history_new (void)
{
  return g_object_new (MILDENHALL_TYPE_SPELLER_URL_HISTORY, NULL);
}

/********************************************************
 * Function : mildenhall_speller_url_history_add_item
 * Description: function to add history data
 * Parameters: MildenhallSpellerUrlHistory*,historyType,gchar*
 * Return value: void
 ********************************************************/
void mildenhall_speller_url_history_add_item(MildenhallSpellerUrlHistory* self,historyType type,gchar* item)
{
	MildenhallSpellerUrlHistoryPrivate* priv = SPELLER_URL_HISTORY_PRIVATE (self);
	if(NULL == priv->m_history[type])
		priv->m_history[type] = g_slist_append(priv->m_history[type],g_strdup(item));
	else if(!g_slist_find_custom(priv->m_history[type],item,(GCompareFunc)_mildenhall_speller_url_history_compare))
		priv->m_history[type] =
				g_slist_insert_sorted(priv->m_history[type],g_strdup(item),(GCompareFunc)_mildenhall_speller_url_history_compare);
}

/********************************************************
 * Function : mildenhall_speller_url_history_clear
 * Description: function to clear history data
 * Parameters: MildenhallSpellerUrlHistory*,historyType
 * Return value: void
 ********************************************************/
void mildenhall_speller_url_history_clear(MildenhallSpellerUrlHistory* self,historyType type)
{
	MildenhallSpellerUrlHistoryPrivate* priv = SPELLER_URL_HISTORY_PRIVATE (self);
	g_slist_free(priv->m_history[type]);
	priv->m_history[type] = NULL;
}

/********************************************************
 * Function : mildenhall_speller_url_history_match
 * Description: function to match history data
 * Parameters: MildenhallSpellerUrlHistory*,historyType,gchar*
 * Return value: historyiter
 ********************************************************/
historyiter mildenhall_speller_url_history_match(MildenhallSpellerUrlHistory* self,historyType type,gchar* search_item)
{
	MildenhallSpellerUrlHistoryPrivate* priv = SPELLER_URL_HISTORY_PRIVATE (self);
	GSList* found_item = g_slist_find_custom(priv->m_history[type],(gconstpointer)search_item, (GCompareFunc)_mildenhall_speller_url_history_compare);
	if(found_item)
	{
		historyiter history_match = g_new(history,1);
		history_match->m_type = type;
		history_match->m_search_text = g_strdup(search_item);
		history_match->m_head = history_match->m_cur = found_item;
		return (gpointer)history_match;
	}
	return NULL;
}

/********************************************************
 * Function : mildenhall_speller_url_history_next
 * Description: function to match history data
 * Parameters: MildenhallSpellerUrlHistory,historyiter
 * Return value: gchar
 ********************************************************/
gchar* mildenhall_speller_url_history_next(MildenhallSpellerUrlHistory* self,historyiter history_match_iter)
{
        GSList* found_item = NULL;
    //MildenhallSpellerUrlHistoryPrivate* priv = SPELLER_URL_HISTORY_PRIVATE (self);
	if(!history_match_iter) return NULL;

	found_item = g_slist_find_custom (history_match_iter->m_cur, (gconstpointer) history_match_iter->m_search_text, (GCompareFunc) _mildenhall_speller_url_history_compare);
	if(!found_item)
	{
		//g_print("\n searching %s",history_match_iter->m_search_text);
		found_item = g_slist_find_custom(history_match_iter->m_head,(gconstpointer)history_match_iter->m_search_text, (GCompareFunc)_mildenhall_speller_url_history_compare);
		g_assert(found_item);
	}
	if(found_item->next)
	{
	    history_match_iter->m_cur = found_item->next;
	}
	else
	{
	    history_match_iter->m_cur =history_match_iter->m_head;
	}
	return (gchar*)(found_item->data);
}

/********************************************************
 * Function : _mildenhall_speller_url_history_compare
 * Description: function to compare history data
 * Parameters: gconstpointer,history_item,gconstpointer
 * Return value: gint
 ********************************************************/
static gint _mildenhall_speller_url_history_compare(gconstpointer history_item,gconstpointer search_text)
{
	//g_print("search=%s,history=%s",(const gchar*)search_text,(const gchar*)history_item);
	if(search_text && history_item)
		//return g_ascii_strncasecmp((const gchar*)search_text,(const gchar*)history_item,strlen(search_text));
		return strncmp((const gchar*)search_text,(const gchar*)history_item,strlen(search_text));
	return -1;

}

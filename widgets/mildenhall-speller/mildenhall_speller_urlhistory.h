/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-speller-url-history.h */

#ifndef _MILDENHALL_SPELLER_URL_HISTORY_H
#define _MILDENHALL_SPELLER_URL_HISTORY_H

#include <clutter/clutter.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_URL_HISTORY mildenhall_speller_url_history_get_type()

#define MILDENHALL_SPELLER_URL_HISTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_URL_HISTORY, MildenhallSpellerUrlHistory))

#define MILDENHALL_SPELLER_URL_HISTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_URL_HISTORY, MildenhallSpellerUrlHistoryClass))

#define MILDENHALL_IS_SPELLER_URL_HISTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_URL_HISTORY))

#define MILDENHALL_IS_SPELLER_URL_HISTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_URL_HISTORY))

#define MILDENHALL_SPELLER_URL_HISTORY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_URL_HISTORY, MildenhallSpellerUrlHistoryClass))

typedef struct _MildenhallSpellerUrlHistory MildenhallSpellerUrlHistory;
typedef struct _MildenhallSpellerUrlHistoryClass MildenhallSpellerUrlHistoryClass;
typedef struct _MildenhallSpellerUrlHistoryPrivate MildenhallSpellerUrlHistoryPrivate;

struct _MildenhallSpellerUrlHistory
{
  GObject parent;

  MildenhallSpellerUrlHistoryPrivate *priv;
};

struct _MildenhallSpellerUrlHistoryClass
{
  GObjectClass parent_class;
};

typedef enum
{
	HISTORY_URL = 0,
	HISTORY_SEARCH,
	HISTORY_LAST
}historyType;

typedef struct
{
	historyType m_type;
	gchar* 		m_search_text;
	GSList*		m_head;
	GSList*		m_cur;
}history;
typedef history* historyiter;

GType mildenhall_speller_url_history_get_type (void) G_GNUC_CONST;

MildenhallSpellerUrlHistory *mildenhall_speller_url_history_new (void);
void mildenhall_speller_url_history_add_item(MildenhallSpellerUrlHistory* self,historyType type,gchar* item);
void mildenhall_speller_url_history_clear(MildenhallSpellerUrlHistory* self,historyType type);
historyiter mildenhall_speller_url_history_match(MildenhallSpellerUrlHistory* self,historyType type,gchar* search_item);
gchar* mildenhall_speller_url_history_next(MildenhallSpellerUrlHistory* self,historyiter history_match_iter);


G_END_DECLS

#endif /* _MILDENHALL_SPELLER_URL_HISTORY_H */

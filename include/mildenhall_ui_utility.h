/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_UI_UTILITY_H__
#define __MILDENHALL_UI_UTILITY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <clutter/clutter.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <glib/gprintf.h>
#include <glib-object.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define MILDENHALL_DEFAULT_FONT(size) "DejaVuSansCondensed " #size"px"

ClutterActor *mildenhall_stage_new                              (const gchar *name);

#ifndef MILDENHALL_DISABLE_DEPRECATED
G_DEPRECATED_FOR(clutter_init)
gint initialize_ui_toolkit(gint argc, gchar **argv );
G_DEPRECATED_FOR(mildenhall_stage_new)
ClutterActor* create_mildenhall_window(gchar *app_name);
#endif /* MILDENHALL_DISABLE_DEPRECATED */

ClutterActor *mildenhall_ui_utils_create_application_background (void);
ClutterActor *mildenhall_ui_utils_create_application_overlay (void);
ClutterActor *mildenhall_ui_utils_create_application_background_with_overlay (void);
ClutterActor *mildenhall_ui_utils_create_application_roller_background (void);

G_END_DECLS

#endif /* __MILDENHALL_UI_UTILITY_H__ */

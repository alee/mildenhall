/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_LBS_BOTTOM_BAR_H__
#define __MILDENHALL_LBS_BOTTOM_BAR_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_LBS_BOTTOM_BAR mildenhall_lbs_bottom_bar_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallLbsBottomBar, mildenhall_lbs_bottom_bar, MILDENHALL, LBS_BOTTOM_BAR, ClutterActor)

MildenhallLbsBottomBar *mildenhall_lbs_bottom_bar_new (void);

void mildenhall_lbs_bottom_bar_set_lefttext (MildenhallLbsBottomBar *self, const gchar *text);

void mildenhall_lbs_bottom_bar_set_righttext (MildenhallLbsBottomBar *self, const gchar *text);

void mildenhall_lbs_bottom_bar_set_middletext (MildenhallLbsBottomBar *self, const gchar *text);

void mildenhall_lbs_bottom_bar_set_width (MildenhallLbsBottomBar *self, gfloat width);

void mildenhall_lbs_bottom_bar_set_height (MildenhallLbsBottomBar *self, gfloat height);

void mildenhall_lbs_bottom_bar_set_x (MildenhallLbsBottomBar *self, gfloat posx);

void mildenhall_lbs_bottom_bar_set_y (MildenhallLbsBottomBar *self, gfloat posy);

G_END_DECLS

#endif /* __MILDENHALL_LBS_BOTTOM_BAR_H__ */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_roller_container
 * @title: MildenhallRollerContainer
 * @short_description: The generic implementation for the LightwoodRoller.
 * It is actually not a subclass, but rather a container that can host either a FixedRoller or a VariableRoller.
 * @see_also: #ClutterActor, #LightwoodRoller, #ThornburyItemFactory
 *
 * The basic idea of MildenhallRollerContainer is to customize the mx-roller as per mildenhall requirements.
 * They are,
 *	1)Background color for roller
 *	2)Arrow at the focus item
 *	3)Blurr Effects
 *	4)Any addtional overlay upon the roller if any.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * A private enum to set column of #ThornburyModel
 * |[<!-- language="C" -->
 *
 * enum
 * {
 *   COLUMN_NAME,
 *   COLUMN_ICON,
 *   COLUMN_LABEL,
 *   COLUMN_TOGGLE,
 *   COLUMN_VIDEO,
 *   COLUMN_EXTRA_HEIGHT,
 *   COLUMN_COVER,
 *   COLUMN_THUMB,
 *   COLUMN_LONG_TEXT,
 *   COLUMN_LAST
 * };
 *
 * ]|
 *
 * How to create #MildenhallRollerContainer
 * |[<!-- language="C" -->
 *   ThornburyItemFactory *item_factory;
 *   GObject *object = NULL;
 *   ClutterActor *container = NULL;
 *   g_autofree gchar *launcher_video_file = NULL;
 *   g_autofree gchar *content_video_file = NULL;
 *
 *   ThornburyModel *model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_BOOLEAN, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_FLOAT, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      G_TYPE_STRING, NULL,
 *                                                                      -1);
 *   launcher_video_file = g_strdup ("/var/lib/MILDENHALL_extensions/themes/blau/mildenhallroller/01_appl_launcher_2009.03.09.avi");
 *   content_video_file = g_strdup ("/var/lib/MILDENHALL_extensions/themes/blau/mildenhallroller/03_content_roll_2009.03.09.avi");
 *
 *   for (i = 0; i < 3; i++)
 *   {
 *     g_autofree gchar *column_name = g_strdup_printf ("item number %i", i);
 *     g_autofree gchar *column_text = g_strdup_printf (LONG_TEXT, i);
 *
 *     thornbury_model_append (model, COLUMN_NAME, column_name,
 *                            COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
 *                            COLUMN_LABEL, column_name,
 *                            COLUMN_TOGGLE, i % 2,
 *                            COLUMN_VIDEO, i % 2 == 0 ? launcher_video_file : content_video_file,
 *                            COLUMN_EXTRA_HEIGHT, (float) (i % 100),
 *                            COLUMN_COVER, covers[i % 3],
 *                            COLUMN_THUMB,NULL,
 *                            COLUMN_LONG_TEXT, column_text,
 *                            -1);
 *   }
 *   item_factory = thornbury_item_factory_generate_widget_with_props (
 *          MILDENHALL_TYPE_ROLLER_CONTAINER,
 *          "/usr/share/mildenhall/mh_roller_container_prop.json");
 *
 *   g_object_get (item_factory, "object", &object, NULL);
 *   container = CLUTTER_ACTOR (object);
 *
 *   g_object_set (object, "item-type", MILDENHALL_TYPE_SAMPLE_ITEM,
 *                "model", model, NULL);
 *
 *   mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (container),
 *                                             "name", COLUMN_NAME);
 *   mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (container),
 *                                             "label", COLUMN_LABEL);
 *   clutter_actor_add_child (stage, container);
 *
 * ]|
 *
 * Since: 0.3.0
 */

/***********************************************************************************
*	@Filename	:	mildenhall_roller_container.c
*	@Module		:	MildenhallRollerContainer
*	@Project	:	--
*----------------------------------------------------------------------------------
*	@Date		:	18/07/2012
*----------------------------------------------------------------------------------
*	@Description	: 	The basic idea of MildenhallRollerContainer is to customize the
*				mx-roller as per mildenhall requirements.They are,
*				1)Background color for roller
*				2)Arrow at the focus item
*				3)Blurr Effects
*				4)Any addtional overlay upon the roller if any.
*
************************************************************************************
Description of FIXES:
-----------------------------------------------------------------------------------
 	Description				Date			Name
 	----------				----			----
* 1) Added "focus-to-centre" property,		18-dec-2012		Ramya Ajiri
*    wrap on roller
* 2) Added "background" property to
*    enable/disable the roller background.  	11-Jan-2012	    	Abhiruchi
* 3) Added watch to state chnge of scrollview,	17-Jan-2012 		Ramya Ajiri
*    on state change brings
*    relevent row to focus
* 4) Hadjustment and Vadjustment properties	22-jan-2012		Ramya Ajiri
*    added.
* 5) ScrollView property "clamp-to-center"      05-Feb-2012	 	Abhiruchi
*    updated if "roll-over" is FALSE
*    (list roller).
* 6) Added "footer-model" as property		12-Feb-2013		Abhiruchi
* 7) Added signal "roller-footer-clicked"	13-Feb-2013		Abhiruchi
* 8) Wrapped two functions of roller i,e. 	25-feb-2012		Ramya Ajiri
* lightwood_fixed_roller_set_min_visible_actors()
* fixed_roller_set_expanded_item()
* 9) "row-changed" functionality added for 	05-Mar-2013		Abhiruchi
*    footer- model
* 10)deprecated api replaced with new ones:	05-Mar-2013		Abhiruchi
    ->  clutter_actor_remove_child in
	v_ROLLERCONT_set_roller_type()
    ->  clutter_actor_insert_child_at_index in
	roller_container_create_seamless_texture()
* 11)Get exact value for child-height/child-width 23-04-2013		Ramya
*   from roller.
*12)signal ::"item-expanded" connnected only for  10-06-2013		Ramya Ajiri
* fixed roller.
* 13) touch handled for footer clicked		  13-09-2013		Abhiruchi
* 14) roller resize updated based on
      roller updated api			  07-Aug-2014		Abhiruchi 	   
*15)visible-items and first-visible-item
    properties added.           	          13-Nov-2014         Abhiruchi
*16)scroll-finished signal added with visible
    items and first visible item data   	  13-Nov-2014         Abhiruchi

************************************************************************************/

#include "mildenhall-internal.h"
#include "mildenhall_roller_container.h"

#include <math.h>
#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-fixedroller.h"
#include "liblightwood-roller.h"
#include "liblightwood-varroller.h"


/***********************************************************************************
	@Prototypes of local functions
************************************************************************************/
static void roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data);
static void locking_finished_cb (LightwoodRoller *roller, gpointer   data);
static void roller_item_expanded_cb (LightwoodRoller *roller, gboolean isExpanded, gpointer data);
static void scroll_started_cb (LightwoodRoller *roller, gpointer   data);
static void scroll_finished_cb (LightwoodRoller *roller, guint inFirstVisibleItem, guint inTotalvisibleItem, gpointer data);
static void scroll_cb(LightwoodRoller *roller, gpointer   data);
static void row_added_cb (ThornburyModel *model, ThornburyModelIter *iter, MildenhallRollerContainer *container);
static void row_removed_cb (ThornburyModel *model, ThornburyModelIter *iter, MildenhallRollerContainer *container);
static void footer_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRollerContainer *container);
static void create_footer_item(MildenhallRollerContainer *container);

/***********************************************************************************
	@Macro Definitions
************************************************************************************/
#define MILDENHALL_ROLLERCONT_DEBUG(...)   //g_print( __VA_ARGS__)//g_printerr(__VA_ARGS__)/*g_print( __VA_ARGS__)*/

/***********************************************************************************
        @Global variable
************************************************************************************/
static ClutterActor *currentGlowing = NULL;	/* to maintain active roller */


/***********************************************************************************
	@Local type Definitions
************************************************************************************/

/* roller type as GType */
static GType roller_container_get_roller_type (void) G_GNUC_CONST;
#define ROLLER_CONTAINER_TYPE (roller_container_get_roller_type())

enum
{
  LOCKING_FINISHED,
  ITEM_ACTIVATED,
  SCROLL_FINISHED,
  ITEM_EXPANDED,
  SCROLL_STARTED,
  SCROLLING,
  FOOTER_CLICKED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

typedef struct
{
	/* Flag to know arrow to be shown or not */
	gboolean arrowEnable;
	/* abhi: wrapped roller property */
	gboolean bRollover;
	gboolean bCentreFocus;
	gboolean bAnimationMode;
	gboolean bFlowLayout;
	gboolean bMotionBlur;
	gboolean bCylinderDistort;
	gboolean bEnableBg;
	gboolean bScroll;

	ThornburyItemFactory *itemFactory;

	ThornburyModel *model;
	ThornburyModel *footerModel;
	gfloat childHeight;
	gfloat childWidth;
	MildenhallRollerContainerType enRollerType;
	GType itemType;

	/* threshold value for drag */
	guint dragThreshold;
	/* scroll view actor */
	ClutterActor *scrollView;
	ClutterActor *roller;
	ClutterActor *header;
	ClutterActor *footer;
	ClutterActor *footerItem;
	ClutterActor *arrowRight;
	/* abhi: seamless normal texture for inactive roller */
	ClutterActor *seamlessNormalTexture;
	/* abhi: seamless active texture for active roller */
	ClutterActor *seamlessActiveTexture;

	gfloat fWidth;
	gfloat fHeight;
	gfloat fArrowPosition;
	GList *children;

	/* abhi: style fields */
        guint clampMode;
        guint clampDuration;
	gchar *seamlessNormalFilePath;
	gchar *seamlessActiveFilePath;
	gchar *arrowFont;
        gchar *arrowMarkup;
	ClutterColor arrowColor;
	gchar *language;
} MildenhallRollerContainerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallRollerContainer, mildenhall_roller_container, LIGHTWOOD_TYPE_CONTAINER)

enum {
  PROP_NONE,
  PROP_DISPLAY_BACKGROUND,
  PROP_ROLLER,
  PROP_ROLLER_TYPE,	/* roller type: fixed/variable */
  PROP_ITEM_TYPE,		/* roller item type */
  PROP_DRAG_THRESHOLD,	/* read only coming from style */
  PROP_CLAMP_MODE,	/* read only coming from style */
  PROP_CLAMP_DURATION,	/* read only coming from style */
  PROP_ROLL_OVER,
  PROP_FOCUS_TO_CENTER,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_MODEL,
  PROP_FOCUSED_ROW,
  PROP_MOTION_BLUR,
  PROP_CYLINDER_DISTORT,
  PROP_SHOW_ANIMATION,
  PROP_SHOW_FLOW_LAYOUT,	/* fixed roller property */
  PROP_CHILD_HEIGHT,	/* fixed roller property */
  PROP_CHILD_WIDTH,	/* fixed roller property */
  PROP_SHOW_ARROW,
  PROP_ARROW_POSITION,
  PROP_HADJUST,
  PROP_VADJUST,
  PROP_FOOTER_MODEL,
  PROP_ANIMATE_FOOTER,
  PROP_FIRST_VISIBLE_ITEM,
  PROP_VISIBLE_ITEMS,
  PROP_LANGUAGE,
};

/********************************************************
 * Function : roller_container_get_roller_type
 * Description: define roller type enum as GType
 * Parameters: void
 * Return value: void
 ********************************************************/
static GType roller_container_get_roller_type (void)
{
        /* argument must point to a static 0-initialized variable,
         * that will be set to a value other than 0 at the end of
         * the initialization section.
         */
        static volatile gsize gEnumTypeVolatile = 0;

        /* drawer type as enum initialization section */
        if (g_once_init_enter (&gEnumTypeVolatile))
        {
                /* A structure which contains a single flags value, its name, and its nickname. */
                static const GEnumValue arValues[] =
                {
                        { MILDENHALL_FIXED_ROLLER_CONTAINER, "MILDENHALL_FIXED_ROLLER_CONTAINER", "fixed-roller-container" },
                        { MILDENHALL_VARIABLE_ROLLER_CONTAINER, "MILDENHALL_VARIABLE_ROLLER_CONTAINER", "variable-roller-container" },
                        { 0, NULL, NULL }
                };

                GType gEnumType;

                /* Registers a new static flags type with the name name. */
                gEnumType = g_enum_register_static (g_intern_static_string ("enMildenhallRollerContainerType"), arValues);

                /* In combination with g_once_init_leave() and the unique address value_location,
                 * it can be ensured that an initialization section will be executed only once
                 * during a program's life time, and that concurrent threads are blocked
                 * until initialization completed.
                 */
                g_once_init_leave (&gEnumTypeVolatile, gEnumType);
        }

        return gEnumTypeVolatile;
}

static void
roller_container_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	MildenhallRollerContainer *container = MILDENHALL_ROLLER_CONTAINER (object);
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);
	MxAdjustment *adjustment;
	gint inItem;
	switch (property_id)
	{
		case PROP_ROLLER:
			g_value_set_object (value, priv->roller);
			break;
		case PROP_DISPLAY_BACKGROUND:
			g_value_set_boolean(value, priv->bEnableBg);
			break;
		case PROP_ROLLER_TYPE:
			g_value_set_enum(value, priv->enRollerType);
			break;
		case PROP_ITEM_TYPE:
			 g_value_set_gtype(value, priv->itemType);
			break;
		case PROP_SHOW_ARROW:
			g_value_set_boolean(value, priv->arrowEnable);
			break;
		case PROP_ARROW_POSITION:
			g_value_set_float(value, priv->fArrowPosition);
			break;
		case PROP_ROLL_OVER:
			g_value_set_boolean(value, priv->bRollover);
			break;
		case PROP_FOCUS_TO_CENTER:
			g_value_set_boolean(value, priv->bCentreFocus);
			break;
		case PROP_MODEL:
			g_value_set_object (value, priv->model);
			break;
		case PROP_FOCUSED_ROW:
			g_value_set_int (value, mildenhall_roller_container_get_focused_row (container));
			break;
		case PROP_MOTION_BLUR:
			g_value_set_boolean (value, priv->bMotionBlur);
			break;
		case PROP_CYLINDER_DISTORT:
			g_value_set_boolean (value, priv->bCylinderDistort);
			break;
		case PROP_SHOW_ANIMATION:
			g_value_set_boolean (value, priv->bAnimationMode);
			break;
		case PROP_CLAMP_MODE:
			g_value_set_int (value, priv->clampMode);
			break;
		case PROP_CLAMP_DURATION:
			g_value_set_int (value, priv->clampDuration);
			break;
		case PROP_DRAG_THRESHOLD:
			g_value_set_int(value, priv->dragThreshold);
			break;
		 case PROP_WIDTH:
			g_value_set_float (value, priv->fWidth);
			break;
		/* fixed roller property */
		 case PROP_SHOW_FLOW_LAYOUT:
			g_value_set_boolean (value, priv->bFlowLayout);
			break;
		 case PROP_HEIGHT:
			g_value_set_float (value, priv->fHeight);
			break;
		 case PROP_CHILD_HEIGHT:
			g_object_get(G_OBJECT(priv->roller),"child-height",&priv->childHeight,NULL);
			g_value_set_float (value,  priv->childHeight);
			break;
		 case PROP_CHILD_WIDTH:
			g_object_get(G_OBJECT(priv->roller),"child-width",&priv->childWidth,NULL);
                        g_value_set_float (value, priv->childWidth);
			break;
		case PROP_HADJUST:
			mx_scrollable_get_adjustments (MX_SCROLLABLE (priv->roller),
								&adjustment, NULL);
			g_value_set_object (value, adjustment);
			break;
		case PROP_VADJUST:
			{
			mx_scrollable_get_adjustments (MX_SCROLLABLE (priv->roller),
								 NULL,&adjustment);
			g_value_set_object (value, adjustment);
			}
			break;
		case PROP_FOOTER_MODEL:
                        g_value_set_object (value, priv->footerModel);
                        break;
		case PROP_FIRST_VISIBLE_ITEM:
		{
		        g_object_get(priv->roller, "first-visible-item", &inItem, NULL);
            		g_value_set_int(value, inItem);
		}
            		break;
        	case PROP_VISIBLE_ITEMS:
		{
            		g_object_get(priv->roller, "visible-items", &inItem, NULL);
            		g_value_set_int(value, inItem);
		}
            		break;
                case PROP_LANGUAGE:
                        g_value_set_string (value, priv->language);
                break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/**
 * mildenhall_roller_container_get_focus_to_center:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:focus-to-center property
 *
 * Returns: the value of #MildenhallRollerContainer:focus-to-center property
 */
gboolean
mildenhall_roller_container_get_focus_to_center (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bCentreFocus;
}

/**
 * mildenhall_roller_container_set_focus_to_center:
 * @self: a #MildenhallRollerContainer
 * @enable: the new value
 *
 * Update #MildenhallRollerContainer:focus-to-center property
 */
void
mildenhall_roller_container_set_focus_to_center (MildenhallRollerContainer *self,
                                                 gboolean enable)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bCentreFocus == enable)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "focus-to-centre", enable, NULL);

  priv->bCentreFocus = enable;
  g_object_notify (G_OBJECT (self), "focus-to-center");
}

/**
 * mildenhall_roller_container_set_roll_over:
 * @self: a #MildenhallRollerContainer
 * @enable: the new value
 *
 * Update #MildenhallRollerContainer:roll-over property
 */
void
mildenhall_roller_container_set_roll_over (MildenhallRollerContainer *self,
                                           gboolean enable)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bRollover == enable)
    return;

  if (priv->roller != NULL && priv->fHeight > 0.0)
    g_object_set (priv->roller, "roll-over", enable, NULL);

  if (priv->scrollView != NULL)
    g_object_set (priv->scrollView, "clamp-to-center", enable, NULL);

  priv->bRollover = enable;
  g_object_notify (G_OBJECT (self), "roll-over");
}

/**
 * mildenhall_roller_container_set_roller_type:
 * @self: a #MildenhallRollerContainer
 * @roller_type: the new value
 *
 * Update #MildenhallRollerContainer:roller-type property
 */
void
mildenhall_roller_container_set_roller_type (MildenhallRollerContainer *self,
                                             MildenhallRollerContainerType roller_type)
{
  GType lightwood_roller_type;
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));
  g_return_if_fail (roller_type == MILDENHALL_FIXED_ROLLER_CONTAINER
                 || roller_type == MILDENHALL_VARIABLE_ROLLER_CONTAINER);

  if (priv->enRollerType == roller_type)
    return;

  if (priv->roller != NULL)
    {
      clutter_actor_remove_child (priv->scrollView, priv->roller);
      clutter_actor_destroy (priv->roller);
    }

  if (roller_type == MILDENHALL_FIXED_ROLLER_CONTAINER)
    {
      lightwood_roller_type = LIGHTWOOD_TYPE_FIXED_ROLLER;
    }
  else
    {
      lightwood_roller_type = LIGHTWOOD_TYPE_VARIABLE_ROLLER;
    }

  priv->roller = g_object_new (lightwood_roller_type, "width", priv->fWidth, "height", priv->fHeight, NULL);

  g_signal_connect (G_OBJECT (priv->roller), "item-activated",
                    G_CALLBACK (roller_item_activated_cb),
                    self);

  if (roller_type == MILDENHALL_FIXED_ROLLER_CONTAINER)
    {
      g_signal_connect (G_OBJECT (priv->roller), "item-expanded",
                        G_CALLBACK (roller_item_expanded_cb),
                        self);
    }

  g_signal_connect (G_OBJECT (priv->roller), "locking-finished",
                    G_CALLBACK (locking_finished_cb),
                    self);

  g_signal_connect (G_OBJECT (priv->roller), "scroll-started",
                    G_CALLBACK (scroll_started_cb),
                    self);

  g_signal_connect (G_OBJECT (priv->roller), "scrolling",
                    G_CALLBACK (scroll_cb),
                    self);

  g_signal_connect (G_OBJECT (priv->roller), "scroll-finished",
                    G_CALLBACK (scroll_finished_cb),
                    self);

  if (priv->scrollView != NULL)
    clutter_actor_add_child (priv->scrollView, priv->roller);

  priv->enRollerType = roller_type;
  g_object_notify (G_OBJECT (self), "roller-type");
}

/**
 * mildenhall_roller_container_set_item_type:
 * @self: a #MildenhallRollerContainer
 * @item_type: the new value
 *
 * Update #MildenhallRollerContainer:item-type property
 */
void
mildenhall_roller_container_set_item_type (MildenhallRollerContainer *self,
                                           GType item_type)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
  
  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->itemType == item_type)
    return;

  g_clear_object (&priv->itemFactory);
  priv->itemFactory = g_object_new (THORNBURY_TYPE_ITEM_FACTORY,
                                    "item-type", item_type,
                                    NULL);

  if (priv->roller != NULL)
    g_object_set (priv->roller, "factory", priv->itemFactory, NULL);

  priv->itemType = item_type;
  g_object_notify (G_OBJECT (self), "item-type");
}

/**
 * mildenhall_roller_container_set_motion_blur:
 * @self: a #MildenhallRollerContaine
 * @motion_blur: the new value
 *
 * Update #MildenhallRollerContainer:motion-blur property
 */
void
mildenhall_roller_container_set_motion_blur (MildenhallRollerContainer *self,
                                             gboolean motion_blur)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
  
  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bMotionBlur == motion_blur)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "motion-blur", motion_blur, NULL);

  priv->bMotionBlur = motion_blur;
  g_object_notify (G_OBJECT (self), "motion-blur");
}

/**
 * mildenall_roller_container_set_cylinder_distort:
 * @self: a #MildenhallRollerContainer
 * @cylinder_distort: the new value
 *
 * Update #MildenhallRollerContainer:cylinder-distort property
 */
void
mildenall_roller_container_set_cylinder_distort (MildenhallRollerContainer *self,
                                                 gboolean cylinder_distort)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
  
  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bCylinderDistort == cylinder_distort)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "cylinder-distort", cylinder_distort, NULL);

  priv->bCylinderDistort = cylinder_distort;
  g_object_notify (G_OBJECT (self), "cylinder-distort");
}

/**
 * mildenhall_roller_container_set_show_animation:
 * @self: a #MildenhallRollerContainer
 * @animate: the new value
 *
 * Update #MildenhallRollerContainer:show-animation property
 */
void
mildenhall_roller_container_set_show_animation (MildenhallRollerContainer *self,
                                                gboolean animate)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
  
  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bAnimationMode == animate)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "animation-mode", animate, NULL);

  priv->bAnimationMode = animate;
  g_object_notify (G_OBJECT (self), "show-animation");
}

/**
 * mildenhall_roller_container_set_model:
 * @self: a #MildenhallRollerContainer
 * @model: (transfer none): the new model
 * 
 * Update #MildenhallRollerContainer:model property
 */
void
mildenhall_roller_container_set_model (MildenhallRollerContainer *self,
                                       ThornburyModel *model)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));
  /* FIXME: ThornburyModel is actually ClutterListModel which is deprecated. T808 */
  g_return_if_fail (THORNBURY_IS_MODEL (model) || CLUTTER_IS_LIST_MODEL (model));

  if (priv->roller == NULL)
    return;

  if (priv->model == model)
    return;

  g_object_set (priv->roller, "model", model, NULL);

  if (priv->model != NULL)
    {
      g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (row_added_cb),
					    priv->roller);

      g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (row_removed_cb),
                                            priv->roller);
      g_object_unref (priv->model);
    }

  g_signal_connect (model, "row-added",
                    G_CALLBACK (row_added_cb),
                    self);

  g_signal_connect (model, "row-removed",
                    G_CALLBACK (row_removed_cb),
                    self);

  priv->model = g_object_ref (model);

  if (priv->itemFactory != NULL)
    g_object_set (priv->itemFactory, "model", priv->model, NULL);
  g_object_notify (G_OBJECT (self), "model");
}

/**
 * mildenhall_roller_container_set_focused_row:
 * @self: a #MildenhallRollerContainer
 * @focused_row: row to focus
 * @animate: whether to move to the new position with an animation
 *
 * Scroll so @focused_row is placed in the middle.
 */
void
mildenhall_roller_container_set_focused_row (MildenhallRollerContainer *self,
                                             gint focused_row,
                                             gboolean animate)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  lightwood_roller_set_focused_row (LIGHTWOOD_ROLLER (priv->roller), focused_row, animate);
}

/**
 * mildenhall_roller_container_set_show_flow_layout:
 * @self: a #MildenhallRollerContainer
 * @show: the new value
 *
 * Update #MildenhallRollerContainer:show-flow-layout property
 */
void
mildenhall_roller_container_set_show_flow_layout (MildenhallRollerContainer *self,
                                                  gboolean show)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
  
  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bFlowLayout == show)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "flow-layout", show, NULL);

  priv->bFlowLayout = show;
  g_object_notify (G_OBJECT (self), "show-flow-layout");
}


/**
 * v_ROLLERCONT_animate_footer:
 * @container: Object reference
 * @inAnimatefooter: Value which need to be set for roller footer to animate
 *
 * Set the value to TRUE to animate roller footer
 */
void
v_ROLLERCONT_animate_footer(MildenhallRollerContainer *container,
                                guint inAnimatefooter)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);
	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_ROLLER_CONTAINER ( container ) );

        if(priv->footerItem != NULL )
        {
                gfloat fltY, fltHeight;
        	MILDENHALL_ROLLERCONT_DEBUG("%s  %d\n", __FUNCTION__ , __LINE__);

        	fltY = clutter_actor_get_y(CLUTTER_ACTOR(priv->footerItem));
        	fltHeight = clutter_actor_get_height(CLUTTER_ACTOR(priv->footerItem));

        	clutter_actor_set_y(CLUTTER_ACTOR(priv->footerItem) , fltY + fltHeight);

        	clutter_actor_save_easing_state(CLUTTER_ACTOR(priv->footerItem));
        	clutter_actor_set_easing_mode (CLUTTER_ACTOR(priv->footerItem), inAnimatefooter);
        	clutter_actor_set_easing_duration (CLUTTER_ACTOR(priv->footerItem), 500);
        	clutter_actor_set_x (CLUTTER_ACTOR(priv->footerItem), 0);
        	clutter_actor_set_y (CLUTTER_ACTOR(priv->footerItem), 0);
        	clutter_actor_restore_easing_state (CLUTTER_ACTOR(priv->footerItem));
        }
}




/**
 * v_ROLLERCONT_set_footer_model:
 * @container: Object reference
 * @footerModel: Value which need to be set for roller footer
 *
 * Set the model used by the roller footer
 */
void
v_ROLLERCONT_set_footer_model(MildenhallRollerContainer *container,
                                ThornburyModel *footerModel)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);
	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_ROLLER_CONTAINER ( container ) );

	if(NULL == footerModel)
        {
                if(NULL != priv->footerModel)
                {
                        g_signal_handlers_disconnect_by_func (priv->footerModel,
                                        G_CALLBACK (footer_row_changed_cb),
                                        container);
                }
        }
        else
        {
                if(NULL != priv->footerModel && priv->footerModel != footerModel)
                {
                        g_signal_handlers_disconnect_by_func (priv->footerModel,
                                        G_CALLBACK (footer_row_changed_cb),
                                        container);
                        g_signal_connect (footerModel,
                                        "row-changed",
                                        G_CALLBACK (footer_row_changed_cb),
                                        container);
                }
                else if(NULL == priv->footerModel)
                {
                        g_signal_connect (footerModel,
                                        "row-changed",
                                        G_CALLBACK (footer_row_changed_cb),
                                        container);
                }
        }
        priv->footerModel = footerModel;

	/* create footer item */
	create_footer_item(container);

	g_object_notify (G_OBJECT (container), "footer-model");
}

/**
 * mildenhall_roller_container_set_width:
 * @self: a #MildenhallRollerContainer
 * @width: the new value
 *
 * Update #MildenhallRollerContainer:width property
 *
 */
void
mildenhall_roller_container_set_width (MildenhallRollerContainer *self,
                                        gfloat width)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->fWidth == width)
    return;

  clutter_actor_set_width (CLUTTER_ACTOR (self), width);

  if (priv->footer != NULL)
    {
      if (clutter_actor_has_clip (priv->footer)) 
        clutter_actor_remove_clip (priv->footer);

      clutter_actor_set_clip (priv->footer,
                              0, 0,
                              width, clutter_actor_get_height (priv->footer));
    }

  priv->fWidth = width;
  g_object_notify (G_OBJECT (self), "width");
}

/**
 * mildenhall_roller_container_set_height:
 * @self: a #MildenhallRollerContainer
 * @height: the new value
 *
 * Update #MildenhallRollerContainer:height property
 */
void
mildenhall_roller_container_set_height (MildenhallRollerContainer *self,
                                        gfloat height)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->fHeight == height)
    return;

  if (priv->roller != NULL)
    {
      g_object_set (priv->roller, "height", height, NULL);
    }

  if (height > 0.0)
    {
      g_object_set (self, "roll-over", priv->bRollover, NULL);
    }

  priv->fHeight = height;
  g_object_notify (G_OBJECT (self), "height");
}

/**
 * mildenhall_roller_container_set_child_width:
 * @self: a #MildenhallRollerContainer
 * @width: the new value
 *
 * Update #MildenhallRollerContainer:child-width property
 */
void
mildenhall_roller_container_set_child_width (MildenhallRollerContainer *self,
                                             gfloat width)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->childWidth == width)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "child-width", width, NULL);

  priv->childWidth = width;
  g_object_notify (G_OBJECT (self), "child-width");
}

/**
 * mildenhall_roller_container_set_child_height:
 * @self: a #MildenhallRollerContainer
 * @height: the new value
 *
 * Update #MildenhallRollerContainer:child-height property
 */
void
mildenhall_roller_container_set_child_height (MildenhallRollerContainer *self,
                                              gfloat height)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->childWidth == height)
    return;

  if (priv->roller != NULL)
    g_object_set (priv->roller, "child-height", height, NULL);

  priv->childHeight = height;
  g_object_notify (G_OBJECT (self), "child-height");
}

/**
 * mildenhall_roller_container_get_show_arrow:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:show-arrow property
 *
 * Returns: the value of #MildenhallRollerContainer:show-arrow property
 */
gboolean
mildenhall_roller_container_get_show_arrow (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->arrowEnable;
}

/**
 * mildenhall_roller_container_set_show_arrow:
 * @self: a #MildenhallRollerContainer
 * @enable: the new value
 *
 * Update #MildenhallRollerContainer:show-arrow property
 */
void
mildenhall_roller_container_set_show_arrow (MildenhallRollerContainer *self,
                                            gboolean enable)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->arrowEnable == enable)
    return;

  if (priv->roller != NULL)
    {
      enable ? clutter_actor_show (priv->arrowRight) :
               clutter_actor_hide (priv->arrowRight);
    }

  priv->arrowEnable = enable;
  g_object_notify (G_OBJECT (self), "show-arrow");
}

/**
 * mildenhall_roller_container_set_arrow_position:
 * @self: a #MildenhallRollerContainer
 * @pos: the new value
 *
 * Update #MildenhallRollerContainer:arrow-position property
 */
void
mildenhall_roller_container_set_arrow_position (MildenhallRollerContainer *self,
                                                gfloat pos)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->fArrowPosition == pos)
    return;

  priv->fArrowPosition = pos;
  g_object_notify (G_OBJECT (self), "arrow-position");
}

/**
 * mildenhall_roller_container_get_arrow_position:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:arrow-position property
 *
 * Returns: the value of #MildenhallRollerContainer:arrow-position property
 */
gfloat
mildenhall_roller_container_get_arrow_position (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), -1.0);

  return priv->fArrowPosition;
}

/**
 * mildenhall_roller_container_set_display_background:
 * @self: a #MildenhallRollerContainer
 * @display: the new value
 *
 * Update #MildenhallRollerContainer:display-background property
 */
void
mildenhall_roller_container_set_display_background (MildenhallRollerContainer *self,
                                                    gboolean display)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->bEnableBg == display)
    return;


  if (priv->seamlessNormalTexture != NULL)
    {
      clutter_actor_set_opacity (priv->seamlessActiveTexture, display ? 255 : 0);
      clutter_actor_set_opacity (priv->seamlessNormalTexture, display ? 255 : 0);
    }

  priv->bEnableBg = display;
  g_object_notify (G_OBJECT (self), "display-background");
}

/**
 * mildenhall_roller_container_get_drag_threshold:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:drag-threshold property
 *
 * Returns: the value of #MildenhallRollerContainer:drag-threshold property
 */
guint
mildenhall_roller_container_get_drag_threshold (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), 0);

  return priv->dragThreshold;
}

/**
 * mildenhall_roller_container_get_roller_type:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:roller-type property
 *
 * Returns: the value of #MildenhallRollerContainer:roller-type property
 */
MildenhallRollerContainerType
mildenhall_roller_container_get_roller_type (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), MILDENHALL_FIXED_ROLLER_CONTAINER); 

  return priv->enRollerType;
}

/**
 * mildenhall_roller_container_get_item_type:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:item-type property
 *
 * Returns: the value of #MildenhallRollerContainer:item-type property
 */
GType
mildenhall_roller_container_get_item_type (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), G_TYPE_NONE);

  return priv->itemType;
}

/**
 * mildenhall_roller_container_get_roll_over:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:roll-over property
 *
 * Returns: the value of #MildenhallRollerContainer:roll-over property
 */
gboolean
mildenhall_roller_container_get_roll_over (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bRollover;
}

/**
 * mildenhall_roller_container_get_cylinder_distort:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:cylinder-distort property
 *
 * Returns: the value of #MildenhallRollerContainer:cylinder-distort property
 */
gboolean
mildenhall_roller_container_get_cylinder_distort (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bCylinderDistort;
}

/**
 * mildenhall_roller_container_get_motion_blur:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:motion-blur property
 *
 * Returns: the value of #MildenhallRollerContainer:motion-blur property
 */
gboolean
mildenhall_roller_container_get_motion_blur (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bMotionBlur;
}

/**
 * mildenhall_roller_container_get_show_animation:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:show-animation property
 *
 * Returns: the value of #MildenhallRollerContainer:show-animation property
 */
gboolean
mildenhall_roller_container_get_show_animation (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bAnimationMode;
}

/**
 * mildenhall_roller_container_get_show_flow_layout:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:show-flow-layout property
 *
 * Returns: the value of #MildenhallRollerContainer:show-flow-layout property
 */
gboolean
mildenhall_roller_container_get_show_flow_layout (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bFlowLayout;
}

/**
 * mildenhall_roller_container_get_model:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:model property
 *
 * Returns: (transfer none) (nullable): a #ThurnburyModel
 */
ThornburyModel *
mildenhall_roller_container_get_model (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), NULL);

  return priv->model;
}

/**
 * mildenhall_roller_container_get_focused_row:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:focused-row property
 *
 * Returns: the value of #MildenhallRollerContainer:focused-row property
 */
gint
mildenhall_roller_container_get_focused_row (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), -1);

  return lightwood_roller_get_focused_row (LIGHTWOOD_ROLLER (priv->roller));
}

/**
 * mildenhall_roller_container_get_width:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:width property
 *
 * Returns: the value of #MildenhallRollerContainer:width property
 */
gfloat
mildenhall_roller_container_get_width (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), -1.0);

  return priv->fWidth;
}

/**
 * mildenhall_roller_container_get_height:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:height property
 *
 * Returns: the value of #MildenhallRollerContainer:height property
 */
gfloat
mildenhall_roller_container_get_height (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), 0.0);

  return priv->fHeight;
}

/**
 * mildenhall_roller_container_get_child_width:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:child-width property
 *
 * Returns: the value of #MildenhallRollerContainer:child-width property
 */
gfloat
mildenhall_roller_container_get_child_width (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), -1.0);

  return priv->childWidth;
}

/**
 * mildenhall_roller_container_get_child_height:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:child-height property
 *
 * Returns: the value of #MildenhallRollerContainer:child-height property
 */
gfloat
mildenhall_roller_container_get_child_height (MildenhallRollerContainer *self)
{

  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), -1.0);

  return priv->childHeight;
}

/**
 * mildenhall_roller_container_get_display_background:
 * @self: a #MildenhallRollerContainer
 *
 * Return the #MildenhallRollerContainer:display-background property
 *
 * Returns: the value of #MildenhallRollerContainer:display-background property
 */
gboolean
mildenhall_roller_container_get_display_background (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), FALSE);

  return priv->bEnableBg;
}

static void
roller_container_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
	MildenhallRollerContainer *self = MILDENHALL_ROLLER_CONTAINER (object);
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

	switch (property_id)
	{
		# if 0
		case PROP_ROLLER:
			v_ROLLERCONT_set_roller(MILDENHALL_ROLLER_CONTAINER (object),
							g_value_get_object (value));
			break;
		# endif
		case PROP_DISPLAY_BACKGROUND:
			mildenhall_roller_container_set_display_background (self, g_value_get_boolean (value));
			break;
		case PROP_ROLLER_TYPE:
			mildenhall_roller_container_set_roller_type (self, g_value_get_enum (value));
			break;
		case PROP_ITEM_TYPE:
			mildenhall_roller_container_set_item_type (self, g_value_get_gtype(value));
			break;
		case PROP_ROLL_OVER:
			mildenhall_roller_container_set_roll_over (self, g_value_get_boolean (value));
			break;
		case PROP_FOCUS_TO_CENTER:
			mildenhall_roller_container_set_focus_to_center (self,
                                        g_value_get_boolean (value));
			break;
		case PROP_MODEL:
			mildenhall_roller_container_set_model (self, g_value_get_object (value));
			break;
		case PROP_FOCUSED_ROW:
			mildenhall_roller_container_set_focused_row (self,
					g_value_get_int (value), TRUE);
			break;
		case PROP_MOTION_BLUR:
			 mildenhall_roller_container_set_motion_blur (self,
					g_value_get_boolean (value));
			break;
		case PROP_CYLINDER_DISTORT:
			mildenall_roller_container_set_cylinder_distort (self,
					g_value_get_boolean (value));
			break;
		case PROP_SHOW_ANIMATION:
			mildenhall_roller_container_set_show_animation (self,
					g_value_get_boolean (value));
			break;
		case PROP_CLAMP_MODE:
			break;
		case PROP_CLAMP_DURATION:
			break;
		case PROP_DRAG_THRESHOLD:
			break;
		case PROP_SHOW_ARROW:
			mildenhall_roller_container_set_show_arrow (self, g_value_get_boolean (value));
			break;
		case PROP_ARROW_POSITION:
			mildenhall_roller_container_set_arrow_position (self, g_value_get_float (value));
			break;
		case PROP_WIDTH:
			mildenhall_roller_container_set_width (self, g_value_get_float (value));
			break;
		/* fixed roller property */
		case PROP_SHOW_FLOW_LAYOUT:
			mildenhall_roller_container_set_show_flow_layout (self, g_value_get_boolean (value));
			break;
		case PROP_HEIGHT:
			mildenhall_roller_container_set_height (self, g_value_get_float (value));

			break;
		case PROP_CHILD_HEIGHT:
			mildenhall_roller_container_set_child_height (self, g_value_get_float (value));
			break;
		case PROP_CHILD_WIDTH:
			mildenhall_roller_container_set_child_width (self, g_value_get_float (value));
			break;
		case PROP_VADJUST:
			mx_scrollable_set_adjustments(MX_SCROLLABLE (priv->roller),
							NULL,(MxAdjustment *)g_value_get_object (value));
			break;
		case PROP_HADJUST:
			mx_scrollable_set_adjustments(MX_SCROLLABLE (priv->roller),
							(MxAdjustment *)g_value_get_object (value),NULL);
			break;
		 case PROP_FOOTER_MODEL:
                        v_ROLLERCONT_set_footer_model (MILDENHALL_ROLLER_CONTAINER (object),
                                        g_value_get_object (value));
                        break;
		 case PROP_ANIMATE_FOOTER:
		 {
			 v_ROLLERCONT_animate_footer (MILDENHALL_ROLLER_CONTAINER (object),
			                                        g_value_get_uint (value));

		 }
			 break;
                case PROP_LANGUAGE:
                        mildenhall_roller_container_set_language (self, g_value_get_string (value));
                        break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
roller_container_dispose (GObject *object)
{
	MildenhallRollerContainer *self = MILDENHALL_ROLLER_CONTAINER (object);
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

	g_clear_object (&priv->itemFactory);
        g_clear_pointer (&priv->language, g_free);

	if(NULL != priv->seamlessNormalFilePath)
	{
		g_free(priv->seamlessNormalFilePath);
		priv->seamlessNormalFilePath = NULL;
	}
        if(NULL != priv->seamlessActiveFilePath)
	{
		g_free(priv->seamlessActiveFilePath);
		priv->seamlessActiveFilePath = NULL;
	}
	if(NULL != priv->arrowFont)
	{
		g_free(priv->arrowFont);
		priv->arrowFont = NULL;
	}
	if(NULL != priv->arrowMarkup)
	{
		g_free(priv->arrowMarkup);
		priv->arrowMarkup = NULL;
	}
	/*if(NULL != priv->itemTypeName)
	{
		g_free(priv->itemTypeName);
		priv->itemTypeName = NULL;
	}*/

	G_OBJECT_CLASS (mildenhall_roller_container_parent_class)->dispose (object);
}

static void
roller_container_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_roller_container_parent_class)->finalize (object);
}

static void
actor_paint (ClutterActor *actor)
{
	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);

	CLUTTER_ACTOR_CLASS (mildenhall_roller_container_parent_class)->paint (actor);
}
static void
_allocate_arrow(ClutterActor *actor,ClutterAllocationFlags flags)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (actor));
	if((priv->arrowEnable == TRUE) && (priv->arrowRight != NULL))
	{
		ClutterActorBox childbox;
                if(priv->fWidth != 0.00  || priv->fHeight != 0.0)
                {
			if(priv->fArrowPosition <= 0.0)
				priv->fArrowPosition = priv->fWidth - 20;
                        childbox.x1 = priv->fArrowPosition;
                        childbox.x2 = priv->fArrowPosition + 20;
                        childbox.y1 = (priv->fHeight / 2 ) - 17;
                        childbox.y2 = (priv->fHeight / 2 ) + 4;
                        clutter_actor_allocate(priv->arrowRight,&childbox,flags);
                }
	}
}

static void
_allocate_scrollview(ClutterActor *actor,ClutterAllocationFlags flags)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (actor));
	if(priv->roller != NULL)
	{
		ClutterActorBox scrollBox;
		scrollBox.y1 = 0.0;
		scrollBox.y2 = priv->fHeight;
		scrollBox.x1 = 0.0;
		scrollBox.x2 = priv->fWidth;
		clutter_actor_allocate (priv->scrollView, &scrollBox, flags);
	}

}

static void
_allocate_background_texture(ClutterActor *actor,ClutterAllocationFlags flags)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (actor));
	/* abhi: allocation box for normal/actvie seamless texture */
	ClutterActorBox imageBox;
	imageBox.y1 = 0.0;
	imageBox.y2 = priv->fHeight;
	imageBox.x1 = 0.0;
	imageBox.x2 = priv->fWidth;

	if(NULL != priv->seamlessNormalTexture)// && CLUTTER_ACTOR_IS_VISIBLE(priv->seamlessNormalTexture) )
    {
                clutter_actor_allocate (priv->seamlessNormalTexture, &imageBox, flags);
	}
	if(NULL != priv->seamlessActiveTexture)// && CLUTTER_ACTOR_IS_VISIBLE(priv->seamlessActiveTexture) )
	{
		clutter_actor_allocate (priv->seamlessActiveTexture, &imageBox, flags);
    }

}

static void
_allocate_footer(ClutterActor *actor,ClutterAllocationFlags flags)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (actor));
	if(NULL != priv->footer)
	{
		ClutterActorBox footerBox;
		footerBox.y1 =394.0;
		footerBox.y2 = footerBox.y1 + clutter_actor_get_height(priv->footer);
		footerBox.x1 = 0.0;
		footerBox.x2 = priv->fWidth;
		clutter_actor_allocate (priv->footer, &footerBox, flags);
	}

}

static void
actor_allocate (ClutterActor          *actor,
                    const ClutterActorBox *box,
                    ClutterAllocationFlags flags)
{
        ClutterActorClass *klass;
	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);

	klass = CLUTTER_ACTOR_CLASS (mildenhall_roller_container_parent_class);

	klass->allocate (actor, box, flags);
	_allocate_arrow(actor,flags);
	_allocate_scrollview(actor,flags);

	_allocate_background_texture(actor,flags);
	_allocate_footer(actor,flags);

}

static void
mildenhall_roller_container_class_init (MildenhallRollerContainerClass *klass)
{
	GParamSpec *pspec;
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

	object_class->get_property = roller_container_get_property;
	object_class->set_property = roller_container_set_property;
	object_class->dispose = roller_container_dispose;
	object_class->finalize = roller_container_finalize;

	actor_class->paint = actor_paint;
	actor_class->allocate = actor_allocate;

	/* defining the Property */
        /**
         * MildenhallRollerContainer:display-background:
         *
         * get/sets the property value for background to roller
         */
	pspec = g_param_spec_boolean ("display-background",
			"background",
			"Whether showing container background",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_DISPLAY_BACKGROUND, pspec);

        /**
         * MildenhallRollerContainer:roller:
         *
         * get/sets the model property to the Roller
         */
	pspec = g_param_spec_object ("roller",
			"Roller",
			"Roller Object",
			CLUTTER_TYPE_ACTOR,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ROLLER, pspec);

        /**
         * MildenhallRollerContainer:roller-type:
         *
         * Type of the roller to be created:
         *              LIGHTWOOD_FIXED_ROLLER
         *              MILDENHALL_VARIABLE_ROLLER
         * Default: LIGHTWOOD_FIXED_ROLLER
         */
        pspec = g_param_spec_enum ("roller-type",
                        "RollerType",
                        "Roller Type to create the roller",
                        ROLLER_CONTAINER_TYPE,
                        MILDENHALL_FIXED_ROLLER_CONTAINER,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_ROLLER_TYPE, pspec);

        /**
         * MildenhallRollerContainer:item-type:
         *
         * get/sets the model property to the Roller
         */
	pspec = g_param_spec_gtype ("item-type",
                        "ItemType",
                        "Item Type for the roller item factory",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ITEM_TYPE, pspec);

        /**
         * MildenhallRollerContainer:show-arrow:
         *
         * get/sets the property value for arrow to roller.
         * It will be located in the middle of the roller container.
         */
	pspec = g_param_spec_boolean ("show-arrow",
			"arrow",
			"mid-arrow of the roller",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_SHOW_ARROW, pspec);

        /**
         * MildenhallRollerContainer: arrow-position:
         *
         * get/sets the property value for arrow position to roller
         */
	pspec = g_param_spec_float ("arrow-position",
                        "arrowPosition",
                        "Position for mid-arrow of the roller",
                        -G_MAXFLOAT,
                        G_MAXFLOAT,
                        0.0,
                        G_PARAM_READWRITE);
    g_object_class_install_property (object_class, PROP_ARROW_POSITION, pspec);

        /**
         * MildenhallRollerContainer:drag-threshold:
         *
         * Pixel threshold to exceed before initiating a drag event 
         */
	pspec = g_param_spec_uint ("drag-threshold",
			"dragthreshold",
			"Pixel threshold to exceed before initiating a drag event",
			0, G_MAXUINT, 250,
			G_PARAM_READABLE);
	g_object_class_install_property (object_class, PROP_DRAG_THRESHOLD, pspec);

        /**
         * MildenhallRollerContainer:roll-over:
         *
         * get/sets value Roll over to the start when the end is reached
         */
	pspec = g_param_spec_boolean ("roll-over",
                                "Roll over",
                                "Roll over to the start when the end is reached",
                                FALSE,
                                G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ROLL_OVER, pspec);

	/**
	 * MildenhallRollerContainer:focus-to-center:
	 *
	 * get/sets value for focus-to-center,
         * if TRUE First item will be arranged at the mid of the roller
	 */
	pspec = g_param_spec_boolean ("focus-to-center",
                                "Focus to center",
                                "First item will be arranged at the mid of the roller",
                                FALSE,
                                G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_FOCUS_TO_CENTER, pspec);

        /**
         * MildenhallRollerContainer:focused-row:
         *
         */
	pspec = g_param_spec_int ("focused-row",
			"Focused row",
			"Row currently focused",
			-1,
			G_MAXINT,
			-1,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_FOCUSED_ROW, pspec);

        /**
         * MildenhallRollerContainer:model:
         *
         * Model data to the roller.Data to roller through the model is given only with the understanding of the roller item.
         * It is recommended to write your own roller item type according to your requirement.
         * Please refer for creation of the roller item in CODE EXAMPLES-Roller 
         *	 
         * Please refer for creation of the model CODE EXAMPLES-Roller
         * Example: For below shown roller item model can be written like this.
         *
         */
	pspec = g_param_spec_object ("model",
			"Model",
			"Model to use to construct the items",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MODEL, pspec);

        /**
         * MildenhallRollerContainer:motion-blur:
         *
         * Enables motion blur while scrolling
         *
         */
	pspec = g_param_spec_boolean ("motion-blur",
			"Motion blur",
			"Enable motion blur while scrolling",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MOTION_BLUR, pspec);

        /**
         * MildenhallRollerContainer:cylinder-distort:
         *
         * To enable cylinder distort effect while scrolling
         *
         */
	pspec = g_param_spec_boolean ("cylinder-distort",
			"Cylinder distort",
			"Enable cylinder distort effect while scrolling",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CYLINDER_DISTORT, pspec);

        /**
         * MildenhallRollerContainer:show-animation:
         *
         */
	pspec = g_param_spec_boolean ("show-animation",
                                "Animation mode",
                                "Allocates children based on their fixed size "
                                "and position, assures children actors won't "
                                "be repointed to other rows",
				FALSE,
				G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_SHOW_ANIMATION, pspec);

        /**
         * MildenhallRollerContainer:clamp-duration:
         *
         * Duration for the adjustment clamp animation.
         *
         */
	pspec = g_param_spec_uint ("clamp-duration",
			"Clamp duration",
			"Duration of the adjustment clamp animation.",
			0, G_MAXUINT, 250,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CLAMP_DURATION, pspec);

        /**
         * MildenhallRollerContainer:clamp-mode:
         *
         * Animation mode to use for the clamp animation.
         *
         */
	pspec = g_param_spec_ulong ("clamp-mode",
			"Clamp mode",
			"Animation mode to use for the clamp animation.",
			0, G_MAXULONG, CLUTTER_EASE_OUT_QUAD,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CLAMP_MODE, pspec);

        /**
         * MildenhallRollerContainer:width:
         *
         * sets width for the roller container
         *
         */
	pspec = g_param_spec_float ("width",
                              "Width",
                              "Roller Width",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_WIDTH, pspec);

        /**
         * MildenhallRollerContainer:height:
         *
         * sets the Height for the roller container
         *
         */
	pspec = g_param_spec_float ("height",
                              "Height",
                              "Roller Height",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_HEIGHT, pspec);

        /**
         * MildenhallRollerContainer:show-flow-layout:
         *
         * To check whether more than one item can be placed in the same row
         */
	pspec = g_param_spec_boolean ("show-flow-layout",
			"Flow layout",
			"Whether more than one item can be placed in the same row",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_SHOW_FLOW_LAYOUT, pspec);

        /**
         * MildenhallRollerContainer:child-height:
         *
         * Height of each item
         *
         */
	pspec = g_param_spec_float ("child-height",
			"Child height",
			"Height of each item",
			-G_MAXFLOAT,
			G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CHILD_HEIGHT, pspec);

        /**
         * MildenhallRollerContainer:child-width:
         *
         * Width of each item
         *
         */
	pspec = g_param_spec_float ("child-width",
			"Child width",
			"Width of each item",
			-G_MAXFLOAT,
			G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CHILD_WIDTH, pspec);

        /**
         * MildenhallRollerContainer:hadjust:
         *
         * Horizontal adjustment object used for roller
         */
	 pspec = g_param_spec_object ("hadjust",
                        "Horizontal adujstment",
                        "Horizontal adujstment object used for roller",
                        MX_TYPE_ADJUSTMENT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_HADJUST, pspec);

        /**
         * MildenhallRollerContainer:vadjust:
         *
         * Vertical adjustment object used for roller
         *
         */
	pspec = g_param_spec_object ("vadjust",
                        "Vertical adujstment",
                        "Vertical adujstment object used for roller",
                        MX_TYPE_ADJUSTMENT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_VADJUST, pspec);

        /**
         * MildenhallRollerContainer:footer-model:
         *
         * Footer Model to use to construct the roller footer
         *
         */
	pspec = g_param_spec_object ("footer-model",
                        "Footer-Model",
                        "Footer Model to use to construct the roller footer",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_FOOTER_MODEL, pspec);

        /**
         * MildenhallRollerContainer:animate-footer:
         *
         * animates  the footer from down to up
         *
         */
    	pspec = g_param_spec_uint ("animate-footer",
                                    "animate-footer",
                                    "animates  the footer from down to up " ,
                        			0, G_MAXUINT, 250,
                        			G_PARAM_READWRITE);
    	g_object_class_install_property (object_class, PROP_ANIMATE_FOOTER, pspec);

        /**
         * MildenhallRollerContainer:first-visible-item:
         *
         * get/sets the first visible item in view port area 
         */
	pspec = g_param_spec_int ("first-visible-item",
                        "First Visible Item",
                        "Row currently visible",
                        -1,
                        G_MAXINT,
                        -1,
                        G_PARAM_READABLE);
        g_object_class_install_property (object_class, PROP_FIRST_VISIBLE_ITEM, pspec);

        /**
         * MildenhallRollerContainer:visible-items:
         *
         * get/sets the total visible item in view port area 
         */
        pspec = g_param_spec_int ("visible-items",
                        "Visible Items",
                        "Items Currently Visible",
                        -1,
                        G_MAXINT,
                        -1,
                        G_PARAM_READABLE);
        g_object_class_install_property (object_class, PROP_VISIBLE_ITEMS, pspec);

        /**
         * MildenhallRollerContainer:language:
         *
         * The language to support i18n
         */ 
        pspec = g_param_spec_string ("language",
                        "Language",
                        "Language",
                        "en_US",
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_LANGUAGE, pspec);

	/**
	 * MildenhallRollerContainer::roller-locking-finished:
         * @self: a #MildenhallRollerContainer reference
   	 *
         * Emitted when the locking animation finishes
         */
  	signals[LOCKING_FINISHED] =
    	g_signal_new ("roller-locking-finished",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MildenhallRollerContainerClass, locking_finished),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

        /**
         * MildenhallRollerContainer::item-activated:
         * @self: a #MildenhallRollerContainer reference
         * @row: the row of item gets activated.
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
        signals[ITEM_ACTIVATED] =
                g_signal_new ("roller-item-activated",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallRollerContainerClass, item_activated),
                                NULL, NULL,
                                g_cclosure_marshal_VOID__UINT,
                                G_TYPE_NONE, 1, G_TYPE_UINT);
        /**
         * MildenhallRollerContainer::roller-item-expanded:
         * @self: a #MildenhallRollerContainer reference
         * @expanded: the value of expanded
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
             signals[ITEM_EXPANDED] =
                     g_signal_new ("roller-item-expanded",
                                     G_TYPE_FROM_CLASS (klass),
                                     G_SIGNAL_RUN_LAST,
                                     G_STRUCT_OFFSET (MildenhallRollerContainerClass, item_expanded),
                                     NULL, NULL,
                                     g_cclosure_marshal_VOID__BOOLEAN,
                                     G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
        /**
         * MildenhallRollerContainer::roller-scroll-started:
         * @self: a #MildenhallRollerContainer reference
         *
         * Emitted when scroll starts.
         */
	signals[SCROLL_STARTED] =
                g_signal_new ("roller-scroll-started",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallRollerContainerClass, scroll_started),
                                NULL, NULL,
                                g_cclosure_marshal_VOID__VOID,
                                G_TYPE_NONE, 0);
        /**
         * MildenhallRollerContainer::roller-scrolling:
         * @self: a #MildenhallRollerContainer reference
         *
         * Emitted when scroll is going on.
         */
        signals[SCROLLING] =
                g_signal_new ("roller-scrolling",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallRollerContainerClass, scrolling),
                                NULL, NULL,
                                g_cclosure_marshal_VOID__VOID,
                                G_TYPE_NONE, 0);

        /**
         * MildenhallRollerContainer::roller-scroll-finished:
         * @self: a #MildenhallRollerContainer reference
         * @first_visible_item: the number of first visible item
         * @total_visible_item: total numbers of visible items
         *
         * Emitted when the scroll animation finishes
         */
        signals[SCROLL_FINISHED] =
                 g_signal_new ("roller-scroll-finished",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallRollerContainerClass, roller_scroll_finished),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_UINT);

        /**
         * MildenhallRollerContainer::roller-footer-clicked:
         * @self: a #MildenhallRollerContainer reference
         *
         * Emitted when footer is clicked.
         */
        signals[FOOTER_CLICKED] =
                g_signal_new ("roller-footer-clicked",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallRollerContainerClass, footer_clicked),
                                NULL, NULL,
                                g_cclosure_marshal_VOID__VOID,
                                G_TYPE_NONE, 0);

}

static void 
scroll_finished_cb (LightwoodRoller *roller, guint inFirstVisibleItem, guint inTotalvisibleItem, gpointer data)
{
    MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (data);
    g_print("scroll_finished: first visible item = %d total visible item = %d\n", inFirstVisibleItem, inTotalvisibleItem);

    if(priv->bScroll)
    {
	g_print("\nin roller container inside if\n");
        priv->bScroll = FALSE;
        g_signal_emit (G_OBJECT(data), signals[SCROLL_FINISHED], 0, inFirstVisibleItem, inTotalvisibleItem);
    }
}

static void
roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (data);
	MILDENHALL_ROLLERCONT_DEBUG("item_activated\n");
	if((priv->arrowEnable == TRUE) && (priv->roller != NULL))
		clutter_actor_show(priv->arrowRight);

	g_signal_emit (G_OBJECT(data), signals[ITEM_ACTIVATED], 0, row);
}

static void
roller_item_expanded_cb (LightwoodRoller *roller, gboolean isExpanded, gpointer data)
{
	MILDENHALL_ROLLERCONT_DEBUG("item_expanded\n");

	g_signal_emit (G_OBJECT(data), signals[ITEM_EXPANDED], 0, isExpanded);
}

static void
locking_finished_cb (LightwoodRoller *roller,
                     gpointer   data)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (data);
	MILDENHALL_ROLLERCONT_DEBUG("locking_finished\n");
	if((priv->arrowEnable == TRUE) && (priv->roller != NULL))
		clutter_actor_show(priv->arrowRight);

	g_signal_emit (G_OBJECT (data), signals[LOCKING_FINISHED], 0);
}

static void
scroll_started_cb (LightwoodRoller *roller,
                     gpointer   data)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (data);
        MILDENHALL_ROLLERCONT_DEBUG("scroll_stated\n");
        if((priv->arrowEnable == TRUE) && (priv->roller != NULL))
                clutter_actor_hide(priv->arrowRight);

	g_signal_emit (G_OBJECT (MILDENHALL_ROLLER_CONTAINER (data)), signals[SCROLL_STARTED], 0);
	priv->bScroll = TRUE;
}

static void
scroll_cb(LightwoodRoller *roller,
                     gpointer   data)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (data);
        MILDENHALL_ROLLERCONT_DEBUG("scroll\n");
        if((priv->arrowEnable == TRUE) && (priv->roller != NULL))
                clutter_actor_hide(priv->arrowRight);

	g_signal_emit (G_OBJECT (data), signals[SCROLLING], 0);
}

static void
row_added_cb (ThornburyModel     *model,
              ThornburyModelIter *iter,
              MildenhallRollerContainer *container)
{
}

static void
row_removed_cb (ThornburyModel     *model,
              ThornburyModelIter *iter,
              MildenhallRollerContainer *container)
{
     return ;
}

/********************************************************
 * Function : footer_clicked_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, container*
 * Return value: void
 ********************************************************/
static gboolean footer_clicked_cb(ClutterActor *actor, ClutterButtonEvent *pEvent, gpointer pUserData)
{
	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_ROLLER_CONTAINER(pUserData) )
		return FALSE;

	if (pEvent->type == CLUTTER_BUTTON_RELEASE || pEvent->type == CLUTTER_TOUCH_END)
	{
		g_signal_emit (G_OBJECT (pUserData), signals[FOOTER_CLICKED], 0);
	}

        return FALSE;
}

/********************************************************
 * Function : footer_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, container*
 * Return value: void
 ********************************************************/
static void footer_row_changed_cb (ThornburyModel *pModel,
                              ThornburyModelIter *pIter,
                              MildenhallRollerContainer *container)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);

        MILDENHALL_ROLLERCONT_DEBUG ("%s %d\n", __FUNCTION__, __LINE__);

        if (G_IS_OBJECT(priv->footerModel) && thornbury_model_get_n_rows (priv->footerModel) > 0)
        {
                ThornburyModelIter *child_pIter = thornbury_model_get_iter_at_row (priv->footerModel, 0);
                guint i = 0;
                if (NULL != child_pIter)
                {
                        /* model should have property name followed by property value */
                        for( i = 0; i < thornbury_model_get_n_columns(priv->footerModel); i+=2)
                        {
                                GValue key = { 0, };
                                GValue value = { 0, };
                                const gchar *name = NULL;

                                /* get the first column value for button name */
                                thornbury_model_iter_get_value (child_pIter, i, &key);
                                name = g_value_get_string(&key);

                                thornbury_model_iter_get_value (child_pIter, i+1, &value);

                                if (g_object_class_find_property (G_OBJECT_GET_CLASS (priv->footerItem), name) == NULL)
                                        continue;
                                g_object_set_property (G_OBJECT (priv->footerItem), name, &value);
                                g_value_unset (&value);
                                g_value_unset (&key);
                                MILDENHALL_ROLLERCONT_DEBUG("index = %d\n", i);
                        }
                }
        }
}

/********************************************************
 * Function : create_footer_item
 * Description: footer item creation
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
create_footer_item(MildenhallRollerContainer *container)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);
	MILDENHALL_ROLLERCONT_DEBUG("%s, %d \n", __FUNCTION__, __LINE__);

	if(NULL != priv->footerModel)
	{
		/* create footer group */
		if(NULL == priv->footer)
		{
			priv->footer = clutter_actor_new();
			clutter_actor_set_reactive(priv->footer, TRUE);
			clutter_actor_set_width(priv->footer, priv->fWidth);
			g_signal_connect(priv->footer, "button-release-event", G_CALLBACK(footer_clicked_cb), container);
			g_signal_connect (priv->footer, "touch-event", G_CALLBACK(footer_clicked_cb), container);
		}
		if(NULL != priv->itemFactory)
		{
			/* create item type already set by roller "item-type" */
			priv->footerItem = mx_item_factory_create((MxItemFactory *)priv->itemFactory);
			clutter_actor_add_child(priv->footer, priv->footerItem);

			clutter_actor_add_child(CLUTTER_ACTOR(container), priv->footer);
			/* parse the model and set properties */
			if (G_IS_OBJECT(priv->footerModel) && thornbury_model_get_n_rows (priv->footerModel) > 0)
		        {
				ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->footerModel, 0);
				guint i = 0;
	                        if(NULL != pIter)
        	                {
					MILDENHALL_ROLLERCONT_DEBUG("columns are  = %d\n", thornbury_model_get_n_columns(priv->footerModel));
					/* model should have property name followed by property value */
					for( i = 0; i < thornbury_model_get_n_columns(priv->footerModel); i+=2)
					{
						GValue key = { 0, };
						GValue value = { 0, };
                                                const gchar *name = NULL;

	                                	/* get the first column value for button name */
        	                	        thornbury_model_iter_get_value(pIter, i, &key);
						name = g_value_get_string (&key);
						MILDENHALL_ROLLERCONT_DEBUG("prop name = %s\n", name);
						thornbury_model_iter_get_value(pIter, i+1, &value);

						if (g_object_class_find_property (G_OBJECT_GET_CLASS (priv->footerItem), name) == NULL)
						        continue;
						g_object_set_property (G_OBJECT (priv->footerItem), name, &value);
						g_value_unset (&value);
						g_value_unset (&key);
						MILDENHALL_ROLLERCONT_DEBUG("index = %d\n", i);
					}
				}
			}
		}
	}
}

/********************************************************
 * Function : roller_container_relay_texture
 * Description: set active/normal seamless texture for the given roller container
 * Parameters: The object reference, bActive
 * Return value: void
 ********************************************************/
static void
roller_container_relay_texture(ClutterActor *cont, gboolean bActive)
{
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (cont));
	/* abhi: explicit paint call required to paint roller again */
	/* abhi: if bActive is set as TRUE, hide normal seamless and show active one */
	if(NULL != priv->seamlessActiveTexture && NULL != priv->seamlessNormalTexture)
	{
		if(bActive)
		{
			clutter_actor_show(priv->seamlessActiveTexture);
			clutter_actor_hide(priv->seamlessNormalTexture);
	clutter_actor_queue_redraw (cont);
		}
		/* abhi: if bActive is set as FALSE, hide active seamless and show normal one */
		else
		{
			clutter_actor_hide(priv->seamlessActiveTexture);
			clutter_actor_show(priv->seamlessNormalTexture);
	clutter_actor_queue_redraw (cont);
		}
	}
}

/********************************************************
 * Function : roller_container_set_texture_with_glow
 * Description: switch active/normal seamless texture for current/prev roller container
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
roller_container_set_texture_with_glow(ClutterActor *cont)
{
        if (currentGlowing == NULL)
        {
		/* abhi: make current roller container as active */
		roller_container_relay_texture(cont, TRUE);
	}
	 else if (currentGlowing != cont)
        {
		/* abhi: if roller container is different than the currentGlowing container,
		 * change previous roller container(currentGlowing) to normal seamless
 		 * and present roller container to active seamless texture */
		roller_container_relay_texture(currentGlowing, FALSE);
		roller_container_relay_texture(cont, TRUE);
	}
	currentGlowing = cont;
}

/********************************************************
 * Function : roller_container_press_cb
 * Description: callback for roller container button press
 * Parameters: The object reference, event, userdata
 * Return value: void
 ********************************************************/
static gboolean
roller_container_press_cb(ClutterActor *actor, ClutterEvent *event, gpointer user_data)
{
        MILDENHALL_ROLLERCONT_DEBUG("%s, %d \n", __FUNCTION__, __LINE__);

        /*abhi: Change the texture to active seamless when roller is clicked */
       	roller_container_set_texture_with_glow(actor);

	return FALSE;
}

/********************************************************
 * Function : roller_container_create_seamless_texture
 * Description: creates active/normal seamless texture
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
roller_container_create_seamless_texture(ClutterActor *cont)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (MILDENHALL_ROLLER_CONTAINER (cont));

	if (NULL == priv->seamlessNormalTexture && NULL != priv->seamlessNormalFilePath)
	{
		/* abhi: creation of normal seamless texture */
		priv->seamlessNormalTexture = thornbury_ui_texture_create_new (priv->seamlessNormalFilePath, 0.0, 0.0, FALSE, TRUE);//(MILDENHALL_ROLLER_CONTAINER_NORMAL_SEAMLESS, &err);
		if(priv->seamlessNormalTexture)
		{
			clutter_actor_set_size(priv->seamlessNormalTexture, priv->fWidth,  priv->fHeight);
			clutter_actor_insert_child_at_index(cont, priv->seamlessNormalTexture, 0);
			//clutter_actor_add_child (cont, priv->seamlessNormalTexture);
			//clutter_actor_lower_bottom(priv->seamlessNormalTexture);
		}
	}
	if (NULL == priv->seamlessActiveTexture && NULL !=priv->seamlessActiveFilePath)
	{
		/*  abhi: creation of active seamless texture foe active roller */
		priv->seamlessActiveTexture = thornbury_ui_texture_create_new (priv->seamlessActiveFilePath, 0.0, 0.0, FALSE, TRUE);//(MILDENHALL_ROLLER_CONTAINER_ACTIVE_SEAMLESS, &err);
		if(priv->seamlessActiveTexture)
		{
			clutter_actor_set_size(priv->seamlessActiveTexture, priv->fWidth,  priv->fHeight);
			clutter_actor_insert_child_at_index(cont, priv->seamlessActiveTexture, 0);
			clutter_actor_hide(priv->seamlessActiveTexture);
		}
	}

}

/********************************************************
 * Function : roller_container_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void
roller_container_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallRollerContainer *cont = pUserData;
        MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (cont);

        gchar *pStyleKey = g_strdup (pKey);

	MILDENHALL_ROLLERCONT_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_ROLLER_CONTAINER(pUserData) )
		return;

	if(g_strcmp0(pStyleKey, "rolls-active-seamless") == 0)
	{
		g_free (priv->seamlessActiveFilePath);
		priv->seamlessActiveFilePath = g_build_filename (
		    _mildenhall_get_theme_path (),
		    "mildenhallroller",
		    g_value_get_string (pValue),
		    NULL);

		MILDENHALL_ROLLERCONT_DEBUG("rolls-active-seamless = %s\n", priv->seamlessActiveFilePath);
	}
	else if(g_strcmp0(pStyleKey, "rolls_normal_seamless") == 0)
	{
		g_free (priv->seamlessNormalFilePath);
		priv->seamlessNormalFilePath = g_build_filename (
		    _mildenhall_get_theme_path (),
		    "mildenhallroller",
		    g_value_get_string (pValue),
		    NULL);

		MILDENHALL_ROLLERCONT_DEBUG("rolls_normal_seamless = %s\n", priv->seamlessNormalFilePath);
	}
	else if(g_strcmp0(pStyleKey, "roller-mid-arrow") == 0)
	{
		priv->arrowMarkup = g_value_dup_string(pValue);
		MILDENHALL_ROLLERCONT_DEBUG("roller-mid-arrow = %s\n", priv->arrowMarkup);
	}
	else if(g_strcmp0(pStyleKey, "roller-mid-arrow-color") == 0)
	{
		clutter_color_from_string(&priv->arrowColor, g_value_get_string(pValue) );
		MILDENHALL_ROLLERCONT_DEBUG("roller-mid-arrow-color  = %s\n", g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "roller-mid-arrow-font") == 0)
	{
		priv->arrowFont = g_value_dup_string(pValue);
		MILDENHALL_ROLLERCONT_DEBUG("roller-mid-arrow-font= %s\n", priv->arrowFont);
	}
	else if(g_strcmp0(pStyleKey, "clamp-duration") == 0)
	{
		priv->clampDuration = g_value_get_int64(pValue);
		MILDENHALL_ROLLERCONT_DEBUG("clamp-duration = %d\n", priv->clampDuration);
	}
	else if(g_strcmp0(pStyleKey, "clamp-mode") == 0)
	{
		priv->clampMode = g_value_get_int64(pValue);
		MILDENHALL_ROLLERCONT_DEBUG("clamp-mode = %d\n", priv->clampMode);
	}
	else if(g_strcmp0(pStyleKey, "drag-threshold") == 0)
	{
		priv->dragThreshold = g_value_get_int64 (pValue);
		MILDENHALL_ROLLERCONT_DEBUG("drag-threshold = %d\n", priv->dragThreshold);
	}
    else if(g_strcmp0(pStyleKey, "item-type-name") == 0)
	{
        MILDENHALL_ROLLERCONT_DEBUG("item-type-name = %s\n", g_value_get_string(pValue));
	}
	else
	{
		; /* do nothing */
	}

	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}

}

static void
notify_state_cb (MxKineticScrollView *scroll, GParamSpec *pspec,
                 MildenhallRollerContainer *container)
{
	MxKineticScrollViewState enState;
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (container);

	g_object_get (scroll, "state", &enState, NULL);
	if(enState == MX_KINETIC_SCROLL_VIEW_STATE_CLAMPING)
	{
		/* find the height and width of roller item type */
		gfloat fTotalHeight,x,y;
                ThornburyModel *model = NULL;
		gfloat height;
                gboolean cylinder_distort;
                LightwoodRollerClass *roller_class = NULL;
		clutter_actor_get_preferred_height(CLUTTER_ACTOR(priv->roller),-1,&fTotalHeight,NULL);
		roller_class = LIGHTWOOD_ROLLER_GET_CLASS (priv->roller);
		roller_class->get_visible_items(LIGHTWOOD_ROLLER(priv->roller),
				-1,
				-1,
				&x,
				&y);
		height = priv->fHeight;
                g_object_get (priv->roller, "cylinder-distort", &cylinder_distort, NULL);
                if (cylinder_distort)
                        height = M_PI * height / 2;

		model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (priv->roller));
		if((priv->bRollover == TRUE) && (height > fTotalHeight ))
		{
			gfloat trX = 0, trY = 0;
                        ClutterActor *child_actor = NULL;
                        gint row;
			/* get position of the container */
			clutter_actor_get_transformed_position (CLUTTER_ACTOR (container), &trX, &trY);
			MILDENHALL_ROLLERCONT_DEBUG("trX = %f trY = %f\n",trX,trY);
			/* get reactive actors in the middel of the roller */
			child_actor = clutter_stage_get_actor_at_pos (CLUTTER_STAGE (clutter_actor_get_stage (CLUTTER_ACTOR (priv->roller))), CLUTTER_PICK_REACTIVE, trX + 10.0, trY + priv->fHeight / 2.0);
			row = LIGHTWOOD_ROLLER_GET_CLASS (priv->roller)->get_row_from_item (LIGHTWOOD_ROLLER (priv->roller),
					child_actor);
			MILDENHALL_ROLLERCONT_DEBUG("\nrow = %d\n",row);
			/* if there are no reactive actors in middle focus the row depending on the direction */
			if(row == -1)
			{
				/* get position of the roller when scrolled, to findout the direction */
				clutter_actor_get_transformed_position (CLUTTER_ACTOR (priv->roller), &trX, &trY);
				if(trY < 0)
				{
					MILDENHALL_ROLLERCONT_DEBUG("try is negative %d\n",thornbury_model_get_n_rows(model));
					if(priv->bCentreFocus)
					{
						MILDENHALL_ROLLERCONT_DEBUG("priv->bCentreFocus is TRUE\n");
						lightwood_roller_set_focused_row(LIGHTWOOD_ROLLER (priv->roller),(thornbury_model_get_n_rows(model) - 1),TRUE);
					}
					else
					{
						MILDENHALL_ROLLERCONT_DEBUG("priv->bCentreFocus is FALSE\n");
						lightwood_roller_set_focused_row(LIGHTWOOD_ROLLER (priv->roller),0,TRUE);
					}
				}
				else
				{
					MILDENHALL_ROLLERCONT_DEBUG("try is positive %d\n",thornbury_model_get_n_rows(model));
					lightwood_roller_set_focused_row(LIGHTWOOD_ROLLER (priv->roller),0,TRUE);
				}
			}
			else
				lightwood_roller_set_focused_row(LIGHTWOOD_ROLLER (priv->roller),row,TRUE);

		}
	}

}

ClutterActor *
create_scroll_view (void)
{
  return g_object_new (MX_TYPE_KINETIC_SCROLL_VIEW,
                       "acceleration-factor", 2.0,
                       "deceleration", 1.03,
                       "clamp-duration", 1250,
                       "clamp-mode", CLUTTER_EASE_OUT_ELASTIC,
                       "clamp-to-center", TRUE,
                       "overshoot", 1.0,
                       NULL);
}

static void
mildenhall_roller_container_init (MildenhallRollerContainer *self)
{
	MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);
        /* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set (PKGDATADIR"/mildenhallroller/mildenhall_roller_container_style.json");
        MxSettings *settings;
        ClutterActor *text = clutter_text_new ();
	priv->arrowEnable = FALSE;
	priv->bEnableBg = TRUE;
	priv->fWidth = 0.0;
    priv->fHeight = 0.0;

	/* abhi: texture for normal/active seamless texture */
	priv->seamlessNormalTexture = NULL;
	priv->seamlessActiveTexture = NULL;
	priv->seamlessNormalFilePath = NULL;
	priv->seamlessActiveFilePath = NULL;
	priv->footerModel = NULL;
	priv->footer = NULL;
	priv->arrowFont = NULL;
	priv->arrowMarkup = NULL;
	priv->dragThreshold = 0;
	priv->clampMode = 0;
	priv->clampDuration = 0;
	priv->enRollerType = -1;
	priv->itemType = G_TYPE_NONE;
	priv->bCentreFocus = TRUE;

        priv->language = g_strdup ("en_US");

        /* abhi: pares the hash for styles */
        if(NULL != styleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, styleHash);
                /* abhi: iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        //MILDENHALL_ROLLERCONT_DEBUG("style layer = %s\n", (gchar*)key);
                        if(NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, roller_container_parse_style, self);
                        }
                }
	        /* abhi: free the style hash */
        	thornbury_style_free(styleHash);
        }

	settings = mx_settings_get_default ();
	g_object_set (G_OBJECT (settings), "drag-threshold", priv->dragThreshold, NULL);

	priv->scrollView = create_scroll_view ();
	clutter_actor_add_child (CLUTTER_ACTOR(self), priv->scrollView);
	g_signal_connect (priv->scrollView, "notify::state",
                      G_CALLBACK (notify_state_cb), self);

	/* create the default arrow */
	priv->arrowRight = clutter_actor_new();
	clutter_text_set_color (CLUTTER_TEXT (text), &priv->arrowColor);// &white);
	clutter_text_set_font_name (CLUTTER_TEXT (text), priv->arrowFont);//ROLLER_FONT(28));//"DejaVuSansCondensed 28px");
	clutter_text_set_editable (CLUTTER_TEXT (text), FALSE);
	clutter_text_set_use_markup (CLUTTER_TEXT (text), TRUE);
	clutter_text_set_markup (CLUTTER_TEXT (text), priv->arrowMarkup);//ROLLER_ARROW_MARKUP);
	clutter_actor_add_child (CLUTTER_ACTOR(priv->arrowRight),text);
	clutter_actor_add_child (CLUTTER_ACTOR(self),priv->arrowRight);
	clutter_actor_hide (priv->arrowRight);

	/* abhi: create seamless texture to change to active/normal when roller is clicked */
	roller_container_create_seamless_texture(CLUTTER_ACTOR(self) );

	clutter_actor_set_reactive(CLUTTER_ACTOR(self), TRUE);
	/* abhi: button press signal added to change the seamless texture to normal/active texture */
	g_signal_connect (self, "button-press-event", G_CALLBACK (roller_container_press_cb), NULL);
}

/**
 * mildenhall_roller_container_add_attribute:
 * @self: a #MildenhallRollerContainer
 * @property: Name of the attribute
 * @column: Column number
 *
 * Maps a property of the item actors to a column of the current #ThornburyModel.
 *
 */
void
mildenhall_roller_container_add_attribute (MildenhallRollerContainer *self,
                                           const gchar *property,
                                           gint column)
{

  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));
  g_return_if_fail (property != NULL);
  g_return_if_fail (column >= 0);

  if (priv->roller != NULL)
    {
      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER(priv->roller), property, column);
    }
}

/**
 * mildenhall_roller_container_get_actor_for_row:
 * @self: a #MildenhallRollerContainer
 * @row: Row of the actor we are interested in
 *
 * The actor being used to represent a row, or %NULL if the row doesn't
 * have one, for example because it isn't visible.
 *
 * Returns: (transfer full) (nullable): a #ClutterActor
 */
ClutterActor *
mildenhall_roller_container_get_actor_for_row (MildenhallRollerContainer *self, guint row)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), NULL);

  if (priv->roller != NULL)
    return lightwood_roller_get_actor_for_row (LIGHTWOOD_ROLLER(priv->roller), row);

  return NULL;
}

/**
 * mildenhall_roller_container_resize
 * @self: a #MildenhallRollerContainer
 * @roller_width: Final width of the roller
 * @item_width: Final width of each item
 * @item_height: Final height of each item
 * @focused_row: Row that should be kept in the middle
 * @timeline: Timeline of the animation
 * @animate: whether animation is required
 *
 * Resize roller and its children
 */
void
mildenhall_roller_container_resize (MildenhallRollerContainer *self,
                                    gfloat roller_width,
                                    gfloat item_width,
                                    gfloat item_height,
                                    gint focused_row,
                                    ClutterTimeline *timeline,
                                    gboolean animate)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (priv->roller != NULL)
    lightwood_roller_resize (LIGHTWOOD_ROLLER(priv->roller), roller_width, item_width, item_height, focused_row, timeline, animate);
}

/**
 * mildenhall_roller_container_set_min_visible_actors:
 * @self: a #MildenhallRollerContaine
 * @min_visible_actors: minimum visble actors to be created
 *
 * Update the value of minimum visible actors by calling 
 * lightwood_fixed_roller_set_min_visible_actors ()
 */
void
mildenhall_roller_container_set_min_visible_actors (MildenhallRollerContainer *self,
                                                    gint min_visible_actors)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (LIGHTWOOD_IS_FIXED_ROLLER (priv->roller))
    lightwood_fixed_roller_set_min_visible_actors (LIGHTWOOD_FIXED_ROLLER (priv->roller), min_visible_actors);
}

/**
 * mildenhall_roller_container_refresh
 * @self: a #MildenhallRollerContainer
 *
 * Refreshes only if internal roller is #LightwoodFixedRoller
 */
void
mildenhall_roller_container_refresh (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  if (LIGHTWOOD_IS_FIXED_ROLLER (priv->roller))
    lightwood_fixed_roller_refresh (LIGHTWOOD_FIXED_ROLLER (priv->roller));
}

/**
 * mildenhall_roller_container_get_language:
 * @self: a #MildenhallRollerContainer
 *
 * Returns: (transfer none) (nullable): the value of #MildenhallRollerContainer:language property
 */
const gchar *
mildenhall_roller_container_get_language (MildenhallRollerContainer *self)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self), NULL);

  return priv->language;
}

/**
 * mildenhall_roller_container_get_language:
 * @self: a #MildenhallRollerContainer
 * @language: the new value
 *
 * Update #MildenhallRollerContainer:language property
 */
void
mildenhall_roller_container_set_language (MildenhallRollerContainer *self, const gchar *language)
{
  MildenhallRollerContainerPrivate *priv = mildenhall_roller_container_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_ROLLER_CONTAINER (self));

  /* FIXME: No specific action for language now, but language property is used by
     API users.  */
  g_warning ("Not yet implemented");

  if (g_strcmp0 (priv->language, language) == 0)
    return;

  g_free (priv->language);
  priv->language = g_strdup (language);
  g_object_notify (G_OBJECT (self), "language");
}

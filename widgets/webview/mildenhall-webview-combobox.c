/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-webview-combobox.h"

#include "mildenhall_audio_player_songs_item.h"
#include "mildenhall_roller_container.h"
#include <thornbury/thornbury.h>

enum
{
  COLUMN_NUMBER,
  COLUMN_TEXT,
  COLUMN_LAST
};

SHM_COMBO_BOX_PLUGIN_DECLARE (MildenhallWebviewComboBox,
                              mildenhall_webview_combo_box)

#define MILDENHALL_WEBVIEW_COMBO_BOX_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_WEBVIEW_COMBO_BOX, MildenhallWebviewComboBoxPrivate))

struct _MildenhallWebviewComboBoxPrivate
{
  ClutterActor *roller; /* owned : roller list object */
  ThornburyModel *model; /* owned : ROLLER_CONTAINER type model */
};

typedef struct
{
  ShmComboBoxSelectCallback callback_handler; /* callback function called when user select the list item */
  gpointer user_data; /* return user data from list item selected callback */
} CloseComboBoxData;

static void
mildenhall_webview_combo_box_item_activated_cb (MildenhallRollerContainer *roller,
                                                guint row,
                                                gpointer user_data)
{
  CloseComboBoxData *method_data;

  g_assert (roller != NULL);
  g_assert (user_data != NULL);

  method_data = (CloseComboBoxData *) user_data;
  if (method_data->callback_handler != NULL)
    method_data->callback_handler (row, method_data->user_data);
  g_slice_free (CloseComboBoxData, method_data);
}

static void
mildenhall_webview_combo_box_init (MildenhallWebviewComboBox *self)
{
  g_assert (self != NULL);
}

static void
mildenhall_webview_combo_box_show (ShmComboBox *shm_combo_box,
                                   ClutterActor *actor,
                                   GList *items,
                                   gint selected_index,
                                   ShmComboBoxSelectCallback callback_handler,
                                   gpointer user_data)
{
  MildenhallWebviewComboBoxPrivate *priv;
  CloseComboBoxData *method_data;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  GList *iterator;
  ClutterActor *stage;
  guint row_index;

  g_assert (MILDENHALL_IS_WEBVIEW_COMBO_BOX (shm_combo_box));
  g_assert (CLUTTER_IS_ACTOR (actor));
  g_assert (items != NULL);

  priv = MILDENHALL_WEBVIEW_COMBO_BOX_PRIVATE (shm_combo_box);
  method_data = g_slice_new0 (CloseComboBoxData);
  method_data->callback_handler = callback_handler;
  method_data->user_data = user_data;
  stage = clutter_actor_get_stage (actor);
  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_ROLLER_CONTAINER,
      PKGDATADIR"/mildenhall_webview_combo_box_list.json");
  g_object_get (item_factory, "object", &priv->roller, NULL);
  g_object_set (priv->roller, "item-type",
                MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM,
                NULL);
  priv->model = (ThornburyModel *) thornbury_list_model_new (COLUMN_LAST,
        /* MildenhallAudioPlayerSongsItem expects list numbers in textual form */
        G_TYPE_STRING, "Number-Text",
        G_TYPE_STRING, "Text");

  mildenhall_roller_container_add_attribute (
      MILDENHALL_ROLLER_CONTAINER(priv->roller), "number", COLUMN_NUMBER);
  mildenhall_roller_container_add_attribute (
      MILDENHALL_ROLLER_CONTAINER(priv->roller), "text", COLUMN_TEXT);

  /* FIXME: The most generic item type is this one, even we have nothing to do with songs,
   * see https://phabricator.apertis.org/T3673 */
  g_object_set (priv->roller,
                "model", priv->model,
                "item-type", MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM,
                NULL);
  g_signal_connect (G_OBJECT (priv->roller), "roller-item-activated",
                    G_CALLBACK (mildenhall_webview_combo_box_item_activated_cb),
                    method_data);

  /* FIXME : if there is no selected item on list, selected index is negative,
   * forcing selected index to zero  */
  if (selected_index < 0)
    selected_index = 0;

  mildenhall_roller_container_set_focused_row (MILDENHALL_ROLLER_CONTAINER (priv->roller), selected_index, FALSE);
  clutter_actor_set_size (priv->roller,
                          clutter_actor_get_width (stage),
                          clutter_actor_get_height (stage));
  clutter_actor_add_child (stage, priv->roller);

  for (iterator = items, row_index = 1; iterator != NULL; iterator = iterator->next, row_index++)
    {
      g_autofree gchar *number = g_strdup_printf("%d", row_index);
      thornbury_model_append (priv->model, 
                              COLUMN_NUMBER, number,
                              COLUMN_TEXT, iterator->data,
                              -1);
    }
  clutter_actor_show (priv->roller);
}

static void
mildenhall_webview_combo_box_hide (ShmComboBox *shm_combo_box)
{
  MildenhallWebviewComboBoxPrivate *priv;

  g_return_if_fail (MILDENHALL_IS_WEBVIEW_COMBO_BOX (shm_combo_box));

  priv = MILDENHALL_WEBVIEW_COMBO_BOX_PRIVATE (shm_combo_box);
  g_clear_pointer (&priv->roller, clutter_actor_destroy);
}

static void
mildenhall_webview_combo_box_class_init (MildenhallWebviewComboBoxClass *kclass)
{
  ShmComboBoxClass *combobox_class;

  g_assert (kclass != NULL);

  combobox_class  = SHM_COMBO_BOX_CLASS (kclass);
  g_type_class_add_private (kclass, sizeof (MildenhallWebviewComboBoxPrivate));

  combobox_class->show = mildenhall_webview_combo_box_show;
  combobox_class->hide = mildenhall_webview_combo_box_hide;
}

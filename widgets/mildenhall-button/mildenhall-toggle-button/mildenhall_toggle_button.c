/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**
 * SECTION:mildenhall_toggle_button
 * @title: MildenhallToggleButton
 * @short_description: Toggle button for MILDENHALL
 * @see_also: #MildenhallButton, #MildenhallButtonDrawer
 *
 * #MildenhallToggleButton widget can be used to display the options to user.
 * One option among them can be selected and the info is communicated
 * to the application.
 * Model Contains:
 *	1. @DISPLAY_FIRST
 *	2. @DISPLAY_TEXT_ONLY
 *	3. @DISPLAY_IMAGE_ONLY
 *	4. @DISPLAY_TEXT_AND_IMAGE
 *	5. @DISPLAY_LAST
 *
 * ## Freeing the widget
 * 	Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 * MildenhallToggleButton *toggle_button = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 * 	MILDENHALL_TYPE_TOGGLE_BUTTON,
 * 	"/usr/Application/MyApp/share/mildenhall_toggle_button_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * toggle_button = MILDENHALL_TOGGLE_BUTTON (object);
 *
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (toggle_button));
 *
 * ]|
 *
 * Refer #ThornburyItemFactory for Thornbury Item Factory details.
 *
 * Since: 0.3.0
 */

#include "mildenhall_toggle_button.h"

#include <thornbury/thornbury.h>

/**
 * enToggleButtonProperty:
 *
 * property enums of toggle button
 */
typedef enum _enToggleButtonProperty enToggleButtonProperty;
enum _enToggleButtonProperty
{
    /*< private >*/
    PROP_FIRST,
    PROP_WIDTH,
    PROP_HEIGHT,
    PROP_DISPLAY_TYPE,
    PROP_CURRENT_STATE,
    PROP_HIGHLIGHT,
    PROP_STATE_MODEL,
    PROP_ALIGNMENT_TYPE,
    PROP_LAST
};

/**
 * _enToggleButtonSignals:
 * @SIG_BUTTON_TOGGLED: enum type for button toggled event
 *
 * The signals emitted by the toggle button
 */

enum _enToggleButtonSignals
{
    SIG_FIRST,
    SIG_BUTTON_TOGGLED,
    SIG_LAST
};

/**
 * enToggleButtonModelColumn:
 *
 * enum to fetch info from Model column of particular row
 */
typedef enum _enToggleButtonModelColumn enToggleButtonModelColumn;
enum _enToggleButtonModelColumn
{
    /*< private >*/
    COLOUMN_FIRST,
    COLOUMN_SECOND,
    COLOUMN_THIRD,
    COLOUMN_FOURTH
};

GType
mildenhall_toggle_button_display_type_get_type (void)
{
  static volatile gsize once_init_value = 0;
  static GType enum_type_id = 0;
  
  if (g_once_init_enter (&once_init_value))
    {
      static const GEnumValue values[] =
        {
          { MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE, "MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE", "none" },
          { MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY, "MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY", "image-only" },
          { MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY, "MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY", "text-only" },
          { MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT, "MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT", "image-text" },
          { 0, NULL, NULL }
        };
      enum_type_id = g_enum_register_static (g_intern_static_string ("MildenhallToggleButtonDisplayType"), values);
      g_once_init_leave (&once_init_value, 1);
    } 

  return enum_type_id;
}

GType
mildenhall_toggle_button_alignment_type_get_type (void)
{
  static volatile gsize once_init_value = 0;
  static GType enum_type_id = 0;
  
  if (g_once_init_enter (&once_init_value))
    {
      static const GEnumValue values[] =
        {
          { MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE, "MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE", "none" },
          { MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT, "MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT", "left" },
          { MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE, "MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE", "middle" },
          { MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT, "MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT", "right" },
          { 0, NULL, NULL }
        };
      enum_type_id = g_enum_register_static (g_intern_static_string ("MildenhallToggleButtonAlignmentType"), values);
      g_once_init_leave (&once_init_value, 1);
    } 

  return enum_type_id;
}


/*< private >*/
#define	TOGGLE_BUTTON_ARROW_LEFT	"toggle-button-arrow-left"
#define	TOGGLE_BUTTON_ARROW_RIGHT	"toggle-button-arrow-right"
#define MILDENHALL_TOGGLE_BUTTON_PRINT( a ...)			/* g_print(a); */

G_DEFINE_TYPE (MildenhallToggleButton, mildenhall_toggle_button, LIGHTWOOD_TYPE_BUTTON)

#define MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_TOGGLE_BUTTON, MildenhallToggleButtonPrivate))

/* Storage for the signals */
static guint32 mildenhall_toggle_bt_signals[SIG_LAST] = {0,};

/* Note: One line space has to be given between Structure Name and Discription if there is no
 * parameters
 */
/**
 * MildenhallToggleButtonPrivate:
 *
 * structure to hold the private variables of toggle button
 */

struct _MildenhallToggleButtonPrivate
{
	 /*< private >*/
    gfloat fltWidth;					/* width of the toggle button */
    gfloat fltHeight;					/* height of the toggle button */
    gboolean bMultiState;  				/* Whether the button has more than one state as stated by model */
    gint inCurrentState;				/* current state of button */
    gboolean bActive;					/* Whether the button should be active or not */
    MildenhallToggleButtonDisplayType display_type;     /* int value indicating the type of display */
    gchar *pTextFont;					/* Font of text to display */
    gfloat fltTextPositionXdisplace;	/* In case of 'text-image' type position of text from image */
    gfloat fltTextImageSpace;			/* space between image and text */
    gfloat fltArrowMargin;				/* margin space for arrow on either side  */
    gchar *pStrArrowLeft;				/* variable to hold markup string for arrow left*/
    gchar *pStrArrowRight;				/* variable to hold markup string for arrow right*/
    gchar *pArrowFont;					/* Arrow font */
    gboolean bHighlight;				/* priv variable to know teh active / normal state */
    MildenhallToggleButtonAlignmentType alignment_type; /* to determine the alignment type: Left , Middle or Right*/
    gfloat fltTextMargin;				/* margin space to display text */
    ThornburyModel *pStateModel;			/* Model */
    GHashTable *pStyleHash;
    ClutterActor *pArrowLeft;			/* ClutterText to display left arrow */
    ClutterActor *pArrowRight;			/* clutterText to display right arrow */
    ClutterActor *pButtonGroup;
    ClutterActor *pShowTextActor;
    ClutterActor *pShowIcon;
    ClutterColor TextFontColor;			/* Text font color to be displayed Active/Normal */
    ClutterColor ActiveTextFontColor;	/* Active text font color */
    ClutterColor NormalTextFontColor;	/* Normal text font color */
    gchar *pDisplayImage; 				/* toggle button image */
    gchar *pDisplayText;				/* toggle button text */
    ClutterActor *pLangText;
};


/*internal function declarations */
static 	void v_toggle_button_check_multi_state(MildenhallToggleButton *pToggleButton, ThornburyModel *pModel);
static 	void v_toggle_button_update_arrows(MildenhallToggleButton *pToggleButton);
static 	void v_toggle_button_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static 	void v_toggle_button_set_width(MildenhallToggleButton *pToggleButton, gfloat fltWidth);
static 	void v_toggle_button_set_height(MildenhallToggleButton *pToggleButton, gfloat fltHeight);
static 	void v_toggle_button_update_view(MildenhallToggleButton *pToggleButton);
static 	void v_toggle_button_set_text(ClutterActor *pActor);
static 	void v_toggle_button_set_icon(ClutterActor *pActor);

/* callback function */
static void v_toggle_button_active_cb (MildenhallToggleButton *pToggleButton, GParamSpec *pspec, gpointer user_data);
static 	void v_toggle_button_release_cb(ClutterActor *pToggleButton, gpointer pUserData);
static	void v_toggle_button_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton);
static 	void v_toggle_button_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton);
static 	void v_toggle_button_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton);

/* setter functions definition */



/**
 * mildenhall_toggle_button_set_current_state:
 * @pToggleButton: toggle button object reference of type #MildenhallToggleButton
 * @inState: current state of the button of type #gint
 *
 * Sets the current state of the toggle button.
 *
 * Since: 0.3.0
 */
void mildenhall_toggle_button_set_current_state(MildenhallToggleButton *pToggleButton, gint inState)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return;
    }

    if(NULL == priv->pStateModel)
    {
        //g_warning("model not set\n");
        return;
    }
    /* if state is same as previous , ignore it */
    if(priv->inCurrentState == inState)
        return;

    if(priv->bActive == TRUE)
    {
        /* check if the state is within the rage of the given model */
        if(inState >= 0 && (unsigned int) inState < thornbury_model_get_n_rows (priv->pStateModel ))
        {
            gpointer *pSendStateId = NULL;
            ThornburyModelIter *pIter = NULL;
            priv->inCurrentState = inState;
            /* update the view */
            v_toggle_button_update_view(pToggleButton);


            pIter = thornbury_model_get_iter_at_row (priv->pStateModel, priv->inCurrentState);
            if(NULL != pIter)
            {
                GValue value = { 0, };
                /* | TEXT  | IMAGE_PATH_ACTIVE  | IMAGE_PATH_INACTIVE  | STATE_ID | */

                /* get the Last column value to get State ID  */
                thornbury_model_iter_get_value(pIter,COLOUMN_FOURTH, &value);
                pSendStateId = g_value_get_pointer(&value);
                g_value_unset (&value);
            }


            /* emit the signal for state changed with the new state info */
            g_signal_emit(pToggleButton, mildenhall_toggle_bt_signals[SIG_BUTTON_TOGGLED], 0, pSendStateId);
        }
        else
            g_warning("state out of range\n");
    }
}

/**
 * mildenhall_toggle_button_set_text_alignment:
 * @self: a #MildenhallToggleButton
 * @alignment_type: the new #MildenhallToggleAlignmentType value
 *
 * Sets the alignment of the text in the toggle button.
 *
 */
void
mildenhall_toggle_button_set_text_alignment (MildenhallToggleButton *self,
                                             MildenhallToggleButtonAlignmentType alignment_type)
{
  MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (self);
  g_return_if_fail (MILDENHALL_IS_TOGGLE_BUTTON (self));

  if (priv->alignment_type == alignment_type)
    return;

  priv->alignment_type = alignment_type;
  v_toggle_button_update_view (self);
  g_object_notify (G_OBJECT (self), "alignment-type");
}


/**
 * mildenhall_toggle_button_set_model:
 * @pToggleButton: a #MildenhallToggleButton toggle button  object reference
 * @pModel: a #ThornburyModel.
 *
 * Model contains the data to be displayed on the button.
 * Toggle button model contains:
 *			1. Text
 *			2. Active Image Path
 *			3. Normal Image Path
 *			4. State ID
 * If button has more than one row with above mentioned columns
 * then arrows will be shown and button will be made reactive
 *
 * Since: 0.3.0
 */
void mildenhall_toggle_button_set_model(MildenhallToggleButton *pToggleButton, ThornburyModel *pModel)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid  toggle button object\n");
        return;
    }

    if(NULL != priv->pStateModel)
    {
        g_signal_handlers_disconnect_by_func (priv->pStateModel,
                                              G_CALLBACK (v_toggle_button_row_added_cb),
                                              pToggleButton);
        g_signal_handlers_disconnect_by_func (priv->pStateModel,
                                              G_CALLBACK (v_toggle_button_row_changed_cb),
                                              pToggleButton);
        g_signal_handlers_disconnect_by_func (priv->pStateModel,
                                              G_CALLBACK (v_toggle_button_row_removed_cb),
                                              pToggleButton);
        g_object_unref (priv->pStateModel);

        priv->pStateModel = NULL;
    }

    /* update the new model with signals */
    if (pModel != NULL)
    {
        g_return_if_fail (G_IS_OBJECT (pModel));

        priv->pStateModel = g_object_ref (pModel);

        g_signal_connect (priv->pStateModel,
                          "row-added",
                          G_CALLBACK (v_toggle_button_row_added_cb),
                          pToggleButton);

        g_signal_connect (priv->pStateModel,
                          "row-changed",
                          G_CALLBACK (v_toggle_button_row_changed_cb),
                          pToggleButton);

        g_signal_connect (priv->pStateModel,
                          "row-removed",
                          G_CALLBACK (v_toggle_button_row_removed_cb),
                          pToggleButton);

    }
    /* if model has more than one state, update multi state property */
    v_toggle_button_check_multi_state(pToggleButton, priv->pStateModel);
    /* update the view */
    v_toggle_button_update_view(pToggleButton);

    clutter_actor_queue_relayout (CLUTTER_ACTOR (pToggleButton));
    g_object_notify (G_OBJECT(pToggleButton), "state-model");
}


/**
 * mildenhall_toggle_button_set_display_type:
 * @self: a #MildenhallToggleButton
 * @display_type: the new value of #MildenhallToggleButtonDisplay
 *
 * Sets the Display type in the Toggle Button.
 */
void
mildenhall_toggle_button_set_display_type (MildenhallToggleButton *self,
                                           MildenhallToggleButtonDisplayType display_type)
{
  MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (self);

  g_return_if_fail (MILDENHALL_IS_TOGGLE_BUTTON (self));

  if (priv->display_type == display_type)
    return;

  if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT )
    {
      priv->fltTextPositionXdisplace = priv->fltHeight;
    }

  priv->display_type = display_type;
  v_toggle_button_update_view (self);
  g_object_notify (G_OBJECT (self), "display-type");
}


/**
 * mildenhall_toggle_button_set_highlight:
 * @pToggleButton: a #MildenhallToggleButton toggle button object reference
 * @bHighLightState: highlight state of the button of type #gboolean
 *
 * This method is to highlight a particular button with the given state.
 *
 */
void mildenhall_toggle_button_set_highlight(MildenhallToggleButton *pToggleButton, gboolean bHighLightState)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid  toggle button object\n");
        return;
    }

    /* Return if value to be set is redundant */
    if(priv->bHighlight == bHighLightState)
        return;

    priv->bHighlight = bHighLightState;
    MILDENHALL_TOGGLE_BUTTON_PRINT("HIGHLIGHT STATE = %s\n", (priv->bHighlight)?"true":"false");
    if(NULL != priv->pStateModel)
    {
        priv->TextFontColor = (priv->bHighlight) ? priv->ActiveTextFontColor : priv->NormalTextFontColor;
        clutter_text_set_color (CLUTTER_TEXT (priv->pArrowRight), &priv->TextFontColor);
        clutter_text_set_color (CLUTTER_TEXT (priv->pArrowLeft), &priv->TextFontColor);
        v_toggle_button_update_view(pToggleButton);
    }
    g_object_notify (G_OBJECT(pToggleButton), "highlight");
}



/* getter functions */

/**
 * mildenhall_toggle_button_get_width:
 * @pToggleButton: a #MildenhallToggleButton toggle button object reference
 *
 * Function to get the toggle button width
 *
 * Returns: width of the toggle button
 *
 * Since: 0.3.0
 *
 */

gfloat mildenhall_toggle_button_get_width( MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return -1;
    }

    return priv->fltWidth;

}



/**
 * mildenhall_toggle_button_get_height:
 * @pToggleButton: a #MildenhallToggleButton toggle button object reference
 *
 * Function to get the toggle button height
 *
 * Returns: height of the toggle button
 *
 * Since: 0.3.0
 */
gfloat mildenhall_toggle_button_get_height( MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return -1;
    }

    return priv->fltHeight;

}


/**
 * mildenhall_toggle_button_get_current_state:
 * @pToggleButton:  a #MildenhallToggleButton
 *
 * Function to get the toggle button current state
 *
 * Returns: current state of the toggle button
 *
 * Since: 0.3.0
 */
gint mildenhall_toggle_button_get_current_state( MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return -1;
    }

    return priv->inCurrentState;

}


/**
 * mildenhall_toggle_button_get_model:
 * @pToggleButton: a #MildenhallToggleButton
 *
 * Function to get the toggle button model
 *
 * Returns: (transfer none) (nullable): model of the toggle button.
 * ( In ideal case: Call g_object_unref() on the returned object after use.)
 *
 * Since: 0.3.0
 *
 */
ThornburyModel *mildenhall_toggle_button_get_model( MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid  toggle button object\n");
        return NULL;
    }

    return priv->pStateModel;

}


/**
 * mildenhall_toggle_button_get_display_type:
 * @self: a #MildenhallToggleButton
 *
 * Function to get the current display type of the button
 *
 * Returns: Display type of the button in #MildenhallToggleButtonDisplayType
 *
 */
MildenhallToggleButtonDisplayType
mildenhall_toggle_button_get_display_type (MildenhallToggleButton *self)
{
  MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (self);

  g_return_val_if_fail (MILDENHALL_IS_TOGGLE_BUTTON (self), MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE);

  return priv->display_type;
}

/**
 * mildenhall_toggle_button_get_text_alignment:
 * @self: a #MildenhallToggleButton
 *
 * Function to get the current text alignment
 *
 * Returns: #MildenhallToggleButtonAlignmentType alignment the button.
 */
MildenhallToggleButtonAlignmentType
mildenhall_toggle_button_get_text_alignment (MildenhallToggleButton *self)
{
  MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (self);

  g_return_val_if_fail (MILDENHALL_IS_TOGGLE_BUTTON (self), MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE);

  return priv->alignment_type;
}

/**
 * mildenhall_toggle_button_get_highlight:
 * @pToggleButton: a #MildenhallToggleButton
 *
 * Function to get the Active / normal state of button
 *
 * Returns: TRUE if button highlighted, FALSE otherwise
 *
 * Since: 0.3.0
 */

gboolean mildenhall_toggle_button_get_highlight(MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid  toggle button object\n");
        return FALSE;
    }
    return priv->bHighlight;
}

/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function :v_toggle_button_set_width
 * Description: set toggle button  width internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_toggle_button_set_width(MildenhallToggleButton *pToggleButton, gfloat fltWidth)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return ;
    }

    if(priv->fltWidth != fltWidth)
    {
        clutter_actor_set_width(CLUTTER_ACTOR(pToggleButton), fltWidth);
        priv->fltWidth = fltWidth;
    }
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
}

/********************************************************
 * Function :v_toggle_button_set_height
 * Description: set toggle button  height internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_toggle_button_set_height(MildenhallToggleButton *pToggleButton, gfloat fltHeight)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return ;
    }

    if(priv->fltHeight != fltHeight)
    {
        clutter_actor_set_height(CLUTTER_ACTOR(pToggleButton), fltHeight);
        priv->fltHeight = fltHeight;
    }
}


/****************************************************************
 * Function : v_toggle_button_active_cb
 * Description: callback for reactive state notification
 * Parameters: The object, GParamSpec *, gpointer
 * Returns: void
 ****************************************************************/
static void v_toggle_button_active_cb (MildenhallToggleButton *pToggleButton, GParamSpec *pspec, gpointer user_data)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    gboolean bState;
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid toggle button object\n");
        return ;
    }

    g_object_get (pToggleButton, "reactive", &bState, NULL);

    if(priv->bActive != bState)
    {
        priv->bActive = bState;
    }
}



/********************************************************
 * Function : mildenhall_toggle_button_get_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/

static void mildenhall_toggle_button_get_property (GObject    *pObject, guint    uinProperty_id, GValue     *pValue,  GParamSpec *pspec)
{
    MildenhallToggleButton *pToggleButton = MILDENHALL_TOGGLE_BUTTON (pObject);
    MILDENHALL_TOGGLE_BUTTON_PRINT("%s \n",__FUNCTION__);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pObject))
    {
        g_warning("invalid toggle button object\n");
        return;
    }
    switch (uinProperty_id)
    {
    case PROP_WIDTH:
        g_value_set_float (pValue, mildenhall_toggle_button_get_width(pToggleButton) );
        break;
    case PROP_HEIGHT:
        g_value_set_float (pValue, mildenhall_toggle_button_get_height(pToggleButton) );
        break;
    case PROP_DISPLAY_TYPE:
        g_value_set_enum (pValue, mildenhall_toggle_button_get_display_type (pToggleButton) );
        break;
    case PROP_CURRENT_STATE:
        g_value_set_int (pValue, mildenhall_toggle_button_get_current_state(pToggleButton) );
        break;
    case PROP_STATE_MODEL:
        g_value_set_object (pValue, mildenhall_toggle_button_get_model(pToggleButton) );
        break;
    case PROP_ALIGNMENT_TYPE:
        g_value_set_enum (pValue,mildenhall_toggle_button_get_text_alignment (pToggleButton) );
        break;
    case PROP_HIGHLIGHT:
        g_value_set_boolean (pValue, mildenhall_toggle_button_get_highlight(pToggleButton) );
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pspec);
        break;
    }
}

/********************************************************
 * Function : mildenhall_toggle_button_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/


static void mildenhall_toggle_button_set_property (GObject      *pObject,  guint  uinProperty_id, const GValue *pValue, GParamSpec   *pspec)
{
    MildenhallToggleButton *pToggleButton = MILDENHALL_TOGGLE_BUTTON (pObject);
    MILDENHALL_TOGGLE_BUTTON_PRINT("%s \n",__FUNCTION__);
    if(!MILDENHALL_IS_TOGGLE_BUTTON(pObject))
    {
        g_warning("invalid toggle button object\n");
        return;
    }
    switch (uinProperty_id)
    {
    case PROP_WIDTH:
        v_toggle_button_set_width(pToggleButton, g_value_get_float (pValue) );
        break;
    case PROP_HEIGHT:
        v_toggle_button_set_height(pToggleButton, g_value_get_float (pValue) );
        break;
    case PROP_DISPLAY_TYPE:
        mildenhall_toggle_button_set_display_type (pToggleButton, g_value_get_enum (pValue));
        break;
    case PROP_CURRENT_STATE:
        mildenhall_toggle_button_set_current_state(pToggleButton,g_value_get_int (pValue));
        break;
    case PROP_STATE_MODEL:
        mildenhall_toggle_button_set_model(pToggleButton,g_value_get_object (pValue));
        break;
    case PROP_ALIGNMENT_TYPE:
        mildenhall_toggle_button_set_text_alignment (pToggleButton,g_value_get_enum (pValue));
        break;
    case PROP_HIGHLIGHT:
        mildenhall_toggle_button_set_highlight(pToggleButton,g_value_get_boolean(pValue));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pspec);
        break;
    }
}

/********************************************************
 * Function : v_toggle_button_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_toggle_button_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
    gchar *pStyleKey = g_strdup(pKey);
    MildenhallToggleButton *pToggleButton = pUserData;
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE(pToggleButton);
    if(g_strcmp0(pStyleKey, "markup-arrow-right") == 0)
        priv->pStrArrowRight = (gchar*)g_value_dup_string(pValue);
    else if(g_strcmp0(pStyleKey, "markup-arrow-left") == 0)
        priv->pStrArrowLeft = (gchar*)g_value_dup_string(pValue);
    else if(g_strcmp0(pStyleKey, "markup-arrow-font") == 0)
        priv->pArrowFont = (gchar*)g_value_dup_string(pValue);
    else if(g_strcmp0(pStyleKey, "active-text-font-color") == 0)
        clutter_color_from_string(&priv->ActiveTextFontColor, g_value_get_string(pValue));
    else if(g_strcmp0(pStyleKey, "normal-text-font-color") == 0)
        clutter_color_from_string(&priv->NormalTextFontColor, g_value_get_string(pValue));
    else if(g_strcmp0(pStyleKey, "text-font") == 0)
        priv->pTextFont = g_strdup(g_value_get_string(pValue));
    else if(g_strcmp0(pStyleKey, "text-image-space") == 0)
    {
        MILDENHALL_TOGGLE_BUTTON_PRINT(" \n %s \n",pStyleKey);
        priv->fltTextImageSpace = g_value_get_double(pValue);
    }
    else if(g_strcmp0(pStyleKey, "arrow-margin") == 0)
    {
        priv->fltArrowMargin = g_value_get_double(pValue);
    }
}

/********************************************************
 * Function : mildenhall_toggle_button_dispose
 * Description: Dispose the mildenhall button drawer object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/


static void mildenhall_toggle_button_dispose (GObject *pObject)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pObject);
    MILDENHALL_TOGGLE_BUTTON_PRINT ("%s \n",__FUNCTION__);
    G_OBJECT_CLASS (mildenhall_toggle_button_parent_class)->dispose (pObject);

   g_clear_pointer (&priv->pStrArrowLeft, g_free);
   g_clear_pointer (&priv->pStrArrowRight, g_free);
   g_clear_pointer (&priv->pArrowFont, g_free);
   g_clear_pointer (&priv->pTextFont, g_free);
}


/********************************************************
 * Function : mildenhall_toggle_button_finalize
 * Description: Finalize the mildenhall button drawerobject
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_toggle_button_finalize (GObject *pObject)
{
    MILDENHALL_TOGGLE_BUTTON_PRINT("%s \n",__FUNCTION__);
    G_OBJECT_CLASS (mildenhall_toggle_button_parent_class)->finalize (pObject);
}


/********************************************************
 * Function : mildenhall_toggle_button_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void  mildenhall_toggle_button_class_init (MildenhallToggleButtonClass *pKlass)
{
    GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
    GParamSpec *pspec = NULL;
    MILDENHALL_TOGGLE_BUTTON_PRINT ("%s \n",__FUNCTION__);

    g_type_class_add_private (pKlass, sizeof (MildenhallToggleButtonPrivate));

    pObjectClass->get_property = mildenhall_toggle_button_get_property;
    pObjectClass->set_property = mildenhall_toggle_button_set_property;
    pObjectClass->dispose = mildenhall_toggle_button_dispose;
    pObjectClass->finalize = mildenhall_toggle_button_finalize;

    /* Create a signal for the button to indicate state change */


    /* Note: This signal documentation cn also be written inside the structure
     * where the signal format is defined.This can help to list explanation for
     * all parameters without missing any.
     */

    /**
     * MildenhallToggleButton::button-toggled
     * @mildenhallToggleButton: The object which received the signal
     * @oldState: name of the old state
     * @newState: name of the new state
     *
     * button-toggled is emitted when button has multi-state and there
     * is a change in the button state
     */
    mildenhall_toggle_bt_signals[SIG_BUTTON_TOGGLED] = g_signal_new ("button-toggled",
            G_TYPE_FROM_CLASS (pObjectClass),
            G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET (MildenhallToggleButtonClass, button_toggled),
            NULL, NULL,
            g_cclosure_marshal_VOID__INT,
            G_TYPE_NONE, 1, G_TYPE_INT);
    /**
     * MildenhallToggleButton:width:
     *
     * Width of the #MildenhallToggleButton
     */
    pspec = g_param_spec_float ("width",
                                "Width",
                                "Width of the toggle button",
                                0.0, G_MAXFLOAT,
                                0.0,
                                G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);
    /**
     * MildenhallToggleButton:height:
     *
     * Height of the #MildenhallToggleButton
     */
    pspec = g_param_spec_float ("height",
                                "Height",
                                "Height of the toggle button",
                                0.0, G_MAXFLOAT,
                                0.0,
                                G_PARAM_READWRITE);
    g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

    /**
     * MildenhallToggleButton:display-type:
     *
     * #MildenhallToggleButton:display-type property sets or gets the display type of #MildenhallToggleButton.
     */
    pspec = g_param_spec_enum ("display-type",
                               "DisplayType",
                               "Display Type for the button",
                               MILDENHALL_TOGGLE_BUTTON_DISPLAY_TYPE,
                               MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
                               G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_DISPLAY_TYPE, pspec);

    /**
     * MildenhallToggleButton:alignment-type:
     *
     * Property #MildenhallToggleButton:alignment-type will gives/sets the alignment of text
     */
    pspec = g_param_spec_enum ("alignment-type",
                               "Alignment",
                               "Alignment of Text inside button",
                               MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_TYPE,
                               MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE,
                               G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_ALIGNMENT_TYPE, pspec);


    /**
     * MildenhallToggleButton:highlight:
     *
     * Use Property #MildenhallToggleButton:highlight to set or unset the highlight in #MildenhallToggleButton.
     *
     * In case of type #TB_DISPLAY_IMAGE_ONLY or #TB_DISPLAY_IMAGE_TEXT model should contain
     * corresponding image.
     * Default: #FALSE
     */
    pspec = g_param_spec_boolean ("highlight",
                                  "HighLight",
                                  "Highlight state of the button ;true/false",
                                  FALSE,
                                  G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_HIGHLIGHT, pspec);

    /**
     * MildenhallToggleButton:current-state:
     *
     *Property #MildenhallToggleButton:current-state gives the current state of #MildenhallToggleButton as state index
     * Default: 0
     */
    pspec = g_param_spec_int ("current-state",
                              "CurrentState",
                              "Current state of the button",
                              0, G_MAXINT,
                              0,
                              G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_CURRENT_STATE, pspec);

    /**
     * MildenhallToggleButton:state-model:
     *
     * model for the button states. If button model has more than
     * one row,  left/right arrows will be enabled
     * model content:
     *
     * 1. Text
     * 2. Active icon path,
     * 3. Normal icon path
     * 4. Unique State ID for corresponding states
     *
     * Default: #NULL
     */
    pspec = g_param_spec_object ("state-model",
                                 "StateModel",
                                 "model having button states data",
                                 G_TYPE_OBJECT,
                                 G_PARAM_READWRITE);
    g_object_class_install_property (pObjectClass, PROP_STATE_MODEL, pspec);
}

/********************************************************
 * Function : mildenhall_toggle_button_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_toggle_button_init (MildenhallToggleButton *self)
{
    MILDENHALL_TOGGLE_BUTTON_PRINT("%s \n",__FUNCTION__);
    self->priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (self);

    /* initialize the priv structure */
    self->priv->fltWidth = 0.0;
    self->priv->fltHeight = 0.0;
    self->priv->bMultiState = FALSE ;
    self->priv->bActive = FALSE;
    self->priv->inCurrentState = -1;
    self->priv->pStateModel = NULL;
    self->priv->pButtonGroup = NULL ;
    self->priv->pArrowRight = NULL;
    self->priv->pArrowLeft = NULL;
    self->priv->fltTextImageSpace = 10.0;
    self->priv->fltTextPositionXdisplace = 0.0;
    self->priv->pStyleHash = NULL;
    self->priv->fltArrowMargin = 5.0;
    self->priv->pStrArrowLeft = NULL;
    self->priv->pStrArrowRight = NULL;
    self->priv->bHighlight = FALSE;
    self->priv->fltTextMargin = 20.0;
    self->priv->pLangText = NULL;
    /* initialise the button group */
    self->priv->pButtonGroup = clutter_actor_new ();

    clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pButtonGroup);

    /* get the hash table for style properties */
    self->priv->pStyleHash = thornbury_style_set(PKGDATADIR"/mildenhall_toggle_button_style.json");

    /* pares the hash for styles */
    if(NULL != self->priv->pStyleHash)
    {
        GHashTableIter iter;
        gpointer key, value;

        g_hash_table_iter_init(&iter, self->priv->pStyleHash);
        /* iter per layer */
        while (g_hash_table_iter_next (&iter, &key, &value))
        {
            GHashTable *pHash = value;
            if(NULL != pHash)
            {
                g_hash_table_foreach(pHash, v_toggle_button_parse_style, self);
            }
        }
    }
    /* free the style hash */
    thornbury_style_free(self->priv->pStyleHash);

    /* connect the signals */
    g_signal_connect(self, "button-release", G_CALLBACK(v_toggle_button_release_cb), NULL);
    g_signal_connect(self, "button-reset", G_CALLBACK(v_toggle_button_release_cb), NULL);

    /* connect to reactive state to update the button view  */
    g_signal_connect (self, "notify::reactive", G_CALLBACK (v_toggle_button_active_cb),NULL);

}

/**
 * mildenhall_toggle_button_new:
 *
 * Creates a  #MildenhallToggleButton and type-casted to #ClutterActor
 *
 * Returns: (transfer full): a newly created #MildenhallToggleButton object.
 *
 * Since: 0.3.0
 */


ClutterActor *mildenhall_toggle_button_new (void)
{
    MILDENHALL_TOGGLE_BUTTON_PRINT("%s \n",__FUNCTION__);
    return g_object_new (MILDENHALL_TYPE_TOGGLE_BUTTON, NULL);
}




/********************************************************
 * Function : v_toggle_button_update_arrows
 * Description: update the left/right arrows if No of rows is more than one
 * Parameters: MildenhallToggleButton*,
 * Return value: void
 ********************************************************/
static void v_toggle_button_update_arrows(MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT ("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    /* if multi state is set as TRUE, show left/right arrows */
    if(priv->bMultiState)
    {
        priv->TextFontColor = (priv->bHighlight) ? priv->ActiveTextFontColor : priv->NormalTextFontColor;
        if(priv->pStrArrowRight != NULL)
        {
            priv->pArrowRight = clutter_text_new ();
            clutter_text_set_color (CLUTTER_TEXT (priv->pArrowRight), &priv->TextFontColor);
            clutter_text_set_font_name (CLUTTER_TEXT (priv->pArrowRight), priv->pArrowFont );
            clutter_text_set_use_markup (CLUTTER_TEXT (priv->pArrowRight), TRUE);
            /* Use the Right arrow markup from style property */
            clutter_text_set_markup (CLUTTER_TEXT (priv->pArrowRight), priv->pStrArrowRight);
            clutter_actor_set_position(priv->pArrowRight,priv->fltArrowMargin,(priv->fltHeight - clutter_actor_get_height (priv->pArrowRight)) / 2 );
            clutter_actor_add_child (priv->pButtonGroup,priv->pArrowRight);
        }

        if(priv->pStrArrowLeft != NULL )
        {
            priv->pArrowLeft = clutter_text_new ();
            clutter_text_set_color (CLUTTER_TEXT (priv->pArrowLeft), &priv->TextFontColor);
            clutter_text_set_font_name (CLUTTER_TEXT (priv->pArrowLeft), priv->pArrowFont);
            clutter_text_set_use_markup (CLUTTER_TEXT (priv->pArrowLeft), TRUE);
            /* Use the Left arrow markup from style property */
            clutter_text_set_markup (CLUTTER_TEXT (priv->pArrowLeft), priv->pStrArrowLeft);
            clutter_actor_set_position(priv->pArrowLeft,priv->fltWidth - priv->fltArrowMargin,(priv->fltHeight - clutter_actor_get_height (priv->pArrowLeft)) / 2 );
            clutter_actor_add_child (priv->pButtonGroup,priv->pArrowLeft);
        }


    }
    else
    {
        /* if not multi state, hide arrows */

        if(priv->pArrowLeft)
            clutter_actor_hide(priv->pArrowLeft);
        if(priv->pArrowRight)
            clutter_actor_hide(priv->pArrowRight);
    }
}










/********************************************************
 * Function : v_toggle_button_check_multi_state
 * Description: if model has more than one state, show arrows and set as reactive
 * Parameters: ThornburyModel *, MildenhallToggleButton *
 * Return value: void
 ********************************************************/
static void v_toggle_button_check_multi_state(MildenhallToggleButton *pToggleButton, ThornburyModel *pModel)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    g_return_if_fail (G_IS_OBJECT (pModel));

    g_object_set(pToggleButton, "reactive", TRUE /* FALSE */, NULL);
    if(thornbury_model_get_n_rows (priv->pStateModel) > 1)
    {
        priv->bMultiState = TRUE;
        priv->bActive = TRUE;
        //g_object_set(pToggleButton, "reactive", TRUE, NULL);
    }
    else
    {
        priv->bMultiState = FALSE;
        priv->bActive = FALSE;
       // g_object_set(pToggleButton, "reactive", FALSE, NULL);
        if(priv->pArrowLeft)
            clutter_actor_hide(priv->pArrowLeft);
        if(priv->pArrowRight)
            clutter_actor_hide(priv->pArrowRight);
    }

    v_toggle_button_update_arrows(pToggleButton);

}


/********************************************************
 * Function : v_toggle_button_release_cb
 * Description: callabck on toggle button release
 * Parameters: MildenhallToggleButton*, pUserData
 * Return value: void
 ********************************************************/
static void v_toggle_button_release_cb(ClutterActor *pToggleButton, gpointer pUserData)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(!MILDENHALL_IS_TOGGLE_BUTTON(pToggleButton))
    {
        g_warning("invalid button\n");
        return;
    }

    if(thornbury_model_get_n_rows(priv->pStateModel) == 1)
    {
        gpointer* pSendStateId = NULL;
        ThornburyModelIter *pIter = thornbury_model_get_iter_at_row (priv->pStateModel, 0);
        if(NULL != pIter)
        {
            GValue value = { 0, };
            /* | TEXT  | IMAGE_PATH_ACTIVE  | IMAGE_PATH_INACTIVE  | STATE_ID | */

            /* get the Last column value to get State ID  */
            thornbury_model_iter_get_value(pIter,COLOUMN_FOURTH, &value);
            pSendStateId = g_value_get_pointer(&value);
            g_value_unset (&value);
        }
        g_signal_emit(pToggleButton, mildenhall_toggle_bt_signals[SIG_BUTTON_TOGGLED], 0, pSendStateId);
        return;
    }

    if ((unsigned int) priv->inCurrentState + 1 < thornbury_model_get_n_rows (priv->pStateModel))
        g_object_set(pToggleButton, "current-state", priv->inCurrentState + 1, NULL);
    else
        g_object_set(pToggleButton, "current-state", priv->inCurrentState -priv->inCurrentState, NULL);



}


/********************************************************
 * Function : v_toggle_button_set_icon
 * Description: set the icon
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_toggle_button_set_icon(ClutterActor *pActor)
{
    MildenhallToggleButton *pToggleButton = MILDENHALL_TOGGLE_BUTTON (pActor);
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if(NULL != priv->pDisplayImage)
    {
        gfloat flX = 0.0, flY = 0.0;
        /* creating first time */
        if(NULL == priv->pShowIcon)
        {
            priv->pShowIcon = thornbury_ui_texture_create_new(priv->pDisplayImage, 0, 0, FALSE, FALSE);
            if(priv->pShowIcon != NULL)
            {
                if(clutter_actor_get_parent (priv->pShowIcon) == NULL)
                    clutter_actor_add_child(priv->pButtonGroup, priv->pShowIcon);
            }
            else
            {
                MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: Icon group not created %s\n", __FUNCTION__);
                return;
            }
        }
        else
            thornbury_ui_texture_set_from_file(priv->pShowIcon, priv->pDisplayImage, 0, 0, FALSE, FALSE);

        /* if icon size exceeds, restrict to max */
        if(clutter_actor_get_height(priv->pShowIcon) > priv->fltHeight)
            g_object_set(priv->pShowIcon, "height", priv->fltHeight, NULL);


        switch (priv->display_type)
        {
        case MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY:
        case MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT:
          if (clutter_actor_get_width (priv->pShowIcon) > priv->fltWidth)
            {
              g_object_set (priv->pShowIcon, "width", priv->fltWidth, NULL);
            }
          flX = (priv->fltWidth-clutter_actor_get_width (priv->pShowIcon)) / 2;
          break;
        case MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY:
          /* FIXME: do nothing for text-only? */
          break;
        case MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE:
          break;
        default:
          MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: Invalid Display type\n");
          break;
        }

        /* set the position of icon to mid of the button */
        flY = (priv->fltHeight-clutter_actor_get_height(priv->pShowIcon)) / 2;
        g_object_set(priv->pShowIcon, "x", flX, "y", flY, NULL);

    }

}

/********************************************************
 * Function : v_toggle_button_set_text
 * Description: set the text
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_toggle_button_set_text(ClutterActor *pActor)
{
    MildenhallToggleButton *pToggleButton = MILDENHALL_TOGGLE_BUTTON(pActor);
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE(pToggleButton);
    gfloat flX, flY, flCurrentTextWidth, flCurrentTextHeight;
    MILDENHALL_TOGGLE_BUTTON_PRINT ("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    if (priv->pShowTextActor == NULL)
    {
        priv->pShowTextActor = clutter_text_new ();
        /* add to the button group */
        if(priv->pShowTextActor != NULL)
        {
            /* if its created first time, add to the container */
            if (clutter_actor_get_parent (priv->pShowTextActor) == NULL)
                clutter_actor_add_child (priv->pButtonGroup, priv->pShowTextActor);
        }
        clutter_text_set_single_line_mode (CLUTTER_TEXT (priv->pShowTextActor), TRUE);
        clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pShowTextActor), PANGO_ELLIPSIZE_END);
        clutter_text_set_font_name (CLUTTER_TEXT (priv->pShowTextActor), priv->pTextFont);
        clutter_actor_show(priv->pShowTextActor);
    }

    clutter_text_set_text (CLUTTER_TEXT (priv->pShowTextActor), gettext(priv->pDisplayText ));

    if(clutter_actor_get_width(priv->pShowTextActor) > (priv->fltWidth - priv->fltTextPositionXdisplace) )
    {
        clutter_actor_set_width(priv->pShowTextActor, (priv->fltWidth - priv->fltTextPositionXdisplace));
    }


    flCurrentTextWidth = clutter_actor_get_width(priv->pShowTextActor);
    flCurrentTextHeight = clutter_actor_get_height(priv->pShowTextActor);


    if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY)
      {
        switch (priv->alignment_type)
          {
          case MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_LEFT:
            flX = priv->fltTextMargin;
            break;
          case MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_MIDDLE:
            flX = (priv->fltWidth - flCurrentTextWidth) / 2;
            break;
          case MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_RIGHT:
            flX = priv->fltWidth -(flCurrentTextWidth + priv->fltTextMargin);
	    break;
	  case MILDENHALL_TOGGLE_BUTTON_ALIGNMENT_NONE:
	    break;
          default:
            MILDENHALL_TOGGLE_BUTTON_PRINT ("MILDENHALL_TOGGLE_BUTTON_PRINT: Invalid Alignment type\n");
            break;
          }
      }
    else if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT)
      {
        flX = priv->fltTextPositionXdisplace + priv->fltTextImageSpace;
      }
    else
      {
        MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: Invalid Display type\n");
      }

    flY = (priv->fltHeight - flCurrentTextHeight) /2 ;
    clutter_actor_set_position (priv->pShowTextActor,flX, flY );
    priv->TextFontColor = (priv->bHighlight) ? priv->ActiveTextFontColor : priv->NormalTextFontColor;
    clutter_text_set_color(CLUTTER_TEXT (priv->pShowTextActor),&priv->TextFontColor);
}
/********************************************************
 * Function : v_toggle_button_update_view
 * Description: update the button view
 * Parameters: MildenhallToggleButton *
 * Return value: void
 ********************************************************/
static void v_toggle_button_update_view(MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE (pToggleButton);
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);

    /* Return if display type is not valid */
    g_return_if_fail (priv->display_type != MILDENHALL_TOGGLE_BUTTON_DISPLAY_NONE);

    if (G_IS_OBJECT(priv->pStateModel) && thornbury_model_get_n_rows (priv->pStateModel) > 0)
    {
        /* if current state is not set, set it to 0 */
        ThornburyModelIter *pIter;
        if(thornbury_model_get_n_rows (priv->pStateModel) > 0 && priv->inCurrentState == -1)
            priv->inCurrentState = 0;

        pIter = thornbury_model_get_iter_at_row (priv->pStateModel, priv->inCurrentState);
        if(NULL != pIter)
        {
            GValue value = { 0, };
            /* | TEXT  | IMAGE_PATH  | USERDATA | */

            if(priv->display_type != MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY)
                /* get the first column value to get text */
            {
                thornbury_model_iter_get_value(pIter, COLOUMN_FIRST, &value);
		g_free (priv->pDisplayText);
		priv->pDisplayText = g_value_dup_string (&value);
		if (priv->pDisplayText == NULL)
		  {
		    priv->pDisplayText = g_strdup ("");
		  }
                g_value_unset (&value);
            }

            if (priv->display_type != MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY)
            {
                /* get the second column value for icon image path */
                gchar *icon = NULL;
                if(priv->bHighlight == FALSE)
                    thornbury_model_iter_get_value(pIter, COLOUMN_THIRD, &value);
                else
                    thornbury_model_iter_get_value(pIter, COLOUMN_SECOND, &value);
                /* set the icon */
                icon =  g_value_dup_string (&value) ;
                if(icon != NULL)
                    priv->pDisplayImage = g_strdup(icon);
                else
                    g_warning("TOGGLE BUTTON ; check image path: icon  path returned null: reloading previous one");
                g_value_unset (&value);

            }

            if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY
              || priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT)
                if(priv->pDisplayImage != NULL)
                    v_toggle_button_set_icon(CLUTTER_ACTOR(pToggleButton));

            if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY
              || priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT)
                v_toggle_button_set_text(CLUTTER_ACTOR(pToggleButton));

            /* Show Text or Image depending upon Display Type */
            if(priv->pShowIcon != NULL)
            {
                if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY)
                    clutter_actor_hide(priv->pShowIcon);
                else
                    clutter_actor_show(priv->pShowIcon);
            }


            if(priv->pShowTextActor != NULL)
            {
                if (priv->display_type == MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY)
                    clutter_actor_hide(priv->pShowTextActor);
                else
                    clutter_actor_show(priv->pShowTextActor);
            }

        }
        else
	{
            MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: Model Iter failed : exiting function %s\n",__FUNCTION__);
	}
    }
    else
    {
        MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: Model not set : exiting function %s\n",__FUNCTION__);
    }
}




/********************************************************
 * Function : v_toggle_button_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallToggleButton *
 * Return value: void
 ********************************************************/
static void v_toggle_button_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton)
{
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    /* update the multi state */
    v_toggle_button_check_multi_state(pToggleButton, pModel);
    /* update view */
    v_toggle_button_update_view(pToggleButton);
    clutter_actor_queue_relayout (CLUTTER_ACTOR (pToggleButton));
    if(thornbury_model_get_n_rows(pModel) > 0)
    {
        clutter_actor_show(CLUTTER_ACTOR(pToggleButton));
    }
}

/********************************************************
 * Function : v_toggle_button_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallToggleButton *
 * Return value: void
 ********************************************************/
static void v_toggle_button_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton)
{
    MILDENHALL_TOGGLE_BUTTON_PRINT("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);
    /* update view */
    v_toggle_button_update_view(pToggleButton);
    clutter_actor_queue_relayout (CLUTTER_ACTOR (pToggleButton));
    if(thornbury_model_get_n_rows(pModel) > 0)
    {
        clutter_actor_show(CLUTTER_ACTOR(pToggleButton));
    }
}

/********************************************************
 * Function : v_toggle_button_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallToggleButton *
 * Return value: void
 ********************************************************/
static void v_toggle_button_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallToggleButton *pToggleButton)
{
    MildenhallToggleButtonPrivate *priv = MILDENHALL_TOGGLE_BUTTON_GET_PRIVATE(pToggleButton);

    /* this callback gives the actual row count before removal, and not after removal of row */
    gint inRemovedRow = thornbury_model_iter_get_row (pIter);
    MILDENHALL_TOGGLE_BUTTON_PRINT ("MILDENHALL_TOGGLE_BUTTON_PRINT: %s\n", __FUNCTION__);


    /* if the row removed is the current state, reset the current state */
    if(thornbury_model_get_n_rows(pModel) == 1)
    {
        clutter_actor_hide(CLUTTER_ACTOR(pToggleButton));
    }
    else if(inRemovedRow == priv->inCurrentState)
    {
        /* if there is next row, switch to next state else switch to previous state */
        if((unsigned int) priv->inCurrentState + 1 < thornbury_model_get_n_rows (priv->pStateModel) )
            g_object_set(pToggleButton, "current-state", priv->inCurrentState + 1, NULL);
        else
            g_object_set(pToggleButton, "current-state", priv->inCurrentState - 1, NULL);

    /* state removed is less than the current state, update the current state in priv */
    if(inRemovedRow < priv->inCurrentState)
        priv->inCurrentState = priv->inCurrentState - 1;
    }

    /* this callback gives the actual row count before removal, so update the multi state */
    if(thornbury_model_get_n_rows(priv->pStateModel) == 2)
        priv->bMultiState = FALSE;

}

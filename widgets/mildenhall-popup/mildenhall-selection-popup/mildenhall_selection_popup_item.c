/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *	@Filename : mildenhall-selection-popup-item.c
 *	@Project: --
 *-----------------------------------------------------------------------------
 * 	@Created on : Jan 7, 2013
 *------------------------------------------------------------------------------
 *  @Description : This widget can be used to create roller item type for roller
 *  			Icon | Label | Label |
 *  			Icon | Label | Radio button |
 *  			any of the above two Selection popup Roller Item type can be created
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description								Date			Name
 *	----------								----			----
 *	1 . replace of deprecated Cogltexture with ui_texture			7/3/2013
 *	2 . Adjustment of Text width according to space available		28/3/2013
 *	    without overlaping
 *	3.  Complexity score reduced						28/05/2013
 *******************************************************************************/

#include "mildenhall_selection_popup_item.h"

#include <clutter/clutter.h>
#include <cogl/cogl.h>
#include <glib-object.h>
#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include <liblightwood-roller.h>

#include "mildenhall_radio_button.h"

/***********************************************************
 * @local function declarations
 **********************************************************/
#if 0
static void mildenhall_selection_popup_item_expandable_iface_init(
		LightwoodExpandableIface *pIface);
#endif
static gboolean mildenhall_selection_popup_item_row_pressed_cb(ClutterActor *actor,
		ClutterButtonEvent *pEvent, gpointer pUserData);
static gboolean mildenhall_selection_popup_item_row_released_cb(ClutterActor *actor,
		ClutterButtonEvent *pEvent, gpointer pUserData);
static gboolean mildenhall_selection_popup_item_touch_event_cb(ClutterActor *actor,
		ClutterButtonEvent *pEvent, gpointer pUserData);


static void v_mildenhall_selection_popup_item_scrolled_cb(ClutterActor *actor,
		gpointer pUserData);
static void v_selection_popup_item_set_item_type(
		MildenhallSelectionPopupItem *pSelectionPopupItem, enMildenhallSelectionPopupItemType inItemType);
/******************************************************************************
 * @Defines
 *****************************************************************************/

/**
 * _enSelectionPopupItemProperty:
 * @PROP_LEFT_ICON : Property to handle Left Icon To be displayed
 * @PROP_LABEL : Property for displaying the test in Main Text Field
 * @PROP_RIGHT_STRING : Property for displaying the right text or setting the state of Radio button
 * @PROP_ITEM_TYPE : Property for specifing the item type
 *
 * property enums of Selection Popup Roller Item
 */
typedef enum _enSelectionPopupItemProperty enSelectionPopupItemProperty;
enum _enSelectionPopupItemProperty
{
	/*< pSelectionPopupItemate >*/
	PROP_FIRST,
	PROP_LEFT_ICON,
	PROP_LABEL,
	PROP_RIGHT_STRING,
	PROP_ITEM_TYPE,
	PROP_MODEL,
	PROP_SELECTION_ROW,
	PROP_LAST
};

#if 0
G_DEFINE_TYPE_WITH_CODE (MildenhallSelectionPopupItem, mildenhall_selection_popup_item, CLUTTER_TYPE_GROUP,
		G_IMPLEMENT_INTERFACE (LIGHTWOOD_TYPE_EXPANDABLE,
				mildenhall_selection_popup_item_expandable_iface_init));
*/
#endif

G_DEFINE_TYPE(MildenhallSelectionPopupItem, mildenhall_selection_popup_item,
		CLUTTER_TYPE_GROUP);

#define WITH_COGL 0

#define MILDENHALL_SELECTION_POPUP_ITEM_PRINT( a ...)		/*	g_print(a) */

/* 'MILDENHALL Selection Popup Item' Specific style */

#define SELECTION_POPUP_ITEM_NORMAL_IMAGE "normal-image"
#define SELECTION_POPUP_ITEM_PRESSED_IMAGE "pressed-image"
#define SELECTION_POPUP_ITEM_LABEL_FIRST_FONT "main-font"
#define SELECTION_POPUP_ITEM_LABEL_FIRST_COLOR "main-font-color"
#define SELECTION_POPUP_ITEM_LABEL_SECOND_FONT "text-font"
#define SELECTION_POPUP_ITEM_LABEL_SECOND_COLOR "text-font-color"
#define SELECTION_POPUP_ITEM_VERTICAL_LINE_COLOR "line-color"

/* MILDENHALL Selection Popup Item Specific Dimensions */

#define VERTICAL_LINE_WIDTH 1.0							/* Vertical line width  */
#define VERTICAL_LINE_HEIGHT 58.0						/* Vertical line height   */
#define VERTICAL_LINE_X 64.0							/* Vertical line x Position  */
#define VERTICAL_LINE_Y 5.0								/* Vertical line y Position   */
#define MILDENHALL_SELECTION_POPUP_ROW_HEIGHT  66.0			/* Row height  */
#define MILDENHALL_SELECTION_POPUP_ROW_WIDTH   588.0			/* Row Width */
/* Spacing of Text from Vertical line and from end of row  */
#define SELECTION_POPUP_ITEM_TEXT_SPACING 10.0
#define SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH 310.0
#define SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH 180.0


#define LEFT_ICON_MAX_W	58.0
#define LEFT_ICON_MAX_H 58.0
#define LEFT_ICON_EXTRA_Y_DISP 1.0


#define FREE_MEM_IF_NOT_NULL(pointer) 		if(NULL != pointer) \
		{ \
	g_free(pointer); \
	pointer = NULL; \
		}


/************************************************************
 * @Functions
 ***********************************************************/





GType v_selection_popup_get_item_type(void) {
	/* argument must point to a static 0-initialized variable,
	 * that will be set to a value other than 0 at the end of
	 * the initialization section.
	 */
	static volatile gsize gEnumTypeVolatile = 0;

	/* swipe direction as enum initialization section */
	if (g_once_init_enter(&gEnumTypeVolatile)) {
		/* A structure which contains a single flags value, its name, and its nickname. */
		static const GEnumValue arValues[] = {
				{ TYPE_ICON_LABEL_LABEL,"ICON_LABEL_LABEL_TYPE", "default" },
				{ TYPE_ICON_LABEL_RADIO,"ICON_LABEL_RADIO_TYPE", "radio" },
				{ TYPE_ICON_LABEL_CONFIRM,"TYPE_ICON_LABEL_CONFIRM","InfoConfirm"},
				{ 0, NULL, NULL }
		};
		GType gEnumType;

		/* Registers a new static flags type with the name name. */
		gEnumType = g_enum_register_static(
				g_intern_static_string("enMildenhallSelectionPopupItemType"), arValues);

		/* In combination with g_once_init_leave() and the unique address value_location,
		 * it can be ensured that an initialization section will be executed only once
		 * during a program's life time, and that concurrent threads are blocked
		 * until initialization completed.
		 */
		g_once_init_leave(&gEnumTypeVolatile, gEnumType);
	}

	return gEnumTypeVolatile;
}

static void v_mildenhall_selection_popup_item_set_right_string(MildenhallSelectionPopupItem *pSelectionPopupItem ,const GValue *pValue)
{

	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pRightFieldString);
	pSelectionPopupItem->pRightFieldString = g_value_dup_string(pValue);

	if (NULL != pSelectionPopupItem->pRightField)
	{
		if (TYPE_ICON_LABEL_LABEL == pSelectionPopupItem->inItemType)
		{
			gint inTotalNoChars = 0;
			inTotalNoChars = strlen(clutter_text_get_text(CLUTTER_TEXT (pSelectionPopupItem->pRightField)));
			clutter_text_delete_text(CLUTTER_TEXT (pSelectionPopupItem->pRightField), 0,inTotalNoChars);

			/* Text has to be displayed */
			if(NULL != pSelectionPopupItem->pRightFieldString)
			{
				clutter_text_set_text(
						CLUTTER_TEXT(pSelectionPopupItem->pRightField),
						pSelectionPopupItem->pRightFieldString);
			}

			gfloat fltTextWidth = clutter_actor_get_width(
					pSelectionPopupItem->pRightField);
			if (SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH < fltTextWidth)
			{
				/* if text width exceeds the maximum alloted area , restrict the width */
				gfloat fltExtraTextWidth = 0.0;
				fltExtraTextWidth = clutter_actor_get_width(
						pSelectionPopupItem->pMiddleField);

				if(fltExtraTextWidth < SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH)
				{
					/* extra space that can be allocated to Mid field in case of Right Text Type */
					fltExtraTextWidth =  SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH  - SELECTION_POPUP_ITEM_TEXT_SPACING - fltExtraTextWidth;
				}
				else if(fltExtraTextWidth >= SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH)
				{
					fltExtraTextWidth = 0.0;
					clutter_actor_set_width(pSelectionPopupItem->pMiddleField , SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH);
				}

				clutter_actor_set_width(pSelectionPopupItem->pRightField,
						( SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH + fltExtraTextWidth));
				fltTextWidth = SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH + fltExtraTextWidth;
			}
			/* set position of Text aligned to centre  of the Row */

			clutter_actor_set_position(pSelectionPopupItem->pRightField,
					(MILDENHALL_SELECTION_POPUP_ROW_WIDTH
							- (SELECTION_POPUP_ITEM_TEXT_SPACING
									+ fltTextWidth)),
									(MILDENHALL_SELECTION_POPUP_ROW_HEIGHT
											- clutter_actor_get_height(
													pSelectionPopupItem->pRightField)) / 2);
		}
		else if (NULL != pSelectionPopupItem->pRightFieldString
				&& TYPE_ICON_LABEL_RADIO == pSelectionPopupItem->inItemType)
		{
			/* Radio button has to be set/unset */
			guint uinState;
			sscanf(g_value_get_string(pValue), "%d", &uinState);
			/* set the state of the Radio button only if the state requested is true */
			if (uinState)
				mildenhall_radio_button_set_current_state(
						MILDENHALL_RADIO_BUTTON(pSelectionPopupItem->pRightField ),
						(gboolean) uinState);
		}
	}

}


static void v_mildenhall_selection_popup_item_restrict_width(MildenhallSelectionPopupItem *pSelectionPopupItem)
{
	gfloat fltRightTextWidth = 0.0;
	if(TYPE_ICON_LABEL_LABEL == pSelectionPopupItem->inItemType)
	{
		if(NULL != pSelectionPopupItem->pRightField)
		{
			fltRightTextWidth = clutter_actor_get_width(
					pSelectionPopupItem->pRightField);

			if(fltRightTextWidth < SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH)
			{
				/* extra space that can be allocated to Mid field in case of Right Text Type */
				fltRightTextWidth =  SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH  - SELECTION_POPUP_ITEM_TEXT_SPACING - fltRightTextWidth;
			}
			else if(fltRightTextWidth >= SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH)
			{
				fltRightTextWidth = 0.0;
				clutter_actor_set_width(pSelectionPopupItem->pRightField , SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH);

				clutter_actor_set_position(pSelectionPopupItem->pRightField,
						(MILDENHALL_SELECTION_POPUP_ROW_WIDTH
								- (SELECTION_POPUP_ITEM_TEXT_SPACING
										+ SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH)),
										(MILDENHALL_SELECTION_POPUP_ROW_HEIGHT
												- clutter_actor_get_height(
														pSelectionPopupItem->pRightField)) / 2);

			}
		}
	}
	else if(TYPE_ICON_LABEL_RADIO == pSelectionPopupItem->inItemType)
	{
		fltRightTextWidth = MILDENHALL_SELECTION_POPUP_ROW_WIDTH - clutter_actor_get_x(CLUTTER_ACTOR(pSelectionPopupItem->pRightField));
		/* extra space that can be allocated to Mid field in case of Radio button */
		fltRightTextWidth = SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH -
				( SELECTION_POPUP_ITEM_TEXT_SPACING  )-
				fltRightTextWidth ;

	}

	clutter_actor_set_width(pSelectionPopupItem->pMiddleField,
			(SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH + fltRightTextWidth ) );

}


static void v_mildenhall_selection_popup_item_set_label(MildenhallSelectionPopupItem *pSelectionPopupItem ,const GValue *pValue)
{
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pMiddleText);
	pSelectionPopupItem->pMiddleText = g_value_dup_string(pValue);

	gfloat fltNewPositionX = 0.0 , fltNewPositionY = 0.0 ;

	if(NULL != pSelectionPopupItem->pMiddleField )
	{
		gint inTotalNoChars = 0;
		inTotalNoChars = strlen(clutter_text_get_text(CLUTTER_TEXT (pSelectionPopupItem->pMiddleField)));
		clutter_text_delete_text(CLUTTER_TEXT (pSelectionPopupItem->pMiddleField), 0,inTotalNoChars);

		if (NULL != pSelectionPopupItem->pMiddleText)
		{
			/* Display the text in Mid field*/
			clutter_text_set_text(	CLUTTER_TEXT(pSelectionPopupItem->pMiddleField),pSelectionPopupItem->pMiddleText);
		}

		gfloat fltTextWidth = clutter_actor_get_width(pSelectionPopupItem->pMiddleField);
		gfloat fltRightTextWidth = 0.0;

		if (SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH < fltTextWidth)
		{
			/* if text width exceeds the maximum alloted area , restrict the width */
			v_mildenhall_selection_popup_item_restrict_width(pSelectionPopupItem);
		}
		else
		{
			if(TYPE_ICON_LABEL_LABEL == pSelectionPopupItem->inItemType && NULL != pSelectionPopupItem->pRightField)
			{
				fltRightTextWidth = clutter_actor_get_width( pSelectionPopupItem->pRightField);
				gfloat fltExtraTextWidth = clutter_actor_get_width(	pSelectionPopupItem->pMiddleField);

				if(fltRightTextWidth >= SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH)
				{
					fltExtraTextWidth = SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH - fltExtraTextWidth ;
					fltExtraTextWidth += SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH ;
					clutter_actor_set_width(pSelectionPopupItem->pRightField ,( fltExtraTextWidth) );

					fltNewPositionX = (MILDENHALL_SELECTION_POPUP_ROW_WIDTH	- (SELECTION_POPUP_ITEM_TEXT_SPACING
							+ fltExtraTextWidth)) ;
					fltNewPositionY = ((MILDENHALL_SELECTION_POPUP_ROW_HEIGHT - clutter_actor_get_height(pSelectionPopupItem->pRightField)) / 2 );
					clutter_actor_set_position(pSelectionPopupItem->pRightField,fltNewPositionX	,fltNewPositionY	);

				}
			}

		}

		/* set position of Text aligned to centre  of the Row */
		fltNewPositionX = (SELECTION_POPUP_ITEM_TEXT_SPACING + VERTICAL_LINE_WIDTH
				+ VERTICAL_LINE_X) ;
		fltNewPositionY = ((MILDENHALL_SELECTION_POPUP_ROW_HEIGHT	- clutter_actor_get_height(	pSelectionPopupItem->pMiddleField)) / 2) ;

		clutter_actor_set_position(pSelectionPopupItem->pMiddleField,fltNewPositionX ,fltNewPositionY );
	}
}

static void v_mildenhall_selection_popup_item_reposition_left_icon(
		MildenhallSelectionPopupItem *pSelectionPopupItem ,ClutterActor *pLeftIconActor)
{
	if(NULL != pLeftIconActor)
		{
			if(clutter_actor_get_parent (pLeftIconActor) == NULL)
			{
				clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
						pLeftIconActor);
			}
			clutter_actor_show(pLeftIconActor);

			/* Restrict the width if icon exceeds allocated space */
			gfloat fltWidth , fltHeight  , fltDisp = 0.0 ;
			clutter_actor_get_size(pLeftIconActor,&fltWidth , &fltHeight );
			if(fltWidth > LEFT_ICON_MAX_W)
				clutter_actor_set_width(pLeftIconActor,LEFT_ICON_MAX_W);
			if(fltHeight > LEFT_ICON_MAX_H)
			{
				clutter_actor_set_height(pLeftIconActor,LEFT_ICON_MAX_H);
				fltDisp = LEFT_ICON_EXTRA_Y_DISP ;
			}

			/* set position of Icon aligned to centre  of the Row */
			clutter_actor_set_position(pLeftIconActor,
					(VERTICAL_LINE_X
							- clutter_actor_get_width(
									pLeftIconActor)) / 2,
									fltDisp +	(MILDENHALL_SELECTION_POPUP_ROW_HEIGHT
											- clutter_actor_get_height(
													pLeftIconActor)) / 2);

//			g_print("WIDTH= %f \t Height = %f \t X = %f /t Y = %f \n ", clutter_actor_get_width(pLeftIconActor),
//					clutter_actor_get_height(pLeftIconActor),clutter_actor_get_x(pLeftIconActor),clutter_actor_get_y(pLeftIconActor)	);

		}

}



static void v_mildenhall_selection_popup_item_set_left_icon(MildenhallSelectionPopupItem *pSelectionPopupItem ,const GValue *pValue)
{
	/* Load the file to previously created texture */
	MildenhallSelPopupItemIconDetail *pIconDetail = g_value_get_pointer(pValue);
	if(NULL == pIconDetail  )
	{
		if(NULL != pSelectionPopupItem->pLeftField)
			clutter_actor_hide(pSelectionPopupItem->pLeftField);
		return;
	}
	gchar *pIconName = NULL ;
	if(NULL != pSelectionPopupItem->pLeftField)
	{
		pIconName = (gchar*)clutter_actor_get_name(CLUTTER_ACTOR(pSelectionPopupItem->pLeftField));
	}
	/* ClutterContent is NULL and Icon shown is same as in the model then do nothing */
	if( g_strcmp0(pIconName ,pIconDetail->pRowId) || ( pIconDetail->pIconContent != NULL   &&
			pIconDetail->bHttpIconUpdated == FALSE ))
	{
		g_print("_______________________ Need To Update MILDENHALL SELECTION POPUP :  %d   _______________________\n" , __LINE__);
		gchar *pCurrentIcon = NULL ;
		/* diff Item data */
		if(NULL != pSelectionPopupItem->pLeftField)
		{
			/* destroy and recreate the Actor */
			clutter_actor_destroy(pSelectionPopupItem->pLeftField);
			pSelectionPopupItem->pLeftField = NULL ;
		}
		/* else
		 * check if the content is valid and update the content
		 */
		if(NULL != pIconDetail->pIconContent && CLUTTER_IS_CONTENT(pIconDetail->pIconContent))
		{
			g_print("_______________________ Settings Content MILDENHALL SELECTION POPUP :  %d   _______________________\n" , __LINE__);
			pSelectionPopupItem->pLeftField = clutter_actor_new();
			clutter_actor_set_content (pSelectionPopupItem->pLeftField ,pIconDetail->pIconContent ) ;
			clutter_actor_set_size(pSelectionPopupItem->pLeftField,LEFT_ICON_MAX_W , LEFT_ICON_MAX_H );
			pIconDetail->bHttpIconUpdated = TRUE ;
			pCurrentIcon = pIconDetail->pHttpFilePath ;
		}
		else if(NULL != pIconDetail->pLocalFilePath	)
		{
			pSelectionPopupItem->pLeftField = thornbury_ui_texture_create_new(
					pIconDetail->pLocalFilePath	,
					0.0 , 0.0, FALSE, FALSE);
			pCurrentIcon = pIconDetail->pLocalFilePath ;
		}
		else
		{
			if(NULL != pSelectionPopupItem->pLeftField)
				clutter_actor_hide(pSelectionPopupItem->pLeftField);
			return;
		}
		if(NULL != pSelectionPopupItem->pLeftField)
		{
		clutter_actor_set_name(CLUTTER_ACTOR(pSelectionPopupItem->pLeftField) ,pIconDetail->pRowId );
		FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pLeftIcon )
		pSelectionPopupItem->pLeftIcon = g_strdup(pCurrentIcon);
		/* check if Texture path is valid and Texture is created */
		v_mildenhall_selection_popup_item_reposition_left_icon(pSelectionPopupItem ,pSelectionPopupItem->pLeftField );
		}
	}
}



/* setter functions definition */

/********************************************************
 * Function : v_mildenhall_selection_popup_item_get_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_get_property(GObject *pObject,
		guint inProperty_id, GValue *pValue, GParamSpec *pSpec)
{
	if (!MILDENHALL_IS_SELECTION_POPUP_ITEM(pObject))
	{
		g_warning("invalid MILDENHALL_SELECTION_POPUP_ROLLER_ITEM object\n");
		return;
	}
	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pObject);
	switch (inProperty_id)
	{
	case PROP_LEFT_ICON:
	{
		if(NULL != pSelectionPopupItem->pLeftField )
		{
#if WITH_COGL
			CoglTexture = clutter_texture_get_cogl_texture(
					CLUTTER_TEXTURE(pSelectionPopupItem->pLeftField));
			cogl_handle_ref(CoglTexture);
			g_value_set_boxed(pValue, CoglTexture);
#endif
			g_value_set_pointer(pValue, pSelectionPopupItem->pLeftIcon);

		}
	}
	break;
	case PROP_RIGHT_STRING:

		if (TYPE_ICON_LABEL_LABEL == pSelectionPopupItem->inItemType)
		{
			/* Return the Text displayed in the Right field */
			g_value_set_string(pValue, pSelectionPopupItem->pRightFieldString);
		}
		else if (TYPE_ICON_LABEL_RADIO == pSelectionPopupItem->inItemType)
		{
			/* Return the state of the radio button */
			gboolean bRadioState = mildenhall_radio_button_get_current_state(
					MILDENHALL_RADIO_BUTTON(pSelectionPopupItem->pRightField ));
			g_value_set_string(pValue,
					g_strdup(bRadioState ? "true" : "false"));
		}
		break;
	case PROP_ITEM_TYPE:
		/* Return the type of item type created */
		g_value_set_enum(pValue, pSelectionPopupItem->inItemType);
		break;
	case PROP_LABEL:
		/* Return the Text displayed in the Mid field */
		g_value_set_string(pValue, pSelectionPopupItem->pMiddleText);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(pObject, inProperty_id, pSpec);
		break;
	}
}

/********************************************************
 * Function : v_mildenhall_selection_popup_item_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_set_property(GObject *pObject,
		guint inProperty_id, const GValue *pValue, GParamSpec *pSpec)
{
	if (!MILDENHALL_IS_SELECTION_POPUP_ITEM(pObject))
	{
		g_warning("invalid MILDENHALL_SELECTION_POPUP_ROLLER_ITEM object\n");
		return;
	}
	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pObject);
	enMildenhallSelectionPopupItemType inItemTypeLocal;
	switch (inProperty_id)
	{

	case PROP_LEFT_ICON:
	{
		v_mildenhall_selection_popup_item_set_left_icon(pSelectionPopupItem ,pValue );
	}

	break;
	case PROP_RIGHT_STRING:
		/* Depending upon type of the item type update view of Right field */
	{
		v_mildenhall_selection_popup_item_set_right_string(pSelectionPopupItem ,pValue );

	}
	break;
	case PROP_LABEL:
	{
		v_mildenhall_selection_popup_item_set_label(pSelectionPopupItem ,pValue );
	}
	break;
	case PROP_ITEM_TYPE:
		inItemTypeLocal = g_value_get_enum(pValue);
		if (pSelectionPopupItem->inItemType != inItemTypeLocal)
		{
			/* set the Item type of MILDENHALL Selection Popup Roller Item */
			MILDENHALL_SELECTION_POPUP_ITEM_PRINT("MILDENHALL_SELECTION_POPUP_ITEM: ***** diff item type******%s\n", __FUNCTION__);
			pSelectionPopupItem->inItemType = inItemTypeLocal;
			/* Create or change view based on the item type */
			v_selection_popup_item_set_item_type(pSelectionPopupItem,
					pSelectionPopupItem->inItemType);
		}

		break;
	case PROP_MODEL:
		pSelectionPopupItem->model = g_value_get_object(pValue);
		break;
	case PROP_SELECTION_ROW:
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(pObject, inProperty_id, pSpec);
		break;
	}
}

/********************************************************
 * Function : v_selection_popup_item_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_selection_popup_item_parse_style(gpointer pKey, gpointer pValue,
		gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallSelectionPopupItem *pSelectionPopupItem = pUserData;

	if (g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_NORMAL_IMAGE) == 0)
	{
		pSelectionPopupItem->pNormalImagePath = g_strdup_printf(PKGTHEMEDIR"/%s", (gchar*)g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_PRESSED_IMAGE) == 0)
	{
		pSelectionPopupItem->pPressedImagePath = g_strdup_printf(PKGTHEMEDIR"/%s", (gchar*)g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_LABEL_FIRST_FONT) == 0)
	{
		pSelectionPopupItem->pMainTextFont = (gchar*) g_value_dup_string(pValue);
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_LABEL_FIRST_COLOR) == 0)
	{
		clutter_color_from_string(&pSelectionPopupItem->MainTextColor, g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_LABEL_SECOND_COLOR) == 0)
	{
		clutter_color_from_string(&pSelectionPopupItem->SecondTextColor, g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_LABEL_SECOND_FONT) == 0)
	{
		pSelectionPopupItem->pSecondTextFont = (gchar*) g_value_dup_string(pValue);
	}
	else if(g_strcmp0(pStyleKey, SELECTION_POPUP_ITEM_VERTICAL_LINE_COLOR) == 0)
	{
		clutter_color_from_string(&pSelectionPopupItem->VerticalLineSeperatorColor, g_value_get_string(pValue));
	}

	g_free(pStyleKey);
}

#if 0

static void mildenhall_selection_popup_item_expandable_iface_init (LightwoodExpandableIface *pIface)
{
	/*iface->set_expanded = icon_label_radiobutton_item_set_expanded; */
}
#endif

/********************************************************
 * Function : v_mildenhall_selection_popup_item_dispose
 * Description: Dispose the MILDENHALL selection Popup Roller Item type
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_dispose(GObject *pObject)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);
	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pObject);

	/* free internally allocated memory */

	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pNormalImagePath);
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pPressedImagePath);
	FREE_MEM_IF_NOT_NULL( pSelectionPopupItem->pMainTextFont);
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pSecondTextFont);
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pRightFieldString);
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pLeftIcon);
	FREE_MEM_IF_NOT_NULL(pSelectionPopupItem->pMiddleText);

	G_OBJECT_CLASS(mildenhall_selection_popup_item_parent_class)->dispose(pObject);
}

/********************************************************
 * Function : v_mildenhall_selection_popup_item_finalize
 * Description: Finalise the MILDENHALL selection Popup Roller Item type
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_finalize(GObject *pObject)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);
	G_OBJECT_CLASS(mildenhall_selection_popup_item_parent_class)->finalize(pObject);
}

/********************************************************
 * Function : v_mildenhall_selection_popup_item_actor_allocate
 * Description: Resize and re position to be done when move out of container
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_actor_allocate(ClutterActor *actor,
		const ClutterActorBox *box, ClutterAllocationFlags flags)
{
	CLUTTER_ACTOR_CLASS(mildenhall_selection_popup_item_parent_class)->allocate(actor,
			box, flags);
}

/********************************************************
 * Function : v_mildenhall_selection_popup_item_actor_paint
 * Description: Paint
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_item_actor_paint(ClutterActor *actor)
{
	CLUTTER_ACTOR_CLASS(mildenhall_selection_popup_item_parent_class)->paint(actor);
}

/********************************************************
 * Function : mildenhall_selection_popup_item_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_selection_popup_item_class_init(
		MildenhallSelectionPopupItemClass *pKlass)
{

	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);

	GObjectClass *pObjectClass = G_OBJECT_CLASS(pKlass);
	ClutterActorClass *pActorClass = CLUTTER_ACTOR_CLASS(pKlass);
	GParamSpec *pSpec = NULL;

	pObjectClass->get_property = v_mildenhall_selection_popup_item_get_property;
	pObjectClass->set_property = v_mildenhall_selection_popup_item_set_property;
	pObjectClass->dispose = v_mildenhall_selection_popup_item_dispose;
	pObjectClass->finalize = v_mildenhall_selection_popup_item_finalize;

	pActorClass->paint = v_mildenhall_selection_popup_item_actor_paint;
	pActorClass->allocate = v_mildenhall_selection_popup_item_actor_allocate;

	/**
	 *	MildenhallSelectionPopupItem:left-icon:
	 *
	 * left-icon for roller Item type provided using @MildenhallSelPopupItemIconDetail structure.
	 * Default: NULL
	 */
	pSpec =	g_param_spec_pointer("left-icon", "left icon",
			"complete path of the icon to be displayed",
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_LEFT_ICON, pSpec);

#if WITH_COGL
	pSpec = g_param_spec_boxed("left-icon", "left icon",
			"COGL texture handle for the icon", COGL_TYPE_HANDLE,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_LEFT_ICON, pSpec);
#endif

	/**
	 *	MildenhallSelectionPopupItem:right-string:
	 *
	 * right-string for Item type.String is treated as text or image depending upon type.
	 * If image is needed , image path has to sent and if text is needed to be displayed,
	 * text string has to be passed.This field is ignored if Radio Button has to be displayed.
	 * Default: NULL
	 */
	pSpec =
			g_param_spec_string("right-string", "right-string",
					"right-string for Item type.String is treated as text or image \
			 path depending upon type",
			 NULL, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RIGHT_STRING, pSpec);

	/**
	 *	MildenhallSelectionPopupItem:main-label:
	 *
	 * Main Text to be displayed in Item type
	 * Default: NULL
	 */
	pSpec = g_param_spec_string("main-label", "main-label",
			"Main Text to be displayed in Item type", NULL, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_LABEL, pSpec);

	/**
	 * MildenhallSelectionPopupItem: item-type:
	 *
	 * Specifies the roller item to be created
	 * Default: TYPE_ICON_LABEL_LABEL => ICON | LABEL | LABEL
	 *
	 *
	 *
	 */


	pSpec = g_param_spec_enum("item-type", "item-type",
			"Specifies the roller item to be created", SELECTION_POPUP_GET_ITEM_TYPE,
			TYPE_ICON_LABEL_LABEL, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_ITEM_TYPE, pSpec);


	/**
	 * MildenhallSelectionPopupItem: model:
	 *
	 * Model property
	 */

	pSpec = g_param_spec_object("model", "Roller Model", "Roller model ",
			G_TYPE_OBJECT, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_MODEL, pSpec);

	/**
	 * MildenhallSelectionPopupItem: row:
	 *
	 * Row property
	 */

	pSpec = g_param_spec_uint("row", "row number", "row number", 0, G_MAXUINT,
			0, G_PARAM_WRITABLE);
	g_object_class_install_property(pObjectClass, PROP_SELECTION_ROW, pSpec);
}

/********************************************************
 * Function : mildenhall_selection_popup_item_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_selection_popup_item_init(
		MildenhallSelectionPopupItem *pSelectionPopupItem)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);

	pSelectionPopupItem->pNormalImagePath = NULL;
	pSelectionPopupItem->pPressedImagePath = NULL;
	pSelectionPopupItem->pMainTextFont = NULL;
	pSelectionPopupItem->pSecondTextFont = NULL;
	pSelectionPopupItem->pLeftIcon = NULL;
	pSelectionPopupItem->pRightFieldString = NULL;
	pSelectionPopupItem->inItemType = TYPE_FIRST;
	pSelectionPopupItem->pLeftField = NULL;
	pSelectionPopupItem->pRightField = NULL;
	pSelectionPopupItem->pMiddleField = NULL;
	pSelectionPopupItem->pMiddleText = NULL;
	pSelectionPopupItem->pItemTexture = NULL;
	pSelectionPopupItem->pVerticalLine = NULL;
	pSelectionPopupItem->bSignalConnected = FALSE;
	pSelectionPopupItem->bPressedState = FALSE;

	/* get the hash table for style properties */
	pSelectionPopupItem->pStyleHash = thornbury_style_set(PKGDATADIR"/mh_select_popup_item_style.json");

	/* pares the hash for styles */
	if (NULL != pSelectionPopupItem->pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter, pSelectionPopupItem->pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next(&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if (NULL != pHash)
			{
				g_hash_table_foreach(pHash, v_selection_popup_item_parse_style,
						pSelectionPopupItem);
			}
		}
	}
	/* free the style hash */
	thornbury_style_free(pSelectionPopupItem->pStyleHash);

	if (NULL != pSelectionPopupItem->pNormalImagePath)
	{
		/* create the Normal Texture Background */

		pSelectionPopupItem->pItemTexture = thornbury_ui_texture_create_new(
				pSelectionPopupItem->pNormalImagePath, 0, 0, FALSE, FALSE);
		clutter_actor_set_size(pSelectionPopupItem->pItemTexture,
				MILDENHALL_SELECTION_POPUP_ROW_WIDTH, MILDENHALL_SELECTION_POPUP_ROW_HEIGHT);
		clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
				pSelectionPopupItem->pItemTexture);
		clutter_actor_show(pSelectionPopupItem->pItemTexture);

	}
	/* Create the Vertical Line */
	pSelectionPopupItem->pVerticalLine = clutter_actor_new();
	clutter_actor_set_background_color(CLUTTER_ACTOR(pSelectionPopupItem->pVerticalLine),
			&pSelectionPopupItem->VerticalLineSeperatorColor);
	clutter_actor_set_size(pSelectionPopupItem->pVerticalLine,
			VERTICAL_LINE_WIDTH, VERTICAL_LINE_HEIGHT);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
			pSelectionPopupItem->pVerticalLine);
	clutter_actor_set_position(pSelectionPopupItem->pVerticalLine,
			VERTICAL_LINE_X, VERTICAL_LINE_Y);
	clutter_actor_show(pSelectionPopupItem->pVerticalLine);

	/* Create the Text area for the displaying text in Mid field */

	pSelectionPopupItem->pMiddleField = clutter_text_new();
	clutter_text_set_color(CLUTTER_TEXT(pSelectionPopupItem->pMiddleField),
			&pSelectionPopupItem->MainTextColor);
	clutter_text_set_font_name(CLUTTER_TEXT(pSelectionPopupItem->pMiddleField),
			pSelectionPopupItem->pMainTextFont);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
			pSelectionPopupItem->pMiddleField);
	clutter_actor_show(pSelectionPopupItem->pMiddleField);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pSelectionPopupItem->pMiddleField),
			PANGO_ELLIPSIZE_END);

	/* set the Roller Item to reactive */
	clutter_actor_set_reactive(CLUTTER_ACTOR(pSelectionPopupItem), TRUE);
	g_signal_connect(pSelectionPopupItem, "button-press-event",
			G_CALLBACK(mildenhall_selection_popup_item_row_pressed_cb),
			pSelectionPopupItem);
	g_signal_connect(pSelectionPopupItem, "button-release-event",
			G_CALLBACK(mildenhall_selection_popup_item_row_released_cb),
			pSelectionPopupItem);
	 g_signal_connect (pSelectionPopupItem, "touch-event",
			 G_CALLBACK(mildenhall_selection_popup_item_touch_event_cb),
			 pSelectionPopupItem);

}

/**
 * mildenhall_selection_popup_item_new :
 * @parameters : void
 * @Returns: MILDENHALL Selection popup Roller Item
 *
 * Since: 1.0
 * Creates a MILDENHALL Selection popup Roller Item
 */

ClutterActor* mildenhall_selection_popup_item_new(void)
{

	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);
	return g_object_new(MILDENHALL_TYPE_SELECTION_POPUP_ITEM, NULL);
}


static void v_mildenhall_selection_popup_item_create_radio_button(MildenhallSelectionPopupItem *pSelectionPopupItem)
{
	pSelectionPopupItem->pRightField = mildenhall_radio_button_new();
	g_object_set(pSelectionPopupItem->pRightField, "width",
			MILDENHALL_SELECTION_POPUP_ROW_HEIGHT, "height",
			MILDENHALL_SELECTION_POPUP_ROW_HEIGHT, "group-id",
			"SelectionPopupItem", "current-state", FALSE, NULL);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
			pSelectionPopupItem->pRightField);
	/* set position of button  aligned to centre  of the Row */
	clutter_actor_set_position(pSelectionPopupItem->pRightField,
			(MILDENHALL_SELECTION_POPUP_ROW_WIDTH
					- (SELECTION_POPUP_ITEM_TEXT_SPACING
							+ clutter_actor_get_width(
									pSelectionPopupItem->pRightField))),
									0.0);
	clutter_actor_show(pSelectionPopupItem->pRightField);
}


static void v_selection_popup_item_create_right_text(MildenhallSelectionPopupItem *pSelectionPopupItem)
{

	pSelectionPopupItem->pRightField = clutter_text_new();
	clutter_text_set_color(
			CLUTTER_TEXT(pSelectionPopupItem->pRightField),
			&pSelectionPopupItem->SecondTextColor);
	clutter_text_set_font_name(
			CLUTTER_TEXT(pSelectionPopupItem->pRightField),
			pSelectionPopupItem->pSecondTextFont);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelectionPopupItem),
			pSelectionPopupItem->pRightField);
	clutter_text_set_ellipsize(
			CLUTTER_TEXT(pSelectionPopupItem->pRightField),
			PANGO_ELLIPSIZE_END);
	clutter_text_set_line_alignment(CLUTTER_TEXT(pSelectionPopupItem->pRightField),PANGO_ALIGN_RIGHT);

	if (NULL != pSelectionPopupItem->pRightFieldString)
	{
		/* Text has to be displayed */
		clutter_text_set_text(
				CLUTTER_TEXT(pSelectionPopupItem->pRightField),
				pSelectionPopupItem->pRightFieldString);
		gfloat fltTextWidth = clutter_actor_get_width(
				pSelectionPopupItem->pRightField);
		if (SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH < fltTextWidth)
		{
			/* if text width exceeds the maximum alloted area , restrict the width */

			gfloat fltExtraTextWidth = 0.0;
			fltExtraTextWidth = clutter_actor_get_width(
					pSelectionPopupItem->pMiddleField);

			if(fltExtraTextWidth < SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH)
			{
				/* extra space that can be allocated to Mid field in case of Right Text Type */
				fltExtraTextWidth =  SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH  - SELECTION_POPUP_ITEM_TEXT_SPACING - fltExtraTextWidth;
			}
			else if(fltExtraTextWidth >= SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH)
			{
				fltExtraTextWidth = 0.0;
				clutter_actor_set_width(pSelectionPopupItem->pMiddleField , SELECTION_POPUP_ITEM_MIDFIELD_MAX_WIDTH);
			}

			clutter_actor_set_width(pSelectionPopupItem->pRightField,
					( SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH + fltExtraTextWidth));
			fltTextWidth = SELECTION_POPUP_ITEM_RIGHTFIELD_MAX_WIDTH + fltExtraTextWidth;
		}

		/* set position of Text aligned to centre  of the Row */
		clutter_actor_set_position(pSelectionPopupItem->pRightField,
				(MILDENHALL_SELECTION_POPUP_ROW_WIDTH
						- (SELECTION_POPUP_ITEM_TEXT_SPACING
								+ fltTextWidth)),
								(MILDENHALL_SELECTION_POPUP_ROW_HEIGHT
										- clutter_actor_get_height(
												pSelectionPopupItem->pRightField)) / 2);
	}
	clutter_actor_show(pSelectionPopupItem->pRightField);

}


/****************************************************************************
 * Function : v_selection_popup_item_set_item_type
 * Description: called When the Item type is specified via set property
 * 				Creation of Item specific view is handled.
 * Parameters: MildenhallSelectionPopupItem *,gint64
 * Return value: void
 *
 ***************************************************************************/

static void v_selection_popup_item_set_item_type(
		MildenhallSelectionPopupItem *pSelectionPopupItem, enMildenhallSelectionPopupItemType inItemType)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);

	if (TYPE_FIRST == pSelectionPopupItem->inItemType)
	{
		g_warning(	"Specify Proper  MILDENHALL_SELECTION_POPUP_ROLLER_ITEM Type To be created\n");
		return;
	}

	if(NULL != pSelectionPopupItem->pRightField)
	{
		clutter_actor_destroy(CLUTTER_ACTOR(pSelectionPopupItem->pRightField));
		pSelectionPopupItem->pRightField = NULL ;
	}

	if (TYPE_ICON_LABEL_LABEL == pSelectionPopupItem->inItemType)
	{
		if (NULL == pSelectionPopupItem->pRightField)
		{
			/* based on Item type label has to be added to right field */
			v_selection_popup_item_create_right_text(pSelectionPopupItem);
		}

	}

	if (TYPE_ICON_LABEL_RADIO == pSelectionPopupItem->inItemType)
	{
		if (NULL == pSelectionPopupItem->pRightField)
		{
			/* create new Radio button */
			v_mildenhall_selection_popup_item_create_radio_button(pSelectionPopupItem);
		}
	}

}

/****************************************************************************
 * Function : mildenhall_selection_popup_item_row_pressed_cb
 * Description: callabck on "button-press-event".If button press event is
 * 			observed change from pressed state image to normal state image
 * Parameters: ClutterActor *,ClutterButtonEvent *, gpointer
 * Return value: void
 ***************************************************************************/

static gboolean mildenhall_selection_popup_item_row_pressed_cb(ClutterActor *pActor,
		ClutterButtonEvent *pEvent, gpointer pUserData)
{

	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);

	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pActor);

	if (NULL != pSelectionPopupItem->pItemTexture)
	{
		/* get the parent class of the Item Type i.e, Roller */
		ClutterActor *pItemParentContainer = clutter_actor_get_parent(
				CLUTTER_ACTOR(pActor));
		if (NULL != pItemParentContainer
				&& FALSE == pSelectionPopupItem->bSignalConnected)
		{
			/* connect to "scroll-started" of Roller in order to change the
			 * pressed state to normal state on scroll detection */
			g_signal_connect(pItemParentContainer, "scroll-started",
					G_CALLBACK(v_mildenhall_selection_popup_item_scrolled_cb), pActor);
			pSelectionPopupItem->bSignalConnected = TRUE;
		}
		/* Load the Pressed image background */

		thornbury_ui_texture_set_from_file(pSelectionPopupItem->pItemTexture,
				pSelectionPopupItem->pPressedImagePath, 0, 0, FALSE, FALSE);

		pSelectionPopupItem->bPressedState = TRUE;

	}

	return FALSE;
}

/****************************************************************************
 * Function : mildenhall_selection_popup_item_row_released_cb
 * Description: callabck on "button-press-event". If button press event
 * 				 is observed change from normal state image to pressed state image
 * Parameters: ClutterActor *,ClutterButtonEvent *, gpointer
 * Return value: void
 ***************************************************************************/

static gboolean mildenhall_selection_popup_item_row_released_cb(ClutterActor *pActor,
		ClutterButtonEvent *pEvent, gpointer pUserData)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);
	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pActor);

	if (NULL != pSelectionPopupItem->pItemTexture)
	{
		/* Load the Pressed image background */

		thornbury_ui_texture_set_from_file(pSelectionPopupItem->pItemTexture,
				pSelectionPopupItem->pNormalImagePath, 0, 0, FALSE, FALSE);
		pSelectionPopupItem->bPressedState = FALSE;
	}
	if (TYPE_ICON_LABEL_RADIO == pSelectionPopupItem->inItemType)
	{
		/* change the state of the Radio button to true */
		mildenhall_radio_button_set_current_state(
				MILDENHALL_RADIO_BUTTON(pSelectionPopupItem->pRightField), TRUE);
	}
	return FALSE;
}


/****************************************************************************
 * Function : mildenhall_selection_popup_item_touch_event_cb
 * Description: callabck on "touch-event".If button press event is
 * 			observed change from pressed state image to normal state image
 * Parameters: ClutterActor *,ClutterButtonEvent *, gpointer
 * Return value: void
 ***************************************************************************/

static gboolean mildenhall_selection_popup_item_touch_event_cb(ClutterActor *pActor,
		ClutterButtonEvent *pEvent, gpointer pUserData)
{
	switch (pEvent->type)
	{
		case CLUTTER_BUTTON_PRESS:
		case CLUTTER_TOUCH_BEGIN:
			mildenhall_selection_popup_item_row_pressed_cb(pActor, pEvent, pUserData);
		break;

		case CLUTTER_BUTTON_RELEASE:
		case CLUTTER_TOUCH_END:
			mildenhall_selection_popup_item_row_released_cb(pActor, pEvent, pUserData);
		break;

		default:
			return FALSE;
	}
	return FALSE;
}





/****************************************************************************
 * Function : v_mildenhall_selection_popup_item_scrolled_cb
 * Description: callabck on scroll detected. If button press event is
 * 				observed and scroll is started change from pressed state
 * 				 image to normal state image
 * Parameters: ClutterActor *, gpointer
 * Return value: void
 ***************************************************************************/

static void v_mildenhall_selection_popup_item_scrolled_cb(ClutterActor *actor,
		gpointer pUserData)
{
	MILDENHALL_SELECTION_POPUP_ITEM_PRINT(
			"MILDENHALL_SELECTION_POPUP_ITEM: %s\n", __FUNCTION__);
	if (!MILDENHALL_IS_SELECTION_POPUP_ITEM(pUserData))
	{
		g_warning("invalid MILDENHALL_SELECTION_POPUP_ROLLER_ITEM object\n");
		return;
	}
	MildenhallSelectionPopupItem *pSelectionPopupItem =
			MILDENHALL_SELECTION_POPUP_ITEM(pUserData);

	ClutterActor *pItemParentContainer = clutter_actor_get_parent(
			CLUTTER_ACTOR(pUserData));
	if (NULL != pItemParentContainer
			&& TRUE == pSelectionPopupItem->bSignalConnected)
	{
		g_signal_handlers_disconnect_by_func(pItemParentContainer,
				v_mildenhall_selection_popup_item_scrolled_cb, pUserData);
		pSelectionPopupItem->bSignalConnected = FALSE;
	}

	if (NULL != pSelectionPopupItem->pItemTexture
			&& TRUE == pSelectionPopupItem->bPressedState)
	{
		thornbury_ui_texture_set_from_file(pSelectionPopupItem->pItemTexture,
				pSelectionPopupItem->pNormalImagePath, 0, 0, FALSE, FALSE);
		pSelectionPopupItem->bPressedState = FALSE;
	}
}

/*** END OF FILE *************************************************************/

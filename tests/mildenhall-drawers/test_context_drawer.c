/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-drawer-widget.c */

#include "mildenhall-internal.h"

#include "mildenhall_context_drawer.h"
#include "mildenhall_views_drawer.h"
#include "mildenhall_navi_drawer.h"

/* model columns */
enum
{
        COLUMN_NAME,
        COLUMN_ICON,
        COLUMN_TOOLTIP_TEXT,
        COLUMN_REACTIVE,
        COLUMN_LAST
};

/* creation of model */
ThornburyModel *create_reset_model()
{
        g_autofree gchar *music_icon_file = NULL;
        g_autofree gchar *artist_icon_file = NULL;
        g_autofree gchar *internet_icon_file = NULL;

        ThornburyModel *model = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_music_AC.png",
            NULL);

        artist_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_music_artists_AC.png",
            NULL);

        internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_internet_AC.png",
            NULL);

        thornbury_model_append (model, COLUMN_NAME, "MUSIC",
                                        COLUMN_ICON, music_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "MUSIC",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME, "ARTISTS",
                                        COLUMN_ICON, artist_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "ARTIST",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model,  COLUMN_NAME, "INTERNET",
                                        COLUMN_ICON, internet_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "INTERNET",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
	return model;
}

/* creation of model */
ThornburyModel *create_model()
{
        g_autofree gchar *music_icon_file = NULL;
        g_autofree gchar *artist_icon_file = NULL;
        g_autofree gchar *internet_icon_file = NULL;

        ThornburyModel *model = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);

        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_music_AC.png",
            NULL);

        artist_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_music_artists_AC.png",
            NULL);

        internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer",
            "icon_internet_AC.png",
            NULL);

        thornbury_model_append (model, COLUMN_NAME, "MUSIC",
                                        COLUMN_ICON, music_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "MUSIC",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME, "ARTISTS",
                                        COLUMN_ICON, artist_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "ARTIST",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model,  COLUMN_NAME, "INTERNET",
                                        COLUMN_ICON, internet_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "INTERNET",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME, "ALBUMS",
                                        COLUMN_ICON, music_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "ALBUMS",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);


        return model;

}

void drawer_opened(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void drawer_closed(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

	ThornburyModel *model3 = create_reset_model();
        g_object_set(CLUTTER_ACTOR(pUserData), "model", model3, NULL);

}

void tooltip_hidden(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void tooltip_shown(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}


void drawer_button_released(GObject *pActor, gchar *pName, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);
        g_print("DRAWER_WIDGET: button released = %s\n", pName);

}

int main (int argc, char **argv)
{
        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;
        //ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

        stage = clutter_stage_new ();
        //clutter_actor_set_background_color (stage, &black);
        gchar *pImage = g_build_filename (_mildenhall_get_theme_path (),
                                          "test-drawer",
                                          "background.png",
                                          NULL);
        //ClutterActor *actor = clutter_texture_new_from_file("background.png", NULL);
        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
	if(NULL != pImage)
	{
		g_free(pImage);
		pImage = NULL;
	}
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

# if 0
        /* context drawer */
        ClutterActor *pContextDrawer = mildenhall_context_drawer_new();
        ClutterActor *pNaviDrawer = mildenhall_navi_drawer_new();
        ClutterActor *pViewsDrawer = mildenhall_views_drawer_new();
# endif

		ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_CONTEXT_DRAWER, PKGDATADIR"/mildenhall_context_drawer_prop.json");
        GObject *pObject2 = NULL;
        g_object_get(itemFactory, "object", &pObject2, NULL);
        ClutterActor *pContextDrawer = CLUTTER_ACTOR(pObject2);

		ThornburyItemFactory *itemFactory1 = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_NAVI_DRAWER, PKGDATADIR"/mildenhall_navi_drawer_prop.json");
        GObject *pObject3 = NULL;
        g_object_get(itemFactory1, "object", &pObject3, NULL);
        ClutterActor *pNaviDrawer = CLUTTER_ACTOR(pObject3);

		ThornburyItemFactory *itemFactory2 = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_VIEWS_DRAWER, PKGDATADIR"/mildenhall_views_drawer_prop.json");
        GObject *pObject4 = NULL;
        g_object_get(itemFactory2, "object", &pObject4, NULL);
        ClutterActor *pViewsDrawer = CLUTTER_ACTOR(pObject4);

        ThornburyModel *model1 = create_model();
        g_object_set(pContextDrawer, "model", model1, NULL);
        clutter_actor_add_child(stage, pContextDrawer);

	g_signal_connect(pContextDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        //g_signal_connect(pContextDrawer, "drawer-close", G_CALLBACK(drawer_closed), NULL);
        g_signal_connect(pContextDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pContextDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);
        g_signal_connect(pContextDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);


        ThornburyModel *model2 = create_model();
        g_object_set(pNaviDrawer, "model", model2, NULL);
	clutter_actor_add_child(stage, pNaviDrawer);

	g_signal_connect(pNaviDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        g_signal_connect(pNaviDrawer, "drawer-close", G_CALLBACK(drawer_closed), pViewsDrawer);
        g_signal_connect(pNaviDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pNaviDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);
        g_signal_connect(pNaviDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);

	ThornburyModel *model3 = create_model();
        g_object_set(pViewsDrawer, "model", model3, NULL);
        clutter_actor_add_child(stage, pViewsDrawer);


	g_signal_connect(pViewsDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        //g_signal_connect(pViewsDrawer, "drawer-close", G_CALLBACK(drawer_closed), NULL);
        g_signal_connect(pViewsDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pViewsDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);
        g_signal_connect(pViewsDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);

	clutter_actor_show (stage);
	clutter_main();
        return 0;
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: popup_info
 * @title: PopupInfo
 * @short_description: Its an implementation for #LightwoodPopupBase.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #LightwoodPopupBase
 *
 * #PopupInfo shall be able to show text followed by 3 buttons (which shall be used to show text/interactive buttons
 * to show in a popup).
 * It supports model, using wich the applications can fill the following data
 * required for the popup.
 * 	1. MSGSTYLE - Text-Active/Text-Passive
 * 	2. MSGTEXT - Text Messgae in String
 * 	3. IMAGEICON - AppIcon/MessageIcon
 * 	4. BUTTONTEXT - Text for the button1
 * 	5. BUTTONNUM - Button number out of three buttons
 * 	6. IMAGEPATH - Image path for the Icon
 * 	7. TIMER - Timeout value to be set for the popup
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
*  How to create #PopupInfo
 * |[<!-- language="C" -->
 *
 *  ThornburyItemFactory *item_factory = NULL;
 *  gdouble timeout;
 *  ThornburyModel *model = NULL;
 *  GObject *object = NULL;
 *  PopupInfo *actor = NULL;
 *
 *  model = (ThornburyModel*) thornbury_list_model_new (COLUMN_LAST,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_STRING, NULL,
 *                                                     G_TYPE_INT, NULL, -1);
 *
 *   item_factory = thornbury_item_factory_generate_widget_with_props (
 *              POPUP_TYPE_INFO,
 *              "/usr/share/mildenhall/mildenhall_popup_prop.json");
 *
 *   g_object_get (item_factory, "object", &object, NULL);
 *   actor = POPUP_INFO (object);
 *   g_signal_connect (actor, "popup-shown", (GCallback) shown_cb, NULL);
 *   g_signal_connect (actor, "popup-hidden", (GCallback) hidden_cb, NULL);
 *   g_signal_connect (actor, "popup-action", (GCallback) action_cb, NULL);
 *
 *   thornbury_model_append (model,
 *                          MSGSTYLE, "Text-Active",
 *                          MSGTEXT, "RATE THE APP",
 *                          IMAGEICON, "AppIcon",
 *                          BUTTONTEXT, "R A T E",
 *                          BUTTONNUM, "BUTTON1",
 *                          IMAGEPATH, "/var/lib/MILDENHALL_extensions/themes/blau/icon_request_inactive.png",
 *                          TIMER, 10, -1);
 *
 *   thornbury_model_append (model, BUTTONTEXT, "N O",
 *                          BUTTONNUM, "BUTTON2",
 *                          IMAGEICON, "MsgIcon", IMAGEPATH,
 *                          "/var/lib/MILDENHALL_extensions/themes/blau/icon_request_inactive.png",
 *                          -1);
 *
 *   thornbury_model_append (model, BUTTONTEXT, "T H I R D", -1);
 *   g_object_set (actor, "model", model, NULL);
 *   clutter_actor_add_child (stage, CLUTTER_ACTOR (actor));
 *   popup_info_show (actor);
 *
 * ]|
 *
 * Example for #PopupInfo::popup-shown, #PopupInfo::popup-hidden
 * and #PopupInfo::popup-action callback implementations.
 *
 * |[<!-- language="C" -->
 *  gboolean shown_cb (PopupInfo *actor, gpointer data)
 *  {
 *    return FALSE;
 *  }
 *
 *  gboolean hidden_cb (PopupInfo *actor, gpointer data)
 *  {
 *    return FALSE;
 *  }
 *
 *  gboolean action_cb (PopupInfo *actor, gchar *button,
 *                     gpointer value, gpointer data)
 *  {
 *    return FALSE;
 *  }
 *
 *  ]|
 *
 * Since: 0.3.0
 */

#include "popup_info.h"

#include <assert.h>
#include <glib.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <canterbury/gdbus/canterbury.h>
#include <thornbury/thornbury.h>

#include "barkway-enums.h"

G_DEFINE_TYPE(PopupInfo, popup_info, LIGHTWOOD_TYPE_POPUP_BASE)

#define INFO_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), POPUP_TYPE_INFO, PopupInfoPrivate))
#define POPUP_INFO_GET (popup_get_info_type())
#define POPUP_TIMEOUT_MAX 5
#define POPUP_TIMEOUT_MIN 3
#define POPUP_MAX_TEXT_BOX_WIDTH 532
void popup_info_get_model(GObject *pObject, GValue *pValue);
void popup_info_get_popup_type(GObject *pObject, GValue *pValue);
void popup_info_set_model(GObject *pObject, const GValue *pValue);
void popup_info_set_popup_type(GObject *pObject, const GValue *pValue);
static void v_popup_info_model_row_added_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup);
static void v_popup_info_model_row_changed_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup);
static void v_popup_info_model_row_removed_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup);
void design_popup_widget(gpointer data);
static void popup_extract_model_info(gpointer data);
void extract_popup_type(gpointer data);
void free_control_bar(PopupInfo *pPopup);
void free_progress_bar(PopupInfo *pPopup);
void vChangeTexture(ClutterActor **actor, const gchar *textureFile);
static gboolean msgbox_key_touch_cb(ClutterActor* pClutterActor,
		ClutterEvent* pEvent, gpointer pUserData);
void popup_add_rating_stars(PopupInfo *pPopup, gint count);
void popup_info_add_style_to_hash(PopupInfo *pPopup, gchar *pKey,
		gpointer pValue);
void updateTimer(ClutterActor *popup, gchar* str);
void vAnimateMsgBox(PopupInfo *self);
void vAnimateHideMsgBox(PopupInfo *self);
static gboolean msgbox_key_pressed_cb(ClutterActor* pClutterActor,
		ClutterEvent* pEvent, gpointer pUserData);

gboolean msgbox_key_pressed_timed_cb(gpointer pUserData);
void popup_fill_rating_stars(PopupInfo *pPopup, gint count);
gboolean popup_rate_star_released_cb(ClutterActor* pClutterActor,
		ClutterEvent* pEvent, gpointer pUserData);
void popup_add_control_bar(gpointer data, gint count);
void popup_add_progress_bar(gpointer data, gint count);

void on_popup_animation(ClutterAnimation *anim, gpointer popup);
gboolean voice_control_update_timer(gpointer data);
static void popup_update_control_bar(PopupInfo *self, gdouble pitch_value);
static void v_popup_destroy_actor(ClutterActor *actor);
gint rateVal = 0;

PopupInfo *pPopupinfo = NULL;
typedef enum _enPopupInfoProperty enPopupInfoProperty;
typedef enum _enModelInfo enModelInfo;
typedef enum _enPopupDebugFlag enPopupDebugFlag;
typedef enum _PopupSignals PopupSignals;

enum _enPopupDebugFlag {
	MILDENHALL_POPUP_DEBUG = 1 << 0,

};


enum _enPopupInfoProperty {
	PROP_POPUPINFO_FIRST, PROP_POPUPINFO_MODEL, PROP_POPUPINFO_TYPE

};

enum _enModelInfo {
	MSGSTYLE,
	MSGTEXT,
	BUTTONNUM,
	BUTTONTEXT,
	BUTTONIMAGE,
	IMAGEICON,
	IMAGEPATH,
	TIMER,
	COLUMN_LAST

};

struct _PopupInfoPrivate {
	ThornburyModel *pModel;
	BarkwayPopupType popuptype;
	guint inCurrentRow;
	gint gesture_start_pos;
	ClutterActor* pHeadBar; // HeadBar texture.
	ClutterActor* pBetweenBar; // Between Bar texture.
	ClutterActor* pAppIcon; // Application Icon texture.
	ClutterActor* pPopupIcon; // Popup Icon texture.
	ClutterActor* pTextBG; // Text BG rectangle.
	ClutterActor* pPopUpText; // Popup Text/Text;
	ClutterActor* pPopUpText2; // Popup Text/Text;
	ClutterActor* tempText2;
	ClutterActor* pSeemlessLeft; // Seemless left texture.
	ClutterActor* pSeemlessRight; // Seemless right texture.
	ClutterActor* pButton1Texture; // Button 1 Texture
	ClutterActor* pButton2Texture; // Button 2 Texture
	ClutterActor* pButton3Texture; // Button 3 Texture
	ClutterActor* pButton1Text; // Button 1 Text
	ClutterActor* pButton2Text; // Button 2 Text
	ClutterActor* pButton3Text; // Button 3 Text
	ClutterActor* fillBarRect; //for recording popup
	ClutterActor* rectEntry; //for voicecontrol popup

//   ClutterEffectTemplate* pPopupEffect;
	ClutterAnimation* pPopupEffect;
	GHashTable *pPrivHash;
	GList *ratingActorList;
	GTimer *timer;
	gdouble model_time;
	guint time_out;
	gboolean show_start;
	gboolean hide_start;
	gboolean animate_show;
	ClutterActor* control_bar;
	CanterburyAudioMgr *audio_mgr_proxy;

};

/* debug flag setting */
guint mildenhall_popup_debug_flags = 0;

static const GDebugKey mildenhall_popup_debug_keys[] = {
		{ "mildenhall-popup", MILDENHALL_POPUP_DEBUG } };

#define MILDENHALL_POPUP_HAS_DEBUG               ((mildenhall_popup_debug_flags ) & 1)
#define MILDENHALL_POPUP_PRINT( a ...) \
        if (G_LIKELY (MILDENHALL_POPUP_HAS_DEBUG )) \
        {                               \
                g_print(a);           \
        }

static GType popup_get_info_type(void) {
	/* argument must point to a static 0-initialized variable,
	 * that will be set to a value other than 0 at the end of
	 * the initialization section.
	 */
	static volatile gsize gEnumTypeVolatile = 0;

	/* swipe direction as enum initialization section */
	if (g_once_init_enter(&gEnumTypeVolatile)) {
		/* A structure which contains a single flags value, its name, and its nickname. */
		static const GEnumValue arValues[] = { { BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR,
				"INFO_OR_ERROR", "errorinfo" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT,
				"CONFIRM_OPTION_ONE_WITH_TEXT", "onebutton" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_TEXT,
				"CONFIRM_OPTION_TWO_WITH_TEXT", "twobutton" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_TEXT,
				"CONFIRM_OPTION_THREE_WITH_TEXT", "threebutton" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_IMAGE,
				"CONFIRM_OPTION_ONE_WITH_IMAGE", "onebuttonimage" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_IMAGE,
				"CONFIRM_OPTION_TWO_WITH_IMAGE", "twobuttonimage" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_IMAGE,
				"CONFIRM_OPTION_THREE_WITH_IMAGE", "threebuttonimage" }, {
				BARKWAY_POPUP_TYPE_ENUM_CONFIRM_WITH_RATING, "CONFIRM_WITH_RATING",
				"rating" }, { BARKWAY_POPUP_TYPE_ENUM_WITH_RECORDING_STATUS,
				"WITH_RECORDING_STATUS", "recording" }, {
				BARKWAY_POPUP_TYPE_ENUM_WITH_VOICECONTROL, "WITH_VOICECONTROL",
				"voicecontrol" }, { BARKWAY_POPUP_TYPE_ENUM_WITH_READ_AND_CONFIRM,
				"READ_AND_CONFIRM", "readandconfirm" }, {
				BARKWAY_POPUP_TYPE_ENUM_FOR_IM_WITH_WEBCAM, "IM_WITH_WEBCAM",
				"imwebcams" }, { BARKWAY_POPUP_TYPE_ENUM_FOR_PHONE_WITH_TIME,
				"PHONE_WITH_TIME", "phonewithtime" }, { 0, NULL, NULL } };
		GType gEnumType;

		/* Registers a new static flags type with the name name. */
		gEnumType = g_enum_register_static(
				g_intern_static_string("BarkwayPopupType"), arValues);

		/* In combination with g_once_init_leave() and the unique address value_location,
		 * it can be ensured that an initialization section will be executed only once
		 * during a program's life time, and that concurrent threads are blocked
		 * until initialization completed.
		 */
		g_once_init_leave(&gEnumTypeVolatile, gEnumType);
	}

	return gEnumTypeVolatile;
}

static gboolean popup_voice_pitch_clb(GObject *object, gdouble pitch_value,
		gpointer data) {

	PopupInfo *self = POPUP_INFO(data);
	if (self->priv->popuptype == BARKWAY_POPUP_TYPE_ENUM_WITH_VOICECONTROL) {
		popup_update_control_bar(self, pitch_value);
	}
	return FALSE;
}

static void popup_audio_mgr_proxy_clb(GObject *source_object, GAsyncResult *res,
		gpointer user_data) {
	GError *error;
	PopupInfo *self = POPUP_INFO(user_data);
	MILDENHALL_POPUP_PRINT(" popup_audio_mgr_proxy_clb \n");

	self->priv->audio_mgr_proxy = canterbury_audio_mgr_proxy_new_finish(
			res, &error);

	if (self->priv->audio_mgr_proxy == NULL) {
		g_printerr("error %s \n", g_dbus_error_get_remote_error(error));
		return;
	}

	g_signal_connect(self->priv->audio_mgr_proxy, "voice-pitch",
			G_CALLBACK(popup_voice_pitch_clb), user_data);

}

static void on_popup_audio_mgr_name_appeared(GDBusConnection *connection,
		const gchar *name, const gchar *name_owner, gpointer user_data) {

	/* Asynchronously creates a proxy for the App manager D-Bus interface */
	canterbury_audio_mgr_proxy_new(connection, G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury", "/org/apertis/Canterbury/AudioMgr", NULL,
			popup_audio_mgr_proxy_clb, user_data);
}

static void on_popup_audio_mgr_name_vanished(GDBusConnection *connection,
		const gchar *name, gpointer user_data) {
	PopupInfo *self = POPUP_INFO(user_data);
	if (NULL != self->priv->audio_mgr_proxy)
		g_object_unref(self->priv->audio_mgr_proxy);
}

static void v_popup_info_model_row_added_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup) {
	//popup_extract_model_info(pPopup);
	//extract_popup_type(pPopup);
}
static void v_popup_info_model_row_changed_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup) {
	//popup_extract_model_info(pPopup);
	//extract_popup_type(pPopup);

}
static void v_popup_info_model_row_removed_cb(ThornburyModel *pModel,
		ThornburyModelIter *pIter, PopupInfo *pPopup) {
	//popup_extract_model_info(pPopup);
	//extract_popup_type(pPopup);
}

void popup_info_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData) {
	gchar *pStyleKey = g_strdup(pKey);
	PopupInfo *self = POPUP_INFO(pUserData);
	if (g_strcmp0(pStyleKey, "popup-top-bar-bmp") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-head-sep-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-head-sep-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-head-sep-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-head-sep-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-colour") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-textbg-opac") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-text-colourenable") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-text-colourdisable") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-text-font") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-buttontext-font") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemless-normal-bmp") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessleft-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessleft-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessleft-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessleft-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessleft-opac") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessright-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessright-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessright-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-seemlessright-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-bet-bar-bmp") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-bet-bar-sep-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-bet-bar-sep-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-bet-bar-sep-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-bet-bar-sep-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-appicon-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-appicon-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-icon-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-icon-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-popupicon-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-popupicon-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-singlebutton-one-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-singlebutton-one-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-singlebutton-one-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-singlebutton-one-height") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-doublebutton-one-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

	else if (g_strcmp0(pStyleKey, "popup-info-doublebutton-two-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-doublebutton-two-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-doublebutton-two-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-one-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-two-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-two-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-two-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-three-x") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-three-y") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-tripplebutton-three-width") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

	else if (g_strcmp0(pStyleKey, "popup-info-buttoninfo-norm-bmp") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);
	else if (g_strcmp0(pStyleKey, "popup-info-buttoninfo-press-bmp") == 0)
		popup_info_add_style_to_hash(self, pStyleKey, pValue);

}

void popup_info_add_style_to_hash(PopupInfo *pPopup, gchar *pKey,
		gpointer pValue) {
	if (!POPUP_IS_INFO(pPopup)) {
		g_warning("invalid  popup object\n");
		return;
	}

	if (NULL != pKey || NULL != pValue) {

		/* maintain key and value pair for style name and its value */
		if (G_VALUE_HOLDS_INT64(pValue))
			g_hash_table_insert(pPopup->priv->pPrivHash, pKey,
					g_strdup_printf ("%" G_GINT64_FORMAT, g_value_get_int64 (pValue)));
		else if (G_VALUE_HOLDS_STRING(pValue))
			g_hash_table_insert (pPopup->priv->pPrivHash,
			                     pKey, g_value_dup_string (pValue));

	}

}

void popup_info_get_model(GObject *pObject, GValue *pValue) {
        PopupInfoPrivate *priv = INFO_PRIVATE (pObject);
	if (!POPUP_IS_INFO(pObject)) {
		g_warning("invalid popupinfo object\n");
		return;
	}

	if (NULL != priv) {
		g_value_set_object(pValue, priv->pModel);
	}
}

void popup_info_get_popup_type(GObject *pObject, GValue *pValue) {
        PopupInfoPrivate *priv = INFO_PRIVATE (pObject);
	if (!POPUP_IS_INFO(pObject)) {
		g_warning("invalid popupinfo object\n");
		return;
	}

	if (NULL != priv) {
		g_value_set_enum(pValue, priv->popuptype);
	}
}

void popup_info_set_model(GObject *pObject, const GValue *pValue) {

        PopupInfoPrivate *priv = INFO_PRIVATE (pObject);
        ThornburyModel *pModel = NULL;
	if (!POPUP_IS_INFO(pObject)) {
		g_warning("invalid popupinfo object\n");
		return;
	}

	if (NULL != priv->pModel) {
		g_signal_handlers_disconnect_by_func(priv->pModel,
				G_CALLBACK(v_popup_info_model_row_added_cb), pObject);
		g_signal_handlers_disconnect_by_func(priv->pModel,
				G_CALLBACK(v_popup_info_model_row_changed_cb), pObject);
		g_signal_handlers_disconnect_by_func(priv->pModel,
				G_CALLBACK(v_popup_info_model_row_removed_cb), pObject);
		g_object_unref(priv->pModel);

		priv->pModel = NULL;
	}
	pModel = g_value_get_object (pValue);
	if (pModel != NULL) {
		g_return_if_fail(G_IS_OBJECT(pModel));

		priv->pModel = g_object_ref(pModel);

		g_signal_connect(priv->pModel, "row-added",
				G_CALLBACK(v_popup_info_model_row_added_cb), pObject);

		g_signal_connect(priv->pModel, "row-changed",
				G_CALLBACK(v_popup_info_model_row_changed_cb), pObject);

		g_signal_connect(priv->pModel, "row-removed",
				G_CALLBACK(v_popup_info_model_row_removed_cb), pObject);

	}

	/*   TODO Update the popup widget */
	popup_extract_model_info(pObject);

	g_object_notify(pObject, "model");
}

void popup_info_set_popup_type(GObject *pObject, const GValue *pValue) {
        PopupInfoPrivate *priv = INFO_PRIVATE (pObject);
	if (!POPUP_IS_INFO(pObject)) {
		g_warning("invalid popupinfo object\n");
		return;
	}

	if (NULL != priv) {
		BarkwayPopupType type = g_value_get_enum(pValue);
		priv->popuptype = type;

		extract_popup_type(pObject);
		g_object_notify(pObject, "popupinfo-type");
	}
}

static void popup_info_get_property(GObject *object, guint property_id,
		GValue *value, GParamSpec *pspec) {
	switch (property_id) {
	case PROP_POPUPINFO_MODEL:
		popup_info_get_model(object, value);
		break;
	case PROP_POPUPINFO_TYPE:
		popup_info_get_popup_type(object, value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
	}
}

static void popup_info_set_property(GObject *object, guint property_id,
		const GValue *value, GParamSpec *pspec) {
	switch (property_id) {
	case PROP_POPUPINFO_MODEL:
		popup_info_set_model(object, value);
		break;
	case PROP_POPUPINFO_TYPE:
		popup_info_set_popup_type(object, value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
	}
}

static void popup_info_dispose(GObject *object) {
	PopupInfo *self = POPUP_INFO(object);
	if (self->priv->pPrivHash) {
		g_hash_table_destroy(self->priv->pPrivHash);
		self->priv->pPrivHash = NULL;
	}
	G_OBJECT_CLASS(popup_info_parent_class)->dispose(object);
}

static void popup_info_finalize(GObject *object) {
	PopupInfo *self = POPUP_INFO(object);
	if (self->priv->pPrivHash) {
		g_hash_table_destroy(self->priv->pPrivHash);
		self->priv->pPrivHash = NULL;
	}
	G_OBJECT_CLASS(popup_info_parent_class)->finalize(object);
}

static void popup_info_class_init(PopupInfoClass *klass) {
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GParamSpec *pspec = NULL;
	g_type_class_add_private(klass, sizeof(PopupInfoPrivate));

	object_class->get_property = popup_info_get_property;
	object_class->set_property = popup_info_set_property;
	object_class->dispose = popup_info_dispose;
	object_class->finalize = popup_info_finalize;

	/**
	 * PopupInfo:model:
	 *
	 * model data for popup widget
	 */
	pspec = g_param_spec_object("model", "Model",
			"Model information of popup widget", G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_POPUPINFO_MODEL, pspec);

	/**
	 * PopupInfo:popupinfo-type:
	 *
	 * popup info type of popup widget
	 * DEFAULT:BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR
	 */
	pspec = g_param_spec_enum("popupinfo-type", "PopupType",
			"Popup type of the widget", POPUP_INFO_GET,
			BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_POPUPINFO_TYPE, pspec);

}

static void popup_info_init(PopupInfo *self) {

	GHashTable *style_table = NULL;
        const char *pEnvString;

	self->priv = INFO_PRIVATE (self);
	self->priv->pModel = NULL;
	self->priv->popuptype = BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR;
	self->priv->ratingActorList = NULL;
	self->priv->pHeadBar = NULL; // HeadBar texture.
	self->priv->pBetweenBar = NULL; // Between Bar texture.
	self->priv->pAppIcon = NULL; // Application Icon texture.
	self->priv->pPopupIcon = NULL; // Popup Icon texture.
	self->priv->pTextBG = NULL; // Text BG rectangle.
	self->priv->pPopUpText = NULL; // Popup Text/Text;
	self->priv->pPopUpText2 = NULL; // Popup Text/Text;
	self->priv->tempText2 = NULL;
	self->priv->pSeemlessLeft = NULL; // Seemless left texture.
	self->priv->pSeemlessRight = NULL; // Seemless right texture.
	self->priv->pButton1Texture = NULL; // Button 1 Texture
	self->priv->pButton2Texture = NULL; // Button 2 Texture
	self->priv->pButton3Texture = NULL; // Button 3 Texture
	self->priv->pButton1Text = NULL; // Button 1 Text
	self->priv->pButton2Text = NULL; // Button 2 Text
	self->priv->pButton3Text = NULL; // Button 3 Text
	self->priv->fillBarRect = NULL; //for recording popup
	self->priv->rectEntry = NULL; //for voicecontrol popup

	//   ClutterEffectTemplate* pPopupEffect;
	self->priv->pPopupEffect = NULL;
	self->priv->pPrivHash = NULL;
	self->priv->timer = g_timer_new();
	self->priv->show_start = FALSE;
	self->priv->hide_start = FALSE;

	g_bus_watch_name(G_BUS_TYPE_SESSION, "org.apertis.Canterbury",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START, on_popup_audio_mgr_name_appeared,
			on_popup_audio_mgr_name_vanished, self, NULL);

	/* check for env to enable traces */
	pEnvString = g_getenv("MILDENHALL_POPUP_DEBUG");
	if (pEnvString != NULL) {
		mildenhall_popup_debug_flags = g_parse_debug_string(pEnvString,
				mildenhall_popup_debug_keys, G_N_ELEMENTS(mildenhall_popup_debug_keys));

	}

	g_signal_connect (CLUTTER_ACTOR (self),
	                  "transitions-completed", G_CALLBACK(on_popup_animation),
	                  self);

	self->priv->pPrivHash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

	/* get the hash table for style properties */
	style_table = thornbury_style_set (PKGDATADIR "/mildenhall_popup_style.json");

	if (style_table != NULL)
	  {
	    GHashTableIter iter;
	    gpointer key, value;

	    g_hash_table_iter_init (&iter, style_table);

	    while (g_hash_table_iter_next (&iter, &key, &value))
	      {
	        GHashTable *table = value;

	        if (table == NULL)
	          continue;

	        g_hash_table_foreach (table, popup_info_parse_style, self);
	      }
	  }

	/* free the style hash */
	thornbury_style_free (style_table);

	pPopupinfo = self;
	design_popup_widget(self);

}

static void v_popup_widget_create_top_bar(PopupInfo *self)
{
	gchar* top_bar_bmp = g_strconcat(POPUP_PATH,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-top-bar-bmp"), NULL);
        gint64 sep_x, sep_y, sep_width, sep_height;


		self->priv->pHeadBar = thornbury_ui_texture_create_new(top_bar_bmp, 0, 0, FALSE,
				FALSE);
		g_free (top_bar_bmp);
		sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-head-sep-x"),
			"%" G_GINT64_FORMAT,
			&sep_x);
		sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-head-sep-y"),
			"%" G_GINT64_FORMAT,
			&sep_y);
		sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-head-sep-width"),
			"%" G_GINT64_FORMAT,
			&sep_width);
		sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-head-sep-height"),
			"%" G_GINT64_FORMAT,
			&sep_height);
		clutter_actor_set_position(self->priv->pHeadBar, sep_x, sep_y);

		clutter_actor_set_size(self->priv->pHeadBar, sep_width, sep_height);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pHeadBar);

}

static void v_popup_widget_create_text_bg(PopupInfo *self)
{
	// Set the colour parameter of the rectangle
		ClutterColor textbgcolor;
		gint64 textbg_x, textbg_y, textbg_width, textbg_height;
		gint textbg_opac;
		clutter_color_from_string (&textbgcolor,
                                (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                "popup-info-textbg-colour"));
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-textbg-x"),
                        "%" G_GINT64_FORMAT,
                        &textbg_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-textbg-y"),
                        "%" G_GINT64_FORMAT,
                        &textbg_y);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-textbg-width"),
                        "%" G_GINT64_FORMAT,
                        &textbg_width);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-textbg-height"),
                        "%" G_GINT64_FORMAT,
                        &textbg_height);
		sscanf(
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-textbg-opac"), "%d", &textbg_opac);

		// Create the text background.
		self->priv->pTextBG = clutter_actor_new();
		clutter_actor_set_background_color(self->priv->pTextBG, &textbgcolor);
		clutter_actor_set_position(self->priv->pTextBG, textbg_x, textbg_y);

		clutter_actor_set_size(self->priv->pTextBG, textbg_width, textbg_height);

		clutter_actor_set_opacity(self->priv->pTextBG, textbg_opac);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pTextBG);

}

static void v_popup_widget_create_seemless_rectangles(PopupInfo *self)
{
	gchar* seemless_norm_bmp = g_strconcat(POPUP_PATH,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-seemless-normal-bmp"), NULL);
		gint64 seemless_left_x, seemless_left_y, seemless_left_width,
				seemless_left_height;
		gint seemless_left_opac;
		gint64 seemless_right_x, seemless_right_y, seemless_right_width,
				seemless_right_height;
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessleft-x"),
                        "%" G_GINT64_FORMAT,
                        &seemless_left_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessleft-y"),
                        "%" G_GINT64_FORMAT,
                        &seemless_left_y);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessleft-width"),
                        "%" G_GINT64_FORMAT,
                        &seemless_left_width);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessleft-height"),
                        "%" G_GINT64_FORMAT,
                        &seemless_left_height);
                sscanf(
                                (gchar*) g_hash_table_lookup(self->priv->pPrivHash,
                                                "popup-info-seemlessleft-opac"), "%d", &seemless_left_opac);

                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessright-x"),
                        "%" G_GINT64_FORMAT,
                        &seemless_right_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessright-y"),
                        "%" G_GINT64_FORMAT,
                        &seemless_right_y);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessright-width"),
                        "%" G_GINT64_FORMAT,
                        &seemless_right_width);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-seemlessright-height"),
                        "%" G_GINT64_FORMAT,
                        &seemless_right_height);

                /* FIXME: s/seemless/seamless 
                 *        This typo should be fixed in resource file too. */
                g_debug ("seemless_left_x = %" G_GINT64_FORMAT, seemless_left_x);
                g_debug ("seemless_left_y = %" G_GINT64_FORMAT, seemless_left_y);
                g_debug ("seemless_left_width = %" G_GINT64_FORMAT, seemless_left_width);
                g_debug ("seemless_left_height = %" G_GINT64_FORMAT, seemless_left_height);
                g_debug ("seemless_norm_bmp = %" G_GINT64_FORMAT, seemless_norm_bmp);

		self->priv->pSeemlessLeft = thornbury_ui_texture_create_new(seemless_norm_bmp, 0, 0,
				FALSE, FALSE);

		clutter_actor_set_position(self->priv->pSeemlessLeft, seemless_left_x,
				seemless_left_y);

		clutter_actor_set_size(self->priv->pSeemlessLeft, seemless_left_width,
				seemless_left_height);
		MILDENHALL_POPUP_PRINT("\n seemless_left_opac is %d\n", seemless_left_opac);
		clutter_actor_set_opacity(self->priv->pSeemlessLeft, seemless_left_opac);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pSeemlessLeft);

		////////////////////////////////////////////////////////////////////////////

		// Create the seemless right
		self->priv->pSeemlessRight = thornbury_ui_texture_create_new(seemless_norm_bmp, 0, 0,
				FALSE, FALSE);
		g_free (seemless_norm_bmp);
		clutter_actor_set_position(self->priv->pSeemlessRight, seemless_right_x,
				seemless_right_y);

		clutter_actor_set_size(self->priv->pSeemlessRight, seemless_right_width,
				seemless_right_height);

		clutter_actor_set_opacity(self->priv->pSeemlessRight, seemless_left_opac);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pSeemlessRight);

}

static void v_popup_widget_create_between_bar(PopupInfo *self)
{
	gchar* between_bar_bmp = g_strconcat(POPUP_PATH,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-bet-bar-bmp"), NULL);
		gint64 between_bar_x, between_bar_y, between_bar_width, between_bar_height;
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-bet-bar-sep-x"),
                        "%" G_GINT64_FORMAT,
                        &between_bar_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-bet-bar-sep-y"),
                        "%" G_GINT64_FORMAT,
                        &between_bar_y);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-bet-bar-sep-width"),
                        "%" G_GINT64_FORMAT,
                        &between_bar_width);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-bet-bar-sep-height"),
                        "%" G_GINT64_FORMAT,
                        &between_bar_height);
		self->priv->pBetweenBar = thornbury_ui_texture_create_new(between_bar_bmp, 0, 0,
				FALSE, FALSE);
		g_free (between_bar_bmp);
		clutter_actor_set_position(self->priv->pBetweenBar, between_bar_x,
				between_bar_y);

		clutter_actor_set_size(self->priv->pBetweenBar, between_bar_width,
				between_bar_height);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pBetweenBar);
}



void design_popup_widget(gpointer data) {
	PopupInfo *self = POPUP_INFO(data);
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;

	gchar* popup_text_font = (gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-font");
  v_popup_widget_create_top_bar(self);
  v_popup_widget_create_text_bg(self);
  v_popup_widget_create_seemless_rectangles(self);
  v_popup_widget_create_between_bar(self);

	if (NULL != self->priv->pModel) {

		popup_extract_model_info(self);

	}

	else {
		gint64 appicon_x, appicon_y, appicon_width, appicon_height, popupicon_x,
				popupicon_y;
                ClutterColor colorEnable;
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-appicon-x"),
                        "%" G_GINT64_FORMAT,
                        &appicon_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-appicon-y"),
                        "%" G_GINT64_FORMAT,
                        &appicon_y);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-icon-width"),
                        "%" G_GINT64_FORMAT,
                        &appicon_width);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-icon-height"),
                        "%" G_GINT64_FORMAT,
                        &appicon_height);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-popupicon-x"),
                        "%" G_GINT64_FORMAT,
                        &popupicon_x);
                sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-popupicon-y"),
                        "%" G_GINT64_FORMAT,
                        &popupicon_y);
		self->priv->pAppIcon = thornbury_ui_texture_create_new(MMD_REQUEST_ICON, 0, 0,
				FALSE, FALSE);

		clutter_actor_set_position(self->priv->pAppIcon, appicon_x, appicon_y);

		clutter_actor_set_size(self->priv->pAppIcon, appicon_width,
				appicon_height);

		clutter_actor_set_opacity(self->priv->pAppIcon, 0x33);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pAppIcon);

		self->priv->pPopupIcon = thornbury_ui_texture_create_new(MMD_REQUEST_ICON, 0, 0,
				FALSE, FALSE);

		clutter_actor_set_opacity(self->priv->pPopupIcon, 0x33);

		clutter_actor_set_position(self->priv->pPopupIcon, popupicon_x,
				popupicon_y);

		clutter_actor_set_size(self->priv->pPopupIcon, appicon_width,
				appicon_height);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pPopupIcon);

		////////////////////////////////////////////////////////////////////////////
		clutter_color_from_string(&colorEnable,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-text-colourenable"));

		self->priv->pPopUpText = clutter_text_new();
		clutter_text_set_font_name(CLUTTER_TEXT(self->priv->pPopUpText),
				popup_text_font);
		clutter_text_set_use_markup(CLUTTER_TEXT(self->priv->pPopUpText), TRUE);
		clutter_text_set_markup(CLUTTER_TEXT(self->priv->pPopUpText),
				"HELLO WORLD");

		clutter_actor_set_name(CLUTTER_ACTOR(self->priv->pPopUpText),
				"Message");
			clutter_text_set_color(CLUTTER_TEXT(self->priv->pPopUpText),
					&colorEnable);

			clutter_actor_get_size(self->priv->pPopUpText, &guiWidth,
					&guiHeight);

			clutter_text_set_justify(CLUTTER_TEXT(self->priv->pPopUpText),
					TRUE);
			clutter_text_set_line_alignment(
					CLUTTER_TEXT(self->priv->pPopUpText), PANGO_ALIGN_CENTER);

			clutter_text_set_single_line_mode(
					CLUTTER_TEXT(self->priv->pPopUpText), TRUE);
			clutter_actor_set_width(CLUTTER_ACTOR(self->priv->pPopUpText), 460);
			clutter_text_set_ellipsize(CLUTTER_TEXT(self->priv->pPopUpText),
					PANGO_ELLIPSIZE_END);

			clutter_actor_set_position(self->priv->pPopUpText, 80, 40);

			// add the actor
			clutter_actor_add_child(CLUTTER_ACTOR(self),
					self->priv->pPopUpText);
	}

	extract_popup_type(self);
	clutter_actor_set_position(CLUTTER_ACTOR(self), MMD_POPUP_HIDE_X,
			(MMD_POPUP_HIDE_Y + 2));

}



static void popup_model_free_popup_text(PopupInfo *self)
{
	if (NULL != self->priv->pPopUpText
				&& CLUTTER_IS_ACTOR(self->priv->pPopUpText)) {
			clutter_actor_destroy(self->priv->pPopUpText);
			self->priv->pPopUpText = NULL;
		}
		if (NULL != self->priv->tempText2
				&& CLUTTER_IS_ACTOR(self->priv->tempText2)) {
			clutter_actor_destroy(self->priv->tempText2);
			self->priv->tempText2 = NULL;
		}
		if (NULL != self->priv->pPopUpText2
				&& CLUTTER_IS_ACTOR(self->priv->pPopUpText2)) {
			clutter_actor_destroy(self->priv->pPopUpText2);
			self->priv->pPopUpText2 = NULL;
		}

}

static void v_popup_destroy_actor(ClutterActor *actor)
{
	if ((CLUTTER_IS_ACTOR(actor)) && (NULL != actor))
	{
	clutter_actor_destroy(actor);
	actor = NULL;
	}
}

static void popup_model_extract_icon_info(PopupInfo *self,ThornburyModelIter *pIter)
{
	gint64 appicon_x, appicon_y, appicon_width, appicon_height, popupicon_x,
					popupicon_y;
        GValue Icon = {0, };
        GValue ImagePath = {0, };
        GValue Msgstyle = {0, };
        GValue Msgtext = {0, };
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-appicon-x"),
               "%" G_GINT64_FORMAT,
                &appicon_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-appicon-y"),
               "%" G_GINT64_FORMAT,
                &appicon_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-icon-width"),
               "%" G_GINT64_FORMAT,
                &appicon_width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-icon-height"),
               "%" G_GINT64_FORMAT,
                &appicon_height);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-popupicon-x"),
               "%" G_GINT64_FORMAT,
                &popupicon_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-popupicon-y"),
               "%" G_GINT64_FORMAT,
                &popupicon_y);
				thornbury_model_iter_get_value(pIter, IMAGEICON, &Icon);
				thornbury_model_iter_get_value(pIter, IMAGEPATH, &ImagePath);
				thornbury_model_iter_get_value(pIter, MSGSTYLE, &Msgstyle);
				thornbury_model_iter_get_value(pIter, MSGTEXT, &Msgtext);

				/* set the icon */
				if (!g_strcmp0(g_value_get_string(&Icon), "AppIcon")) {
					MILDENHALL_POPUP_PRINT("\n AppIcon loop \n");
					v_popup_destroy_actor(self->priv->pAppIcon);
					self->priv->pAppIcon = thornbury_ui_texture_create_new(
							g_value_get_string(&ImagePath), 0, 0, FALSE, FALSE);

					clutter_actor_set_position(self->priv->pAppIcon, appicon_x,
							appicon_y);

					clutter_actor_set_size(self->priv->pAppIcon, appicon_width,
							appicon_height);

					clutter_actor_add_child(CLUTTER_ACTOR(self),
							self->priv->pAppIcon);
				}

				if (!g_strcmp0(g_value_get_string(&Icon), "MsgIcon")) {
					MILDENHALL_POPUP_PRINT("\n MsgIcon loop \n");

					v_popup_destroy_actor(self->priv->pPopupIcon);
					self->priv->pPopupIcon = thornbury_ui_texture_create_new(
							g_value_get_string(&ImagePath), 0, 0, FALSE, FALSE);

					clutter_actor_set_position(self->priv->pPopupIcon,
							popupicon_x, popupicon_y);

					clutter_actor_set_size(self->priv->pPopupIcon,
							appicon_width, appicon_height);

					clutter_actor_add_child(CLUTTER_ACTOR(self),
							self->priv->pPopupIcon);

				}

}

static gchar * v_popup_widget_extract_first_row(PopupInfo *self,ThornburyModelIter *pIter)
{
	ClutterColor colorEnable;
	ClutterColor colorDisable;
	gchar* popup_text_font = (gchar*) g_hash_table_lookup(self->priv->pPrivHash,
			"popup-info-text-font");
	gfloat guiWidth = 0;
		gfloat guiHeight = 0;
		gint giPosX = 80;
		gint giPosY = 20;
		gchar *firstText = NULL;
		gchar *secondText = NULL;
	GValue Msgstyle = {0, };
        GValue Msgtext = {0, };
        clutter_color_from_string (&colorEnable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourenable"));
        clutter_color_from_string (&colorDisable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourdisable"));
						thornbury_model_iter_get_value(pIter, MSGSTYLE, &Msgstyle);
						thornbury_model_iter_get_value(pIter, MSGTEXT, &Msgtext);
						self->priv->pPopUpText = clutter_text_new();
											clutter_text_set_font_name(
													CLUTTER_TEXT(self->priv->pPopUpText),
													popup_text_font);
											clutter_text_set_use_markup(
													CLUTTER_TEXT(self->priv->pPopUpText), TRUE);
											clutter_text_set_markup(
													CLUTTER_TEXT(self->priv->pPopUpText),
													gettext((gchar *) g_value_get_string(&Msgtext)));

											clutter_actor_set_name(
													CLUTTER_ACTOR(self->priv->pPopUpText), "Message");
											if (!g_strcmp0("Text-Active",
													g_value_get_string(&Msgstyle)) && (CLUTTER_IS_TEXT(self->priv->pPopUpText))) {
													clutter_text_set_color(
															CLUTTER_TEXT(self->priv->pPopUpText),
															&colorEnable);
											}
											if (!g_strcmp0("Text-Passive",
																		g_value_get_string(&Msgstyle)) && (CLUTTER_IS_TEXT(self->priv->pPopUpText))) {
																		clutter_text_set_color(
																				CLUTTER_TEXT(self->priv->pPopUpText),
																				&colorDisable);
																}
											guiWidth = 0;
											guiHeight = 0;

												clutter_actor_get_size(self->priv->pPopUpText,
														&guiWidth, &guiHeight);

											giPosX = (MMD_POPUP_TXT_LINE_MID_X - (guiWidth / 2));
											giPosY = (MMD_POPUP_TXT_LINE_MID_Y - guiHeight
													+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR);

											if (guiWidth > POPUP_MAX_TEXT_BOX_WIDTH) {
                                                                                                gfloat giPosX2 = 60;
                                                                                                gfloat giPosY2 = 20;
                                                                                                gfloat width = 0, height = 0;

												gchar *mainText = (gchar*) g_value_get_string(&Msgtext);

												firstText = g_strndup(mainText,
														MMD_POPUP_TEXT_LETTER_NUM);
												secondText = g_strdup(
														mainText + MMD_POPUP_TEXT_LETTER_NUM);

												clutter_text_set_text(
														CLUTTER_TEXT(self->priv->pPopUpText),
														firstText);

												clutter_actor_set_width(self->priv->pPopUpText,
														POPUP_MAX_TEXT_BOX_WIDTH - 8);
												clutter_text_set_line_wrap(
														CLUTTER_TEXT(self->priv->pPopUpText), TRUE);
												clutter_text_set_line_wrap_mode(
														CLUTTER_TEXT(self->priv->pPopUpText),
														PANGO_WRAP_CHAR);
												clutter_text_set_line_alignment(
														CLUTTER_TEXT(self->priv->pPopUpText),
														PANGO_ALIGN_CENTER);

												self->priv->tempText2 = clutter_text_new();
												clutter_text_set_font_name(
														CLUTTER_TEXT(self->priv->tempText2),
														popup_text_font);
												clutter_text_set_use_markup(
														CLUTTER_TEXT(self->priv->tempText2), TRUE);
												clutter_text_set_markup(
														CLUTTER_TEXT(self->priv->tempText2),
														secondText);

												clutter_actor_get_size(self->priv->tempText2, &width,
														&height);
												if (width < POPUP_MAX_TEXT_BOX_WIDTH) {

													giPosX2 = ((POPUP_MAX_TEXT_BOX_WIDTH - width) / 2)
															+ 57;

												} else
													giPosX2 = 65;
												clutter_actor_set_width(self->priv->tempText2,
														POPUP_MAX_TEXT_BOX_WIDTH - 10);
												clutter_text_set_justify(
														CLUTTER_TEXT(self->priv->tempText2), TRUE);
													clutter_text_set_color(
															CLUTTER_TEXT(self->priv->tempText2),
															&colorEnable);
												clutter_text_set_line_alignment(
														CLUTTER_TEXT(self->priv->tempText2),
														PANGO_ALIGN_LEFT);
												clutter_text_set_ellipsize(
														CLUTTER_TEXT(self->priv->tempText2),
														PANGO_ELLIPSIZE_END);
												giPosX = 63;
												giPosY = 20;
												clutter_actor_set_position(self->priv->pPopUpText,
														giPosX, giPosY);

												clutter_actor_set_position(self->priv->tempText2,
														giPosX2, giPosY2 + 38.0);

												// add the actor
												clutter_actor_add_child(CLUTTER_ACTOR(self),
														self->priv->tempText2);
												clutter_actor_add_child(CLUTTER_ACTOR(self),
														self->priv->pPopUpText);

											} else {
												clutter_text_set_justify(
														CLUTTER_TEXT(self->priv->pPopUpText), TRUE);
												clutter_text_set_line_alignment(
														CLUTTER_TEXT(self->priv->pPopUpText),
														PANGO_ALIGN_CENTER);
												clutter_text_set_ellipsize(
														CLUTTER_TEXT(self->priv->pPopUpText),
														PANGO_ELLIPSIZE_END);
												clutter_actor_set_position(self->priv->pPopUpText,
														giPosX, giPosY);

												clutter_actor_add_child(CLUTTER_ACTOR(self),
														self->priv->pPopUpText);
											}
		g_free (firstText);
		return secondText;

}

static void v_popup_widget_extract_second_row(PopupInfo *self,ThornburyModelIter *pIter,gchar *secondText)
{
	ClutterColor colorEnable;
	ClutterColor colorDisable;
	gchar* popup_text_font = (gchar*) g_hash_table_lookup(self->priv->pPrivHash,
			"popup-info-text-font");

	gint giPosX = 80;
	gint giPosY = 20;

	GValue Msgstyle = {0, };
        GValue Msgtext = {0, };
        clutter_color_from_string (&colorEnable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourenable"));
        clutter_color_from_string (&colorDisable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourdisable"));
	thornbury_model_iter_get_value(pIter, MSGSTYLE, &Msgstyle);
	thornbury_model_iter_get_value(pIter, MSGTEXT, &Msgtext);
	if (g_value_get_string(&Msgtext)) {

                                                        gfloat width = 0, height = 0;
                                                        gfloat giPosX2;
							if (secondText) {
								gchar *popuptext2 = g_strconcat(secondText,
										gettext(
												(gchar *) g_value_get_string(
														&Msgtext)), NULL);

								self->priv->pPopUpText2 = clutter_text_new();
								clutter_text_set_font_name(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										popup_text_font);
								clutter_text_set_use_markup(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										TRUE);
								clutter_text_set_markup(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										popuptext2);
								g_free (popuptext2);

							} else {
								self->priv->pPopUpText2 = clutter_text_new();
								clutter_text_set_font_name(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										popup_text_font);
								clutter_text_set_use_markup(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										TRUE);
								clutter_text_set_markup(
										CLUTTER_TEXT(self->priv->pPopUpText2),
										gettext(
												(gchar *) g_value_get_string(
														&Msgtext)));
							}

							clutter_actor_set_name(
									CLUTTER_ACTOR(self->priv->pPopUpText2),
									"Message2");
							clutter_text_set_justify(
									CLUTTER_TEXT(self->priv->pPopUpText2), TRUE);
							if ((!g_strcmp0("Text-Active",
									g_value_get_string(&Msgstyle))) && (CLUTTER_IS_TEXT(self->priv->pPopUpText2)))
									clutter_text_set_color(
											CLUTTER_TEXT(self->priv->pPopUpText2),
											&colorEnable);
							if ((!g_strcmp0("Text-Passive",g_value_get_string(&Msgstyle))) && (CLUTTER_IS_TEXT(self->priv->pPopUpText2)))
									clutter_text_set_color(
											CLUTTER_TEXT(self->priv->pPopUpText2),
											&colorDisable);
							clutter_actor_get_size(self->priv->pPopUpText2, &width,
									&height);
							if (width < POPUP_MAX_TEXT_BOX_WIDTH) {

								giPosX2 = ((POPUP_MAX_TEXT_BOX_WIDTH - width) / 2)
										+ 57;
							} else
								giPosX2 = 65;
							clutter_actor_set_width(self->priv->pPopUpText2,
									POPUP_MAX_TEXT_BOX_WIDTH - 10);

							clutter_text_set_line_alignment(
									CLUTTER_TEXT(self->priv->pPopUpText2),
									PANGO_ALIGN_LEFT);

							clutter_text_set_ellipsize(
									CLUTTER_TEXT(self->priv->pPopUpText2),
									PANGO_ELLIPSIZE_END);
							giPosX = 80;
							giPosY = 20;
							clutter_actor_get_size(self->priv->pPopUpText, &width,
									&height);
							MILDENHALL_POPUP_PRINT("\n width is %f\n", width);
							if (width < POPUP_MAX_TEXT_BOX_WIDTH)
								giPosX = ((POPUP_MAX_TEXT_BOX_WIDTH - width) / 2)
										+ 57;
							clutter_actor_set_position(self->priv->pPopUpText,
									giPosX, giPosY);

							giPosY = 20;
							clutter_actor_set_position(self->priv->pPopUpText2,
									giPosX2, giPosY + 38.0);

							// add the actor
							clutter_actor_add_child(CLUTTER_ACTOR(self),
									self->priv->pPopUpText2);
							v_popup_destroy_actor(self->priv->tempText2);
						}
}

void popup_extract_model_info(gpointer data) {

	PopupInfo *self = POPUP_INFO(data);

	if (G_IS_OBJECT(self->priv->pModel)
			&& thornbury_model_get_n_rows(self->priv->pModel) > 0)
	{
		int iCurrentrow = 0;
		gchar *secondText = NULL;
		popup_model_free_popup_text(self);
		for ( ; (unsigned int) iCurrentrow < thornbury_model_get_n_rows (self->priv->pModel) ;
				iCurrentrow++) {

			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, iCurrentrow);

			if (NULL != pIter) {
				popup_model_extract_icon_info(self,pIter);
			   switch(iCurrentrow)
			   {
			   case 0:
			   {
				   secondText=v_popup_widget_extract_first_row(self,pIter);

				break;
			   }
			   case 1:
			   {
				   v_popup_widget_extract_second_row(self,pIter,secondText);
				   g_free (secondText);
				   break;
			   }
			   default:
				   break;
			   }
			}

		}
	}

}


static void v_popup_destroy_button_text(PopupInfo *self)
{
	if (self->priv->pButton1Text)
	{
		clutter_actor_destroy(self->priv->pButton1Text);
		self->priv->pButton1Text=NULL;
	}
		if (self->priv->pButton2Text)
		{
			clutter_actor_destroy(self->priv->pButton2Text);
			self->priv->pButton2Text=NULL;
		}
		if (self->priv->pButton3Text)
			{
			clutter_actor_destroy(self->priv->pButton3Text);
			self->priv->pButton3Text=NULL;
			}
}


static void v_popup_free_button_norm_img(gchar *button_norm_bmp)
{
	if (button_norm_bmp) {
			g_free(button_norm_bmp);
			button_norm_bmp = NULL;
		}
}

static void extract_popup_type_with_single_button(PopupInfo *self) {
	gint giPosButton1X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosY = 20;
	ClutterColor colorEnable;
	ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttonnum = {0, };
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");
        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_x, singlebutton_y, singlebutton_width,
                        singlebutton_height;
        clutter_color_from_string (&colorEnable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));

        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &singlebutton_width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);
	MILDENHALL_POPUP_PRINT("\n  BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR \n");

	if (((NULL != self->priv->pModel))
					&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);

			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);

		}


	v_popup_destroy_button_text(self);
	if (G_VALUE_HOLDS_STRING(&Buttontext))
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	if (CLUTTER_IS_TEXT(self->priv->pButton1Text))
		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
				&colorEnable);
	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);

	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, singlebutton_width,
			singlebutton_height);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);
	// calculate the text position
	if (CLUTTER_IS_ACTOR(self->priv->pButton1Text))
	{
		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);
	giPosButton1X = ((MMD_SINGLE_BUTTON_WIDTH / 2) - (guiWidth / 2));
	giPosY = MMD_POPUP_BUTTON1_TEXT_Y
			- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;
		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);
	// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);
}
	self->priv->pButton2Text = NULL;
	self->priv->pButton2Texture = NULL;
	self->priv->pButton3Text = NULL;
	self->priv->pButton3Texture = NULL;
	v_popup_free_button_norm_img(button_norm_bmp);

}

static void extract_popup_type_with_two_button(PopupInfo *self) {
	gint giPosButton1X;
	gint giPosButton2X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosY = 20;
	ClutterColor colorEnable;
        ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttontext2 = {0, };
        GValue Buttonnum = {0, };
        GValue Buttonnum2 = {0, };
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");
        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_height, singlebutton_x, singlebutton_y,
                        singlebutton_width, doublebutton_2x, doublebutton_2y,
                        doublebutton_1width, doublebutton_2width;
	clutter_color_from_string(&colorEnable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));

        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-x"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-y"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_1width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &singlebutton_width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);
	if (((NULL != self->priv->pModel))
						&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);

			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);

			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 1);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext2);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum2);

		}

	v_popup_destroy_button_text(self);
	if ((G_VALUE_HOLDS_STRING(&Buttontext))
			&& (NULL != g_value_get_string(&Buttontext))) {

		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	} else {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

	clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
			&colorEnable);

	if ((G_VALUE_HOLDS_STRING(&Buttontext2))
			&& (NULL != g_value_get_string(&Buttontext2))) {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext2)));
	} else {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

	clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton2Text),
			&colorDisable);

	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, doublebutton_1width,
			singlebutton_height);

	// Set the button 1 as reactive.
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);
	// calculate the text position
	if (CLUTTER_IS_ACTOR(self->priv->pButton1Text))
	{
		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);

	giPosButton1X = (MMD_POPUP_BUTTON1_TEXT_X - (guiWidth / 2));
	giPosY = MMD_POPUP_BUTTON1_TEXT_Y
			- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;
		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);

	// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);
}
	vChangeTexture(&(self->priv->pButton2Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum2))
		clutter_actor_set_name(self->priv->pButton2Texture,
				g_value_get_string(&Buttonnum2));
	clutter_actor_set_position(self->priv->pButton2Texture, doublebutton_2x,
			doublebutton_2y);

	clutter_actor_set_size(self->priv->pButton2Texture, doublebutton_2width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Texture);

	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position
	if (CLUTTER_IS_ACTOR(self->priv->pButton2Text))
	{
		clutter_actor_get_size(self->priv->pButton2Text, &guiWidth, &guiHeight);

	giPosButton2X = (MMD_POPUP_BUTTON2_TEXT_X - (guiWidth / 2));
	giPosY = MMD_POPUP_BUTTON2_TEXT_Y
			- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;
		clutter_actor_set_position(self->priv->pButton2Text, giPosButton2X,
				giPosY);

	// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Text);
	}
	self->priv->pButton3Text = NULL;
	self->priv->pButton3Texture = NULL;
	v_popup_free_button_norm_img(button_norm_bmp);

}

static void extract_popup_type_with_three_button(PopupInfo *self) {
	gint giPosButton1X;
	gint giPosButton2X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosY = 20;
	ClutterColor colorEnable;
	ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttontext2 = {0, };
        GValue Buttontext3 = {0, };
        GValue Buttonnum = {0, };
        GValue Buttonnum2 = {0, };
        GValue Buttonnum3 = {0, };
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");
        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_x, singlebutton_height, singlebutton_y,
                        tripplebutton_1width, tripplebutton_2x, tripplebutton_2y,
                        tripplebutton_2width, tripplebutton_3x, tripplebutton_3y,
                        tripplebutton_3width;
        clutter_color_from_string (&colorEnable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));

        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_1width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-two-x"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_2x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-two-y"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_2y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-two-width"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_2width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-three-x"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_3x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-three-y"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_3y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-tripplebutton-three-width"),
               "%" G_GINT64_FORMAT,
                &tripplebutton_3width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);


	if (((NULL != self->priv->pModel))
						&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);
			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 1);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext2);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum2);
			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 2);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext3);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum3);

		}

	v_popup_destroy_button_text(self);

	if ((G_VALUE_HOLDS_STRING(&Buttontext))
			&& (NULL != g_value_get_string(&Buttontext))) {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	} else {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

	if ((G_VALUE_HOLDS_STRING(&Buttontext2))
			&& (NULL != g_value_get_string(&Buttontext2))) {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext2)));
	} else {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

	if ((G_VALUE_HOLDS_STRING(&Buttontext3))
			&& (NULL != g_value_get_string(&Buttontext3))) {
		self->priv->pButton3Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext3)));
	} else {
		self->priv->pButton3Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

	clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
			&colorEnable);
	clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton2Text),
			&colorDisable);
	clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton3Text),
			&colorDisable);

	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, tripplebutton_1width,
			singlebutton_height);

	// Set the button 1 as reactive.
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);
	// calculate the text position

		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);
	giPosButton1X = (107 - (guiWidth / 2));
	giPosY = MMD_POPUP_BUTTON1_TEXT_Y
			- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;
		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);

	// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);

	vChangeTexture(&(self->priv->pButton2Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum2))
		clutter_actor_set_name(self->priv->pButton2Texture,
				g_value_get_string(&Buttonnum2));
	clutter_actor_set_position(self->priv->pButton2Texture, tripplebutton_2x,
			tripplebutton_2y);

	clutter_actor_set_size(self->priv->pButton2Texture, tripplebutton_2width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Texture);

	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position
		clutter_actor_get_size(self->priv->pButton2Text, &guiWidth, &guiHeight);

		giPosButton2X = (321 - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON2_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton2Text, giPosButton2X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Text);
	//self->priv->pButton3Texture = NULL;
	vChangeTexture(&(self->priv->pButton3Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum3))
		clutter_actor_set_name(self->priv->pButton3Texture,
				g_value_get_string(&Buttonnum3));
	clutter_actor_set_position(self->priv->pButton3Texture,
			tripplebutton_3x + 1, tripplebutton_3y);

	clutter_actor_set_size(self->priv->pButton3Texture, tripplebutton_3width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton3Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton3Texture);
	// calculate the text position

		clutter_actor_get_size(self->priv->pButton3Text, &guiWidth, &guiHeight);

		giPosButton2X = (536 - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON2_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton3Text, giPosButton2X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton3Text);
	v_popup_free_button_norm_img(button_norm_bmp);

}

static void extract_popup_type_confirm_with_rating(PopupInfo *self) {
	gint giPosButton1X;
	gint giPosButton2X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosX = 80;
	gint giPosY = 20;
	ClutterColor colorEnable;
        ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttontext2 = {0, };
        GValue Buttonnum = {0, };
        GValue Buttonnum2 = {0, };
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");
        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_height, singlebutton_x, singlebutton_y,
                        singlebutton_width, doublebutton_2x, doublebutton_2y,
                        doublebutton_1width, doublebutton_2width;
	clutter_color_from_string(&colorEnable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-x"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-y"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_1width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &singlebutton_width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);
	if (NULL != self->priv->pModel) {
		if (G_IS_OBJECT(self->priv->pModel)
				&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);

			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);

			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 1);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext2);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum2);

		}
	}
	v_popup_destroy_button_text(self);
	if (G_VALUE_HOLDS_STRING(&Buttontext)
			&& (NULL != g_value_get_string(&Buttontext))) {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	} else {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
				&colorEnable);

	if (G_VALUE_HOLDS_STRING(&Buttontext2)
			&& (NULL != g_value_get_string(&Buttontext2))) {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext2)));
	} else {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton2Text),
				&colorDisable);

	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, doublebutton_1width,
			singlebutton_height);

	// Set the button 1 as reactive.
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);
	// calculate the text position

		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);

		giPosButton1X = (MMD_POPUP_BUTTON1_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON1_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);
	vChangeTexture(&(self->priv->pButton2Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum2))
		clutter_actor_set_name(self->priv->pButton2Texture,
				g_value_get_string(&Buttonnum2));
	clutter_actor_set_position(self->priv->pButton2Texture, doublebutton_2x,
			doublebutton_2y);

	clutter_actor_set_size(self->priv->pButton2Texture, doublebutton_2width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Texture);
	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position

		clutter_actor_get_size(self->priv->pButton2Text, &guiWidth, &guiHeight);

		giPosButton2X = (MMD_POPUP_BUTTON2_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON2_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton2Text, giPosButton2X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Text);
	self->priv->pButton3Text = NULL;
	self->priv->pButton3Texture = NULL;

	giPosX = 80;
	giPosY = 20;
	clutter_text_set_single_line_mode(CLUTTER_TEXT(self->priv->pPopUpText),
			TRUE);
	clutter_actor_set_width(CLUTTER_ACTOR(self->priv->pPopUpText), 460);
	clutter_text_set_ellipsize(CLUTTER_TEXT(self->priv->pPopUpText),
			PANGO_ELLIPSIZE_END);
	clutter_actor_set_position(self->priv->pPopUpText, giPosX, giPosY);
	popup_add_rating_stars(self, 5);
	v_popup_free_button_norm_img(button_norm_bmp);

}

static void extract_popup_type_recording_popup(PopupInfo *self) {
	gint giPosButton1X;
	gint giPosButton2X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosX = 80;
	gint giPosY = 20;
	ClutterColor colorEnable;
        ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttontext2 = {0, };
        GValue Buttonnum = {0, };
        GValue Buttonnum2 = {0, };
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");

        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_height, singlebutton_x, singlebutton_y, doublebutton_2x,
                        doublebutton_2y, doublebutton_1width, doublebutton_2width;
        clutter_color_from_string (&colorEnable,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-x"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-y"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_1width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);
	if (NULL != self->priv->pModel) {
		if (G_IS_OBJECT(self->priv->pModel)
				&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);

			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);

			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 1);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext2);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum2);

		}
	}
	v_popup_destroy_button_text(self);
	if ((G_VALUE_HOLDS_STRING(&Buttontext))
			&& (NULL != g_value_get_string(&Buttontext))) {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	} else {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
				&colorEnable);

	if ((G_VALUE_HOLDS_STRING(&Buttontext2))
			&& (NULL != g_value_get_string(&Buttontext2))) {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext2)));
	} else {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}

		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton2Text),
				&colorDisable);
	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, doublebutton_1width,
			singlebutton_height);

	// Set the button 1 as reactive.
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);

	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position

		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);

		giPosButton1X = (MMD_POPUP_BUTTON1_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON1_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);


	vChangeTexture(&(self->priv->pButton2Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum2))
		clutter_actor_set_name(self->priv->pButton2Texture,
				g_value_get_string(&Buttonnum2));
	clutter_actor_set_position(self->priv->pButton2Texture, doublebutton_2x,
			doublebutton_2y);

	clutter_actor_set_size(self->priv->pButton2Texture, doublebutton_2width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Texture);
	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position
		clutter_actor_get_size(self->priv->pButton2Text, &guiWidth, &guiHeight);

		giPosButton2X = (MMD_POPUP_BUTTON2_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON2_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton2Text, giPosButton2X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Text);
	self->priv->pButton3Text = NULL;
	self->priv->pButton3Texture = NULL;
	giPosX = 80;
	giPosY = 20;
	clutter_text_set_single_line_mode(CLUTTER_TEXT(self->priv->pPopUpText),
			TRUE);
	clutter_actor_set_width(CLUTTER_ACTOR(self->priv->pPopUpText), 460);
	clutter_text_set_ellipsize(CLUTTER_TEXT(self->priv->pPopUpText),
			PANGO_ELLIPSIZE_END);
	clutter_actor_set_position(self->priv->pPopUpText, giPosX, giPosY);
	popup_add_progress_bar(self, 5);
	v_popup_free_button_norm_img(button_norm_bmp);
}

static void extract_popup_type_voice_control_popup(PopupInfo *self) {
	gint giPosButton1X;
	gint giPosButton2X;
	gfloat guiWidth = 0;
	gfloat guiHeight = 0;
	gint giPosX = 80;
	gint giPosY = 20;
	ClutterColor colorEnable;
        ClutterColor colorDisable;
        GValue Buttontext = {0, };
        GValue Buttontext2 = {0, };
        GValue Buttonnum = {0, };
        GValue Buttonnum2 = {0, };
        GValue time_model = {0, };
        gchar *str;
        gdouble time_;
        gchar* popup_button_text_font = (gchar*) g_hash_table_lookup (
                        self->priv->pPrivHash, "popup-info-buttontext-font");

        gchar* button_norm_bmp = g_strconcat (POPUP_PATH,
                        (gchar*) g_hash_table_lookup (self->priv->pPrivHash,
                                        "popup-info-buttoninfo-norm-bmp"), NULL);
        gint64 singlebutton_height, singlebutton_x, singlebutton_y, doublebutton_2x,
                        doublebutton_2y, doublebutton_1width, doublebutton_2width;
	clutter_color_from_string(&colorEnable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourenable"));
	clutter_color_from_string(&colorDisable,
			(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
					"popup-info-text-colourdisable"));
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-x"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-y"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-one-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_1width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-doublebutton-two-width"),
               "%" G_GINT64_FORMAT,
                &doublebutton_2width);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-x"),
               "%" G_GINT64_FORMAT,
                &singlebutton_x);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-y"),
               "%" G_GINT64_FORMAT,
                &singlebutton_y);
        sscanf (g_hash_table_lookup (self->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);

	if (((NULL != self->priv->pModel))
							&& thornbury_model_get_n_rows(self->priv->pModel) > 0) {
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
					self->priv->pModel, 0);

			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum);
			thornbury_model_iter_get_value(pIter, TIMER, &time_model);
			self->priv->model_time = g_value_get_int(&time_model);
			pIter = thornbury_model_get_iter_at_row(self->priv->pModel, 1);
			thornbury_model_iter_get_value(pIter, BUTTONTEXT, &Buttontext2);
			thornbury_model_iter_get_value(pIter, BUTTONNUM, &Buttonnum2);

		}

	v_popup_destroy_button_text(self);
	if ((G_VALUE_HOLDS_STRING(&Buttontext))
			&& (NULL != g_value_get_string(&Buttontext))) {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext)));
	} else {
		self->priv->pButton1Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}
		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton1Text),
				&colorEnable);

	if ((G_VALUE_HOLDS_STRING(&Buttontext2))
			&& (NULL != g_value_get_string(&Buttontext2))) {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font,
				gettext((gchar *) g_value_get_string(&Buttontext2)));
	} else {
		self->priv->pButton2Text = clutter_text_new_with_text(
				popup_button_text_font, "D E F A U L T");
	}
		clutter_text_set_color(CLUTTER_TEXT(self->priv->pButton2Text),
				&colorDisable);

	vChangeTexture(&(self->priv->pButton1Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum))
		clutter_actor_set_name(self->priv->pButton1Texture,
				g_value_get_string(&Buttonnum));
	clutter_actor_set_position(self->priv->pButton1Texture, singlebutton_x,
			singlebutton_y);

	clutter_actor_set_size(self->priv->pButton1Texture, doublebutton_1width,
			singlebutton_height);

	// Set the button 1 as reactive.
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Texture);

	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position

		clutter_actor_get_size(self->priv->pButton1Text, &guiWidth, &guiHeight);

		giPosButton1X = (MMD_POPUP_BUTTON1_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON1_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton1Text, giPosButton1X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton1Text);

	vChangeTexture(&(self->priv->pButton2Texture), button_norm_bmp);
	if (G_VALUE_HOLDS_STRING(&Buttonnum2))
		clutter_actor_set_name(self->priv->pButton2Texture,
				g_value_get_string(&Buttonnum2));
	clutter_actor_set_position(self->priv->pButton2Texture, doublebutton_2x,
			doublebutton_2y);

	clutter_actor_set_size(self->priv->pButton2Texture, doublebutton_2width,
			singlebutton_height);

	// Set the button 2 as reactive.
	clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

	// add the actor
	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Texture);

	guiWidth = 0;
	guiHeight = 0;

	// calculate the text position

		clutter_actor_get_size(self->priv->pButton2Text, &guiWidth, &guiHeight);

		giPosButton2X = (MMD_POPUP_BUTTON2_TEXT_X - (guiWidth / 2));
		giPosY = MMD_POPUP_BUTTON2_TEXT_Y
				- guiHeight+ MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR;

		clutter_actor_set_position(self->priv->pButton2Text, giPosButton2X,
				giPosY);

		// add the actor
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pButton2Text);
	self->priv->pButton3Text = NULL;
	self->priv->pButton3Texture = NULL;
	giPosX = 80;
	giPosY = 20;
	clutter_text_set_single_line_mode(CLUTTER_TEXT(self->priv->pPopUpText),
			TRUE);
	clutter_actor_set_width(CLUTTER_ACTOR(self->priv->pPopUpText), 460);
	clutter_text_set_ellipsize(CLUTTER_TEXT(self->priv->pPopUpText),
			PANGO_ELLIPSIZE_END);
	clutter_actor_set_position(self->priv->pPopUpText, giPosX, giPosY);
	popup_add_control_bar(self, 20);

	g_timer_start(self->priv->timer);

	time_ = g_timer_elapsed(self->priv->timer, NULL) + (self->priv->model_time);
	MILDENHALL_POPUP_PRINT("\n time is %lf\n", time_);
	str = g_strdup_printf(("%s — %d:%02dm"), "  ", (int) time_ / 60,
			(int) time_ % 60);
	updateTimer((gpointer) self, str);
	g_timeout_add(1000, voice_control_update_timer, (gpointer) self);
	v_popup_free_button_norm_img(button_norm_bmp);
}



static void v_popup_free_child_actors(PopupInfo *self)
{
	int count = 0;
		while ((unsigned int) count < g_list_length (self->priv->ratingActorList)) {
			clutter_actor_destroy(
					CLUTTER_ACTOR(
							g_list_nth_data(self->priv->ratingActorList, count)));
			count++;

		}
		if (self->priv->ratingActorList) {
			g_list_free(self->priv->ratingActorList);
			self->priv->ratingActorList = NULL;
		}

		free_control_bar(self);
		free_progress_bar(self);
}



void extract_popup_type(gpointer data) {
        PopupInfo *self = POPUP_INFO (data);
	MILDENHALL_POPUP_PRINT("\n extract_popup_type \n ");
	v_popup_free_child_actors(self);
	switch (self->priv->popuptype) {
	case BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR:

	{
		extract_popup_type_with_single_button(self);

	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT: {
		extract_popup_type_with_single_button(self);

	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_TEXT: {
		extract_popup_type_with_two_button(self);
	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_TEXT: {
		extract_popup_type_with_three_button(self);
	}

		break;

	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_WITH_RATING: {
		extract_popup_type_confirm_with_rating(self);
	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_WITH_RECORDING_STATUS: {
		extract_popup_type_recording_popup(self);

	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_WITH_VOICECONTROL: {
		extract_popup_type_voice_control_popup(self);
	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_WITH_READ_AND_CONFIRM:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_IM_WITH_WEBCAM:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_PHONE_WITH_TIME:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_OTHER_TYPE:
		break;
	default:
		break;

	}

}

void vChangeTexture(ClutterActor **actor, const gchar *textureFile) {

	ClutterActor *parent = NULL;
	gdouble x = 0;
	gdouble y = 0;
	gdouble width = 0;
	gboolean bFlag = FALSE;

	gint64 singlebutton_height;
        MILDENHALL_POPUP_PRINT ("\n vChangeTexture\n");

        sscanf (g_hash_table_lookup (pPopupinfo->priv->pPrivHash, "popup-info-singlebutton-one-height"),
               "%" G_GINT64_FORMAT,
                &singlebutton_height);

	if (NULL != *actor) {
		if (CLUTTER_IS_ACTOR(*actor)) {
			parent = clutter_actor_get_parent(*actor);
			x = clutter_actor_get_x(*actor);
			y = clutter_actor_get_y(*actor);
			width = clutter_actor_get_width(*actor);
		}
		bFlag = TRUE;
	}
	if (NULL != parent && (CLUTTER_IS_ACTOR(*actor))) {
			clutter_actor_remove_child(parent, *actor);
	}

	*actor = thornbury_ui_texture_create_new(textureFile, width, singlebutton_height,
			FALSE, FALSE);


	if ((TRUE == bFlag) && (CLUTTER_IS_ACTOR(*actor))) {
			clutter_actor_set_position((*actor), x, y);
	}
}

gboolean voice_control_update_timer(gpointer data) {
	PopupInfo *self = POPUP_INFO(data);

	gchar *str;
	gdouble time_;

	time_ = g_timer_elapsed(self->priv->timer, NULL) + (self->priv->model_time);

	str = g_strdup_printf(("%s — %d:%02dm"), "  ", (int) time_ / 60,
			(int) time_ % 60);
	updateTimer((gpointer) self, str);

	return TRUE;

}

/**
 * popup_info_show:
 * @data: gpointer data which is holding objects reference.
 *
 * Function which shows the popup
 */
void popup_info_show(gpointer data) {

	PopupInfo *self = POPUP_INFO(data);
	// GError *error=NULL;

	clutter_actor_show(CLUTTER_ACTOR(self));
	if (self->priv->show_start == FALSE)
		vAnimateMsgBox(self);

}

static gboolean msgbox_key_touch_cb(ClutterActor* pClutterActor,
		ClutterEvent* pEvent, gpointer pUserData) {
	if (NULL != pUserData) {
		PopupInfo *self = POPUP_INFO(pUserData);
		gchar* button_press_bmp = g_strconcat(POPUP_PATH,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-buttoninfo-press-bmp"), NULL);

		gdouble width = clutter_actor_get_width(pClutterActor);
		gdouble height = clutter_actor_get_height(pClutterActor);
		thornbury_ui_texture_set_from_file(pClutterActor, button_press_bmp, width, height,
				FALSE, FALSE);
		if (button_press_bmp) {
			g_free(button_press_bmp);
			button_press_bmp = NULL;
		}

	}
	return TRUE;
}

static gboolean msgbox_key_pressed_cb(ClutterActor* pClutterActor,
		ClutterEvent* pEvent, gpointer pUserData) {

	MILDENHALL_POPUP_PRINT("KEY release callback function\n");

	if (NULL != pUserData) {
		PopupInfo *self = POPUP_INFO(pUserData);
		gchar* button_norm_bmp = g_strconcat(POPUP_PATH,
				(gchar*) g_hash_table_lookup(self->priv->pPrivHash,
						"popup-info-buttoninfo-norm-bmp"), NULL);
                gdouble width;
                gdouble height;
                const gchar* actor_name = NULL;
                MILDENHALL_POPUP_PRINT ("\n button name is %s  \n", actor_name);

		if (NULL != self->priv->pButton1Texture)
			clutter_actor_set_reactive(self->priv->pButton1Texture, FALSE);
		if (NULL != self->priv->pButton2Texture)
			clutter_actor_set_reactive(self->priv->pButton2Texture, FALSE);
		if (NULL != self->priv->pButton3Texture)
			clutter_actor_set_reactive(self->priv->pButton3Texture, FALSE);
                actor_name = clutter_actor_get_name (pClutterActor);

		width = clutter_actor_get_width(pClutterActor);
		height = clutter_actor_get_height(pClutterActor);
		thornbury_ui_texture_set_from_file(pClutterActor, button_norm_bmp, width, height,
				FALSE, FALSE);

		if (button_norm_bmp) {
			g_free(button_norm_bmp);
			button_norm_bmp = NULL;
		}

		if (self->priv->popuptype == BARKWAY_POPUP_TYPE_ENUM_CONFIRM_WITH_RATING) {
			MILDENHALL_POPUP_PRINT("rating=%d\n", rateVal);
			g_signal_emit_by_name(self, "popup-action", actor_name,
					GINT_TO_POINTER(rateVal));
		} else if (self->priv->popuptype == BARKWAY_POPUP_TYPE_ENUM_WITH_VOICECONTROL) {
                        gdouble time_;
			g_timer_stop(self->priv->timer);
			time_ = g_timer_elapsed(self->priv->timer, NULL)
					+ (self->priv->model_time);
			g_signal_emit_by_name(self, "popup-action", actor_name,
					GINT_TO_POINTER(time_));

		} else {
			g_signal_emit_by_name(self, "popup-action", actor_name, NULL);
		}

		// hide the popup
		if (self->priv->hide_start == FALSE)
			vAnimateHideMsgBox(self);

	}

	return TRUE;
}

gboolean msgbox_key_pressed_timed_cb(gpointer pUserData) {

	if (NULL != pUserData) {
		PopupInfo *self = POPUP_INFO(pUserData);
		// GError *error=NULL;

		if (NULL != self->priv->pButton1Texture)
			clutter_actor_set_reactive(self->priv->pButton1Texture, FALSE);
		if (NULL != self->priv->pButton2Texture)
			clutter_actor_set_reactive(self->priv->pButton2Texture, FALSE);
		if (NULL != self->priv->pButton3Texture)
			clutter_actor_set_reactive(self->priv->pButton3Texture, FALSE);

		//clutter_actor_hide(self);

		// hide the popup
		if (self->priv->hide_start == FALSE)
			vAnimateHideMsgBox(self);

	} // if(NULL != pUserData)

	return FALSE;

}

void popup_add_rating_stars(PopupInfo *self, gint count) {

	GError *error = NULL;
	ClutterActor *image = NULL;
	gint i = 0;
	gint X = 200;
	gint Y = 70;

	for (; i < count; i++) {
                ClutterColor rectColor = {0x00, 0x00, 0x00, 0x00};
                gchar *name = NULL;
                ClutterActor *rect = clutter_actor_new ();

		image = thornbury_ui_texture_create_new(POPUP_PATH"rating_rate_IN.png", 0,0,FALSE,FALSE);

		if (error != NULL)
			return;

		clutter_actor_set_position(image, X, Y);
                name = g_strdup_printf ("%d", i + 1);
		clutter_actor_set_name(image, name);
		clutter_actor_set_reactive(image, TRUE);

		//ClutterColor rectColor   =  {0x98, 0xA9, 0xB3, 0xFF};
		clutter_actor_set_background_color(rect, &rectColor);
		clutter_actor_set_position(rect, X - 10, Y - 10);
		clutter_actor_set_size(rect, 48, 30);
		clutter_actor_set_reactive(rect, TRUE);
		clutter_actor_set_name(rect, name);
		if (name) {
			g_free(name);
			name = NULL;
		}
		clutter_actor_add_child(CLUTTER_ACTOR(self), image);
		clutter_actor_add_child(CLUTTER_ACTOR(self), rect);
		g_signal_connect(rect, "button-release-event",
				G_CALLBACK(popup_rate_star_released_cb), self);

		g_signal_connect(image, "button-release-event",
				G_CALLBACK(popup_rate_star_released_cb), self);
		X += 50;
		//store the created actors in list
		self->priv->ratingActorList = g_list_append(self->priv->ratingActorList,
				image);
	}

}

void popup_fill_rating_stars(PopupInfo *self, gint count) {
	// POPUP_DEBUG("func=%s,count=%d\n",__FUNCTION__, count);
	GError *error = NULL;
	gint i = 0;
	// fill stars till the user pressed star entry
	for (; i < count; i++) {
		ClutterActor *image = g_list_nth_data(self->priv->ratingActorList, i);
		if (image == NULL)
			break;

		thornbury_ui_texture_set_from_file(image, POPUP_PATH"rating_full_AC.png",0,0,FALSE,FALSE);
		if (error != NULL) {
			g_warning("error setting new image\n");
			break;
		}
	}
	// fill the remaining stars as empty
	for (; i < 5; i++) {
		ClutterActor *image = g_list_nth_data(self->priv->ratingActorList, i);
		if (image == NULL)
			break;

	thornbury_ui_texture_set_from_file (image, POPUP_PATH"rating_rate_IN.png",0,0,FALSE,FALSE);

}

}

gboolean popup_rate_star_released_cb(ClutterActor* pClutterActor,
	ClutterEvent* pEvent, gpointer pUserData) {
rateVal = 0;
if (NULL != pUserData) {
	PopupInfo *self = POPUP_INFO(pUserData);
	//MILDENHALL_POPUP_PRINT("Actor clicked is=%s\n", clutter_actor_get_name(pClutterActor));
	rateVal = atoi((gchar*) clutter_actor_get_name(pClutterActor));
	//MILDENHALL_POPUP_PRINT("Rating value=%d\n",rateVal);
	popup_fill_rating_stars(self, rateVal);
}
return TRUE;
}

void popup_add_control_bar(gpointer data, gint count) {
PopupInfo *self = POPUP_INFO(data);
ClutterColor rectColor = { 0x98, 0xA9, 0xB3, 0xCC };
gfloat Xposition = 83;
gint i = 0;
gchar* popup_text_font = (gchar*) g_hash_table_lookup(self->priv->pPrivHash,
		"popup-info-text-font");

ClutterColor entryColor = { 0xff, 0xff, 0xff, 0xff };
gint position = 0;

self->priv->rectEntry = clutter_text_new_with_text(popup_text_font, " ");
self->priv->control_bar = clutter_actor_new();
clutter_actor_set_position(self->priv->control_bar, Xposition, 78);
clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->control_bar);
clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->rectEntry);
if (CLUTTER_IS_TEXT(self->priv->rectEntry))
	clutter_text_set_color(CLUTTER_TEXT(self->priv->rectEntry), &entryColor);
clutter_actor_set_size(self->priv->rectEntry, 300, 40);
clutter_actor_set_position(self->priv->rectEntry, 388, 20);
clutter_actor_set_name(self->priv->rectEntry, "rectEntry");
clutter_actor_show(self->priv->rectEntry);
for (i = 0; i < count; i++) {
	ClutterActor *rect = clutter_actor_new();
        gchar *rectName = g_strdup_printf ("rect%d", count);
	clutter_actor_set_background_color (rect, &rectColor);
	clutter_actor_set_name(rect, rectName);
	if (rectName) {
		g_free(rectName);
		rectName = NULL;

	}
	clutter_actor_set_size(rect, 20, 10);
	clutter_actor_set_position(rect, position, 0);
	position = position + 25;
	clutter_actor_add_child(CLUTTER_ACTOR(self->priv->control_bar), rect);
	clutter_actor_set_child_at_index(CLUTTER_ACTOR(self->priv->control_bar),
			rect, i);
}

}
void free_control_bar(PopupInfo *self) {

if (self->priv->rectEntry) {
	clutter_actor_destroy(self->priv->rectEntry);
	self->priv->rectEntry = NULL;

}

if (self->priv->control_bar) {
	clutter_actor_destroy(self->priv->control_bar);
	self->priv->control_bar = NULL;
}

}

void free_progress_bar(PopupInfo *self) {
GList *list = NULL;
gint count = 0;
const gchar *name = NULL;
if (self->priv->fillBarRect) {
	clutter_actor_destroy(self->priv->fillBarRect);
	self->priv->fillBarRect = NULL;

}
list = clutter_actor_get_children (CLUTTER_ACTOR (self));
while ((unsigned int) count < g_list_length (list)) {
	ClutterActor *actor = CLUTTER_ACTOR(g_list_nth_data(list, count));
	name = clutter_actor_get_name(actor);
	if (name) {
		if (!g_strcmp0(name, "clockImage")) {
			clutter_actor_destroy(actor);
			actor = NULL;
		} else if (!g_strcmp0(name, "volumeImage")) {
			clutter_actor_destroy(actor);
			actor = NULL;
		} else if (g_str_has_prefix(name, "line")) {
			clutter_actor_destroy(actor);
			actor = NULL;
		}
	}
	count++;

}
}
void popup_add_progress_bar(gpointer data, gint count) {
PopupInfo *self = POPUP_INFO(data);
ClutterColor lineColor = { 0xFF, 0xFF, 0xFF, 0x33 };
ClutterColor rectColor = { 0x98, 0xA9, 0xB3, 0xCC };
ClutterColor fillBarColor = { 0xFF, 0xFF, 0xFF, 0xFF };
GError *error = NULL;
gint i = 0;
gfloat X = 511;

ClutterActor *line = NULL;
ClutterActor *clockImage = NULL;
ClutterActor *volumeImage = NULL;
ClutterActor *rect = clutter_actor_new ();

clockImage = thornbury_ui_texture_create_new(POPUP_PATH"icon_recent.png", 0,0,FALSE,FALSE);
if (error != NULL)
	return;
clutter_actor_set_position(clockImage, 83, 54);
clutter_actor_set_size(clockImage, 36, 36);
clutter_actor_set_name(clockImage, "clockImage");
clutter_actor_add_child(CLUTTER_ACTOR(self), clockImage);

volumeImage = thornbury_ui_texture_create_new(POPUP_PATH"icon_podcasts_castaudio_AC.png", 0,0,FALSE,FALSE);
if (error != NULL)
	return;
clutter_actor_set_position(volumeImage, 468, 54);
clutter_actor_set_size(volumeImage, 36, 36);
clutter_actor_set_name(volumeImage, "volumeImage");
clutter_actor_add_child(CLUTTER_ACTOR(self), volumeImage);

clutter_actor_set_background_color(rect, &rectColor);
clutter_actor_set_size(rect, 326, 20);
clutter_actor_set_position(rect, 123, 62);
clutter_actor_add_child(CLUTTER_ACTOR(self), rect);

self->priv->fillBarRect = clutter_actor_new();
clutter_actor_set_background_color(self->priv->fillBarRect, &fillBarColor);
clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->fillBarRect);
clutter_actor_set_size(self->priv->fillBarRect, 326, 20);
clutter_actor_set_position(self->priv->fillBarRect, 123, 62);
clutter_actor_set_clip(self->priv->fillBarRect, 0, 0, 0, 20);
clutter_actor_set_name(self->priv->fillBarRect, "fillBarRect");

for (i = 0; i < 10; i++) {
        gchar *linename = NULL;
	if (i < 5) {
		line = clutter_actor_new();
		clutter_actor_set_background_color(line, &rectColor);
	} else {
		line = clutter_actor_new();
		clutter_actor_set_background_color(line, &lineColor);
	}
	clutter_actor_set_size(line, 2.5, 22);
        linename = g_strdup_printf ("line%d", i);
	clutter_actor_set_name(line, linename);
	if (linename) {
		g_free(linename);
		linename = NULL;
	}
	clutter_actor_set_position(line, X, 62);
	X = X + 5;
	clutter_actor_add_child(CLUTTER_ACTOR(self), line);
}

}

void vAnimateMsgBox(PopupInfo *self) {

self->priv->animate_show = TRUE;
self->priv->show_start = TRUE;
clutter_actor_save_easing_state(CLUTTER_ACTOR(self));
clutter_actor_set_easing_mode(CLUTTER_ACTOR(self), CLUTTER_EASE_OUT_BOUNCE);
clutter_actor_set_easing_duration(CLUTTER_ACTOR(self), MMD_POPUP_SHOWTIME);
clutter_actor_set_y(CLUTTER_ACTOR(self),
		clutter_actor_get_y(CLUTTER_ACTOR(self))
				- clutter_actor_get_height(CLUTTER_ACTOR(self)));
clutter_actor_restore_easing_state(CLUTTER_ACTOR(self));

}

void vAnimateHideMsgBox(PopupInfo *self) {

self->priv->animate_show = FALSE;
self->priv->hide_start = TRUE;
clutter_actor_save_easing_state(CLUTTER_ACTOR(self));
clutter_actor_set_easing_mode(CLUTTER_ACTOR(self), CLUTTER_EASE_OUT_BOUNCE);
clutter_actor_set_easing_duration(CLUTTER_ACTOR(self), MMD_POPUP_SHOWTIME);
clutter_actor_set_y(CLUTTER_ACTOR(self),
		clutter_actor_get_y(CLUTTER_ACTOR(self))
				+ clutter_actor_get_height(CLUTTER_ACTOR(self)));
clutter_actor_restore_easing_state(CLUTTER_ACTOR(self));

}

static gboolean msgbox_touch_event_cb(ClutterActor *actor,ClutterEvent *event,gpointer userdata)
{
	if (event->type == CLUTTER_TOUCH_BEGIN)
		msgbox_key_touch_cb(actor,event,userdata);
	else if (event->type == CLUTTER_TOUCH_END)
		msgbox_key_pressed_cb(actor,event,userdata);

	return FALSE;
}



static void on_popup_animation_info_or_error(PopupInfo *self)
{
	gdouble timeout;
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);
			g_signal_connect(self->priv->pButton1Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton1Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton1Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);

			g_object_get(self, "timeout", &timeout, NULL);

			if (timeout > POPUP_TIMEOUT_MAX) {
				self->priv->time_out = g_timeout_add_seconds(POPUP_TIMEOUT_MAX,
						msgbox_key_pressed_timed_cb, self);

			} else if (timeout < POPUP_TIMEOUT_MIN) {
				self->priv->time_out = g_timeout_add_seconds(POPUP_TIMEOUT_MIN,
						msgbox_key_pressed_timed_cb, self);

			} else {
				MILDENHALL_POPUP_PRINT("timeout is 4 seconds \n");
				self->priv->time_out = g_timeout_add_seconds(timeout,
						msgbox_key_pressed_timed_cb, self);

			}

}

static void on_popup_animation_one_with_text(PopupInfo *self)
{
	gdouble timeout;
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);

			g_signal_connect(self->priv->pButton1Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton1Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton1Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
			g_object_get(self, "timeout", &timeout, NULL);
			if (timeout > 0)
				self->priv->time_out = g_timeout_add_seconds(timeout,
						msgbox_key_pressed_timed_cb, self);
}

static void on_popup_animation_two_with_text(PopupInfo *self)
{
	gdouble timeout;
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);
			clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);

			g_signal_connect(self->priv->pButton1Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton1Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);

			g_signal_connect(self->priv->pButton2Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton2Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton1Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
			g_signal_connect(self->priv->pButton2Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
			g_object_get(self, "timeout", &timeout, NULL);
			if (timeout > 0)
				self->priv->time_out = g_timeout_add_seconds(timeout,
						msgbox_key_pressed_timed_cb, self);
}

static void on_popup_animation_three_with_text(PopupInfo *self)
{
	gdouble timeout;
	clutter_actor_set_reactive(self->priv->pButton1Texture, TRUE);
			clutter_actor_set_reactive(self->priv->pButton2Texture, TRUE);
			clutter_actor_set_reactive(self->priv->pButton3Texture, TRUE);

			g_signal_connect(self->priv->pButton1Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton1Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton2Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton2Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton3Texture, "button-release-event",
					G_CALLBACK(msgbox_key_pressed_cb), self);
			g_signal_connect(self->priv->pButton3Texture, "button-press-event",
					G_CALLBACK(msgbox_key_touch_cb), self);
			g_signal_connect(self->priv->pButton1Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
		    g_signal_connect(self->priv->pButton2Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
		    g_signal_connect(self->priv->pButton3Texture,"touch-event",G_CALLBACK(msgbox_touch_event_cb),self);
			g_object_get(self, "timeout", &timeout, NULL);
			if (timeout > 0)
				self->priv->time_out = g_timeout_add_seconds(timeout,
						msgbox_key_pressed_timed_cb, self);
}


void on_popup_animation(ClutterAnimation *anim, gpointer popup) {

PopupInfo *self = POPUP_INFO(popup);
if (self->priv->animate_show) {
	self->priv->show_start = FALSE;


	switch (self->priv->popuptype) {
	case BARKWAY_POPUP_TYPE_ENUM_INFO_OR_ERROR: {
		on_popup_animation_info_or_error(self);

	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT: {

		on_popup_animation_one_with_text(self);

	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_TEXT: {
		on_popup_animation_two_with_text(self);

	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_TEXT: {
		on_popup_animation_three_with_text(self);

	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_WITH_RATING: {
		on_popup_animation_two_with_text(self);

	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_WITH_RECORDING_STATUS: {
		on_popup_animation_two_with_text(self);

	}
		break;

	case BARKWAY_POPUP_TYPE_ENUM_WITH_VOICECONTROL: {
		on_popup_animation_two_with_text(self);

	}
		break;
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_IMAGE:
	case BARKWAY_POPUP_TYPE_ENUM_WITH_READ_AND_CONFIRM:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_IM_WITH_WEBCAM:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_PHONE_WITH_TIME:
	case BARKWAY_POPUP_TYPE_ENUM_FOR_OTHER_TYPE:
		break;
	default:
		break;
	}
	g_signal_emit_by_name(self, "popup-shown");
} else {

	self->priv->hide_start = FALSE;
	// set the popup to hide
	clutter_actor_hide(CLUTTER_ACTOR(self));
	v_popup_free_child_actors(self);
	if (self->priv->time_out > 0)
		g_source_remove(self->priv->time_out);
	g_signal_emit_by_name(self, "popup-hidden");
}
}

void updateTimer(ClutterActor *popup, gchar* str) {

if (popup) {
	ClutterActor *actor = NULL;
	actor = clutter_container_find_child_by_name(CLUTTER_CONTAINER(popup),
			"rectEntry");

	if (actor != NULL) {
		clutter_text_set_text(CLUTTER_TEXT(actor), str);
		if (str) {
			g_free(str);
			str = NULL;
		}

	}
}
}

/**
 * popup_info_hide:
 * @data: gpointer data which is holding objects reference.
 *
 * Function which hides the popup
 */
void popup_info_hide(gpointer data) {
if (NULL != data) {
	PopupInfo *self = POPUP_INFO(data);
	if (self->priv->hide_start == FALSE)
		vAnimateHideMsgBox(self);
}

}

static void popup_update_control_bar(PopupInfo *self, gdouble pitch_value) {
ClutterColor ColourDisable = { 0x98, 0xA9, 0xB3, 0xCC };
ClutterColor ColourEnable = { 0xff, 0xff, 0xff, 0xff };

gint numRect;
gint index = 0;
ClutterActor *actor = NULL;
numRect = (gint) (20 * pitch_value);

while (index < numRect) {
	actor = clutter_actor_get_child_at_index(
			CLUTTER_ACTOR(self->priv->control_bar), index);
	if (actor) {
		clutter_actor_set_background_color(actor, NULL);
		clutter_actor_set_background_color(actor, &ColourEnable);
	}
	index++;
}

while (index <= 20) {
	actor = clutter_actor_get_child_at_index(
			CLUTTER_ACTOR(self->priv->control_bar), index);
	if (actor) {
		clutter_actor_set_background_color(actor, &ColourDisable);
	}
	index++;
}

}


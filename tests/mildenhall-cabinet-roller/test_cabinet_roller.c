/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_cabinet_roller.h"
#include "mildenhall-internal.h"

#include <thornbury/thornbury.h>

#include "mildenhall_meta_info_header.h"
#include "sample-item.h"
#include "sample-variable-item.h"

enum
{
	ROLLER_OVERLAY,
	ROLLER_ICON,
	ROLLER_TEXT,
	ROLLER_TYPE,
	ROLLER_COLUMN_LAST,
};

ThornburyModel *create_cabinet_roller_model(int n)
{
	ThornburyModel *cabinet_roller_model;
	cabinet_roller_model = (ThornburyModel*)thornbury_list_model_new(ROLLER_COLUMN_LAST, G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_INT,NULL,
			-1);

	//for(index=0;index< n;index++)
	{
		thornbury_model_append (cabinet_roller_model,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("unknownhfjhfhfhfghfg"),
				ROLLER_TYPE,MILDENHALL_CABINET_FOLDER_TYPE,
				-1);
		thornbury_model_append (cabinet_roller_model,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("whgfhfghhhhhhhhhhhhhhhhh"),
				ROLLER_TYPE,MILDENHALL_CABINET_FILE_TYPE,
				-1);
		thornbury_model_append (cabinet_roller_model,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("irghfggggggggggg"),
				ROLLER_TYPE,MILDENHALL_CABINET_FOLDER_TYPE,
				-1);
		thornbury_model_append (cabinet_roller_model,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("iajhjhofghgjhjufjklawjdioqwjknwejkfijgnfj"),
				ROLLER_TYPE,MILDENHALL_CABINET_FOLDER_TYPE,
				-1);
		thornbury_model_append (cabinet_roller_model,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("iajhjhilhjkjhlgijfbnfklbjfilbnjlgfijkvjbhfkbnfgblfkgjbjgfkn"),
				ROLLER_TYPE,MILDENHALL_CABINET_FOLDER_TYPE,
				-1);
	}


	return cabinet_roller_model;

}
static void
roller_prev_item_activated_cb(MildenhallCabinetRoller *roller, guint row, ThornburyModel *model,gpointer data)
{

#if 0
	static ThornburyModel *prevModel;
	static guint prevRow;
	if((prevModel == model) && (prevRow == row))
	{
		g_object_set(roller,"show-prev-roller",TRUE,NULL);
	}
	else
#endif
	{
		/* get active roller */
		MildenhallRollerContainer *active_roller = NULL;
		g_object_get(roller,"active-roller",&active_roller,NULL);
		ThornburyModel *activeModel = NULL;
		g_object_get(active_roller,"model",&activeModel,NULL);
		thornbury_model_append (activeModel,
				ROLLER_OVERLAY,MILDENHALL_CABINET_PICTURE_OVERLAY_PATH,
				ROLLER_ICON, NULL,
				ROLLER_TEXT,g_strdup("fgfgfgf"),
				ROLLER_TYPE,MILDENHALL_CABINET_FOLDER_TYPE,
				-1);
	}
	//prevModel = model;
	//prevRow = row;
}

static void
roller_active_item_activated_cb(MildenhallCabinetRoller *roller, guint row, ThornburyModel *model,gpointer data)
{
#if 0
	g_object_get(active_roller,"model",&activeModel,NULL);

	if((activeModel == model))
	{
#endif
	static gint count = 0;
	if(count == 5)
	{
		g_print(" show_root_roller\n");
		g_object_set(G_OBJECT(roller),"show-root-roller",TRUE,NULL);
		count = 0;
		return;
	}

		ThornburyModel *model2 = create_cabinet_roller_model(3);
		g_object_set(G_OBJECT(roller),"create-roller",model2,NULL);
	count ++;
}

int main (int argc, char **argv)
{
        g_autofree gchar *pImage = NULL;

        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;

        stage = clutter_stage_new ();

        pImage = g_build_filename (_mildenhall_get_theme_path (),
	    "test-drawer-base",
	    "background.png",
	    NULL);

        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);
#if 1
	ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_CABINET_ROLLER, PKGDATADIR"/mildenhall_cabinet_roller_prop.json");
        GObject *pObject2 = NULL;
        g_object_get(itemFactory, "object", &pObject2, NULL);
        ClutterActor *pActor = CLUTTER_ACTOR(pObject2);

	ThornburyModel *rootmodel1 = create_cabinet_roller_model(3);
	g_object_set(G_OBJECT(pObject2),"create-roller",rootmodel1,NULL);

	g_signal_connect(G_OBJECT(pActor),"cabinet-prev-item-activated",G_CALLBACK(roller_prev_item_activated_cb),NULL);
	g_signal_connect(G_OBJECT(pActor),"cabinet-active-item-activated",G_CALLBACK(roller_active_item_activated_cb),NULL);
	//g_signal_connect(G_OBJECT(pActor),"cabinet-locking_finished",G_CALLBACK(roller_locking_cb),NULL);

#if 0
	ThornburyModel *model3 = create_cabinet_roller_model(3);
	g_object_set(G_OBJECT(pObject2),"model",model3,NULL);

	ThornburyModel *model4 = create_cabinet_roller_model(3);
	g_object_set(G_OBJECT(pObject2),"model",model4,NULL);
#endif
/* show stage */
        clutter_actor_add_child(stage, pActor);
#endif
        clutter_actor_show (stage);
        clutter_main();
        return 0;

}


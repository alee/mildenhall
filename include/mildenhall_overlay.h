/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**************************************************************************************************
 * Filename          : mildenhall_overlay.h
 * Package           : Mildenhall-Base
 * Widget		     : MildenhallOverlay
 * Description       : 
 **************************************************************************************************/


#ifndef __MILDENHALL_OVERLAY_H__
#define __MILDENHALL_OVERLAY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/**********************************************************************
                     	Header Files
***********************************************************************/
#include <glib-object.h>
#include <clutter/clutter.h>
#include <string.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_OVERLAY mildenhall_overlay_get_type()

#define MILDENHALL_OVERLAY(obj) \
		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				MILDENHALL_TYPE_OVERLAY, MildenhallOverlay))

#define MILDENHALL_OVERLAY_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_CAST ((klass), \
				MILDENHALL_TYPE_OVERLAY, MildenhallOverlayClass))

#define MILDENHALL_IS_OVERLAY(obj) \
		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
				MILDENHALL_TYPE_OVERLAY))

#define MILDENHALL_IS_OVERLAY_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_TYPE ((klass), \
				MILDENHALL_TYPE_OVERLAY))

#define MILDENHALL_OVERLAY_GET_CLASS(obj) \
		(G_TYPE_INSTANCE_GET_CLASS ((obj), \
				MILDENHALL_TYPE_OVERLAY, MildenhallOverlayClass))

typedef struct _MildenhallOverlay MildenhallOverlay;
typedef struct _MildenhallOverlayClass MildenhallOverlayClass;
typedef struct _MildenhallOverlayPrivate MildenhallOverlayPrivate;

struct _MildenhallOverlay
{
	ClutterActor parent;

	MildenhallOverlayPrivate *priv;
};

struct _MildenhallOverlayClass
{
	ClutterActorClass parent_class;
};

GType mildenhall_overlay_get_type (void) G_GNUC_CONST;

MildenhallOverlay *mildenhall_overlay_new (void);

G_END_DECLS

#endif /* __MILDENHALL_OVERLAY_H__ */

/**********************************************************************
                        End of file
***********************************************************************/

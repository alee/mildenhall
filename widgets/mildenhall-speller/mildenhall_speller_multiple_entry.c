/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_speller_multiple_entry
 * @title: MildenhallSpellerMultipleEntry
 * @short_description: Several entries that can be edited on the same virtual keyboard.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #MildenhallSpeller
 *
 * #MildenhallSpellerDefaultEntry is an another text entry widget for speller where
 * several entries that can be edited on the same virtual keyboard.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * Since: 0.3.0
 */

#include "mildenhall_speller_multiple_entry.h"
#include "mildenhall_speller.h"
#include "mildenhall_text_box_entry.h"
#include "mildenhall_scroller.h"

#define MILDENHALL_MULTIPLE_SPELLER_PRINT(...) //g_print(__VA_ARGS__)
#define MILDENHALL_BLANK_RECT_WIDTH 648
#define MILDENHALL_BLANK_RECT_HEIGHT 114

G_DEFINE_TYPE (MildenhallSpellerMultipleEntry, mildenhall_speller_multiple_entry, CLUTTER_TYPE_ACTOR)

#define SPELLER_MULTIPLE_ENTRY_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, MildenhallSpellerMultipleEntryPrivate))

static void v_model_data_for_multiple_entry( MildenhallSpellerMultipleEntry *pSelf );

enum
{
	SIG_MULTIPLE_ENTRY_FIRST_SIGNAL,
	SIG_MULTIPLE_ENTRY_ACTION,
	SIG_MULTIPLE_ENTRY_LAST_SIGNAL
};

guint32 multiple_entry_action_signals[SIG_MULTIPLE_ENTRY_LAST_SIGNAL] = {0,};

struct _MildenhallSpellerMultipleEntryPrivate
{
	ThornburyModel *model;
	ClutterActor *blankRect;
	ClutterActor *activeEntry;
	GVariant *argList;
	GVariantBuilder *pVariant;
	GList *entryIdList;
	GList *EntryList;
	GList *buttonList;
	GList *buttonIdList;
	ClutterActor *SpellerGroup;
	ClutterActor *EntryGroup;
	ClutterActor *scroller;
	gboolean clearText;
	gint entryRows;
	gint cursor_focus_entry;
	gint offset;
	GHashTable *toggleList;


	ClutterActor *rightButton;
	ClutterActor *leftButton;
	ClutterActor *imageSep;
	gint inColumnId;
};

/* toggle button model columns */
enum
{
	COLUMN_TEXT,                // set text to toggle button
	COLUMN_ACTIVE_ICON_PATH,    // set active image to toggle button
	COLUMN_INACTIVE_ICON_PATH,  // set inactive image to toggle button
	COLUMN_STATE_ID,            // set stateId to toggle button
	COLUMN_LAST
};

enum _MildenhallMultipleSpellerProperties
{
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_FIRST,
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_TEXT,
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_CLEAR_TEXT,
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_MODEL,
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_FOCUS_CURSOR,
	MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_LAST
};

/****************************************************
 * Function : multiple_create_text_box_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
multiple_create_text_box_model (void)
{
	ThornburyModel *pModel = NULL;
	pModel = (ThornburyModel *)thornbury_list_model_new (1 ,G_TYPE_STRING, NULL ,-1);
	return pModel;
}

/****************************************************
 * Function : multiple_create_toggle_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
multiple_create_toggle_model (void)
{
	ThornburyModel *model = NULL;
	model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			-1);
	return model;
}

/********************************************************
 * Function : v_left_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_left_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE(pUserData);
	int inCnt =0 ;
	for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->buttonList); inCnt++)
	{
		ClutterActor *pEntryBox  = g_list_nth_data(priv->buttonList,inCnt);
		if(!g_strcmp0((gchar*)pEntryBox,(gchar*)pObject))
		{
			g_hash_table_insert(priv->toggleList,g_strdup(g_list_nth_data(priv->buttonIdList,inCnt)), g_strdup(newState));
			break;
		}
	}
}

/********************************************************
 * Function : v_right_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_right_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	//TBD
}

/********************************************************
 * Function : v_multiple_speller_row_added_cb
 * Description: callback on model row added
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultipleEntry *
 * Return value: void
 ********************************************************/
static void v_multiple_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerMultipleEntry *pSelf)
{
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
}

/********************************************************
 * Function : v_multiple_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultipleEntry *
 * Return value: void
 ********************************************************/
static void v_multiple_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallSpellerMultipleEntry *pSelf)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
        GValue value = {0, };
        gint inRowNum;
        ClutterActor *pEntryBox = NULL;
        ThornburyModel *TextModel = NULL;
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);

	priv->model = pModel;

	inRowNum = thornbury_model_iter_get_row (pIter);

	pEntryBox  = g_list_nth_data (priv->EntryList, inRowNum);
	g_object_get(pEntryBox,"model",&TextModel,NULL);

	if(-1 != priv->inColumnId)
	{
		thornbury_model_iter_get_value(pIter, priv->inColumnId, &value);
		thornbury_model_insert_value(TextModel, 1, 0, &value);
		g_value_unset (&value);
	}
}

/********************************************************
 * Function : v_multiple_speller_row_removed_cb
 * Description: callback on model row remove
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultipleEntry *
 * Return value: void
 ********************************************************/
static void v_multiple_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerMultipleEntry *pSelf)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
        gint inRemovedRow;
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);

	/* this callback gives the actual row count before removal, and not after removal of row */
	inRemovedRow = thornbury_model_iter_get_row (pIter);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && thornbury_model_get_n_rows(priv->model) == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
}

/********************************************************
 * Function : scrollup_cb
 * Description: callback on scroll up
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean scrollup_cb(ClutterActor *actor, gpointer user_data)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (user_data);
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(user_data) , FALSE);
	MILDENHALL_MULTIPLE_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
	if(priv->offset)
	{
		priv->offset--;
		clutter_actor_move_by (priv->EntryGroup,0,57.0);
	}
	return TRUE;
}

/********************************************************
 * Function : scrolldown_cb
 * Description: callback on scroll down
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean scrolldown_cb(ClutterActor *actor, gpointer user_data)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (user_data);
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(user_data) , FALSE);
	MILDENHALL_MULTIPLE_SPELLER_PRINT(" \n %s \n", __FUNCTION__);
	if(priv->offset < (priv->entryRows - 2))
	{
		priv->offset++;
		clutter_actor_move_by (priv->EntryGroup,0,-57.0);
	}
	return TRUE;
}

/********************************************************
 * Function : b_entry_press_callback
 * Description: callback on entry press
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean b_entry_press_callback(ClutterActor *pEntry, gpointer pUserData)
{
	MildenhallSpellerMultipleEntry *pSelf = MILDENHALL_SPELLER_MULTIPLE_ENTRY(pUserData);
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE(pSelf);
        gchar* id = NULL;
        MILDENHALL_MULTIPLE_SPELLER_PRINT ("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);

	priv->activeEntry = pEntry;
	id = (gchar*) g_list_nth_data (priv->entryIdList, g_list_index (priv->EntryList, priv->activeEntry));
	g_signal_emit(pSelf, multiple_entry_action_signals[SIG_MULTIPLE_ENTRY_ACTION],0,id);
	return FALSE;
}

/********************************************************
 * Function : construct_line
 * Description: function to create the line
 * Parameters: ClutterActor,gint,gint
 * Return value: ClutterActor
 ********************************************************/
static ClutterActor* construct_line(ClutterActor *EntryGroup,gint width,gint height)
{
	ClutterColor lineColor = {0xFF,0xFF,0xFF,0x33};

	ClutterActor *Line = clutter_actor_new();
	clutter_actor_set_background_color (Line,&lineColor);
	clutter_actor_add_child(EntryGroup, Line);
	clutter_actor_set_size(Line,width,height);

	return Line;
}

/********************************************************
 * Function : update_left_button_model
 * Description: function to update the button model
 * Parameters: MildenhallSpellerMultipleEntry,GVariant,gboolean,ThornburyModel,gchar
 * Return value: ClutterActor
 ********************************************************/
static void update_left_button_model(MildenhallSpellerMultipleEntry *pSelf,GVariant *gvLeftIcon,gboolean bIsText,ThornburyModel *pLeftModel,gchar *leftButtonId)
{
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE( pSelf );
	if(gvLeftIcon)
	{
		GVariantIter inCount;
		gchar *pValue = NULL;
		gchar *pKey = NULL;
		int iCnt =0;
                g_variant_iter_init (&inCount, gvLeftIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			if( TRUE == bIsText  )
			{
				g_object_set (priv->leftButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (pLeftModel, COLUMN_TEXT, g_strdup(pValue),
						COLUMN_ACTIVE_ICON_PATH, NULL,
						COLUMN_INACTIVE_ICON_PATH, NULL,
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				thornbury_model_append (pLeftModel, COLUMN_TEXT, NULL,
						COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			if(0 == iCnt)
			{
				g_hash_table_insert(priv->toggleList,leftButtonId, g_strdup(pKey));
				iCnt++;
			}
		}
		g_object_set(priv->leftButton, "state-model", pLeftModel, NULL);
	}
}

/********************************************************
 * Function : update_right_button_model
 * Description: function to update the button model
 * Parameters: MildenhallSpellerMultipleEntry,GVariant,gboolean,ThornburyModel,gchar
 * Return value: ClutterActor
 ********************************************************/
static void update_right_button_model(MildenhallSpellerMultipleEntry *pSelf,GVariant *gvRightIcon,gboolean bIsText1,ThornburyModel *pRightModel)
{
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE( pSelf );
	if(gvRightIcon)
	{
		GVariantIter inCount;
		gchar *pValue = NULL;
		gchar *pKey = NULL;
                g_variant_iter_init (&inCount, gvRightIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			if( TRUE == bIsText1  )
			{
				g_object_set (priv->rightButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (pRightModel, COLUMN_TEXT, g_strdup(pValue),
						COLUMN_ACTIVE_ICON_PATH, NULL,
						COLUMN_INACTIVE_ICON_PATH, NULL,
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				thornbury_model_append (pRightModel, COLUMN_TEXT, NULL,
						COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
		}
		g_object_set(priv->rightButton, "state-model", pRightModel, NULL);
	}
}

/********************************************************
 * Function : construct_model_data
 * Description: function to updte the button model
 * Parameters: MildenhallSpellerMultipleEntry,ClutterActor,ClutterActor,guint
 * Return value: void
 ********************************************************/
static void construct_model_data(MildenhallSpellerMultipleEntry *pSelf,ClutterActor *textBoxEntry,ClutterActor *EntryGroup,guint inCnt)
{
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE( pSelf );

	ClutterActor *leftLine  = construct_line(EntryGroup,1,54);
	ClutterActor *rightLine = construct_line(EntryGroup,1,54);
	ClutterActor *seperator = construct_line(EntryGroup,590,1);

	ThornburyModel *pTextBoxModel = multiple_create_text_box_model();
	ThornburyModel *pRightModel   = multiple_create_toggle_model();
	ThornburyModel *pLeftModel    = multiple_create_toggle_model();
        MILDENHALL_MULTIPLE_SPELLER_PRINT ("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);

	if (G_IS_OBJECT(priv->model) && thornbury_model_get_n_rows ( priv->model) > 0 )
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->model, inCnt );
		if(NULL != pIter)
		{
			GValue value = {0, };
                        gchar *pEntryId;
                        gboolean bIsText;
                        gfloat fltWidth;
                        gchar *leftButtonId = NULL;
                        gpointer gPtr;
                        GVariant *gvLeftIcon = NULL;
                        gfloat fltHeight ;
                        gboolean bIsText1;
                        gfloat fltWidth1;
                        gpointer gPtr1 = NULL;
                        GVariant *gvRightIcon = NULL;
                        gboolean bIsPasswordEnable;
			thornbury_model_iter_get_value(pIter, 0, &value);
			pEntryId = g_value_dup_string (&value);
			priv->entryIdList = g_list_append(priv->entryIdList,pEntryId);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 1, &value);
			bIsText = g_value_get_boolean (&value);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 2, &value);
			fltWidth = g_value_get_float (&value);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 3, &value);
			leftButtonId = g_value_dup_string (&value);
			priv->buttonIdList  = g_list_append(priv->buttonIdList,leftButtonId);
			g_value_unset (&value);

			g_object_set(priv->leftButton ,"width",fltWidth,NULL);

			thornbury_model_iter_get_value(pIter, 4, &value);
			gPtr = g_value_get_pointer (&value);
			gvLeftIcon = (GVariant*) gPtr;

			update_left_button_model(pSelf,gvLeftIcon,bIsText,pLeftModel,leftButtonId);
			g_value_unset (&value);

			g_object_get(priv->leftButton,"height",&fltHeight,NULL);

			clutter_actor_set_position( priv->leftButton,0,0);
			clutter_actor_set_position( leftLine,fltWidth+1,0);
			clutter_actor_set_position( seperator ,0,57);

			thornbury_model_iter_get_value(pIter, 5, &value);
			bIsText1 = g_value_get_boolean (&value);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 6, &value);
			fltWidth1 = g_value_get_float (&value);
			g_value_unset (&value);

			g_object_set(priv->rightButton ,"width",fltWidth1,NULL);
			clutter_actor_set_x( textBoxEntry,4+fltWidth);
			g_object_set(textBoxEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(fltWidth+fltWidth1+4),NULL);

			thornbury_model_iter_get_value(pIter, 8, &value);
			gPtr1 = g_value_get_pointer (&value);
			gvRightIcon = (GVariant*) gPtr1;

			update_right_button_model(pSelf,gvRightIcon,bIsText1,pRightModel);
			g_value_unset (&value);

			clutter_actor_set_position( priv->rightButton,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1,0);
			clutter_actor_set_position( rightLine,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1-1,0);

			if(priv->entryRows <= 2)
			{
				clutter_actor_set_position( priv->imageSep ,591,57);
			}
			thornbury_model_iter_get_value(pIter, 9, &value);
			thornbury_model_insert_value(pTextBoxModel, 1, 0, &value);
			g_object_set(textBoxEntry,"model",pTextBoxModel,NULL);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 10, &value);
			bIsPasswordEnable = g_value_get_boolean (&value);
			MILDENHALL_MULTIPLE_SPELLER_PRINT(" bIsPasswordEnable = %d \n",bIsPasswordEnable);
			g_object_set(textBoxEntry, "password-enable",bIsPasswordEnable, NULL);
			g_value_unset (&value);
		}
	}
}

/****************************************************
 * Function : v_model_data_for_multiple_entry
 * Description: update the speller entry with model info
 * Parameters: MildenhallSpellerMultipleEntry*
 * Return value: void
 *******************************************************/
static void v_model_data_for_multiple_entry( MildenhallSpellerMultipleEntry *pSelf )
{
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE( pSelf );
        guint inCnt = 0;
        MILDENHALL_MULTIPLE_SPELLER_PRINT ("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);

	priv->entryRows = thornbury_model_get_n_rows(priv->model);
	for (inCnt = 0; inCnt < (unsigned int) priv->entryRows; inCnt++)
	{
		ClutterActor *EntryGroup = clutter_actor_new();
                ThornburyItemFactory *itemFactory = NULL;
                GObject *pTextObject = NULL;
                MildenhallTextBoxEntry *pTextBox = NULL;
                ClutterActor *textBoxEntry = NULL;
                ThornburyItemFactory *ToggleRightitemFactory = NULL;
                GObject *pToggleRightObject = NULL;
                ThornburyItemFactory *ToggleLeftitemFactory = NULL;
                GObject *pToggleLeftObject = NULL;
                MildenhallToggleButton *leftToggle = NULL;
                MildenhallToggleButton *rightToggle = NULL;
		clutter_actor_add_child( CLUTTER_ACTOR( priv->EntryGroup ), EntryGroup);
		clutter_actor_set_position(EntryGroup,0,57*inCnt);

		itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TEXT_BOX_ENTRY,
				PKGDATADIR"/mildenhall_multiple_speller_text_box_prop.json");
		g_object_get(itemFactory, "object", &pTextObject, NULL);
		pTextBox = MILDENHALL_TEXT_BOX_ENTRY (pTextObject);
		textBoxEntry = CLUTTER_ACTOR (pTextBox);
		clutter_actor_add_child(EntryGroup, textBoxEntry);
		clutter_actor_set_reactive(textBoxEntry,TRUE);
		priv->EntryList = g_list_append(priv->EntryList,textBoxEntry);
		g_signal_connect(textBoxEntry,"entry-press",G_CALLBACK(b_entry_press_callback),pSelf);

		if(0 == inCnt)
		{
			priv->activeEntry = textBoxEntry;
		}

		ToggleLeftitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON,
				PKGDATADIR"/mildenhall_multiple_speller_left_toggle_prop.json");
		g_object_get(ToggleLeftitemFactory, "object", &pToggleLeftObject, NULL);
		leftToggle = MILDENHALL_TOGGLE_BUTTON (pToggleLeftObject);
		priv->leftButton = CLUTTER_ACTOR(leftToggle);
		clutter_actor_add_child(EntryGroup,priv->leftButton);
		g_signal_connect(priv->leftButton, "button-toggled",  G_CALLBACK(v_left_toggle_button_cb), pSelf);

		priv->buttonList = g_list_append(priv->buttonList,priv->leftButton);

		ToggleRightitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON,
				PKGDATADIR"/mildenhall_multiple_speller_right_toggle_prop.json");
		g_object_get(ToggleRightitemFactory, "object", &pToggleRightObject, NULL);
		rightToggle = MILDENHALL_TOGGLE_BUTTON (pToggleRightObject);
		priv->rightButton = CLUTTER_ACTOR(rightToggle);

		if(priv->entryRows <= 2)
		{
			clutter_actor_add_child(EntryGroup,priv->rightButton);
			g_signal_connect(priv->rightButton,"button-toggled",  G_CALLBACK(v_right_toggle_button_cb),pSelf);

			priv->imageSep = construct_line(EntryGroup,58,1);
		}
		construct_model_data(pSelf,textBoxEntry,EntryGroup,inCnt);
	}

	if(priv->entryRows > 2 && NULL == priv->scroller)
	{
		priv->scroller = mildenhall_scroller_new(MILDENHALL_SCROLLER_3L);
		clutter_actor_set_size(priv->scroller,68, 102);
		clutter_actor_add_child(priv->SpellerGroup, priv->scroller);
		clutter_actor_set_position(priv->scroller, 590, 0);

		g_object_set(priv->scroller,"width",58.0,"height",112.0,NULL);
		g_signal_connect(priv->scroller, "scrolled-up"  , G_CALLBACK( scrollup_cb ), pSelf);
		g_signal_connect(priv->scroller, "scrolled-down", G_CALLBACK( scrolldown_cb ), pSelf);

		clutter_actor_set_clip(clutter_actor_get_parent(priv->EntryGroup),0,0,648,154);
		clutter_actor_set_child_above_sibling(priv->SpellerGroup, priv->scroller, NULL);
	}
}


/****************************************************
 * Function : v_multiple_speller_update_view
 * Description: update the speller entry
 * Parameters: MildenhallSpellerMultipleEntry*
 * Return value: None
 *******************************************************/
static void v_multiple_speller_update_view( MildenhallSpellerMultipleEntry *pSelf )
{
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	v_model_data_for_multiple_entry(pSelf);
}


/**
 * v_multiple_speller_set_text:
 * @pSelf: object reference
 * @pArgList: text to be extracted from type #GVariant
 *
 * set the text on TextEntryBox
 *
 */
void v_multiple_speller_set_text(MildenhallSpellerMultipleEntry *pSelf, GVariant *pArgList)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
        ThornburyModel *pTextBoxModel = NULL;
        GVariantIter iter;
        gchar *pText = NULL;
        gchar *key = NULL;
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(priv->activeEntry)
	{
		g_object_get(priv->activeEntry,"model",&pTextBoxModel,NULL);
	}
	if(NULL == pArgList)
		return;

	g_variant_iter_init (&iter, pArgList);
	while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
	{
		if(!g_strcmp0(pText,"SPACE"))
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value," ");
			thornbury_model_insert_value(pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		else if(!g_strcmp0(pText,"RETURN") || !g_strcmp0(pText,"go") )
		{
			GVariantBuilder *pVariant = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
			int inCnt =0 ;
                        GHashTableIter hash_iter;
                        gpointer local_key = NULL, value = NULL;
			for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->EntryList); inCnt++)
			{
				ClutterActor *pEntryBox  = g_list_nth_data(priv->EntryList,inCnt);
				gchar *pInfoVal = NULL;
				g_object_get(pEntryBox, "text", &pInfoVal, NULL);
				g_variant_builder_add (pVariant, "{ss}", g_strdup((gchar*)g_list_nth_data(priv->entryIdList,inCnt)), g_strdup(pInfoVal ));
			}
			g_hash_table_iter_init (&hash_iter, priv->toggleList);
			while(g_hash_table_iter_next (&hash_iter, &local_key, &value))
			{
				g_variant_builder_add (pVariant, "{ss}", g_strdup(local_key), g_strdup (value));
			}
			priv->argList = g_variant_builder_end (pVariant);
			g_variant_builder_unref(pVariant);
		}
		else if(!g_strcmp0(pText,"dot"))
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value,".");
			thornbury_model_insert_value(pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		else if(!g_strcmp0(pText,"CANCEL"))
		{
			lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (priv->activeEntry));
		}
		else if(!g_strcmp0(pText,"SYMBOLS") || !g_strcmp0(pText,"SHIFT") || !g_strcmp0(pText,"AUTOCOMPLETE"))
		{
			//TBD
		}
		else
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value,g_strdup(pText));
			thornbury_model_insert_value(pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		if(pText!=NULL)
		{
			g_free (pText);
			pText = NULL;
		}
		if(key!=NULL)
		{
			g_free (key);
			key = NULL;
		}
	}
}

/**
 * v_multiple_speller_set_clear_text:
 * @pSelf: object reference
 * @clearText: set to true/false to clear the text
 *
 * set to clear the text on entry
 *
 */
void v_multiple_speller_set_clear_text(MildenhallSpellerMultipleEntry *pSelf, gboolean clearText)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT :%s clearText = %d \n", __FUNCTION__,clearText);
	if(!MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	priv->clearText = clearText;
	if( TRUE == priv->clearText)
	{
		int inCnt =0 ;
		for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->EntryList); inCnt++)
		{
			ClutterActor *pEntryBox  = g_list_nth_data(priv->EntryList,inCnt);
			lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (pEntryBox));
		}
		priv->clearText = FALSE;
	}
}

/**
 * v_multiple_speller_set_cursor_focus:
 * @pSelf: object reference
 * @cursor_focus_entry: set to cursor focus entry
 *
 * set to cursor focus on particular entry
 *
 */
void v_multiple_speller_set_cursor_focus(MildenhallSpellerMultipleEntry *pSelf, gint cursor_focus_entry)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTIPLE_SPELLER_PRINT(" MILDENHALL_MULTIPLE_SPELLER_PRINT :%s cursor_focus_entry = %d \n", __FUNCTION__,cursor_focus_entry);
	if(!MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	priv->cursor_focus_entry = cursor_focus_entry;

	if(NULL != priv->EntryList)
	{
		ClutterActor* pEntryBox  = CLUTTER_ACTOR(g_list_nth_data(priv->EntryList,(cursor_focus_entry)));
		if(pEntryBox)
		{
			if(0 == cursor_focus_entry)
			{
				pSelf->priv->offset = 0;
				clutter_actor_set_position (priv->EntryGroup,0,0.0);
				g_object_set(pSelf->priv->scroller,"reset-slider",TRUE,NULL);
			}
			g_object_set(pEntryBox, "focus-cursor",TRUE, NULL);
			priv->activeEntry = pEntryBox;
		}
	}
}
void v_multiple_speller_column_changed_cb(gint col,gpointer pSelf)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->inColumnId = col;
}

/**
 * v_multiple_speller_set_model:
 * @pSelf: meta info header object reference
 * @pModel: model for the speller
 *
 * Model format:
 *	"left-icon-istext"  - gboolean
 *	"left-icon-width"   - gfloat
 *	"left-icon-info"    - GVariant*
 *	"right-icon-istext" - gboolean
 *	"right-icon-width"  - gfloat
 *	"right-icon-info"   - GVariant*
 * 	"default-text"     - gchar*
 **/

void v_multiple_speller_set_model(MildenhallSpellerMultipleEntry *pSelf, ThornburyModel *pModel)
{
        MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTIPLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(NULL != priv->model)
	{
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multiple_speller_row_added_cb),   pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multiple_speller_row_changed_cb), pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multiple_speller_row_removed_cb), pSelf);
		//g_object_unref (priv->model);
		thornbury_list_model_destroy(priv->model);
		priv->model = NULL;
	}
	/* update the new model with signals */
	if ( NULL != pModel)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));
		priv->model = g_object_ref(pModel);
		g_signal_connect (priv->model, "row-added",   G_CALLBACK (v_multiple_speller_row_added_cb),   pSelf );
		g_signal_connect (priv->model, "row-changed", G_CALLBACK (v_multiple_speller_row_changed_cb), pSelf );
		g_signal_connect (priv->model, "row-removed", G_CALLBACK (v_multiple_speller_row_removed_cb), pSelf );
		thornbury_model_register_column_changed_cb(priv->model,v_multiple_speller_column_changed_cb,pSelf);
	}
	v_multiple_speller_update_view(pSelf);
	g_object_notify (G_OBJECT(pSelf), "model");
}


/********************************************************
 * Function : v_mildenhall_speller_multiple_entry_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multiple_entry_get_property (GObject *pObject, guint uinPropertyId,
		GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerMultipleEntry *pSelf = MILDENHALL_SPELLER_MULTIPLE_ENTRY (pObject);
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_TEXT:
	{
		g_value_set_variant (pValue, pSelf->priv->argList);
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		g_value_set_boolean (pValue, pSelf->priv->clearText);
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_FOCUS_CURSOR:
	{
		g_value_set_int (pValue, pSelf->priv->cursor_focus_entry);
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_MODEL:
	{
		g_value_set_object (pValue, pSelf->priv->model);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}

/********************************************************
 * Function : v_mildenhall_speller_multiple_entry_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multiple_entry_set_property (GObject *pObject, guint uinPropertyId,
		const GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerMultipleEntry *pSelf = MILDENHALL_SPELLER_MULTIPLE_ENTRY (pObject);
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_TEXT:
	{
		v_multiple_speller_set_text (pSelf, g_value_get_variant( pValue ));
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		v_multiple_speller_set_clear_text (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_FOCUS_CURSOR:
	{
		v_multiple_speller_set_cursor_focus(pSelf, g_value_get_int( pValue ));
		break;
	}
	case MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_MODEL:
	{
		v_multiple_speller_set_model (pSelf, g_value_get_object ( pValue ));
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}




/********************************************************
 * Function : v_mildenhall_speller_multiple_entry_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multiple_entry_dispose (GObject *pObject)
{
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_multiple_entry_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : v_mildenhall_speller_multiple_entry_finalize
 * Description: Finalize the speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multiple_entry_finalize (GObject *pObject)
{
	MILDENHALL_MULTIPLE_SPELLER_PRINT("MILDENHALL_MULTIPLE_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_multiple_entry_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : v_mildenhall_speller_multiple_entry_constructed
 * Description: the constructor function is called by g_object_new()
                to complete the object initialization after all the
                construction properties are set.
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multiple_entry_constructed (GObject *pObject)
{
	MildenhallSpellerMultipleEntry *pSelf = MILDENHALL_SPELLER_MULTIPLE_ENTRY(pObject);
	MildenhallSpellerMultipleEntryPrivate *priv = SPELLER_MULTIPLE_ENTRY_PRIVATE(pSelf);

	ClutterColor backGroundColor = {0x00,0x00,0x00,0xCC};
        MILDENHALL_MULTIPLE_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);

	//create a group for speller
	priv->SpellerGroup = clutter_actor_new();
	clutter_actor_set_position( priv->SpellerGroup ,0 ,0);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->SpellerGroup);

	//create a group for all entries speller
	priv->EntryGroup = clutter_actor_new();
	clutter_actor_set_position( priv->EntryGroup ,0 ,0);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->EntryGroup);

	// create a blank rectangle to add entry items
	//priv->blankRect = clutter_rectangle_new_with_color(&backGroundColor);
	priv->blankRect = clutter_actor_new();
	clutter_actor_set_background_color ( priv->blankRect,&backGroundColor);
	clutter_actor_set_opacity(priv->blankRect,252);
	clutter_actor_set_size(priv->blankRect,MILDENHALL_BLANK_RECT_WIDTH,MILDENHALL_BLANK_RECT_HEIGHT);
	clutter_actor_add_child(priv->SpellerGroup, priv->blankRect);
	clutter_actor_set_position( priv->blankRect,0,0);
}

/********************************************************
 * Function : mildenhall_speller_multiple_entry_class_init
 * Description: Class initialisation function for the pObject type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_multiple_entry_class_init (MildenhallSpellerMultipleEntryClass *pKlass)
{
        GObjectClass *pObject_class = G_OBJECT_CLASS (pKlass);
        GParamSpec *pPspec = NULL;
	MILDENHALL_MULTIPLE_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	g_type_class_add_private (pKlass, sizeof (MildenhallSpellerMultipleEntryPrivate));

	pObject_class->get_property = v_mildenhall_speller_multiple_entry_get_property;
	pObject_class->set_property = v_mildenhall_speller_multiple_entry_set_property;
	pObject_class->dispose      = v_mildenhall_speller_multiple_entry_dispose;
	pObject_class->finalize     = v_mildenhall_speller_multiple_entry_finalize;
	pObject_class->constructed  = v_mildenhall_speller_multiple_entry_constructed;

	/**
	 * MildenhallSpellerMultipleEntry:text:
	 *
	 *  text to be set which can be set from #GVariant type
	 */
	pPspec = g_param_spec_variant("text",
			"text",
			"text",
			G_VARIANT_TYPE("a{ss}"),NULL,
			(G_PARAM_READWRITE));
	g_object_class_install_property ( pObject_class, MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_TEXT, pPspec );

	/**
	 * MildenhallSpellerMultipleEntry:clear-text:
	 *
	 * If set to TRUE, will clear the text present in the text entry.
	 */
	pPspec = g_param_spec_boolean ( "clear-text",
			"clear-text",
			"clear-text",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_CLEAR_TEXT, pPspec );

	/**
	 * MildenhallSpellerMultipleEntry:cursor-focus:
	 *
	 * this is to set the cursor focus on a particular entry.
	 */
	pPspec = g_param_spec_int ( "cursor-focus",
			"cursor-focus",
			"cursor-focus",
			0,
			40,
			0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_FOCUS_CURSOR, pPspec );

	/**
	 * MildenhallSpellerMultipleEntry:model:
	 *
	 * Sets the model to be displayed on the multiple entry
	 *  Model contains the following data:
	 *      "left-icon-istext"  - set to TRUE, if left icon is a text. FALSE otherwise
	 *      "left-icon-width"   -  width of the left icon
	 *      "right-icon-istext" - set to TRUE, if right icon is a text. FALSE otherwise
	 *      "right-icon-width"  - set to TRUE, if right icon is a text. FALSE otherwise
	 *      "default-text"     - default text to be shown on the text entry.
	 */
	pPspec = g_param_spec_object ("model",
			"model",
			"model",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObject_class, MILDENHALL_MULTIPLE_SPELLER_PROP_ENUM_MODEL, pPspec);

	/**
	 * MildenhallSpellerMultipleEntry::entry-action:
	 *
	 * ::entry-action is emitted when the text entry is pressed.
	 *
	 */
	multiple_entry_action_signals[SIG_MULTIPLE_ENTRY_ACTION] = g_signal_new("multiple-entry-action",
			G_TYPE_FROM_CLASS (pObject_class),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallSpellerMultipleEntryClass, multiple_state_change),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 1,G_TYPE_STRING);
}

/********************************************************
 * Function : mildenhall_speller_multiple_entry_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_multiple_entry_init (MildenhallSpellerMultipleEntry *pSelf)
{
	pSelf->priv = SPELLER_MULTIPLE_ENTRY_PRIVATE (pSelf);
	pSelf->priv->blankRect    = NULL;
	pSelf->priv->SpellerGroup = NULL;
	pSelf->priv->entryIdList  = NULL;
	pSelf->priv->scroller     = NULL;
	pSelf->priv->offset       = 0;
	pSelf->priv->buttonList   = NULL;
	pSelf->priv->toggleList  = g_hash_table_new( g_str_hash, g_str_equal );
	pSelf->priv->cursor_focus_entry = -1;
}

/**
 * mildenhall_speller_multiple_entry_new:
 *
 * Creates a speller entry object
 *
 * Returns: (transfer full): a newly created speller entry object
 *
 */
ClutterActor *mildenhall_speller_multiple_entry_new (void)
{
	MILDENHALL_MULTIPLE_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	return g_object_new (MILDENHALL_TYPE_SPELLER_MULTIPLE_ENTRY, NULL);
}



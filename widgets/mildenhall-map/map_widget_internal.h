/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* map-widget.h */

#ifndef __MAP_WIDGET_INTERNAL_H__
#define __MAP_WIDGET_INTERNAL_H__


#include <stdio.h>
#include <math.h>
#include<stdlib.h>
#include "map_widget.h"
#include <thornbury/thornbury.h>

#define MAP_WIDGET_MAX_NEAR_DISTANCE 150  // 0- 100 pixels near tap
#define MAP_WIDGET_MAX_FAR_DISTANCE 700   // 100-700 pixels far tap
#define MAP_WIDGET_MAX_TIME_FOR_SECOND_TAP 2000 // 2 secs.

G_BEGIN_DECLS


typedef enum _MapModel MapModel;



enum _MapModel
{
   COLUMN_VIEW_POINT_TYPE,
   COLUMN_LATITUDE,
   COLUMN_LONGITUDE,
   COLUMN_MARKER_NAME,
   COLUMN_NORMAL_IMAGE,
   COLUMN_SELECTED_IMAGE,
   COLUMN_HIGHLIGHT_MARKER



};




typedef struct _Imagedata Imagedata;

struct _Imagedata
{
    gchar *normalImage;
    gchar *selectedImage;
};

struct _MapWidgetPrivate
{
    gfloat longitude;
    gfloat latitude;
    gint zoom_level;
    gint width;
    gint height;
    gint current_type_counter;
    gint coumn_changed;
    gint highlight_marker_counter;
    gint path_highlight_marker_counter;
    gint selectedMarkerIndex;
    gint selectedPathMarkerIndex;
    gboolean poi_touch_event;
    gboolean path_touch_event;
    gint touchendpoints;
    /*gint animation;*/
    gint mapState;
    gboolean bEnableMove;
    ClutterActor *content_map;
    ClutterActor *content_bkground;
    ClutterActor *content_group;
    ThornburyModel *pModel;

    ClutterActor *pLayerActor;
    ClutterActor *prevMarker;
    ClutterActor *prevPathMarker;
    ClutterActor *pPathLayerActor;
    ClutterActor *pPosDestinationLayer;
    GPtrArray *glMarkerslist;
    GPtrArray *glImagelist;
    GPtrArray *pPathMarkerslist;
    GPtrArray *pPathImagelist;
    GHashTable *pStyleHash;
    GHashTable *pPrivHash;

};



typedef enum
{

    MAP_WIDGET_DEBUG = 1 << 0,

} MapWidgetFlag;
#define MAP_WIDGET_HAS_DEBUG               ((map_widget_debug_flags ) & 1)
#define MAP_WIDGET_DEBUG( a ...) \
    if (G_LIKELY (MAP_WIDGET_HAS_DEBUG )) \
    {			            	\
        g_print(a);		\
    }

extern guint map_widget_debug_flags;

static const GDebugKey map_widget_debug_keys[] =
{

    { "map-widget", MAP_WIDGET_DEBUG }
};





void v_extract_model_to_add_view_points(MapWidget *pSelf);
gboolean b_map_widget_map_selected_clb(ClutterActor *pActor,
ClutterButtonEvent *pEvent, gpointer pUserData);
gboolean b_map_widget_map_selected_touch_clb(ClutterActor *pActor,
		ClutterButtonEvent *pEvent, gpointer pUserData);
void map_widget_go_to_location(ClutterActor *actor, gfloat flLatitude,gfloat flLongitude );
ClutterActor* p_map_widget_get_map (MapWidget *pSelf);
ClutterActor* p_map_widget_get_bkground (MapWidget *pSelf);
void v_map_widget_set_location(MapWidget *pSelf, gfloat latitude,gfloat longitude );
ThornburyModel* p_map_widget_get_model (MapWidget *pSelf);
ClutterActor* p_map_widget_get_layer_actor (MapWidget *pSelf);
gboolean b_map_widget_marker_selected_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData);
gboolean b_map_widget_marker_selected_touch_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData);
gboolean b_map_widget_path_marker_selected_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData);
gboolean b_map_widget_path_marker_selected_touch_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData);
GPtrArray *p_map_widget_get_markers_list (MapWidget *pSelf);
GPtrArray *p_map_widget_get_image_list (MapWidget *pSelf);
void v_map_widget_move_to_location(MapWidget *pSelf,gfloat latitude,gfloat longitude,gint numsteps);
ClutterColor *p_map_widget_return_normal_colour(MapWidget *pSelf);
ClutterColor *p_map_widget_return_selected_colour(MapWidget *pSelf);
ClutterActor* p_map_widget_get_path_layer_actor(MapWidget *pSelf);
ClutterColor *p_map_widget_return_path_colour(MapWidget *pSelf);
void v_map_widget_draw_path(MapWidget *pSelf,ClutterActor *pLayer,gfloat width);
GHashTable *p_map_widget_get_private_hash(MapWidget *pSelf);
GPtrArray *p_map_widget_get_path_image_list(MapWidget *pSelf);
GPtrArray *p_map_widget_get_path_markers_list(MapWidget *pSelf);

G_END_DECLS

#endif /* __MAP_WIDGET_H__ */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH <sdk.support@de.bosch.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "test-utils.h"
#include "mildenhall-internal.h"

#define LONG_TEXT               "long text for item %i\nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \n"

ThornburyModel *
lightwood_create_model (guint num_of_items)
{
#if with_cogl
  CoglHandle icon1, icon2, thumb1, thumb2;
  CoglHandle covers[4];
#else
  gchar *icon1,*icon2;
  gchar *covers[4];
#endif

  g_autofree gchar *email_icon_file = NULL;
  g_autofree gchar *music_icon_file = NULL;
  g_autofree gchar *cover1_file = NULL;
  g_autofree gchar *cover2_file = NULL;
  g_autofree gchar *cover3_file = NULL;
  g_autofree gchar *cover4_file = NULL;
  g_autofree gchar *launcher_video_file = NULL;
  g_autofree gchar *content_video_file = NULL;
  g_autofree gchar *launcher_thumbnail_file = NULL;
  g_autofree gchar *content_thumbnail_file = NULL;

  ThornburyModel *model;
  int i;

  email_icon_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "icon_email.png",
      NULL);

  music_icon_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "icon_music.png",
      NULL);

  cover1_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "cover1.jpg",
      NULL);

  cover2_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "cover2.jpg",
      NULL);

  cover3_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "cover3.jpg",
      NULL);

  cover4_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "cover4.jpg",
      NULL);

  launcher_video_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "01_appl_launcher_2009.03.09.avi",
      NULL);

  content_video_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "03_content_roll_2009.03.09.avi",
      NULL);

  launcher_thumbnail_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "01_appl_launcher_2009.03.09.png",
      NULL);

  content_thumbnail_file = g_build_filename (
      _mildenhall_get_theme_path (),
      "mildenhallroller",
      "03_content_roll_2009.03.09.png",
      NULL);

#if with_cogl
  icon1 = cogl_texture_new_from_file (email_icon_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);

  icon2 = cogl_texture_new_from_file (music_icon_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);
#else
  icon1 = email_icon_file;
  icon2 = music_icon_file;
#endif

#if with_cogl
  covers[0] = cogl_texture_new_from_file (cover1_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);

  covers[1] = cogl_texture_new_from_file (cover2_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);

  covers[2] = cogl_texture_new_from_file (cover3_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);

  covers[3] = cogl_texture_new_from_file (cover4_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);
#else
  covers[0] = cover1_file;
  covers[1] = cover2_file;
  covers[2] = cover3_file;
  covers[3] = cover4_file;
#endif

#if with_cogl
  thumb1 = cogl_texture_new_from_file (launcher_thumbnail_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);

  thumb2 = cogl_texture_new_from_file (content_thumbnail_file, COGL_TEXTURE_NONE,
                  COGL_PIXEL_FORMAT_ANY, NULL);
#endif


  model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST, G_TYPE_STRING, NULL,
#if with_cogl
                  COGL_TYPE_HANDLE, NULL,
#else
                  G_TYPE_STRING, NULL,
#endif
                  G_TYPE_STRING, NULL,
                  G_TYPE_BOOLEAN, NULL,
                  G_TYPE_STRING, NULL,
                  G_TYPE_FLOAT, NULL,
#if with_cogl
                  COGL_TYPE_HANDLE, NULL,
                  COGL_TYPE_HANDLE, NULL,
#else
                  G_TYPE_STRING, NULL,
                  G_TYPE_STRING, NULL,
#endif
                  G_TYPE_STRING, NULL,
                  -1);

  for (i = 0; i < num_of_items; i++)
    thornbury_model_append (model, COLUMN_NAME, g_strdup_printf ("item number %i", i),
                    COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
                    COLUMN_LABEL, g_strdup_printf ("item number %i", i),
                    COLUMN_TOGGLE, i % 2,
                    COLUMN_VIDEO, i % 2 == 0 ? launcher_video_file : content_video_file,
                    COLUMN_EXTRA_HEIGHT, (float) (i % 100),
                    COLUMN_COVER, covers[i % 3],
#if with_cogl
                    COLUMN_THUMB, i % 2 == 0 ? thumb1 : thumb2,
#else
                    COLUMN_THUMB,NULL,
#endif
                    COLUMN_LONG_TEXT, g_strdup_printf (LONG_TEXT, i),
                    -1);

  return model;
}

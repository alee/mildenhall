/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_progress_bar
 * @title: MildenhallProgressBar
 * @short_description: #MildenhallProgressBar creates a new progress bar wigets
 * which extends from #LightwoodProgressBase.
 * @see_also: #ThornburyItemFactory, #ClutterActor
 *
 * #MildenhallProgressBar provides seek end, seek-start, update progress bar
 * and top to seek,play started,pause features.
 * It is designed to use mostly for the media player applications.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 *
 * How to create #MildenhallProgressBar
 *
 * |[<!-- language="C" -->
 * ThornburyItemFactory *item_factory = NULL;
 * GObject *object = NULL;
 * MildenhallProgressBar *progress_bar
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 *      MILDENHALL_TYPE_PROGRESS_BAR,
 *      "/usr/share/mildenhall/mildenhall_progress_bar_prop.json");
 * g_object_get (itemFactory, "object", &object, NULL);
 *
 * progress_bar = MILDENHALL_PROGRESS_BAR (object);
 *
 * g_object_set (progress_bar, "current-duration", 0.5, NULL);
 * clutter_actor_add_child (CLUTTER_ACTOR (stage), CLUTTER_ACTOR (progress_bar));
 *
 * g_signal_connect (progress_bar, "play-requested",
 *      G_CALLBACK (on_play_requested_cb), progress_bar);
 *
 * ]|
 *
 * Example of #MildenhallProgressBar::play-requested callback implementation
 *
 * |[<!-- language="C" -->
 * static gboolean
 * on_play_requested_cb (MildenhallProgressBar *progress_bar, gpointer user_data)
 * {
 *   g_timeout_add_seconds (1, set_current_duration, (gpointer) progress_bar);
 *
 *   return TRUE;
 * }
 * ]|
 *
 * #MildlenhallProgressBar::play-requested will be called when #MildenhallProgressBar:play-state is changed.
 * |[<!-- language="C" -->
 * g_object_set (progress_bar, "play-state", FALSE, NULL);
 * ]|
 *
 * Since: 0.3.0
 *
 */
#include "mildenhall_progress_bar.h"

G_DEFINE_TYPE (MildenhallProgressBar, mildenhall_progress_bar, LIGHTWOOD_TYPE_PROGRESS_BASE)
#define PROGRESS_BAR_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_PROGRESS_BAR, MildenhallProgressBarPrivate))

#define MILDENHALL_ROLLS_NORMAL_SEAMLESS	"rolls-normal-seamless"
#define MILDENHALL_BG_PLAY_NORMAL			  "bg-play-normal"
#define MILDENHALL_BG_PLAY_PRESSED 		  "bg-play-pressed"
#define MILDENHALL_ICON_PLAY				      "icon-play"
#define MILDENHALL_RADIO_STOP_AC			    "radio-stop-ac"
#define MILDENHALL_ICON_PAUSE 				    "icon-pause"
static gboolean on_gesture_end(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData);
void update_play_state(GObject *progressB, GParamSpec *pspec,gpointer pUserData);
void update_draggable_action(GObject *progressB, GParamSpec *pspec,gpointer pUserData);
static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static void on_drag_begin (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,ClutterModifierType modifiers,gpointer user_data);
static void on_drag_end (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,ClutterModifierType modifiers,gpointer user_data);
static void on_drag_motion (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,gpointer user_data);
static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData);

static void v_progress_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static void
v_progress_bar_add_style_to_hash (MildenhallProgressBar *progressBar, const gchar *pKey, gpointer pValue);

void create_progress_bar(MildenhallProgressBar *progressBar);
void update_progress_bar(GObject *progressB, GParamSpec *pspec,gpointer data);
void progress_bar_reset_width(GObject *progressB, GParamSpec *pspec,gpointer data);
void update_buffer_fill(GObject *progressB, GParamSpec *pspec,gpointer data);
void set_progress_bar_position(GObject *progressBar, GParamSpec *pspec,gpointer data);
static void progress_bar_reset_opacity(GObject *progressB, GParamSpec *pspec,gpointer data);

static void mildenhall_create_progress_bar_actors(MildenhallProgressBar *progressBar);
static void mildenhall_progress_bar_parse_style(MildenhallProgressBar *self);
/* Set the environment variable in terminal to enable traces: export MILDENHALL_PROGRESS_BAR_DEBUG=mildenhall-progress-bar */
enum _enProgressBarDebugFlag
{
	MILDENHALL_PROGRESS_BAR_DEBUG = 1 << 0,

};

guint  mildenhall_progress_bar_debug_flags = 0;

static const GDebugKey mildenhall_progress_bar_debug_keys[] =
{
		{ "mildenhall-progress-bar",   MILDENHALL_PROGRESS_BAR_DEBUG }
};

#define MILDENHALL_PROGRESS_BAR_HAS_DEBUG               ((mildenhall_progress_bar_debug_flags ) & 1)
#define MILDENHALL_PROGRESS_BAR_PRINT( a ...) \
		if (G_LIKELY (MILDENHALL_PROGRESS_BAR_HAS_DEBUG )) \
		{                               \
			g_print(a);           \
		}



struct _MildenhallProgressBarPrivate
{
	ClutterAction *action;

	ClutterActor *playPauseGroup;
	ClutterActor *progressBarGroup;
	ClutterActor *sliderRect;
	ClutterActor *fillBarRect;
	ClutterActor *bufferRect;
	ClutterActor *playPauseButton;
	ClutterActor *playPauseBackground;
	ClutterActor *textureBackground;
	ClutterActor *seamlessTexture;
	ClutterActor *upperLine;

	gboolean isDragMotion;
	gboolean isDragEnd;
	gboolean playState;
	gfloat seekPos;

	GHashTable *pStyleHash;
	GHashTable *pLocalHash;

	ClutterColor fillBarColor;
	ClutterColor sliderColor;
	ClutterColor lineColor;

};

static void mildenhall_progress_bar_get_property (GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void mildenhall_progress_bar_set_property (GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void mildenhall_progress_bar_dispose (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_progress_bar_parent_class)->dispose (object);
}

static void mildenhall_progress_bar_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_progress_bar_parent_class)->finalize (object);
}

static void mildenhall_progress_bar_class_init (MildenhallProgressBarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	MILDENHALL_PROGRESS_BAR_PRINT("\n MILDENHALL PROGRESS BAR NEW CLASS INIT \n");
	g_type_class_add_private (klass, sizeof (MildenhallProgressBarPrivate));

	object_class->get_property = mildenhall_progress_bar_get_property;
	object_class->set_property = mildenhall_progress_bar_set_property;
	object_class->dispose = mildenhall_progress_bar_dispose;
	object_class->finalize = mildenhall_progress_bar_finalize;
}

static void mildenhall_create_progress_bar_actors(MildenhallProgressBar *progressBar)
{
	MildenhallProgressBarPrivate *priv = progressBar->priv;
	GError *error = NULL;
	gchar *pRollsNormal = NULL;
	gchar *pPlayNormal = NULL;
        gfloat progressBarWidth;
        gdouble progressBarOffset;

	pRollsNormal = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ROLLS_NORMAL_SEAMLESS);
	pPlayNormal = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_BG_PLAY_NORMAL);

	g_object_get(progressBar,"width",&progressBarWidth,NULL);
	g_object_get(progressBar,"offset",&progressBarOffset,NULL);
	/* create progress bar group */
	priv->progressBarGroup = clutter_actor_new();
	/* add seamless texture to the progress bar */
	priv->textureBackground = thornbury_ui_texture_create_new(pRollsNormal, 0,0,FALSE,FALSE);
	if (error)
	{
		g_warning("texture load failed: %s\n", error->message);
		g_clear_error(&error);
	}
	priv->seamlessTexture = priv->textureBackground;
	//FIXME:Use after adding the opacity property.

	clutter_actor_add_child (CLUTTER_ACTOR(priv->progressBarGroup), priv->seamlessTexture);
	clutter_actor_set_size (priv->seamlessTexture,progressBarWidth, 64);

	/* add upper line */
	priv->upperLine = clutter_actor_new();//(&lineColor);
	clutter_actor_set_background_color(CLUTTER_ACTOR(priv->upperLine),&priv->lineColor);
	clutter_actor_set_size (priv->upperLine, progressBarWidth, 1);
	clutter_actor_add_child (CLUTTER_ACTOR(priv->progressBarGroup), priv->upperLine);

	/* progress bar slider */
	priv->sliderRect = clutter_actor_new();//(&sliderColor);
	clutter_actor_set_background_color(CLUTTER_ACTOR(priv->sliderRect),&priv->sliderColor);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->progressBarGroup), priv->sliderRect);
	clutter_actor_set_size(priv->sliderRect, progressBarWidth - progressBarOffset, 2);
	clutter_actor_set_position(priv->sliderRect, 10, 32);

	/* fill bar */
	priv->fillBarRect = clutter_actor_new();//(&fillBarColor);
	clutter_actor_set_background_color(CLUTTER_ACTOR(priv->fillBarRect),&priv->fillBarColor);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->progressBarGroup), priv->fillBarRect);
	clutter_actor_set_size(priv->fillBarRect,progressBarWidth - progressBarOffset, 2);
	clutter_actor_set_position(priv->fillBarRect, 10, 32);
	clutter_actor_set_clip(priv->fillBarRect, 0, 0, clutter_actor_get_height(priv->fillBarRect), 0);

	/* buffer bar */
	priv->bufferRect = clutter_actor_new();//(&fillBarColor);
	clutter_actor_set_background_color(CLUTTER_ACTOR(priv->bufferRect),&priv->fillBarColor);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->progressBarGroup), priv->bufferRect);
	clutter_actor_set_size(priv->bufferRect, progressBarWidth - progressBarOffset, 2);
	clutter_actor_set_position(priv->bufferRect, 10, 32);
	clutter_actor_set_clip(priv->bufferRect, 0, 0, clutter_actor_get_height(priv->bufferRect), 0);

	/* play button group */
	priv->playPauseGroup = lightwood_progress_base_get_play_pause_group (LIGHTWOOD_PROGRESS_BASE (progressBar));

	/* play/pause button background */
	priv->playPauseBackground = thornbury_ui_texture_create_new(pPlayNormal, 0,0,FALSE,FALSE);
	clutter_actor_set_reactive(priv->playPauseGroup, TRUE);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->playPauseGroup), priv->playPauseBackground);
	clutter_actor_set_size(priv->playPauseBackground, 64, 64);
}

static void mildenhall_create_progress_bar_play_button(MildenhallProgressBar *progressBar)
{
	MildenhallProgressBarPrivate *priv = progressBar->priv;
	gchar *pIconPlay = NULL;
	gchar *pIconPause = NULL;
	gchar *pRadioStopAC = NULL;
        gboolean reactive;
        gboolean playState;
	pRadioStopAC = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_RADIO_STOP_AC);
	pIconPlay = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PLAY);
	pIconPause = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PAUSE);
	g_object_get(progressBar,"play-state",&playState,NULL);
	priv->playState=playState;
	g_object_get(progressBar,"reactive",&reactive,NULL);
	/* play button image */
	if (reactive)
	{
		MILDENHALL_PROGRESS_BAR_PRINT("\nPlayState in CREATE PROGRESS BAR %d\n",priv->playState);
		if(priv->playState)
		{
			MILDENHALL_PROGRESS_BAR_PRINT("\n Inside play\n");
			priv->playPauseButton = thornbury_ui_texture_create_new(pIconPlay, 0,0,FALSE,FALSE);
		}
		else
		{
			MILDENHALL_PROGRESS_BAR_PRINT("\n Inside pause\n");
			priv->playPauseButton = thornbury_ui_texture_create_new(pIconPause, 0,0,FALSE,FALSE);
		}
		MILDENHALL_PROGRESS_BAR_PRINT("\n Inside Reactive \n");

	}
	else
	{
		MILDENHALL_PROGRESS_BAR_PRINT("\n If it is not reactive\n");
		priv->playPauseButton = thornbury_ui_texture_create_new(pRadioStopAC, 0,0,FALSE,FALSE);
	}
	clutter_actor_add_child(CLUTTER_ACTOR(priv->playPauseGroup), priv->playPauseButton);
	clutter_actor_set_position(priv->playPauseButton, 14, 14);

}

void create_progress_bar(MildenhallProgressBar *progressBar)
{
	MildenhallProgressBarPrivate *priv = progressBar->priv;
	ClutterAction *action = clutter_gesture_action_new ();

	gdouble currentDuration;
	g_object_set(progressBar, "reactive", TRUE, NULL);
	g_object_get(progressBar,"current-duration",&currentDuration,NULL);

	MILDENHALL_PROGRESS_BAR_PRINT("********************CREATE PROGRESS BAR START************************");

	mildenhall_create_progress_bar_actors(progressBar);

	mildenhall_create_progress_bar_play_button(progressBar);

	/* add the play pause group to the progress bar */
	clutter_actor_add_child(CLUTTER_ACTOR(priv->progressBarGroup),CLUTTER_ACTOR(priv->playPauseGroup));
	clutter_actor_add_child(CLUTTER_ACTOR(progressBar), priv->progressBarGroup);
	clutter_actor_set_x (priv->playPauseGroup, currentDuration);
	//Vertical Drag
	clutter_actor_add_action (priv->playPauseGroup, action);
	g_signal_connect (action, "gesture-end", G_CALLBACK (on_gesture_end), progressBar);
}

static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
    MildenhallProgressBar *progressBar = MILDENHALL_PROGRESS_BAR (userData);
    MildenhallProgressBarPrivate *priv = PROGRESS_BAR_PRIVATE (progressBar);
    gboolean state;
    gboolean reactive;
    gchar *pIconPlay = NULL;
    gchar *pIconPause = NULL;
    gchar *pPlayNormal = NULL;

    MILDENHALL_PROGRESS_BAR_PRINT("\n***************** ON BUTTON RELEASE START *********************** \n");

	pIconPlay = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PLAY);
	pIconPause = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PAUSE);
	pPlayNormal = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_BG_PLAY_NORMAL);



	g_object_get(progressBar,"reactive",&reactive,NULL);

	if(reactive)
	{

		if(priv->isDragMotion==FALSE)
		{

			if(priv->playState)
			{
				//MILDENHALL_PROGRESS_BAR_PRINT("\n PLAYSTATE IS TRUE \n");
				MILDENHALL_PROGRESS_BAR_PRINT("\n Before CHangeing IN BUTTON RELEASE PLAYSTATE %d",priv->playState);
				thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseButton),pIconPause, 0,0,FALSE,FALSE);
				priv->playState= FALSE;
				lightwood_progress_base_set_play_state (LIGHTWOOD_PROGRESS_BASE (progressBar), priv->playState);
				g_object_get(progressBar,"play-state",&state,NULL);
				g_signal_emit_by_name(progressBar,"pause-requested");
				MILDENHALL_PROGRESS_BAR_PRINT("\n After CHangeing IN BUTTON RELEASE PLAYSTATE %d",priv->playState);
			}
			else
			{
				MILDENHALL_PROGRESS_BAR_PRINT("\n Before CHangeing IN BUTTON RELEASE PLAYSTATE %d",priv->playState);
				thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseButton),pIconPlay, 0,0,FALSE,FALSE);
				priv->playState= TRUE;
				lightwood_progress_base_set_play_state (LIGHTWOOD_PROGRESS_BASE (progressBar), priv->playState);
				g_object_get(progressBar,"play-state",&state,NULL);
				g_signal_emit_by_name(progressBar,"play-requested");
				MILDENHALL_PROGRESS_BAR_PRINT("\n After CHangeing IN BUTTON RELEASE PLAYSTATE %d",priv->playState);
			}
		}
		priv->seekPos = clutter_actor_get_x (priv->playPauseGroup);
		clutter_actor_set_clip(priv->fillBarRect, 0, 0, priv->seekPos, clutter_actor_get_height(priv->fillBarRect) );
		thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseBackground),pPlayNormal, 0,0,FALSE,FALSE);

	}

	MILDENHALL_PROGRESS_BAR_PRINT("\n***************** ON BUTTON RELEASE END *********************** \n");
	return TRUE;
}

static void on_drag_begin (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,ClutterModifierType modifiers,gpointer user_data)
{
        MildenhallProgressBar *progressBar = MILDENHALL_PROGRESS_BAR (user_data);
	MILDENHALL_PROGRESS_BAR_PRINT("\n**************On Drag Begin*******************\n");
	progressBar->priv->isDragMotion = TRUE;
	progressBar->priv->seekPos = clutter_actor_get_x (progressBar->priv->playPauseGroup);
	g_signal_emit_by_name(progressBar,"seek-start",progressBar->priv->seekPos);

}

static void on_drag_end (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,ClutterModifierType  modifiers,gpointer user_data)
{
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(user_data);
	gdouble offset;
	gfloat progressBarWidth,position;
	gdouble seekButtonSize;
	gchar *pPlayNormal = NULL;
	g_object_get(progressBar,"offset",&offset,NULL);
	g_object_get(progressBar,"width",&progressBarWidth,NULL);
	g_object_get(progressBar,"seek-button-size",&seekButtonSize,NULL);
	pPlayNormal = g_hash_table_lookup(progressBar->priv->pLocalHash, MILDENHALL_BG_PLAY_NORMAL);
	progressBar->priv->seekPos = clutter_actor_get_x (progressBar->priv->playPauseGroup);
	position = progressBar->priv->seekPos/(progressBarWidth-offset-seekButtonSize);
	g_signal_emit_by_name(progressBar,"seek-end",position);

	thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(progressBar->priv->playPauseBackground),pPlayNormal, 0,0,FALSE,FALSE);
	MILDENHALL_PROGRESS_BAR_PRINT("\n**********************On Drag End************************* \n");
}

static void on_drag_motion (ClutterDragAction *action,ClutterActor *actor,gfloat event_x,gfloat event_y,gpointer user_data)
{
        MildenhallProgressBar *progressBar = MILDENHALL_PROGRESS_BAR (user_data);
	MILDENHALL_PROGRESS_BAR_PRINT("\nOn Drag Motion \n");
	progressBar->priv->seekPos = clutter_actor_get_x (progressBar->priv->playPauseGroup);
	clutter_actor_set_clip(progressBar->priv->fillBarRect, 0, 0, progressBar->priv->seekPos, clutter_actor_get_height(progressBar->priv->fillBarRect) );
}

static void mildenhall_progress_bar_parse_style(MildenhallProgressBar *self)
{
	self->priv = PROGRESS_BAR_PRIVATE (self);
	if(NULL != self->priv->pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init(&iter, self->priv->pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				self->priv->pLocalHash = g_hash_table_new_full (g_int_hash, g_str_equal, g_free, g_free);
				g_hash_table_foreach(pHash, v_progress_bar_parse_style, self);
			}
		}
	}
}

static void mildenhall_progress_bar_init (MildenhallProgressBar *self)
{
        const char *pEnvString;
	/* check for env to enable traces */
	self->priv = PROGRESS_BAR_PRIVATE (self);
	pEnvString = g_getenv ("MILDENHALL_PROGRESS_BAR_DEBUG");
	if (pEnvString != NULL)
	{
		mildenhall_progress_bar_debug_flags = g_parse_debug_string (pEnvString, mildenhall_progress_bar_debug_keys, G_N_ELEMENTS (mildenhall_progress_bar_debug_keys));
		MILDENHALL_PROGRESS_BAR_PRINT ("MILDENHALL_PROGRESS_BAR_PRINT: env_string %s %ld %d \n", pEnvString, G_LIKELY (MILDENHALL_PROGRESS_BAR_HAS_DEBUG), mildenhall_progress_bar_debug_flags);

	}
	self->priv->pStyleHash = thornbury_style_set(PKGDATADIR"/mildenhall_progress_bar_style.json");
	MILDENHALL_PROGRESS_BAR_PRINT("\n*****************MILDENHALL PROGRESS BAR NEW INIT START********************* \n");
	mildenhall_progress_bar_parse_style(self);

	thornbury_style_free(self->priv->pStyleHash);
	create_progress_bar(self);
	g_signal_connect (self, "notify::current-duration", G_CALLBACK (set_progress_bar_position), self);
	g_signal_connect (self, "notify::reactive", G_CALLBACK (update_progress_bar), self);
	g_signal_connect (self, "notify::width", G_CALLBACK (progress_bar_reset_width), self);
	g_signal_connect (self, "notify::offset", G_CALLBACK (progress_bar_reset_width), self);
	g_signal_connect (self, "notify::buffer-fill", G_CALLBACK (update_buffer_fill), self);
	g_signal_connect(self,"notify::play-state",G_CALLBACK(update_play_state),self);
	g_signal_connect(self,"notify::seek-button-size",G_CALLBACK(update_draggable_action),self);
	g_signal_connect(self,"notify::opacity",G_CALLBACK(progress_bar_reset_opacity),self);

	MILDENHALL_PROGRESS_BAR_PRINT("\n*****************MILDENHALL PROGRESS BAR NEW INIT START********************* \n");

	self->priv->isDragMotion=FALSE;

}
static void progress_bar_reset_opacity(GObject *progressB, GParamSpec *pspec,gpointer data)
{
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(data);
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(data)->priv;

	gint progressBarOpacity;
	g_object_get(progressBar,"opacity",&progressBarOpacity,NULL);
	g_print("\n OPAC = %d",progressBarOpacity);
	clutter_actor_set_opacity(CLUTTER_ACTOR(priv->seamlessTexture),progressBarOpacity);
}
void update_draggable_action(GObject *progressB, GParamSpec *pspec,gpointer pUserData)
{
	MildenhallProgressBar *progressBar = MILDENHALL_PROGRESS_BAR (pUserData);
        MildenhallProgressBarPrivate *priv = PROGRESS_BAR_PRIVATE (progressBar);

	gfloat progressBarWidth;
	gdouble seekButtonSize;
	gdouble progressOffset;
        ClutterRect rect;

	g_object_get(progressBar,"seek-button-size",&seekButtonSize,NULL);
	g_object_get(progressBar,"offset",&progressOffset,NULL);
	g_object_get(progressBar,"width",&progressBarWidth,NULL);
	g_print("IN UPDATE DRAGGABLE seek button size is = %lf \n",seekButtonSize);

	//Set the Draggable area.
	rect.origin.x = 0.0;
	rect.origin.y = 0.0;
	rect.size.width = (progressBarWidth- progressOffset)-seekButtonSize ;
	rect.size.height = 50;
	g_object_set(priv->action,"drag-area",&rect,NULL);

}
static gboolean on_gesture_end(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	MildenhallProgressBarPrivate *priv = PROGRESS_BAR_PRIVATE (MILDENHALL_PROGRESS_BAR (userData));
        gchar *pPlayNormal = NULL;
	MILDENHALL_PROGRESS_BAR_PRINT("ON GESTURE END");
	pPlayNormal = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_BG_PLAY_NORMAL);
	thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseBackground),pPlayNormal, 0,0,FALSE,FALSE);
	return TRUE;

}
void update_play_state(GObject *progressB, GParamSpec *pspec,gpointer pUserData)
{
	gboolean playState;
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(pUserData);
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(pUserData)->priv;

	gchar *pIconPlay = NULL;
	gchar *pIconPause = NULL;
	pIconPlay = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PLAY);
	pIconPause = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PAUSE);



	g_object_get(progressBar,"play-state",&playState,NULL);
	priv->playState=playState;
	if(priv->playState)
	{
		thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseButton),pIconPlay, 0,0,FALSE,FALSE);
		MILDENHALL_PROGRESS_BAR_PRINT("\n IN NOTIFY FUNCTION %d \n",priv->playState);
	}
	else
	{
		thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseButton),pIconPause, 0,0,FALSE,FALSE);
		MILDENHALL_PROGRESS_BAR_PRINT("\n IN NOTIFY FUNCTION %d \n",priv->playState);
	}



}

/********************************************************
 * Function : v_progress_bar_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_progress_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(pUserData);
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(pUserData)->priv;
	// store the style images in a hash for setting syle based on set properties

	if(g_strcmp0(pStyleKey, "rolls-normal-seamless") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_ROLLS_NORMAL_SEAMLESS, pValue);
	else if(g_strcmp0(pStyleKey, "bg-play-normal") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_BG_PLAY_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "bg-play-pressed") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_BG_PLAY_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "icon-play") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_ICON_PLAY, pValue);
	else if(g_strcmp0(pStyleKey, "radio-stop-ac") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_RADIO_STOP_AC, pValue);
	else if(g_strcmp0(pStyleKey, "icon-pause") == 0)
		v_progress_bar_add_style_to_hash(progressBar, MILDENHALL_ICON_PAUSE, pValue);
	else if(g_strcmp0(pStyleKey, "fill-bar-color") == 0)
		clutter_color_from_string(&priv->fillBarColor,  g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "slider-color") == 0)
		clutter_color_from_string(&priv->sliderColor,  g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "line-color") == 0)
		clutter_color_from_string(&priv->lineColor,  g_value_get_string(pValue));
	else
	{
		;	// do nothing
	}

	if(pStyleKey)
		g_free(pStyleKey);
}

/********************************************************
 * Function : v_progress_bar_add_style_to_hash
 * Description: maintain style hash for bg state and multi state
 * Parameter :  *pButtonDrawer, *pKey, pValue
 * Return value: void
 ********************************************************/
static void
v_progress_bar_add_style_to_hash (MildenhallProgressBar *progressBar, const gchar *pKey, gpointer pValue)
{
	if(!MILDENHALL_IS_PROGRESS_BAR(progressBar))
	{
		g_warning("invalid progress bar object\n");
		return;
	}

	if(NULL != pKey || NULL != pValue)
	{
		MildenhallProgressBarPrivate *priv = progressBar->priv;
		const gchar *pFilePath = NULL;
		pFilePath = g_value_get_string(pValue);
		// maintain key and value pair for style name and releated image path
		g_hash_table_insert (priv->pLocalHash, g_strdup (pKey), g_strdup_printf (PKGTHEMEDIR"/%s", pFilePath));
	}
}

void progress_bar_reset_width(GObject *progressB, GParamSpec *pspec,gpointer data)
{
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(data);
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(data)->priv;

	gfloat progressBarWidth;
	gfloat currentDuration;
	gdouble seekButtonSize;
	gfloat bufferPos;
	gdouble progressOffset;
        ClutterRect rect;

	g_object_get(progressBar,"seek-button-size",&seekButtonSize,NULL);
	g_object_get(progressBar,"offset",&progressOffset,NULL);
	g_object_get(progressBar,"width",&progressBarWidth,NULL);
	g_object_get(progressBar,"current-duration",&currentDuration,NULL);
	g_object_get(progressBar,"buffer-fill",&bufferPos,NULL);
	MILDENHALL_PROGRESS_BAR_PRINT("\n After Chaning to 0.4 %f",progressBarWidth);
	//g_object_get(progressBar,"seek-button-size",&seekButtonSize,NULL);
	//	g_print("\n \n In progress_bar_reset_width \n Width %f \n Offset %lf \n SeekButtonSize %lf \n finish \n",progressBarWidth,progressOffset,seekButtonSize);


	//Set the Draggable area.
	rect.origin.x = 0.0;
	rect.origin.y = 0.0;
	rect.size.width = (progressBarWidth- progressOffset)-seekButtonSize ;
	rect.size.height = 50;
	g_object_set(priv->action,"drag-area",&rect,NULL);


	clutter_actor_set_size (priv->seamlessTexture,progressBarWidth, 64);
	clutter_actor_set_size (priv->upperLine, progressBarWidth, 1);
	clutter_actor_set_size(priv->sliderRect, progressBarWidth - progressOffset, 2);
	clutter_actor_set_position(priv->sliderRect, 10, 32);
	clutter_actor_set_size(priv->fillBarRect,progressBarWidth - progressOffset, 2);
	clutter_actor_set_position(priv->fillBarRect, 10, 32);
	clutter_actor_set_clip(priv->fillBarRect, 0, 0, clutter_actor_get_height(priv->fillBarRect), 0);
	clutter_actor_set_size(priv->bufferRect, progressBarWidth - progressOffset, 2);
	clutter_actor_set_position(priv->bufferRect, 10, 32);
	clutter_actor_set_clip(priv->bufferRect, 0, 0, clutter_actor_get_height(priv->bufferRect), 0);
	clutter_actor_set_size(priv->playPauseBackground, 64, 64);
	clutter_actor_set_clip(priv->fillBarRect, 0, 0, currentDuration, clutter_actor_get_height(priv->fillBarRect) );
	clutter_actor_set_clip(priv->bufferRect, 0, 0, bufferPos, clutter_actor_get_height(priv->fillBarRect) );
	clutter_actor_set_x (priv->playPauseGroup, currentDuration);
}

void update_buffer_fill(GObject *progressB, GParamSpec *pspec,gpointer data)
{
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(data)->priv;
	gdouble bufferPos;
	g_object_get(MILDENHALL_PROGRESS_BAR(data),"buffer-fill",&bufferPos,NULL);
	clutter_actor_set_clip(priv->bufferRect, 0, 0, bufferPos, clutter_actor_get_height(priv->bufferRect) );
}


void update_progress_bar(GObject *progressB, GParamSpec *pspec,gpointer data)
{

	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(data);
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(data)->priv;
	gboolean reactive;
	//gchar *pIconPlay = NULL;
	//gchar *pIconPause = NULL;
	gchar *pRadioStopAC = NULL;
	//LookUp in the hash table for png.
	//	pIconPlay = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PLAY);
	//pIconPause = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_ICON_PAUSE);
	pRadioStopAC = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_RADIO_STOP_AC);
	g_object_get(progressBar,"reactive",&reactive,NULL);
	if (reactive)
	{
		//If it is reactive then connect the signals.Otherwise the playPauseGroup is non-reactive.
		priv->action = lightwood_progress_base_get_drag_action (LIGHTWOOD_PROGRESS_BASE (progressBar));
		//FIXME:Connect touch event signal bp and br signals to same callback.And in that callback call the corresponding bp or br callbacks.
		g_signal_connect (priv->playPauseGroup, "button-release-event", G_CALLBACK(touch_event), progressBar);
		g_signal_connect (priv->playPauseGroup, "button-press-event", G_CALLBACK(touch_event), progressBar);
		g_signal_connect (priv->playPauseGroup, "touch-event", G_CALLBACK(touch_event), progressBar);
		g_signal_connect (priv->action, "drag-begin", G_CALLBACK (on_drag_begin), progressBar);
		g_signal_connect (priv->action, "drag-end", G_CALLBACK (on_drag_end), progressBar);
		g_signal_connect (priv->action, "drag-motion", G_CALLBACK (on_drag_motion), progressBar);

	}
	else
	{
		thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseButton),pRadioStopAC, 0,0,FALSE,FALSE);
	}

}

static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	//MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(userData)->priv;
	if (event->type == CLUTTER_BUTTON_PRESS || event->type == CLUTTER_TOUCH_BEGIN)
		on_button_press (actor, event, userData);
	else if (event->type == CLUTTER_BUTTON_RELEASE || event->type == CLUTTER_TOUCH_END)
		on_button_release (actor, event, userData);
	else
	{
		return FALSE;
	}
	return TRUE;
}


static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(userData)->priv;
	gchar *pPlayPressed = NULL;
	pPlayPressed = g_hash_table_lookup(priv->pLocalHash, MILDENHALL_BG_PLAY_PRESSED);
	thornbury_ui_texture_set_from_file(CLUTTER_ACTOR(priv->playPauseBackground),pPlayPressed, 0,0,FALSE,FALSE);	//When the playPauseButton is pressed update the playPauseGroup background.
	priv->isDragMotion=FALSE;	//If there is no drag motion set the isDragMotion flag to FALSE.
	return TRUE;
}


void set_progress_bar_position(GObject *progressBar, GParamSpec *pspec,gpointer data)
{
	MildenhallProgressBarPrivate *priv = MILDENHALL_PROGRESS_BAR(data)->priv;
	gdouble currentValue;
	g_object_get(MILDENHALL_PROGRESS_BAR(data),"current-duration",&currentValue,NULL);
	clutter_actor_set_x (priv->playPauseGroup, currentValue);
	priv->seekPos = clutter_actor_get_x (priv->playPauseGroup);	//Get the current position of the playPauseGroup and according to that fill the bar.
	clutter_actor_set_clip(priv->fillBarRect, 0, 0, priv->seekPos, clutter_actor_get_height(priv->fillBarRect) ); //Filling the bar.
	g_signal_emit_by_name(MILDENHALL_PROGRESS_BAR(data),"progress-updated",priv->seekPos);
}


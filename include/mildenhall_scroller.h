/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_scroller.h
 *
 *
 * mildenhall_scroller.h */

#ifndef _MILDENHALL_SCROLLER
#define _MILDENHALL_SCROLLER

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SCROLLER mildenhall_scroller_get_type()

#define MILDENHALL_SCROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MILDENHALL_TYPE_SCROLLER, MildenhallScroller))

#define MILDENHALL_SCROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MILDENHALL_TYPE_SCROLLER, MildenhallScrollerClass))

#define MILDENHALL_IS_SCROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MILDENHALL_TYPE_SCROLLER))

#define MILDENHALL_IS_SCROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MILDENHALL_TYPE_SCROLLER))

#define MILDENHALL_SCROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MILDENHALL_TYPE_SCROLLER, MildenhallScrollerClass))

typedef struct _MildenhallScrollerPrivate MildenhallScrollerPrivate;

/**
 * MILDENHALL_SCROLLER_TYPE:
 * @MILDENHALL_SCROLLER_2L: class handler for the #MILDENHALL_SCROLLER_TYPE::MILDENHALL_SCROLLER_2L for 2line scroller
 * @MILDENHALL_SCROLLER_3L: class handler for the #MILDENHALL_SCROLLER_TYPE::MILDENHALL_SCROLLER_3L for 2line scroller
 *
 * The #MILDENHALL_SCROLLER_TYPE struct contains only private data.
 *
 */
typedef enum
{
   MILDENHALL_SCROLLER_2L ,
   MILDENHALL_SCROLLER_3L
}  MILDENHALL_SCROLLER_TYPE;


/**
 * MildenhallScroller:
 *
 * The #MildenhallScroller struct contains only private data.
 *
 */
typedef struct
{
   ClutterActor parent;
   MildenhallScrollerPrivate *priv;
}  MildenhallScroller;


/**
 * MildenhallScrollerClass:
 * @scroller_up_cb: class handler for the #MildenhallScrollerClass::scroller_up_cb signal
 * @scroller_down_cb: class handler for the #MildenhallScrollerClass::scroller_down_cb signal
 *
 * The #MildenhallScrollerClass struct contains only private data.
 *
 */
typedef struct
{
   ClutterActorClass parent_class;
   gboolean (*scroller_up_cb)(MildenhallScroller *roller, gpointer user_data);
   gboolean (*scroller_down_cb)(MildenhallScroller *roller, gpointer user_data);
} MildenhallScrollerClass;

GType mildenhall_scroller_get_type (void);

ClutterActor *mildenhall_scroller_new (MILDENHALL_SCROLLER_TYPE scroller_type);

G_END_DECLS

#endif /* _MILDENHALL_SCROLLER */

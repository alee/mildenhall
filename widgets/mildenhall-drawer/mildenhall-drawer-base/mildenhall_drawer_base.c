/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_drawer_base
 * @title: MildenhallDrawerBase
 * @short_description: provides a swipe-based side panel to provide a
 * central location for navigation.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #MildenhallViewsDrawer, #MildenhallNaviDrawer
 *
 * #MildenhallDrawerBase widget can be positioned at any of the four edges of
 * the content item. The drawer is then opened by "dragging" it from edges of
 * the window.
 * In general, it can be used as views drawer/menu's drawer.
 * Both views drawer and menu's drawer extends #MildenhallDrawerBase but only differs in the
 * way it is positioned in the stage. (views drawer is positioned at the top most right side
 * whereas menu's drawer is positoned at right most center).
 * In addition, It is also possible for the application to show the tool-tip by providing
 * the tooltip-text through #ThronburyModel
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * How to create #MildenhallDrawerBase
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * ClutterActor *drawer = NULL;
 * ThornburyModel *model = NULL;
 *
 * drawer = mildenhall_drawer_base_new ();
 * model = (ThornburyModel*) thornbury_list_model_new (COLUMN_LAST,
 * 						      G_TYPE_STRING, NULL,
 * 						      G_TYPE_STRING, NULL,
 * 						      G_TYPE_STRING, NULL,
 * 						      G_TYPE_BOOLEAN, NULL, -1);
 *
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "MUSIC",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/test-drawer-base/icon_music_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "MUSIC",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "ARTISTS",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/test-drawer-base/icon_music_artists_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "ARTIST",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "INTERNET",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/test-drawer-base/icon_internet_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "INTERNET",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "ALBUMS",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/test-drawer-base/icon_music_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "ALBUMS",
 *                        COLUMN_REACTIVE, TRUE, -1);
 *
 * g_object_set (drawer, "model", model, NULL);
 *
 * g_signal_connect (drawer, "drawer-open", G_CALLBACK (drawer_opened),
 *                  NULL);
 * g_signal_connect (drawer, "drawer-close", G_CALLBACK (drawer_closed),
 *                  NULL);
 * g_signal_connect (drawer, "drawer-button-released",
 *                  G_CALLBACK (drawer_button_released), NULL);
 * g_signal_connect (drawer, "drawer-tooltip-hidden",
 *                  G_CALLBACK (tooltip_hidden), NULL);
 * g_signal_connect (drawer, "drawer-tooltip-shown",
 *                  G_CALLBACK (tooltip_shown), NULL);
 *
 * clutter_actor_add_child (stage, drawer);
 *
 * ]|
 *
 * Example of #MildenhallDrawerBase::drawer-open,  #MildenhallDrawerBase::drawer-close,
 * #MildenhallDrawerBase::drawer-button-released,  #MildenhallDrawerBase::drawer-tooltip-hidden,
 * #MildenhallDrawerBase::drawer-tooltip-shown callback implementations.
 *
 * |[<!-- language="C" -->
 * static void drawer_opened (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 * }
 *
 * static void drawer_closed (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s\n", __FUNCTION__);
 * }
 *
 * static void tooltip_hidden (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s\n", __FUNCTION__);
 * }
 *
 * static void tooltip_shown (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s\n", __FUNCTION__);
 * }
 *
 * static void drawer_button_released (GObject *actor, gchar *name, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s\n", __FUNCTION__);
 *   g_debug ("DRAWER_WIDGET: button released = %s\n", name);
 * }
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include "mildenhall_drawer_base.h"
#include "mildenhall-internal.h"

typedef enum _enDrawerBaseProperty enDrawerBaseProperty;
typedef struct _MildenhallDrawerBasePrivate MildenhallDrawerBasePrivate;
typedef enum _enDrawerBaseSignals enDrawerBaseSignals;
typedef enum _enDrawerBaseDebugFlag enDrawerBaseDebugFlag;

/* Set the environment variable in terminal to enable traces: export DRAWER_BASE_DEBUG=drawer-base */
enum _enDrawerBaseDebugFlag
{
        MILDENHALL_DRAWER_BASE_DEBUG = 1 << 0,

};

/* drawer base property enums */
enum _enDrawerBaseProperty
{
        PROP_FIRST,
	PROP_X,
	PROP_Y,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_TYPE,
	PROP_MODEL,
	PROP_LANG,
	PROP_TOOLTIP_WIDTH,
	PROP_CLOSE
};

/* The signals emitted by the mildenhall drawer base */
enum _enDrawerSignals
{
        SIG_FIRST,
        SIG_DRAWER_CLOSE,
        SIG_DRAWER_OPEN,
        SIG_TOOLTIP_SHOWN,
        SIG_TOOLTIP_HIDDEN,
	SIG_BUTTON_RELEASED,
        SIG_LAST
};

/* button model format */
enum
{
        BUTTON_COLUMN_ICON,
        BUTTON_COLUMN_TOOLTIP_TEXT,
        BUTTON_COLUMN_LAST
};


/* Storage for the signals */
static guint32 mildenhall_drawer_base_signals[SIG_LAST] = {0,};

/* debug flag setting */
guint  mildenhall_drawer_base_debug_flags = 0;

static const GDebugKey mildenhall_drawer_base_debug_keys[] =
{
        { "mildenhall-drawer-base",   MILDENHALL_DRAWER_BASE_DEBUG }
};

#define MILDENHALL_DRAWER_BASE_HAS_DEBUG               ((mildenhall_drawer_base_debug_flags ) & 1)
#define MILDENHALL_DRAWER_BASE_PRINT( a ...) \
        if (G_LIKELY (MILDENHALL_DRAWER_BASE_HAS_DEBUG )) \
        {                               \
                g_print(a);           \
        }



/* drawer type as GType */
static GType drawer_type_get_type (void) G_GNUC_CONST;
#define MILDENHALL_DRAWER_TYPE (drawer_type_get_type())

G_DEFINE_TYPE (MildenhallDrawerBase, mildenhall_drawer_base, CLUTTER_TYPE_ACTOR)

#define MILDENHALL_DRAWER_BASE_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_DRAWER_TYPE_BASE, MildenhallDrawerBasePrivate))


struct _MildenhallDrawerBasePrivate
{
	gfloat flWidth;
	gfloat flHeight;
	gfloat flX;
	gfloat flY;
	gfloat flButtonGroupHeight;
	gfloat flBottomRectHeight;
	gfloat flMax;
	//gboolean bCloseState;
	gboolean bDrawerExpanded;
	gint inAnimationDuration;
	gint inButtonOffset;
	gchar *pViewsBottomImagePath;
	gchar *pLanguage;
	enMildenhallDrawerType enType;
	ThornburyModel *pDrawerModel;

	ClutterActor *pDrawerGroup;	/* drawer group contains default butoon + buttongroup */
	ClutterActor *pDefaultButton;	/* first button of the drawer */
	ClutterActor *pButtonGroup;	/* button group having buttons except default button (first button) */
	ClutterActor *pDrawerShadow;	/* bottom shadow image behind default button of views drawer */
	ClutterActor *pBottomRect;	/* black bottom rect for navi/menu drawer to hide bottom animation*/
	ClutterColor bottomRectColor;
	//ClutterAnimation *pSlideAnimation;
	ClutterTransition *pSlideTrans;

	//GHashTable *pButtonHash;
	GList *pButtonList;
};

/* internal functions */
static void v_drawer_base_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, ClutterActor *pDrawer);
static void v_drawer_base_create_view(MildenhallDrawerBase *pDrawerBase);
static void v_drawer_base_create_default_button(MildenhallDrawerBase *pDrawerBase, const gchar *pName, const gchar *pIcon, const gchar *pTooltip, gboolean bReactive, enBackgroundState enBgState);
static void v_drawer_base_set_style(MildenhallDrawerBase *pDrawerBase);
static void v_drawer_base_create_bottom_shadow(MildenhallDrawerBase *pDrawerBase);
static void v_drawer_base_slide_down(MildenhallDrawerBase *pDrawerBase);
static void v_drawer_base_sync_tooltip_width(MildenhallDrawerBase *pDrawerBase);
static void clear_button_list(GList *pList);
void mildenhall_drawer_base_set_close(MildenhallDrawerBase *pDrawerBase, gboolean bCloseState);
void mildenhall_drawer_base_set_language(MildenhallDrawerBase *pDrawerBase, gchar *pLanguage);

/**
 * mildenhall_drawer_base_get_x:
 * @pObject : drawer object reference
 *
 * Function to get the drawer x position
 *
 * Returns: x coordinate of the drawer
 */
gfloat mildenhall_drawer_base_get_x(MildenhallDrawerBase *pDrawerBase)
{
        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return clutter_actor_get_x(CLUTTER_ACTOR(pDrawerBase) );

}

/**
 * mildenhall_drawer_base_get_y:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the drawer y position
 *
 * Returns: y coordinate of the drawer
 */
gfloat mildenhall_drawer_base_get_y(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return priv->flY;
}

/**
 * mildenhall_drawer_base_get_width:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the drawer width
 *
 * Returns: width of the drawer
 */
gfloat mildenhall_drawer_base_get_width(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return priv->flWidth + priv->flMax;

}

/**
 * mildenhall_drawer_base_get_height:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the drawer height
 *
 * Returns: height of the drawer
 */
gfloat mildenhall_drawer_base_get_height(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

	/* TBD: calculate the height */
	return clutter_actor_get_height(CLUTTER_ACTOR(pDrawerBase) ) + priv->flButtonGroupHeight;
}

/**
 * mildenhall_drawer_base_get_close:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the whether the drawer is close or open
 *
 * Returns: close/open state of the drawer
 */
gboolean mildenhall_drawer_base_get_close(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return priv->bDrawerExpanded;//priv->bCloseState;
}

/**
 * mildenhall_drawer_base_get_drawer_type:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the drawer type as enum :
 *			MILDENHALL_CONTEXT_DRAWER
 *			MILDENHALL_VIEWS_DRAWER
 *			MILDENHALL_NAVI_DRAWER
 *
 * Returns: type of the drawer
 */
enMildenhallDrawerType mildenhall_drawer_base_get_drawer_type(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	 if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return priv->enType;
}

/**
 * mildenhall_drawer_base_get_tooltip_width:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the tooltip width
 *
 * Returns: tooltip width
 */
gfloat mildenhall_drawer_base_get_tooltip_width(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return -1;
        }

        return priv->flMax;
}

/**
 * mildenhall_drawer_base_get_model:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the drawer model
 *
 * Returns: model of the drawer
 */
ThornburyModel* mildenhall_drawer_base_get_model(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	 if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return NULL;
        }

        return priv->pDrawerModel;


}

/**
 * mildenhall_drawer_base_set_model:
 * @pDrawerBase: drawer object reference
 * @pModel: model for the drawer base
 *
 * Model contains the data for the drawer buttons.
 * Drawer model each row contains:
 *                      1. button ID (name)
 *                      2. icon image path
 *                      3. tooltip text
 *                      4. reactive
 * If button has multi state, there should be more than one row
 * with same button ID.
 */
void mildenhall_drawer_base_set_model(MildenhallDrawerBase *pDrawerBase, ThornburyModel *pModel)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }
        /* if model is set again, remove previous model */
        if(NULL != priv->pDrawerModel)
        {
# if 0
                g_signal_handlers_disconnect_by_func (priv->pModel,
                                G_CALLBACK (v_drawer_base_row_added_cb),
                                pObject);
                g_signal_handlers_disconnect_by_func (priv->pModel,
                                G_CALLBACK (v_drawer_base_row_removed_cb),
                                pObject);
# endif
                g_signal_handlers_disconnect_by_func (priv->pDrawerModel,
                                G_CALLBACK (v_drawer_base_row_changed_cb),
                                pDrawerBase);
                g_object_unref (priv->pDrawerModel);

                priv->pDrawerModel = NULL;

        }
	/* update the new model with signals */
        if (pModel != NULL)
        {
                g_return_if_fail (G_IS_OBJECT (pModel));

                priv->pDrawerModel = g_object_ref (pModel);
# if 0
                g_signal_connect (priv->pDrawerModel,
                    "row-added",
                    G_CALLBACK (v_drawer_base_row_added_cb),
                    pObject);
                g_signal_connect (priv->pDrawerModel,
                    "row-removed",
                    G_CALLBACK (v_drawer_base_row_removed_cb),
                    pObject);
#endif
                g_signal_connect (priv->pDrawerModel,
                    "row-changed",
                    G_CALLBACK (v_drawer_base_row_changed_cb),
                    pDrawerBase);
        }

	/* close the drawer */
	mildenhall_drawer_base_set_close(pDrawerBase, TRUE);

	//if(NULL != priv->pButtonList)
	{
		clear_button_list(priv->pButtonList);
		priv->pButtonList = NULL;;
	}

	priv->flButtonGroupHeight = 0.0;

	//priv->pSlideAnimation = NULL;
	 if(priv->pSlideTrans != NULL)
	 {
		 clutter_actor_remove_all_transitions(priv->pButtonGroup);
		 priv->pSlideTrans = NULL;
	 }

	/*if(priv->pSlideTrans != NULL)
        {
                clutter_transition_group_remove_all(CLUTTER_TRANSITION_GROUP(priv->pSlideTrans));
                clutter_timeline_stop(CLUTTER_TIMELINE(priv->pSlideTrans));
		g_object_unref(priv->pSlideTrans);
                priv->pSlideTrans = NULL;
        }*/

	/* create drawer */
	v_drawer_base_create_view(pDrawerBase);

	/* update language */
	mildenhall_drawer_base_set_language(pDrawerBase, g_strdup(priv->pLanguage));
	/* sync the tooltip width to max */
	//v_drawer_base_sync_tooltip_width(pDrawerBase);

	/* notify for model property */
	g_object_notify (G_OBJECT(pDrawerBase), "model");
}



/**
 * mildenhall_drawer_base_set_close:
 * @pDrawerBase: drawer object reference
 * @bCloseState: TRUE if the drawer to be closed, FALSE otherwise
 *
 * If button has multi state, there should be more than one row
 * with same button ID.
 */
void mildenhall_drawer_base_set_close(MildenhallDrawerBase *pDrawerBase, gboolean bCloseState)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	/* if drawer is opened, close it */
	if(priv->bDrawerExpanded)
	{
		v_drawer_base_slide_down(pDrawerBase);
	}

	g_object_notify (G_OBJECT(pDrawerBase), "close");
}


/**
 * mildenhall_drawer_base_set_drawer_type:
 * @pDrawerBase: drawer object reference
 * @enType: type for the drawer
 *
 * Type of the drawer to be created:
 *			MILDENHALL_CONTEXT_DRAWER
 *                      MILDENHALL_VIEWS_DRAWER
 *                      MILDENHALL_NAVI_DRAWER
 */
void mildenhall_drawer_base_set_drawer_type(MildenhallDrawerBase *pDrawerBase, enMildenhallDrawerType enType)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(enType == MILDENHALL_CONTEXT_DRAWER ||
			enType == MILDENHALL_VIEWS_DRAWER ||
			enType == MILDENHALL_NAVI_DRAWER)
	{

		if(enType != priv->enType)
		{
			priv->enType = enType;
			/* update style */
			v_drawer_base_set_style(pDrawerBase);
		}
	}
	else
	{
		g_warning("invalid drawer type\n");
		return;
	}

	g_object_notify (G_OBJECT(pDrawerBase), "type");
}

/**
 * mildenhall_drawer_base_set_language:
 * @pDrawerBase: drawer object reference
 * @pLanguage: language to be set
 *
 * Function to update the text on set language for all drawer buttons
 *
 * Returns: language to be set
 */
void mildenhall_drawer_base_set_language(MildenhallDrawerBase *pDrawerBase, gchar *pLanguage)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        if(! MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(NULL == pLanguage)
		return;

        if(NULL != priv->pLanguage)
        {
                g_free(priv->pLanguage);
                priv->pLanguage = NULL;
        }
        /* update language */
        priv->pLanguage = g_strdup(pLanguage);

	if(NULL != priv->pButtonList)
        {
		GList *pIter = NULL;
                pIter = g_list_first(priv->pButtonList);

                while(pIter && pIter->data)
                {
                        g_object_set(MILDENHALL_BUTTON_DRAWER(pIter->data), "language", priv->pLanguage, NULL);
                        pIter = g_list_next(pIter);
                }
	}
}


/**
 * mildenhall_drawer_base_new:
 *
 * Creates a drawer base object
 *
 * Returns: (transfer full): Mildenhall Drawer base object
 */
ClutterActor* mildenhall_drawer_base_new (void)
{
	return g_object_new (MILDENHALL_DRAWER_TYPE_BASE, NULL);
}

/**
 * mildenhall_drawer_base_get_default_button:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the default button pointer
 *
 * Returns: first button of the drawer
 */
ClutterActor* mildenhall_drawer_base_get_default_button (MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return NULL;
        }

        return priv->pDefaultButton;
}

/**
 * mildenhall_drawer_base_get_button_list:
 * @pDrawerBase: drawer object reference
 *
 * Function to get the list of drawer buttons
 *
 * Returns: list od drawer  buttons
 */
GList* mildenhall_drawer_base_get_button_list (MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return NULL;
        }

        return priv->pButtonList;
}

/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function : mildenhall_drawer_base_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
	MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pObject);
	if(!MILDENHALL_DRAWER_IS_BASE(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	switch (uinPropertyID)
        {
		case PROP_X:
                        g_value_set_float (pValue, mildenhall_drawer_base_get_x(pDrawerBase) );
                        break;
                case PROP_Y:
                        g_value_set_float (pValue, mildenhall_drawer_base_get_y(pDrawerBase) );
                        break;
                case PROP_WIDTH:
                        g_value_set_float (pValue, mildenhall_drawer_base_get_width(pDrawerBase) );
                        break;
                case PROP_HEIGHT:
			g_value_set_float (pValue, mildenhall_drawer_base_get_height(pDrawerBase) );
                        break;
		case PROP_CLOSE:
                        g_value_set_boolean (pValue, mildenhall_drawer_base_get_close(pDrawerBase) );
                        break;
		case PROP_MODEL:
                        g_value_set_object (pValue, mildenhall_drawer_base_get_model(pDrawerBase) );
                        break;
		case PROP_TYPE:
                        g_value_set_enum (pValue, mildenhall_drawer_base_get_drawer_type(pDrawerBase) );
                        break;
		case PROP_LANG:
                        //g_value_set_string (pButtonDrawer, priv->pLanguage);
			break;
		case PROP_TOOLTIP_WIDTH:
                        g_value_set_float (pValue, mildenhall_drawer_base_get_tooltip_width(pDrawerBase) );
                        break;
		default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_drawer_base_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pObject);
	if(!MILDENHALL_DRAWER_IS_BASE(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	switch (uinPropertyID)
        {
                case PROP_MODEL:
                        mildenhall_drawer_base_set_model(pDrawerBase, g_value_get_object (pValue) );
                        break;
		case PROP_CLOSE:
                        mildenhall_drawer_base_set_close(pDrawerBase, g_value_get_boolean (pValue) );
                        break;
		case PROP_TYPE:
                        mildenhall_drawer_base_set_drawer_type(pDrawerBase, g_value_get_enum (pValue) );
                        break;
		 case PROP_LANG:
                        mildenhall_drawer_base_set_language(pDrawerBase, (gchar *)g_value_get_string(pValue));
			break;
		default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_dispose
 * Description: Dispose the mildenhall drawer base object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_dispose (GObject *pObject)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE(pObject);

        if(NULL != priv)
        {
		if(NULL != priv->pButtonList)
		{
			clear_button_list(priv->pButtonList);
			priv->pButtonList = NULL;
		}
		if(NULL != priv->pViewsBottomImagePath)
		{
			g_free(priv->pViewsBottomImagePath);
			priv->pViewsBottomImagePath = NULL;
		}
		if(NULL != priv->pLanguage)
		{
			g_free(priv->pLanguage);
			priv->pLanguage = NULL;
		}
		if(NULL != priv->pDrawerModel)
		{
			g_object_unref (priv->pDrawerModel);
		        priv->pDrawerModel = NULL;
		}
		if(NULL != priv->pSlideTrans)
		{
			g_object_unref (priv->pSlideTrans);
			priv->pSlideTrans = NULL;
		}
	}
	G_OBJECT_CLASS (mildenhall_drawer_base_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : mildenhall_drawer_base_finalize
 * Description: Finalize the mildenhall drawer base object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_drawer_base_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_drawer_base_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_class_init (MildenhallDrawerBaseClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallDrawerBasePrivate));

	pObjectClass->get_property = mildenhall_drawer_base_get_property;
	pObjectClass->set_property = mildenhall_drawer_base_set_property;
	pObjectClass->dispose = mildenhall_drawer_base_dispose;
	pObjectClass->finalize = mildenhall_drawer_base_finalize;

#if 0
	/**
         *MildenhallDrawerBase:x:
         *
         * X coordinate of the button
         */
        pspec = g_param_spec_float ("x",
                        "X coordinate",
                        "x coordinate of the drawer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_X, pspec);

        /**
         * MildenhallDrawerBase:y:
         *
         * Y coordinate of the drawer
         */
        pspec = g_param_spec_float ("y",
                        "Y coordinate",
                        "x coordinate of the drawer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property ( pObjectClass, PROP_Y, pspec);
#endif
	/**
         * MildenhallDrawerBase:width:
         *
         * Width of the drawer
         */
        pspec = g_param_spec_float ("width",
                        "Width",
                        "Width of the drawer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

        /**
         * MildenhallDrawerBase:height:
         *
         * Height of the drawer
         */
        pspec = g_param_spec_float ("height",
                        "Height",
                        "Height of the drawer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	/**
         * MildenhallDrawerBase:model:
         *
    	 * Model contains the data for the drawer buttons.
  	 *
	 * Drawer model each row contains:
	 *                      1. button ID (name)
	 *                      2. icon image path
	 *                      3. tooltip text
	 *                      4. reactive
 	 *
	 * If button has multi state, there should be more than one row
	 * with same button ID.For creating model please refer #MildenhallDrawerBaseModelColumns
	 * 
         * Default: NULL
         */
        pspec = g_param_spec_object ("model",
                        "Model",
                        "model having drawer button states data",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

	 /**
         * MildenhallDrawerBase:position:
         *
         * To set the drawer position as any one of:
	 *		MILDENHALL_CONTEXT_DRAWER
     	 *		MILDENHALL_VIEWS_DRAWER
         *		MILDENHALL_NAVI_DRAWER
	 *
         * Default: MILDENHALL_CONTEXT_DRAWER
         */
        pspec = g_param_spec_enum ("type",
                        "DrawerType",
                        "Based on Drawer Type, position, background, tooltip-width will differ",
                        MILDENHALL_DRAWER_TYPE,
                        MILDENHALL_CONTEXT_DRAWER,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_TYPE, pspec);

        /**
         * MildenhallDrawerBase:close:
         *
         * To set the drawer close state as TRUE/FLASE
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("close",
                        "CloseState",
                        "Whether the drawer is closed",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_CLOSE, pspec);

	/**
         * MildenhallDrawerBase:language:
         *
         * sets language for all drawer buttons
         */
        pspec = g_param_spec_string ("language",
                        "language",
                        "lang change",
                        "en_US",
                        G_PARAM_READWRITE);

        g_object_class_install_property (pObjectClass, PROP_LANG, pspec);

	 /**
         * MildenhallDrawerBase:tooltip-width:
         *
         * Width of the tooltip box.
         */
        pspec = g_param_spec_float ("tooltip-width",
                        "TooltipWidth",
                        "Tooltip width",
                        1.0, G_MAXFLOAT,
                        100.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_TOOLTIP_WIDTH, pspec);

	 // Create drawer open signal for the drawer
        /**
         * MildenhallDrawerBase::drawer-open:
         * @mildenhallDrawerBase: The object which received the signal
         *
         * ::drawer-open is emitted when drawer is opened
         */
        mildenhall_drawer_base_signals[SIG_DRAWER_OPEN] = g_signal_new ("drawer-open",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallDrawerBaseClass, drawer_open),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	 // Create drawer close signal for the drawer
        /**
         * MildenhallDrawerBase::drawer-close:
         * @mildenhallDrawerBase: The object which received the signal
         *
         * ::drawer-close is emitted when drawer is closed
         */
        mildenhall_drawer_base_signals[SIG_DRAWER_CLOSE] = g_signal_new ("drawer-close",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallDrawerBaseClass, drawer_close),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	// Create a signal for the drawer to indicate tooltip shown
        /**
         * MildenhallDrawerBase::drawer-tooltip-shown:
         * @mildenhallDrawerBase: The object which received the signal
         *
         * ::drawer-tooltip-shown is emitted when drawer tooltip is shown
         */
        mildenhall_drawer_base_signals[SIG_TOOLTIP_SHOWN] = g_signal_new ("drawer-tooltip-shown",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallDrawerBaseClass, drawer_tooltip_shown),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

        // Create a signal for the drawer to indicate tooltip hide
        /**
         * MildenhallDrawerBase::drawer-tooltip-hidden:
         * @mildenhallDrawerBase: The object which received the signal
         *
         * ::drawer-tooltip-hidden is emitted when drawer tooltip is hidden
         */
        mildenhall_drawer_base_signals[SIG_TOOLTIP_HIDDEN] = g_signal_new ("drawer-tooltip-hidden",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallDrawerBaseClass, drawer_tooltip_hidden),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	// Create a signal for the drawer button release
        /**
         * MildenhallDrawerBase::drawer-button-released:
         * @mildenhallDrawer: The object which received the signal
         * @buttonIndex: pressed button index
         *
         * ::drawer-button-released is emitted when drawer button is released
         */
        mildenhall_drawer_base_signals[SIG_BUTTON_RELEASED] = g_signal_new ("drawer-button-released",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallDrawerBaseClass, drawer_button_released),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__STRING,
                        G_TYPE_NONE, 1, G_TYPE_STRING);


}

/********************************************************
 * Function : mildenhall_drawer_base_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_drawer_base_init (MildenhallDrawerBase *self)
{
	const char *pEnvString;
	self->priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (self);

	/* initialise the priv */
	self->priv->flWidth = 0.0;
        self->priv->flHeight = 0.0;
        self->priv->flX = 0.0;
        self->priv->flY = 0.0;
	self->priv->flButtonGroupHeight = 0.0;
	self->priv->inAnimationDuration = 0;
	self->priv->inButtonOffset = 0;
        //self->priv->bCloseState = TRUE;
        self->priv->enType = -1;//MILDENHALL_CONTEXT_DRAWER;
        self->priv->pDrawerModel = NULL;
	self->priv->pDrawerGroup = NULL;
	self->priv->pButtonGroup = NULL;
	self->priv->pDefaultButton = NULL;
	self->priv->pDrawerShadow = NULL;
	//self->priv->pSlideAnimation = NULL;
	self->priv->pSlideTrans = NULL;
	self->priv->bDrawerExpanded = FALSE;
	self->priv->pButtonList = NULL;
	self->priv->pBottomRect = NULL;
	self->priv->pViewsBottomImagePath = NULL;
	self->priv->pLanguage = NULL;

	 /* initialise the button group */
        self->priv->pDrawerGroup = clutter_actor_new ();
        clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pDrawerGroup);

	/* initialise the button hash */
	//self->priv->pButtonHash = g_hash_table_new(g_int_hash, g_str_equal);

	/* check for env to enable traces */
        pEnvString = g_getenv ("MILDENHALL_DRAWER_BASE_DEBUG");
        if (pEnvString != NULL)
        {
                mildenhall_drawer_base_debug_flags = g_parse_debug_string (pEnvString, mildenhall_drawer_base_debug_keys, G_N_ELEMENTS (mildenhall_drawer_base_debug_keys));
                MILDENHALL_DRAWER_BASE_PRINT ("MILDENHALL_DRAWER_BASE_PRINT: env_string %s %ld %d \n", pEnvString, G_LIKELY (MILDENHALL_DRAWER_BASE_HAS_DEBUG), mildenhall_drawer_base_debug_flags);

        }
}

/********************************************************
 * Function : drawer_type_get_type
 * Description: define drawer position enum as GType
 * Parameters: void
 * Return value: void
 ********************************************************/
static GType drawer_type_get_type (void)
{
        /* argument must point to a static 0-initialized variable,
         * that will be set to a value other than 0 at the end of
         * the initialization section.
         */
        static volatile gsize gEnumTypeVolatile = 0;

        /* drawer type as enum initialization section */
        if (g_once_init_enter (&gEnumTypeVolatile))
        {
                /* A structure which contains a single flags value, its name, and its nickname. */
                static const GEnumValue arValues[] =
                {
                        { MILDENHALL_CONTEXT_DRAWER, "MILDENHALL_CONTEXT_DRAWER", "context-drawer" },
                        { MILDENHALL_VIEWS_DRAWER, "MILDENHALL_VIEWS_DRAWER", "views-drawer" },
                        { MILDENHALL_NAVI_DRAWER, "MILDENHALL_NAVI_DRAWER", "navi-drawer" },
                        { 0, NULL, NULL }
                };
                GType gEnumType;

                /* Registers a new static flags type with the name name. */
                gEnumType = g_enum_register_static (g_intern_static_string ("enMildenhallDrawerType"), arValues);

                /* In combination with g_once_init_leave() and the unique address value_location,
                 * it can be ensured that an initialization section will be executed only once
                 * during a program's life time, and that concurrent threads are blocked
                 * until initialization completed.
                 */
                g_once_init_leave (&gEnumTypeVolatile, gEnumType);
        }

        return gEnumTypeVolatile;
}

/********************************************************
 * Function : v_drawer_base_sync_tooltip_width
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void clear_button_list(GList *pList)
{
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	if(NULL != pList)
	{
		GList *tempList = g_list_first(pList);
		while(tempList && tempList->data)
                {
			pList = g_list_remove_link (pList, tempList);
			if(MILDENHALL_IS_BUTTON_DRAWER(tempList->data))
			{
				clutter_actor_destroy(CLUTTER_ACTOR(tempList->data));
				tempList->data = NULL;
			}
                        g_list_free_1 (tempList);
                        tempList = g_list_first(pList);
		}
		pList = NULL;
	}
}

/********************************************************
 * Function : v_drawer_base_sync_tooltip_width
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_sync_tooltip_width(MildenhallDrawerBase *pDrawerBase)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

        if(NULL != priv->pButtonList)
        {
		gfloat flTooltipWidth = 0.0;
                GList *pIter = g_list_first (priv->pButtonList);
		priv->flMax = 0.0;

                while(pIter && pIter->data)
                {
			g_object_get(MILDENHALL_BUTTON_DRAWER(pIter->data), "tooltip-width", &flTooltipWidth, NULL);
			//g_print("width = %f\n", flTooltipWidth);
			if(flTooltipWidth > priv->flMax)
				priv->flMax = flTooltipWidth;
			pIter = g_list_next(pIter);
		}
		//g_print("%f\n", priv->flMax);

		pIter = g_list_first(priv->pButtonList);

                while(pIter && pIter->data)
                {
                        g_object_set(MILDENHALL_BUTTON_DRAWER(pIter->data), "tooltip-width", priv->flMax, NULL);
                        pIter = g_list_next(pIter);
                }

	}

}

/********************************************************
 * Function : v_drawer_base_set_tooltip
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_set_tooltip(MildenhallDrawerBase *pDrawerBase, gboolean bShowTooltip)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	v_drawer_base_sync_tooltip_width(pDrawerBase);

	if(bShowTooltip)
		g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_DRAWER_OPEN], 0, NULL);

	if(NULL != priv->pButtonList)
	{
		GList *pIter = g_list_first(priv->pButtonList);
		while(pIter && pIter->data)
		{
			g_object_set(MILDENHALL_BUTTON_DRAWER(pIter->data), "show-tooltip", bShowTooltip, NULL);

			pIter = g_list_next(pIter);
		}
	}


}

/********************************************************
 * Function : v_drawer_base_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_drawer_base_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        gchar *pStyleKey = NULL;
        MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pUserData);
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);

	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

		pStyleKey = g_strdup(pKey);

        /* store the style images in a hash for setting syle based on set properties */
        //g_print("style: name = %s\n", (gchar*)pKey);
	if(priv->enType == MILDENHALL_VIEWS_DRAWER)
	{
		if(g_strcmp0(pStyleKey, "views-drawer-bottom-shadow") == 0)
		{
			const gchar *pFile = g_value_get_string(pValue);
			if(NULL != pFile)
			{
				if(NULL != priv->pViewsBottomImagePath)
				{
					g_free(priv->pViewsBottomImagePath);
					priv->pViewsBottomImagePath = NULL;
				}
				priv->pViewsBottomImagePath = g_strdup_printf(PKGTHEMEDIR"/%s", pFile);
				MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: bottom shadow image = %s\n", priv->pViewsBottomImagePath);
			}
		}
	}
	else
	{
		/* bottom black rect patch for navi/menu drawer */
		if(g_strcmp0(pStyleKey, "drawer-bottom-rect-color") == 0)
		{
			clutter_color_from_string(&priv->bottomRectColor, g_value_get_string(pValue));
		}
		else if(g_strcmp0(pStyleKey, "bottom-rect-height") == 0)
			priv->flBottomRectHeight = g_value_get_double(pValue);
		else
		{
			;
		}
	}

	/* button width */
	if(g_strcmp0(pStyleKey, "drawer-width") == 0)
		priv->flWidth = g_value_get_double(pValue);
	/* get the animation duration */
	if(g_strcmp0(pStyleKey, "drawer-animation-duration") == 0)
		priv->inAnimationDuration = g_value_get_int64(pValue);
	/* animation duration */
	else if(g_strcmp0(pStyleKey, "drawer-buttons-offset") == 0)
		priv->inButtonOffset = g_value_get_int64(pValue);

	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
}


/********************************************************
 * Function : v_drawer_base_set_style
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_set_style(MildenhallDrawerBase *pDrawerBase)
{
        /* get the style properties in form of hash table */
        GHashTable *pHash = thornbury_style_set (PKGDATADIR"/mildenhall_drawer_base_style.json");
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(NULL != pHash)
	{
		GHashTableIter iter;
		gpointer key, value;

                g_hash_table_iter_init(&iter, pHash);
                /* iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *child_pHash = value;
                        //g_print("style layer = %s\n", (gchar*)key);
                        if(NULL != child_pHash)
                        {
                                g_hash_table_foreach (child_pHash, v_drawer_base_parse_style, pDrawerBase);
                        }
                }

		/* free the style hash */
        	thornbury_style_free(pHash);
	}

}

/********************************************************
 * Function :v_drawer_base_animation_completed
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_animation_completed(ClutterAnimation* animation, ClutterActor *pUserData)
{
	MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pUserData);
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);


        if(!MILDENHALL_DRAWER_IS_BASE(pUserData))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->bDrawerExpanded)
	{
		/* show tooltip for all buttons */
		if(NULL != priv->pButtonList)
		{
			v_drawer_base_set_tooltip(pDrawerBase, TRUE);
			/* emit tooltip-shown signal */
			g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_TOOLTIP_SHOWN], 0, NULL);
		}
	}
	else
	{
		/* change the default button bg image */
	        g_object_set(priv->pDefaultButton, "background-state", MILDENHALL_BUTTON_DRAWER_BG_OPEN, NULL);

		if(NULL != priv->pDrawerShadow)
		{
			/* show the views drawer bottom shadow*/
			clutter_actor_show(priv->pDrawerShadow);
		}
	}

	//priv->pSlideAnimation = NULL;
	 if(priv->pSlideTrans != NULL)
	 {
		 clutter_actor_remove_all_transitions(priv->pButtonGroup);
		 priv->pSlideTrans = NULL;
	 }

	/*if(priv->pSlideTrans != NULL)
        {
                clutter_transition_group_remove_all(CLUTTER_TRANSITION_GROUP(priv->pSlideTrans));
                clutter_timeline_stop(CLUTTER_TIMELINE(priv->pSlideTrans));
		g_object_unref(priv->pSlideTrans);
                priv->pSlideTrans = NULL;
        }*/
}

/********************************************************
 * Function :v_drawer_base_slide_down
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_slide_down(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        gint inLength = 0;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        inLength = g_list_length (priv->pButtonList);

        if (inLength == 0)
                return;

	if(priv->bDrawerExpanded && priv->pSlideTrans == NULL)//priv->pSlideAnimation == NULL)
	{

		gfloat newY;
                /* start the animation to open the drawer */
                ClutterAnimationMode mode = CLUTTER_EASE_OUT_BOUNCE;

		if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER)
		{
			//newX = clutter_actor_get_x(CLUTTER_ACTOR(priv->pButtonGroup));
			newY = clutter_actor_get_y(CLUTTER_ACTOR(priv->pButtonGroup))
				+ ( ( (inLength-1) * priv->flWidth)
				+ ( (inLength-1) * priv->inButtonOffset));

		}
		else
		{
			//newX = clutter_actor_get_x(CLUTTER_ACTOR(priv->pButtonGroup));
			newY = clutter_actor_get_y(CLUTTER_ACTOR(priv->pButtonGroup))
				- ( ( (inLength - 1) * priv->flWidth)
				+ ( (inLength-1) * priv->inButtonOffset));

			/* hide the views drawer bottom shadow */
			if(NULL != priv->pDrawerShadow)
				clutter_actor_hide(priv->pDrawerShadow);
		}

		g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_DRAWER_CLOSE], 0, NULL);

		/* reset the drawer expand flag */
		priv->bDrawerExpanded = FALSE;

		/* hide all tooltips for all buttons */
		v_drawer_base_set_tooltip(pDrawerBase, FALSE);

# if 0
		priv->pSlideAnimation = clutter_actor_animate (priv->pButtonGroup, mode, priv->inAnimationDuration,
				"x", newX,
				"y", newY,
				NULL);


		g_signal_connect_after (priv->pSlideAnimation, "completed",
				G_CALLBACK (v_drawer_base_animation_completed),
				pDrawerBase);
# endif

		if(priv->pSlideTrans != NULL)
		{
			clutter_actor_remove_all_transitions(priv->pButtonGroup);
			clutter_timeline_stop(CLUTTER_TIMELINE(priv->pButtonGroup));
			priv->pSlideTrans = NULL;
		}

		clutter_actor_save_easing_state(priv->pButtonGroup);
		clutter_actor_set_easing_mode (priv->pButtonGroup, mode);
		clutter_actor_set_easing_duration (priv->pButtonGroup, priv->inAnimationDuration);
		//clutter_actor_set_x (priv->pButtonGroup, newX);
		clutter_actor_set_y (priv->pButtonGroup, newY);
		clutter_actor_restore_easing_state (priv->pButtonGroup);

		priv->pSlideTrans = clutter_actor_get_transition(priv->pButtonGroup, "y") ;
		clutter_transition_set_remove_on_complete(priv->pSlideTrans, TRUE);
		g_signal_connect(CLUTTER_TIMELINE (priv->pSlideTrans), "completed", G_CALLBACK(v_drawer_base_animation_completed), pDrawerBase);
	}
}

/********************************************************
 * Function :v_drawer_base_slide_up
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_slide_up(MildenhallDrawerBase *pDrawerBase)
{
        /* start the animation to open the drawer */
        ClutterAnimationMode mode = CLUTTER_EASE_OUT_BOUNCE;
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        gint inLength = 0;
        gfloat newY;

	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->bDrawerExpanded || priv->pSlideTrans != NULL)//priv->pSlideAnimation != NULL)
		return;

	//gint inLength = g_hash_table_size(priv->pButtonHash);
	inLength = g_list_length (priv->pButtonList);

	if (inLength == 0)
                return;

        if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER)
        {
		//newX = clutter_actor_get_x(CLUTTER_ACTOR(priv->pButtonGroup));
		newY = clutter_actor_get_y(CLUTTER_ACTOR(priv->pButtonGroup))
                                        - ( ( (inLength-1) * priv->flWidth)
                                        + ( (inLength-1) * priv->inButtonOffset));

	}
	else
	{
		//newX = clutter_actor_get_x(CLUTTER_ACTOR(priv->pButtonGroup));
		newY = clutter_actor_get_y(CLUTTER_ACTOR(priv->pButtonGroup))
                                        + ( ( (inLength - 1) * priv->flWidth)
                                        + ( (inLength-1) * priv->inButtonOffset));

	}

	//g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_DRAWER_OPEN], 0, NULL);

	/* update the drawer expand flag */
	priv->bDrawerExpanded = TRUE;

	/* show the button group for animation */
	//clutter_actor_show(priv->pButtonGroup);

	/* change the default button bg image to normal */
	g_object_set(priv->pDefaultButton, "background-state", MILDENHALL_BUTTON_DRAWER_BG_NORMAL, NULL);

# if 0
        priv->pSlideAnimation = clutter_actor_animate (priv->pButtonGroup, mode, priv->inAnimationDuration,
                                "x", newX,
                                "y", newY,
                                NULL);

	g_signal_connect_after (priv->pSlideAnimation, "completed",
                                G_CALLBACK (v_drawer_base_animation_completed),
                                pDrawerBase);

# endif

	if(priv->pSlideTrans != NULL)
        {
		clutter_actor_remove_all_transitions(priv->pButtonGroup);
		clutter_timeline_stop(CLUTTER_TIMELINE(priv->pButtonGroup));
		priv->pSlideTrans = NULL;
	}

	clutter_actor_save_easing_state(priv->pButtonGroup);
	clutter_actor_set_easing_mode (priv->pButtonGroup, mode);
	clutter_actor_set_easing_duration (priv->pButtonGroup, priv->inAnimationDuration);
	//clutter_actor_set_x (priv->pButtonGroup, newX);
	clutter_actor_set_y (priv->pButtonGroup, newY);
	clutter_actor_restore_easing_state (priv->pButtonGroup);

	priv->pSlideTrans = clutter_actor_get_transition(priv->pButtonGroup, "y") ;
	clutter_transition_set_remove_on_complete(priv->pSlideTrans, TRUE);
	g_signal_connect(CLUTTER_TIMELINE (priv->pSlideTrans), "completed", G_CALLBACK(v_drawer_base_animation_completed), pDrawerBase);
}

/********************************************************
 * Function : v_drawer_base_get_active
 * Description: update drawer view based on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallDrawerBase *
 * Return value: void
 ********************************************************/
static gboolean v_drawer_base_get_active (ClutterActor *pDrawerBase, ClutterActor *pButton)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE(pDrawerBase);
	gboolean bReactive = TRUE;
	ThornburyModelIter *pIter = NULL;
        if(NULL != priv->pButtonList)
        {
                gint inIndex = g_list_index(priv->pButtonList, pButton);
                pIter = thornbury_model_get_iter_at_row(priv->pDrawerModel, inIndex);
                if(pIter)
                {
                        GValue value = {0,};
                        thornbury_model_iter_get_value(pIter, MILDENHALL_DRAWER_COLUMN_ACTIVE, &value);
                        bReactive = g_value_get_boolean(&value);
                        g_value_unset(&value);

                }
        }
	return bReactive;
}

/********************************************************
 * Function : update_each_button
 * Description: update drawer buttons based on model row change
 * Parameters: pButton, pName, pIcon, pTooltip, bReactive
 * Return value: void
 ********************************************************/
static void update_each_button(ClutterActor *pButton, const gchar *pName, const gchar *pIcon, const gchar *pTooltip, gboolean bReactive)
{
	g_print("%s\n", __FUNCTION__);
	if(NULL != pButton)
	{
		ThornburyModel *pButtonModel = NULL;
		/* get the button model */
		g_object_get(pButton, "model", &pButtonModel, NULL);
		if(NULL != pButtonModel)
		{
			/* modify the button model */
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value, pIcon);
			thornbury_model_insert_value(pButtonModel, 0, BUTTON_COLUMN_ICON, &value);
			g_value_unset (&value);

			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value, pTooltip);
			thornbury_model_insert_value(pButtonModel, 0, BUTTON_COLUMN_TOOLTIP_TEXT, &value);

			/* update the active state */
			g_object_set(pButton, "active", bReactive, NULL);
			/* set the button namse */
	                g_object_set(pButton, "name", pName, NULL);

		}
	}
}

/********************************************************
 * Function : v_drawer_base_update_view
 * Description: update drawer view based on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallDrawerBase *
 * Return value: void
 ********************************************************/
static void v_drawer_base_update_view(ThornburyModel *pModel,  gint inRow, ThornburyModelIter *pIter, ClutterActor *pDrawerBase)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE(pDrawerBase);

        const gchar *pName = NULL;
        const gchar *pIcon = NULL;
        const gchar *pTooltip = NULL;
        gboolean bReactive = TRUE;

        GValue value = {0, };
        ClutterActor *pButton = NULL;

	if(NULL == priv->pButtonList)
	{
		g_warning("button list is null\n");
		return;
	}

	/* get the first column value for button name */
	thornbury_model_iter_get_value(pIter, 0, &value);
	pName = g_strdup(g_value_get_string(&value));
	g_value_unset (&value);
	MILDENHALL_DRAWER_BASE_PRINT("pName = %s\n", pName);

	/* get the second column value for icon image path */
	thornbury_model_iter_get_value(pIter, 1, &value);
	pIcon = g_strdup(g_value_get_string(&value));
	g_value_unset (&value);
	MILDENHALL_DRAWER_BASE_PRINT("pIcon = %s\n", pIcon);

	/* get the third column value to get tooltip text */
	thornbury_model_iter_get_value(pIter, 2, &value);
	pTooltip = g_strdup(g_value_get_string(&value));
	g_value_unset (&value);
	MILDENHALL_DRAWER_BASE_PRINT("pTooltip = %s\n", pTooltip);

	/* get the third column value to get reactive state */
	thornbury_model_iter_get_value(pIter, 3, &value);
	bReactive = g_value_get_boolean(&value);
	g_value_unset (&value);
	MILDENHALL_DRAWER_BASE_PRINT("bReactive = %d\n", bReactive);

	pButton = g_list_nth_data(priv->pButtonList, inRow);
	update_each_button(pButton, pName, pIcon, pTooltip, bReactive);
}

/********************************************************
 * Function : v_drawer_base_row_changed_cb
 * Description: callback on drawer model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallDrawerBase *
 * Return value: void
 ********************************************************/
static void v_drawer_base_row_changed_cb (ThornburyModel *pModel,
                              ThornburyModelIter *pIter,
                              ClutterActor *pDrawerBase)
{
        gint inRow = 0;
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

        /* update view */
	if(NULL != pIter)
	{
		if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
		{
			g_warning("invalid drawer object\n");
			return;
		}

		inRow = thornbury_model_iter_get_row(pIter);
		/* update view based on changed model */
		v_drawer_base_update_view(pModel, inRow, pIter, pDrawerBase);
	}
}

/********************************************************
 * Function :  v_drawer_base_default_button_release_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_default_button_release_cb(ClutterActor *pButton, gpointer pUserData)
{
        MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pUserData);
        gboolean bReactive = TRUE;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	bReactive = v_drawer_base_get_active(CLUTTER_ACTOR(pDrawerBase), pButton);
	if(bReactive)
	{
		/* emit the button release with button name/index */
		g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_BUTTON_RELEASED], 0, clutter_actor_get_name(pButton));// g_strdup(clutter_actor_get_name(pButton)) );
	}
	v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );

}
/********************************************************
 * Function :  v_drawer_base_last_button_release_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_last_button_release_cb(ClutterActor *pButton, gpointer pUserData)
{
        MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pUserData);
        gboolean bReactive = TRUE;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	bReactive = v_drawer_base_get_active(CLUTTER_ACTOR(pDrawerBase), pButton);
	if(bReactive)
	{
		/* emit the button release with button name/index */
		g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_BUTTON_RELEASED], 0, clutter_actor_get_name(pButton));//g_strdup(clutter_actor_get_name(pButton)) );
	}
	/* close the drawer */
	v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
}

/********************************************************
 * Function :  v_drawer_base_middle_button_release_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_middle_button_release_cb(ClutterActor *pButton, gpointer pUserData)
{
        MildenhallDrawerBase *pDrawerBase = MILDENHALL_DRAWER_BASE (pUserData);
        gboolean bReactive = TRUE;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

        bReactive = v_drawer_base_get_active(CLUTTER_ACTOR(pDrawerBase), pButton);
        if(bReactive)
        {
		/* emit the button release with button name/index and close the drawer*/
		g_signal_emit(pDrawerBase, mildenhall_drawer_base_signals[SIG_BUTTON_RELEASED], 0, clutter_actor_get_name(pButton));//g_strdup(clutter_actor_get_name(pButton)) );
	}
	v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );

}

/********************************************************
 * Function :  v_drawer_base_default_button_swipe_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_default_button_swipe_cb(ClutterActor *pButton, LightwoodSwipeDirection swipeDirection, gpointer pUserData)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pUserData);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_DRAWER_IS_BASE(pUserData))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER)
	{
		if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_UP)
		{
			v_drawer_base_slide_up(MILDENHALL_DRAWER_BASE(pUserData) );
		}
		else if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_DOWN)
		{
			v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
		}
		else
		{
			;
		}
	}
	else
	{
                if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_DOWN)
                {
			v_drawer_base_slide_up(MILDENHALL_DRAWER_BASE(pUserData) );
                }
		else if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_UP)
                {
                        v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
                }
		else
		{
			;
		}
	}
}

/********************************************************
 * Function :  v_drawer_base_middle_button_swipe_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_middle_button_swipe_cb(ClutterActor *pButton, LightwoodSwipeDirection swipeDirection, gpointer pUserData)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pUserData);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_DRAWER_IS_BASE(pUserData))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER)
	{
		if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_DOWN)
		{
			v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
		}
	}
	else
	{
		if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_UP)
                {
			v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
                }

	}
}

/********************************************************
 * Function :  v_drawer_base_last_button_swipe_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_last_button_swipe_cb(ClutterActor *pButton, LightwoodSwipeDirection swipeDirection, gpointer pUserData)
{
	MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pUserData);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_DRAWER_IS_BASE(pUserData))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER)
	{
		if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_DOWN)
		{
			v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
		}
	}
	else
	{
		if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_UP)
                {
			v_drawer_base_slide_down(MILDENHALL_DRAWER_BASE(pUserData) );
                }

	}
}

/********************************************************
 * Function :  v_drawer_base_last_button_tooltip_hidden_cb
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_last_button_tooltip_hidden_cb(ClutterActor *pButton, gpointer pUserData)
{
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_BASE_PRINT: %s\n", __FUNCTION__);

	 if(!MILDENHALL_DRAWER_IS_BASE(pUserData))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	/* emit tooltip-hidden signal */
	g_signal_emit(MILDENHALL_DRAWER_BASE(pUserData), mildenhall_drawer_base_signals[SIG_TOOLTIP_HIDDEN], 0, NULL);
}

/********************************************************
 * Function : p_drawer_base_create_button
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static ClutterActor* p_drawer_base_create_button(MildenhallDrawerBase *pDrawerBase, const gchar *pName)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        ThornburyItemFactory *itemFactory = NULL;
        GObject *pObject = NULL;
        MildenhallButtonDrawer *pButton = NULL;
	g_autofree gchar *context_file = _mildenhall_get_resource_path ("mildenhall_drawer_base_context_button_prop.json");
        g_autofree gchar *views_file = _mildenhall_get_resource_path ("mildenhall_drawer_base_views_button_prop.json");
        g_autofree gchar *navi_file = _mildenhall_get_resource_path ("mildenhall_drawer_base_navi_button_prop.json");
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return NULL;
        }

	switch(priv->enType)
	{
		case MILDENHALL_CONTEXT_DRAWER:
			itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_BUTTON_DRAWER, context_file);
			break;
		case MILDENHALL_VIEWS_DRAWER:
			itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_BUTTON_DRAWER, views_file);
                        break;
		case MILDENHALL_NAVI_DRAWER:
                        itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_BUTTON_DRAWER, navi_file);
                        break;
		default:
			g_warning("invalid type\n");
	}


        g_object_get(itemFactory, "object", &pObject, NULL);
	if(pObject)
	{
		/* set the button namse */
		g_object_set(pObject, "name", pName, NULL);
		if(pName)
			g_free((gchar*)pName);
		pButton = MILDENHALL_BUTTON_DRAWER(pObject);
		priv->pButtonList = g_list_prepend(priv->pButtonList, pButton);
	}
	/* maintain the button hash by adding  the button to the hash */
	//if(NULL != pName)
	//	g_hash_table_insert(priv->pButtonHash, (gpointer)pName, CLUTTER_ACTOR(pButton));


        return (CLUTTER_ACTOR(pButton) );

}

/********************************************************
 * Function : v_drawer_base_set_button_background
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_set_button_background(MildenhallDrawerBase *pDrawerBase, ClutterActor *pButton, enBackgroundState enBgState)
{
	 MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(pButton == NULL)
		return;

	g_object_set(pButton, "background-state", enBgState, NULL);

}

/********************************************************
 * Function : p_drawer_base_create_button_model
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static ThornburyModel* p_drawer_base_create_button_model(MildenhallDrawerBase *pDrawerBase, const gchar *pIcon, const gchar *pTooltip)
{
        ThornburyModel *pButtonModel = NULL;
	 MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return NULL;
        }

        pButtonModel = (ThornburyModel*)thornbury_list_model_new (BUTTON_COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        -1);

	thornbury_model_append (pButtonModel,
				BUTTON_COLUMN_ICON, pIcon,
                                BUTTON_COLUMN_TOOLTIP_TEXT, pTooltip,
                                -1);


	if(pIcon)
		g_free((gchar*)pIcon);

	if(pTooltip)
		g_free((gchar*)pTooltip);
        return pButtonModel;


}

/********************************************************
 * Function : v_drawer_base_create_bottom_shadow
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_create_bottom_shadow(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if(priv->enType == MILDENHALL_VIEWS_DRAWER)
	{
		if(NULL == priv->pDrawerShadow && NULL != priv->pViewsBottomImagePath)
		{
			priv->pDrawerShadow = thornbury_ui_texture_create_new(priv->pViewsBottomImagePath, 0, 0, FALSE, FALSE);
			if(priv->pDrawerShadow)
			{
	        		clutter_actor_add_child(priv->pDrawerGroup, priv->pDrawerShadow);
				clutter_actor_set_position(priv->pDrawerShadow, 0.0, priv->flWidth);
        			clutter_actor_set_scale(priv->pDrawerShadow, 1, 0.8);
			}
		}
	}
	else
	{
		priv->pBottomRect = clutter_actor_new();
		g_object_set(priv->pBottomRect, "background-color", &priv->bottomRectColor, NULL);
		clutter_actor_add_child(priv->pDrawerGroup, priv->pBottomRect);
		clutter_actor_set_position(priv->pBottomRect, 0.0, priv->flWidth);

		/* height of tooltip rect will be the same as button height */
		clutter_actor_set_size (priv->pBottomRect, priv->flWidth, priv->flBottomRectHeight);//priv->flTooltipWidth, priv->flHeight);

		//clutter_actor_set_child_above_sibling(priv->pBottomRect, priv->pButtonGroup, NULL);
	}

}

/********************************************************
 * Function : v_drawer_base_create_default_button
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_create_default_button(MildenhallDrawerBase *pDrawerBase, const gchar *pName, const gchar *pIcon, const gchar *pTooltip, gboolean bReactive, enBackgroundState enBgState)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        ThornburyModel *pButtonModel = NULL;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	/* TBD: needs to check proper position of this shadow image , create bottom shadow */
	v_drawer_base_create_bottom_shadow(pDrawerBase);

	/* create button based on drawer type */
	priv->pDefaultButton = p_drawer_base_create_button(pDrawerBase, pName);

	/* set button background */
	v_drawer_base_set_button_background(pDrawerBase, priv->pDefaultButton, enBgState);

	/* create button model */
	pButtonModel = p_drawer_base_create_button_model (pDrawerBase, pIcon, pTooltip);
	g_object_set(MILDENHALL_BUTTON_DRAWER(priv->pDefaultButton), "model", pButtonModel, "active", bReactive, NULL);

	/* add defaault button to the main group */
	clutter_actor_add_child( priv->pDrawerGroup, priv->pDefaultButton);


	/* connect signals */
	//g_signal_connect(self, "button-press", G_CALLBACK(v_button_drawer_press_cb), NULL);
        g_signal_connect(priv->pDefaultButton, "button-release", G_CALLBACK(v_drawer_base_default_button_release_cb), pDrawerBase);
        g_signal_connect(priv->pDefaultButton, "swipe-end", G_CALLBACK(v_drawer_base_default_button_swipe_cb), pDrawerBase);
	//g_signal_connect(priv->pDefaultButton, "state-changed",  G_CALLBACK(v_drawer_base_button_state_changed), NULL);

}

/********************************************************
 * Function : v_drawer_base_create_last_button
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_create_last_button(MildenhallDrawerBase *pDrawerBase, const gchar *pName, const gchar *pIcon, const gchar *pTooltip, gboolean bReactive, enBackgroundState enBgState)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        ClutterActor *pButton = NULL;
        ThornburyModel *pButtonModel = NULL;
	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: drawer height = %f\n", clutter_actor_get_height(priv->pButtonGroup) );
	pButton = p_drawer_base_create_button (pDrawerBase, pName);

	g_object_set(pButton, "x", 0.0, "y", clutter_actor_get_height(priv->pButtonGroup), NULL);


        /* set button background */
        v_drawer_base_set_button_background (pDrawerBase, pButton, enBgState);

        /* create button model */
        pButtonModel = p_drawer_base_create_button_model (pDrawerBase, pIcon, pTooltip);
        g_object_set(pButton, "model", pButtonModel, "active", bReactive, NULL);

        /* add defaault button to the main group */
        clutter_actor_add_child(priv->pButtonGroup, pButton);
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: button x = %f button y = %f\n", clutter_actor_get_x(pButton), clutter_actor_get_y(pButton) );

	/* update the button group height */
	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER )
		priv->flButtonGroupHeight =  clutter_actor_get_height(priv->pButtonGroup) + 2;
	else
		priv->flButtonGroupHeight =  priv->flButtonGroupHeight - (clutter_actor_get_height(priv->pButtonGroup) + 2);

	/* connect the signals */
        g_signal_connect(pButton, "button-release", G_CALLBACK(v_drawer_base_last_button_release_cb), pDrawerBase);
        g_signal_connect(pButton, "swipe-end", G_CALLBACK(v_drawer_base_last_button_swipe_cb), pDrawerBase);
        g_signal_connect(pButton, "tooltip-hidden", G_CALLBACK(v_drawer_base_last_button_tooltip_hidden_cb), pDrawerBase);

}

/********************************************************
 * Function : v_drawer_base_create_middle_button
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_create_middle_button(MildenhallDrawerBase *pDrawerBase, const gchar *pName, const gchar *pIcon, const gchar *pTooltip, gboolean bReactive, enBackgroundState enBgState)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        ClutterActor *pButton = p_drawer_base_create_button (pDrawerBase, pName);
        ThornburyModel *pButtonModel = NULL;

	MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        /* set button background */
        v_drawer_base_set_button_background (pDrawerBase, pButton, enBgState);

        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: drawer height = %f\n", priv->flButtonGroupHeight );
        /* create button model */
        pButtonModel = p_drawer_base_create_button_model(pDrawerBase, pIcon, pTooltip);
        g_object_set(pButton, "model", pButtonModel, "active", bReactive, NULL);

	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER )
		g_object_set(pButton, "x", 0.0, "y",  priv->flButtonGroupHeight, NULL);
	else
		g_object_set(pButton, "x", 0.0, "y", priv->flButtonGroupHeight, NULL);

	/* disable the swipe action */
	g_object_set(pButton, "swipe-enabled", TRUE, NULL);
        /* add defaault button to the main group */
        clutter_actor_add_child(priv->pButtonGroup, pButton);

	/* update the button group height */
	if(priv->enType == MILDENHALL_CONTEXT_DRAWER || priv->enType == MILDENHALL_NAVI_DRAWER )
		priv->flButtonGroupHeight =  clutter_actor_get_height(priv->pButtonGroup) + 2;
	else
		priv->flButtonGroupHeight =  (priv->flButtonGroupHeight +  -((clutter_actor_get_height(pButton)) + 2) );

	/* connect the signals */
        g_signal_connect(pButton, "button-release", G_CALLBACK(v_drawer_base_middle_button_release_cb), pDrawerBase);

        g_signal_connect(pButton, "swipe-end", G_CALLBACK(v_drawer_base_middle_button_swipe_cb), pDrawerBase);


}

/********************************************************
 * Function : v_drawer_base_init_group
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_init_group(MildenhallDrawerBase *pDrawerBase, gint inTotalRows)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
	 MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	//v_drawer_base_set_position(pDrawerBase);
	/* initialise the default button and buttongroup */
	if(NULL != priv->pDefaultButton)
		clutter_actor_destroy(priv->pDefaultButton);
	if(NULL != priv->pButtonGroup)
		clutter_actor_destroy(priv->pButtonGroup);

	if(inTotalRows > 1)
	{
		priv->pButtonGroup = clutter_actor_new();
		clutter_actor_add_child(priv->pDrawerGroup, priv->pButtonGroup);
		//clutter_actor_hide(priv->pButtonGroup);
		//clutter_actor_set_position(priv->pButtonGroup, 0, 0);
	}
}

/********************************************************
 * Function : v_drawer_base_create_view
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void v_drawer_base_create_view(MildenhallDrawerBase *pDrawerBase)
{
        MildenhallDrawerBasePrivate *priv = MILDENHALL_DRAWER_BASE_GET_PRIVATE (pDrawerBase);
        MILDENHALL_DRAWER_BASE_PRINT("MILDENHALL_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_DRAWER_IS_BASE(pDrawerBase))
        {
                g_warning("invalid drawer object\n");
                return;
        }

	if (G_IS_OBJECT(priv->pDrawerModel) && thornbury_model_get_n_rows (priv->pDrawerModel) > 0)
        {
		gint inTotalRows = thornbury_model_get_n_rows (priv->pDrawerModel);

		gint inIndex;

		v_drawer_base_init_group(pDrawerBase, inTotalRows);

		//for(inIndex = 0; inIndex < inTotalRows; inIndex++)
		for(inIndex = inTotalRows -1; inIndex >=0; inIndex--)
		{
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pDrawerModel, inIndex);
			if(NULL != pIter)
			{
				const gchar *pName = NULL;
				const gchar *pIcon = NULL;
				const gchar *pTooltip = NULL;
				gboolean bReactive = TRUE;

				GValue value = { 0, };

				/* get the first column value for button name */
				thornbury_model_iter_get_value(pIter, MILDENHALL_DRAWER_COLUMN_NAME, &value);
				pName = g_strdup(g_value_get_string(&value));
				g_value_unset (&value);

				/* get the second column value for icon image path */
				thornbury_model_iter_get_value(pIter, MILDENHALL_DRAWER_COLUMN_ICON, &value);
				pIcon = g_strdup(g_value_get_string(&value));
				/* set the icon */
				//v_button_drawer_set_icon(CLUTTER_ACTOR(pButtonDrawer), g_value_get_string(&value));
				g_value_unset (&value);

				/* get the third column value to get tooltip text */
				thornbury_model_iter_get_value(pIter, MILDENHALL_DRAWER_COLUMN_TOOLTIP_TEXT, &value);
				pTooltip = g_strdup(g_value_get_string(&value));
				/* set the tooltip */
				//v_button_drawer_set_tooltip_text(CLUTTER_ACTOR(pButtonDrawer), g_value_get_string(&value));
				g_value_unset (&value);

				/* get the third column value to get reactive state */
				thornbury_model_iter_get_value(pIter, MILDENHALL_DRAWER_COLUMN_ACTIVE, &value);
				bReactive = g_value_get_boolean(&value);

				/* set the reactive state */
				g_value_unset (&value);

				if(inIndex == 0)
				{
					if(inTotalRows == 1)
						v_drawer_base_create_default_button(pDrawerBase, pName, pIcon, pTooltip, bReactive, MILDENHALL_BUTTON_DRAWER_BG_NORMAL);
					else
						v_drawer_base_create_default_button(pDrawerBase, pName, pIcon, pTooltip, bReactive, MILDENHALL_BUTTON_DRAWER_BG_OPEN);

				}
				else if(inIndex == inTotalRows-1)
				{
					v_drawer_base_create_last_button(pDrawerBase, pName, pIcon, pTooltip, bReactive, MILDENHALL_BUTTON_DRAWER_BG_CLOSE);
				}
				else
					v_drawer_base_create_middle_button(pDrawerBase, pName, pIcon, pTooltip, bReactive, MILDENHALL_BUTTON_DRAWER_BG_NORMAL);
			}


		}
	}
}


/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-cabinet-item.h */

#ifndef _MILDENHALL_CABINET_ITEM_H
#define _MILDENHALL_CABINET_ITEM_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include<clutter/clutter.h>
#include "liblightwood-roller.h"
#include "mildenhall_cabinet_roller.h"

enum {
	ARROW_ASCENDING,
	ARROW_DESCENDING,
	ARROW_NONE,
};
enum {
	MILDENHALL_CABINET_FOLDER_TYPE,
	MILDENHALL_CABINET_FILE_TYPE,
};

G_BEGIN_DECLS

#define MILDENHALL_TYPE_CABINET_ITEM mildenhall_cabinet_item_get_type()

#define MILDENHALL_CABINET_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_CABINET_ITEM, MildenhallCabinetItem))

#define MILDENHALL_CABINET_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_CABINET_ITEM, MildenhallCabinetItemClass))

#define MILDENHALL_IS_CABINET_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_CABINET_ITEM))

#define MILDENHALL_IS_CABINET_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_CABINET_ITEM))

#define MILDENHALL_CABINET_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_CABINET_ITEM, MildenhallCabinetItemClass))

typedef struct _MildenhallCabinetItem MildenhallCabinetItem;
typedef struct _MildenhallCabinetItemClass MildenhallCabinetItemClass;
typedef struct _MildenhallCabinetItemPrivate MildenhallCabinetItemPrivate;

struct _MildenhallCabinetItem
{
  ClutterActor parent;

  MildenhallCabinetItemPrivate *priv;
};

struct _MildenhallCabinetItemClass
{
  ClutterActorClass parent_class;
};

GType mildenhall_cabinet_item_get_type (void) G_GNUC_CONST;

MildenhallCabinetItem *mildenhall_cabinet_item_new (void);

G_END_DECLS

#endif /* _MILDENHALL_CABINET_ITEM_H */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Sample item for MildenhallVariableRoller
 */

#include "sample-variable-item.h"

#include <thornbury/thornbury.h>

#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-glowshader.h"

#define ARROW_UP_FILE_NAME        EXAMPLES_DATADIR"/arrow-up.png"

G_DEFINE_TYPE(SampleVariableItem, sample_variable_item, CLUTTER_TYPE_ACTOR);

enum
{
  PROP_0,

  PROP_ICON,
  PROP_LABEL,
  PROP_EXTRA_HEIGHT,
  PROP_FOCUSED,
};

static void
sample_variable_item_get_property (GObject    *object,
                                   guint       property_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  SampleVariableItem *item = SAMPLE_VARIABLE_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
      break;
    case PROP_LABEL:
      g_value_set_string (value,
          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    case PROP_EXTRA_HEIGHT:
      {
        ClutterActor *actor = CLUTTER_ACTOR (object);
        gfloat preferred_height, extra_height;
        
        clutter_actor_get_preferred_height (actor, -1, NULL, &preferred_height);
        extra_height = clutter_actor_get_height (actor) - preferred_height;
        g_value_set_float (value, extra_height);

        break;
      }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_variable_item_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  SampleVariableItem *item = SAMPLE_VARIABLE_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
	    {
		    if(g_value_get_string(value) !=NULL)
		    {
			    thornbury_ui_texture_set_from_file (item->icon, g_value_get_string(value), 277,277,FALSE,TRUE);
			    clutter_actor_show(item->icon);
		    }
	    }
      break;
    case PROP_LABEL:
      clutter_text_set_text (CLUTTER_TEXT (item->label),
                             g_value_get_string (value));
      break;
    case PROP_EXTRA_HEIGHT:
      {
        ClutterActor *actor = CLUTTER_ACTOR (object);
        gfloat preferred_height, extra_height;
        
        clutter_actor_get_preferred_height (actor, -1, NULL, &preferred_height);
        extra_height = g_value_get_float (value);
        clutter_actor_set_height (actor, preferred_height + extra_height);

        break;
      }
    case PROP_FOCUSED:
      if (g_value_get_boolean (value))
        {
          SampleVariableItemClass *klass = SAMPLE_VARIABLE_ITEM_GET_CLASS (object);

          if (clutter_actor_get_effect (item->icon, "glow") != NULL)
            return;

          clutter_actor_add_effect_with_name (item->icon, "glow", klass->glow_effect_1);
          clutter_actor_add_effect_with_name (item->label, "glow", klass->glow_effect_2);
        }
      else
        {
          if (clutter_actor_get_effect (item->icon, "glow") == NULL)
            return;

          clutter_actor_remove_effect_by_name (item->icon, "glow");
          clutter_actor_remove_effect_by_name (item->label, "glow");
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static gboolean
button_release_event (ClutterActor       *actor,
                      ClutterButtonEvent *event)
{
  g_debug ("actor '%s' has been activated", clutter_actor_get_name (actor));
  return TRUE;
}

static void
sample_variable_item_class_init (SampleVariableItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = sample_variable_item_get_property;
  object_class->set_property = sample_variable_item_set_property;

  actor_class->button_release_event = button_release_event;

  pspec = g_param_spec_boxed ("icon",
                              "icon",
                              "COGL texture handle for the icon",
                              COGL_TYPE_HANDLE,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ICON, pspec);

  pspec = g_param_spec_string ("label",
                               "label",
                               "Text for the label",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_LABEL, pspec);

  pspec = g_param_spec_float ("extra-height",
                              "extra-height",
                              "Space to grow vertically",
                              0.0,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_EXTRA_HEIGHT, pspec);

  pspec = g_param_spec_boolean ("focused",
                                "focused",
                                "Whether this actor should be rendered as focused",
                                FALSE,
                                G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);

  klass->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
  klass->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());
}

static void
sample_variable_item_init (SampleVariableItem *sample_variable_item)
{
  ClutterActor *line;
  ClutterColor text_color = { 255, 255, 255, 255 };
  ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};

  sample_variable_item->icon = thornbury_ui_texture_create_new (ARROW_UP_FILE_NAME,0,0,FALSE,FALSE);//clutter_texture_new ();
  //clutter_container_add_actor (CLUTTER_ACTOR (sample_variable_item), sample_variable_item->icon);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_variable_item), sample_variable_item->icon);
  clutter_actor_set_position (sample_variable_item->icon, 18, 14);
  clutter_actor_show (sample_variable_item->icon);

  sample_variable_item->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (sample_variable_item->label), &text_color);
  clutter_text_set_font_name (CLUTTER_TEXT (sample_variable_item->label), "Sans 28px");
  clutter_actor_set_position (sample_variable_item->label, 79, 17);
  //clutter_container_add_actor (CLUTTER_ACTOR (sample_variable_item), sample_variable_item->label);
	clutter_actor_add_child(CLUTTER_ACTOR(sample_variable_item), sample_variable_item->label);
  clutter_actor_show (sample_variable_item->label);

#if 1
  line = clutter_actor_new();//clutter_rectangle_new_with_color (&line_color);
//clutter_rectangle_new_with_color (&line_color);
        clutter_actor_set_background_color(line,&line_color);
  clutter_actor_set_size (line, 52, 1);
  clutter_actor_set_position (line, 11, 63);
  clutter_actor_add_child (CLUTTER_ACTOR (sample_variable_item), line);
  clutter_actor_show (line);

  line = clutter_actor_new();//clutter_rectangle_new_with_color (&line_color);
	clutter_actor_set_background_color(line,&line_color);
  clutter_actor_set_size (line, 709, 1);
  clutter_actor_set_position (line, 67, 63);
  clutter_actor_add_child (CLUTTER_ACTOR (sample_variable_item), line);
  clutter_actor_show (line);
#endif

  clutter_actor_set_reactive (CLUTTER_ACTOR (sample_variable_item), TRUE);
}

ClutterActor*
sample_variable_item_new ()
{
    return g_object_new (TYPE_SAMPLE_VARIABLE_ITEM, NULL);
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_overlay
 * @title: MildenhallOverlay
 * @short_description: An overlay that can place an image on top of something else.
 * @see_also: #MildenhallMediaOverlay
 * @stability: stable
 *
 * The basic intention of having this widget is In mildenhall variant is that,
 * All application should have overlay upon the stage. Which is a .png with the
 * customized alpha value.
 *
  * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * MildenhallOverlay *overlay = NULL;
 * GObject *object = NULL;
 * ThornburyItemFactory *item_factory =
 * 	thornbury_item_factory_generate_widget_with_props (
 * 		MILDENHALL_TYPE_OVERLAY, NULL);
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * overlay = MILDENHALL_OVERLAY (object);
 * clutter_actor_add_child (CLUTTER_ACTOR (stage), CLUTTER_ACTOR (overlay));
 *
 * ]|
 *
 * Since: 0.3.0
 */

/**********************************************************************
                			Header Files
***********************************************************************/
#include "mildenhall_overlay.h"

/***********************************************************************
                			Macro definitions
 ***********************************************************************/
#define OVERLAY_X "overlay-x"
#define OVERLAY_Y "overlay-y"
#define OVERLAY_IMAGE "overlay-image"

G_DEFINE_TYPE (MildenhallOverlay, mildenhall_overlay, CLUTTER_TYPE_ACTOR)

#define OVERLAY_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_OVERLAY, MildenhallOverlayPrivate))


/***********************************************************************
               	   	   	   Function Prototypes
 ***********************************************************************/
static void
v_overlay_add_style_to_hash (MildenhallOverlay *overlay, const gchar *pKey, gpointer pValue);
static void v_overlay_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static void create_overlay(MildenhallOverlay *overlay);

struct _MildenhallOverlayPrivate
{
	ClutterActor *pOverlay;
	GHashTable *pStyleHash;
	GHashTable *pLocalHash;
};


/*********************************************************************************************
 * Function:    mildenhall_overlay_dispose
 * Description: Dispose the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void
mildenhall_overlay_dispose (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_overlay_parent_class)->dispose (object);
}
/*********************************************************************************************
 * Function:    mildenhall_overlay_finalize
 * Description: Call a finalize on the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void
mildenhall_overlay_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_overlay_parent_class)->finalize (object);
}
/********************************************************
 * Function : mildenhall_overlay_class_init
 * Description: Overlay class init function
 * Parameter :  MildenhallOverlayClass Object.
 * Return value: void
 ********************************************************/
static void
mildenhall_overlay_class_init (MildenhallOverlayClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MildenhallOverlayPrivate));
	object_class->dispose = mildenhall_overlay_dispose;
	object_class->finalize = mildenhall_overlay_finalize;
}
/********************************************************
 * Function : mildenhall_overlay_init
 * Description: Overlay init function
 * Parameter :  Overlay Object.
 * Return value: void
 ********************************************************/
static void
mildenhall_overlay_init (MildenhallOverlay *self)
{
	self->priv = OVERLAY_PRIVATE (self);
	self->priv->pStyleHash = thornbury_style_set(PKGDATADIR"/mildenhall_overlay_style.json");
	if(NULL != self->priv->pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init(&iter, self->priv->pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				self->priv->pLocalHash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
				g_hash_table_foreach(pHash, v_overlay_parse_style, self);
			}
		}
	}
	thornbury_style_free(self->priv->pStyleHash);
	create_overlay(self);
}
/********************************************************
 * Function : create_overlay
 * Description: This fucntion is used to create the overlay.
 * Parameter :  overlay
 * Return value: void
 ********************************************************/
static void create_overlay(MildenhallOverlay *overlay)
{
	MildenhallOverlayPrivate *priv = overlay->priv;
	gchar *ovelayImage = NULL;
	GError *err = NULL;
	gint64 inOverlayX;
	gint64 inOverlayY;

        sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, OVERLAY_X),
                "%" G_GINT64_FORMAT,
                &inOverlayX);
        sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, OVERLAY_Y),
                "%" G_GINT64_FORMAT,
                &inOverlayY);
	ovelayImage = (gchar*) g_hash_table_lookup (priv->pLocalHash, OVERLAY_IMAGE);

	priv->pOverlay = thornbury_ui_texture_create_new(g_strconcat(PKGTHEMEDIR,"/",ovelayImage,NULL),0,0,FALSE,FALSE);
	if (err)
	{
		g_warning ("unable to load texture: %s", err->message);
		g_clear_error (&err);
	}
	clutter_actor_set_position(priv->pOverlay,inOverlayX,inOverlayY);
	clutter_actor_set_name(priv->pOverlay,"overlay");
	clutter_actor_add_child(CLUTTER_ACTOR(overlay),priv->pOverlay);
}
/********************************************************
 * Function : v_overlay_parse_style
 * Description: Parse the style json file.
 * Parameter :  *pKey,pValue,pUserData
 * Return value: void
 ********************************************************/
static void v_overlay_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallOverlay *overlay=MILDENHALL_OVERLAY(pUserData);

	// store the style images in a hash for setting syle based on set properties

	if(g_strcmp0(pStyleKey, "overlay-x") == 0)
		v_overlay_add_style_to_hash(overlay, OVERLAY_X, pValue);
	else if(g_strcmp0(pStyleKey, "overlay-y") == 0)
		v_overlay_add_style_to_hash(overlay, OVERLAY_Y, pValue);
	else if(g_strcmp0(pStyleKey, "overlay-image") == 0)
		v_overlay_add_style_to_hash(overlay, OVERLAY_IMAGE, pValue);
	else
		;	// do nothing

	if(pStyleKey)
		g_free(pStyleKey);
}

/********************************************************
 * Function : v_overlay_add_style_to_hash
 * Description: maintain style hash
 * Parameter :  *overlay, *pKey, pValue
 * Return value: void
 ********************************************************/
static void
v_overlay_add_style_to_hash (MildenhallOverlay *overlay, const gchar *pKey, gpointer pValue)
{
	MildenhallOverlayPrivate *priv = overlay->priv;
	if(!MILDENHALL_IS_OVERLAY(overlay))
	{
		g_warning("invalid overlay object\n");
		return;
	}

	if(NULL != pKey || NULL != pValue)
	{
		if (G_VALUE_HOLDS_INT64(pValue))
			g_hash_table_insert (priv->pLocalHash, g_strdup (pKey), g_strdup_printf ("%ld", g_value_get_int64 (pValue)));
		else
			g_hash_table_insert (priv->pLocalHash, g_strdup (pKey), g_strdup (g_value_get_string (pValue)));
	}
}

/**********************************************************************
                        End of file
***********************************************************************/

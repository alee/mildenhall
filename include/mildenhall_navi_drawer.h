/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_navi_drawer.h
 *
 * Created on: Oct 31, 2012
 *
 * mildenhall_navi_drawer.h */


#ifndef _MILDENHALL_NAVI_DRAWER_H
#define _MILDENHALL_NAVI_DRAWER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_drawer_base.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_NAVI_DRAWER mildenhall_navi_drawer_get_type()

#define MILDENHALL_NAVI_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_NAVI_DRAWER, MildenhallNaviDrawer))

#define MILDENHALL_NAVI_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_NAVI_DRAWER, MildenhallNaviDrawerClass))

#define MILDENHALL_IS_NAVI_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_NAVI_DRAWER))

#define MILDENHALL_IS_NAVI_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_NAVI_DRAWER))

#define MILDENHALL_NAVI_DRAWER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_NAVI_DRAWER, MildenhallNaviDrawerClass))

typedef struct _MildenhallNaviDrawer MildenhallNaviDrawer;
typedef struct _MildenhallNaviDrawerClass MildenhallNaviDrawerClass;
typedef struct _MildenhallNaviDrawerPrivate MildenhallNaviDrawerPrivate;

/**
 * MildenhallNaviDrawer:
 *
 * The #MildenhallNaviDrawer struct contains only private data.
 *
 */
struct _MildenhallNaviDrawer
{
	MildenhallDrawerBase parent;

	MildenhallNaviDrawerPrivate *priv;
};

/**
 * MildenhallNaviDrawerClass:
 *
 * The #MildenhallNaviDrawerClass struct contains only private data.
 *
 */
struct _MildenhallNaviDrawerClass
{
	MildenhallDrawerBaseClass parent_class;
};

GType mildenhall_navi_drawer_get_type (void) G_GNUC_CONST;

ClutterActor*mildenhall_navi_drawer_new (void);

G_END_DECLS

#endif /* _MILDENHALL_NAVI_DRAWER_H */




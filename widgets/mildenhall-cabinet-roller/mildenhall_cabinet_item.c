/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-cabinet-item.c */

#include "mildenhall_cabinet_item.h"

#include <thornbury/thornbury.h>

#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"

G_DEFINE_TYPE (MildenhallCabinetItem, mildenhall_cabinet_item, CLUTTER_TYPE_ACTOR)

#define CABINET_ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_CABINET_ITEM, MildenhallCabinetItemPrivate))

#define MILDENHALL_CABINET_NUM_LABEL_X 45.0
#define MILDENHALL_CABINET_NUM_LABEL_Y 15.0
#define MILDENHALL_CABINET_MID_LABEL_X 96.0
#define MILDENHALL_CABINET_MID_LABEL_Y 15.0
#define MILDENHALL_CABINET_ROLLER_ITEM_FONT(size) "DejaVuSansCondensed " #size "px"
#define MILDENHALL_CABINET_VERTICAL_WIDTH 1.0
#define MILDENHALL_CABINET_VERTICAL_HEIGHT 66.0
#define MILDENHALL_CABINET_VERTICAL_X 90.0
#define MILDENHALL_CABINET_VERTICAL_Y 0.0
#define MILDENHALL_CABINET_HORIZONTAL_WIDTH 656.0
#define MILDENHALL_CABINET_HORIZONTAL_HEIGHT 1.0
#define MILDENHALL_CABINET_HORIZONTAL_X 0.0
#define MILDENHALL_CABINET_HORIZONTAL_Y 0.0
#define MILDENHALL_CABINET_HORIZONTAL_1_Y 10.0
#define MILDENHALL_CABINET_HORIZONTAL_2_Y 65.0
#define MILDENHALL_CABINET_NORMAL_TEXT_WIDTH 240.0
#define MILDENHALL_CABINET_VERTICAL_FOOTER_HEIGHT 86.0

#define MILDENHALL_CABINET_TEXT_WIDTH 90.0

struct _MildenhallCabinetItemPrivate
{
	ClutterActor *NumberLabel;
	ClutterActor *label;
	ClutterActor *vertical;
	ClutterActor *vertical1;
	ClutterActor *depthVertical;
	ClutterActor *icon;
	ClutterActor *overlay;
	ClutterActor *horizontal;
	ClutterActor *horizontal_1;
	ClutterActor *horizontal_2;
	ClutterActor *text;
	ClutterActor *background;
	gint iSortOrder;
	gfloat pFooterWidth;
	ClutterActor *iconRect;
	ClutterActor *pBlockedArrow;
	ClutterActor *extra_separator;
	GHashTable *pOverlayTable;
};
enum	{
	PROP_NONE,
	PROP_NUM_LABEL,
	PROP_LABEL,
	PROP_LEFT_ICON,
	PROP_FOOTER,
	PROP_SHOW_ARROW,
	PROP_ROW,
	PROP_SORT_ARROW,
	PROP_FOCUSED,
	PROP_HEIGHT,
	PROP_WIDTH,
	PROP_FOOTER_WIDTH,
	PROP_MIME,
	PROP_OVERLAY,
};


static void
mildenhall_cabinet_item_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	MildenhallCabinetItem *item = MILDENHALL_CABINET_ITEM(object);
  switch (property_id)
    {
	case PROP_NUM_LABEL:
		break;
	case PROP_LABEL:
		break;
	case PROP_LEFT_ICON:
		break;
	case PROP_OVERLAY:
		break;
	case PROP_SHOW_ARROW:
			break;
		case PROP_SORT_ARROW:
			g_value_set_int (value,item->priv->iSortOrder);
			break;
		case PROP_FOCUSED:
			break;
	    default:
	      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	    }
	}
	static void
	_set_prop_width(MildenhallCabinetItem *item,gfloat width)
	{
		clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width-40);
		clutter_actor_set_x(CLUTTER_ACTOR(item->priv->pBlockedArrow),width - 40);
		clutter_actor_set_width (CLUTTER_ACTOR(item->priv->label), width - 160);//140.0);//MILDENHALL_CABINET_NORMAL_TEXT_WIDTH);//(g_value_get_float(value) - 60));
		clutter_actor_set_width(item->priv->horizontal,width - 7);
		clutter_actor_set_width(item->priv->horizontal_1,width - 7);
		clutter_actor_set_width(item->priv->extra_separator,width - 7);

		/*for footer */
		if(item->priv->background)
		{
			clutter_actor_set_width(CLUTTER_ACTOR(item->priv->background),width);
		}
	}
	static void
	_set_prop_footer(MildenhallCabinetItem *item,gboolean flag)
	{
		if(flag)
		{
		item->priv->background = thornbury_ui_texture_create_new (EXAMPLES_DATADIR"/sortbottom.png",item->priv->pFooterWidth,MILDENHALL_CABINET_VERTICAL_FOOTER_HEIGHT,FALSE,FALSE);
		clutter_actor_set_position (item->priv->background,0,0);
		//clutter_actor_add_child (CLUTTER_ACTOR (item),background);
		clutter_actor_insert_child_below (CLUTTER_ACTOR (item),item->priv->background,NULL);

		clutter_actor_set_position (item->priv->vertical,100.0,0.0);
		clutter_actor_show (item->priv->vertical);
		clutter_actor_set_position (item->priv->depthVertical,101.0,0.0);
		clutter_actor_show (item->priv->depthVertical);
		clutter_actor_set_width(item->priv->horizontal_1,item->priv->pFooterWidth);
		clutter_actor_set_width(item->priv->horizontal,item->priv->pFooterWidth);
		clutter_actor_show (item->priv->horizontal_1);
		clutter_actor_hide (item->priv->text);
		clutter_actor_hide (item->priv->pBlockedArrow);
		clutter_actor_set_height(CLUTTER_ACTOR(item),MILDENHALL_CABINET_VERTICAL_FOOTER_HEIGHT);
		clutter_actor_hide(item->priv->iconRect);
		}
	}
	static void
	_update_item(MildenhallCabinetItem *item,gint row)
	{
		ClutterActor * parent = clutter_actor_get_parent(CLUTTER_ACTOR(item));
		if (parent)
		{
			gfloat width = clutter_actor_get_width(parent);
			ThornburyModel * model = lightwood_roller_get_model(LIGHTWOOD_ROLLER(parent));
			if ((parent != NULL )&&
					(model != NULL) &&
					(row == thornbury_model_get_n_rows (model) - 1))
			{
				clutter_actor_set_size(item->priv->extra_separator,width - 5.0,1);
				clutter_actor_show (item->priv->extra_separator);
			}
			else
				clutter_actor_hide (item->priv->extra_separator);
			if(clutter_actor_has_clip(parent))
			{
				//g_print("width = %f\n",width);
				clutter_actor_get_clip(parent,NULL,NULL,&width,NULL);
			}
			clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width - 40);
			clutter_actor_set_x(CLUTTER_ACTOR(item->priv->pBlockedArrow),width - 40);
			clutter_actor_set_width(item->priv->horizontal,width - 5.0);
			clutter_actor_set_width(item->priv->horizontal_1,width - 5.0);
			//g_print("width = %f\n",width);
		}
	}
	static void
	_set_prop_sort_arrow(MildenhallCabinetItem *item)
	{
		if(item->priv->iSortOrder)
		{
			if(item->priv->iSortOrder == ARROW_DESCENDING)
			{
				clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25BF;");
				gfloat width = 0.0;
				width = clutter_actor_get_width(clutter_actor_get_parent(CLUTTER_ACTOR(item)));
				//g_print("width = %f\n",width);
				clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width - 20);
				clutter_actor_show(CLUTTER_ACTOR(item->priv->text));
			}
			else
				clutter_actor_hide(item->priv->text);
		}
		else
		{
			if(item->priv->iSortOrder == ARROW_ASCENDING)
			{
				clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25B5;");
				gfloat width = 0.0;
				width = clutter_actor_get_width(clutter_actor_get_parent(CLUTTER_ACTOR(item)));
				//g_print("width = %f\n",width);
				clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width - 20);
				clutter_actor_show(CLUTTER_ACTOR(item->priv->text));
			}
		}
	}
	static void
	mildenhall_cabinet_item_set_property (GObject      *object,
				       guint         property_id,
				       const GValue *value,
				       GParamSpec   *pspec)
	{
		MildenhallCabinetItem *item = MILDENHALL_CABINET_ITEM(object);
		switch (property_id)
		{
			case PROP_NUM_LABEL:
				{
					clutter_text_set_text (CLUTTER_TEXT (item->priv->NumberLabel),
							(gchar *)g_value_get_string (value));
					clutter_actor_show(item->priv->NumberLabel);
				}

				break;
			case PROP_WIDTH:
				{
					_set_prop_width(item,g_value_get_float(value));

				}
				break;
			case PROP_HEIGHT:
				{
				}
				break;
			case PROP_LABEL:
				{
					clutter_text_set_text (CLUTTER_TEXT (item->priv->label),
							(gchar *)g_value_get_string (value));
					clutter_actor_show(item->priv->label);
				}
				break;
			case PROP_LEFT_ICON:
				if(g_value_get_string(value))
				{
					//g_print("LEFT ICON");
					//g_print("=%s\n",g_value_get_string(value));
					thornbury_texture_set_from_file (item->priv->icon, (gchar *)g_value_get_string(value),82.0,66.0,NULL);
					//thornbury_ui_texture_set_from_file (item->priv->icon, g_value_get_string(value),82.0,66.0,FALSE,TRUE);
					clutter_actor_show(item->priv->icon);
				}
				else
				{
					clutter_actor_hide(item->priv->icon);
				}
				break;
			case PROP_OVERLAY:
				{
				gchar *overlay = (gchar *)g_value_get_string(value);
					if(overlay)
					{
						gchar *path = g_hash_table_lookup(item->priv->pOverlayTable,overlay);
						//g_print("%s:path = %s\n",__FILE__,path);
					thornbury_texture_set_from_file (item->priv->overlay,path,82.0,66.0,NULL);
						//thornbury_ui_texture_set_from_file (item->priv->overlay,path, 0,0,FALSE,TRUE);
						clutter_actor_show(item->priv->overlay);
						//g_free(overlay);
					}
				}
				break;
			case PROP_FOOTER:
				{
					_set_prop_footer(item,g_value_get_boolean(value));
				}
				break;
			case PROP_SHOW_ARROW:
				{
					if(g_value_get_boolean(value))
						clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25B8;");
				}
				break;
			case PROP_FOOTER_WIDTH:
				{
					////g_print("footer_width = %f\n",g_value_get_float(value));
					if(g_value_get_float(value) > 0.0f)
						item->priv->pFooterWidth = g_value_get_float(value);
					else
						item->priv->pFooterWidth = 0.0;
				}
				break;
			case PROP_ROW:
				{
					_update_item(item,g_value_get_int(value));
				}
				break;
			case PROP_SORT_ARROW:
				{
					item->priv->iSortOrder = g_value_get_int(value);
					_set_prop_sort_arrow(item);
					if(g_value_get_int(value))
					{
						if(item->priv->iSortOrder == ARROW_DESCENDING)
						{
							clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25BF;");
							gfloat width = 0.0;
							width = clutter_actor_get_width(clutter_actor_get_parent(CLUTTER_ACTOR(item)));
							////g_print("width = %f\n",width);
							clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width - 20);
							clutter_actor_show(CLUTTER_ACTOR(item->priv->text));
						}
						else
							clutter_actor_hide(item->priv->text);
					}
					else
					{
						if(item->priv->iSortOrder == ARROW_ASCENDING)
						{
							clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25B5;");
							gfloat width = 0.0;
							width = clutter_actor_get_width(clutter_actor_get_parent(CLUTTER_ACTOR(item)));
							//g_print("width = %f\n",width);
							clutter_actor_set_x(CLUTTER_ACTOR(item->priv->text),width - 20);
							clutter_actor_show(CLUTTER_ACTOR(item->priv->text));
						}
					}
				}
				break;
			case PROP_FOCUSED:
				{
	#if 0
					if(g_value_get_boolean(value))
					{
						//clutter_actor_show(item->priv->pBlockedArrow);
						//clutter_actor_hide(item->priv->text);
						//clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25B8;");
					}
					else
					{
						//clutter_actor_show(item->priv->text);
						//clutter_actor_hide(item->priv->pBlockedArrow);
						//clutter_text_set_markup(CLUTTER_TEXT(item->priv->text),"&#x25B9;");
					}
	#endif
				}
				break;
			case PROP_MIME:
				{
					if(g_value_get_int(value) == MILDENHALL_CABINET_FOLDER_TYPE)
						clutter_actor_show(item->priv->text);
					else
						 clutter_actor_hide (item->priv->text);

				}
				break;
			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		}
	}

	static void
	mildenhall_cabinet_item_dispose (GObject *object)
	{
	  G_OBJECT_CLASS (mildenhall_cabinet_item_parent_class)->dispose (object);
	}

	static void
	mildenhall_cabinet_item_finalize (GObject *object)
	{
	  G_OBJECT_CLASS (mildenhall_cabinet_item_parent_class)->finalize (object);
	}

	static void
	mildenhall_cabinet_item_class_init (MildenhallCabinetItemClass *klass)
	{
	  GObjectClass *object_class = G_OBJECT_CLASS (klass);

	  g_type_class_add_private (klass, sizeof (MildenhallCabinetItemPrivate));

	  object_class->get_property = mildenhall_cabinet_item_get_property;
	  object_class->set_property = mildenhall_cabinet_item_set_property;
	  object_class->dispose = mildenhall_cabinet_item_dispose;
	  object_class->finalize = mildenhall_cabinet_item_finalize;
		GParamSpec *pSpec = NULL;

		/**
		 *      MildenhallCabinetItem: num-label:
		 *
		 * status Text to be displayed in Item type
		 */
		pSpec = g_param_spec_string("number-label", "number-label",
				"Text to be displayed in Item type", NULL, G_PARAM_READWRITE);
		g_object_class_install_property(object_class, PROP_NUM_LABEL, pSpec);
		/**
		 *      MildenhallCabinetItem: label:
		 *
		 * status Text to be displayed in Item type
		 */
		pSpec = g_param_spec_string("label", "label",
				"Text to be displayed in Item type", NULL, G_PARAM_READWRITE);
		g_object_class_install_property(object_class, PROP_LABEL, pSpec);
		/**
		 *      MildenhallCabinetItem: label:
		 *
		 * status Text to be displayed in Item type
		 */
		pSpec = g_param_spec_string("left-icon", "left icon",
				"Complete Path of the image to be displayed in Left side of Roller Item",
				NULL,
				G_PARAM_READWRITE);
		g_object_class_install_property(object_class, PROP_LEFT_ICON, pSpec);

		pSpec = g_param_spec_string("left-overlay", "left overlay",
				"Complete Path of the overlay image to be displayed in Left side of Roller Item",
				NULL,
				G_PARAM_READWRITE);
		g_object_class_install_property(object_class, PROP_OVERLAY, pSpec);

		pSpec = g_param_spec_boolean ("footer",
				"footer",
				"Whether footer is enable/disable",
				TRUE,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_FOOTER, pSpec);

		pSpec = g_param_spec_int ("row",
				"row",
				"Row which gets updated",
				-1,
				G_MAXINT,
				-1,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_ROW, pSpec);
		pSpec = g_param_spec_int ("mime",
				"mime",
				"MIME to show the arrow",
				-1,
				G_MAXINT,
				-1,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_MIME, pSpec);
		pSpec = g_param_spec_int ("sort-arrow",
				"sort-arrow",
				"arrow-indicates sorting is enable/disable",
				-1,
				G_MAXINT,
				-1,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_SORT_ARROW, pSpec);

		pSpec = g_param_spec_boolean ("focused",
				"focused",
				"Whether this actor should be rendered as focused",
				FALSE,
				G_PARAM_WRITABLE);
		g_object_class_install_property (object_class, PROP_FOCUSED, pSpec);
		pSpec = g_param_spec_float ("width",
				"Width",
				"Width of the item",
				-G_MAXFLOAT,
				G_MAXFLOAT,
				0.0,
				G_PARAM_READWRITE);
		g_object_class_install_property (object_class, PROP_WIDTH, pSpec);

		pSpec = g_param_spec_float ("height",
				"Height",
				"Height of the item",
				-G_MAXFLOAT,
				G_MAXFLOAT,
				0.0,
				G_PARAM_READWRITE);
		g_object_class_install_property ( object_class, PROP_HEIGHT, pSpec);
		pSpec = g_param_spec_float ("footer-width",
				"footer-width",
				"width of the footer",
				-G_MAXFLOAT,
				G_MAXFLOAT,
				0.0,
				G_PARAM_READWRITE);
		g_object_class_install_property ( object_class, PROP_FOOTER_WIDTH, pSpec);
	}

	static
	ClutterActor *createLabel(ClutterColor *text_color,
					gchar *font,
					gint x,gint y)
	{
		ClutterActor *text = clutter_text_new ();
		clutter_text_set_color (CLUTTER_TEXT (text), (const ClutterColor *)text_color);
		clutter_text_set_font_name (CLUTTER_TEXT (text), font);
		clutter_actor_set_position (text,x,y);
		return text;
	}
	static
	ClutterActor *createLine(ClutterColor *line_color,
					guint width,
					guint height,
					gint x,
					gint y)
	{
		ClutterActor *pLine = clutter_actor_new();
		clutter_actor_set_background_color(pLine,line_color);
		clutter_actor_set_size(pLine,width,height);
		clutter_actor_set_position(pLine,x,y);
		return pLine;
	}


	static void
	mildenhall_cabinet_item_init (MildenhallCabinetItem *self)
	{
		self->priv = CABINET_ITEM_PRIVATE (self);
		self->priv->iSortOrder = ARROW_NONE;
		self->priv->pFooterWidth = 0.0;
		ClutterColor text_color = {0x98, 0xA9, 0xB2, 0xFF};//{ 255, 255, 255, 255 };
		//ClutterColor line_horizon_color = {0xFF, 0xFF, 0xFF, 0x33};
		ClutterColor line_color = {0x00, 0x00, 0x00, 0x66};
		ClutterColor depthLineColor = {0xFF, 0xFF, 0xFF, 0x33};
		/* Number text */
		self->priv->NumberLabel = createLabel(&text_color,MILDENHALL_CABINET_ROLLER_ITEM_FONT(28),MILDENHALL_CABINET_NUM_LABEL_X,MILDENHALL_CABINET_NUM_LABEL_Y);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->NumberLabel);
		clutter_actor_hide (self->priv->NumberLabel);

		self->priv->pBlockedArrow = clutter_text_new ();
		clutter_text_set_color (CLUTTER_TEXT (self->priv->pBlockedArrow), &text_color);// &white);
		clutter_text_set_font_name (CLUTTER_TEXT (self->priv->pBlockedArrow), MILDENHALL_CABINET_ROLLER_ITEM_FONT(28));//"DejaVuSansCondensed 28px");
		clutter_text_set_editable (CLUTTER_TEXT (self->priv->pBlockedArrow), FALSE);
		clutter_text_set_use_markup (CLUTTER_TEXT (self->priv->pBlockedArrow), TRUE);
		clutter_text_set_markup (CLUTTER_TEXT (self->priv->pBlockedArrow), "&#x25B8;");//ROLLER_ARROW_MARKUP);
		//clutter_actor_set_position (CLUTTER_ACTOR (self->priv->pBlockedArrow),100,15.0);
		clutter_actor_set_y (self->priv->pBlockedArrow,MILDENHALL_CABINET_MID_LABEL_Y);
		clutter_actor_add_child (CLUTTER_ACTOR(self),self->priv->pBlockedArrow);
		clutter_actor_hide(self->priv->pBlockedArrow);

		self->priv->text = clutter_text_new ();
		clutter_text_set_color (CLUTTER_TEXT (self->priv->text), &text_color);// &white);
		clutter_text_set_font_name (CLUTTER_TEXT (self->priv->text), MILDENHALL_CABINET_ROLLER_ITEM_FONT(28));//"DejaVuSansCondensed 28px");
		clutter_text_set_editable (CLUTTER_TEXT (self->priv->text), FALSE);
		clutter_text_set_use_markup (CLUTTER_TEXT (self->priv->text), TRUE);
		clutter_text_set_markup (CLUTTER_TEXT (self->priv->text), "&#x25B9;");//ROLLER_ARROW_MARKUP);
		//clutter_actor_set_position (CLUTTER_ACTOR (text),30,
		clutter_actor_set_y (self->priv->text,MILDENHALL_CABINET_MID_LABEL_Y);
		clutter_actor_add_child (CLUTTER_ACTOR(self),self->priv->text);
		clutter_actor_hide(self->priv->text);

		/* Mid text */
		self->priv->label = createLabel(&text_color,g_strdup(MILDENHALL_CABINET_ROLLER_ITEM_FONT(28)),MILDENHALL_CABINET_MID_LABEL_X,MILDENHALL_CABINET_MID_LABEL_Y);

		clutter_text_set_editable(CLUTTER_TEXT(self->priv->label),FALSE);
		clutter_text_set_single_line_mode(CLUTTER_TEXT(self->priv->label),TRUE);
		clutter_text_set_ellipsize(CLUTTER_TEXT(self->priv->label),PANGO_ELLIPSIZE_MIDDLE);
		clutter_actor_set_width(CLUTTER_ACTOR(self->priv->label),MILDENHALL_CABINET_NORMAL_TEXT_WIDTH);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->label);
		clutter_actor_hide (self->priv->label);
		/* vertical line */
		/* these are for footers */
		self->priv->horizontal = createLine(&depthLineColor,MILDENHALL_CABINET_HORIZONTAL_WIDTH,MILDENHALL_CABINET_HORIZONTAL_HEIGHT,MILDENHALL_CABINET_HORIZONTAL_X,MILDENHALL_CABINET_HORIZONTAL_Y);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->horizontal);

		self->priv->horizontal_1 = createLine(&depthLineColor,MILDENHALL_CABINET_HORIZONTAL_WIDTH,MILDENHALL_CABINET_HORIZONTAL_HEIGHT,MILDENHALL_CABINET_HORIZONTAL_X,MILDENHALL_CABINET_HORIZONTAL_1_Y);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->horizontal_1);
		clutter_actor_hide(self->priv->horizontal_1);

		/* these are for footers */
		self->priv->vertical = createLine(&depthLineColor,MILDENHALL_CABINET_VERTICAL_WIDTH,MILDENHALL_CABINET_VERTICAL_FOOTER_HEIGHT,MILDENHALL_CABINET_VERTICAL_X,MILDENHALL_CABINET_VERTICAL_Y);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->vertical);
		clutter_actor_hide(self->priv->vertical);

		self->priv->depthVertical = createLine(&line_color,MILDENHALL_CABINET_VERTICAL_WIDTH,MILDENHALL_CABINET_VERTICAL_FOOTER_HEIGHT,MILDENHALL_CABINET_VERTICAL_X,MILDENHALL_CABINET_VERTICAL_Y);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->depthVertical);
		clutter_actor_hide(self->priv->depthVertical);

		ClutterColor semitransperent = {0xFF, 0xFF, 0xFF, 0x55};
		self->priv->iconRect = createLine(&semitransperent,82.0,66.0,0,0);
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->iconRect);
		/* extra horizontal */
		self->priv->extra_separator = createLine(&depthLineColor,MILDENHALL_CABINET_HORIZONTAL_WIDTH,MILDENHALL_CABINET_HORIZONTAL_HEIGHT,MILDENHALL_CABINET_HORIZONTAL_X,MILDENHALL_CABINET_HORIZONTAL_Y);//clutter_actor_new ();
		clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->extra_separator);
		clutter_actor_set_position (self->priv->extra_separator, 0, 65);
		clutter_actor_hide(self->priv->extra_separator);

		/* create hash table for overlay */
		self->priv->pOverlayTable = g_hash_table_new_full(g_str_hash, g_str_equal,g_free,g_free);
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_PICTURE_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_picture_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_BOOKMARK_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_bookmark_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_FOLDER_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_folder_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_MUSIC_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_music_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_VIDEO_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_video_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_DOC_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_document_active.png"));
		g_hash_table_insert(self->priv->pOverlayTable,g_strdup(MILDENHALL_CABINET_DELICOUS_OVERLAY_PATH),g_strdup(PKGTHEMEDIR"/icon_folder_delicous_active.png"));

		GError *error = NULL;
		self->priv->icon = thornbury_texture_new();
		if(error != NULL)
		{
			g_warning("err->message = %s\n",error->message);
			//g_print("create texture \n");
		}
			clutter_actor_set_position(self->priv->icon,0,0);
			clutter_actor_add_child (CLUTTER_ACTOR (self), self->priv->icon);
			clutter_actor_hide(self->priv->icon);

		self->priv->overlay = thornbury_texture_new ();
		clutter_actor_set_position(self->priv->overlay,0,0);
		clutter_actor_add_child (CLUTTER_ACTOR (self), self->priv->overlay);
		clutter_actor_hide(self->priv->overlay);


		clutter_actor_set_height(CLUTTER_ACTOR(self),67.0);
		clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);
	}

	MildenhallCabinetItem *
	mildenhall_cabinet_item_new (void)
	{
	  return g_object_new (MILDENHALL_TYPE_CABINET_ITEM, NULL);
	}

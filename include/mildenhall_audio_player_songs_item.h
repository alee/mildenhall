/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-audio-player-item.h */

#ifndef _MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_H
#define _MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/***********************************************************************************
*       @Filename       :       mildenhall_audio_player_songs_item.h
*       @Module         :       MildenhallAudioPlayerSongsItem
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       01/02/2013
*----------------------------------------------------------------------------------
*       @Description    :       Header file for mildenhall_audio_player_item type
*
************************************************************************************/

/***********************************************************************************
        @Includes
************************************************************************************/
#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM mildenhall_audio_player_songs_item_get_type()

#define MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM, MildenhallAudioPlayerSongsItem))

#define MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM, MildenhallAudioPlayerSongsItemClass))

#define MILDENHALL_IS_AUDIO_PLAYER_SONGS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM))

#define MILDENHALL_IS_AUDIO_PLAYER_SONGS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM))

#define MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM, MildenhallAudioPlayerSongsItemClass))

typedef struct _MildenhallAudioPlayerSongsItem MildenhallAudioPlayerSongsItem;
typedef struct _MildenhallAudioPlayerSongsItemClass MildenhallAudioPlayerSongsItemClass;
typedef struct _MildenhallAudioPlayerSongsItemPrivate MildenhallAudioPlayerSongsItemPrivate;

struct _MildenhallAudioPlayerSongsItem
{
  ClutterActor parent;
  //ClutterGroup parent;

  MildenhallAudioPlayerSongsItemPrivate *priv;
};

struct _MildenhallAudioPlayerSongsItemClass
{
  ClutterActorClass parent_class;
  //ClutterEffect *glow_effect_1;
  //ClutterEffect *glow_effect_2;
};

GType mildenhall_audio_player_songs_item_get_type (void) G_GNUC_CONST;

MildenhallAudioPlayerSongsItem *mildenhall_audio_player_songs_item_new (void);

G_END_DECLS

#endif /* _MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_H */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_button_drawer
 * @title: MildenhallButtonDrawer
 * @short_description: Drawer provides a swipe-based side panel to provide a central location for navigation.
 * @see_also: #ClutterActor
 *
 * #MildenhallButtonDrawer widget can be positioned at any of the four edges of the content item.
 * The drawer is then opened by "dragging" it from edges of the window.  
 * In general, it can be used as views drawer/menu's drawer.
 * It also has an ability to show left and right arrows which can be configured using multi-state property.
 * In addition, it provides tool-tip property using which the applications can show support text for the each menu item.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 * MildenhallButtonSpeller *button = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 *      MILDENHALL_TYPE_BUTTON_DRAWER
 *      "/usr/Application/MyApp/share/mildenhall_button_speller_text_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * button = MILDENHALL_BUTTON_SPELLER (object);
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (button));
 *
 * ]|
 *
 * Refer #ThornburyItemFactory for Thornbury Item Factory details.
 *
 * Since: 0.3.0
 */
#include "mildenhall_button_drawer.h"

typedef enum _enButtonDrawerProperty enButtonDrawerProperty;
typedef struct _MildenhallButtonDrawerPrivate MildenhallButtonDrawerPrivate;
typedef enum _enButtonDrawerSignals enButtonDrawerSignals;
typedef enum _enButtonDrawerDebugFlag enButtonDrawerDebugFlag;
typedef enum _enButtonDrawerBg enButtonDrawerBg;

/* Set the environment variable in terminal to enable traces: export MILDENHALL_BUTTON_DRAWER_DEBUG=mildenhall-button-drawer */
enum _enButtonDrawerDebugFlag
{
        MILDENHALL_BUTTON_DRAWER_DEBUG = 1 << 0,

};

/* property enums */
enum _enButtonDrawerProperty
{
        PROP_FIRST,
	PROP_WIDTH,
	PROP_HEIGHT,
        PROP_MULTI_STATE,
        PROP_TOOLTIP_WIDTH,
	PROP_SHOW_TOOLTIP,
	PROP_CURRENT_STATE,
	PROP_ACTIVE,
	PROP_STATE_MODEL,
	PROP_BG_STATE,
	PROP_VIEWS_DRAWER,
	PROP_LANG,
	PROP_LAST
};

/* The signals emitted by the mildenhall button drawer */
enum _enButtonDrawerSignals
{
        SIG_FIRST,
        SIG_STATE_CHANGED,                       /* Emitted when state changes */
	    SIG_SWIPE_END,
        SIG_LAST
};

/* names used to get images from hash */

//#define BG_INACTIVE		"bg-inactive"
//#define TOOLTIP_BG_INACTIVE	"tooltip-bg-inactive"
#define MENU_BG_SINGLE_NORMAL	"menu-bg-single-normal"
#define	MENU_BG_SINGLE_PRESSED "menu-bg-single-pressed"
#define	MENU_BG_OPEN_NORMAL   "menu-bg-open-normal"
#define	MENU_BG_OPEN_PRESSED	"menu-bg-open-pressed"
#define	MENU_BG_CLOSED_NORMAL	"menu-bg-closed-normal"
#define	MENU_BG_CLOSED_PRESSED	"menu-bg-closed-pressed"
#define	MENU_ARROW_LEFT		"menu-arrow-left"
#define	MENU_ARROW_RIGHT	"menu-arrow-right"
#define	VIEWS_BG_SINGLE_NORMAL	"views-bg-singal-normal"
#define	VIEWS_BG_SINGLE_PRESSED "views-bg-singal-pressed"
#define	VIEWS_BG_OPEN_NORMAL	"views-bg-open-normal"
#define	VIEWS_BG_OPEN_PRESSED	"views-bg-open-pressed"
#define	VIEWS_BG_CLOSED_NORMAL	"views-bg-closed-normal"
#define	VIEWS_BG_CLOSED_PRESSED "views-bg-closed-pressed"
#define	VIEWS_ARROW_LEFT	"views-arrow-left"
#define	VIEWS_ARROW_RIGHT	"views-arrow-right"

/* Storage for the signals */
static guint32 mildenhall_bt_drawer_signals[SIG_LAST] = {0,};

/* debug flag setting */
guint  mildenhall_button_drawer_debug_flags = 0;

static const GDebugKey mildenhall_button_drawer_debug_keys[] =
{
        { "mildenhall-button-drawer",   MILDENHALL_BUTTON_DRAWER_DEBUG }
};

#define MILDENHALL_BUTTON_DRAWER_HAS_DEBUG               ((mildenhall_button_drawer_debug_flags ) & 1)
#define MILDENHALL_BUTTON_DRAWER_PRINT( a ...) \
        if (G_LIKELY (MILDENHALL_BUTTON_DRAWER_HAS_DEBUG )) \
        {                               \
                g_print(a);           \
        }


G_DEFINE_TYPE (MildenhallButtonDrawer, mildenhall_button_drawer, LIGHTWOOD_TYPE_BUTTON)

#define MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_BUTTON_DRAWER, MildenhallButtonDrawerPrivate))


/* get background state as GType */
static GType mildenhall_button_drawer_bg_state_get_type (void) G_GNUC_CONST;
#define MILDENHALL_BUTTON_DRAWER_TYPE_BG_STATE (mildenhall_button_drawer_bg_state_get_type())

/* button drawer private structure */
struct _MildenhallButtonDrawerPrivate
{
	gfloat flWidth;			// width of the drawer button
	gfloat flHeight;		// height of the drawer button

	gboolean bMultiState;  		// Whether the button has more than one state
        gfloat flTooltipWidth;          // Width of the tooltip. Height will be same as button height
	gint64 inCurrentState;
	gint inTooltipFontWidth;
	gint inSourceID;		// time out ID for tooltip hide animation */
	gint inAnimationDuration;
	gint inThreshold;
	ClutterSwipeDirection hDirection;
        ClutterSwipeDirection vDirection;

	gboolean bActive;
	gboolean bShowTooltip;
	ThornburyModel *pStateModel;
	enBackgroundState enBgState;	// button background state as close/open/normal
	gboolean bViewsDrawer;
	gboolean bTooltipConstructed;
	gchar *pTooltipFont;
	GHashTable *pImageHash;

	ClutterColor tooltipRectColor;
	ClutterColor tooltipFontColor;
	ClutterColor buttonInactiveColor;
	ClutterColor tooltipInactiveFontColor;
	ClutterColor tooltipInactiveRectColor;

	ClutterActor *pButtonGroup;
	ClutterActor *pTooltipGroup;
	ClutterActor *pTooltipRect;

	ClutterActor *pBgNormal;
	ClutterActor *pBgPressed;
	ClutterActor *pBgInactive;	/* inactive rect */
	ClutterActor *pArrowLeft;
	ClutterActor *pArrowRight;
	ClutterActor *pIcon;
	ClutterActor *pTooltipText;
	//ClutterActor *pTooltipBgInactive;
	ClutterActor *pIconInactive;

	//ClutterAnimation *pTooltipShowAnim;
    	//ClutterAnimation *pTooltipHideAnim;

	ClutterTransition *pTooltipShowTrans;
	ClutterTransition *pTooltipHideTrans;

	gchar *language;
};

/* internal functions */
static gboolean gesture_begin(ClutterGestureAction *pAction, ClutterActor *pButtonDrawer, gpointer pUserData);
static gboolean gesture_progress (ClutterGestureAction *pAction, ClutterActor *pButtonDrawer);
static void gesture_end(ClutterGestureAction *pAction, ClutterActor *pButtonDrawer, gpointer pUserData);
static void v_button_drawer_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_press_cb(ClutterActor *pButtonDrawer, gpointer pUserData);
static void v_button_drawer_release_cb(ClutterActor *pButtonDrawer, gpointer pUserData);
static void v_button_drawer_swipe_cb(ClutterActor *pButtonDrawer, LightwoodSwipeDirection swipeDirection, gpointer pUserData);
static void v_button_drawer_check_multi_state(MildenhallButtonDrawer *pButtonDrawer, ThornburyModel *pModel);
static void v_button_drawer_update_view(MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_update_arrows(MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_tooltip_show (MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_tooltip_hide (MildenhallButtonDrawer *pButtonDrawer, gboolean bAnimate);
static void v_button_drawer_update_tooltip_width (MildenhallButtonDrawer *pButtonDrawer, gfloat flTooltipWidth);
static void v_button_drawer_background(MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_update_active(MildenhallButtonDrawer *pButtonDrawer);
static void v_button_drawer_set_tooltip_text(ClutterActor *pActor, const gchar *pTooltipText);
static gboolean clear_hash_entries(gpointer key, gpointer value, gpointer userData);

/**
 * mildenhall_button_drawer_set_tooltip_enabled:
 * @pButtonDrawer: button drawer object reference
 * @bEnabled: enable/disable tooltip as TRUE/FALSE
 *
 * Whether tooltip is associated with the drawer button
 */
void mildenhall_button_drawer_set_tooltip_enabled(MildenhallButtonDrawer *pButtonDrawer, gboolean bEnabled)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        if(NULL != priv)
        {
		g_object_set(pButtonDrawer, "tooltip-enabled", bEnabled, NULL);
        }
}

/**
 * mildenhall_button_drawer_set_tooltip_duration:
 * @pButtonDrawer: button drawer object reference
 * @inTooltipDuration: tooltip duration
 *
 * duration to show tooltip
 */
void mildenhall_button_drawer_set_tooltip_duration(MildenhallButtonDrawer *pButtonDrawer, gint inTooltipDuration)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        if(NULL != priv)
        {
		g_object_set(pButtonDrawer, "tooltip-duration", inTooltipDuration, NULL);
        }
}

/**
 * mildenhall_button_drawer_set_tooltip_direction:
 * @pButtonDrawer: button drawer object reference
 * @direction: tooltip direction as enum
 *
 * To which direction tooltip will be will be shown.
 */
void mildenhall_button_drawer_set_tooltip_direction(MildenhallButtonDrawer *pButtonDrawer, LightwoodTooltipDirection direction)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        if(NULL != priv)
        {
		g_object_set(pButtonDrawer, "tooltip-direction", direction, NULL);
        }
}

/**
 * mildenhall_button_drawer_set_multi_state:
 * @pObject: button drawer object reference
 * @bMultiState: multi state as TRUE/FALSE
 *
 * Set the multi state property as TRUE/FALSE. If multi state
 * is set as TRUE, left/right arrows will be shown and swipe
 * will get enabled to switch between states
 */
void mildenhall_button_drawer_set_multi_state(MildenhallButtonDrawer *pButtonDrawer, gboolean bMultiState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	if(priv->bMultiState != bMultiState)
	{
		if(bMultiState)
		{
			if(NULL != priv->pStateModel && thornbury_model_get_n_rows (priv->pStateModel) > 1)
			{
				priv->bMultiState = bMultiState;
			}
			else
			{
				g_warning("buton model does not have multi state\n");
				return;
			}
		}
		else
			priv->bMultiState = bMultiState;

		/* update the left/right arrows */
		v_button_drawer_update_arrows(pButtonDrawer);
	}

	g_object_notify (G_OBJECT(pButtonDrawer), "multi-state");
}


/**
 * mildenhall_button_drawer_set_tooltip_width:
 * @pButtonDrawer: button drawer object reference
 * @flWidth: tooltip width
 *
 * sets the tooltip width property for the tooltip rect. Tooltip
 * should be enabled to set this property
 */
void mildenhall_button_drawer_set_tooltip_width(MildenhallButtonDrawer *pButtonDrawer, gfloat flWidth)
{
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

       // MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pButtonDrawer);


	/* if width is same , ignore it */
	//if(priv->flTooltipWidth != flWidth)
	{
		/* update the tooltip width */
		v_button_drawer_update_tooltip_width(pButtonDrawer, flWidth);
	}
	g_object_notify (G_OBJECT(pButtonDrawer), "tooltip-width");
}

/**
 * mildenhall_button_drawer_set_current_state:
 * @pButtonDrawer: button drawer object reference
 * @inState: current state of the button
 *
 * If button has multi-state, this function is used to
 * change the state from one state to another
 */
void mildenhall_button_drawer_set_current_state(MildenhallButtonDrawer *pButtonDrawer, gint64 inState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }


	/* if state is same as previous , ignore it */
	if(priv->inCurrentState == inState)
		return;

	if(NULL == priv->pStateModel)
	{
		g_warning("model not set\n");
		return;
	}

	/* check if the state is within the rage of the given model */
	if(inState >= 0 && inState < thornbury_model_get_n_rows( priv->pStateModel ))
	{
		priv->inCurrentState = inState;
		/* update the view */
		v_button_drawer_update_view(pButtonDrawer);
		/* emit the signal for state changed with the new state info */
		g_signal_emit(pButtonDrawer, mildenhall_bt_drawer_signals[SIG_STATE_CHANGED], 0, inState);
		g_object_notify (G_OBJECT(pButtonDrawer), "current-state");
	}
	else
		g_warning("state out of range\n");
}

/**
 * mildenhall_button_drawer_set_model:
 * @pButtonDrawer: button drawer object reference
 * @pModel: model for the button drawer
 *
 * Model contains the data to be displayed on the butoon.
 * Drawer button model contains:
 *			1. icon image path
 *			2. tooltip text
 * If button has multi state, there should be more than one row
 * with above mentioned columns
 */
void mildenhall_button_drawer_set_model(MildenhallButtonDrawer *pButtonDrawer, ThornburyModel *pModel)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }
	/* if model is set again, remove previous model */
        if(NULL != priv->pStateModel)
        {
		g_signal_handlers_disconnect_by_func (priv->pStateModel,
                                            G_CALLBACK (v_button_drawer_row_added_cb),
                                            pButtonDrawer);
		g_signal_handlers_disconnect_by_func (priv->pStateModel,
				G_CALLBACK (v_button_drawer_row_changed_cb),
				pButtonDrawer);
		g_signal_handlers_disconnect_by_func (priv->pStateModel,
				G_CALLBACK (v_button_drawer_row_removed_cb),
				pButtonDrawer);
		g_object_unref (priv->pStateModel);

		priv->pStateModel = NULL;

	}

	/* update the new model with signals */
	if (pModel != NULL)
    	{
      		g_return_if_fail (G_IS_OBJECT (pModel));

	      	priv->pStateModel = g_object_ref (pModel);

		g_signal_connect (priv->pStateModel,
                    "row-added",
                    G_CALLBACK (v_button_drawer_row_added_cb),
                    pButtonDrawer);

  		g_signal_connect (priv->pStateModel,
                    "row-changed",
                    G_CALLBACK (v_button_drawer_row_changed_cb),
                    pButtonDrawer);

		g_signal_connect (priv->pStateModel,
                    "row-removed",
                    G_CALLBACK (v_button_drawer_row_removed_cb),
                    pButtonDrawer);

        }
	/* if model has more than one state, update multi state property */
	v_button_drawer_check_multi_state(pButtonDrawer, priv->pStateModel);
	/* update the view */
	v_button_drawer_update_view(pButtonDrawer);

	clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonDrawer));
	mildenhall_button_drawer_set_language(pButtonDrawer, g_strdup(priv->language));
	g_object_notify (G_OBJECT(pButtonDrawer), "model");
}

/**
 * mildenhall_button_drawer_set_bg_state:
 * @pButtonDrawer: button drawer object reference
 * @bgState: background state
 *
 * Drawer button has three states for background: open/normal/closed
 * Based on background state, background view will get changed
 */
void mildenhall_button_drawer_set_bg_state(MildenhallButtonDrawer *pButtonDrawer, enBackgroundState bgState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }


	priv->enBgState = bgState;
	/* set the bg state */
	v_button_drawer_background(pButtonDrawer);
	g_object_notify (G_OBJECT(pButtonDrawer), "background-state");
}

/**
 * mildenhall_button_drawer_set_views_drawer:
 * @pButtonDrawer: button drawer object reference
 * @bState: whether drawer button is views drawer/menu drawer button
 *
 * based on state, view will get changed: background and arrows
 */
void mildenhall_button_drawer_set_views_drawer(MildenhallButtonDrawer *pButtonDrawer, gboolean bState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	/* update the view based on set value */
	if(priv->bViewsDrawer != bState)
	{
	        priv->bViewsDrawer = bState;

		/* update the bg */
		v_button_drawer_background(pButtonDrawer);

		/* update the arrows */
		v_button_drawer_update_arrows(pButtonDrawer);
        }
	g_object_notify (G_OBJECT(pButtonDrawer), "views-drawer");
}

/**
 * mildenhall_button_drawer_set_active:
 * @pButtonDrawer: button drawer object reference
 * @bState: whether drawer button is views drawer/menu drawer button
 *
 * based on state, view will get changed: background and arrows
 */
void mildenhall_button_drawer_set_active(MildenhallButtonDrawer *pButtonDrawer, gboolean bState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        /* update the view based on set value */
        priv->bActive = bState;

	if(clutter_actor_get_reactive(CLUTTER_ACTOR(pButtonDrawer) )== FALSE)
		clutter_actor_set_reactive(CLUTTER_ACTOR(pButtonDrawer), TRUE);
	v_button_drawer_update_active(pButtonDrawer);

        g_object_notify (G_OBJECT(pButtonDrawer), "active");
}

/**
 * mildenhall_button_drawer_set_tooltip_show:
 * @pButtonDrawer: button drawer object reference
 * @bShowState: tooltip show/hide state
 *
 * Based on state,tooltip wiil be shown or hidden.
 * tooltip needs to be enabled to use this property
 */
void mildenhall_button_drawer_set_tooltip_show(MildenhallButtonDrawer *pButtonDrawer, gboolean bShowState)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        gboolean bEnabled;
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	/* if tooltip is not enalbed, return */
        g_object_get(pButtonDrawer, "tooltip-enabled", &bEnabled, NULL);
        if(!bEnabled || !priv->bTooltipConstructed)
        {
                g_warning ("Tooltip disabled / not constructed !");
                return;
        }

	/* if its already set, ignore the state */
	if(priv->bShowTooltip != bShowState)
	{
		priv->bShowTooltip = bShowState;
		if(priv->bShowTooltip)
			v_button_drawer_tooltip_show(pButtonDrawer);
		else
			v_button_drawer_tooltip_hide(pButtonDrawer, FALSE);
	}
	g_object_notify (G_OBJECT(pButtonDrawer), "show-tooltip");
}

/**
 * mildenhall_button_drawer_set_language:
 * @pButtonDrawer: button drawer object reference
 * @pLanguage: language to be set
 *
 * Function to update text on set language
 *
 * Returns: language to be set
 *
 */
void mildenhall_button_drawer_set_language(MildenhallButtonDrawer *pButtonDrawer, gchar *pLanguage)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	GValue value = {0,};
	ThornburyModelIter *pIter = NULL;
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
		return;
        }

	if(NULL == pLanguage)
		return;

	if(NULL != priv->language)
	{
		g_free(priv->language);
    		priv->language = NULL;
	}
	/* update language */
	priv->language = g_strdup(pLanguage);

	pIter = thornbury_model_get_iter_at_row (priv->pStateModel, 0);
	thornbury_model_iter_get_value(pIter, 1, &value);
	if(g_value_get_string(&value)!= NULL)
	{
		gchar *pText = gettext((gchar*)g_value_get_string(&value));
		if(pText)
			v_button_drawer_set_tooltip_text(CLUTTER_ACTOR(pButtonDrawer), pText);
		// g_print("string is %s %s",g_value_get_string(&value),_(g_value_get_string(&value)));
	}
}

/**
 * mildenhall_button_drawer_get_width:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get the drawer button width
 *
 * Returns: width of the drawer button
 *
 */
gfloat mildenhall_button_drawer_get_width( MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

	return priv->flWidth;

}

/**
 * mildenhall_button_drawer_get_height:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get the drawer button height
 *
 * Returns: height drawer button
 */
gfloat mildenhall_button_drawer_get_height( MildenhallButtonDrawer *pButtonDrawer)
{
       MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
       if(!MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

	return priv->flHeight;
}

/**
 * mildenhall_button_drawer_get_tooltip_enabled:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get tooltip enable state as TRUE/FALSE
 *
 * Returns: tooltip enabled or not
 */
gboolean mildenhall_button_drawer_get_tooltip_enabled(MildenhallButtonDrawer *pButtonDrawer)
{
        gboolean bEnabled = FALSE;
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return FALSE;
        }

        if(NULL != priv)
        {
		g_object_get(pButtonDrawer, "tooltip-enabled", &bEnabled, NULL);

        }
	return bEnabled;
}

/**
 * mildenhall_button_drawer_get_tooltip_duration:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get tooltip duration
 *
 * Returns: tooltip duration
 */
gint mildenhall_button_drawer_get_tooltip_duration(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	guint uinDuration = -1;
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

        if(NULL != priv)
        {
		g_object_get(pButtonDrawer, "tooltip-duration", &uinDuration, NULL);
        }
	return uinDuration;
}

/**
 * mildenhall_button_drawer_get_tooltip_direction:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get the tooltip direction.
 *
 * Returns: tooltip direction value as enum
 */
LightwoodTooltipDirection mildenhall_button_drawer_get_tooltip_direction(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	LightwoodTooltipDirection direction = LIGHTWOOD_TOOLTIP_LEFT;
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

        if(NULL != priv)
        {
		g_object_get(pButtonDrawer, "tooltip-direction", &direction, NULL);
        }
	return direction;
}

/**
 * mildenhall_button_drawer_get_multi_state:
 * @pButtonDrawer: button drawer object reference
 *
 * whether multi state is set or not
 *
 * Returns: TRUE/FALSE
 */
gboolean mildenhall_button_drawer_get_multi_state(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return FALSE;
        }

	return priv->bMultiState;
}

/**
 * mildenhall_button_drawer_get_tooltip_width:
 * @pButtonDrawer: button drawer object reference
 *
 * Function to get the tooltip width
 *
 * Returns: tooltip width
 */
gfloat mildenhall_button_drawer_get_tooltip_width(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

        return priv->flTooltipWidth;
}

/**
 * mildenhall_button_drawer_get_current_state:
 * @pButtonDrawer: button drawer object reference
 *
 * To get the current state of the button. Useful if
 * button has multi-state.
 *
 * Returns: current state
 */
gint mildenhall_button_drawer_get_current_state(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

        return priv->inCurrentState;
}

/**
 * mildenhall_button_drawer_get_model:
 * @pButtonDrawer: button drawer object reference
 *
 * To get the model having drawer button data
 *
 * Returns: drawer button model
 */
ThornburyModel *mildenhall_button_drawer_get_model(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return NULL;
        }

        return priv->pStateModel;
}

/**
 * mildenhall_button_drawer_get_bg_state:
 * @pButtonDrawer: button drawer object reference
 *
 * Current background state of the drawer button
 *
 * Returns: background state
 */
enBackgroundState mildenhall_button_drawer_get_bg_state(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return -1;
        }

        return priv->enBgState;
}

/**
 * mildenhall_button_drawer_get_views_drawer:
 * @pButtonDrawer: button drawer object reference
 *
 * Whether button is for views rawer/menu drawer
 *
 * Returns: TRUE/FALSE
 */
gboolean mildenhall_button_drawer_get_views_drawer(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return FALSE;
        }

        return  priv->bViewsDrawer;

}

/**
 * mildenhall_button_drawer_get_tooltip_show:
 * @pButtonDrawer: button drawer object reference
 *
 * To get the tooltip-show property value
 *
 * Returns: TRUE/FALSE
 */
gboolean mildenhall_button_drawer_get_tooltip_show(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        if(!MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return FALSE;
        }

        return  priv->bShowTooltip;
}

/**
 * mildenhall_button_drawer_new:
 *
 * Creates a drawer button object
 *
 * Returns: (transfer full): Drawer button object
 */
ClutterActor *mildenhall_button_drawer_new (void)
{
	return g_object_new (MILDENHALL_TYPE_BUTTON_DRAWER, NULL);
}



/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function :v_button_drawer_set_width
 * Description: set button drawer width internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_width(MildenhallButtonDrawer *pButtonDrawer, gfloat flWidth)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        if(priv->flWidth != flWidth)
        {
		clutter_actor_set_width(CLUTTER_ACTOR(pButtonDrawer), flWidth);
                priv->flWidth = flWidth;
        }
}

/********************************************************
 * Function :v_button_drawer_set_height
 * Description: set button drawer height internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_height(MildenhallButtonDrawer *pButtonDrawer, gfloat flHeight)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

        if(priv->flHeight != flHeight)
        {
		clutter_actor_set_height(CLUTTER_ACTOR(pButtonDrawer), flHeight);
                priv->flHeight = flHeight;
        }
}

/********************************************************
 * Function : mildenhall_button_drawer_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	MildenhallButtonDrawer *pButtonDrawer = MILDENHALL_BUTTON_DRAWER (pObject);
	 if(!MILDENHALL_IS_BUTTON_DRAWER(pObject))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	switch (uinPropertyID)
	{
		case PROP_WIDTH:
                        v_button_drawer_set_width(pButtonDrawer, g_value_get_float (pValue) );
                        break;
                case PROP_HEIGHT:
                        v_button_drawer_set_height(pButtonDrawer, g_value_get_float (pValue) );
                        break;
                case PROP_MULTI_STATE:
			mildenhall_button_drawer_set_multi_state(pButtonDrawer, g_value_get_boolean (pValue) );
                        break;
                case PROP_TOOLTIP_WIDTH:
			mildenhall_button_drawer_set_tooltip_width(pButtonDrawer, g_value_get_float (pValue) );
                        break;
                case PROP_SHOW_TOOLTIP:
			mildenhall_button_drawer_set_tooltip_show(pButtonDrawer, g_value_get_boolean (pValue) );
                        break;
                case PROP_CURRENT_STATE:
			mildenhall_button_drawer_set_current_state(pButtonDrawer, g_value_get_int64 (pValue) );
                        break;
                case PROP_STATE_MODEL:
			 mildenhall_button_drawer_set_model(pButtonDrawer, g_value_get_object (pValue) );
                        break;
		case PROP_BG_STATE:
                        mildenhall_button_drawer_set_bg_state(pButtonDrawer, g_value_get_enum (pValue) );
                        break;
		case PROP_VIEWS_DRAWER:
                        mildenhall_button_drawer_set_views_drawer(pButtonDrawer, g_value_get_boolean (pValue) );
                        break;
		case PROP_ACTIVE:
                        mildenhall_button_drawer_set_active(pButtonDrawer, g_value_get_boolean (pValue) );
                        break;
		case PROP_LANG:
			mildenhall_button_drawer_set_language(pButtonDrawer, (gchar *)g_value_get_string(pValue));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
	MildenhallButtonDrawer *pButtonDrawer = MILDENHALL_BUTTON_DRAWER (pObject);
	if(!MILDENHALL_IS_BUTTON_DRAWER(pObject))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	switch (uinPropertyID)
	{

		case PROP_WIDTH:
                        g_value_set_float (pValue, mildenhall_button_drawer_get_width(pButtonDrawer) );
                        break;
                case PROP_HEIGHT:
                        g_value_set_float (pValue, mildenhall_button_drawer_get_height(pButtonDrawer) );
                        break;
		case PROP_MULTI_STATE:
			g_value_set_boolean (pValue, mildenhall_button_drawer_get_multi_state(pButtonDrawer) );
			break;
		case PROP_TOOLTIP_WIDTH:
			g_value_set_float (pValue, mildenhall_button_drawer_get_tooltip_width(pButtonDrawer) );
			break;
		case PROP_SHOW_TOOLTIP:
			g_value_set_boolean (pValue, mildenhall_button_drawer_get_tooltip_show(pButtonDrawer) );
			break;
		case PROP_CURRENT_STATE:
			g_value_set_int64 (pValue, mildenhall_button_drawer_get_current_state(pButtonDrawer));
			break;
		case PROP_STATE_MODEL:
			g_value_set_object (pValue, mildenhall_button_drawer_get_model(pButtonDrawer) );
			break;
		case PROP_BG_STATE:
			g_value_set_enum (pValue, mildenhall_button_drawer_get_bg_state(pButtonDrawer) );
			break;
		case PROP_VIEWS_DRAWER:
                        g_value_set_boolean (pValue, mildenhall_button_drawer_get_views_drawer(pButtonDrawer) );
                        break;
		case PROP_ACTIVE:
                        g_value_set_boolean (pValue, pButtonDrawer->priv->bActive );
                        break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_unref_object
 * Description: unref object pointer
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_unref_object(GObject *pObj)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(G_IS_OBJECT(pObj))
	{
		g_object_unref (pObj);
		pObj = NULL;
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_free_str_on_dispose
 * Description: free gchar pointer
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_free_str_on_dispose(GObject *pObject)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pObject);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(NULL != priv->pTooltipFont)
	{
		g_free(priv->pTooltipFont);
		priv->pTooltipFont = NULL;
	}
	if(NULL != priv->language)
	{
		g_free(priv->language);
		priv->language = NULL;
	}
}

/********************************************************
 * Function : v_button_drawer_free_str
 * Description: free gchar pointer
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_button_drawer_free_str(gchar *pStr)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(NULL != pStr)
	{
		g_free(pStr);
		pStr = NULL;
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_dispose
 * Description: Dispose the mildenhall button drawer object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_dispose (GObject *pObject)
{

	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pObject);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_button_drawer_parent_class)->dispose (pObject);

	if(NULL != priv)
	{
		if(NULL != priv->pImageHash)
		{
			/* TBD : free each key and value */
			g_hash_table_foreach_remove(priv->pImageHash, (GHRFunc)clear_hash_entries, MILDENHALL_BUTTON_DRAWER(pObject));
			g_hash_table_destroy(priv->pImageHash);
			priv->pImageHash = NULL;
		}

		mildenhall_button_drawer_free_str_on_dispose(pObject);
		/* unref object */
		mildenhall_button_drawer_unref_object((GObject*)priv->pStateModel);
		mildenhall_button_drawer_unref_object((GObject*)priv->pTooltipHideTrans);
		mildenhall_button_drawer_unref_object((GObject*)priv->pTooltipShowTrans);
# if 0
		if(NULL != priv->pStateModel)
                {
                        g_object_unref (priv->pStateModel);
                        priv->pStateModel = NULL;
                }
		if(NULL != priv->pTooltipHideTrans)
		{
			g_object_unref(priv->pTooltipHideTrans);
			priv->pTooltipHideTrans = NULL;
		}
		if(NULL != priv->pTooltipShowTrans)
		{
			g_object_unref(priv->pTooltipShowTrans);
			priv->pTooltipShowTrans = NULL;
		}
# endif
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_finalize
 * Description: Finalize the mildenhall button drawerobject
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_button_drawer_parent_class)->finalize (pObject);
}

/********************************************************
 * Function :   en_get_left_right_direction
 * Description: returns left/right swipe direction
 * Parameters: gfloat, gfloat
 * Return value: LightwoodSwipeDirection
 ********************************************************/
static LightwoodSwipeDirection en_get_left_right_direction(gfloat flPressX, gfloat flReleaseX)
{
        LightwoodSwipeDirection enDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;

        /* if pressed Y coord is greater than the released one, direction will be up
         * otherwise down */
        if(flPressX >= flReleaseX)
        {
                MILDENHALL_BUTTON_DRAWER_PRINT("direction is left\n");
                enDirection = LIGHTWOOD_SWIPE_DIRECTION_LEFT;
        }
        else
        {
                MILDENHALL_BUTTON_DRAWER_PRINT("direction is right\n");
                enDirection = LIGHTWOOD_SWIPE_DIRECTION_RIGHT;
        }
        return enDirection;
}

/********************************************************
 * Function :   en_get_button_drawer_direction
 * Description: returns up/down swipe direction
 * Parameters: gfloat, gfloat
 * Return value: LightwoodSwipeDirection
 ********************************************************/
static LightwoodSwipeDirection en_get_button_drawer_direction(gfloat flPressY, gfloat flReleaseY, gfloat flPressX, gfloat flReleaseX, gfloat angle, gboolean bMultiState)
{
        LightwoodSwipeDirection enDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
	if(! bMultiState)
	{
		/* for up/down direction, angle varies between -0 to -90 and 0 to 90 */
                /* if pressed Y coord is greater than the released one, direction will be up
                 * otherwise down */
		if(flPressY >= flReleaseY)
                {
                        MILDENHALL_BUTTON_DRAWER_PRINT("direction is up\n");
                        enDirection = LIGHTWOOD_SWIPE_DIRECTION_UP;
                }
                else
                {
                        MILDENHALL_BUTTON_DRAWER_PRINT("direction is down\n");
                        enDirection = LIGHTWOOD_SWIPE_DIRECTION_DOWN;
                }

	}
        else if( (angle <= -45 && angle >= -90) || (angle >= 45 && angle <= 90) )
        {
                /* if pressed Y coord is greater than the released one, direction will be up
                 * otherwise down */
                if(flPressY >= flReleaseY)
                {
                        MILDENHALL_BUTTON_DRAWER_PRINT("direction is up\n");
                        enDirection = LIGHTWOOD_SWIPE_DIRECTION_UP;
                }
                else
                {
                        MILDENHALL_BUTTON_DRAWER_PRINT("direction is down\n");
                        enDirection = LIGHTWOOD_SWIPE_DIRECTION_DOWN;
                }
        }
        else
        {
                enDirection = en_get_left_right_direction(flPressX, flReleaseX);
        }
        return enDirection;
}


/*******************************************************************
 *Function: clear_hash_entries
 *Description: Clear all hash table entries
 *Paramters: Key, value, userdata
 *Return Value: Bool (To remove or not)
 *******************************************************************/
static gboolean clear_hash_entries(gpointer key, gpointer value, gpointer userData)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	v_button_drawer_free_str( (gchar *)key);
	v_button_drawer_free_str(  (gchar *)value);
# if 0
        if(NULL != key)
        {
		MILDENHALL_BUTTON_DRAWER_PRINT("%s\n", (gchar *)key);
                g_free(key);
                key = NULL;
        }

        if(NULL != value)
        {
		MILDENHALL_BUTTON_DRAWER_PRINT("%s\n", (gchar *)value);
                g_free(value);
                value = NULL;
        }
#endif
        return TRUE;
}

/*******************************************************************
 * Function : v_button_drawer_update_tooltip_color
 * Description: update the tooltip rect and text color based in active state
 * Parameters: The object
 * Return value: void
 ******************************************************************/
static void
v_button_drawer_update_tooltip_color (MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button\n");
                return;
        }

	if(NULL != priv->pTooltipRect && NULL != priv->pTooltipText)
	{
		/* update the tooltip fontand rect color */
		if(!priv->bActive)
		{
			g_object_set(priv->pTooltipRect, "background-color", &priv->tooltipInactiveRectColor, NULL);
			g_object_set(priv->pTooltipText, "color", &priv->tooltipInactiveFontColor, NULL);
			clutter_actor_set_opacity(priv->pIcon, 0xBB);
		}
		else
		{

			g_object_set(priv->pTooltipRect, "background-color", &priv->tooltipRectColor, NULL);
			g_object_set(priv->pTooltipText, "color", &priv->tooltipFontColor, NULL);
			clutter_actor_set_opacity(priv->pIcon, 0xFF);
		}
	}
}

/*******************************************************************
 * Function : v_button_drawer_create_inactive_bg
 * Description: update the inactive bg
 * Parameters: The object
 * Return value: void
 ******************************************************************/
static void v_button_drawer_create_inactive_bg(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);

        MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(NULL == priv->pBgInactive)
                {
                        priv->pBgInactive = clutter_actor_new();
                        g_object_set(priv->pBgInactive, "background-color", &priv->buttonInactiveColor, NULL);
                        clutter_actor_insert_child_at_index(priv->pButtonGroup, priv->pBgInactive, 0);
                        if(NULL != priv->pBgNormal)
                        {
                                clutter_actor_set_size(priv->pBgInactive, clutter_actor_get_width(priv->pBgNormal), clutter_actor_get_height(priv->pBgNormal));
                                clutter_actor_hide(priv->pBgNormal);
                        }
                }
                else
                {
                        /* hide normal, show inactive */
                        clutter_actor_show(priv->pBgInactive);
                        if(NULL != priv->pBgNormal)
                                clutter_actor_hide(priv->pBgNormal);
                }
}

/*******************************************************************
 * Function : v_button_drawer_update_active
 * Description: update the view based on button active state
 * Parameters: The object, BgNormal image path, BgPressed image path
 * Return value: void
 ******************************************************************/
static void v_button_drawer_update_active(MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        gboolean bState;
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	/* update button view */
	/* if active, hide the inactive bg and update the tootip color if enabled */
	if( priv->bActive)
	{
		if(NULL != priv->pBgNormal)
			clutter_actor_show(priv->pBgNormal);
		if(NULL != priv->pBgInactive)
			clutter_actor_hide(priv->pBgInactive);
	}
	else
	{
		/* if inactive bg is not created, create it */
		v_button_drawer_create_inactive_bg(pButtonDrawer);
	}
	/* tooltip is enabled, create inactive bg for and change the tooltip text font color */
	g_object_get(pButtonDrawer, "tooltip-enabled", &bState, NULL);

	if(bState)
	{
		/* update the tooltip font color */
		v_button_drawer_update_tooltip_color(pButtonDrawer);
	}

}

/********************************************************
 * Function : v_button_drawer_set_bg_size
 * Description: update the background size
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_bg_size(MildenhallButtonDrawer *pButtonDrawer, ClutterActor *pBg)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pButtonDrawer);
	/* if button width/height ie not set, update it with bg image width/height */
	if(priv->flWidth)
		g_object_set(pBg, "width", priv->flWidth, NULL);
	else
		v_button_drawer_set_width(pButtonDrawer, clutter_actor_get_width(pBg) );

	if(priv->flHeight)
		g_object_set(pBg, "height", priv->flHeight, NULL);
	else
		v_button_drawer_set_height(pButtonDrawer, clutter_actor_get_height(pBg) );
}

/********************************************************
 * Function : v_button_drawer_update_bg
 * Description: update the background based on button state and drawer type
 * Parameters: The object, BgNormal image path, BgPressed image path
 * Return value: void
 ********************************************************/
static void
v_button_drawer_update_bg (MildenhallButtonDrawer *pButtonDrawer, const gchar *pBgNormal, const gchar *pBgPressed)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pButtonDrawer);
	gchar *pNormalImage = NULL;
	gchar *pPressedImage = NULL;

	if(NULL == priv->pImageHash)
		return;

	/* lookup for normal image in the hash */
	pNormalImage = g_hash_table_lookup(priv->pImageHash, pBgNormal);
	pPressedImage = g_hash_table_lookup(priv->pImageHash, pBgPressed);

	/* initialise the bg textures */
	if(NULL == priv->pBgNormal)
	{
		if(NULL != pNormalImage)
		{
			priv->pBgNormal = thornbury_ui_texture_create_new(pNormalImage, 0, 0, FALSE, FALSE);
			//if(clutter_actor_get_parent (priv->pBgNormal) == NULL)
			clutter_actor_add_child(priv->pButtonGroup, priv->pBgNormal);
		}
	}
	else
	{
		thornbury_ui_texture_set_from_file(priv->pBgNormal, pNormalImage, 0, 0, FALSE, FALSE);
		/* if button width/height ie not set, update it with bg image width/height */
		v_button_drawer_set_bg_size(pButtonDrawer, priv->pBgNormal);
	}
	if(NULL == priv->pBgPressed)
	{
		if(NULL != pPressedImage)
		{
			priv->pBgPressed = thornbury_ui_texture_create_new(pPressedImage, 0, 0, FALSE, FALSE);
			//if(clutter_actor_get_parent (priv->pBgPressed) == NULL)
			{
				clutter_actor_add_child(priv->pButtonGroup, priv->pBgPressed);
				clutter_actor_hide(priv->pBgPressed);
			}
		}
	}
	else
	{
		thornbury_ui_texture_set_from_file(priv->pBgPressed, pPressedImage, 0, 0, FALSE, FALSE);
		/* if button width/height ie not set, update it with bg image width/height */
		v_button_drawer_set_bg_size(pButtonDrawer, priv->pBgPressed);
	}
}

/********************************************************
 * Function : v_button_drawer_background
 * Description: call update background based on button state and drawer type
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_button_drawer_background(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pButtonDrawer);

	/* set the background */
	if(priv->bViewsDrawer)
	{
		/* views drawer bg */
		switch(priv->enBgState)
		{
			case MILDENHALL_BUTTON_DRAWER_BG_OPEN:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: VIEWS MILDENHALL_BUTTON_DRAWER_BG_OPEN\n");
				v_button_drawer_update_bg(pButtonDrawer, VIEWS_BG_OPEN_NORMAL, VIEWS_BG_OPEN_PRESSED);
				break;
			case MILDENHALL_BUTTON_DRAWER_BG_CLOSE:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: VIEWS MILDENHALL_BUTTON_DRAWER_BG_CLOSE\n");
				v_button_drawer_update_bg(pButtonDrawer, VIEWS_BG_CLOSED_NORMAL, VIEWS_BG_CLOSED_PRESSED);
				break;
			case MILDENHALL_BUTTON_DRAWER_BG_NORMAL:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: VIEWS MILDENHALL_BUTTON_DRAWER_BG_NORMAL\n");
				v_button_drawer_update_bg(pButtonDrawer, VIEWS_BG_SINGLE_NORMAL, VIEWS_BG_SINGLE_PRESSED);
				break;
			default:
				break;
		}
	}
	else
	{
		/* menu drawer bg */
		switch(priv->enBgState)
		{
			case MILDENHALL_BUTTON_DRAWER_BG_OPEN:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: MENU MILDENHALL_BUTTON_DRAWER_BG_OPEN\n");
				v_button_drawer_update_bg(pButtonDrawer, MENU_BG_OPEN_NORMAL, MENU_BG_OPEN_PRESSED);
				break;
			case MILDENHALL_BUTTON_DRAWER_BG_CLOSE:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: MENU MILDENHALL_BUTTON_DRAWER_BG_CLOSE\n");
				v_button_drawer_update_bg(pButtonDrawer, MENU_BG_CLOSED_NORMAL, MENU_BG_CLOSED_PRESSED);
				break;
			case MILDENHALL_BUTTON_DRAWER_BG_NORMAL:
				MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: MENU MILDENHALL_BUTTON_DRAWER_BG_NORMAL\n");
				v_button_drawer_update_bg(pButtonDrawer, MENU_BG_SINGLE_NORMAL, MENU_BG_SINGLE_PRESSED);
				break;
			default:
				break;

		}
	}
}

/********************************************************
 * Function : v_button_drawer_set_icon_opacity
 * Description: set icon opacity
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_icon_opacity(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(priv->pBgInactive)
		clutter_actor_set_opacity(priv->pIcon, 0x33);
	else
		clutter_actor_set_opacity(priv->pIcon, 0xFF);
}

/********************************************************
 * Function : v_button_drawer_set_icon
 * Description: set the icon
 * Parameters: The object, pIconPath
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_icon(ClutterActor *pActor, const gchar *pIconPath)
{
	MildenhallButtonDrawer *pButtonDrawer = MILDENHALL_BUTTON_DRAWER (pActor);
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	gfloat flX, flY;

	gchar *icon = g_strdup (pIconPath);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	/* first time texture creation */
	if(NULL == priv->pIcon)
	{
		priv->pIcon = thornbury_ui_texture_create_new(icon, 0, 0, FALSE, FALSE);
		clutter_actor_add_child(priv->pButtonGroup, priv->pIcon);
	}
	else
	{
		thornbury_ui_texture_set_from_file(priv->pIcon, icon, 0, 0, FALSE, FALSE);
	}
	v_button_drawer_free_str(icon);
	v_button_drawer_set_icon_opacity(pButtonDrawer);

# if 0
	/* update the texture with new icon path */
	gchar *icon = g_strdup_printf(PKGTHEMEDIR"/%s", pIconPath);
	thornbury_ui_texture_set_from_file(
	CoglHandle icon1 = cogl_texture_new_from_file (icon, COGL_TEXTURE_NONE, COGL_PIXEL_FORMAT_ANY, NULL);
	clutter_texture_set_cogl_texture(CLUTTER_TEXTURE(priv->pIcon), icon1);
	g_object_set(priv->pIcon, "height", (gfloat)cogl_texture_get_height(icon1), "width", (gfloat)cogl_texture_get_width(icon1), NULL);
# endif
	/* if icon size exceeds, restrict to max */
	if(clutter_actor_get_height(priv->pIcon) > priv->flHeight)
		g_object_set(priv->pIcon, "height", priv->flHeight, NULL);
	if(clutter_actor_get_width(priv->pIcon) > priv->flWidth)
		g_object_set(priv->pIcon, "width", priv->flWidth, NULL);

	/* set the position of icon to mid of the button */
	flX = (priv->flWidth-clutter_actor_get_width (priv->pIcon)) / 2;
	flY = (priv->flHeight-clutter_actor_get_height (priv->pIcon)) / 2;
	g_object_set(priv->pIcon, "x", flX, "y", flY, NULL);
}

/********************************************************
 * Function : v_button_drawer_tooltip_hide_completed_cb
 * Description: callback on tooltip hide completion
 * Parameters: ClutterAnimation *, The object
 * Return value: void
 ********************************************************/
//static void v_button_drawer_tooltip_hide_completed_cb (ClutterAnimation *pAnimation, ClutterActor *pButtonDrawer)
static void v_button_drawer_tooltip_hide_completed_cb (ClutterTimeline *pAnimation, ClutterActor *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER (pButtonDrawer)->priv;
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
	{
		g_warning ("Invalid button");
                return;
	}

        clutter_actor_hide (priv->pTooltipGroup);

	/* emit the tooltip hide signal */

	priv->bShowTooltip = FALSE;
        //priv->pTooltipHideAnim = NULL;
	//priv->pTooltipHideTrans = NULL;

	lightwood_button_set_tooltip_showing (LIGHTWOOD_BUTTON (pButtonDrawer), FALSE);
}

static void
on_show_transition_stopped (ClutterActor *actor,
                       const gchar  *transition_name,
                       gboolean      is_finished,
		       MildenhallButtonDrawer  *pButtonDrawer)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s \n", __FUNCTION__);
	/* disconnect so we don't get multiple notifications */
	g_signal_handlers_disconnect_by_func (actor,
			on_show_transition_stopped,
                                        pButtonDrawer);
}

static void
on_hide_transition_stopped (ClutterActor *actor,
                       const gchar  *transition_name,
                       gboolean      is_finished,
		       MildenhallButtonDrawer  *pButtonDrawer)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT:%s \n", __FUNCTION__);
	/* disconnect so we don't get multiple notifications */
	g_signal_handlers_disconnect_by_func (actor,
			on_hide_transition_stopped,
                                        pButtonDrawer);
}

/********************************************************
 * Function : v_button_drawer_tooltip_hide
 * Description: function to hide tooltip
 * Parameters: The object, whether hide should happen with animation
 * Return value: void
 ********************************************************/
static void v_button_drawer_tooltip_hide (MildenhallButtonDrawer *pButtonDrawer, gboolean bAnimate)
{
        MildenhallButtonDrawerPrivate *priv = pButtonDrawer->priv;
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning ("Invalid button");
                return;
        }

        // Avoid multiple hide calls
        // If showing, stop it
# if 0
        if (priv->pTooltipShowAnim != NULL)
                clutter_animation_completed (priv->pTooltipShowAnim);
# endif
	if(priv->pTooltipShowTrans != NULL)
	{
		clutter_actor_remove_transition(priv->pTooltipGroup, "opacity");
		clutter_timeline_stop(CLUTTER_TIMELINE(priv->pTooltipShowTrans));
		priv->pTooltipShowTrans = NULL;
	}
	/* stop the timeout hide, if running */
	if(priv->inSourceID )
        {
                g_source_remove(priv->inSourceID );
                priv->inSourceID = 0;
        }

	if(bAnimate)
	{
        	/*priv->pTooltipHideAnim = clutter_actor_animate (priv->pTooltipGroup, CLUTTER_LINEAR, priv->inAnimationDuration,
                                                                                             "opacity", 0,
                                                                                             "signal-after::completed", v_button_drawer_tooltip_hide_completed_cb, pButtonDrawer,
                                                                                              NULL);*/
		priv->pTooltipHideTrans = clutter_property_transition_new ("opacity");
		clutter_timeline_set_duration (CLUTTER_TIMELINE (priv->pTooltipHideTrans), priv->inAnimationDuration);
		clutter_actor_set_easing_mode(priv->pTooltipGroup, CLUTTER_LINEAR);
		clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (priv->pTooltipHideTrans), 0);
		clutter_transition_set_from (priv->pTooltipHideTrans, G_TYPE_UINT, 0xFF);
		clutter_transition_set_to (priv->pTooltipHideTrans, G_TYPE_UINT, 0);

		g_signal_connect(CLUTTER_TIMELINE (priv->pTooltipHideTrans), "completed", G_CALLBACK(v_button_drawer_tooltip_hide_completed_cb), pButtonDrawer);
		g_signal_connect (priv->pTooltipGroup, "transition-stopped::opacity", G_CALLBACK (on_hide_transition_stopped), pButtonDrawer);

		clutter_actor_add_transition (priv->pTooltipGroup, "opacity", priv->pTooltipHideTrans);
	}
	else
	{
        	clutter_actor_set_opacity (priv->pTooltipGroup, 0);
        	clutter_actor_hide (priv->pTooltipGroup);
       	}
}

/********************************************************
 * Function : b_button_drawer_tooltip_timeout_hide
 * Description: tooltip hide after show with timeout
 * Parameters: The object
 * Return value: gboolean
 ********************************************************/
static gboolean b_button_drawer_tooltip_timeout_hide( gpointer pUserData)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT:%s %d\n", __FUNCTION__, __LINE__);
	if (! MILDENHALL_IS_BUTTON_DRAWER (pUserData))
	{
		g_warning("invalid button\n");
                return FALSE;
	}

	/* hide tooltip with animation */
	v_button_drawer_tooltip_hide (MILDENHALL_BUTTON_DRAWER (pUserData), TRUE);

	/* come out from timeout */
        return FALSE;
}

/********************************************************
 * Function : v_button_drawer_tooltip_show_completed_cb
 * Description: callback on tooltip show completion
 * Parameters: ClutterAnimation *, The object
 * Return value: void
 ********************************************************/
//static void v_button_drawer_tooltip_show_completed_cb (ClutterAnimation *pAnimation, ClutterActor *pButtonDrawer)
static void v_button_drawer_tooltip_show_completed_cb (ClutterTimeline *pTimeline, ClutterActor *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	guint inDuration = 0;
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT:%s %d\n", __FUNCTION__, __LINE__);
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
	{
		g_warning("invalid button\n");
                return;
	}

	/* get the tooltip duration */
	g_object_get(pButtonDrawer, "tooltip-duration", &inDuration, NULL);
	if(inDuration > 0)
	{
		priv->inSourceID = g_timeout_add (inDuration, b_button_drawer_tooltip_timeout_hide, pButtonDrawer);
	}

	/* emit the tooltip show signal */
	lightwood_button_set_tooltip_showing (LIGHTWOOD_BUTTON (pButtonDrawer), TRUE);

	//priv->pTooltipShowAnim = NULL;
	//priv->pTooltipShowTrans = NULL;
}

/********************************************************
 * Function : v_button_drawer_tooltip_show
 * Description: function to show tooltip
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_button_drawer_tooltip_show (MildenhallButtonDrawer *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning ("Invalid button");
                return;
        }

# if 0
	if (priv->pTooltipShowAnim != NULL)
        	clutter_animation_completed (priv->pTooltipShowAnim);
	if (priv->pTooltipHideAnim != NULL)
                clutter_animation_completed (priv->pTooltipHideAnim);
# endif
	if(priv->inSourceID )
	{
		g_source_remove(priv->inSourceID );
		priv->inSourceID = 0;
	}

	if(priv->pTooltipShowTrans != NULL)
	{
                clutter_actor_remove_transition(priv->pTooltipGroup, "opacity");
		clutter_timeline_stop(CLUTTER_TIMELINE(priv->pTooltipShowTrans));
		priv->pTooltipShowTrans = NULL;
	}
	if(priv->pTooltipHideTrans != NULL)
	{
		clutter_actor_remove_transition(priv->pTooltipGroup, "opacity");
		clutter_timeline_stop(CLUTTER_TIMELINE(priv->pTooltipHideTrans));
		priv->pTooltipHideTrans = NULL;
	}

        clutter_actor_set_opacity (priv->pTooltipGroup, 0);
        clutter_actor_show (priv->pTooltipGroup);
# if 0
	/* show tooltip with animation */
        priv->pTooltipShowAnim = clutter_actor_animate (priv->pTooltipGroup, CLUTTER_LINEAR, priv->inAnimationDuration,
                                                                                                "opacity", 0xFF,
                                                                                                "signal-after::completed", v_button_drawer_tooltip_show_completed_cb, pButtonDrawer,
                                                                                                NULL);
# endif
	priv->pTooltipShowTrans = clutter_property_transition_new ("opacity");
	clutter_timeline_set_duration (CLUTTER_TIMELINE (priv->pTooltipShowTrans), priv->inAnimationDuration);
	clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (priv->pTooltipShowTrans), 0);
	clutter_transition_set_from (priv->pTooltipShowTrans, G_TYPE_UINT, 0);
	clutter_transition_set_to (priv->pTooltipShowTrans, G_TYPE_UINT, 255);
	clutter_actor_set_easing_mode (priv->pTooltipGroup, CLUTTER_LINEAR);
	g_signal_connect(CLUTTER_TIMELINE (priv->pTooltipShowTrans), "completed", G_CALLBACK(v_button_drawer_tooltip_show_completed_cb), pButtonDrawer);
	g_signal_connect (priv->pTooltipGroup, "transition-stopped::opacity", G_CALLBACK (on_show_transition_stopped), pButtonDrawer);

	clutter_actor_add_transition (priv->pTooltipGroup, "opacity", priv->pTooltipShowTrans);
}

/********************************************************
 * Function : v_button_drawer_update_tooltip_direction
 * Description: update the tooltip direction
 * Parameters: The object, tooltip direction
 * Return value: void
 ********************************************************/
static void v_button_drawer_update_tooltip_direction(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	LightwoodTooltipDirection direction;
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning ("Invalid button");
                return;
        }

	/* get the tooltip direction */
	g_object_get(LIGHTWOOD_BUTTON(pButtonDrawer), "tooltip-direction", &direction, NULL);

	if(direction == LIGHTWOOD_TOOLTIP_LEFT)
	{
		clutter_actor_set_position (priv->pTooltipGroup, -(priv->flTooltipWidth), 0.0);
		//clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pTooltipText), PANGO_ALIGN_RIGHT);
		clutter_actor_set_x(priv->pTooltipText,  (priv->flTooltipWidth - clutter_actor_get_width(priv->pTooltipText))- 10.0);
		//clutter_actor_set_x (priv->pTooltipText,-10);// (priv->flTooltipWidth - clutter_actor_get_width (priv->pTooltipText)) / 2);
	}
	if(direction == LIGHTWOOD_TOOLTIP_RIGHT)
	{
		clutter_actor_set_position (priv->pTooltipGroup, priv->flWidth, 0.0);
		clutter_actor_set_x (priv->pTooltipText, 10.0);
		//clutter_actor_set_anchor_point (priv->pTooltipText, 0.0, 0.0);
	}

}

/********************************************************
 * Function : v_button_drawer_create_tooltip_group
 * Description: create tooltip group
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_button_drawer_create_tooltip_group(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	LightwoodTooltipDirection direction = LIGHTWOOD_TOOLTIP_LEFT;

	if (priv->pTooltipGroup == NULL)
	{
		priv->pTooltipGroup = clutter_actor_new ();
		/* add to the button group */
		//if (clutter_actor_get_parent (priv->pTooltipGroup) == NULL)
		clutter_actor_add_child (priv->pButtonGroup, priv->pTooltipGroup);
	}
	/* hide the tooltip group because based on tooltip-show flag, tooltip group will be shown */
	clutter_actor_hide (priv->pTooltipGroup);

	// Create Tooltip Rectangle
	if (priv->pTooltipRect == NULL)
	{
		priv->pTooltipRect = clutter_actor_new ();
		/* add rect to the tooltip group */
		//if (clutter_actor_get_parent (priv->pTooltipRect) == NULL)
		clutter_actor_add_child (priv->pTooltipGroup, priv->pTooltipRect);
	}
	clutter_actor_set_background_color (priv->pTooltipRect, &priv->tooltipRectColor);
	/* height of tooltip rect will be the same as button height */
	clutter_actor_set_size (priv->pTooltipRect, priv->flTooltipWidth, priv->flHeight);

        g_object_get(pButtonDrawer, "tooltip-direction", &direction, NULL);

	if(direction == LIGHTWOOD_TOOLTIP_RIGHT)
		clutter_actor_set_position (priv->pTooltipGroup, 64.0, 0.0);
	else if(direction == LIGHTWOOD_TOOLTIP_LEFT)
		clutter_actor_set_position (priv->pTooltipGroup, -64.0, 0.0);
}

/********************************************************
 * Function : v_button_drawer_update_tooltip_width
 * Description: set the tooltip width
 * Parameters: The object, Tooltip Width
 * Return value: void
 ********************************************************/
static void v_button_drawer_update_tooltip_width (MildenhallButtonDrawer *pButtonDrawer, gfloat flTooltipWidth)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        if (! MILDENHALL_IS_BUTTON_DRAWER (pButtonDrawer))
        {
                g_warning ("Invalid button");
                return;
        }

	if (! CLUTTER_IS_ACTOR (priv->pTooltipRect))
	{
		/* if tooltip group is not created, create the group */
		v_button_drawer_create_tooltip_group(pButtonDrawer);
	}

	/* reset the width */
	//clutter_actor_set_width(priv->pTooltipGroup, flTooltipWidth);
        clutter_actor_set_width (priv->pTooltipRect, flTooltipWidth);


        priv->flTooltipWidth = flTooltipWidth;

	if(NULL != priv->pTooltipText)
	{
		//const gchar *str = clutter_text_get_text(CLUTTER_TEXT(priv->pTooltipText));
		v_button_drawer_update_tooltip_direction(pButtonDrawer);
		//clutter_actor_destroy(priv->pTooltipText);
		//v_button_drawer_set_tooltip_text(CLUTTER_ACTOR(pButtonDrawer), str);
	}

}

/********************************************************
 * Function : v_button_drawer_set_tooltip_text
 * Description: set the tooltip text
 * Parameters: The object, pTooltipText
 * Return value: void
 ********************************************************/
static void v_button_drawer_set_tooltip_text(ClutterActor *pActor, const gchar *pTooltipText)
{
	MildenhallButtonDrawer *pButtonDrawer = MILDENHALL_BUTTON_DRAWER (pActor);
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

# if 0
	/* check whether tooltip is enabled or not */
	gboolean bState;
	g_object_get(LIGHTWOOD_BUTTON(pButtonDrawer), "tooltip-enabled", &bState, NULL);
	if (! bState)
	{
		g_warning ("MILDENHALL_BUTTON_DRAWER_PRINT: Tooltip disabled !");
		return;
	}
# endif
	if (pTooltipText == NULL)
        {
                if (CLUTTER_IS_TEXT (priv->pTooltipText))
                        clutter_text_set_text (CLUTTER_TEXT (priv->pTooltipText), "");

                return;
        }

	priv->bTooltipConstructed = FALSE;
	/* if tooltip group is not created, create the group */
	v_button_drawer_create_tooltip_group(pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: tooltip x= %f tooltip y = %f tooltip width = %f, tooltip height = %f\n", clutter_actor_get_x(priv->pTooltipGroup),
							 clutter_actor_get_y(priv->pTooltipGroup),
							 clutter_actor_get_width(priv->pTooltipGroup),
							 clutter_actor_get_height(priv->pTooltipGroup));
	// Create Tooltip Text
        if (priv->pTooltipText == NULL)
                priv->pTooltipText = clutter_text_new ();

	/* set text properties */
        clutter_text_set_text (CLUTTER_TEXT (priv->pTooltipText), pTooltipText );
        clutter_text_set_single_line_mode (CLUTTER_TEXT (priv->pTooltipText), TRUE);
	clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pTooltipText), PANGO_ELLIPSIZE_END);
        clutter_text_set_font_name (CLUTTER_TEXT (priv->pTooltipText), priv->pTooltipFont);

# if 0
	if(clutter_actor_get_width(priv->pTooltipText) > clutter_actor_get_width(priv->pTooltipGroup))
	{
		clutter_actor_set_width(priv->pTooltipText, clutter_actor_get_width(priv->pTooltipGroup));
	}
# endif
        if (clutter_actor_get_parent (priv->pTooltipText) == NULL)
                clutter_actor_add_child (priv->pTooltipGroup, priv->pTooltipText);

	clutter_actor_set_y(priv->pTooltipText, (priv->flHeight - clutter_actor_get_height (priv->pTooltipText)) / 2);
	mildenhall_button_drawer_set_tooltip_width(pButtonDrawer, (clutter_actor_get_width(priv->pTooltipText) + 30));
	v_button_drawer_update_tooltip_color(pButtonDrawer);
	/* if its created first time, add to the container */

	/* reset the construct flag */
        priv->bTooltipConstructed = TRUE;

}

/********************************************************
 * Function : v_button_drawer_add_style_to_hash
 * Description: maintain style hash for bg state and multi state
 * Parameter :  *pButtonDrawer, *pKey, pValue
 * Return value: void
 ********************************************************/
static void
v_button_drawer_add_style_to_hash (MildenhallButtonDrawer *pButtonDrawer, const gchar *pKey, gpointer pValue)
{
	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("invalid button drawer object\n");
                return;
        }

	if(NULL != pKey || NULL != pValue)
	{
		MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pButtonDrawer);
		const gchar *pFilePath = NULL;
		pFilePath = g_value_get_string(pValue);
		/* maintain key and value pair for style name and releated image path */
		g_hash_table_insert(priv->pImageHash, g_strdup(pKey), g_strdup_printf(PKGTHEMEDIR"/%s", pFilePath));
# if 0
		if(NULL != pFilePath)
		{
			g_free((gchar*)pFilePath);
			pFilePath = NULL;
		}
# endif
	}

}

/********************************************************
 * Function : v_button_drawer_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_button_drawer_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	MildenhallButtonDrawer *pButtonDrawer = pUserData;
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        gchar *pStyleKey = g_strdup (pKey);

	/* store the style images in a hash for setting syle based on set properties */
	//// g_print("style: name = %s\n", (gchar*)pKey);
	if(g_strcmp0(pStyleKey, "menu-bg-single-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_SINGLE_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "menu-bg-single-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_SINGLE_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "menu-bg-closed-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_CLOSED_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "menu-bg-closed-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_CLOSED_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "menu-bg-open-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_OPEN_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "menu-bg-open-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_BG_OPEN_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "menu-arrow-left") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_ARROW_LEFT, pValue);
	else if(g_strcmp0(pStyleKey, "menu-arrow-right") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, MENU_ARROW_RIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-single-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_SINGLE_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-single-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_SINGLE_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-closed-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_CLOSED_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-closed-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_CLOSED_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-open-normal") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_OPEN_NORMAL, pValue);
	else if(g_strcmp0(pStyleKey, "views-bg-open-pressed") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_BG_OPEN_PRESSED, pValue);
	else if(g_strcmp0(pStyleKey, "views-arrow-left") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_ARROW_LEFT, pValue);
	else if(g_strcmp0(pStyleKey, "views-arrow-right") == 0)
		v_button_drawer_add_style_to_hash(pButtonDrawer, VIEWS_ARROW_RIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "button-inactive-color") == 0)
		clutter_color_from_string(&priv->buttonInactiveColor,  g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-rect-color") == 0)
		clutter_color_from_string(&priv->tooltipRectColor,  g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-inactive-rect-color") == 0)
		clutter_color_from_string(&priv->tooltipInactiveRectColor,  g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-inactive-font-color") == 0)
		clutter_color_from_string(&priv->tooltipInactiveFontColor, g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-font-color") == 0)
		clutter_color_from_string(&priv->tooltipFontColor, g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-font") == 0)
		priv->pTooltipFont = g_strdup(g_value_get_string(pValue));
	else if(g_strcmp0(pStyleKey, "tooltip-font-width") == 0)
		priv->inTooltipFontWidth = g_value_get_int64(pValue);
	else if(g_strcmp0(pStyleKey, "tooltip-animation-duration") == 0)
		priv->inAnimationDuration = g_value_get_int64(pValue);
	else
	{
		;	/* do nothing */
	}

	/* free the key */
	v_button_drawer_free_str(pStyleKey);
# if 0
	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
# endif
}

/********************************************************
 * Function : v_button_drawer_update_view
 * Description: update the button view
 * Parameters: MildenhallButtonDrawer *
 * Return value: void
 ********************************************************/
static void v_button_drawer_update_view(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	gint inRows;
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER: %s\n", __FUNCTION__);

	if(! G_IS_OBJECT(priv->pStateModel) )
		return;

	inRows = thornbury_model_get_n_rows (priv->pStateModel);
	if (inRows > 0)
        {
                ThornburyModelIter *pIter = NULL;
		/* if current state is not set, set it to 0 */
		if(priv->inCurrentState == -1)
			priv->inCurrentState = 0;

                pIter = thornbury_model_get_iter_at_row (priv->pStateModel, priv->inCurrentState);
                if(NULL != pIter)
                {
                        GValue value = { 0, };
			MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER: %s\n", __FUNCTION__);
			/* get the first column value for icon image path */
                        thornbury_model_iter_get_value(pIter, 0, &value);
                        /* set the icon */
                        v_button_drawer_set_icon(CLUTTER_ACTOR(pButtonDrawer), g_value_get_string(&value));
			g_value_unset (&value);

			/* get the second column value to get tooltip text */
			thornbury_model_iter_get_value(pIter, 1, &value);
			/* set the tooltip */
			v_button_drawer_set_tooltip_text(CLUTTER_ACTOR(pButtonDrawer), g_value_get_string(&value));
			g_value_unset (&value);
		}
	}
}

/********************************************************
 * Function : v_button_drawer_check_multi_state
 * Description: if model has more than one state, set multi state as true
 * Parameters: ThornburyModel *, MildenhallButtonDrawer *
 * Return value: void
 ********************************************************/
static void v_button_drawer_check_multi_state(MildenhallButtonDrawer *pButtonDrawer, ThornburyModel *pModel)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER: %s\n", __FUNCTION__);

	if(NULL == pModel)
                return;
	g_return_if_fail (G_IS_OBJECT (pModel));
	g_return_if_fail (G_IS_OBJECT (priv->pStateModel));

	// g_print("row = %d\n", thornbury_model_get_n_rows(priv->pStateModel));
	/* if rows are greater than 1, enable the multi state */
	if(thornbury_model_get_n_rows (priv->pStateModel) > 1)
	{
		g_object_set(pButtonDrawer, "multi-state", TRUE, NULL);
	}
	else
	{
		g_object_set(pButtonDrawer, "multi-state", FALSE, NULL);
	}
}

/********************************************************
 * Function : v_button_drawer_create_arrow_textures
 * Description: create left/right arrows for multi state
 * Parameters: MildenhallButtonDrawer*, leftImage, rightImage
 * Return value: void
 ********************************************************/
static void v_button_drawer_create_arrow_textures(MildenhallButtonDrawer *pButtonDrawer, gchar *strArrowLeft, gchar *strArrowRight)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);

        MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	/* if arrows not created, create them */
	if(NULL == priv->pArrowLeft)
	{
		//priv->pArrowLeft = clutter_texture_new();
		priv->pArrowLeft = thornbury_ui_texture_create_new(strArrowLeft, 0, 0, FALSE, FALSE);
		g_object_set(priv->pArrowLeft, "x", 5.0, "y", (priv->flHeight - clutter_actor_get_height (priv->pArrowLeft)) / 2, NULL);
		clutter_actor_add_child(priv->pButtonGroup, priv->pArrowLeft);
	}
	if(NULL == priv->pArrowRight)
	{
		//priv->pArrowRight = clutter_texture_new();
		priv->pArrowRight = thornbury_ui_texture_create_new(strArrowRight, 0, 0, FALSE, FALSE);
		clutter_actor_add_child(priv->pButtonGroup, priv->pArrowRight);
		g_object_set(priv->pArrowRight, "x", priv->flWidth - 5.0, "y", (priv->flHeight - clutter_actor_get_height (priv->pArrowRight)) / 2, NULL);
	}
}

/********************************************************
 * Function : v_button_drawer_update_arrows
 * Description: update the left/right arrows based on multi state
 * Parameters: MildenhallButtonDrawer*,
 * Return value: void
 ********************************************************/
static void v_button_drawer_update_arrows(MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);

	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	/* if multi state is set as TRUE, show left/right arrows */
	if(priv->bMultiState)
	{
		gchar *strArrowLeft = NULL;
		gchar *strArrowRight = NULL;

		/* if views drawer, set related arrows images */
		if(priv->bViewsDrawer)
                {
                        strArrowLeft = g_hash_table_lookup(priv->pImageHash, VIEWS_ARROW_LEFT);
			strArrowRight = g_hash_table_lookup(priv->pImageHash, VIEWS_ARROW_RIGHT);
		}
		else
		{
			strArrowLeft = g_hash_table_lookup(priv->pImageHash, MENU_ARROW_LEFT);
			strArrowRight = g_hash_table_lookup(priv->pImageHash, MENU_ARROW_RIGHT);
		}

		/* create arrow textures */
		v_button_drawer_create_arrow_textures(pButtonDrawer, strArrowLeft, strArrowRight);

		/* update arrow actors */
		if(strArrowLeft && strArrowRight)
		{
			thornbury_ui_texture_set_from_file(priv->pArrowLeft, strArrowLeft, 0, 0, FALSE, FALSE);
			thornbury_ui_texture_set_from_file(priv->pArrowRight, strArrowRight, 0, 0, FALSE, FALSE);
		}

		/* if multi state, show arrows, and enable swipe action */
		clutter_actor_show(priv->pArrowLeft);
		clutter_actor_show(priv->pArrowRight);

		/* enable swipe action to switch between multi states */
		g_object_set(pButtonDrawer, "swipe-enabled", TRUE, NULL);
	}
	else
	{
		/* if not multi state, hide arrows, and disable swipe action */
		if(priv->pArrowLeft && priv->pArrowRight)
		{
			clutter_actor_hide(priv->pArrowLeft);
			if(priv->pArrowRight)
				clutter_actor_hide(priv->pArrowRight);
		}
		g_object_set(pButtonDrawer, "swipe-enabled", FALSE, NULL);
	}
}

/********************************************************
 * Function : mildenhall_button_drawer_bg_state_get_type
 * Description: define background state enum as GType
 * Parameters: void
 * Return value: void
 ********************************************************/
static GType mildenhall_button_drawer_bg_state_get_type (void)
{
        /* argument must point to a static 0-initialized variable,
         * that will be set to a value other than 0 at the end of
         * the initialization section.
         */
        static volatile gsize gEnumTypeVolatile = 0;

        /* swipe direction as enum initialization section */
        if (g_once_init_enter (&gEnumTypeVolatile))
        {
                /* A structure which contains a single flags value, its name, and its nickname. */
                static const GEnumValue arValues[] =
                {
                        { MILDENHALL_BUTTON_DRAWER_BG_OPEN, "BG_OPEN", "open" },
                        { MILDENHALL_BUTTON_DRAWER_BG_CLOSE, "BG_CLOSE", "close" },
                        { MILDENHALL_BUTTON_DRAWER_BG_NORMAL, "BG_NORMAL", "normal" },
                        { 0, NULL, NULL }
                };
                GType gEnumType;

                /* Registers a new static flags type with the name name. */
                gEnumType = g_enum_register_static (g_intern_static_string ("enBackgroundState"), arValues);

                /* In combination with g_once_init_leave() and the unique address value_location,
                 * it can be ensured that an initialization section will be executed only once
                 * during a program's life time, and that concurrent threads are blocked
                 * until initialization completed.
                 */
                g_once_init_leave (&gEnumTypeVolatile, gEnumType);
        }

        return gEnumTypeVolatile;
}


/********************************************************
 * Function : mildenhall_button_drawer_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_class_init (MildenhallButtonDrawerClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallButtonDrawerPrivate));

	pObjectClass->get_property = mildenhall_button_drawer_get_property;
	pObjectClass->set_property = mildenhall_button_drawer_set_property;
	pObjectClass->dispose = mildenhall_button_drawer_dispose;
	pObjectClass->finalize = mildenhall_button_drawer_finalize;


	// Create a signal for the button to indicate state change
        /**
         * MildenhallButtonDrawer::state-changed:
         * @mildenhallButtonDrawer: The object which received the signal
         * @oldState: name of the old state
         * @newState: name of hte new state
         *
         * ::state-changed is emitted when button has multi-state and there
	 * is a change in the button state
         */
        mildenhall_bt_drawer_signals[SIG_STATE_CHANGED] = g_signal_new ("state-changed",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
	                G_STRUCT_OFFSET (MildenhallButtonDrawerClass, state_changed),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__INT,
                        G_TYPE_NONE, 1, G_TYPE_INT);
        /**
         * MildenhallButtonDrawer::swipe-end:
         * @mildenhallButtonDrawer: The object which received the signal
         * @LightwoodSwipeDirection: swipe direction
         *
         * ::wipe-end is emitted when button has a swipe action, based on multi-state
	 * property of the button swipe direction is calculated, If button is not a
  	 * multi-state button, up/down swipe area will be wide.
         */
        mildenhall_bt_drawer_signals[SIG_SWIPE_END] = g_signal_new ("swipe-end",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
	                G_STRUCT_OFFSET (MildenhallButtonDrawerClass, swipe_end),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__ENUM,
                        G_TYPE_NONE, 1, LIGHTWOOD_BUTTON_TYPE_SWIPE_DIRECTION);

	/**
	 * LightwoodButton:width:
	 *
	 * Width of the button
	 */
	pspec = g_param_spec_float ("width",
			"Width",
			"Width of the button",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READABLE);
	g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

	/**
	 * LightwoodButton:height:
	 *
	 * Height of the button
	 */
	pspec = g_param_spec_float ("height",
			"Height",
			"Height of the button",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READABLE);
	g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	/**
         * MildenhallButtonDrawer:multi-state:
         *
         * Set this to TRUE to make it a multiple state button
         * This will enable drag left and right on the button
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("multi-state",
                        "MultiState",
                        "Whether the button has multi-state",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MULTI_STATE, pspec);
        /**
         * MildenhallButtonDrawer:views-drawer:
         *
         * Set this to TRUE to use style for views drawer button
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("views-drawer",
                        "ViewsDrawer",
                        "Whether the button should use style for views drawer",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_VIEWS_DRAWER, pspec);

	/**
         * MildenhallButtonDrawer:tooltip-width:
         *
         * Width of the tooltip box. Height will be same as button height
         */
        pspec = g_param_spec_float ("tooltip-width",
                        "TooltipWidth",
                        "Tooltip width",
                        1.0, G_MAXFLOAT,
                        100.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_TOOLTIP_WIDTH, pspec);

        /**
         * MildenhallButtonDrawer:tooltip-show:
         *
         * Set this property to TRUE to show tooltip
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("show-tooltip",
                        "Show-Tooltip",
                        "Whether the tooltip should be shown",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW_TOOLTIP, pspec);

        /**
         * MildenhallButtonDrawer:active:
         *
         * Set reacticve property to TRUE to make button as inactive
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("active",
                        "Active-State",
                        "Whether the button is active",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ACTIVE, pspec);

	/**
         * MildenhallButtonDrawer:current-state:
         *
         * gives the current state of the button as state index
         * Default: NULL
         */
        pspec = g_param_spec_int64 ("current-state",
                        "CurrentState",
                        "Current state of the button",
                        -1, G_MAXINT,
			-1,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_CURRENT_STATE, pspec);

	/**
         * MildenhallButtonDrawer:model:
         *
         * model for the button states. If button model has more than
	 * one row, multi-state will get enabled and swipe feature will
	 * get enabled with left/right arrows
     *
	 * Model content:
	 * 	1. icon-path,
	 * 	2. tooltip-text
	 *
         * Default: NULL
         */
        pspec = g_param_spec_object ("model",
                        "Model",
                        "model having button states data",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_STATE_MODEL, pspec);

	/**
         * MildenhallButtonDrawer:backgroung-state:
         *
         * background images differs based on the background states.
   	 *
	 * background states for drawer can be :
	 * MILDENHALL_BUTTON_DRAWER_BG_OPEN or
	 * MILDENHALL_BUTTON_DRAWER_BG_CLOSE or
	 * MILDENHALL_BUTTON_DRAWER_BG_NORMAL
         * Default: MILDENHALL_BUTTON_DRAWER_BG_NORMAL
         */
        pspec = g_param_spec_enum ("background-state",
                        "BackgroundState",
                        "background state for the drawer button to set the proper background images",
                        MILDENHALL_BUTTON_DRAWER_TYPE_BG_STATE,
			MILDENHALL_BUTTON_DRAWER_BG_NORMAL,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BG_STATE, pspec);

	/**
         * MildenhallButtonDrawer:language:
         *
	 * set language
  	 * Test gets updated
	 */
	pspec = g_param_spec_string ("language",
                        "language",
                        "lang change",
                        "en_US",
                        G_PARAM_READWRITE);

        g_object_class_install_property (pObjectClass, PROP_LANG, pspec);
}

/********************************************************
 * Function : mildenhall_button_drawer_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_button_drawer_init (MildenhallButtonDrawer *self)
{
	GHashTable *pStyleHash;
        ClutterAction *pGestureAction = clutter_gesture_action_new ();
        const char *pEnvString = NULL;
	self->priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (self);

	/* initialise the priv */
	self->priv->flWidth = 0.0;
	self->priv->flHeight = 0.0;
	self->priv->inTooltipFontWidth = 0;
	self->priv->flTooltipWidth = 0.0;
	self->priv->inCurrentState = -1;
	self->priv->inSourceID = 0;
        self->priv->inAnimationDuration = 0;
	self->priv->enBgState = -1;

	self->priv->bMultiState = FALSE;
	self->priv->bShowTooltip = FALSE;
	self->priv->bActive = TRUE;
	self->priv->bTooltipConstructed = TRUE;

	self->priv->pStateModel = NULL;

	self->priv->pBgInactive = NULL;
	self->priv->pButtonGroup = NULL;
	self->priv->pTooltipGroup = NULL;
	self->priv->pTooltipRect = NULL;
	self->priv->pBgNormal = NULL;
	self->priv->pBgPressed = NULL;
	self->priv->pArrowRight = NULL;
	self->priv->pArrowLeft = NULL;
	self->priv->pIcon = NULL;
	self->priv->pIconInactive = NULL;
	self->priv->language = NULL;

	/* initialise the button group */
	self->priv->pButtonGroup = clutter_actor_new ();
        clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pButtonGroup);

	/* check for env to enable traces */
        pEnvString = g_getenv ("MILDENHALL_BUTTON_DRAWER_DEBUG");
        if (pEnvString != NULL)
        {
                mildenhall_button_drawer_debug_flags = g_parse_debug_string (pEnvString, mildenhall_button_drawer_debug_keys, G_N_ELEMENTS (mildenhall_button_drawer_debug_keys));
                MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: env_string %s %ld %d \n", pEnvString, G_LIKELY (MILDENHALL_BUTTON_DRAWER_HAS_DEBUG), mildenhall_button_drawer_debug_flags);

        }

	/* get the hash table for style properties */
        pStyleHash = thornbury_style_set(PKGDATADIR"/mildenhall_button_drawer_style.json");

	/* pares the hash for styles */
	if(NULL != pStyleHash)
	{
		GHashTableIter iter;
	        gpointer key, value;

		g_hash_table_iter_init(&iter, pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			//// g_print("style layer = %s\n", (gchar*)key);
			if(NULL != pHash)
			{
				self->priv->pImageHash = g_hash_table_new(g_int_hash, g_str_equal);
				g_hash_table_foreach(pHash, v_button_drawer_parse_style, self);
			}
		}
		/* free the style hash */
		thornbury_style_free(pStyleHash);
	}

	/* connect the signals */
	g_signal_connect(self, "button-press", G_CALLBACK(v_button_drawer_press_cb), NULL);
	g_signal_connect(self, "button-release", G_CALLBACK(v_button_drawer_release_cb), NULL);
	//g_signal_connect(self, "swipe-action", G_CALLBACK(v_button_drawer_swipe_cb), NULL);
	g_signal_connect(self, "button-reset", G_CALLBACK(v_button_drawer_release_cb), NULL);


	/* add gesture action to get swipe event on the lightwood button*/
        /* Inhancement: May be we can implementt a generic swipe gesture */
        clutter_actor_add_action (CLUTTER_ACTOR(self), pGestureAction);
        g_signal_connect (pGestureAction, "gesture-begin", G_CALLBACK (gesture_begin), NULL);
        g_signal_connect (pGestureAction, "gesture-progress", G_CALLBACK (gesture_progress), NULL);
        g_signal_connect (pGestureAction, "gesture-end", G_CALLBACK (gesture_end), NULL);

	/* set the default background. In case user does not set this property, it should show some default bg */
	g_object_set(self, "background-state", MILDENHALL_BUTTON_DRAWER_BG_NORMAL, NULL);
}

/********************************************************
 * Function : v_button_drawer_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonDrawer *
 * Return value: void
 ********************************************************/
static void v_button_drawer_row_added_cb (ThornburyModel *pModel,
                  	      ThornburyModelIter *pIter,
                  	      MildenhallButtonDrawer *pButtonDrawer)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	/* update the multi state */
	v_button_drawer_check_multi_state(pButtonDrawer, pModel);
	/* update view */
	v_button_drawer_update_view(pButtonDrawer);
	//clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonDrawer));
}

/********************************************************
 * Function : v_button_drawer_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonDrawer *
 * Return value: void
 ********************************************************/
static void v_button_drawer_row_changed_cb (ThornburyModel *pModel,
                  	      ThornburyModelIter *pIter,
                  	      MildenhallButtonDrawer *pButtonDrawer)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s %d\n", __FUNCTION__, __LINE__);
	/* update view */
	v_button_drawer_update_view(pButtonDrawer);
	//clutter_actor_queue_relayout (CLUTTER_ACTOR (pButtonDrawer));
}

/********************************************************
 * Function : v_button_drawer_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallButtonDrawer *
 * Return value: void
 ********************************************************/
static void v_button_drawer_row_removed_cb (ThornburyModel *pModel,
                  	      ThornburyModelIter *pIter,
                  	      MildenhallButtonDrawer *pButtonDrawer)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);

	/* this callback gives the actual row count before removal, and not after removal of row */
	gint inRemovedRow = thornbury_model_iter_get_row (pIter);
	gint inTotalRows = thornbury_model_get_n_rows(priv->pStateModel);
	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);
	//// g_print("removed row = %d\n", inRemovedRow);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && inTotalRows == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
	/* if the row removed is the current state, reset the current state */
	if(inRemovedRow == priv->inCurrentState)
	{
		/* if there is next row, switch to next state else switch to previous state */
		if(priv->inCurrentState + 1 < inTotalRows )
			g_object_set(pButtonDrawer, "current-state", priv->inCurrentState + 1, NULL);
		else
			g_object_set(pButtonDrawer, "current-state", priv->inCurrentState - 1, NULL);
	}
	/* state removed is less than the current state, update the current state in priv */
	if(inRemovedRow < priv->inCurrentState)
		priv->inCurrentState = priv->inCurrentState - 1;

	/* this callback gives the actual row count before removal, so update the multi state */
	if(inTotalRows == 2)
		g_object_set(pButtonDrawer, "multi-state", FALSE, NULL);

}

# if 0
/********************************************************
 * Function : b_button_drawer_reset_button
 * Description: timeout to switch back to button release
 * Parameters: MildenhallButtonDrawer*
 * Return value: void
 ********************************************************/
static void v_button_drawer_reset_button(gpointer pUserData)
{
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_BUTTON_DRAWER(pUserData))
	{
		g_warning("invalid button\n");
		return;
	}


	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE(pUserData);
        if(NULL != priv->pBgPressed)
                clutter_actor_hide(priv->pBgPressed);

        if(NULL != priv->pBgNormal)
                clutter_actor_show(priv->pBgNormal);

}
# endif

/********************************************************
 * Function : v_button_drawer_press_cb
 * Description: callabck on button drawer press
 * Parameters: MildenhallButtonDrawer*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_drawer_press_cb(ClutterActor *pButtonDrawer, gpointer pUserData)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
	{
		g_warning("invalid button\n");
		return;
	}

	if(priv->bActive)
	{
		if(NULL != priv->pBgPressed)
			clutter_actor_show(priv->pBgPressed);

		if(NULL != priv->pBgNormal)
			clutter_actor_hide(priv->pBgNormal);
	}
}

/********************************************************
 * Function : v_button_drawer_release_cb
 * Description: callabck on button drawer release
 * Parameters: MildenhallButtonDrawer*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_drawer_release_cb(ClutterActor *pButtonDrawer, gpointer pUserData)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

       	if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
	{
		g_warning("invalid button\n");
		return;
	}

	if(priv->bActive)
	{
		if(NULL != priv->pBgPressed)
			clutter_actor_hide(priv->pBgPressed);

		if(NULL != priv->pBgNormal)
			clutter_actor_show(priv->pBgNormal);
	}
}

/********************************************************
 * Function : v_button_drawer_swipe_cb
 * Description: callabck on button drawer swipe
 * Parameters: MildenhallButtonDrawer*, pUserData
 * Return value: void
 ********************************************************/
static void v_button_drawer_swipe_cb(ClutterActor *pButtonDrawer, LightwoodSwipeDirection swipeDirection, gpointer pUserData )
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

	v_button_drawer_release_cb(pButtonDrawer, NULL);

	/* if direction is left, check whether left state is there or not */
	if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_LEFT)
	{
		MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: LIGHTWOOD_SWIPE_DIRECTION_LEFT\n");
		/* if current state is smaller than total states, move to next state */
		if(priv->inCurrentState < thornbury_model_get_n_rows(priv->pStateModel) - 1)
			g_object_set(pButtonDrawer, "current-state", priv->inCurrentState + 1, NULL);
		//else
		//	g_warning("MILDENHALL_BUTTON_DRAWER_PRINT: no state on left side\n");
	}
	else if(swipeDirection == LIGHTWOOD_SWIPE_DIRECTION_RIGHT)
	{
		/* if current state is greater than 0, move to prev state */
		MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: LIGHTWOOD_SWIPE_DIRECTION_RIGHT\n");
		if(priv->pStateModel && priv->inCurrentState > 0 )

                        g_object_set(pButtonDrawer, "current-state", priv->inCurrentState - 1, NULL);
                //else
                  //      g_warning("no state on right side\n");

	}

	g_signal_emit (pButtonDrawer, mildenhall_bt_drawer_signals[SIG_SWIPE_END], 0, swipeDirection);
}

/********************************************************
 * Function : gesture_begin
 * Description: callback on gesture begin on the the lightwood button
 * Parameters: ClutterGestureAction *,
                button object reference, userData
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean gesture_begin(ClutterGestureAction *pAction, ClutterActor *pButtonDrawer, gpointer pUserData)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
        ClutterSettings *settings = clutter_settings_get_default ();
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("MILDENHALL_BUTTON_DRAWER_PRINT: invalid button\n");
                return TRUE;
        }

        /* reset the state at the beginning of a new gesture */
        priv->hDirection = 0;
        priv->vDirection = 0;

        g_object_get (settings, "dnd-drag-threshold", &priv->inThreshold, NULL);

        MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: threshold = %d\n", priv->inThreshold);
        return TRUE;
}

/********************************************************
 * Function : gesture_progress
 * Description: callback on gesture progress on the lightwood button
 * Parameters: ClutterGestureAction *, button object reference
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean gesture_progress (ClutterGestureAction *pAction, ClutterActor *pButtonDrawer)
{
        MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);

	gfloat flPressX, flPressY;
	gfloat motion_x, motion_y;
	gfloat delta_x, delta_y;
	ClutterSwipeDirection hDirection = 0;
	ClutterSwipeDirection vDirection = 0;
	MILDENHALL_BUTTON_DRAWER_PRINT ("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("MILDENHALL_BUTTON_DRAWER_PRINT: invalid button\n");
                return TRUE;
        }

        clutter_gesture_action_get_press_coords (pAction,
                        0,
                        &flPressX,
                        &flPressY);

        clutter_gesture_action_get_motion_coords (pAction,
                        0,
                        &motion_x,
                        &motion_y);

        delta_x = flPressX - motion_x;
        delta_y = flPressY - motion_y;

        if (delta_x >= priv->inThreshold)
                hDirection = CLUTTER_SWIPE_DIRECTION_RIGHT;
        else if (delta_x < -priv->inThreshold)
                hDirection = CLUTTER_SWIPE_DIRECTION_LEFT;

        if (delta_y >= priv->inThreshold)
                vDirection = CLUTTER_SWIPE_DIRECTION_DOWN;
        else if (delta_y < -priv->inThreshold)
                vDirection = CLUTTER_SWIPE_DIRECTION_UP;


	/* cancel gesture on direction reversal */
        if (priv->hDirection == 0)
                priv->hDirection = hDirection;

        if (priv->vDirection == 0)
                priv->vDirection = vDirection;

        /* swipe should always go to one direction */
        /* if swipe starts to  one direction i.e. LIGHTWOOD_SWIPE_DIRECTION_RIGHT
         *  and during swipe progress direction changes to some other direction,
         * swipe will get cancelled */
        if (priv->hDirection != hDirection)
        {
                g_warning("hDirection invalid button object\n");
                priv->hDirection = 0;

                priv->vDirection = 0;
                //return FALSE;
        }
        if (priv->vDirection != vDirection)
        {
                g_warning("vDirection invalid button object\n");
                priv->vDirection = 0;
                priv->hDirection = 0;
                //return FALSE;
        }
        return TRUE;
}

/********************************************************
 * Function : gesture_end
 * Description: callback on gesture end
 * Parameters:  ClutterGestureAction *, button object reference,
                userData
 * Return value: void
 ********************************************************/
static void gesture_end(ClutterGestureAction *pAction, ClutterActor *pButtonDrawer, gpointer pUserData)
{
	MildenhallButtonDrawerPrivate *priv = MILDENHALL_BUTTON_DRAWER_GET_PRIVATE (pButtonDrawer);
	gfloat flPressX, flPressY;
	gfloat flReleaseX, flReleaseY;
	LightwoodSwipeDirection enDirection = 0;
	MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT: %s\n", __FUNCTION__);

        if(!MILDENHALL_IS_BUTTON_DRAWER(pButtonDrawer))
        {
                g_warning("MILDENHALL_BUTTON_DRAWER_PRINT: invalid button\n");
                return;
        }

        clutter_gesture_action_get_press_coords (pAction,
                        0,
                        &flPressX, &flPressY);

        clutter_gesture_action_get_release_coords (pAction,
                        0,
                        &flReleaseX, &flReleaseY);

        MILDENHALL_BUTTON_DRAWER_PRINT("MILDENHALL_BUTTON_DRAWER_PRINT:press X = %f press Y = %f release X = %f release Y = %f\n", flPressX, flPressY, flReleaseX, flReleaseY);

        if ( (flReleaseX - flPressX > priv->inThreshold)
                        || (flReleaseY - flPressY > priv->inThreshold)
                        || (flPressX - flReleaseX > priv->inThreshold)
                        ||(flPressY - flReleaseY > priv->inThreshold) )
        {

                gdouble diff, angle;
                /* get the difference of x and y */
                diff = (flReleaseY - flPressY)/ (flReleaseX - flPressX);
                /* calculate the angle */
                angle = atan(diff) * 180 / 3.14159265;

                MILDENHALL_BUTTON_DRAWER_PRINT("angle = %f \n", angle);

                /* angles on swipe event

                        -90
                  +45   |     -45
                        |
                -0 _____|_____ +0
                        |
			|
			-45     |     +45
			+90
		 */
		/* if multi-state is disabled : For up/down direction, angle varies between -0 to -90 and 0 to 90
		    if pressed Y coord is greater than the released one, direction will be up otherwise down */
		/* else for up/down direction, angle varies between -45 to -90 and 45 to 90 */
		/* else for left/right direction, angle varies from 45 to 0 and 0 to -45 */
		enDirection = en_get_button_drawer_direction(flPressY, flReleaseY, flPressX, flReleaseX, angle, priv->bMultiState);
		/* emit the signal*/
		v_button_drawer_swipe_cb(pButtonDrawer, enDirection, pUserData);
        }
}


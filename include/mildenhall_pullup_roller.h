/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-pullup-roller.h */

#ifndef _MILDENHALL_PULLUP_ROLLER_H
#define _MILDENHALL_PULLUP_ROLLER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_roller_container.h"                                                                                                                                    
#include "mildenhall_toggle_button.h"                                                                                                                                       
#include "mildenhall_meta_info_footer.h"
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_PULLUP_ROLLER mildenhall_pullup_roller_get_type()

#define MILDENHALL_PULLUP_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_PULLUP_ROLLER, MildenhallPullupRoller))

#define MILDENHALL_PULLUP_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_PULLUP_ROLLER, MildenhallPullupRollerClass))

#define MILDENHALL_IS_PULLUP_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_PULLUP_ROLLER))

#define MILDENHALL_IS_PULLUP_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_PULLUP_ROLLER))

#define MILDENHALL_PULLUP_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_PULLUP_ROLLER, MildenhallPullupRollerClass))

typedef struct _MildenhallPullupRoller MildenhallPullupRoller;
typedef struct _MildenhallPullupRollerClass MildenhallPullupRollerClass;
typedef struct _MildenhallPullupRollerPrivate MildenhallPullupRollerPrivate;

/**
 * MildenhallPullupRoller:
 *
 * The #MildenhallPullupRoller struct contains only private data.
 *
 */
struct _MildenhallPullupRoller
{
	ClutterActor parent;

	MildenhallPullupRollerPrivate *priv;
};

/**
 * MildenhallPullupRollerClass:
 * @pullup_roller_animated: class handler for the #MildenhallPullupRoller::pullup_roller_animated signal
 * @pullup_roller_up_animation_started: class handler for the #MildenhallPullupRoller::pullup_roller_up_animation_started signal 
 * @pullup_roller_down_animation_started: class handler for the #MildenhallPullupRoller::pullup_roller_down_animation_started signal
 * @pullup_roller_item_selected: class handler for the #MildenhallPullupRoller::pullup_roller_item_selected signal	
 *
 * The #MildenhallPullupRollerClass struct contains only private data.
 *
 */
struct _MildenhallPullupRollerClass
{
	ClutterActorClass parent_class;
	
	void (* pullup_roller_animated) (MildenhallPullupRoller *pPullupRoller);
        void (* pullup_roller_up_animation_started) (MildenhallPullupRoller *pPullupRoller);
        void (* pullup_roller_down_animation_started) (MildenhallPullupRoller *pPullupRoller);
        void (* pullup_roller_item_selected) (MildenhallPullupRoller *pPullupRoller, guint uinRow);
	
};

GType mildenhall_pullup_roller_get_type (void) G_GNUC_CONST;

/* creates a new pullup roller object */
MildenhallPullupRoller *mildenhall_pullup_roller_new (void);

/*******************************************************
 * get property functions for pullup roller
 *******************************************************/
/* to get whether rating star are required as TRUE/FALSE */
gboolean mildenhall_pullup_roller_get_footer_ratings(MildenhallPullupRoller *pPullupRoller);
/* get left icon is needed as TRUE/ FALSE */
gboolean mildenhall_pullup_roller_get_footer_icon_left(MildenhallPullupRoller *pPullupRoller);
/* get footer model */
ThornburyModel *mildenhall_pullup_roller_get_footer_model(MildenhallPullupRoller *pPullupRoller);
/* get pullup roller item model */
ThornburyModel *mildenhall_pullup_roller_get_model(MildenhallPullupRoller *pPullupRoller);
/* get roller item type */
GType mildenhall_pullup_roller_get_item_type(MildenhallPullupRoller *pPullupRoller);
/* get roller type TRUE for fixed roller, FALSE for variable roller */
gboolean mildenhall_pullup_roller_get_fix_roller(MildenhallPullupRoller *pPullupRoller);
/* get pullup roller width */
gfloat mildenhall_pullup_roller_get_width(MildenhallPullupRoller *pPullupRoller);
/* get pullup roller height */
gfloat mildenhall_pullup_roller_get_height(MildenhallPullupRoller *pPullupRoller);
/* whether roller is hidden/shown as FALSE/TRUE */
gboolean mildenhall_pullup_roller_get_show(MildenhallPullupRoller *pPullupRoller);

/*******************************************************
 * set property functions for pullup roller
 *******************************************************/
/* set footer model */
void mildenhall_pullup_roller_set_footer_model(MildenhallPullupRoller *pullupRoller, ThornburyModel *pModel);
/* to set whether rating star are required as TRUE/FALSE */
void mildenhall_pullup_roller_set_footer_ratings(MildenhallPullupRoller *pPullupRoller, gboolean bIconRight);
/* set left icon is needed as TRUE/ FALSE */
void mildenhall_pullup_roller_set_footer_icon_left(MildenhallPullupRoller *pPullupRoller, gboolean bIconLeft);
/* to set roller type TRUE for fixed roller, FALSE for variable roller */
void mildenhall_pullup_roller_set_fix_roller(MildenhallPullupRoller *pPullupRoller, gboolean bFixedRoller);
/* to set pullup roller item model */
void mildenhall_pullup_roller_set_model(MildenhallPullupRoller *pPullupRoller, ThornburyModel *model);
/* set roller item type */
void mildenhall_pullup_roller_set_item_type(MildenhallPullupRoller *pPullupRoller, GType gType);
/* whether roller needs to be hidden/shown as FALSE/TRUE with animation */
void mildenhall_pullup_roller_set_show(MildenhallPullupRoller *pPullupRoller, gboolean bShow);

/* add attributes to be shown for roller items */
void mildenhall_pullup_roller_add_attribute(MildenhallPullupRoller *pPullupRoller, const gchar *pProperty, gint inColumn);

G_END_DECLS

#endif /* _MILDENHALL_PULLUP_ROLLER_H */

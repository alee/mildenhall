/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_rating_bottom_bar.h */

#ifndef __MILDENHALL_RATING_BOTTOM_BAR_H__
#define __MILDENHALL_RATING_BOTTOM_BAR_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_RATING_BOTTOM_BAR mildenhall_rating_bottom_bar_get_type()

#define MILDENHALL_RATING_BOTTOM_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_RATING_BOTTOM_BAR, MildenhallRatingBottomBar))

#define MILDENHALL_RATING_BOTTOM_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_RATING_BOTTOM_BAR, MildenhallRatingBottomBarClass))

#define MILDENHALL_IS_RATING_BOTTOM_BAR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_RATING_BOTTOM_BAR))

#define MILDENHALL_IS_RATING_BOTTOM_BAR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_RATING_BOTTOM_BAR))

#define MILDENHALL_RATING_BOTTOM_BAR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_RATING_BOTTOM_BAR, MildenhallRatingBottomBarClass))

typedef struct _MildenhallRatingBottomBar MildenhallRatingBottomBar;
typedef struct _MildenhallRatingBottomBarClass MildenhallRatingBottomBarClass;
typedef struct _MildenhallRatingBottomBarPrivate MildenhallRatingBottomBarPrivate;

struct _MildenhallRatingBottomBar
{
  ClutterActor parent;

  MildenhallRatingBottomBarPrivate *priv;
};

struct _MildenhallRatingBottomBarClass
{
	ClutterActorClass parent_class;
};

GType mildenhall_rating_bottom_bar_get_type (void) G_GNUC_CONST;

ClutterActor *mildenhall_rating_bottom_bar_new (void);

void
v_mildenhall_rating_bottom_bar_set_model (MildenhallRatingBottomBar *pRatingBottomBar, ThornburyModel *pModel);

G_END_DECLS

#endif /* __MILDENHALL_RATING_BOTTOM_BAR_H__ */

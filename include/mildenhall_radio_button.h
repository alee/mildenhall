/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_RADIO_BUTTON_H
#define _MILDENHALL_RADIO_BUTTON_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/******************************************************************************
 *	@Filename : mildenhall_radio_button.h
 *	@Created on : Jan 4, 2012
 *-----------------------------------------------------------------------------
 *------------------------------------------------------------------------------
 *  @Description : Header file for mildenhall_radio_button
 *******************************************************************************/

#include <glib-object.h>
#include <clutter/clutter.h>
#include "liblightwood-button.h"

G_BEGIN_DECLS

/********************************************************************************
 * @Defines
 *******************************************************************************/

#define MILDENHALL_TYPE_RADIO_BUTTON mildenhall_radio_button_get_type()

#define MILDENHALL_RADIO_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_RADIO_BUTTON, MildenhallRadioButton))

#define MILDENHALL_RADIO_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_RADIO_BUTTON, MildenhallRadioButtonClass))

#define MILDENHALL_IS_RADIO_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_RADIO_BUTTON))

#define MILDENHALL_IS_RADIO_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_RADIO_BUTTON))

#define MILDENHALL_RADIO_BUTTON_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_RADIO_BUTTON, MildenhallRadioButtonClass))

typedef struct _MildenhallRadioButton MildenhallRadioButton;
typedef struct _MildenhallRadioButtonClass MildenhallRadioButtonClass;
typedef struct _MildenhallRadioButtonPrivate MildenhallRadioButtonPrivate;

struct _MildenhallRadioButton
{
  LightwoodButton parent;

  MildenhallRadioButtonPrivate *priv;
};

struct _MildenhallRadioButtonClass
{
  LightwoodButtonClass parent_class;

  /* Hash Table related variables */
     GHashTable *pGroupID;				/* Hash Table */
     gchar* pLocalGroupId;				/* Hash Table Key:  Group ID */
     MildenhallRadioButton *pRadioButonlocal ;	/* Hash Table Value :  Radio Button Instance*/
    gint inInstanceCount;				/* Variable to hold the number of button instance created */

  /* signal */
  void (* rb_state_change) (GObject *pButton, gboolean inNewState , gpointer* pButtonId);
};

/********************************************************************************
 * @Prototypes of global functions
 *******************************************************************************/


GType mildenhall_radio_button_get_type (void) G_GNUC_CONST;

ClutterActor *mildenhall_radio_button_new (void);


/* setter functions */

void mildenhall_radio_button_set_current_state(MildenhallRadioButton *pRadioButton, gboolean inState);
void mildenhall_radio_button_set_group_id(MildenhallRadioButton *pRadioButton,  const GValue *pValue);
void mildenhall_radio_button_set_button_id(MildenhallRadioButton *pRadioButton,  gpointer *pButtonId);
void mildenhall_radio_button_set_highlight(MildenhallRadioButton *pRadioButton, gboolean bHighLightState);

/* getter functions */

gfloat mildenhall_radio_button_get_width( MildenhallRadioButton *pRadioButton);
gfloat mildenhall_radio_button_get_height( MildenhallRadioButton *pRadioButton);
gchar* mildenhall_radio_button_get_group_id(MildenhallRadioButton *pRadioButton);
gboolean mildenhall_radio_button_get_current_state( MildenhallRadioButton *pRadioButton);
gpointer *mildenhall_radio_button_get_button_id(MildenhallRadioButton *pRadioButton);
gboolean mildenhall_radio_button_get_highlight(MildenhallRadioButton *pRadioButton);



G_END_DECLS

#endif /* __MILDENHALL_RADIO_BUTTON_H__ */

/*** END OF FILE *************************************************************/

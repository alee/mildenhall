/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/***********************************************************************************
*       @Filename       :       roller-container.c
*       @Module         :       RollerContainer
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       18/07/2012
*----------------------------------------------------------------------------------
*       @Description    :       The basic idea is to test all the
*				properties of RollerContainer(fixed roller)
*				and also showcase the usage of lib in application.
*
************************************************************************************/

#include "mildenhall-internal.h"
#include "mildenhall_widget_container.h"

#include <thornbury/thornbury.h>

#include "mildenhall_roller_container.h"
#include "sample-item.h"
#include "test-utils.h"

/* model columns */
enum
{
        COLUMN_WIDGET_ACTOR,
        COLUMN_WIDGET_X,
        COLUMN_WIDGET_Y,
        COLUMN_WIDGET_LAST
};


static void roller_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer data)
{
	g_print("%s:row = %d\n",__FILE__,row);
	g_object_set(G_OBJECT(data), "move-left", 30.0, "maximize", 700.0, NULL);

}

static void roller2_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer data)
{
	g_print("%s:row = %d\n",__FILE__,row);
	g_object_set(G_OBJECT(data), "move-right", 400.0, /*"maximize", 50.0,*/ NULL);

}

static void left_animation_completed_cb(MildenhallWidgetContainer *pWidgetContainer, gpointer userData)
{
	g_print("%s \n", __FUNCTION__);
}

static void right_animation_completed_cb(MildenhallWidgetContainer *pWidgetContainer, gpointer userData)
{
	g_print("%s \n", __FUNCTION__);
}

static void maximize_completed_cb(MildenhallWidgetContainer *pWidgetContainer, gpointer userData)
{
	g_print("%s \n", __FUNCTION__);
}

static void minimize_completed_cb(MildenhallWidgetContainer *pWidgetContainer, gpointer userData)
{
	g_print("%s \n", __FUNCTION__);
}

int main(int argc,char ** argv)
{
  g_autofree gchar *pImage = NULL;
  g_autofree gchar *widget_container_prop_file = NULL;
  g_autofree gchar *roller_container_prop_file = NULL;
  ThornburyItemFactory *itemFactoryContainer;
  ThornburyItemFactory *itemFactory;
  ThornburyItemFactory *itemFactory1;
  ClutterInitError err = clutter_init(&argc, &argv);
  (void)err;
  //ClutterColor black = {0x00, 0x00, 0x00, 0xff};
  /* creating the stage */
  ClutterActor *stage = clutter_stage_new();
  pImage = g_build_filename (_mildenhall_get_theme_path (),
      "test-drawer-base",
      "background.png",
      NULL);
  ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
  clutter_actor_add_child(stage, actor);
  clutter_actor_set_size (stage, 728, 480);
  g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

  //MildenhallWidgetContainer *pWidgetCont = mildenhall_widget_container_new();
  /* create itemFactory instance for widget container */
  widget_container_prop_file = _mildenhall_get_resource_path ("mildenhall_widget_container_prop.json");
  itemFactoryContainer = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_WIDGET_CONTAINER, widget_container_prop_file);
  GObject *pObjContainer = NULL;

  /* Get roller instance from item factory */
  g_object_get(itemFactoryContainer, "object", &pObjContainer, NULL);
  ClutterActor *pWidgetContainer = CLUTTER_ACTOR(pObjContainer);

  /* create widget container model */
  ThornburyModel *pWidgetContModel = NULL;

  pWidgetContModel = (ThornburyModel*)thornbury_list_model_new (COLUMN_WIDGET_LAST,
                                        G_TYPE_OBJECT, NULL,
                                        G_TYPE_FLOAT, NULL,
                                        G_TYPE_FLOAT, NULL,
                                        -1);

/******************************************************************/
    /* create model */
  ThornburyModel *model = lightwood_create_model (3);

  /* create itemFactory instance */
  roller_container_prop_file = _mildenhall_get_resource_path ("mh_roller_container_prop.json");
  itemFactory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_ROLLER_CONTAINER, roller_container_prop_file);
  GObject *pObject = NULL;

  /* Get roller instance from item factory */
  g_object_get(itemFactory, "object", &pObject, NULL);
  ClutterActor *cont = CLUTTER_ACTOR(pObject);
  /* Set relevent roller Property according to the requirement*/
  g_object_set (pObject,
                "item-type", MILDENHALL_TYPE_SAMPLE_ITEM,
                "model", model,
                NULL);
  /* Set the attributes on the roller to be shown */
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "name", COLUMN_NAME);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "label", COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "icon", COLUMN_ICON);
  /* Add roller to stage */
  /* connect signal on item activation (double click)*/
  g_signal_connect (G_OBJECT (cont), "roller-item-activated", G_CALLBACK (roller_item_activated_cb), pWidgetContainer);
/**********************************************************************/

/**********************************************************************/
  /* similarly creation of 2nd roller */
  ThornburyModel *model1 = lightwood_create_model (100);
  itemFactory1 = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_ROLLER_CONTAINER, roller_container_prop_file);
  GObject *pObject1 = NULL;
  g_object_get(itemFactory1, "object", &pObject1, NULL);
  g_object_set (pObject1,
                "display-background", TRUE,
                "item-type", MILDENHALL_TYPE_SAMPLE_ITEM,
                "model", model1,
                NULL);

  ClutterActor *cont1 = CLUTTER_ACTOR(pObject1);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont1), "name", COLUMN_NAME);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont1), "label", COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont1), "icon", COLUMN_ICON);
  g_signal_connect (G_OBJECT (cont1), "roller-item-activated", G_CALLBACK (roller2_item_activated_cb), pWidgetContainer);
/*******************************************************************************/


  /* add these widgets to widget container model */
  thornbury_model_append (pWidgetContModel,
				COLUMN_WIDGET_ACTOR, cont,
				COLUMN_WIDGET_X, 5.0,
				COLUMN_WIDGET_Y, 0.0,
				-1);
  thornbury_model_append (pWidgetContModel,
				COLUMN_WIDGET_ACTOR, cont1,
				COLUMN_WIDGET_X, 345.0,
				COLUMN_WIDGET_Y, 0.0,
			-1);

  /* connect signals */
  g_signal_connect (G_OBJECT (pWidgetContainer), "move-left-completed", G_CALLBACK (left_animation_completed_cb), NULL);
  g_signal_connect (G_OBJECT (pWidgetContainer), "move-right-completed", G_CALLBACK (right_animation_completed_cb), NULL);
  g_signal_connect (G_OBJECT (pWidgetContainer), "minimize-completed", G_CALLBACK (minimize_completed_cb), NULL);
  g_signal_connect (G_OBJECT (pWidgetContainer), "maximize-completed", G_CALLBACK (maximize_completed_cb), NULL);


  g_object_set(pWidgetContainer, "model", pWidgetContModel, "minimize", 120.0, NULL);
  /* add to stage */
  clutter_actor_add_child(stage, CLUTTER_ACTOR(pWidgetContainer) );

  clutter_actor_show(stage);
  /* Run Main Loop */
  clutter_main();

  /* Returns success status on exiting main thread */
  return 0;

}

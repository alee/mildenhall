/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_H__
#define __MILDENHALL_H__

#define __MILDENHALL_H_INSIDE__


#pragma GCC diagnostic push
/* FIXME: Fix the headers: https://phabricator.apertis.org/T3073 */
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#pragma GCC diagnostic ignored "-Wredundant-decls"

#include "fast_sort_roller.h"
#include "lbs_bottom_bar.h"
#include "map_widget.h"
#include "mildenhall_audio_player_item.h"
#include "mildenhall_audio_player_songs_item.h"
#include "mildenhall_bottom_bar_type_one.h"
#include "mildenhall_button_drawer.h"
#include "mildenhall_button_speller.h"
#include "mildenhall_cabinet_item.h"
#include "mildenhall_cabinet_roller.h"
#include "mildenhall_context_drawer.h"
#include "mildenhall_drawer_base.h"
#include "mildenhall_info_roller.h"
#include "mildenhall_loading_bar.h"
#include "mildenhall_media_overlay.h"
#include "mildenhall_meta_info_footer.h"
#include "mildenhall_meta_info_header.h"
#include "mildenhall_navi_drawer.h"
#include "mildenhall_overlay.h"
#include "mildenhall_popup_strings.h"
#include "mildenhall_progress_bar.h"
#include "mildenhall_pullup_roller.h"
#include "mildenhall_radio_button.h"
#include "mildenhall_rating_bottom_bar.h"
#include "mildenhall_roller_container.h"
#include "mildenhall_scroller.h"
#include "mildenhall_selection_popup.h"
#include "mildenhall_selection_popup_item.h"
#include "mildenhall_speller_default_entry.h"
#include "mildenhall_speller_five_toggle_entry.h"
#include "mildenhall_speller_four_toggle_entry.h"
#include "mildenhall_speller.h"
#include "mildenhall_speller_multi_line_entry.h"
#include "mildenhall_speller_multiple_entry.h"
#include "mildenhall_speller_three_toggle_entry.h"
#include "mildenhall_text_box_entry.h"
#include "mildenhall_toggle_button.h"
#include "mildenhall_ui_utility.h"
#include "mildenhall_views_drawer.h"
#include "mildenhall_widget_container.h"
#include "popup_info.h"
#include "sample-item.h"
#include "sample-thumbnail-item.h"
#include "sample-variable-item.h"
#include "thumbnailitem.h"
#include "video-list-roller-item.h"
#include "video_thumb_roller.h"

#pragma GCC diagnostic pop

/* Generated headers */
#include "mildenhall-statusbar.h"

#endif /* __MILDENHALL_H__ */

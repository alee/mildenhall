/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *	@Filename :  mildenhall_selection_popup.h
 *	@Project: --
 *-----------------------------------------------------------------------------
 * 	@Created on : Jan 24, 2013
 *------------------------------------------------------------------------------
 *  @Description : Header file for the mildenhall selection popup widget
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description				Date			Name
 *	----------				----			----
 *
 *******************************************************************************/

#ifndef _MILDENHALL_SELECTION_POPUP_H
#define _MILDENHALL_SELECTION_POPUP_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/*******************************************************************************
 * @System Includes
 ******************************************************************************/

#include <glib-object.h>
#include <clutter/clutter.h>


/*******************************************************************************
 * @Project Includes
 ******************************************************************************/

#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"
#include "mildenhall_roller_container.h"
#include "mildenhall_selection_popup_item.h"
#include "mildenhall_button_speller.h"
#include "liblightwood-popupbase.h"

/******************************************************************************
 * @Defines
 *****************************************************************************/


G_BEGIN_DECLS

#define MILDENHALL_TYPE_SELECTION_POPUP mildenhall_selection_popup_get_type()

#define MILDENHALL_SELECTION_POPUP(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SELECTION_POPUP, MildenhallSelectionPopup))

#define MILDENHALL_SELECTION_POPUP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SELECTION_POPUP, MildenhallSelectionPopupClass))

#define MILDENHALL_IS_SELECTION_POPUP(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SELECTION_POPUP))

#define MILDENHALL_IS_SELECTION_POPUP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SELECTION_POPUP))

#define MILDENHALL_SELECTION_POPUP_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SELECTION_POPUP, MildenhallSelectionPopupClass))

typedef struct _MildenhallSelectionPopup MildenhallSelectionPopup;
typedef struct _MildenhallSelectionPopupClass MildenhallSelectionPopupClass;
typedef struct _MildenhallSelectionPopupPrivate MildenhallSelectionPopupPrivate;

struct _MildenhallSelectionPopup
{
  LightwoodPopupBase parent;

  MildenhallSelectionPopupPrivate *priv;
};

struct _MildenhallSelectionPopupClass
{
  LightwoodPopupBaseClass parent_class;
};



GType mildenhall_selection_popup_get_type (void) G_GNUC_CONST;

/********************************************************************************
 * @Prototypes of global functions
 *******************************************************************************/
/* function to be called to display the popup*/
void v_mildenhall_selection_popup_hide( MildenhallSelectionPopup *pSelectionPopup);
/* function to be called to hide the popup*/
void v_mildenhall_selection_popup_show( MildenhallSelectionPopup *pSelectionPopup);
/* Function to create the MILDENHALL selection popup object */
ClutterActor *mildenhall_selection_popup_new (void);
gfloat mildenhall_selection_popup_get_width (MildenhallSelectionPopup *pSelectionPopup);
gfloat mildenhall_selection_popup_get_height (MildenhallSelectionPopup *pSelectionPopup);
void v_mildenhall_selection_popup_set_model (MildenhallSelectionPopup *pSelectionPopup, ThornburyModel *pModel);

G_END_DECLS

#endif /* _MILDENHALL_SELECTION_POPUP_H */

/*** END OF FILE *************************************************************/

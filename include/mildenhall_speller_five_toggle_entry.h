/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_five_toggle_entry.h
 *
 *
 * mildenhall_speller_five_toggle_entry.h  */

#ifndef __MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY_H__
#define __MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY mildenhall_speller_five_toggle_entry_get_type()

#define MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY, MildenhallSpellerFiveToggleEntry))

#define MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY, MildenhallSpellerFiveToggleEntryClass))

#define MILDENHALL_IS_SPELLER_FIVE_TOGGLE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY))

#define MILDENHALL_IS_SPELLER_FIVE_TOGGLE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY))

#define MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_FIVE_TOGGLE_ENTRY, MildenhallSpellerFiveToggleEntryClass))

typedef struct _MildenhallSpellerFiveToggleEntry MildenhallSpellerFiveToggleEntry;
typedef struct _MildenhallSpellerFiveToggleEntryClass MildenhallSpellerFiveToggleEntryClass;
typedef struct _MildenhallSpellerFiveToggleEntryPrivate MildenhallSpellerFiveToggleEntryPrivate;

/**
 * MildenhallSpellerFiveToggleEntry:
 *
 * The #MildenhallSpellerFiveToggleEntry struct contains only private data.
 *
 */
struct _MildenhallSpellerFiveToggleEntry
{
  ClutterActor parent;
  MildenhallSpellerFiveToggleEntryPrivate *priv;
};


/**
 * MildenhallSpellerFiveToggleEntryClass:
 *
 * The #MildenhallSpellerFiveToggleEntryClass struct contains only private data.
 *
 */
struct _MildenhallSpellerFiveToggleEntryClass
{
  ClutterActorClass parent_class;
};

GType mildenhall_speller_five_toggle_entry_get_type (void) G_GNUC_CONST;

/*function to create new five toggle entry */
ClutterActor *mildenhall_speller_five_toggle_entry_new (void);

G_END_DECLS

#endif /* __MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY_H__ */

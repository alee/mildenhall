/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "popup_info.h"

#include <thornbury/thornbury.h>

#include "mildenhall_selection_popup.h"

gboolean shown_clb(PopupInfo *actor,gpointer data);
gboolean hidden_clb(PopupInfo *actor,gpointer data);
gboolean action_clb(PopupInfo *actor,gchar *button,gpointer value,gpointer data);

#define MILDENHALL_TEST_SELECTION_POPUP_PRINT( a ...)			g_print(a);


/* model columns */
enum
{
	COLUMN_TEXT,
	COLUMN_ACTIVE_ICON_PATH,
	COLUMN_INACTIVE_ICON_PATH,
	COLUMN_STATE_ID,
	COLUMN_LAST
};

typedef enum _enSelectionPopupModelInfo enSelectionPopupModelInfo;
enum _enSelectionPopupModelInfo
{
	TEXT_ENTRY_MSGSTYLE,
	TEXT_ENTRY_MSGTEXT,
	IMAGEICON_KEY,
	IMAGEPATH_VALUE,
	SELECTION_POPUP_ROWS_INFO,
	SELECTION_POPUP_COLUMN_LAST
};

ThornburyModel *pPopupTestModel= NULL;

static gboolean v_test_mildenhall_selection_popup_show (gpointer *data)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP(data);
	g_object_set( pSelectionPopup,"popup-model",pPopupTestModel,NULL);

	 g_timeout_add(2000, (GSourceFunc) v_mildenhall_selection_popup_show,  pSelectionPopup);

    return FALSE;
}

int main (int argc, char **argv)
{
	g_autofree gchar *request_inactive_icon_file = NULL;
	g_autofree gchar *prop_file = NULL;
	ThornburyItemFactory *itemFactory;
	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;

	ClutterActor *stage;
	ClutterColor white = { 0xff, 0xff, 0xff, 0x00 };

	stage = clutter_stage_new ();
	clutter_actor_set_background_color (stage, &white);
	clutter_actor_set_size (stage, 800, 480);
	g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	prop_file = _mildenhall_get_resource_path ("test_mildenhall_selection_popup_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
            MILDENHALL_TYPE_SELECTION_POPUP, prop_file);
	if(itemFactory == NULL)
	{
		MILDENHALL_TEST_SELECTION_POPUP_PRINT("\n \n item not created \n");
	}
	GObject *pObject = NULL;

	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP(pObject);
	ClutterActor *popup = CLUTTER_ACTOR(pSelectionPopup);

 pPopupTestModel = (ThornburyModel*)thornbury_list_model_new( SELECTION_POPUP_COLUMN_LAST,
                                         G_TYPE_STRING, NULL,
	                                     G_TYPE_STRING, NULL,
	                                     G_TYPE_STRING, NULL,
	                                     G_TYPE_STRING, NULL,
	                                     G_TYPE_POINTER, NULL,
	                                     -1);




    request_inactive_icon_file = g_build_filename (_mildenhall_get_theme_path (),
        "icon_request_inactive.png",
        NULL);
    thornbury_model_append (pPopupTestModel,
        TEXT_ENTRY_MSGSTYLE, "static",
        TEXT_ENTRY_MSGTEXT, 
            "The site could be temporarily unavailable or too busy. Try again in a few  moments. ",
        IMAGEICON_KEY, "AppIcon",
        IMAGEPATH_VALUE, request_inactive_icon_file,
        -1);

    thornbury_model_append (pPopupTestModel,
        TEXT_ENTRY_MSGSTYLE, "dynamic",
        TEXT_ENTRY_MSGTEXT, "hello world an MILDENHALL and phsase IIIIIII",
        IMAGEICON_KEY, "MsgIcon",
        IMAGEPATH_VALUE, request_inactive_icon_file,
        -1);

    g_timeout_add(5000, (GSourceFunc) v_test_mildenhall_selection_popup_show, (gpointer) pSelectionPopup);

  	clutter_actor_add_child(stage, popup);

	clutter_actor_show (stage);

	clutter_main();

	return 0;

}

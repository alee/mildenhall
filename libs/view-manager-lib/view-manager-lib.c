/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* view-manager-lib.c */

#include "view-manager-lib.h"

#include <clutter/x11/clutter-x11.h>
#include <gobject/gvaluecollector.h>
#include <thornbury/thornbury.h>

#include "mildenhall_roller_container.h"
#include "mildenhall_pullup_roller.h"
#include "mildenhall_info_roller.h"
#include "mildenhall_progress_bar.h"
#include "mildenhall_context_drawer.h"


static void full_screen_scale_in_com_cb(ClutterTimeline *timeline, gpointer pUserData);

static void on_event_animation(GObject *pActor, gpointer data , gpointer pUserData);
static void cover_to_detail_transition(MildenhallRollerContainer *roller, gint row,guint anim_duration);
static void exit_animation_completed_cb(ClutterTimeline *timeline, gpointer user_data);
static void start_exit_animation(ThornburyViewManagerViewData *exit_view, gboolean animation_needed,guint iSelected_row);
static void start_entry_animation(ThornburyViewManagerViewData *entry_view , gboolean animation_needed,guint iSelected_row);
static void roller_animation_completed_cb(ClutterTimeline *timeline,gpointer pUserData);
static void detail_animation_completed_cb(ClutterTimeline *timeline,  gpointer pUserData);
static void fs_slideout_animation_completed_cb(ClutterTimeline *timeline, gpointer user_data);
static void fs_scale_in_completed(ClutterActor *actor,const gchar  *transition_name,
		gboolean is_finished,gpointer pUserData);
static void fs_entry_completed_cb(ClutterTimeline *timeline, gpointer pUserData);
static void fs_fade_out_completed(ClutterTimeline *timeline, gpointer user_data);
static void detail_view_fade_out_completed(ClutterTimeline *timeline, gpointer user_data);

static gfloat roller_width=0.0;
static gfloat roller_height=0.0;
static gfloat roller_child_width=0.0;
static gfloat roller_child_height=0.0;

static ClutterActor *pfullScreenDummy = NULL;
static ClutterActor *pClonedObject = NULL;
ClutterActor *pFullScreenContainer = NULL;
static guint roller_selected_row = 0;
static gboolean in_func = FALSE;


static ThornburyViewManagerViewData *current_view = NULL;
static ThornburyViewManagerViewData *new_view = NULL;


G_DEFINE_TYPE (ViewManagerLib, view_manager_lib, THORNBURY_TYPE_VIEW_MANAGER)

#define MANAGER_LIB_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), VIEW_TYPE_MANAGER_LIB, ViewManagerLibPrivate))

struct _ViewManagerLibPrivate
{

};

struct _ViewManagerLibAnimInfo
{
	ClutterActor *widget;
	gchar *exit_effect;
	gint stage;
	GList *anim_params;
};

typedef struct _VMLibSignalHandlerData VMLibSignalHandlerData;
struct _VMLibSignalHandlerData
{
	ThornburyViewManager *pViewMangerInstance ;
	gpointer pUserData ;
};

static void
view_manager_lib_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
view_manager_lib_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
view_manager_lib_dispose (GObject *object)
{
	G_OBJECT_CLASS (view_manager_lib_parent_class)->dispose (object);
}

static void
view_manager_lib_finalize (GObject *object)
{
  G_OBJECT_CLASS (view_manager_lib_parent_class)->finalize (object);
}

static void
view_manager_lib_class_init (ViewManagerLibClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (ViewManagerLibPrivate));

  object_class->get_property = view_manager_lib_get_property;
  object_class->set_property = view_manager_lib_set_property;
  object_class->dispose = view_manager_lib_dispose;
  object_class->finalize = view_manager_lib_finalize;
}

static void
view_manager_lib_init (ViewManagerLib *self)
{
  self->priv = MANAGER_LIB_PRIVATE (self);
}

ViewManagerLib *
view_manager_lib_new (void)
{
	return g_object_new (VIEW_TYPE_MANAGER_LIB, NULL);

}

void v_switch_view (ThornburyViewManager *view_manager,
                    ThornburyViewManagerViewData *currentview,
                    ThornburyViewManagerViewData *newview,
                    gboolean animation_needed,
                    guint iSelected_row)
{
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

	thornbury_view_manager_close_widgets (view_manager, currentview->view_name);

	current_view = currentview;
	new_view = newview;


	roller_selected_row = iSelected_row;

	if((currentview->exit_effect != NULL)&&(g_strcmp0("",currentview->exit_effect)))
	{
	  start_exit_animation(currentview,animation_needed,iSelected_row);
	}
	else
	{
		g_signal_emit_by_name(view_manager,"view_switch_exit_anim_completed",currentview->view_name,newview->view_name);


		if((newview->entry_effect != NULL)&&(g_strcmp0("",newview->entry_effect)))
		{
			start_entry_animation(newview,animation_needed,iSelected_row);
		}
		else
		{
			g_signal_emit_by_name(view_manager,"view_switch_entry_anim_completed",newview->view_name);

		}
	}


}

ThornburyViewManagerError v_set_property(ThornburyViewManager *view_manager, ClutterActor* actor, va_list var_args)
{
	ThornburyViewManagerError err = (ThornburyViewManagerError)THORNBURY_VIEW_MANAGER_SET_PROP_ERR;
	const gchar *name = NULL;
	GObjectClass *klass = NULL;

	if(NULL != actor )
	{
		klass = G_OBJECT_GET_CLASS (actor);
	}
	else
	{
		g_warning("Invalid widget name!");
		err = (ThornburyViewManagerError)THORNBURY_VIEW_MANAGER_INVALID_WIDGET;
		return err;
	}

	/* Iterate over all the name/value pairs and add them to the hash
	 * table */
	name = va_arg (var_args, gchar*);
	while(name)
	{

		if(0 == g_strcmp0(name,""))
			return err;

//		VIEW_MANAGER_PRINT("VIEW MANAGER : property : %s\n",name);

		/* Get the param spec of the property name */
		GParamSpec *pspec = g_object_class_find_property (klass, name);
		GValue *value = g_new0( GValue , 1);
		gchar *error = NULL;

		/* if param spec is not found,set attribute */
		if (!pspec)
		{
			gint column = 0;
			/* FIXME: This should be improved not to use va_list. T1343 */
			if (g_strcmp0 (name, "progress-bar-width") == 0)
				 column = (gint) va_arg (var_args, gdouble);
			else
				 column = va_arg (var_args, gint);

			if (MILDENHALL_IS_ROLLER_CONTAINER (actor))
			{
				MildenhallRollerContainer *roller = MILDENHALL_ROLLER_CONTAINER (actor);
				mildenhall_roller_container_add_attribute (roller, name, column);
			}
			else if(MILDENHALL_IS_INFO_ROLLER(actor))
			{
				MildenhallInfoRoller *info_roller = MILDENHALL_INFO_ROLLER(actor);
				if(NULL != info_roller)
				{
					mildenhall_info_roller_add_attribute(info_roller,name,column);
				}

			}
			else if(MILDENHALL_IS_PULLUP_ROLLER(actor))
			{
				MildenhallPullupRoller *pullup_roller = MILDENHALL_PULLUP_ROLLER(actor);
				if(NULL != pullup_roller)
				{
					mildenhall_pullup_roller_add_attribute(pullup_roller,name,column);
				}

			}


		}
		/* Else collect the value and set the property of the object in
		 * the hash table.
		 * NOTE: DO NOT DO A g_object_set here. DO IT IN switch_view*/
		else
		{
			/* Collect the value for the particular property */
			g_value_init (value, G_PARAM_SPEC_VALUE_TYPE (pspec));
			G_VALUE_COLLECT (value, var_args, 0, &error);

			g_object_set_property(G_OBJECT(actor),name,value);
		}

		/* Get the next element of the name/value pair */
		name = va_arg (var_args, gchar*);
		err = (ThornburyViewManagerError)THORNBURY_VIEW_MANAGER_OK;
	}
	return err;
}

void v_set_animation(ThornburyViewManager * view_manager,ClutterActor *widget,ThornburyViewManagerAnimData *anim_data)
{
	if((widget != NULL) && (NULL != anim_data->signal_name))
	{
		if(!g_strcmp0(anim_data->signal_name, "")) return;

		VMLibSignalHandlerData *pVMLibSignalHandlerData = g_new0( VMLibSignalHandlerData , 1);
		pVMLibSignalHandlerData->pViewMangerInstance = view_manager ;
		pVMLibSignalHandlerData->pUserData = anim_data ;
		g_signal_connect(widget,anim_data->signal_name,G_CALLBACK(on_event_animation),pVMLibSignalHandlerData);
	}
}


static void on_event_animation(GObject *pActor, gpointer data , gpointer pUserData)
{
	VMLibSignalHandlerData *pVMLibSignalHandlerData = (VMLibSignalHandlerData*)pUserData;
	ThornburyViewManagerAnimData *animation = (ThornburyViewManagerAnimData*)pVMLibSignalHandlerData->pUserData;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	animation = (ThornburyViewManagerAnimData *) pVMLibSignalHandlerData->pUserData;

//	gchar *effect = g_strdup(animation->effect);

	if(!g_strcmp0(animation->effect, "cover_to_detail"))
	{
		gint row = (gint)data;

		if (MILDENHALL_IS_ROLLER_CONTAINER (pActor))
		{
			MildenhallRollerContainer *roller = MILDENHALL_ROLLER_CONTAINER (pActor);
			if(NULL != roller)
			{
				cover_to_detail_transition(roller,row,2000);
			}
		}
	}
}

static void
roller_animation_marker_cb (ClutterTimeline *timeline,
                            gchar           *marker_name,
                            gint             msecs,
                            gpointer pUserData)
{
  VMLibSignalHandlerData *pVMLibSignalHandlerData = (VMLibSignalHandlerData *) pUserData;
  ClutterActor *roller;

  g_return_if_fail (pVMLibSignalHandlerData != NULL);
  g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

  roller = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

  g_object_set(roller,"width", 277.0, NULL);

  /* FIXME: Freed userdata in signal handler.
   * This is incorrect since the handler can run multiple times.
   */
  g_free(pUserData);
}

static void cover_to_detail_transition(MildenhallRollerContainer *roller, gint row,guint anim_duration)
{
	//store the original width and height for further use.
	ThornburyViewManager *view_manager = thornbury_get_vm_ptr();
    g_object_get (roller, "child-height", &roller_child_height, "child-width", &roller_child_width,"width", &roller_width, "height", &roller_height,NULL);

	ClutterTimeline *timeline;
	mildenhall_roller_container_set_min_visible_actors (MILDENHALL_ROLLER_CONTAINER (roller), 16);
	mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);

  guint uinAnimduration = (anim_duration -100) ;
  uinAnimduration = anim_duration == 1 ? 1 : uinAnimduration;
	timeline = clutter_timeline_new (anim_duration);
	clutter_timeline_add_marker_at_time (timeline, "bar", uinAnimduration );

	VMLibSignalHandlerData *pRollAnimMarkerData = g_new0( VMLibSignalHandlerData , 1);
	pRollAnimMarkerData->pViewMangerInstance = view_manager ;
	pRollAnimMarkerData->pUserData = roller ;

	VMLibSignalHandlerData *pRollAnimCompleteData = g_new0( VMLibSignalHandlerData , 1);
	pRollAnimCompleteData->pViewMangerInstance = view_manager ;
	pRollAnimCompleteData->pUserData = roller ;

	g_signal_connect (G_OBJECT (timeline), "marker-reached", G_CALLBACK (roller_animation_marker_cb),pRollAnimMarkerData);
	g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (roller_animation_completed_cb),pRollAnimCompleteData);

	mildenhall_roller_container_set_display_background (MILDENHALL_ROLLER_CONTAINER (roller),FALSE);
	mildenhall_roller_container_resize (MILDENHALL_ROLLER_CONTAINER (roller),
						277.0, /* roller width */
						277.0, /* item width */
						300.0, /* item height */
						row,
						timeline, TRUE);


  clutter_actor_save_easing_state (CLUTTER_ACTOR(roller));
  clutter_actor_set_easing_mode (CLUTTER_ACTOR(roller),CLUTTER_EASE_IN_QUART);
  clutter_actor_set_easing_duration (CLUTTER_ACTOR(roller), anim_duration);
  clutter_actor_set_y(CLUTTER_ACTOR(roller) , 28);
  clutter_actor_restore_easing_state (CLUTTER_ACTOR(CLUTTER_ACTOR(roller)));

}

static void exit_animation_completed_cb(ClutterTimeline *timeline, gpointer user_data)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = user_data;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	/* FIXME: Shouldn't we use ThornburyViewManager form user_data? */
	view_manager = thornbury_get_vm_ptr ();

	if(NULL != view_manager)
	{
		g_signal_emit_by_name(view_manager,"view_switch_exit_anim_completed",
				current_view->view_name,new_view->view_name);

		if((new_view->entry_effect != NULL)&&(g_strcmp0("",new_view->entry_effect)))
		{
			start_entry_animation(new_view,FALSE,roller_selected_row);
		}
		else
		{
			g_signal_emit_by_name(view_manager,"view_switch_entry_anim_completed",
					new_view->view_name);
		}


	}

}

static void start_exit_animation(ThornburyViewManagerViewData *exit_view, gboolean animation_needed,guint iSelected_row)
{
	ClutterActor *app;


	ThornburyViewManager *view_manager = thornbury_get_vm_ptr();
	if(NULL == view_manager) return;

	guint anim_duration;

	if(animation_needed)
		anim_duration = 2000;
	else
		anim_duration = 1;

	MildenhallRollerContainer *roller = NULL;
	roller = MILDENHALL_ROLLER_CONTAINER (thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                                 exit_view->view_name,
                                                                                 "MildenhallRollerContainer"));

	g_object_get (view_manager, "clutter-main", &app, NULL);

	if(!g_strcmp0(exit_view->exit_effect, "cover_to_detail"))
	{
		if(-1 == iSelected_row)
		{
			GHashTable *propHash = NULL;
			propHash =  g_hash_table_new(g_str_hash, g_str_equal);
			propHash = thornbury_get_property(view_manager,g_strdup(clutter_actor_get_name(CLUTTER_ACTOR(roller))),"focused-row", NULL);
			iSelected_row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));
		}

		if (NULL != roller)
		{
			cover_to_detail_transition(MILDENHALL_ROLLER_CONTAINER (roller),iSelected_row,anim_duration);
		}

	}
	if(!g_strcmp0(exit_view->exit_effect, "full_screen"))
	{

		//stage 1: Get the clicked actor and add it to the dummy container and SCALE_IN
		//stage 2 : expand the window, stage
		//stage 3 : move all widgets by 72 pixels
		//stage 4 : start sliding out the bottom widgets (views/search drawer, BACK, progress bar) and SCALE out the dummy actor
		//stage 5 : show full screen view
		//stage 6 : slide in the exit fullscreen button, aspectratio, progressbar widgets from below.This could be entry effect of full screen view
		ClutterActor *pMedia_item;

		in_func = FALSE;

		ClutterTransition* group = clutter_transition_group_new();

	  clutter_timeline_set_duration (CLUTTER_TIMELINE (group), 500);//anim_duration);

		//get the selected actor from roller

		pMedia_item = mildenhall_roller_container_get_actor_for_row (roller, roller_selected_row);

		pFullScreenContainer = clutter_actor_new();
		ClutterColor *color = clutter_color_new(0x00,0x00,0x00,0xff);
		clutter_actor_set_background_color (pFullScreenContainer,color);
		clutter_actor_set_size(pFullScreenContainer,277.0,280.0);
		clutter_actor_set_position(pFullScreenContainer,clutter_actor_get_x(pMedia_item),115);

		//clone it and set it's initial size and position
		pfullScreenDummy = clutter_clone_new(pMedia_item);
		clutter_actor_set_position(pfullScreenDummy,0,115);
		clutter_actor_set_size(pfullScreenDummy,277.0,280.0);
		clutter_actor_set_reactive(pfullScreenDummy,TRUE);
		//get the stage and add it the clone above to the stage directly..
		clutter_actor_add_child(app, pFullScreenContainer);
		clutter_actor_add_child(app, pfullScreenDummy);
		//clutter_actor_show(pFullScreenContainer);

		//scale in the actor first

		ClutterTransition *transition = clutter_property_transition_new ("scale-x");
		clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (transition),  CLUTTER_LINEAR);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (transition), 100);
		clutter_transition_set_from (transition, G_TYPE_FLOAT, 1.0);
		clutter_transition_set_to (transition, G_TYPE_FLOAT, 1.0);
		clutter_transition_group_add_transition (CLUTTER_TRANSITION_GROUP(group),transition);
		g_object_unref(transition);


		transition = clutter_property_transition_new ("scale-y");
		clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (transition),  CLUTTER_LINEAR);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (transition), 100);
		clutter_transition_set_from (transition, G_TYPE_FLOAT, 1.0);
		clutter_transition_set_to (transition, G_TYPE_FLOAT, 0.73);
		clutter_transition_group_add_transition (CLUTTER_TRANSITION_GROUP(group),transition);
		g_object_unref(transition);


		transition = clutter_property_transition_new ("y");
		clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (transition),  CLUTTER_LINEAR);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (transition), 100);
		clutter_transition_set_from (transition, G_TYPE_INT, 115);
		clutter_transition_set_to (transition, G_TYPE_INT, 150);
		clutter_transition_group_add_transition (CLUTTER_TRANSITION_GROUP(group),transition);
		g_object_unref(transition);


		clutter_actor_remove_all_transitions (pfullScreenDummy);
		clutter_actor_add_transition(pfullScreenDummy,"full_screen_scale_in",CLUTTER_TRANSITION(group));

		VMLibSignalHandlerData *pVMLibSignalHandlerData = g_new0( VMLibSignalHandlerData , 1);
		pVMLibSignalHandlerData->pViewMangerInstance = view_manager ;
		pVMLibSignalHandlerData->pUserData = GINT_TO_POINTER(animation_needed) ;

		g_signal_connect (pfullScreenDummy, "transition-stopped",G_CALLBACK (fs_scale_in_completed), pVMLibSignalHandlerData);

		g_object_unref(group);

	}

	if(!g_strcmp0(exit_view->exit_effect, "exit_full_screen"))
	{
		GList *drawerList;
		ClutterActor *progress_bar;

		progress_bar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                             current_view->view_name,
                                                                             "MildenhallProgressBar");

		clutter_actor_save_easing_state (CLUTTER_ACTOR(progress_bar));
		clutter_actor_set_easing_mode (progress_bar,CLUTTER_LINEAR);
		clutter_actor_set_easing_duration (progress_bar, 800);
		clutter_actor_set_position (progress_bar, clutter_actor_get_x(progress_bar),clutter_actor_get_y(progress_bar) + 125.0);
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(progress_bar));

		drawerList = thornbury_view_manager_get_actor_list_by_typename (view_manager,
                                                                                current_view->view_name,
                                                                                "MildenhallContextDrawer");
		gint innerloop;
		for(innerloop = 0; innerloop < g_list_length(drawerList); innerloop++)
		{
			ClutterActor *drawer = CLUTTER_ACTOR(g_list_nth_data(drawerList,innerloop));
			clutter_actor_save_easing_state (drawer);
			clutter_actor_set_easing_mode (drawer,CLUTTER_LINEAR);
			clutter_actor_set_easing_duration (drawer, 800);
			clutter_actor_set_position (drawer, clutter_actor_get_x(drawer), clutter_actor_get_y(drawer) + 125.0);
			clutter_actor_restore_easing_state (drawer);
		}
		if(NULL != roller)
		{
			ClutterTimeline *timeline;
			ClutterActor *pMedia_item;
			mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);
			pMedia_item = mildenhall_roller_container_get_actor_for_row (roller, roller_selected_row);
			pClonedObject = clutter_clone_new(pMedia_item);
			clutter_actor_set_position(pClonedObject,0,46);
			clutter_actor_set_size(pClonedObject,728.0,435.0);
			clutter_actor_add_child (app ,pClonedObject);

			g_object_set(roller,"show-flow-layout", TRUE, NULL);
			g_object_set(roller,"cylinder-distort", TRUE, NULL);


			timeline = clutter_timeline_new (1);//anim_duration);
			clutter_timeline_add_marker_at_time (timeline, "bar", 1 );

			VMLibSignalHandlerData *pRollAnimMarkerData = g_new0( VMLibSignalHandlerData , 1);
			pRollAnimMarkerData->pViewMangerInstance = view_manager ;
			pRollAnimMarkerData->pUserData = roller ;

			VMLibSignalHandlerData *pRollAnimCompleteData = g_new0( VMLibSignalHandlerData , 1);
			pRollAnimCompleteData->pViewMangerInstance = view_manager ;
			pRollAnimCompleteData->pUserData = roller ;



      g_signal_connect (G_OBJECT (timeline), "marker-reached",
    		  G_CALLBACK (roller_animation_marker_cb), pRollAnimMarkerData);
			g_signal_connect (G_OBJECT (timeline), "completed",
					G_CALLBACK (roller_animation_completed_cb),pRollAnimCompleteData);

			mildenhall_roller_container_resize (MILDENHALL_ROLLER_CONTAINER (roller),
								277.0, /* roller width */
								277.0, /* item width */
								300.0, /* item height */
								iSelected_row,
								timeline, TRUE);


		//Thushar

			ClutterTimeline *pFStimeline =  clutter_timeline_new (1000);//anim_duration);

			VMLibSignalHandlerData *pFullScreenScaleData = g_new0( VMLibSignalHandlerData , 1);
			pFullScreenScaleData->pViewMangerInstance = view_manager ;
			pFullScreenScaleData->pUserData = pClonedObject ;

			g_signal_connect (G_OBJECT (pFStimeline), "completed",
					G_CALLBACK (full_screen_scale_in_com_cb),pFullScreenScaleData);
			clutter_timeline_start(pFStimeline);

//	       	clutter_actor_set_scale (pClonedObject,5.0, 5.0);
		clutter_actor_save_easing_state (pClonedObject);
		 clutter_actor_set_easing_mode (pClonedObject,CLUTTER_LINEAR);
	        //clutter_actor_set_easing_delay (pClonedObject, 100);
        	clutter_actor_set_easing_duration (pClonedObject, 800);
        	clutter_actor_set_position (pClonedObject,0,115);//50);
		clutter_actor_restore_easing_state (pClonedObject);

		clutter_actor_save_easing_state (pClonedObject);
        	clutter_actor_set_easing_mode (pClonedObject,CLUTTER_LINEAR);
        	clutter_actor_set_easing_duration (pClonedObject, 800);
        	clutter_actor_set_width (pClonedObject, 277);//765);
		clutter_actor_restore_easing_state (pClonedObject);

		clutter_actor_save_easing_state (pClonedObject);
	        clutter_actor_set_easing_mode (pClonedObject,CLUTTER_LINEAR);
//	        clutter_actor_set_easing_delay (pClonedObject, 400);
	        clutter_actor_set_easing_duration (pClonedObject, 800);
	        clutter_actor_set_height (pClonedObject, 280);// 420);
		clutter_actor_restore_easing_state (pClonedObject);
	        //clutter_actor_set_easing_mode (pClonedObject,CLUTTER_LINEAR);
			//clutter_actor_set_easing_delay (pClonedObject, 100);
	        //clutter_actor_set_easing_duration (pClonedObject, 500);

		}
	}

	g_object_unref (app);
}

static void cover_roll_resize_completed_cb(ClutterTimeline *timeline, gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;
	ClutterActor *roller = NULL;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	roller = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

	view_manager = thornbury_get_vm_ptr();

	if(NULL == view_manager) return;

	g_signal_handlers_disconnect_by_func(timeline, cover_roll_resize_completed_cb, pUserData);

	if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
	{
  	g_object_set(roller,"width", roller_width, NULL);
		g_object_set (roller, "child-height", roller_child_height, NULL);
		g_object_set (roller, "child-width", roller_child_width, NULL);
		g_object_set (roller, "natural-width", roller_child_width, NULL);
	}
}

static void start_entry_animation(ThornburyViewManagerViewData *entry_view , gboolean animation_needed,guint iSelected_row)
{

	ThornburyViewManager *view_manager = thornbury_get_vm_ptr();
	if(NULL == view_manager) return;

	MildenhallRollerContainer *roller = NULL;
	roller = MILDENHALL_ROLLER_CONTAINER (thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                                 entry_view->view_name,
                                                                                 "MildenhallRollerContainer"));


	if(!g_strcmp0(entry_view->entry_effect, "detail_to_cover"))
	{
		if(NULL == roller)
		{
			g_printerr("Invalid effect!\n");

		}
		else
		{
			if(-1 == iSelected_row)
			{
				//get the actual focussed row here..
				GHashTable *propHash = NULL;
				propHash =  g_hash_table_new(g_str_hash, g_str_equal);
				propHash = thornbury_get_property(view_manager,g_strdup(clutter_actor_get_name(CLUTTER_ACTOR(roller))),"focused-row", NULL);
				iSelected_row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));

			}

			ClutterTimeline *timeline , *tDummyTimeline;

      timeline = clutter_timeline_new (1);
      tDummyTimeline = clutter_timeline_new (250);//Added extra delay to avoid flicker of roller resizing..

      VMLibSignalHandlerData *pCoverRollResizeData = g_new0( VMLibSignalHandlerData , 1);
      pCoverRollResizeData->pViewMangerInstance = view_manager ;
      pCoverRollResizeData->pUserData = roller ;

      VMLibSignalHandlerData *pDetailAnimData = g_new0( VMLibSignalHandlerData , 1);
      pDetailAnimData->pViewMangerInstance = view_manager ;
      pDetailAnimData->pUserData = roller ;

      g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (cover_roll_resize_completed_cb), pCoverRollResizeData);
      g_signal_connect (G_OBJECT (tDummyTimeline), "completed", G_CALLBACK (detail_animation_completed_cb), pDetailAnimData);
      clutter_timeline_start(tDummyTimeline);

      mildenhall_roller_container_resize ( MILDENHALL_ROLLER_CONTAINER (roller),
                            roller_width, /* roller width */
                            roller_child_width,     /* item width */
                            roller_child_height,     /* item height */
                            iSelected_row,
                            timeline,
			    			FALSE);
		}
	}
	if(!g_strcmp0(entry_view->entry_effect, "enter_full_screen"))
	{
		if(-1 == iSelected_row)
		{
			//get the actual focussed row here..
			GHashTable *propHash = NULL;
			propHash =  g_hash_table_new(g_str_hash, g_str_equal);
			propHash = thornbury_get_property(view_manager,g_strdup(clutter_actor_get_name(CLUTTER_ACTOR(roller))),"focused-row", NULL);
			iSelected_row = g_value_get_int(g_hash_table_lookup(propHash, "focused-row"));

		}

		ClutterTimeline *timeline ;
		mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);
		mildenhall_roller_container_set_display_background (MILDENHALL_ROLLER_CONTAINER (roller), FALSE);
    //clutter_actor_set_width(current_view->view,728);
    g_object_set(roller,"cylinder-distort", FALSE, NULL);

    timeline = clutter_timeline_new (100);



    VMLibSignalHandlerData *pVMLibSignalHandlerData = g_new0( VMLibSignalHandlerData , 1);
    pVMLibSignalHandlerData->pViewMangerInstance = view_manager ;
    pVMLibSignalHandlerData->pUserData = roller ;
   	g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (fs_entry_completed_cb), pVMLibSignalHandlerData);

    mildenhall_roller_container_resize ( MILDENHALL_ROLLER_CONTAINER (roller),
                          728.0,
                          728.0,
                          435.0,
                          iSelected_row,
                          timeline, FALSE);

	}
}

static void roller_animation_completed_cb(ClutterTimeline *timeline, gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;
	ClutterActor *roller = NULL;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	roller = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

	view_manager = thornbury_get_vm_ptr ();
	if(NULL == view_manager) return;

	g_signal_handlers_disconnect_by_func(timeline, roller_animation_completed_cb, pUserData);

	if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
	{
		g_object_set (roller, "child-height", 300.0, NULL);
		g_object_set (roller, "child-width", 277.0, NULL);
		g_object_set (roller, "natural-width", 277.0, NULL);
		mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), FALSE);
		mildenhall_roller_container_set_display_background (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);
	}

	if(NULL != view_manager)
	{
		g_signal_emit_by_name(view_manager,"view_switch_exit_anim_completed",current_view->view_name,new_view->view_name);
		if((new_view->entry_effect != NULL)&&(g_strcmp0("",new_view->entry_effect)))
			start_entry_animation(new_view,FALSE,roller_selected_row);
		else
		{
			g_signal_emit_by_name(view_manager,"view_switch_entry_anim_completed",new_view->view_name);
			if(!(g_strcmp0(current_view->view_name,"FullScreenView")))
			{
        			ClutterActor *progress_bar;

				progress_bar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                                             new_view->view_name,
                                                                                             "MildenhallProgressBar");

        			clutter_actor_set_y (progress_bar,clutter_actor_get_y (progress_bar) + 125);
			}
		}
	}
	g_free(pUserData);
}

static void fs_slideout_animation_completed_cb(ClutterTimeline *timeline, gpointer  pUserData)
{
}

static void detail_animation_completed_cb(ClutterTimeline *timeline, gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	view_manager = thornbury_get_vm_ptr ();
	if(NULL == view_manager) return;
	g_signal_handlers_disconnect_by_func(timeline, detail_animation_completed_cb, pUserData);

	if(NULL != new_view->view_name)
		g_signal_emit_by_name(view_manager,"view_switch_entry_anim_completed",new_view->view_name);

}


static void fs_scale_in_completed(ClutterActor *actor,const gchar  *transition_name,gboolean is_finished,gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;
	ClutterActor *context_drawer;
	ClutterActor *progress_bar;
	ClutterActor *bottombar;
	gboolean bAnimationNeeded;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	bAnimationNeeded = GPOINTER_TO_INT(pVMLibSignalHandlerData->pUserData);

	/* FIXME: Shouln't it be a parameter? */
	if(in_func)return;

	view_manager = thornbury_get_vm_ptr ();
	if(NULL == view_manager) return;

	in_func = TRUE;

	if(NULL != actor)
	{
		clutter_actor_remove_transition(actor, "full_screen_scale_in");
		clutter_actor_remove_all_transitions (actor);
		g_signal_handlers_disconnect_by_func(actor, fs_scale_in_completed, pUserData);
	}


	//slide down the progress bar and drawer widgets
	/************************************************************************************************************************************************************/
	ClutterTimeline *timeline = clutter_timeline_new(700);

    VMLibSignalHandlerData *pFsSlideoutAnimData = g_new0( VMLibSignalHandlerData , 1);
    pFsSlideoutAnimData->pViewMangerInstance = view_manager ;
    pFsSlideoutAnimData->pUserData = current_view ;

	g_signal_connect(timeline,"completed",
			G_CALLBACK(fs_slideout_animation_completed_cb),pFsSlideoutAnimData);

	clutter_timeline_start(timeline);


	context_drawer = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                       current_view->view_name,
                                                                       "MildenhallContextDrawer");

	bottombar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                  current_view->view_name,
                                                                  "BottomBar");

	progress_bar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                     current_view->view_name,
                                                                     "MildenhallProgressBar");

	clutter_actor_save_easing_state (CLUTTER_ACTOR(context_drawer));
	clutter_actor_set_easing_mode (context_drawer,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (context_drawer, 1000);
	clutter_actor_set_position (context_drawer, clutter_actor_get_x(context_drawer), clutter_actor_get_y(context_drawer) + 125.0);
	clutter_actor_restore_easing_state (CLUTTER_ACTOR(context_drawer));

	clutter_actor_save_easing_state (CLUTTER_ACTOR(progress_bar));
	clutter_actor_set_easing_mode (progress_bar,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (progress_bar, 1000);
	clutter_actor_set_position (progress_bar, clutter_actor_get_x(progress_bar), clutter_actor_get_y(progress_bar) + 125.0);
	clutter_actor_restore_easing_state (CLUTTER_ACTOR(progress_bar));

	clutter_actor_save_easing_state (CLUTTER_ACTOR(bottombar));
	clutter_actor_set_easing_mode (bottombar,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (bottombar, 1000);
	clutter_actor_set_position (bottombar, clutter_actor_get_x(bottombar), clutter_actor_get_y(bottombar) + 125.0);
	clutter_actor_restore_easing_state (CLUTTER_ACTOR(bottombar));

#if 10
	ClutterTimeline *anim_time = clutter_timeline_new(2000);

    VMLibSignalHandlerData *pExitAnimCompleteData = g_new0( VMLibSignalHandlerData , 1);
    pExitAnimCompleteData->pViewMangerInstance = view_manager ;
    pExitAnimCompleteData->pUserData = GINT_TO_POINTER(bAnimationNeeded);

	g_signal_connect(G_OBJECT(anim_time),"completed",
			G_CALLBACK (exit_animation_completed_cb), pExitAnimCompleteData);
	clutter_timeline_start(anim_time);
#endif

	clutter_actor_set_easing_mode (pfullScreenDummy,CLUTTER_LINEAR);
	clutter_actor_set_easing_delay (pfullScreenDummy, 10);
	clutter_actor_set_easing_duration (pfullScreenDummy, 1000);
	clutter_actor_set_position (pfullScreenDummy, 0, 45);//50);

	clutter_actor_set_easing_mode (pfullScreenDummy,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (pfullScreenDummy, 1000);
	clutter_actor_set_width (pfullScreenDummy, 728);//765);


	clutter_actor_set_easing_mode (pfullScreenDummy,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (pfullScreenDummy, 1000);
	clutter_actor_set_height (pfullScreenDummy, 435);// 420);

	clutter_actor_set_easing_mode (pfullScreenDummy,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (pfullScreenDummy, 1000);
	clutter_actor_set_scale (pfullScreenDummy, 1.0, 1.0);
}

static void fs_fade_out_completed(ClutterTimeline *timeline, gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;
	ClutterActor *roller = NULL;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	roller = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

	view_manager = thornbury_get_vm_ptr ();
	if(NULL == view_manager) return;

	clutter_actor_hide(pFullScreenContainer);
	clutter_actor_destroy(pFullScreenContainer);

	clutter_actor_hide(pfullScreenDummy);
	clutter_actor_destroy(pfullScreenDummy);
	clutter_actor_show(CLUTTER_ACTOR(roller));

}

static void fs_entry_completed_cb(ClutterTimeline *timeline, gpointer pUserData)
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;
	ClutterActor *roller = NULL;
	ClutterActor *progress_bar;
	GList *drawerList;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	roller = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

	view_manager = thornbury_get_vm_ptr ();
	if(NULL == view_manager) return;

	if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
	{

 		g_object_set(roller,"show-flow-layout", FALSE, NULL);

		g_object_set(roller,"width", 728.0, NULL);
		g_object_set (roller, "child-width", 728.0, NULL);
		g_object_set (roller, "child-height", 435.0, NULL);
		g_object_set (roller, "natural-width", 728.0, NULL);

		mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), FALSE);
		mildenhall_roller_container_set_display_background (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);
		clutter_actor_set_opacity (pFullScreenContainer, 0);
		ClutterTimeline *anim_time = clutter_timeline_new(500);

	    VMLibSignalHandlerData *pVMLibSignalHandlerData = g_new0( VMLibSignalHandlerData , 1);
	    pVMLibSignalHandlerData->pViewMangerInstance = view_manager ;
	    pVMLibSignalHandlerData->pUserData = roller ;

		g_signal_connect(G_OBJECT(anim_time),"completed", G_CALLBACK (fs_fade_out_completed), pVMLibSignalHandlerData);
		clutter_timeline_start(anim_time);
		current_view->exit_effect = NULL;
	}

	g_signal_emit_by_name(view_manager,"view_switch_entry_anim_completed",new_view->view_name);


		progress_bar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                             new_view->view_name,
                                                                             "MildenhallProgressBar");

		clutter_actor_save_easing_state (CLUTTER_ACTOR(progress_bar));
		clutter_actor_set_easing_mode (progress_bar,CLUTTER_LINEAR);
		clutter_actor_set_easing_duration (progress_bar, 800);
		clutter_actor_set_position (progress_bar, clutter_actor_get_x(progress_bar),clutter_actor_get_y(progress_bar) - 125.0);
		clutter_actor_restore_easing_state (CLUTTER_ACTOR(progress_bar));


		drawerList = thornbury_view_manager_get_actor_list_by_typename (view_manager,
                                                                                new_view->view_name,
                                                                                "MildenhallContextDrawer");
		gint innerloop;
		for(innerloop = 0; innerloop < g_list_length(drawerList); innerloop++)
		{
		ClutterActor *drawer = CLUTTER_ACTOR(g_list_nth_data(drawerList,innerloop));
		clutter_actor_save_easing_state (drawer);
		clutter_actor_set_easing_mode (drawer,CLUTTER_LINEAR);
		clutter_actor_set_easing_duration (drawer, 800);
		clutter_actor_set_position (drawer, clutter_actor_get_x(drawer), clutter_actor_get_y(drawer) - 125.0);
		clutter_actor_restore_easing_state (drawer);
		}
}

static void detail_view_fade_out_completed(ClutterTimeline *timeline, gpointer user_data)
{
  /* FIXME: Freed global variable in singal handler? */
  clutter_actor_destroy(pClonedObject);
  /* FIXME: Freed user data in singal handler? */
  g_free(user_data);
}
static void full_screen_scale_in_com_cb(ClutterTimeline *timeline, gpointer pUserData  )
{
	ThornburyViewManager *view_manager;
	VMLibSignalHandlerData *pVMLibSignalHandlerData = pUserData;

	g_return_if_fail (pVMLibSignalHandlerData != NULL);
	g_return_if_fail (pVMLibSignalHandlerData->pUserData != NULL);
	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (pVMLibSignalHandlerData->pViewMangerInstance));

	/* FIXME: Shouldn't it be a local variable? */
	pClonedObject = CLUTTER_ACTOR (pVMLibSignalHandlerData->pUserData);

	view_manager = thornbury_get_vm_ptr ();
	if(NULL != view_manager)
	{
		ClutterActor *context_drawer;
		ClutterActor *progress_bar;
		ClutterActor *bottombar;

		clutter_actor_set_opacity (pClonedObject, 0);
		ClutterTimeline *anim_time = clutter_timeline_new(500);

	    VMLibSignalHandlerData *pVMLibSignalHandlerData = g_new0( VMLibSignalHandlerData , 1);
	    pVMLibSignalHandlerData->pViewMangerInstance = view_manager ;
	    pVMLibSignalHandlerData->pUserData = NULL  ;

		g_signal_connect(G_OBJECT(anim_time),"completed",
				G_CALLBACK (detail_view_fade_out_completed), pVMLibSignalHandlerData);
		clutter_timeline_start(anim_time);

		context_drawer = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                               new_view->view_name,
                                                                               "MildenhallContextDrawer");

		bottombar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                          new_view->view_name,
                                                                          "BottomBar");

		progress_bar = thornbury_view_manager_get_actor_by_typename (view_manager,
                                                                             new_view->view_name,
                                                                             "MildenhallProgressBar");

    clutter_actor_save_easing_state (CLUTTER_ACTOR(context_drawer));
    clutter_actor_set_easing_mode (context_drawer,CLUTTER_LINEAR);
    clutter_actor_set_easing_duration (context_drawer, 1000);
    clutter_actor_set_position (context_drawer, clutter_actor_get_x(context_drawer), clutter_actor_get_y(context_drawer) - 125.0);
    clutter_actor_restore_easing_state (CLUTTER_ACTOR(context_drawer));

    clutter_actor_save_easing_state (CLUTTER_ACTOR(progress_bar));
    clutter_actor_set_easing_mode (progress_bar,CLUTTER_LINEAR);
    clutter_actor_set_easing_duration (progress_bar, 1000);
    clutter_actor_set_position (progress_bar, clutter_actor_get_x(progress_bar), clutter_actor_get_y(progress_bar) - 125.0);
    clutter_actor_restore_easing_state (CLUTTER_ACTOR(progress_bar));

    clutter_actor_save_easing_state (CLUTTER_ACTOR(bottombar));
    clutter_actor_set_easing_mode (bottombar,CLUTTER_LINEAR);
    clutter_actor_set_easing_duration (bottombar, 1000);
    clutter_actor_set_position (bottombar, clutter_actor_get_x(bottombar), clutter_actor_get_y(bottombar) - 125.0);
    clutter_actor_restore_easing_state (CLUTTER_ACTOR(bottombar));
	}
	g_free(pUserData);
}


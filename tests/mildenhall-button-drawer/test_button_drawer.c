/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_button_drawer.h"
#include "mildenhall-internal.h"

#include <glib/gi18n.h>
#include <locale.h>
#include <thornbury/thornbury.h>

/* model columns */
enum
{
	COLUMN_ICON_ACTIVE,
	//COLUMN_ICON_INACTIVE,
	COLUMN_TOOLTIP_TEXT,
	COLUMN_LAST
};

/* creation of model */
ThornburyModel *create_model()
{
	g_autofree gchar *active_music_icon_file = NULL;
	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
					G_TYPE_STRING, NULL,
					//G_TYPE_STRING, NULL,
					G_TYPE_STRING, NULL,
					-1);

	active_music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
	    "test-button-drawer",
	    "icon_music_AC.png",
	    NULL);

	thornbury_model_append (model, COLUMN_ICON_ACTIVE, active_music_icon_file,
					//COLUMN_ICON_INACTIVE, "test-button-drawer/icon_music_IN.png",
				     	COLUMN_TOOLTIP_TEXT,_("MUSIC"),
				     -1);

	return model;

}

/* tooltip shown callback */
static void tooltip_shown(GObject *button, gpointer pUserData)
{
	g_print("MILDENHALL_BUTTON_DRAWER_TEST: %s\n", __FUNCTION__);


	g_object_set(button, "reactive", FALSE, NULL);
}

/* tooltip hidden callback */
static void tooltip_hidden(GObject *button, gpointer pUserData)
{
	g_print("MILDENHALL_BUTTON_DRAWER_TEST: %s\n", __FUNCTION__);
	gint64 state;
	g_object_get(button, "current-state", &state, NULL);
	if(state == 0)
		g_object_set(button,  "current-state", (gint64)1, NULL);
	else if(state == 1)
		g_object_set(button,  "current-state", (gint64)2, NULL);
	else
		g_object_set(button,  "current-state", (gint64)0, NULL);

	g_object_set(button, "reactive", TRUE, "show-tooltip", TRUE, NULL);
}

/* button state changed callback, if multi state */
static void state_changed(GObject *button, gint newState)
{
	g_print("MILDENHALL_BUTTON_DRAWER_TEST: new state = %d\n", newState);

}

int main (int argc, char **argv)
{
	g_autofree gchar *active_internet_icon_file = NULL;
	g_autofree gchar *active_artist_icon_file = NULL;
	g_autofree gchar *prop_file = NULL;

	ThornburyItemFactory *itemFactory;
	ClutterActor *stage;
        ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

	setlocale (LC_ALL, "");
	/* FIXME: This test relies on the de.po file in the same directory
	 *        to be compiled into ./de/LC_MESSAGES/test.mo
	 *        After test.mo is properly created, the commented-out lines
	 *        should be enabled.
	 *
	 * bindtextdomain ("test", ".");
	 * bind_textdomain_codeset ("test", "UTF-8");
	 * textdomain ("test");
	 */

	/* FIXME: Although the old I18N path was defined like the below,
	 *        they weren't actually existed.
	 *        This example should be revised with T3365.
	 *
	 * rAppData.pBaseFolderPath = g_strdup_printf(PKGDATADIR "/test-button-drawer/language-files");
	 * rAppData.pBaseFileName = "language";
	 */
	/* set language to german */
	setlocale (LC_MESSAGES, "de_DE");

        if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
                return -1;

        stage = clutter_stage_new ();
        clutter_actor_set_background_color (stage, &black);
        clutter_actor_set_size (stage, 728, 480);
	g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	prop_file = _mildenhall_get_resource_path ("mildenhall_button_drawer_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
	    MILDENHALL_TYPE_BUTTON_DRAWER, prop_file);
	GObject *pObject = NULL;

	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallButtonDrawer *drawer = MILDENHALL_BUTTON_DRAWER(pObject);
	ClutterActor *button = CLUTTER_ACTOR(drawer);

# if 0
	g_object_set(button, "tooltip-enabled", TRUE,
				"tooltip-width", 269.0,
				"tooltip-duration", 3000,
				"tooltip-direction", LIGHTWOOD_TOOLTIP_LEFT,
				"background-state", MILDENHALL_BUTTON_DRAWER_BG_OPEN,
				"reactive", FALSE,
				"x", 664.0, "y", 0.0,
				/*"width", 64.0, "height", 64.0,*/ NULL);

# endif
	/*g_object_set(button, "tooltip-enabled", TRUE,
				"tooltip-width", 269.0,
				"tooltip-duration", 3000,
				"background-state", MILDENHALL_BUTTON_DRAWER_BG_OPEN,
				"active", TRUE,
				"x", 628.0, "y", 380.0,
				"width", 100.0, "height", 100.0,
				NULL);*/

	clutter_actor_add_child(stage, button);
	ThornburyModel *model = create_model();
	g_object_set(button, "model", model, /*"views-drawer", TRUE,*/ NULL);

	g_signal_connect(button, "state-changed",  G_CALLBACK(state_changed), NULL);
	g_signal_connect(button, "tooltip-shown",  G_CALLBACK(tooltip_shown), NULL);
	g_signal_connect(button, "tooltip-hidden",  G_CALLBACK(tooltip_hidden), NULL);

	mildenhall_button_drawer_set_tooltip_width(MILDENHALL_BUTTON_DRAWER(button), 269.0);

	mildenhall_button_drawer_set_current_state(MILDENHALL_BUTTON_DRAWER(button), 0);

	clutter_actor_show (stage);

	//GValue value = {0};
	//g_value_init(&value, G_TYPE_STRING);
	//g_value_set_static_string (&value, "icon-title_tab_active.png");
	//thornbury_model_insert_value(model, 1, COLUMN_ICON, &value);

	active_internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
	    "test-button-drawer",
	    "icon_internet_AC.png",
	    NULL);

	active_artist_icon_file = g_build_filename (_mildenhall_get_theme_path (),
	    "test-button-drawer",
	    "icon_music_artists_AC.png",
	    NULL);


	thornbury_model_append (model, COLUMN_ICON_ACTIVE, active_internet_icon_file,
					//COLUMN_ICON_INACTIVE, "test-button-drawer/icon_internet_IN.png",
					COLUMN_TOOLTIP_TEXT, _("INTERNET"), -1);

	thornbury_model_append (model, COLUMN_ICON_ACTIVE, active_artist_icon_file,
					//COLUMN_ICON_INACTIVE, "test-button-drawer/icon_music_artists_IN.png",
					COLUMN_TOOLTIP_TEXT, _("ARTIST"), -1);


	//thornbury_model_remove (model, 2);
	mildenhall_button_drawer_set_tooltip_show(MILDENHALL_BUTTON_DRAWER(button), TRUE);

	//g_object_set(button, "multi-state", FALSE, NULL);
	clutter_main();
        return 0;

}

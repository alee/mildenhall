/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_meta_info_header
 * @title: MildenhallMetaInfoHeader
 * @short_description: #MildenhallMetaInfoHeader widget designed basically to
 * show organize header data (title info).
 * @see_also: #ThornburyItemFactory, #ClutterActor
 *
 * #MildenhallMetaInfoHeader widget designed to show title information.
 * Using this, an application can show an icon followed by the text in the
 * left-most side as well as right-most side.
 * It also supports to show  the text in the center using 'mid-text' property.
 * In addition, the signal "MildenhallMetaInfoHeader::icon-toggled" is emitted on
 * any of the icon click if it contains more than one state. (ie.e if the icon present
 * at the left/right side is a toggled button which contains DEFAULT/SELECTED states).
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * ThornburyModel *header_model = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 * ClutterActor *header = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 *       MILDENHALL_TYPE_META_INFO_HEADER,
 *       "/usr/share/mildenhall/mildenhall_meta_info_header_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * header = CLUTTER_ACTOR (object);
 *
 * header_model = (ThornburyModel*) thornbury_list_model_new (COLUMN_NONE,
 *                                                           G_TYPE_POINTER, NULL,
 *                                                           G_TYPE_STRING, NULL,
 *                                                           G_TYPE_STRING, NULL, -1);
 *
 * thornbury_model_append (header_model,
 *                        COLUMN_LEFT_ICON_TEXT, "/usr/var/lib/MILDENHALL_extensions/themes/blau/test-drawer-baseicon_music_AC.png",
 *                        COLUMN_MID_TEXT, "MUSIC DERFASD AWra wer Araerase",
 *                        COLUMN_RIGHT_ICON_TEXT, NULL,
 *                        -1);
 * g_object_set (header, "model", header_model, NULL);
 * clutter_actor_add_child (stage, header);
 *
 * ]|
 *
 * Since: 0.3.0
 *
 */


#include "mildenhall_meta_info_header.h"

/***********************************************************************************
        @Macro Definitions
 ************************************************************************************/
#define META_INFO_HEADER_DEBUG(...)   //g_print( __VA_ARGS__)

/* property enums */
enum _enHeaderProperty
{
	PROP_FIRST,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_X,
	PROP_Y,
	PROP_SHOW_BOTTOM,
	PROP_ICON_LEFT,
	PROP_ICON_RIGHT,
	PROP_MODEL,
	PROP_TOGGLE_ICON,
	PROP_LAST
};

/* The signals emitted by the mildenhall meta info header */
enum _enMetaInfoHeaderSignals
{
	SIG_FIRST,
	SIG_STATE_CHANGED,                       /* Emitted when state changes */
	SIG_LAST
};

/* header model columns */
enum
{
	COLUMN_LEFT_ICON_TEXT,
	COLUMN_MID_TEXT,
	COLUMN_RIGHT_ICON_TEXT,
	COLUMN_NONE
};

/* toggle button model columns */
enum
{
	COLUMN_TEXT,                // set text to toggle button
	COLUMN_ACTIVE_ICON_PATH,    // set active image to toggle button
	COLUMN_INACTIVE_ICON_PATH,  // set inactive image to toggle button
	COLUMN_STATE_ID,            // set stateId to toggle button
	COLUMN_LAST
};

static guint32 header_signals[SIG_LAST] = {0,};

typedef struct _MildenhallMetaInfoHeaderPrivate
{
	gfloat flWidth;                 // width of the meta-info-header
	gfloat flHeight;                // height of the meta-info-header
	gfloat flX;                 // x coord of the meta-info-header
	gfloat flY;                // y coord of the meta-info-header
	gfloat flImageWidth;
	gfloat flImageHeight;
	gfloat flIconX;
	gfloat flSepLineWidth;
	gfloat flSepLineHeight;
	gfloat flIconSepLineX;
	gfloat flTextSepLineX;
	gfloat flSepLineY;
	gfloat flMidTextOffsetX;
	gfloat flTextY;
	gfloat flBottomImageHeight;

	gboolean bIconLeft;
	gboolean bIconRight;
	gboolean bToggleIcon;
	ThornburyModel *pModel;
	gboolean bShowBottom;

	ClutterColor lineColor;
	ClutterColor depthLineColor;
	ClutterColor fontColor;
	ClutterActor *pBottomBar;
	ClutterActor *pUpperGroup;
	ClutterActor *pLeftButton;
	ClutterActor *pLeftText;
	ClutterActor *pMiddleText;
	ClutterActor *pLeftSep;

	gchar *pSeamlessFilePath;
	gchar *pSeamlessBottomFilePath;
	gchar *pShadowBottomFilePath;
	gchar *pFontName;
} MildenhallMetaInfoHeaderPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallMetaInfoHeader, mildenhall_meta_info_header, CLUTTER_TYPE_ACTOR)

/***************************************************************************************************************************
 * Internal Functions
 ***************************************************************************************************************************/
static void meta_info_header_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallMetaInfoHeader *pHeader);
static void meta_info_header_update_view (MildenhallMetaInfoHeader *pHeader);
static ClutterActor *meta_info_header_create_line(gfloat x, gfloat y, gfloat width, gfloat height, ClutterColor color);
static ClutterActor *meta_info_header_create_texture(const gchar *pImagePath, gfloat width, gfloat height, gfloat x, gfloat y);

/**
 * mildenhall_metainfo_header_set_model:
 * @MetaInfoHeader: meta info header object reference
 * @pModel: model for the meta info header
 *
 * Model format:
 *	"header-left-icon"  - GVariant( can be toggled)
 *      "header-left-text"  - gchar*
 *	"header-right-icon" - gchar*
 *	"header-right-text" - gchar*
 * 	"header-mid-text"   - gchar*
 **/
static void
mildenhall_metainfo_header_set_model (MildenhallMetaInfoHeader *pHeader, ThornburyModel *pModel)
{
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);
	if(NULL != priv->pModel)
	{
		g_signal_handlers_disconnect_by_func (priv->pModel,
				G_CALLBACK (meta_info_header_row_changed_cb),
				pHeader);
		g_object_unref (priv->pModel);

		priv->pModel = NULL;
	}
	/* update the new model with signals */
	if (pModel != NULL)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));

		priv->pModel = g_object_ref (pModel);

		g_signal_connect (priv->pModel,
				"row-changed",
				G_CALLBACK (meta_info_header_row_changed_cb),
				pHeader);
	}

	/* TBD: update view */
	meta_info_header_update_view(pHeader);

	g_object_notify (G_OBJECT(pHeader), "model");
}

/**
 * mildenhall_meta_info_header_new:
 * 
 * Returns: (transfer floating): a new #MildenhallMetaInfoHeader
 */
MildenhallMetaInfoHeader *
mildenhall_meta_info_header_new (void)
{
  return g_object_new (MILDENHALL_TYPE_META_INFO_HEADER, NULL);
}

/********************************************************
 * Function : meta_info_header_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void meta_info_header_get_property (GObject    *pObject, guint       uinPropertyID,  GValue     *pValue,                GParamSpec *pspec)
{
	MildenhallMetaInfoHeader *pHeader = MILDENHALL_META_INFO_HEADER (pObject);
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	switch (uinPropertyID)
	{
	case PROP_WIDTH:
		g_value_set_float (pValue, priv->flWidth);
		break;
	case PROP_HEIGHT:
		g_value_set_float (pValue, priv->flHeight);
		break;
	case PROP_X:
		g_value_set_float (pValue, priv->flX);
		break;
	case PROP_Y:
		g_value_set_float (pValue, priv->flY);
		break;
	case  PROP_SHOW_BOTTOM:
		g_value_set_boolean (pValue, priv->bShowBottom);
		break;
	case PROP_ICON_LEFT:
		g_value_set_boolean (pValue, priv->bIconLeft);
		break;
	case PROP_ICON_RIGHT:
		g_value_set_boolean (pValue, priv->bIconRight);
		break;
	case PROP_TOGGLE_ICON:
		g_value_set_boolean (pValue, priv->bToggleIcon);
		break;
	case PROP_MODEL:
		g_value_set_object (pValue, priv->pModel);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}


/********************************************************
 * Function : meta_info_header_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void meta_info_header_set_property (GObject      *pObject,
		guint         uinPropertyID,
		const GValue *pValue,
		GParamSpec   *pspec)
{
	MildenhallMetaInfoHeader *pHeader = MILDENHALL_META_INFO_HEADER (pObject);
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	switch (uinPropertyID)
	{
	case PROP_X:
		priv->flX = g_value_get_float (pValue);
		clutter_actor_set_x (CLUTTER_ACTOR (pHeader), priv->flX);
		break;
	case PROP_Y:
		priv->flY = g_value_get_float (pValue);
		clutter_actor_set_y (CLUTTER_ACTOR (pHeader), priv->flY);
		break;
	case  PROP_SHOW_BOTTOM:
		priv->bShowBottom = g_value_get_boolean (pValue);
		priv->bShowBottom ? 
			clutter_actor_show (priv->pBottomBar) : 	
			clutter_actor_hide (priv->pBottomBar);
		break;
	case PROP_ICON_LEFT:
		/* FIXME: What should we do for ICON_LEFT? */
		priv->bIconLeft = g_value_get_boolean (pValue);
		break;
	case PROP_ICON_RIGHT:
		/* FIXME: What should we do for ICON_RIGHT? */
		priv->bIconRight = g_value_get_boolean (pValue);
		break;
	case PROP_TOGGLE_ICON:
		priv->bToggleIcon = g_value_get_boolean (pValue);
		break;
	case PROP_MODEL:
		mildenhall_metainfo_header_set_model(pHeader, g_value_get_object (pValue));
		break;
	case PROP_WIDTH:
	case PROP_HEIGHT:
	     /* Read Only */
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}


/********************************************************
 * Function : meta_info_header_dispose
 * Description: Dispose the meta info header object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
meta_info_header_dispose (GObject *pObject)
{
	MildenhallMetaInfoHeader *pHeader = MILDENHALL_META_INFO_HEADER (pObject);
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	g_clear_pointer (&priv->pSeamlessFilePath, g_free);
	g_clear_pointer (&priv->pSeamlessBottomFilePath, g_free);
	g_clear_pointer (&priv->pShadowBottomFilePath, g_free);
	g_clear_pointer (&priv->pFontName, g_free);

	g_clear_object (&priv->pModel);

	G_OBJECT_CLASS (mildenhall_meta_info_header_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : meta_info_header_finalize
 * Description: Finalize the meta info heade object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
meta_info_header_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_meta_info_header_parent_class)->finalize (object);
}

/********************************************************
 * Function : meta_info_header_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void
mildenhall_meta_info_header_class_init (MildenhallMetaInfoHeaderClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	pObjectClass->get_property = meta_info_header_get_property;
	pObjectClass->set_property = meta_info_header_set_property;
	pObjectClass->dispose = meta_info_header_dispose;
	pObjectClass->finalize = meta_info_header_finalize;


	/**
	 * MetaInfoHeader:: icon-toggled:
	 * @mildenhallButtonDrawer: The object which received the signal
	 * @newState: index of new state
	 *
	 * ::icon-toggled is emitted on left icon click if it has more than one state(icon).
	 *
	 */
	header_signals[SIG_STATE_CHANGED] = g_signal_new ("icon-toggled",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallMetaInfoHeaderClass, icon_toggled),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 1, G_TYPE_STRING);


	/**
	 * MetaInfoHeader :x:
	 *
	 * x coord of the header
	 */
	pspec = g_param_spec_float ("x",
			"X",
			"x position of the header",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_X, pspec);

	/**
	 * MetaInfoHeader:y:
	 *
	 * y coord of the header
	 */
	pspec = g_param_spec_float ("y",
			"Y",
			"y position of the header",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, PROP_Y, pspec);

	/**
	 *MetaInfoHeader :width:
	 *
	 * Width of the header
	 */
	pspec = g_param_spec_float ("width",
			"Width",
			"Width of the header",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READABLE);
	g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

	/**
	 * MetaInfoHeader:height:
	 *
	 * Height of the header
	 */
	pspec = g_param_spec_float ("height",
			"Height",
			"Height of the header",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READABLE);
	g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	/**
	 * MetaInfoHeader:show-bottom:
	 *
	 * Set this property to FALSE to hide header bottom
	 *
	 * Default: TRUE
	 */
	pspec = g_param_spec_boolean ("show-bottom",
			"Show-Bottom",
			"Whether the bottom of header should be shown",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_SHOW_BOTTOM, pspec);

	/**
	 * MetaInfoHeader:left-icon:
	 *
	 * Set this property to FALSE to set text at left position
	 *
	 * Default: TRUE
	 */
	pspec = g_param_spec_boolean ("left-icon",
			"Left-Icon",
			"Whether the icon or text on left side of header",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_ICON_LEFT, pspec);

	/**
	 * MetaInfoHeader:right-icon:
	 *
	 * Set this property to FALSE to set text at right position
	 *
	 * Default: TRUE
	 */
	pspec = g_param_spec_boolean ("right-icon",
			"RIGHt-Icon",
			"Whether the icon or text on right side of header",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_ICON_RIGHT, pspec);

	/**
	 * MetaInfoHeader:toggle-icon:
	 *
	 * Set this property to TRUE to have toggle button for header else FALSE
	 *
	 * Default: FALSE
	 */
	pspec = g_param_spec_boolean ("toggle-icon",
			"toggle-icon",
			"Whether the have toggle function for header ",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_TOGGLE_ICON, pspec);

	/**
	 * MetaInfoHeader:model:
	 *
	 * model for the meta-info header.
	 * 1. left-icon
	 * 2. left-text
	 * 3. right-icon-text
	 * 4. middled-text
	 *
	 * Default: NULL
	 */
	pspec = g_param_spec_object ("model",
			"Model",
			"model having header data",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

}

/********************************************************
 * Function : meta_info_header_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void meta_info_header_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	MildenhallMetaInfoHeader *pHeader = pUserData;
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	gchar *pStyleKey = NULL;

	g_return_if_fail (MILDENHALL_IS_META_INFO_HEADER (pUserData));

	pStyleKey = g_strdup(pKey);

	if(g_strcmp0(pStyleKey, "seamless_top_image") == 0)
	{
		gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
			priv->pSeamlessFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
			META_INFO_HEADER_DEBUG("seamless_top_image = %s\n", priv->pSeamlessFilePath);
		}
	}
	else if(g_strcmp0(pStyleKey, "seamless_bottom_image") == 0)
	{
		gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
			priv->pSeamlessBottomFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
			META_INFO_HEADER_DEBUG("seamless_top_image = %s\n", priv->pSeamlessBottomFilePath);
		}
	}
	else if(g_strcmp0(pStyleKey, "shadow_bottom_image") == 0)
	{
		gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
			priv->pShadowBottomFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
			META_INFO_HEADER_DEBUG("seamless_top_image = %s\n", priv->pShadowBottomFilePath);
		}
	}
	else if(g_strcmp0(pStyleKey, "line-color") == 0)
	{
		clutter_color_from_string(&priv->lineColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "depth-line-color")== 0)
	{
		clutter_color_from_string(&priv->depthLineColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "image-width") == 0)
	{
		priv->flImageWidth = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("image-width = %f\n", priv->flImageWidth);
	}
	else if(g_strcmp0(pStyleKey, "image-height") == 0)
	{
		priv->flImageHeight = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("image-height = %f\n", priv->flImageHeight);
	}
	else if(g_strcmp0(pStyleKey, "icon-x") == 0)
	{
		priv->flIconX = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("icon-x = %f\n", priv->flIconX);
	}
	else if(g_strcmp0(pStyleKey, "separator-line-width") == 0)
	{
		priv->flSepLineWidth = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("separator-line-width = %f\n", priv->flSepLineWidth);

	}
	else if(g_strcmp0(pStyleKey, "separator-line-height") == 0)
	{
		priv->flSepLineHeight = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("separator-line-height = %f\n", priv->flSepLineHeight);
	}
	else if(g_strcmp0(pStyleKey, "icon-separator-line-x") == 0)
	{
		priv->flIconSepLineX = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("icon-separator-line-x = %f\n", priv->flIconSepLineX);
	}
	else if(g_strcmp0(pStyleKey, "text-separator-line-x") == 0)
	{
		priv->flTextSepLineX = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("text-separator-line-x = %f\n", priv->flTextSepLineX);
	}
	else if(g_strcmp0(pStyleKey, "separator-line-y") == 0)
	{
		priv->flSepLineY = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("separator-line-y = %f\n", priv->flSepLineY);
	}
	else if(g_strcmp0(pStyleKey, "mid-text-x-offset") == 0)
	{
		priv->flMidTextOffsetX = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("mid-text-x-offset = %f\n", priv->flMidTextOffsetX);
	}
	else if(g_strcmp0(pStyleKey, "text-y") == 0)
	{
		priv->flTextY = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("text-y = %f\n", priv->flTextY);
	}
	else if(g_strcmp0(pStyleKey, "bottom-image-height") == 0)
	{
		priv->flBottomImageHeight = g_value_get_double(pValue);
		META_INFO_HEADER_DEBUG("bottom-image-height = %f\n", priv->flBottomImageHeight);
	}
	else if(g_strcmp0(pStyleKey, "font-color") == 0)
	{
		clutter_color_from_string(&priv->fontColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "font-name") == 0)
	{
		g_free (priv->pFontName);
		priv->pFontName = g_value_dup_string (pValue);
		META_INFO_HEADER_DEBUG("font-name = %s\n", priv->pFontName);
	}

	g_free (pStyleKey);
}

/********************************************************
 * Function : toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
  MildenhallMetaInfoHeader *pHeader = pUserData;

  g_return_if_fail (MILDENHALL_IS_META_INFO_HEADER (pUserData));

  g_signal_emit (pHeader, header_signals[SIG_STATE_CHANGED], 0, newState);
}

/****************************************************
 * Function : meta_info_header_update_icon_left
 * Description: creates left icon as toggle button
 * Parameters: MetaInfoHeader*, GValue*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_icon_left(MildenhallMetaInfoHeader *pHeader, GValue *value)
{
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	gpointer ptr = g_value_get_pointer(value);

	if(priv->bToggleIcon == TRUE)
	{
		gpointer gPtr = g_value_get_pointer(value);
		GVariant *gvLeftIcon = (GVariant*)gPtr;

		if(gvLeftIcon)
		{
			ThornburyModel *pModel;
			GVariantIter inCount;
			gchar *pValue = NULL;
			gchar *pKey = NULL;
			ClutterActor *pLeftButton = NULL;

			g_variant_iter_init ( &inCount, gvLeftIcon);

			pModel = THORNBURY_MODEL (thornbury_list_model_new (COLUMN_LAST,
				G_TYPE_STRING, NULL,
				G_TYPE_STRING, NULL,
				G_TYPE_STRING, NULL,
				G_TYPE_POINTER, NULL,
				-1));

			while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
			{
				META_INFO_HEADER_DEBUG("pKey = %s pValue= %s \n",pKey,pValue);
				thornbury_model_append (pModel, COLUMN_TEXT, NULL,
						COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_STATE_ID, pKey,-1);
			}

			pLeftButton = g_object_new (MILDENHALL_TYPE_TOGGLE_BUTTON,
			                            "width", 58.0, "height", 64.0,
			                            "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
			                            "state-model", pModel,
			                            NULL);

			clutter_actor_add_child(priv->pUpperGroup, pLeftButton);
			g_signal_connect(pLeftButton, "button-toggled",  G_CALLBACK(toggle_button_cb), pHeader);
		}
		if(NULL == priv->pLeftSep)
		{
			/* create separator line */
			priv->pLeftSep = meta_info_header_create_line(64.0, 1.0, priv->flSepLineWidth, priv->flSepLineHeight, priv->lineColor);
			clutter_actor_add_child(priv->pUpperGroup, priv->pLeftSep);
		}

	}
	else
	{
		gchar *pLeftIcon =  g_strdup((gchar*)ptr);
		if(priv->bIconLeft && pLeftIcon)
		{
			if(NULL == priv->pLeftButton)
			{
				priv->pLeftButton = meta_info_header_create_texture((gchar*)ptr, 0, 0, priv->flIconX, priv->flTextY);
				if(priv->pLeftButton)
					clutter_actor_add_child(priv->pUpperGroup, priv->pLeftButton);
			}
			else
			{
				thornbury_ui_texture_set_from_file(priv->pLeftButton, pLeftIcon, 0, 0, FALSE, TRUE);
				g_free (pLeftIcon); 
			}

		}
		if(NULL == priv->pLeftSep)
		{
			/* create separator line */
			priv->pLeftSep = meta_info_header_create_line(priv->flIconSepLineX, priv->flSepLineY, priv->flSepLineWidth, priv->flSepLineHeight, priv->lineColor);
			clutter_actor_add_child(priv->pUpperGroup, priv->pLeftSep);
		}
	}

}

/****************************************************
 * Function : meta_info_header_update_icon_right
 * Description: creates right icon
 * Parameters: MetaInfoHeader*, GValue*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_icon_right(MildenhallMetaInfoHeader *pHeader, GValue *value)
{
	const gchar *pRightStr =  g_value_get_string(value);
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);
	if(priv->bIconRight && NULL != pRightStr)
	{
		/* set the right icon */
		META_INFO_HEADER_DEBUG("right icon path = %s\n", pRightStr);
	}

}

/****************************************************
 * Function : meta_info_header_update_mid_text
 * Description: creates middle text
 * Parameters: MetaInfoHeader*, GValue*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_mid_text(MildenhallMetaInfoHeader *pHeader, GValue *value)
{
	const gchar *pMidStr =  g_value_get_string(value);

	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);
	if(NULL == priv->pMiddleText)
	{
		priv->pMiddleText = clutter_text_new();
		g_object_set(priv->pMiddleText, "color", &priv->fontColor, NULL);
		clutter_actor_add_child(priv->pUpperGroup, priv->pMiddleText);
		clutter_text_set_font_name (CLUTTER_TEXT (priv->pMiddleText), priv->pFontName);
		clutter_actor_set_position(priv->pMiddleText, clutter_actor_get_x(priv->pLeftSep) + priv->flMidTextOffsetX, priv->flTextY ) ;
		clutter_actor_set_width(priv->pMiddleText, priv->flImageWidth - (clutter_actor_get_x(priv->pLeftSep) + (priv->flMidTextOffsetX * 2) ) );
		clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pMiddleText), PANGO_ELLIPSIZE_END);
	}
	/* default text if not provided by the user */
	g_object_set(priv->pMiddleText, "text", "...", NULL);
	if(NULL != pMidStr)
	{
		META_INFO_HEADER_DEBUG("%s\n", pMidStr);
		g_object_set(priv->pMiddleText, "text", pMidStr, NULL);
	}
}

/****************************************************
 * Function : meta_info_header_update_text_left
 * Description: creates left text
 * Parameters: MetaInfoHeader*, GValue*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_text_left(MildenhallMetaInfoHeader *pHeader, GValue *value)
{
	const gchar *pLeftStr =  g_value_get_string(value);

	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	if(NULL != priv->pLeftText)
	{
		clutter_actor_destroy(priv->pLeftText);
		priv->pLeftText = NULL;
	}
	if(NULL != pLeftStr)
	{
		gfloat flWidth;

		priv->pLeftText = clutter_text_new();
		g_object_set(priv->pLeftText, "y", priv->flTextY, "color", &priv->fontColor, NULL);
		clutter_text_set_font_name (CLUTTER_TEXT (priv->pLeftText), priv->pFontName);
		clutter_actor_add_child(priv->pUpperGroup, priv->pLeftText);
		clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pLeftText), PANGO_ELLIPSIZE_END);
		g_object_set(priv->pLeftText, "text", pLeftStr, NULL);

		/* restrict the width if exceeds */
		flWidth = priv->flTextSepLineX - (priv->flMidTextOffsetX * 2);
		if(clutter_actor_get_width(priv->pLeftText) > flWidth )
			clutter_actor_set_width(priv->pLeftText, flWidth);
		clutter_actor_set_x(priv->pLeftText, (priv->flTextSepLineX - clutter_actor_get_width(priv->pLeftText)) / 2);
	}
	/* create separator line */
	if(NULL == priv->pLeftSep)
	{
		priv->pLeftSep = meta_info_header_create_line(priv->flTextSepLineX, priv->flSepLineY, priv->flSepLineWidth, priv->flSepLineHeight, priv->lineColor);
		clutter_actor_add_child(priv->pUpperGroup, priv->pLeftSep);
	}
}

/****************************************************
 * Function : meta_info_header_update_text_right
 * Description: creates right text
 * Parameters: MetaInfoHeader*, GValue*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_text_right(MildenhallMetaInfoHeader *pHeader, GValue *value)
{
	const gchar *pRightStr =  g_value_get_string(value);
	if(NULL != pRightStr)
	{
		META_INFO_HEADER_DEBUG("%s\n", pRightStr);
		/* TBD: set the left text */

	}
}

/****************************************************
 * Function : meta_info_header_update_view
 * Description: update the header
 * Parameters: MetaInfoHeader*
 * Return value: None
 *******************************************************/
static void meta_info_header_update_view(MildenhallMetaInfoHeader *pHeader)
{
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	if (G_IS_OBJECT(priv->pModel) && thornbury_model_get_n_rows (priv->pModel) > 0)
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pModel, 0 );
		if(NULL != pIter)
		{
			GValue value = { 0, };

			/* get the first/second column value for left icon/text */
			thornbury_model_iter_get_value(pIter, COLUMN_LEFT_ICON_TEXT, &value);
			if(priv->bIconLeft)
			{
				meta_info_header_update_icon_left(pHeader, &value);
			}
			else
			{
				//g_value_unset (&value);
				//thornbury_model_iter_get_value(pIter, 1, &value);
				meta_info_header_update_text_left(pHeader, &value);
			}
			g_value_unset (&value);

			/* get the column value to get middle text */
			thornbury_model_iter_get_value(pIter, COLUMN_MID_TEXT, &value);
			meta_info_header_update_mid_text(pHeader, &value);
			g_value_unset (&value);

			/* get the column value to get right icon/text */
			thornbury_model_iter_get_value(pIter, COLUMN_RIGHT_ICON_TEXT, &value);
			if(priv->bIconRight)
				meta_info_header_update_icon_right(pHeader, &value);
			else
			{
				meta_info_header_update_text_right(pHeader, &value);
			}
			g_value_unset (&value);

		}

	}
}

/****************************************************
 * Function : meta_info_header_create_texture
 * Description: creates a texture actor
 * Parameters: gchar*, gfloat, gfloat, gfloat, gfloat
 * Return value: ClutterActor*
 *******************************************************/
static ClutterActor *meta_info_header_create_texture(const gchar *pImagePath, gfloat width, gfloat height, gfloat x, gfloat y)
{
	ClutterActor *pTexture = thornbury_ui_texture_create_new(pImagePath, width, height, FALSE, TRUE);

	if(pTexture)
		clutter_actor_set_position(pTexture, x, y);

	return pTexture;
}

/****************************************************
 * Function : meta_info_header_create_line
 * Description: creates a line actor
 * Parameters: gfloat, gfloat, gfloat, gfloat, ClutterColor
 * Return value: ClutterActor*
 *******************************************************/
static ClutterActor *meta_info_header_create_line(gfloat x, gfloat y, gfloat width, gfloat height, ClutterColor color)
{
	ClutterActor *pLine = NULL;

	/*Create a new line actor of given width and height*/
	pLine = clutter_actor_new();
	if(pLine)
	{
		clutter_actor_set_background_color(pLine, &color);
		clutter_actor_set_size(pLine, width, height);
		clutter_actor_set_position(pLine, x, y);
	}

	return pLine;
}

/****************************************************
 * Function : create_meta_bottombar
 * Description: Create the Bottom bar in the meta actor
 *              of the metainforoller object
 * Parameters: none
 * Return value: The Meta Bottom Bar actor
 *******************************************************/
static ClutterActor*mildenhall_meta_info_create_bottombar( MildenhallMetaInfoHeader *pHeader)
{
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (pHeader);

	ClutterActor *pTexture = NULL;
	ClutterActor *pLine = NULL;
	ClutterActor *pBottomShadow = NULL;

	/*Create the meta bottom bar group, set a seamless and all lines and the arrow*/
	ClutterActor *pBottomBar = clutter_actor_new();

	pTexture = meta_info_header_create_texture(priv->pSeamlessBottomFilePath, priv->flImageWidth, priv->flBottomImageHeight, 0, 0);
	clutter_actor_add_child(pBottomBar, pTexture);
	/* add top line */
	pLine = meta_info_header_create_line(0, 0,  priv->flImageWidth, priv->flSepLineWidth, priv->lineColor);
	clutter_actor_add_child(pBottomBar, pLine);
	/* add one more top line to match the drawer color */
	pLine = meta_info_header_create_line(0, 0,  priv->flImageWidth, priv->flSepLineWidth, priv->lineColor);
	clutter_actor_add_child(pBottomBar, pLine);

	pBottomShadow = thornbury_ui_texture_create_new (priv->pShadowBottomFilePath,
		 priv->flImageWidth, priv->flBottomImageHeight + 1,
		 FALSE, TRUE);

	if(pBottomShadow)
	{
		clutter_actor_add_child(pBottomBar, pBottomShadow);
		clutter_actor_set_position(pBottomShadow, 0, priv->flBottomImageHeight);
	}
	return pBottomBar;
}
/********************************************************
 * Function : meta_info_header_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void
mildenhall_meta_info_header_init (MildenhallMetaInfoHeader *self)
{
	MildenhallMetaInfoHeaderPrivate *priv = mildenhall_meta_info_header_get_instance_private (self);
	GHashTable *styleHash = NULL;

	ClutterActor *pSeamlessTexture = NULL;
	ClutterActor *pTopLine = NULL;

	ClutterActor *pBottomLine = NULL;
	ClutterActor *pBottomLine1 = NULL;

	priv->bShowBottom = TRUE;
	priv->flY = -1;
	priv->flX = -1;

	/* get the hash table for style properties */
	styleHash = thornbury_style_set(PKGDATADIR"/mildenhall_meta_info_header_style.json");

	/* pares the hash for styles */
	if(NULL != styleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter, styleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				g_hash_table_foreach(pHash, meta_info_header_parse_style, self);
			}
		}
		/* free the style hash */
		thornbury_style_free(styleHash);
	}

	priv->pUpperGroup = clutter_actor_new();

	pSeamlessTexture = meta_info_header_create_texture (priv->pSeamlessFilePath, priv->flImageWidth, priv->flImageHeight, 0, 0);
	pTopLine = meta_info_header_create_line (0, 0, priv->flImageWidth, priv->flSepLineWidth, priv->lineColor);

	pBottomLine = meta_info_header_create_line (0, priv->flImageHeight, priv->flImageWidth, priv->flSepLineWidth, priv->lineColor);
	pBottomLine1 = meta_info_header_create_line (0, priv->flImageHeight - 1, priv->flImageWidth, priv->flSepLineWidth, priv->depthLineColor);

	clutter_actor_add_child(CLUTTER_ACTOR(priv->pUpperGroup), pSeamlessTexture);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->pUpperGroup), pTopLine);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->pUpperGroup), pBottomLine);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->pUpperGroup), pBottomLine1);

	priv->pBottomBar = mildenhall_meta_info_create_bottombar(self);
	clutter_actor_set_position( priv->pBottomBar, 0, priv->flImageHeight);

	clutter_actor_add_child(CLUTTER_ACTOR(self), priv->pUpperGroup);
	clutter_actor_add_child(CLUTTER_ACTOR(self), priv->pBottomBar);

	META_INFO_HEADER_DEBUG("%f %f\n", clutter_actor_get_width(priv->pUpperGroup), clutter_actor_get_height(priv->pUpperGroup));
	META_INFO_HEADER_DEBUG("%f %f\n", clutter_actor_get_width(priv->pBottomBar), clutter_actor_get_height(CLUTTER_ACTOR(priv->pBottomBar)));

	
	priv->flWidth = clutter_actor_get_width (priv->pUpperGroup);
	clutter_actor_set_width (CLUTTER_ACTOR (self), priv->flWidth);

	priv->flHeight = 
		clutter_actor_get_height (priv->pUpperGroup) + clutter_actor_get_height (priv->pBottomBar);
	clutter_actor_set_height (CLUTTER_ACTOR (self), priv->flHeight);

	META_INFO_HEADER_DEBUG("%f %f\n", clutter_actor_get_width(CLUTTER_ACTOR(self)), clutter_actor_get_height(CLUTTER_ACTOR(self)));
}

/********************************************************
 * Function : meta_info_header_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MetaInfoHeader *
 * Return value: void
 ********************************************************/
static void
meta_info_header_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallMetaInfoHeader *pHeader)
{
	/* update the view */
	meta_info_header_update_view(pHeader);
}



/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_speller.h"

#include <thornbury/thornbury.h>

#include "mildenhall_toggle_button.h"

#define MILDENHALL_TEST_MULTI_LINE_SPELLER_PRINT(...) //g_print(__VA_ARGS__)

/* header model columns */
enum //for default entry
{
    MULTI_LINE_COLUMN_LEFT_IS_TEXT,
    MULTI_LINE_COLUMN_LEFT_WIDTH,
    MULTI_LINE_COLUMN_LEFT_ID,
    MULTI_LINE_COLUMN_LEFT_INFO,
    MULTI_LINE_COLUMN_RIGHT_IS_TEXT,
    MULTI_LINE_COLUMN_RIGHT_WIDTH,
    MULTI_LINE_COLUMN_RIGHT_ID,
    MULTI_LINE_COLUMN_RIGHT_INFO,
    MULTI_LINE_COLUMN_TEXT_BOX_DEFAULT_TEXT,
    MULTI_LINE_COLUMN_ENTRY_ID,
    MULTI_LINE_COLUMN_LAST
};

ClutterActor *pMultiLineSpeller = NULL;

gboolean mildenhall_multi_line_speller_go_cb(ClutterActor *actor, GVariant *text ,gpointer user_data)
{
    GVariantIter iter;
    gchar *pText = NULL;
    gchar *key = NULL;
    g_variant_iter_init (&iter, text);
    while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
    {
        g_print(" ###### %s %s #####\n",key,pText);
    }
    mildenhall_speller_hide(actor,TRUE);
    return TRUE;
}


static gboolean multi_line_speller_shown_cb (ClutterActor *actor,  gpointer user_data)
{
    MILDENHALL_TEST_MULTI_LINE_SPELLER_PRINT("%s",__FUNCTION__);
	return FALSE;
}

static gboolean multi_line_speller_hidden_cb (ClutterActor *actor,  gpointer user_data)
{
    MILDENHALL_TEST_MULTI_LINE_SPELLER_PRINT("%s",__FUNCTION__);
    ThornburyModel *model=NULL;
    g_object_get(pMultiLineSpeller,"model",&model,NULL);
    //g_object_set( actor, "clear-entry", TRUE, NULL);
    GValue value = { 0, };
	g_value_init(&value, G_TYPE_STRING);
    g_value_set_string(&value, g_strdup("dummy"));
    thornbury_model_insert_value(model, 0, MULTI_LINE_COLUMN_TEXT_BOX_DEFAULT_TEXT, &value);
	return FALSE;
}

/* creation of model */
ThornburyModel *multi_line_create_speller_model()
{
    /* model for default entry */
    ThornburyModel *model = (ThornburyModel *)thornbury_list_model_new (  MULTI_LINE_COLUMN_LAST,
                                    G_TYPE_BOOLEAN ,0,
                                    G_TYPE_FLOAT , 0,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_POINTER, NULL,
                                    G_TYPE_BOOLEAN ,0,
                                    G_TYPE_FLOAT , 0,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_POINTER, NULL,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_STRING, NULL,
                                    -1);
    return model;
}

int main (int argc, char **argv)
{
    g_autofree gchar *pImage = NULL;
    g_autofree gchar *overlay_img_file = NULL;
    g_autofree gchar *prop_file = NULL;

    ClutterActor *overlay;
    ThornburyItemFactory *itemFactory;

    int clInErr = clutter_init(&argc, &argv);
    if (clInErr != CLUTTER_INIT_SUCCESS)
        return -1;

    ClutterActor *stage;
    stage = clutter_stage_new ();

    pImage = g_build_filename (_mildenhall_get_theme_path (),
        "test-drawer-base",
        "background.png",
        NULL);
    ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
    clutter_actor_add_child(stage, actor);

    clutter_actor_set_size (stage, 728, 480);
    g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

    overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
        "content_overlay.png",
        NULL);
    overlay = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
    clutter_actor_add_child(stage, overlay);

    prop_file = _mildenhall_get_resource_path ("mildenhall_multi_line_speller_prop.json");
    itemFactory = thornbury_item_factory_generate_widget_with_props(
         MILDENHALL_TYPE_SPELLER, prop_file);
    GObject *pObject = NULL;
    g_object_get(itemFactory, "object", &pObject, NULL);
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pObject);
    pMultiLineSpeller = CLUTTER_ACTOR(pSelf);
    clutter_actor_add_child(stage, pMultiLineSpeller);
    clutter_actor_set_position(pMultiLineSpeller, 0, 459);

    ThornburyModel *pMultilineSpellerModel = multi_line_create_speller_model();

    GVariantBuilder *pRowValues = NULL;
    pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
    g_variant_builder_add(pRowValues,  "{ss}", "artist", "1" );

    GVariantBuilder *pRowValues1 = NULL;
    pRowValues1 = g_variant_builder_new( G_VARIANT_TYPE_ARRAY);
    g_variant_builder_add(pRowValues1, "{ss}", "album",  "2" );

    GVariant *p = g_variant_builder_end(pRowValues);
    GVariant *q = g_variant_builder_end(pRowValues1);

        /* append model info for default entry */
    thornbury_model_append ( pMultilineSpellerModel,
                         MULTI_LINE_COLUMN_LEFT_IS_TEXT ,TRUE,
                         MULTI_LINE_COLUMN_LEFT_WIDTH ,58.0,
                         MULTI_LINE_COLUMN_LEFT_ID ,g_strdup("BUTTON1"),
                         MULTI_LINE_COLUMN_LEFT_INFO, (gpointer)p,
                         MULTI_LINE_COLUMN_RIGHT_IS_TEXT ,TRUE,
                         MULTI_LINE_COLUMN_RIGHT_WIDTH ,58.0,
                         MULTI_LINE_COLUMN_RIGHT_ID ,g_strdup("BUTTON2"),
                         MULTI_LINE_COLUMN_RIGHT_INFO, (gpointer)q,
                         MULTI_LINE_COLUMN_TEXT_BOX_DEFAULT_TEXT, "",
                         MULTI_LINE_COLUMN_ENTRY_ID ,g_strdup("entry"),
                         -1);

    g_object_set(pMultiLineSpeller,"model", pMultilineSpellerModel, NULL);

    g_signal_connect(pMultiLineSpeller, "go-pressed",G_CALLBACK(mildenhall_multi_line_speller_go_cb), NULL);
	g_signal_connect_after (pMultiLineSpeller, "speller-shown", G_CALLBACK (multi_line_speller_shown_cb), NULL);
	g_signal_connect_after (pMultiLineSpeller, "speller-hidden", G_CALLBACK (multi_line_speller_hidden_cb), pMultilineSpellerModel);

    clutter_actor_show (stage);
    clutter_main();
    return 0;
}

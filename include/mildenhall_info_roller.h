/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-info-roller.h */

#ifndef _MILDENHALL_INFO_ROLLER_H
#define _MILDENHALL_INFO_ROLLER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>

#include "mildenhall_roller_container.h"
#include "mildenhall_toggle_button.h"
#include "mildenhall_meta_info_header.h"


G_BEGIN_DECLS

#define MILDENHALL_TYPE_INFO_ROLLER mildenhall_info_roller_get_type()

#define MILDENHALL_INFO_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_INFO_ROLLER, MildenhallInfoRoller))

#define MILDENHALL_INFO_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_INFO_ROLLER, MildenhallInfoRollerClass))

#define MILDENHALL_IS_INFO_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_INFO_ROLLER))

#define MILDENHALL_IS_INFO_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_INFO_ROLLER))

#define MILDENHALL_INFO_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_INFO_ROLLER, MildenhallInfoRollerClass))

typedef struct _MildenhallInfoRoller MildenhallInfoRoller;
typedef struct _MildenhallInfoRollerClass MildenhallInfoRollerClass;
typedef struct _MildenhallInfoRollerPrivate MildenhallInfoRollerPrivate;

/**
 * MildenhallInfoRoller:
 * 
 * The #MildenhallInfoRoller struct contains only private data.
 *
 */
struct _MildenhallInfoRoller
{
	ClutterActor parent;

	MildenhallInfoRollerPrivate *priv;
};

/**
 * MildenhallInfoRollerClass:
 * @info_roller_animated: class handler for the #MildenhallPullupRoller::info_roller_animated signal
 * @info_roller_up_animation_started: class handler for the #MildenhallPullupRoller::info_roller_up_animation_started signal 
 * @info_roller_down_animation_started: class handler for the #MildenhallPullupRoller::info_roller_down_animation_started signal
 * @info_roller_item_selected: class handler for the #MildenhallPullupRoller::info_roller_item_selected signal     
 *
 * The #MildenhallInfoRollerClass struct contains only private data.
 *
 */

struct _MildenhallInfoRollerClass
{
	ClutterActorClass parent_class;
	
	void (* info_roller_animated) (MildenhallInfoRoller *pInfoRoller);
        void (* info_roller_up_animation_started) (MildenhallInfoRoller *pInfoRoller);
        void (* info_roller_down_animation_started) (MildenhallInfoRoller *pInfoRoller);
        void (* info_roller_item_selected) (MildenhallInfoRoller *pInfoRoller, guint uinRow);
	
};

/* header model columns */
/**
 * MildenhallInfoRollerHeaderModel:
 * These are enumerations which denotes merely a numbers. Here the type given in front of the enum is the type of model columns.
 *
 * @MILDENHALL_INFO_ROLLER_COLUMN_LEFT_ICON_TEXT: G_TYPE_STRING ,Iconpath/text in string
 * @MILDENHALL_INFO_ROLLER_COLUMN_MID_TEXT: G_TYPE_STRING ,Mid Text.
 * @MILDENHALL_INFO_ROLLER_COLUMN_RIGHT_ICON_TEXT: G_TYPE_STRING ,iconpath/text at the right side.
 *
 * @COLUMN_NONE denotes total number of columns present in the Model.
 *              ---------------------------------------------------------------------------------------------------------
 *              | INFO_ROLLER_COLUMN_LEFT_ICON_TEXT	|INFO_ROLLER_COLUMN_MID_TEXT    |INFO_ROLLER_COLUMN_RIGHT_ICON_TEXT   |
 *              | (G_TYPE_STRING)(col 0)        	|(G_TYPE_STRING)(col 1)         |(G_TYPE_STRING)(col n)|
 *              |---------------------------------------|-------------------------------|-----------------------|
 *ROWS->        |---------------------------------------|-------------------------------|-----------------------|
 *              |---------------------------------------|-------------------------------|-----------------------|
 *              |---------------------------------------|-------------------------------|-----------------------|
 *              |---------------------------------------|-------------------------------|-----------------------|
 *
 * Model has to be created using these enumerations.
 * Example:
 * ThornburyModel * model = thornbury_list_model_new(INFO_ROLLER_COLUMN_NONE,
 *                                              G_TYPE_STRING,NULL,
 *                                              G_TYPE_STRING,NULL,
 *                                              G_TYPE_STRING,NULL,
 *                                              NULL);
 *
 *      thornbury_model_append (model,
 *                        INFO_ROLLER_COLUMN_LEFT_ICON_TEXT,"/icon/path/sometext",
 *                       INFO_ROLLER_COLUMN_MID_TEXT,"SomeText",
 *                       INFO_ROLLER_COLUMN_RIGHT_ICON_TEXT,"/icon/path/sometext",
 *                      -1);
 **/
typedef enum
{
  MILDENHALL_INFO_ROLLER_COLUMN_LEFT_ICON_TEXT,
  MILDENHALL_INFO_ROLLER_COLUMN_MID_TEXT,
  MILDENHALL_INFO_ROLLER_COLUMN_RIGHT_ICON_TEXT,
  MILDENHALL_INFO_ROLLER_COLUMN_NONE
} MildenhallInfoRollerHeaderModel;

GType mildenhall_info_roller_get_type (void) G_GNUC_CONST;

MildenhallInfoRoller *mildenhall_info_roller_new (void);

gboolean mildenhall_info_roller_get_header_icon_right(MildenhallInfoRoller *pInfoRoller);
gboolean mildenhall_info_roller_get_header_icon_left(MildenhallInfoRoller *pInfoRoller);
ThornburyModel *mildenhall_info_roller_get_header_model(MildenhallInfoRoller *pInfoRoller);
ThornburyModel *mildenhall_info_roller_get_model(MildenhallInfoRoller *pInfoRoller);
GType mildenhall_info_roller_get_item_type(MildenhallInfoRoller *pInfoRoller);
gboolean mildenhall_info_roller_get_fix_roller(MildenhallInfoRoller *pInfoRoller);
gfloat mildenhall_info_roller_get_width(MildenhallInfoRoller *pInfoRoller);
gfloat mildenhall_info_roller_get_height(MildenhallInfoRoller *pInfoRoller);
gboolean mildenhall_info_roller_get_show(MildenhallInfoRoller *pInfoRoller);
gboolean mildenhall_info_roller_get_list_roller(MildenhallInfoRoller *pInfoRoller);

void mildenhall_info_roller_set_header_model(MildenhallInfoRoller *infoRoller, ThornburyModel *pModel);
void mildenhall_info_roller_set_header_icon_right(MildenhallInfoRoller *pInfoRoller, gboolean bIconRight);
void mildenhall_info_roller_set_header_icon_left(MildenhallInfoRoller *pInfoRoller, gboolean bIconLeft);
void mildenhall_info_roller_set_fix_roller(MildenhallInfoRoller *pInfoRoller, gboolean bFixedRoller);
void mildenhall_info_roller_set_model(MildenhallInfoRoller *pInfoRoller, ThornburyModel *model);
void mildenhall_info_roller_set_item_type(MildenhallInfoRoller *pInfoRoller, GType gType);
void mildenhall_info_roller_set_show(MildenhallInfoRoller *pInfoRoller, gboolean bShow);
void mildenhall_info_roller_set_list_roller(MildenhallInfoRoller *pInfoRoller, gboolean bListRoller);	

void mildenhall_info_roller_add_attribute(MildenhallInfoRoller *pInfoRoller, const gchar *pProperty, gint inColumn);

G_END_DECLS

#endif /* _MILDENHALL_INFO_ROLLER_H */

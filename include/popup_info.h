/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* popup-info.h */

#ifndef __POPUP_INFO_H__
#define __POPUP_INFO_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include "liblightwood-popupbase.h"

G_BEGIN_DECLS

#define MMD_POPUP_BUTTON_TXT_CORRECTION_FACTOR      8


#define POPUP_PATH                  PKGTHEMEDIR "/"
#define MMD_REQUEST_ICON            POPUP_PATH "icon_request_inactive.png"
#define MMD_APP_ICON                MMD_REQUEST_ICON
#define MMD_INFO_ICON               POPUP_PATH "icon_request_info.png"
#define MMD_ERROR_ICON              POPUP_PATH "icon_request_error.png"
#define MMD_POPUP_BUTTON1_TEXT_X    161
#define MMD_POPUP_BUTTON1_TEXT_Y    152


#define MMD_POPUP_BUTTON2_TEXT_X    486
#define MMD_POPUP_BUTTON2_TEXT_Y    152

#define MMD_SINGLE_BUTTON_WIDTH     648



#define MAX_NUM_POPUP_BUTTON        2
#define MMD_POPUP_SHOWTIME          750

#define MMD_POPUP_SHOW_X            79
#define MMD_POPUP_SHOW_Y            286
#define MMD_POPUP_HIDE_X            79
#define MMD_POPUP_HIDE_Y            458

#define MMD_POPUP_TEXT_LETTER_NUM   44
#define MMD_POPUP_MAX_TEXT_LEN      88

#define MMD_POPUP_TXT_LINE_MID_X    324
#define MMD_POPUP_TXT_LINE1_MID_Y   50
#define MMD_POPUP_TXT_LINE2_MID_Y   86
#define MMD_POPUP_TXT_LINE_MID_Y    68

#define POPUP_TYPE_INFO popup_info_get_type()

#define POPUP_INFO(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  POPUP_TYPE_INFO, PopupInfo))

#define POPUP_INFO_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  POPUP_TYPE_INFO, PopupInfoClass))

#define POPUP_IS_INFO(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  POPUP_TYPE_INFO))

#define POPUP_IS_INFO_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  POPUP_TYPE_INFO))

#define POPUP_INFO_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  POPUP_TYPE_INFO, PopupInfoClass))

typedef struct _PopupInfo PopupInfo;
typedef struct _PopupInfoClass PopupInfoClass;
typedef struct _PopupInfoPrivate PopupInfoPrivate;

struct _PopupInfo
{
  LightwoodPopupBase parent;

  PopupInfoPrivate *priv;
};

struct _PopupInfoClass
{
  LightwoodPopupBaseClass parent_class;
};

GType popup_info_get_type (void) G_GNUC_CONST;


void popup_info_show(gpointer data);
void popup_info_hide(gpointer data);
void
popup_info_parse_style (gpointer pKey, gpointer pValue, gpointer pUserData);


G_END_DECLS

#endif /* __POPUP_INFO_H__ */

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* map_widget_operations.c
 *
 * Created on: Dec 11, 2012
 *
 * map_widget_operations.c */

#include "map_widget_internal.h"

#include <math.h>
#include <thornbury/thornbury.h>

static gboolean MapWidgetIsSecondTap = FALSE;
static guint uinMapWidgetDoubleTapTimer;

/******************************************************************************
 * FUNCTION		: b_map_widget_double_tap_timer_clear
 * PARAMETER		: gpointer
 * RETURN VALUE	: gboolean
 * DESCRIPTION	: Timer call back function to clear double tap flag
 *
 *******************************************************************************/
static gboolean b_map_widget_double_tap_timer_clear(gpointer pUserData)
{

    MapWidgetIsSecondTap = FALSE;
    uinMapWidgetDoubleTapTimer = 0;

    return FALSE;
}




gboolean b_map_widget_map_touch_clb(ClutterActor *pActor,
                                       ClutterButtonEvent *pEvent, gpointer pUserData)
{

    static float flPrevX = 0, flPrevY = 0, flRelX, flRelY;
    MapWidget *pSelf = MAP_WIDGET(pUserData);

    gint zoom_level;

    g_object_get (MAP_WIDGET (pSelf), "zoom-level", &zoom_level,
                 NULL);
     if(pSelf->priv->touchendpoints!=1)
     return FALSE;


    clutter_actor_transform_stage_point(pActor, pEvent->x, pEvent->y, &flRelX,
                                        &flRelY);
    pEvent->x = flRelX;
    pEvent->y = flRelY;

    if (MapWidgetIsSecondTap)
    {
        MAP_WIDGET_DEBUG("\n  MapWidgetIsSecondTap is TRUE  \n");

        if ((abs((pEvent->x) - (flPrevX))) < MAP_WIDGET_MAX_NEAR_DISTANCE)
        {
            if ((abs((pEvent->y) - (flPrevY))) < MAP_WIDGET_MAX_NEAR_DISTANCE)
            {
                                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             ++zoom_level, NULL);
            }
            else if ((abs((pEvent->y) - (flPrevY)))
                     < MAP_WIDGET_MAX_FAR_DISTANCE)
            {

                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             --zoom_level, NULL);
            }
        }
        else if ((abs((pEvent->x) - (flPrevX))) < MAP_WIDGET_MAX_FAR_DISTANCE)
        {
            if ((abs((pEvent->y) - (flPrevY))) < MAP_WIDGET_MAX_FAR_DISTANCE)
            {

                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             --zoom_level, NULL);
            }
        }
        MapWidgetIsSecondTap = FALSE;
    }
    else
    {
        MAP_WIDGET_DEBUG("\n  MapWidgetIsSecondTap is FALSE  \n");
        MapWidgetIsSecondTap = TRUE;

        if (uinMapWidgetDoubleTapTimer != 0)
            g_source_remove(uinMapWidgetDoubleTapTimer);
        uinMapWidgetDoubleTapTimer = g_timeout_add(
                                         MAP_WIDGET_MAX_TIME_FOR_SECOND_TAP,
                                         b_map_widget_double_tap_timer_clear, NULL);
    }
    flPrevX = pEvent->x;
    flPrevY = pEvent->y;
    return TRUE;
}





gboolean b_map_widget_map_selected_touch_clb(ClutterActor *pActor,
		ClutterButtonEvent *pEvent, gpointer pUserData)
{
	MapWidget *pSelf = MAP_WIDGET(pUserData);

	switch (pEvent->type)
		{
 case CLUTTER_TOUCH_BEGIN:
              pSelf->priv->touchendpoints++;
              break;
	case CLUTTER_TOUCH_END:
	      {
          b_map_widget_map_touch_clb(pActor,pEvent,pUserData);
                pSelf->priv->touchendpoints--;
                if(pSelf->priv->touchendpoints < 0)
                 pSelf->priv->touchendpoints=0;
                break;
                }
	default:
		   return FALSE;
		}
	return TRUE;
}


gboolean b_map_widget_marker_selected_touch_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData)
{

	switch (pEvent->type)
		{
	case CLUTTER_TOUCH_END:
		b_map_widget_marker_selected_cb(pActor,pEvent,pUserData);
                 break;
	default:
		   return FALSE;
		}
	return TRUE;

}



gboolean b_map_widget_path_marker_selected_touch_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData)
{

	switch (pEvent->type)
		{
	case CLUTTER_TOUCH_END:
		b_map_widget_path_marker_selected_cb(pActor,pEvent,pUserData);
	default:
		   return FALSE;
		}
	return TRUE;

}


/******************************************************************************
 * FUNCTION		: b_map_widget_marker_selected_cb
 * PARAMETER	: ClutterActor *,ClutterButtonEvent *,gpointer
 * RETURN VALUE	: gboolean
 * DESCRIPTION	: this function is called when ever marker is selected
 *
 *******************************************************************************/
gboolean b_map_widget_marker_selected_cb(ClutterActor *pActor,
        ClutterButtonEvent *pEvent, gpointer pUserData)
{
    MapWidget *pSelf = MAP_WIDGET(pUserData);
    ClutterActor *pMarker = NULL;
    GPtrArray *glMarkerslist = p_map_widget_get_markers_list(pSelf);

    pSelf->priv->poi_touch_event=TRUE;
    GValue ViewPointType = { 0, };
    gint inLoopcounter=0;
    GList *indexarray=NULL;

    while(inLoopcounter < thornbury_model_get_n_rows(pSelf->priv->pModel))
    {
        ThornburyModelIter *Iter = thornbury_model_get_iter_at_row(pSelf->priv->pModel,
                               inLoopcounter);
        thornbury_model_iter_get_value(Iter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);
        if (g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_POI || g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CUSTOM)
        {
            indexarray=g_list_append(indexarray,GINT_TO_POINTER(inLoopcounter));

        }

        inLoopcounter++;
    }
    if(pSelf->priv->selectedMarkerIndex !=-1 )
    {
        GValue pTempValue = { 0 };
        g_value_init(&pTempValue, G_TYPE_BOOLEAN);
        g_value_set_boolean(&pTempValue, FALSE);
        thornbury_model_insert_value(pSelf->priv->pModel,  GPOINTER_TO_INT(g_list_nth_data(indexarray,pSelf->priv->selectedMarkerIndex)), COLUMN_HIGHLIGHT_MARKER,
                                 &pTempValue);
    }

    inLoopcounter = 0;

    while (inLoopcounter < glMarkerslist->len)
    {
        MAP_WIDGET_DEBUG("\n inLoopCounter is %d\n ", inLoopcounter);
        pMarker = CLUTTER_ACTOR(
                      g_ptr_array_index(glMarkerslist, inLoopcounter));

       if (pMarker == pActor)
        {
            MAP_WIDGET_DEBUG("\n Marker found in list\n");

            break;
        }
        inLoopcounter++;

    }

    GValue pTempValue = { 0 };
    g_value_init(&pTempValue, G_TYPE_BOOLEAN);
    g_value_set_boolean(&pTempValue, TRUE);
    thornbury_model_insert_value(pSelf->priv->pModel,  GPOINTER_TO_INT(g_list_nth_data(indexarray,inLoopcounter)), COLUMN_HIGHLIGHT_MARKER,
                             &pTempValue);
     if(indexarray !=NULL)
     {
          g_list_free(indexarray);
          indexarray=NULL;

     }


    g_signal_emit_by_name(pSelf, "marker-selected",
                          clutter_actor_get_name(pActor));

    return TRUE;

}


gboolean b_map_widget_path_marker_selected_cb(ClutterActor *pActor,ClutterButtonEvent *pEvent, gpointer pUserData)
{
    MapWidget *pSelf = MAP_WIDGET(pUserData);
    ClutterActor *pMarker = NULL;
    GPtrArray *pPathMarkerslist = p_map_widget_get_path_markers_list(pSelf);

    GValue ViewPointType = { 0, };
    pSelf->priv->path_touch_event=TRUE;
    gint inLoopcounter=0;
    GList *pathindexarray=NULL;

    while(inLoopcounter < thornbury_model_get_n_rows(pSelf->priv->pModel))
    {
        ThornburyModelIter *Iter = thornbury_model_get_iter_at_row(pSelf->priv->pModel,
                               inLoopcounter);
        thornbury_model_iter_get_value(Iter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);
        if (g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CURRENT || g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_DESTINATION)
        {
            pathindexarray=g_list_append(pathindexarray,GINT_TO_POINTER(inLoopcounter));

        }

        inLoopcounter++;
    }


    if(pSelf->priv->selectedPathMarkerIndex !=-1 )
    {
        GValue pTempValue = { 0 };
        g_value_init(&pTempValue, G_TYPE_BOOLEAN);
        g_value_set_boolean(&pTempValue, FALSE);
        thornbury_model_insert_value(pSelf->priv->pModel,  GPOINTER_TO_INT(g_list_nth_data(pathindexarray,pSelf->priv->selectedPathMarkerIndex)), COLUMN_HIGHLIGHT_MARKER,
                                 &pTempValue);
    }

    inLoopcounter = 0;

    while (inLoopcounter < pPathMarkerslist->len)
    {
        MAP_WIDGET_DEBUG("\n inLoopCounter is %d\n ", inLoopcounter);
        pMarker = CLUTTER_ACTOR(
                      g_ptr_array_index(pPathMarkerslist, inLoopcounter));
        MAP_WIDGET_DEBUG("\n marker name is %s   %s  %d\n",
                         clutter_actor_get_name(pActor), clutter_actor_get_name(pMarker),
                         inLoopcounter);
        if (pMarker == pActor)
        {
            MAP_WIDGET_DEBUG("\n Marker found in list\n");

            break;
        }
        inLoopcounter++;

    }

    GValue pTempValue = { 0 };
    g_value_init(&pTempValue, G_TYPE_BOOLEAN);
    g_value_set_boolean(&pTempValue, TRUE);
    thornbury_model_insert_value(pSelf->priv->pModel,  GPOINTER_TO_INT(g_list_nth_data(pathindexarray,inLoopcounter)), COLUMN_HIGHLIGHT_MARKER,
                             &pTempValue);

     if(pathindexarray !=NULL)
     {
          g_list_free(pathindexarray);
          pathindexarray=NULL;

     }


    g_signal_emit_by_name(pSelf, "marker-selected",
                          clutter_actor_get_name(pActor));
    return TRUE;


}



/******************************************************************************
 * FUNCTION		: b_map_widget_map_selected_clb
 * PARAMETER		: ClutterActor *,ClutterButtonEvent *,gpointer
 * RETURN VALUE	: gboolean
 * DESCRIPTION	: this function handles all touch events on map widget
 *
 *******************************************************************************/

gboolean b_map_widget_map_selected_clb(ClutterActor *pActor,
                                       ClutterButtonEvent *pEvent, gpointer pUserData)
{

    static float flPrevX = 0, flPrevY = 0, flRelX, flRelY;
    MapWidget *pSelf = MAP_WIDGET(pUserData);

    gint zoom_level = 0;


    g_object_get (MAP_WIDGET (pSelf), "zoom-level", &zoom_level,
                 NULL);



    if ((pEvent->button != 1) ||(pEvent->click_count > 1) )
        return FALSE;
    clutter_actor_transform_stage_point(pActor, pEvent->x, pEvent->y, &flRelX,
                                        &flRelY);
    pEvent->x = flRelX;
    pEvent->y = flRelY;

    if (MapWidgetIsSecondTap)
    {

               if ((abs((pEvent->x) - (flPrevX))) < MAP_WIDGET_MAX_NEAR_DISTANCE)
        {
            if ((abs((pEvent->y) - (flPrevY))) < MAP_WIDGET_MAX_NEAR_DISTANCE)
            {

                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             ++zoom_level, NULL);
            }
            else if ((abs((pEvent->y) - (flPrevY)))
                     < MAP_WIDGET_MAX_FAR_DISTANCE)
            {

                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             --zoom_level, NULL);
            }
        }
        else if ((abs((pEvent->x) - (flPrevX))) < MAP_WIDGET_MAX_FAR_DISTANCE)
        {
            if ((abs((pEvent->y) - (flPrevY))) < MAP_WIDGET_MAX_FAR_DISTANCE)
            {


                g_object_set (MAP_WIDGET (pSelf), "zoom-level",
                             --zoom_level, NULL);
            }
        }
        MapWidgetIsSecondTap = FALSE;
    }
    else
    {
        MAP_WIDGET_DEBUG("\n  MapWidgetIsSecondTap is FALSE  \n");
        MapWidgetIsSecondTap = TRUE;

        if (uinMapWidgetDoubleTapTimer != 0)
            g_source_remove(uinMapWidgetDoubleTapTimer);
        uinMapWidgetDoubleTapTimer = g_timeout_add(
                                         MAP_WIDGET_MAX_TIME_FOR_SECOND_TAP,
                                         b_map_widget_double_tap_timer_clear, NULL);
    }
    flPrevX = pEvent->x;
    flPrevY = pEvent->y;
    return TRUE;
}


static void v_map_widget_extract_current_destination(MapWidget *pSelf,ThornburyModelIter *pIter,Imagedata *pExImageInfo,ClutterActor *pMarker)
{
	ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);
	GPtrArray *pPathMarkerslist = p_map_widget_get_path_markers_list(pSelf);
	GPtrArray *pPathImagelist = p_map_widget_get_path_image_list(pSelf);

	 GValue Latitude = { 0, };
	 GValue Longitude = { 0, };
	 GValue HighlightMarker = { 0, };
	 thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
	 thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
	 thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
	 	                                        &HighlightMarker);
	 if (pMarker != NULL)
	            	                {
	            	                    MAP_WIDGET_DEBUG("\n champlain_path_layer_add_node \n");
	            	                    g_signal_connect(pMarker, "button-release-event",
	            	                                     G_CALLBACK(b_map_widget_path_marker_selected_cb), pSelf);
	            	                    g_signal_connect(pMarker, "touch-event",
	            	                                                         G_CALLBACK(b_map_widget_path_marker_selected_touch_cb), pSelf);
	            	                    g_ptr_array_add(pPathMarkerslist, (gpointer) pMarker);
	            	                    g_ptr_array_add(pPathImagelist, (gpointer) pExImageInfo);
	            	                    v_map_widget_draw_path(pSelf,pPathLayerActor,0.0);
	            	                    champlain_marker_layer_add_marker(
	            	                        CHAMPLAIN_MARKER_LAYER(pSelf->priv->pPosDestinationLayer),
	            	                        CHAMPLAIN_MARKER(pMarker));


	            	                    if (G_VALUE_HOLDS_BOOLEAN(&HighlightMarker) && (TRUE == g_value_get_boolean(&HighlightMarker)))
	            	                    {
	            	                            pSelf->priv->path_highlight_marker_counter++;

	            	                            if(pSelf->priv->path_highlight_marker_counter > 1)
	            	                            {
	            	                                g_warning("you cannot highlight more than one marker");
	            	                                pSelf->priv->path_highlight_marker_counter--;
	            	                                return;
	            	                            }

	            	                            else
	            	                            {
	            	                                champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker),
	            	                                                              TRUE);
	            	                                pSelf->priv->selectedPathMarkerIndex=pPathMarkerslist->len-1;
	            	                                champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
	            	                                champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
	            	                                                          CLUTTER_ACTOR(
	            	                                                              thornbury_ui_texture_create_new(
	            	                                                                  pExImageInfo->selectedImage, 0, 0,
	            	                                                                  FALSE, FALSE)));

	            	                                    v_map_widget_move_to_location(pSelf,g_value_get_float(&Latitude),g_value_get_float(&Longitude),20);
	            	                                }



	            	                    }

	            	                }
}

static void v_map_widget_extract_route_type(MapWidget *pSelf,ThornburyModelIter *pIter)
{
	ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);
	 GValue Latitude = { 0, };
	 GValue Longitude = { 0, };
	 thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
	 thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
	champlain_path_layer_add_node(
	            	                    CHAMPLAIN_PATH_LAYER(pPathLayerActor),
	            	                    CHAMPLAIN_LOCATION(champlain_coordinate_new_full (g_value_get_float(&Latitude), g_value_get_float(&Longitude))));
}

static void v_map_widget_extract_poi(MapWidget *pSelf,ThornburyModelIter *pIter,Imagedata *pExImageInfo,ClutterActor *pMarker)
{
	ClutterActor *pLayerActor = p_map_widget_get_layer_actor(pSelf);
	GPtrArray *glMarkerslist = p_map_widget_get_markers_list(pSelf);
	GPtrArray *glImagelist = p_map_widget_get_image_list(pSelf);
	 GValue Latitude = { 0, };
	 GValue Longitude = { 0, };
	 GValue HighlightMarker = { 0, };
	 thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
	 thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
	 thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
	                                        &HighlightMarker);
	g_signal_connect(pMarker, "button-release-event",
	            	                                 G_CALLBACK(b_map_widget_marker_selected_cb), pSelf);
	            	                g_signal_connect(pMarker, "touch-event",
	            	                                                 G_CALLBACK(b_map_widget_marker_selected_touch_cb), pSelf);
	            	                champlain_marker_layer_add_marker(
	            	                    CHAMPLAIN_MARKER_LAYER(pLayerActor),
	            	                    CHAMPLAIN_MARKER(pMarker));
	            	                g_ptr_array_add(glMarkerslist, (gpointer) pMarker);
	            	                g_ptr_array_add(glImagelist, (gpointer) pExImageInfo);

	            	                if (G_VALUE_HOLDS_BOOLEAN(&HighlightMarker) && (TRUE == g_value_get_boolean(&HighlightMarker)))
	            	                {

	            	                        MAP_WIDGET_DEBUG("\n HIGHLIGHT MARKER IS TRUE    \n");
	            	                        pSelf->priv->highlight_marker_counter++;

	            	                        if(pSelf->priv->highlight_marker_counter > 1)
	            	                        {
	            	                            g_warning("you cannot highlight more than one marker");
	            	                            pSelf->priv->highlight_marker_counter--;
	            	                            return;
	            	                        }

	            	                        else
	            	                        {


	            	                            champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker),
	            	                                                          TRUE);
	            	                            pSelf->priv->selectedMarkerIndex=glMarkerslist->len-1;
	            	                            champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
	            	                            champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
	            	                                                      CLUTTER_ACTOR(
	            	                                                          thornbury_ui_texture_create_new(
	            	                                                              pExImageInfo->selectedImage, 0, 0,
	            	                                                              FALSE, FALSE)));
	            	                        }



	            	                }

}

static void v_map_widget_set_marker_properties(MapWidget *pSelf,ClutterActor *pMarker,ThornburyModelIter *pIter)
{
	  ClutterColor *color = p_map_widget_return_normal_colour(pSelf);
	  ClutterColor *SelectedColor = p_map_widget_return_selected_colour(pSelf);
	  GValue Latitude = { 0, };
	  GValue Longitude = { 0, };
	  GValue MarkerName = { 0, };
	 	 thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
	 	 thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
	 	thornbury_model_iter_get_value(pIter, COLUMN_MARKER_NAME, &MarkerName);
	 if (pMarker != NULL)
	            {
	                champlain_label_set_color(CHAMPLAIN_LABEL(pMarker), color);
	                champlain_location_set_location(CHAMPLAIN_LOCATION(pMarker),
	                                                g_value_get_float(&Latitude),
	                                                g_value_get_float(&Longitude));


	                champlain_marker_set_selection_color(SelectedColor);

	                if (G_VALUE_HOLDS_STRING(&MarkerName)
	                        && (NULL != g_value_get_string(&MarkerName)))
	                {
	                    MAP_WIDGET_DEBUG("\n marker name is %s \n",
	                            g_value_get_string(&MarkerName));
	                    clutter_actor_set_name(pMarker,
	                                           g_value_get_string(&MarkerName));
	                }
	                MAP_WIDGET_DEBUG("\n pMarker NOT NULL \n");
	                clutter_actor_set_reactive(pMarker, TRUE);
	            }
}


/******************************************************************************
 * FUNCTION		: v_extract_model_to_add_view_points
 * PARAMETER		: MapWidget *
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to extract the model information feeded by app developer to add view points on map widget
 *
 *******************************************************************************/

void v_extract_model_to_add_view_points(MapWidget *pSelf)
{
    ThornburyModel *pModel = p_map_widget_get_model(pSelf);

    ClutterActor *pLayerActor = p_map_widget_get_layer_actor(pSelf);
    ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);
    ClutterActor *pMarker = NULL;
    gint inRows = thornbury_model_get_n_rows(pModel);
    gint inLoopCounter = 0;

    gchar *pNormalImage=NULL;
    gchar *pSelectedImage=NULL;
    GHashTable *pPrivHash=p_map_widget_get_private_hash(pSelf);
    if ((G_IS_OBJECT(pModel)) && (inRows > 0))
    {
        MAP_WIDGET_DEBUG("\n G_IS_OBJECT(pModel) ) && (inRows > 0) \n");
        GValue Latitude = { 0, };
        GValue Longitude = { 0, };
        GValue MarkerName = { 0, };
        GValue NormalImage = { 0, };
        GValue SelectedImage = { 0, };
        GValue HighlightMarker = { 0, };

        GValue ViewPointType = { 0, };

        while (inLoopCounter < inRows)
        {
            Imagedata *pExImageInfo = g_new0(Imagedata, 1);

            MAP_WIDGET_DEBUG("\ninLoopCounter < inRows \n");
            ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pModel,
                                    inLoopCounter);
            thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
            thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
            thornbury_model_iter_get_value(pIter, COLUMN_MARKER_NAME, &MarkerName);

            thornbury_model_iter_get_value(pIter, COLUMN_NORMAL_IMAGE,
                                       &NormalImage);
            thornbury_model_iter_get_value(pIter, COLUMN_SELECTED_IMAGE,
                                       &SelectedImage);
            thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
                                       &HighlightMarker);
            thornbury_model_iter_get_value(pIter, COLUMN_VIEW_POINT_TYPE,
                                       &ViewPointType);



            switch(g_value_get_int(&ViewPointType))
            {
            case MAP_WIDGET_VPTYPE_CURRENT:
            {
            	pSelf->priv->current_type_counter++;

            	                if(pSelf->priv->current_type_counter > 1)
            	                {
            	                    g_warning("current position cannot be more than one");
            	                    return;
            	                }

            	                else
            	                {
            	                    MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_CURRENT \n");
            	                    pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"current-normal-image"), NULL);
            	                    pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"current-selected-image"), NULL);
            	                    pMarker=champlain_label_new_from_file (pNormalImage,NULL);
            	                    pExImageInfo->normalImage = pNormalImage;
            	                    pExImageInfo->selectedImage = pSelectedImage;

            	                }
            	                break;
            }

            case MAP_WIDGET_VPTYPE_DESTINATION:
            {
            	pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"destination-normal-image"), NULL);
            	                pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"destination-selected-image"), NULL);
            	                MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_DESTINATION \n");
            	                pMarker=champlain_label_new_from_file (pNormalImage,NULL);
            	                pExImageInfo->normalImage = pNormalImage;
            	                pExImageInfo->selectedImage = pSelectedImage;
            	                break;
            }

            case MAP_WIDGET_VPTYPE_POI:
                        {
                        	 MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_POI \n");
                        	                pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"poi-normal-image"), NULL);
                        	                pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"poi-selected-image"), NULL);

                        	                pMarker=champlain_label_new_from_file (pNormalImage,NULL);
                        	                pExImageInfo->normalImage = pNormalImage;
                        	                pExImageInfo->selectedImage = pSelectedImage;
                        	                break;
                        }

            case MAP_WIDGET_VPTYPE_CUSTOM:
                                   {
                                	   MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_CUSTOM \n");
                                	                  if((G_VALUE_HOLDS_STRING(&NormalImage) && (NULL != g_value_get_string(&NormalImage)))&&(G_VALUE_HOLDS_STRING(&SelectedImage) && (NULL != g_value_get_string(&SelectedImage))))
                                	                     {

                                	                      pMarker=champlain_label_new_from_file (g_value_get_string(&NormalImage), NULL);
                                	                      pExImageInfo->normalImage = g_strdup(g_value_get_string(&NormalImage));
                                	                      pExImageInfo->selectedImage = g_strdup(g_value_get_string(&SelectedImage));
                                	                     }
                                	                  else
                                	                  {
                                	                      pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"poi-normal-image"), NULL);
                                	                      pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pPrivHash,"poi-selected-image"), NULL);
                                	                      pMarker=champlain_label_new_from_file (pNormalImage,NULL);
                                	                      pExImageInfo->normalImage = pNormalImage;
                                	                      pExImageInfo->selectedImage = pSelectedImage;

                                	                  }
                                	                  break;
                                   }

            default:break;

            }

         v_map_widget_set_marker_properties(pSelf,pMarker,pIter);

            switch(g_value_get_int(&ViewPointType))
            {
            case MAP_WIDGET_VPTYPE_CURRENT:
            case MAP_WIDGET_VPTYPE_DESTINATION:
            {
            	v_map_widget_extract_current_destination(pSelf,pIter,pExImageInfo,pMarker);
            	break;
            }

            case MAP_WIDGET_VPTYPE_ROUTE:
            {
            	v_map_widget_extract_route_type(pSelf,pIter);
            	break;
            }

            default:
            {
            	v_map_widget_extract_poi(pSelf,pIter,pExImageInfo,pMarker);
            }

            }
            inLoopCounter++;

        }
        clutter_actor_show(pLayerActor);
        clutter_actor_show(pPathLayerActor);
       v_map_widget_draw_path(pSelf,pPathLayerActor,0.0);
    }

}

/******************************************************************************
 * FUNCTION		: v_map_widget_draw_path
 * PARAMETER		: MapWidget *,ClutterActor *
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to draw path on map widget
 *
 *******************************************************************************/

void v_map_widget_draw_path(MapWidget *pSelf, ClutterActor *pLayer,gfloat width)
{

    if (!MAP_IS_WIDGET(pSelf))
        return;

    ClutterColor *color = p_map_widget_return_path_colour(pSelf);
    champlain_path_layer_set_stroke_width(CHAMPLAIN_PATH_LAYER(pLayer),width);
    champlain_path_layer_set_stroke_color(CHAMPLAIN_PATH_LAYER(pLayer), color);

}

/******************************************************************************
 * FUNCTION		: v_map_widget_move_to_location
 * PARAMETER		: MapWidget *,gfloat,gfloat,gint
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to move to specific location  on map widget
 *
 *******************************************************************************/

void v_map_widget_move_to_location(MapWidget *pSelf, gfloat latitude,
                                   gfloat longitude, gint numSteps)
{
    ClutterActor *pViewMap = p_map_widget_get_map(pSelf);
    gfloat curLatitude = 0.0, curLongitude = 0.0;
    gfloat latitudeDiff = 0.0, longitudeDiff = 0.0;
    ChamplainView *Map = CHAMPLAIN_VIEW(pViewMap);
    gint *zoom_level = g_new0 (gint, 1);
    g_object_get (pSelf, "zoom-level", zoom_level, NULL);


    curLatitude = champlain_view_get_center_latitude(Map);
    curLongitude = champlain_view_get_center_longitude(Map);

    latitudeDiff =
        (curLatitude > latitude) ?
        (curLatitude - latitude) : (latitude - curLatitude);
    longitudeDiff =
        (curLongitude > longitude) ?
        (curLongitude - longitude) : (longitude - curLongitude);

    if (latitudeDiff > longitudeDiff)
    {
        gint count = 0;
        gint steps = (int) (latitudeDiff), stepSize = 1;
        //printf("steps:%d \n",steps);
        if (steps > numSteps)
        {
            stepSize = steps / numSteps;
            steps = numSteps;

        }
        if (latitudeDiff > 0.1)
        {

            for (count = 0; count < steps; count++)
            {

                if (curLatitude > latitude)
                {
                    curLatitude = curLatitude - stepSize;

                    map_widget_go_to_location(CLUTTER_ACTOR(pSelf), curLatitude,
                                              curLongitude);

                }
                else
                {

                    curLatitude = curLatitude + stepSize;

                    map_widget_go_to_location(CLUTTER_ACTOR(pSelf), curLatitude,
                                              curLongitude);

                }

            }
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), latitude,
                                      longitude);

        }
        else
        {
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), latitude,
                                      curLongitude);
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), latitude,
                                      longitude);
        }

    }
    else
    {
        gint count = 0;
        gint steps = (int) (longitudeDiff), stepSize = 1;

        if (steps > numSteps)
        {
            stepSize = steps / numSteps;
            steps = numSteps;

        }

        if (longitudeDiff > 0.1)
        {

            for (count = 0; count < steps; count++)
            {

                if (curLongitude > longitude)
                {
                    curLongitude = (curLongitude - stepSize);

                    map_widget_go_to_location(CLUTTER_ACTOR(pSelf), curLatitude,
                                              curLongitude);

                }
                else
                {
                    curLongitude = (curLongitude + stepSize);

                    map_widget_go_to_location(CLUTTER_ACTOR(pSelf), curLatitude,
                                              curLongitude);

                }

            }
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), latitude,
                                      longitude);

        }
        else
        {
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), curLatitude,
                                      longitude);
            map_widget_go_to_location(CLUTTER_ACTOR(pSelf), latitude,
                                      longitude);

        }
    }



}


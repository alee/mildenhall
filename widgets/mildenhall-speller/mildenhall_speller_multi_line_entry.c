/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_speller_multi_line_entry
 * @title: MildenhallSpellerMultiLineEntry
 * @short_description: Text entry where several lines of text can be edited at once.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #MildenhallSpeller
 *
 * #MildenhallSpellerDefaultEntry is an another text entry widget for speller
 * where the several lines of text can be edited at once.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * Since: 0.3.0
 */

#include "mildenhall_speller_multi_line_entry.h"
#include "mildenhall_speller.h"
#include "mildenhall_text_box_entry.h"

G_DEFINE_TYPE (MildenhallSpellerMultiLineEntry, mildenhall_speller_multi_line_entry, CLUTTER_TYPE_ACTOR)

#define SPELLER_MULTI_LINE_ENTRY_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, MildenhallSpellerMultiLineEntryPrivate))

#define MILDENHALL_MULTI_LINE_SPELLER_PRINT(...) //g_print(__VA_ARGS__)
#define MILDENHALL_BLANK_RECT_WIDTH  648.0
#define MILDENHALL_BLANK_RECT_HEIGHT 64.0

struct _MildenhallSpellerMultiLineEntryPrivate
{
	ThornburyModel *model;
	ThornburyModel *pRightModel;
	ThornburyModel *pLeftModel;
	ThornburyModel *pTextBoxModel;

	ClutterActor *blankRect;
	GVariant *argList;
	ClutterActor *textBoxEntry;
	ClutterActor *leftButton;
	ClutterActor *rightButton;
	ClutterActor *multilineSpellerGroup;
	ClutterActor *leftLine;
	ClutterActor *rightLine;
	gboolean clearText;
	gchar *toggleLeftState;
	gchar *toggleRightState;
	gchar *toggleLeftId;
	gchar *toggleRightId;
	gchar *entryId;
	gint inColumnId;
};

/* toggle button model columns */
enum
{
	MILDENHALL_MULTI_LINE_COLUMN_TEXT,                // set text to toggle button
	MILDENHALL_MULTI_LINE_COLUMN_ACTIVE_ICON_PATH,    // set active image to toggle button
	MILDENHALL_MULTI_LINE_COLUMN_INACTIVE_ICON_PATH,  // set inactive image to toggle button
	MILDENHALL_MULTI_LINE_COLUMN_STATE_ID,            // set stateId to toggle button
	MILDENHALL_MULTI_LINE_COLUMN_LAST
};

enum _MildenhallMultiLineSpellerProperties
{
	MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_FIRST,
	MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_TEXT,
	MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_CLEAR_TEXT,
	MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_MODEL,
	MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_LAST
};

/****************************************************
 * Function : p_multi_line_create_text_box_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
p_multi_line_create_text_box_model (void)
{
	ThornburyModel *pModel = NULL;
	pModel = (ThornburyModel *)thornbury_list_model_new (1 ,G_TYPE_STRING, NULL ,-1);
	return pModel;
}

/****************************************************
 * Function : p_multi_line_create_toggle_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
p_multi_line_create_toggle_model (void)
{
	ThornburyModel *model = NULL;
	model = (ThornburyModel *)thornbury_list_model_new (MILDENHALL_MULTI_LINE_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			-1);
	return model;
}

/********************************************************
 * Function : v_multi_line_left_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_multi_line_left_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY(pUserData);
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE(pSelf);
	priv->toggleLeftState = g_strdup(newState);
}

/********************************************************
 * Function : v_multi_line_right_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_multi_line_right_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY(pUserData);
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE(pSelf);
	priv->toggleRightState = g_strdup(newState);
}

/********************************************************
 * Function : v_scroller_status_cb
 * Description: scroller status cb function
 * Parameters: GObject * ,scrollerState
 * Return value: void
 ********************************************************/
static void v_scroller_status_cb(GObject *pObject, gboolean scrollerState ,gpointer pUserData)
{
	MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY(pUserData);
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE(pSelf);
	if(scrollerState == TRUE)
	{
		clutter_actor_hide(priv->rightButton);
	}
	else
	{
		clutter_actor_show(priv->rightButton);
	}
}

/********************************************************
 * Function : v_multi_line_speller_row_added_cb
 * Description: callback on model row added
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultiLineEntry *
 * Return value: void
 ********************************************************/
static void v_multi_line_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerMultiLineEntry *pSelf)
{
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
}

/********************************************************
 * Function : v_multi_line_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultiLineEntry *
 * Return value: void
 ********************************************************/
static void v_multi_line_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallSpellerMultiLineEntry *pSelf)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
        GValue value = {0, };
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	priv->model = pModel;

	if(-1 != priv->inColumnId)
	{
		thornbury_model_iter_get_value(pIter, priv->inColumnId, &value);
		thornbury_model_insert_value(priv->pTextBoxModel, 1, 0, &value);
		g_value_unset (&value);
	}
}

/********************************************************
 * Function : v_multi_line_speller_row_removed_cb
 * Description: callback on model row remove
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerMultiLineEntry *
 * Return value: void
 ********************************************************/
static void v_multi_line_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerMultiLineEntry *pSelf)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
        gint inRemovedRow;
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);

	/* this callback gives the actual row count before removal, and not after removal of row */
	inRemovedRow = thornbury_model_iter_get_row (pIter);

	/* if there is only one state, don't remove it */
	if(inRemovedRow ==  1 && thornbury_model_get_n_rows(priv->model) == 1)
	{
		g_warning("There is only one state, can not remove the state\n");
		return;
	}
}

/****************************************************
 * Function : update_left_button_model
 * Description: function to update the button model
 * Parameters: MildenhallSpellerMultiLineEntry *,GVariant *,gboolean
 * Return value: void
 *******************************************************/
static void update_left_button_model(MildenhallSpellerMultiLineEntry *pSelf,GVariant *gvLeftIcon,gboolean bIsText)
{
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE( pSelf );
	if(gvLeftIcon)
	{
		GVariantIter inCount;
		gchar *pValue = NULL;
		gchar *pKey = NULL;
		int inCnt =0;
                g_variant_iter_init (&inCount, gvLeftIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			MILDENHALL_MULTI_LINE_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
			if( TRUE == bIsText  )
			{
				g_object_set (priv->leftButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (priv->pLeftModel, MILDENHALL_MULTI_LINE_COLUMN_TEXT, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_ACTIVE_ICON_PATH, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_INACTIVE_ICON_PATH, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				thornbury_model_append (priv->pLeftModel, MILDENHALL_MULTI_LINE_COLUMN_TEXT, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			if(0 == inCnt)
			{
				priv->toggleLeftState =g_strdup(pKey);
				inCnt++;
			}
		}
		g_object_set(priv->leftButton, "state-model", priv->pLeftModel, NULL);
	}
}

/****************************************************
 * Function : update_right_button_model
 * Description: function to update the button model
 * Parameters: MildenhallSpellerMultiLineEntry *,GVariant *,gboolean
 * Return value: void
 *******************************************************/
static void update_right_button_model(MildenhallSpellerMultiLineEntry *pSelf,GVariant *gvRightIcon,gboolean bIsText1)
{
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE( pSelf );
	if(gvRightIcon)
	{
		GVariantIter inCount;
		gchar *pValue = NULL;
		gchar *pKey = NULL;
		int inCnt =0;
                g_variant_iter_init (&inCount, gvRightIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			MILDENHALL_MULTI_LINE_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
			if( TRUE == bIsText1  )
			{
				g_object_set (priv->rightButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (priv->pRightModel, MILDENHALL_MULTI_LINE_COLUMN_TEXT, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_ACTIVE_ICON_PATH, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_INACTIVE_ICON_PATH, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				thornbury_model_append (priv->pRightModel, MILDENHALL_MULTI_LINE_COLUMN_TEXT, NULL,
						MILDENHALL_MULTI_LINE_COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						MILDENHALL_MULTI_LINE_COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			if(0 == inCnt)
			{
				priv->toggleRightState =g_strdup(pKey);
				inCnt++;
			}
		}
		g_object_set(priv->rightButton, "state-model", priv->pRightModel, NULL);
	}
}

/****************************************************
 * Function : v_model_data_for_multi_line_entry
 * Description: update the speller entry with model info
 * Parameters: MildenhallSpellerMultiLineEntry*
 * Return value: void
 *******************************************************/
static void v_model_data_for_multi_line_entry( MildenhallSpellerMultiLineEntry *pSelf )
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);

	if (G_IS_OBJECT(priv->model) && thornbury_model_get_n_rows ( priv->model) > 0 )
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->model, 0 );
		if(NULL != pIter)
		{
			GValue value = {0, };
                        gboolean bIsText;
                        gfloat fltWidth;
                        gpointer gPtr;
                        GVariant *gvLeftIcon;
                        gboolean bIsText1;
                        gfloat fltWidth1;
                        gpointer gPtr1;
                        GVariant *gvRightIcon = NULL;
			thornbury_model_iter_get_value(pIter, 0, &value);
			bIsText = g_value_get_boolean (&value);
			MILDENHALL_MULTI_LINE_SPELLER_PRINT(" bIsText = %d \n",bIsText);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 1, &value);
			fltWidth = g_value_get_float (&value);
			MILDENHALL_MULTI_LINE_SPELLER_PRINT(" fltWidth = %f \n",fltWidth);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 2, &value);
			priv->toggleLeftId = g_value_dup_string(&value);
			g_value_unset (&value);

			g_object_set(priv->leftButton ,"width",fltWidth,NULL);

			thornbury_model_iter_get_value(pIter, 3, &value);
			gPtr = g_value_get_pointer (&value);
			gvLeftIcon = (GVariant*) gPtr;

			update_left_button_model(pSelf,gvLeftIcon,bIsText);
			g_value_unset (&value);

			clutter_actor_set_position( priv->leftButton, 0,0);
			clutter_actor_set_position( priv->leftLine,fltWidth+1,0);

			thornbury_model_iter_get_value(pIter, 4, &value);
			bIsText1 = g_value_get_boolean (&value);
			MILDENHALL_MULTI_LINE_SPELLER_PRINT(" bIsText1 = %d \n",bIsText1);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 5, &value);
			fltWidth1 = g_value_get_float (&value);
			MILDENHALL_MULTI_LINE_SPELLER_PRINT(" fltWidth1 = %f \n",fltWidth1);
			g_value_unset (&value);

			g_object_set(priv->rightButton ,"width",fltWidth1,NULL);
			clutter_actor_set_x( priv->textBoxEntry,4+fltWidth );
			g_object_set(priv->textBoxEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(fltWidth+fltWidth1+4),NULL);

			thornbury_model_iter_get_value(pIter, 6, &value);
			priv->toggleRightId = g_value_dup_string(&value);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 7, &value);
			gPtr1 = g_value_get_pointer (&value);
			gvRightIcon = (GVariant*) gPtr1;

			update_right_button_model(pSelf,gvRightIcon,bIsText1);
			g_value_unset (&value);

			clutter_actor_set_position( priv->rightButton,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1,0);
			clutter_actor_set_position( priv->rightLine,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1-1,0);

			thornbury_model_iter_get_value(pIter, 8, &value);
			thornbury_model_insert_value(priv->pTextBoxModel, 1, 0, &value);
			g_object_set(priv->textBoxEntry,"model",priv->pTextBoxModel,NULL);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 9, &value);
			priv->entryId = g_value_dup_string(&value);
			g_value_unset (&value);
		}
	}
}


/****************************************************
 * Function : v_multi_line_speller_update_view
 * Description: update the speller entry
 * Parameters: MildenhallSpellerMultiLineEntry*
 * Return value: None
 *******************************************************/
static void v_multi_line_speller_update_view( MildenhallSpellerMultiLineEntry *pSelf )
{
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	v_model_data_for_multi_line_entry(pSelf);
}

/**
 * v_multi_line_speller_set_text:
 * @pSelf : object reference
 * @pArgList : arg list of type #GVariant
 *
 * set the text on TextEntryBox
 *
 */
void v_multi_line_speller_set_text(MildenhallSpellerMultiLineEntry *pSelf, GVariant *pArgList)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
        GVariantIter iter;
        gchar *pText = NULL;
        gchar *key = NULL;
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(NULL == pArgList)
		return;

	g_variant_iter_init (&iter, pArgList);
	while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
	{
		if(!g_strcmp0(pText,"SPACE"))
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value," ");
			thornbury_model_insert_value(priv->pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		else if(!g_strcmp0(pText,"SHIFT"))
		{
			//TBD
		}
		else if(!g_strcmp0(pText,"RETURN") || !g_strcmp0(pText,"go") )
		{
			GVariantBuilder *pVariant = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
			gchar *child_pText = NULL;
			g_object_get (priv->textBoxEntry, "text", &child_pText, NULL);
			g_variant_builder_add (pVariant, "{ss}", priv->entryId, g_strdup (child_pText));
			g_variant_builder_add (pVariant, "{ss}",priv->toggleLeftId, priv->toggleLeftState);
			g_variant_builder_add (pVariant, "{ss}",priv->toggleRightId, priv->toggleRightState);
			priv->argList = g_variant_builder_end (pVariant);
			g_variant_builder_unref(pVariant);
		}
		else if(!g_strcmp0(pText,"dot"))
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value,".");
			thornbury_model_insert_value(priv->pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		else if(!g_strcmp0(pText,"CANCEL"))
		{
			lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (priv->textBoxEntry));
		}
		else if(!g_strcmp0(pText,"SYMBOLS") || !g_strcmp0(pText,"SHIFT") || !g_strcmp0(pText,"AUTOCOMPLETE"))
		{
			//TBD
		}
		else
		{
			GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value,g_strdup(pText));
			thornbury_model_insert_value(priv->pTextBoxModel, 1, 0, &value);
			g_value_unset (&value);
		}
		if(pText!=NULL)
		{
			g_free (pText);
			pText = NULL;
		}
		if(key!=NULL)
		{
			g_free (key);
			key = NULL;
		}
	}
}

/**
 * v_multi_line_speller_set_clear_text:
 * @pSelf : object reference
 * @clearText : set to true/false to clear the text
 *
 * set to clear the text on entry
 *
 */
void v_multi_line_speller_set_clear_text(MildenhallSpellerMultiLineEntry *pSelf, gboolean clearText)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT :%s clearText = %d \n", __FUNCTION__,clearText);
	if(!MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	priv->clearText = clearText;
	if( TRUE == priv->clearText)
	{
		lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (priv->textBoxEntry));
		priv->clearText = FALSE;
	}
}

void v_multi_line_speller_column_changed_cb(gint col,gpointer pSelf)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->inColumnId = col;
}

/**
 * v_multi_line_speller_set_model:
 * @pSelf: meta info header object reference
 * @pModel: model for the speller
 *
 * Model format:
 *	"left-icon-istext"  - gboolean
 *	"left-icon-width"   - gfloat
 *	"left-icon-info"    - GVariant*
 *	"right-icon-istext" - gboolean
 *	"right-icon-width"  - gfloat
 *	"right-icon-info"   - GVariant*
 * 	"default-text"     - gchar*
 **/

void v_multi_line_speller_set_model(MildenhallSpellerMultiLineEntry *pSelf, ThornburyModel *pModel)
{
        MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(NULL != priv->model)
	{
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multi_line_speller_row_added_cb),   pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multi_line_speller_row_changed_cb), pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_multi_line_speller_row_removed_cb), pSelf);
		//g_object_unref (priv->model);
		thornbury_list_model_destroy(priv->model);
		priv->model = NULL;
	}
	/* update the new model with signals */
	if ( NULL != pModel)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));
		priv->model = g_object_ref(pModel);
		g_signal_connect (priv->model, "row-added",   G_CALLBACK (v_multi_line_speller_row_added_cb),   pSelf );
		g_signal_connect (priv->model, "row-changed", G_CALLBACK (v_multi_line_speller_row_changed_cb), pSelf );
		g_signal_connect (priv->model, "row-removed", G_CALLBACK (v_multi_line_speller_row_removed_cb), pSelf );
		thornbury_model_register_column_changed_cb(priv->model,v_multi_line_speller_column_changed_cb,pSelf);
	}
	v_multi_line_speller_update_view(pSelf);
	g_object_notify (G_OBJECT(pSelf), "model");
}

/********************************************************
 * Function : v_mildenhall_speller_multi_line_entry_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multi_line_entry_get_property (GObject *pObject, guint uinPropertyId,
		GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY (pObject);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_TEXT:
	{
		g_value_set_variant (pValue, pSelf->priv->argList);
		break;
	}
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		g_value_set_boolean (pValue, pSelf->priv->clearText);
		break;
	}
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_MODEL:
	{
		g_value_set_object (pValue, pSelf->priv->model);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}

/********************************************************
 * Function : v_mildenhall_speller_multi_line_entry_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multi_line_entry_set_property (GObject *pObject, guint uinPropertyId,
		const GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY (pObject);
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_TEXT:
	{
		v_multi_line_speller_set_text (pSelf, g_value_get_variant( pValue ));
		break;
	}
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		v_multi_line_speller_set_clear_text (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_MODEL:
	{
		v_multi_line_speller_set_model (pSelf, g_value_get_object ( pValue ));
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}

/********************************************************
 * Function : v_mildenhall_speller_multi_line_entry_constructed
 * Description: the constructor function is called by g_object_new()
                to complete the object initialization after all the
                construction properties are set.
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multi_line_entry_constructed (GObject *pObject)
{
	MildenhallSpellerMultiLineEntry *pSelf = MILDENHALL_SPELLER_MULTI_LINE_ENTRY(pObject);
	MildenhallSpellerMultiLineEntryPrivate *priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE(pSelf);
        ThornburyItemFactory *itemFactory = NULL;
        GObject *pTextObject = NULL;
        MildenhallTextBoxEntry *pTextBox = NULL;
        ThornburyItemFactory *ToggleLeftitemFactory = NULL;
        GObject *pToggleLeftObject = NULL;
        ThornburyItemFactory *ToggleRightitemFactory = NULL;
        GObject *pToggleRightObject = NULL;
        MildenhallToggleButton *leftToggle = NULL;
        MildenhallToggleButton *rightToggle = NULL;

	ClutterColor lineColor = {0xFF,0xFF,0xFF,0x33};
	ClutterColor backGroundColor = {0x00,0x00,0x00,0xCC};
        MILDENHALL_MULTI_LINE_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);

	//create a group for speller
	priv->multilineSpellerGroup = clutter_actor_new();
	clutter_actor_set_position( priv->multilineSpellerGroup ,0 ,0);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->multilineSpellerGroup);

	// create a blank rectangle to add entry items
	//priv->blankRect = clutter_rectangle_new_with_color(&backGroundColor);
	priv->blankRect = clutter_actor_new();
	clutter_actor_set_background_color ( priv->blankRect,&backGroundColor);
	clutter_actor_set_size(priv->blankRect,MILDENHALL_BLANK_RECT_WIDTH,MILDENHALL_BLANK_RECT_HEIGHT);
	clutter_actor_add_child(priv->multilineSpellerGroup, priv->blankRect);
	clutter_actor_set_position( priv->blankRect,0,0);
	clutter_actor_set_reactive( priv->blankRect,TRUE);

	itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TEXT_BOX_ENTRY, PKGDATADIR"/mildenhall_multi_line_speller_text_box_prop.json");
	g_object_get(itemFactory, "object", &pTextObject, NULL);
	pTextBox = MILDENHALL_TEXT_BOX_ENTRY (pTextObject);
	priv->textBoxEntry = CLUTTER_ACTOR(pTextBox);
	clutter_actor_add_child(priv->multilineSpellerGroup, priv->textBoxEntry);
	g_object_set(priv->textBoxEntry, "focus-cursor",TRUE, NULL);
	g_signal_connect(priv->textBoxEntry,"scroller-status",  G_CALLBACK(v_scroller_status_cb),pObject);

	ToggleLeftitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_multi_line_speller_left_toggle_prop.json");
	g_object_get(ToggleLeftitemFactory, "object", &pToggleLeftObject, NULL);
	leftToggle = MILDENHALL_TOGGLE_BUTTON (pToggleLeftObject);
	priv->leftButton = CLUTTER_ACTOR(leftToggle);
	clutter_actor_add_child(priv->multilineSpellerGroup,priv->leftButton);

	ToggleRightitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_multi_line_speller_right_toggle_prop.json");
	g_object_get(ToggleRightitemFactory, "object", &pToggleRightObject, NULL);
	rightToggle = MILDENHALL_TOGGLE_BUTTON (pToggleRightObject);
	priv->rightButton = CLUTTER_ACTOR(rightToggle);
	clutter_actor_add_child(priv->multilineSpellerGroup,priv->rightButton);

	g_signal_connect(priv->leftButton, "button-toggled",  G_CALLBACK(v_multi_line_left_toggle_button_cb), pObject);
	g_signal_connect(priv->rightButton,"button-toggled",  G_CALLBACK(v_multi_line_right_toggle_button_cb),pObject);

	//priv->leftLine= clutter_rectangle_new_with_color(&lineColor);
	priv->leftLine = clutter_actor_new();
	clutter_actor_set_background_color ( priv->leftLine,&lineColor);
	clutter_actor_add_child(priv->multilineSpellerGroup, priv->leftLine);
	clutter_actor_set_size(priv->leftLine,1,63);

	// priv->rightLine= clutter_rectangle_new_with_color(&lineColor);
	priv->rightLine = clutter_actor_new();
	clutter_actor_set_background_color ( priv->rightLine,&lineColor);
	clutter_actor_add_child(priv->multilineSpellerGroup, priv->rightLine);
	clutter_actor_set_size(priv->rightLine,1,63);

	priv->pTextBoxModel = p_multi_line_create_text_box_model();
	priv->pRightModel   = p_multi_line_create_toggle_model();
	priv->pLeftModel    = p_multi_line_create_toggle_model();
}

/********************************************************
 * Function : v_mildenhall_speller_multi_line_entry_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multi_line_entry_dispose (GObject *pObject)
{
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_multi_line_entry_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : v_mildenhall_speller_multi_line_entry_finalize
 * Description: Finalize the speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_multi_line_entry_finalize (GObject *pObject)
{
	MILDENHALL_MULTI_LINE_SPELLER_PRINT("MILDENHALL_MULTI_LINE_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_multi_line_entry_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_speller_multi_line_entry_class_init
 * Description: Class initialisation function for the pObject type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_multi_line_entry_class_init (MildenhallSpellerMultiLineEntryClass *pKlass)
{
        GObjectClass *pObject_class = G_OBJECT_CLASS (pKlass);
        GParamSpec *pPspec = NULL;
	MILDENHALL_MULTI_LINE_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	g_type_class_add_private (pKlass, sizeof (MildenhallSpellerMultiLineEntryPrivate));

	pObject_class->get_property = v_mildenhall_speller_multi_line_entry_get_property;
	pObject_class->set_property = v_mildenhall_speller_multi_line_entry_set_property;
	pObject_class->dispose      = v_mildenhall_speller_multi_line_entry_dispose;
	pObject_class->finalize     = v_mildenhall_speller_multi_line_entry_finalize;
	pObject_class->constructed  = v_mildenhall_speller_multi_line_entry_constructed;

    /**
     * MildenhallSpellerMultiLineEntry:text:
     *
     * this to set text
     */
	pPspec = g_param_spec_variant("text",
			"text",
			"text",
			G_VARIANT_TYPE("a{ss}"),NULL,
			(G_PARAM_READWRITE));
	g_object_class_install_property ( pObject_class, MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_TEXT, pPspec );

    /**
     * MildenhallSpellerMultiLineEntry:clear-text:
     *
     * this to clear text
     */
	pPspec = g_param_spec_boolean ( "clear-text",
			"clear-text",
			"clear-text",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_CLEAR_TEXT, pPspec );

    /**
     * MildenhallSpellerMultiLineEntry:model:
     *
     * this to set model
     */
	pPspec = g_param_spec_object ("model",
			"model",
			"model",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObject_class, MILDENHALL_MULTI_LINE_SPELLER_PROP_ENUM_MODEL, pPspec);
}

/********************************************************
 * Function : mildenhall_speller_multi_line_entry_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_multi_line_entry_init (MildenhallSpellerMultiLineEntry *pSelf)
{
	pSelf->priv = SPELLER_MULTI_LINE_ENTRY_PRIVATE (pSelf);
	pSelf->priv->blankRect    = NULL;
	pSelf->priv->textBoxEntry = NULL;
	pSelf->priv->leftButton   = NULL;
	pSelf->priv->rightButton  = NULL;
	pSelf->priv->leftLine     = NULL;
	pSelf->priv->rightLine    = NULL;
	pSelf->priv->multilineSpellerGroup = NULL;
	pSelf->priv->inColumnId = -1;
}

/**
 * mildenhall_speller_multi_line_entry_new:
 *
 * Creates a speller multi line entry object
 *
 * Returns: (transfer full): speller multi line entry object
 *
 */
ClutterActor *mildenhall_speller_multi_line_entry_new (void)
{
	MILDENHALL_MULTI_LINE_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	return g_object_new (MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, NULL);
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_speller.h"

#include <thornbury/thornbury.h>

#include "mildenhall_toggle_button.h"

#define MILDENHALL_TEST_SPELLER_PRINT(...) //g_print(__VA_ARGS__)

/* header model columns */
enum //for default entry
{
    COLUMN_LEFT_IS_TEXT,
    COLUMN_LEFT_WIDTH,
    COLUMN_LEFT_ID,
    COLUMN_LEFT_INFO,
    COLUMN_RIGHT_IS_TEXT,
    COLUMN_RIGHT_WIDTH,
    COLUMN_RIGHT_ID,
    COLUMN_RIGHT_INFO,
    COLUMN_TEXT_BOX_DEFAULT_TEXT,
    COLUMN_ENTRY_ID,
    COLUMN_LAST
};

ClutterActor *pDefaultSpeller = NULL;
gboolean binitail = TRUE;

gboolean v_value(ThornburyModel* pSpellerModel)
{
    GVariantBuilder *pRowValues = NULL;
    pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
    g_variant_builder_add(pRowValues, "{ss}", "SEARCH", "SEARCCH");
    g_variant_builder_add(pRowValues, "{ss}", "URL", "URRL");

    GVariantBuilder *pRowValues1 = NULL;
    pRowValues1 = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
    g_variant_builder_add(pRowValues1, "{ss}", "album", "/test-drawer-base/icon_music_artists_AC.png");
    g_variant_builder_add(pRowValues1, "{ss}", "artist", "/test-drawer-base/icon_internet_AC.png" );

    GVariant *p = g_variant_builder_end(pRowValues);
    GVariant *q = g_variant_builder_end(pRowValues1);

        /* append model info for default entry */
    thornbury_model_append ( pSpellerModel,
                         COLUMN_LEFT_IS_TEXT ,TRUE,
                         COLUMN_LEFT_WIDTH ,58.0,
                         COLUMN_LEFT_ID ,g_strdup("BUTTON1"),
                         COLUMN_LEFT_INFO, (gpointer)p,
                         COLUMN_RIGHT_IS_TEXT ,FALSE,
                         COLUMN_RIGHT_WIDTH ,158.0,
                         COLUMN_RIGHT_ID ,g_strdup("BUTTON2"),
                         COLUMN_RIGHT_INFO, (gpointer)q,
                         COLUMN_TEXT_BOX_DEFAULT_TEXT, "dhee",
                         COLUMN_ENTRY_ID ,g_strdup("entry"),
                         -1);
    return FALSE;
}


gboolean mildenhall_test_speller_go_cb(ClutterActor *actor, GVariant *text ,gpointer user_data)
{
    GVariantIter iter;
    gchar *pText = NULL;
    gchar *key = NULL;
    g_variant_iter_init (&iter, text);
    while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
    {
        g_print(" ###### %s %s #####\n",key,pText);
    }
    mildenhall_speller_hide(actor,TRUE);
    //thornbury_model_remove((ThornburyModel*)user_data,0);
    binitail = FALSE;

    return TRUE;
}

static gboolean speller_shown_cb (ClutterActor *actor,  gpointer user_data)
{
    MILDENHALL_TEST_SPELLER_PRINT("%s",__FUNCTION__);
    if(binitail != TRUE)
    {
       // v_value((ThornburyModel*)user_data);
    }
	return FALSE;
}

static gboolean
speller_entry_status_cb (MildenhallSpeller *self,
                         const gchar *pButtonName,
                         const gchar *pButtonId,
                         gpointer user_data)
{
    MILDENHALL_TEST_SPELLER_PRINT(" function name = %s pButtonName = %s pButtonId = %s \n",__FUNCTION__,pButtonName,pButtonId);
	return FALSE;
}

static gboolean speller_hidden_cb (ClutterActor *actor,  gpointer user_data)
{
    MILDENHALL_TEST_SPELLER_PRINT("%s",__FUNCTION__);
    ThornburyModel *model=NULL;
    g_object_get(pDefaultSpeller,"model",&model,NULL);
    //g_object_set( actor, "clear-entry", TRUE, NULL);

    if(NULL != model)
    {
        GValue value = { 0, };
        g_value_init(&value, G_TYPE_STRING);
        g_value_set_string(&value, g_strdup("dhee"));

        ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(model , 0);
        if( NULL != pIter)
        {
            thornbury_model_iter_set_value(pIter ,COLUMN_TEXT_BOX_DEFAULT_TEXT , &value );
        }
        //thornbury_model_insert_value(model, 0, COLUMN_TEXT_BOX_DEFAULT_TEXT, &value);
    }
	return FALSE;
}

/* creation of model */
ThornburyModel *create_speller_model()
{
    /* model for default entry */
    ThornburyModel *model = (ThornburyModel *)thornbury_list_model_new (  COLUMN_LAST,
                                    G_TYPE_BOOLEAN ,0,
                                    G_TYPE_FLOAT , 0,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_POINTER, NULL,
                                    G_TYPE_BOOLEAN ,0,
                                    G_TYPE_FLOAT , 0,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_POINTER, NULL,
                                    G_TYPE_STRING, NULL,
                                    G_TYPE_STRING, NULL,
                                    -1);
    return model;
}

int main (int argc, char **argv)
{
    g_autofree gchar *pImage = NULL;
    g_autofree gchar *overlay_img_file = NULL;
    g_autofree gchar *prop_file = NULL;

    int clInErr = clutter_init(&argc, &argv);
    if (clInErr != CLUTTER_INIT_SUCCESS)
        return -1;

    ClutterActor *stage;
    stage = clutter_stage_new ();

    pImage = g_build_filename (_mildenhall_get_theme_path (),
        "test-drawer-base",
        "background.png",
        NULL);

    ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
    clutter_actor_add_child(stage, actor);

    clutter_actor_set_size (stage, 728, 480);
    g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

    overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
        "content_overlay.png",
        NULL);
    ClutterActor *overlay = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
    clutter_actor_add_child(stage, overlay);

    prop_file = _mildenhall_get_resource_path ("mildenhall_speller_prop.json");
    ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props (
        MILDENHALL_TYPE_SPELLER, prop_file);
    GObject *pObject = NULL;
    g_object_get(itemFactory, "object", &pObject, NULL);
    MildenhallSpeller *pSelf = MILDENHALL_SPELLER(pObject);
    pDefaultSpeller = CLUTTER_ACTOR(pSelf);
    clutter_actor_add_child(stage, pDefaultSpeller);
    clutter_actor_set_position(pDefaultSpeller, 0, 459);

    ThornburyModel *pSpellerModel = create_speller_model();

    GVariantBuilder *pRowValues = NULL;
    pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
    g_variant_builder_add(pRowValues, "{ss}","SEARCH","SEARCH");
    g_variant_builder_add(pRowValues, "{ss}","URL","URL" );

    GVariantBuilder *pRowValues1 = NULL;
    pRowValues1 = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
    g_variant_builder_add(pRowValues1, "{ss}", "album", "/test-drawer-base/icon_music_artists_AC.png");
    g_variant_builder_add(pRowValues1, "{ss}", "artist", "/test-drawer-base/icon_internet_AC.png" );

    GVariant *p = g_variant_builder_end(pRowValues);
    GVariant *q = g_variant_builder_end(pRowValues1);

        /* append model info for default entry */
    thornbury_model_append ( pSpellerModel,
                         COLUMN_LEFT_IS_TEXT ,TRUE,
                         COLUMN_LEFT_WIDTH ,158.0,
                         COLUMN_LEFT_ID ,g_strdup("BUTTON1"),
                         COLUMN_LEFT_INFO, (gpointer)p,
                         COLUMN_RIGHT_IS_TEXT ,FALSE,
                         COLUMN_RIGHT_WIDTH ,58.0,
                         COLUMN_RIGHT_ID ,g_strdup("BUTTON2"),
                         COLUMN_RIGHT_INFO, (gpointer)q,
                         COLUMN_TEXT_BOX_DEFAULT_TEXT, "",
                         COLUMN_ENTRY_ID ,g_strdup("entry"),
                         -1);

    g_object_set(pDefaultSpeller,"model", pSpellerModel, NULL);

    g_signal_connect(pDefaultSpeller, "go-pressed",G_CALLBACK(mildenhall_test_speller_go_cb), pSpellerModel);
	g_signal_connect_after (pDefaultSpeller, "speller-shown", G_CALLBACK (speller_shown_cb), pSpellerModel);
	g_signal_connect_after (pDefaultSpeller, "speller-hidden", G_CALLBACK (speller_hidden_cb), pSpellerModel);
	g_signal_connect_after (pDefaultSpeller, "entry-status", G_CALLBACK (speller_entry_status_cb), pSpellerModel);

    clutter_actor_show (stage);
    clutter_main();
    return 0;
}

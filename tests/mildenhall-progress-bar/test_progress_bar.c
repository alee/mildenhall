/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_progress_bar.h"

#include <stdio.h>
#include <thornbury/thornbury.h>

static gboolean on_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
static gboolean on_seek_updated_cb(MildenhallProgressBar *progressBar, gpointer userData);
static gboolean on_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
static gboolean set_current_duration(gpointer data);
int main(int argc, char *argv[])
{
	ClutterActor *stage;
	ClutterColor stageColor = {0x00, 0x00, 0x00, 0xff};

	gfloat progressBarWidth;
	gboolean playState=FALSE;

	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;
	ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_PROGRESS_BAR, PKGDATADIR"/mildenhall_progress_bar_prop.json");
	GObject *pObject = NULL;
	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallProgressBar *progressBar=MILDENHALL_PROGRESS_BAR(pObject);
	g_object_get(progressBar,"width",&progressBarWidth,NULL);

	g_print("Test Program progressBarWidth value is %f",progressBarWidth);
	g_object_set(progressBar,"current-duration",0.5,NULL);
	g_object_set(progressBar,"play-state",playState,NULL);
	g_object_get(progressBar,"play-state",&playState,NULL);
	stage = clutter_stage_new();
	clutter_actor_set_background_color(CLUTTER_ACTOR(stage), &stageColor);
	clutter_actor_set_size(stage, 800, 600);


		if(playState)
		{
			on_play_requested_cb(progressBar,NULL);
		}

	g_signal_connect (progressBar, "play-requested", G_CALLBACK (on_play_requested_cb), progressBar);
	g_signal_connect (progressBar, "pause-requested", G_CALLBACK (on_pause_requested_cb), progressBar);
	g_signal_connect (progressBar, "progress-updated", G_CALLBACK (on_seek_updated_cb), progressBar);

	clutter_actor_add_child(CLUTTER_ACTOR(stage), CLUTTER_ACTOR(progressBar));
	clutter_actor_set_position(CLUTTER_ACTOR(progressBar), 30, 200);
	//Use when progress bar is non-reactive
	//g_timeout_add_seconds(1,set_current_duration,(gpointer)progressBar);
	g_print("After calling");
	clutter_actor_show(stage);
	clutter_main();
	return 0;
}

static gboolean on_seek_updated_cb(MildenhallProgressBar *progressBar, gpointer userData)
{

	g_print("\n \n Seek Updated \n \n");
	return TRUE;

}

static gboolean on_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{

	g_print("Play Requested");
	//Use when progress bar is reactive
	g_timeout_add_seconds(1,set_current_duration,(gpointer)progressBar);

return TRUE;
}

gdouble dur=0.0;
gfloat wid=750.0;
gint count=0;
gdouble durt=0.10;
static gboolean set_current_duration(gpointer data)
{
	MildenhallProgressBar *progressBar = MILDENHALL_PROGRESS_BAR(data);
	dur=dur+0.01;
	g_print("Test Client dur value = %lf",dur);
	g_object_set(progressBar,"current-duration",dur,NULL);
	// When count  becomes 10 change the width
	if(count==10)
	{
		g_print("Count value is = %d",count);
		g_object_set(progressBar,"width",wid,NULL);
		g_object_set(progressBar,"opacity",50,NULL);
	}
	g_print("Success");
	count++;
	return TRUE;
}
static gboolean on_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	g_print("Pause Requested");
	return TRUE;
}

